.class public abstract enum LoT;
.super Ljava/lang/Enum;
.source "TermsOfServiceDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LoT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LoT;

.field private static final synthetic a:[LoT;

.field public static final enum b:LoT;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, LoU;

    const-string v1, "NO_ACCEPTANCE_REQUIRED"

    invoke-direct {v0, v1, v2}, LoU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LoT;->a:LoT;

    .line 64
    new-instance v0, LoV;

    const-string v1, "ACCEPTANCE_REQUIRED_ONCE"

    invoke-direct {v0, v1, v3}, LoV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LoT;->b:LoT;

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [LoT;

    sget-object v1, LoT;->a:LoT;

    aput-object v1, v0, v2

    sget-object v1, LoT;->b:LoT;

    aput-object v1, v0, v3

    sput-object v0, LoT;->a:[LoT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILoR;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, LoT;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LoT;
    .registers 2
    .parameter

    .prologue
    .line 52
    const-class v0, LoT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LoT;

    return-object v0
.end method

.method public static values()[LoT;
    .registers 1

    .prologue
    .line 52
    sget-object v0, LoT;->a:[LoT;

    invoke-virtual {v0}, [LoT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LoT;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lo;Landroid/content/Context;)V
.end method

.method public abstract a(Lo;Landroid/content/Context;)Z
.end method
