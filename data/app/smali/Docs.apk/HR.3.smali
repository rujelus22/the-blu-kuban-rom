.class public LHR;
.super Ljava/lang/Object;
.source "CellView.java"


# static fields
.field private static b:Landroid/graphics/Paint;


# instance fields
.field private a:D

.field private a:F

.field private a:LHP;

.field private a:Landroid/graphics/Bitmap;

.field private a:Landroid/graphics/Paint;

.field private a:Ljava/lang/String;

.field private a:Z

.field private b:D

.field private b:Z

.field private c:D


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, LHR;->b:Landroid/graphics/Paint;

    .line 54
    sget-object v0, LHR;->b:Landroid/graphics/Paint;

    const/16 v1, 0x19

    const/16 v2, 0xff

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 55
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method private b(FFFLandroid/graphics/Canvas;Landroid/graphics/Paint;LHS;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, LHR;->a:LHP;

    if-eqz v0, :cond_46

    .line 134
    invoke-virtual {p4, p1, p2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 135
    iget-wide v2, p0, LHR;->a:D

    double-to-float v0, v2

    mul-float v3, v0, p3

    iget-wide v4, p0, LHR;->b:D

    double-to-float v0, v4

    mul-float v4, v0, p3

    iget-object v5, p0, LHR;->a:Landroid/graphics/Paint;

    move-object v0, p4

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 137
    sget-object v0, LHS;->a:LHS;

    if-eq p6, v0, :cond_2e

    .line 138
    iget-wide v2, p0, LHR;->a:D

    double-to-float v0, v2

    mul-float v3, v0, p3

    iget-wide v4, p0, LHR;->b:D

    double-to-float v0, v4

    mul-float v4, v0, p3

    sget-object v5, LHR;->b:Landroid/graphics/Paint;

    move-object v0, p4

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 142
    :cond_2e
    const/high16 v0, 0x4000

    mul-float/2addr v0, p3

    .line 143
    iget-wide v1, p0, LHR;->c:D

    double-to-float v1, v1

    mul-float/2addr v1, p3

    .line 144
    invoke-virtual {p4, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 145
    iget-object v2, p0, LHR;->a:LHP;

    invoke-virtual {v2, p4, p3}, LHP;->a(Landroid/graphics/Canvas;F)V

    .line 146
    neg-float v2, p1

    sub-float v0, v2, v0

    neg-float v2, p2

    sub-float v1, v2, v1

    invoke-virtual {p4, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 150
    :cond_46
    return-void
.end method


# virtual methods
.method public a()D
    .registers 3

    .prologue
    .line 65
    iget-wide v0, p0, LHR;->a:D

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, LHR;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(FFFLandroid/graphics/Canvas;Landroid/graphics/Paint;LHS;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, LHR;->a:Landroid/graphics/Bitmap;

    .line 126
    iput p3, p0, LHR;->a:F

    .line 127
    invoke-direct/range {p0 .. p6}, LHR;->b(FFFLandroid/graphics/Canvas;Landroid/graphics/Paint;LHS;)V

    .line 129
    return-void
.end method

.method public a(LIS;)V
    .registers 11
    .parameter

    .prologue
    const-wide/high16 v7, 0x4010

    const-wide/high16 v5, 0x4000

    .line 73
    invoke-virtual {p1}, LIS;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LHR;->a:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, LIS;->a()LIW;

    move-result-object v0

    .line 77
    invoke-virtual {p1}, LIS;->a()LJI;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, LJI;->a()D

    move-result-wide v2

    iput-wide v2, p0, LHR;->a:D

    .line 79
    invoke-virtual {v1}, LJI;->b()D

    move-result-wide v2

    iput-wide v2, p0, LHR;->b:D

    .line 80
    invoke-virtual {v1}, LJI;->a()V

    .line 82
    const/high16 v1, -0x4080

    iput v1, p0, LHR;->a:F

    .line 83
    const/4 v1, 0x0

    iput-object v1, p0, LHR;->a:Landroid/graphics/Bitmap;

    .line 85
    invoke-static {p1}, LHQ;->a(LIS;)LHP;

    move-result-object v1

    iput-object v1, p0, LHR;->a:LHP;

    .line 86
    invoke-virtual {v0}, LIW;->a()LJL;

    move-result-object v1

    .line 87
    sget-object v2, LJL;->a:LJL;

    if-ne v1, v2, :cond_6b

    .line 88
    iput-wide v5, p0, LHR;->c:D

    .line 95
    :goto_38
    invoke-virtual {v0}, LIW;->b()LIT;

    move-result-object v1

    invoke-static {v1}, LHQ;->a(LIT;)I

    move-result v1

    .line 96
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 97
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, LHR;->a:Landroid/graphics/Paint;

    .line 99
    iget-object v2, p0, LHR;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 101
    invoke-virtual {v0}, LIW;->a()LIV;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, LIV;->b()Z

    move-result v2

    iput-boolean v2, p0, LHR;->a:Z

    .line 103
    invoke-virtual {v1}, LIV;->a()Z

    move-result v2

    iput-boolean v2, p0, LHR;->b:Z

    .line 105
    invoke-virtual {v0}, LIW;->a()V

    .line 106
    invoke-virtual {v1}, LIV;->a()V

    .line 107
    return-void

    .line 89
    :cond_6b
    sget-object v2, LJL;->c:LJL;

    if-ne v1, v2, :cond_7f

    .line 90
    iget-wide v1, p0, LHR;->b:D

    iget-object v3, p0, LHR;->a:LHP;

    invoke-virtual {v3}, LHP;->a()I

    move-result v3

    int-to-double v3, v3

    sub-double/2addr v1, v3

    sub-double/2addr v1, v7

    div-double/2addr v1, v5

    add-double/2addr v1, v5

    iput-wide v1, p0, LHR;->c:D

    goto :goto_38

    .line 92
    :cond_7f
    iget-wide v1, p0, LHR;->b:D

    iget-object v3, p0, LHR;->a:LHP;

    invoke-virtual {v3}, LHP;->a()I

    move-result v3

    int-to-double v3, v3

    sub-double/2addr v1, v3

    sub-double/2addr v1, v7

    add-double/2addr v1, v5

    iput-wide v1, p0, LHR;->c:D

    goto :goto_38
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 153
    iget-boolean v0, p0, LHR;->a:Z

    return v0
.end method

.method public b()D
    .registers 3

    .prologue
    .line 69
    iget-wide v0, p0, LHR;->b:D

    return-wide v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 157
    iget-boolean v0, p0, LHR;->b:Z

    return v0
.end method
