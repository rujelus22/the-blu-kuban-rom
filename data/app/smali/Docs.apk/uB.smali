.class LuB;
.super Ljava/lang/Object;
.source "FontPicker.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic a:Landroid/widget/RadioButton;

.field final synthetic a:Landroid/widget/ScrollView;

.field final synthetic a:Luw;


# direct methods
.method constructor <init>(Luw;Landroid/widget/RadioButton;Landroid/widget/ScrollView;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 372
    iput-object p1, p0, LuB;->a:Luw;

    iput-object p2, p0, LuB;->a:Landroid/widget/RadioButton;

    iput-object p3, p0, LuB;->a:Landroid/widget/ScrollView;

    iput-object p4, p0, LuB;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .registers 4

    .prologue
    .line 375
    iget-object v0, p0, LuB;->a:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getTop()I

    move-result v0

    iget-object v1, p0, LuB;->a:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 376
    iget-object v1, p0, LuB;->a:Landroid/widget/ScrollView;

    iget-object v2, p0, LuB;->a:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->getLeft()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 377
    iget-object v0, p0, LuB;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 378
    return-void
.end method
