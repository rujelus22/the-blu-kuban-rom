.class public LiW;
.super Ljava/lang/Object;
.source "EntriesFilterCriterion.java"

# interfaces
.implements LiE;


# instance fields
.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lfg;

.field private final a:Ljava/lang/String;

.field private final a:Llf;

.field private final a:LmK;

.field private a:Lnh;

.field private final a:Z

.field private b:Ljava/lang/String;

.field private final b:Z


# direct methods
.method constructor <init>(LmK;ZLaoz;Llf;Lfg;Ljava/lang/String;Z)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LmK;",
            "Z",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Llf;",
            "Lfg;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LmK;

    iput-object v0, p0, LiW;->a:LmK;

    .line 63
    iput-boolean p2, p0, LiW;->a:Z

    .line 64
    invoke-static {p6}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LiW;->a:Ljava/lang/String;

    .line 65
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p0, LiW;->a:Laoz;

    .line 66
    invoke-static {p4}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p0, LiW;->a:Llf;

    .line 67
    iput-boolean p7, p0, LiW;->b:Z

    .line 68
    invoke-static {p5}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfg;

    iput-object v0, p0, LiW;->a:Lfg;

    .line 69
    return-void
.end method

.method public static a(Landroid/os/Bundle;Laoz;Llf;Lfg;)LiW;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Llf;",
            "Lfg;",
            ")",
            "LiW;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 174
    invoke-static {p0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string v1, "accountName"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 177
    const-string v1, "filter"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    if-eqz v1, :cond_37

    invoke-static {v1}, LmK;->valueOf(Ljava/lang/String;)LmK;

    move-result-object v1

    .line 179
    :goto_16
    const-string v2, "isInheritable"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    .line 180
    const-string v2, "isMainFilter"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    .line 181
    const-string v2, "isInheritable"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 182
    const-string v5, "isMainFilter"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 184
    if-eqz v6, :cond_36

    if-eqz v1, :cond_36

    if-eqz v3, :cond_36

    if-nez v4, :cond_39

    .line 187
    :cond_36
    :goto_36
    return-object v0

    :cond_37
    move-object v1, v0

    .line 178
    goto :goto_16

    .line 187
    :cond_39
    new-instance v0, LiW;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v7}, LiW;-><init>(LmK;ZLaoz;Llf;Lfg;Ljava/lang/String;Z)V

    goto :goto_36
.end method


# virtual methods
.method public a()LWr;
    .registers 2

    .prologue
    .line 193
    iget-object v0, p0, LiW;->a:LmK;

    invoke-virtual {v0}, LmK;->a()LWr;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, LiW;->b:Ljava/lang/String;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, LiW;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a()LmK;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, LiW;->a:LmK;

    return-object v0
.end method

.method public a()Lnh;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, LiW;->a:Lnh;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v0, p0, LiW;->a:Lnh;

    return-object v0
.end method

.method public a()V
    .registers 6

    .prologue
    .line 73
    iget-object v0, p0, LiW;->a:Lfg;

    iget-object v1, p0, LiW;->a:LmK;

    invoke-virtual {v1}, LmK;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lfg;->a(I)I

    move-result v1

    .line 75
    iget-object v0, p0, LiW;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 76
    if-eqz v1, :cond_57

    if-eqz v0, :cond_57

    .line 77
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LiW;->b:Ljava/lang/String;

    .line 78
    iget-boolean v1, p0, LiW;->b:Z

    if-nez v1, :cond_32

    .line 79
    sget v1, Len;->navigation_filter:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LiW;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LiW;->b:Ljava/lang/String;

    .line 85
    :cond_32
    :goto_32
    iget-object v0, p0, LiW;->a:Llf;

    iget-object v1, p0, LiW;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 86
    if-nez v0, :cond_5c

    .line 87
    new-instance v0, LiF;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Account is null for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LiW;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LiF;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_57
    const-string v0, ""

    iput-object v0, p0, LiW;->b:Ljava/lang/String;

    goto :goto_32

    .line 89
    :cond_5c
    iget-object v1, p0, LiW;->a:LmK;

    invoke-virtual {v1, v0}, LmK;->a(LkB;)Lnh;

    move-result-object v0

    iput-object v0, p0, LiW;->a:Lnh;

    .line 90
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 142
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const-string v0, "kind"

    const-string v1, "entriesFilter"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v0, "accountName"

    iget-object v1, p0, LiW;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "filter"

    iget-object v1, p0, LiW;->a:LmK;

    invoke-virtual {v1}, LmK;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "isInheritable"

    iget-boolean v1, p0, LiW;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 148
    const-string v0, "isMainFilter"

    iget-boolean v1, p0, LiW;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 149
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 106
    iget-boolean v0, p0, LiW;->a:Z

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 113
    iget-boolean v0, p0, LiW;->b:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 118
    if-ne p1, p0, :cond_5

    .line 126
    :cond_4
    :goto_4
    return v0

    .line 120
    :cond_5
    instance-of v2, p1, LiW;

    if-eqz v2, :cond_27

    .line 121
    check-cast p1, LiW;

    .line 122
    iget-object v2, p0, LiW;->a:LmK;

    iget-object v3, p1, LiW;->a:LmK;

    invoke-virtual {v2, v3}, LmK;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    iget-boolean v2, p0, LiW;->a:Z

    iget-boolean v3, p1, LiW;->a:Z

    if-ne v2, v3, :cond_25

    iget-object v2, p0, LiW;->a:Ljava/lang/String;

    iget-object v3, p1, LiW;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_25
    move v0, v1

    goto :goto_4

    :cond_27
    move v0, v1

    .line 126
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 136
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, LiW;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LiW;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LiW;->a:LmK;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LiW;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LiW;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 159
    const-string v0, "EntriesFilterCriterion {accountName=%s, filter=%s, isInheritable=%s, isMainFilter=%s}"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LiW;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LiW;->a:LmK;

    invoke-virtual {v3}, LmK;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, LiW;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, LiW;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
