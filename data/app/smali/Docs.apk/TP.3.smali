.class public final enum LTP;
.super Ljava/lang/Enum;
.source "ContactSharingOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LTP;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LTP;

.field private static final synthetic a:[LTP;

.field public static final enum b:LTP;

.field public static final enum c:LTP;

.field public static final enum d:LTP;


# instance fields
.field private final a:I

.field private final a:LeG;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 31
    new-instance v0, LTP;

    const-string v1, "WRITER"

    sget v2, Len;->contact_sharing_writer:I

    sget-object v3, LeG;->b:LeG;

    invoke-direct {v0, v1, v4, v2, v3}, LTP;-><init>(Ljava/lang/String;IILeG;)V

    sput-object v0, LTP;->a:LTP;

    .line 32
    new-instance v0, LTP;

    const-string v1, "COMMENTER"

    sget v2, Len;->contact_sharing_commenter:I

    sget-object v3, LeG;->c:LeG;

    invoke-direct {v0, v1, v5, v2, v3}, LTP;-><init>(Ljava/lang/String;IILeG;)V

    sput-object v0, LTP;->b:LTP;

    .line 33
    new-instance v0, LTP;

    const-string v1, "READER"

    sget v2, Len;->contact_sharing_reader:I

    sget-object v3, LeG;->d:LeG;

    invoke-direct {v0, v1, v6, v2, v3}, LTP;-><init>(Ljava/lang/String;IILeG;)V

    sput-object v0, LTP;->c:LTP;

    .line 36
    new-instance v0, LTP;

    const-string v1, "NO_ACCESS"

    sget v2, Len;->contact_sharing_no_access:I

    sget-object v3, LeG;->f:LeG;

    invoke-direct {v0, v1, v7, v2, v3}, LTP;-><init>(Ljava/lang/String;IILeG;)V

    sput-object v0, LTP;->d:LTP;

    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [LTP;

    sget-object v1, LTP;->a:LTP;

    aput-object v1, v0, v4

    sget-object v1, LTP;->b:LTP;

    aput-object v1, v0, v5

    sget-object v1, LTP;->c:LTP;

    aput-object v1, v0, v6

    sget-object v1, LTP;->d:LTP;

    aput-object v1, v0, v7

    sput-object v0, LTP;->a:[LTP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILeG;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LeG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput p3, p0, LTP;->a:I

    .line 45
    iput-object p4, p0, LTP;->a:LeG;

    .line 46
    return-void
.end method

.method public static a(LeG;)LTP;
    .registers 6
    .parameter

    .prologue
    .line 57
    invoke-static {}, LTP;->values()[LTP;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_1a

    aget-object v0, v2, v1

    .line 58
    invoke-virtual {v0}, LTP;->a()LeG;

    move-result-object v4

    invoke-virtual {v4, p0}, LeG;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 62
    :goto_15
    return-object v0

    .line 57
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 62
    :cond_1a
    sget-object v0, LTP;->d:LTP;

    goto :goto_15
.end method

.method public static a(Ljava/util/Set;)Laji;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LeG;",
            ">;)",
            "Laji",
            "<",
            "LTP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Laji;->a()Lajj;

    move-result-object v1

    .line 107
    invoke-static {}, LTP;->values()[LTP;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v3, :cond_2c

    aget-object v4, v2, v0

    .line 108
    invoke-virtual {v4}, LTP;->a()LeG;

    move-result-object v5

    .line 109
    invoke-virtual {v5}, LeG;->a()Ljava/util/Set;

    move-result-object v6

    .line 115
    invoke-interface {v6}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_22

    .line 116
    invoke-virtual {v1, v4}, Lajj;->a(Ljava/lang/Object;)Lajj;

    .line 107
    :cond_1f
    :goto_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 117
    :cond_22
    invoke-interface {p0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 118
    invoke-virtual {v1, v4}, Lajj;->a(Ljava/lang/Object;)Lajj;

    goto :goto_1f

    .line 121
    :cond_2c
    invoke-virtual {v1}, Lajj;->a()Laji;

    move-result-object v0

    return-object v0
.end method

.method public static a(LkY;LZj;Llf;)Laji;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkY;",
            "LZj;",
            "Llf;",
            ")",
            "Laji",
            "<",
            "LTP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, LkY;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, LZj;->a(Ljava/lang/String;)LZi;

    move-result-object v0

    .line 83
    iget-object v1, p0, LkY;->a:Ljava/lang/String;

    invoke-interface {p2, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 84
    iget-object v2, p0, LkY;->b:Ljava/lang/String;

    invoke-interface {p2, v1, v2}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v1

    .line 85
    if-nez v1, :cond_37

    .line 86
    const-string v0, "ContactSharingOption"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load the Entry with resourceId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LkY;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 92
    :goto_32
    invoke-static {v0}, LTP;->a(Ljava/util/Set;)Laji;

    move-result-object v0

    return-object v0

    .line 89
    :cond_37
    invoke-virtual {v0, v1}, LZi;->a(LkO;)Ljava/util/Set;

    move-result-object v0

    goto :goto_32
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LTP;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 127
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTP;

    .line 128
    invoke-virtual {v0}, LTP;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 131
    :cond_20
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)LTP;
    .registers 2
    .parameter

    .prologue
    .line 28
    const-class v0, LTP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LTP;

    return-object v0
.end method

.method public static values()[LTP;
    .registers 1

    .prologue
    .line 28
    sget-object v0, LTP;->a:[LTP;

    invoke-virtual {v0}, [LTP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LTP;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 49
    iget v0, p0, LTP;->a:I

    return v0
.end method

.method public a()LeG;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, LTP;->a:LeG;

    return-object v0
.end method
