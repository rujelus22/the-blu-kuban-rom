.class Lmc;
.super Landroid/os/AsyncTask;
.source "DiscussionSessionApi.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lafo;

.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LlW;

.field final synthetic a:Lmb;


# direct methods
.method constructor <init>(Lmb;Ljava/lang/String;Lafo;Ljava/lang/Runnable;LlW;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 203
    iput-object p1, p0, Lmc;->a:Lmb;

    iput-object p2, p0, Lmc;->a:Ljava/lang/String;

    iput-object p3, p0, Lmc;->a:Lafo;

    iput-object p4, p0, Lmc;->a:Ljava/lang/Runnable;

    iput-object p5, p0, Lmc;->a:LlW;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Object;)Ljava/lang/Void;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 212
    :try_start_1
    iget-object v0, p0, Lmc;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_8
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_8} :catch_27
    .catch LadQ; {:try_start_1 .. :try_end_8} :catch_3e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_8} :catch_4e
    .catch LNt; {:try_start_1 .. :try_end_8} :catch_45
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_8} :catch_4c

    .line 213
    :try_start_8
    iget-object v0, p0, Lmc;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)LNe;

    move-result-object v0

    iget-object v2, p0, Lmc;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "oauth2:https://www.googleapis.com/auth/discussions"

    invoke-interface {v0, v2, v3}, LNe;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 214
    if-nez v0, :cond_2e

    .line 215
    new-instance v0, LNt;

    const-string v2, "null OAuth token of type: oauth2:https://www.googleapis.com/auth/discussions"

    invoke-direct {v0, v2}, LNt;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :catchall_24
    move-exception v0

    monitor-exit v1
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_24

    :try_start_26
    throw v0
    :try_end_27
    .catch Landroid/accounts/AccountsException; {:try_start_26 .. :try_end_27} :catch_27
    .catch LadQ; {:try_start_26 .. :try_end_27} :catch_3e
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_27} :catch_4e
    .catch LNt; {:try_start_26 .. :try_end_27} :catch_45
    .catch Ljava/lang/RuntimeException; {:try_start_26 .. :try_end_27} :catch_4c

    .line 223
    :catch_27
    move-exception v0

    .line 239
    :goto_28
    iget-object v1, p0, Lmc;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    .line 240
    :goto_2d
    return-object v5

    .line 219
    :cond_2e
    :try_start_2e
    iget-object v2, p0, Lmc;->a:Lmb;

    iget-object v3, p0, Lmc;->a:Ljava/lang/String;

    iget-object v4, p0, Lmc;->a:Lafo;

    invoke-static {v2, v0, v3, v4}, Lmb;->a(Lmb;Ljava/lang/String;Ljava/lang/String;Lafo;)V

    .line 220
    iget-object v0, p0, Lmc;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 221
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_2e .. :try_end_3d} :catchall_24

    goto :goto_2d

    .line 225
    :catch_3e
    move-exception v0

    .line 227
    iget-object v1, p0, Lmc;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    goto :goto_28

    .line 230
    :catch_45
    move-exception v0

    .line 232
    iget-object v1, p0, Lmc;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)V

    goto :goto_28

    .line 233
    :catch_4c
    move-exception v0

    goto :goto_28

    .line 228
    :catch_4e
    move-exception v0

    goto :goto_28
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 203
    invoke-virtual {p0, p1}, Lmc;->a([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
