.class Lapn;
.super Ljava/lang/Object;
.source "ConstructorBindingImpl.java"

# interfaces
.implements LapY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LapY",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<*>;"
        }
    .end annotation
.end field

.field private a:Lapo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lapo",
            "<TT;>;"
        }
    .end annotation
.end field

.field private a:Laqz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laqz",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Z

.field private b:Z


# direct methods
.method constructor <init>(ZLaop;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Laop",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-boolean p1, p0, Lapn;->a:Z

    .line 255
    iput-object p2, p0, Lapn;->a:Laop;

    .line 256
    return-void
.end method

.method static synthetic a(Lapn;)Lapo;
    .registers 2
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, Lapn;->a:Lapo;

    return-object v0
.end method

.method static synthetic a(Lapn;Lapo;)Lapo;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 246
    iput-object p1, p0, Lapn;->a:Lapo;

    return-object p1
.end method

.method static synthetic a(Lapn;Laqz;)Laqz;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 246
    iput-object p1, p0, Lapn;->a:Laqz;

    return-object p1
.end method

.method static synthetic a(Lapn;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 246
    iput-boolean p1, p0, Lapn;->b:Z

    return p1
.end method


# virtual methods
.method public a(Lapu;LapX;Larg;Z)Ljava/lang/Object;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapu;",
            "LapX;",
            "Larg",
            "<*>;Z)TT;"
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lapn;->a:Lapo;

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_5
    const-string v1, "Constructor not ready"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 263
    iget-boolean v0, p0, Lapn;->a:Z

    if-eqz v0, :cond_1d

    if-nez p4, :cond_1d

    .line 264
    iget-object v0, p0, Lapn;->a:Laop;

    invoke-virtual {p1, v0}, Lapu;->b(Laop;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 261
    :cond_1b
    const/4 v0, 0x0

    goto :goto_5

    .line 269
    :cond_1d
    iget-object v0, p0, Lapn;->a:Lapo;

    invoke-virtual {p3}, Larg;->a()Laop;

    move-result-object v1

    invoke-virtual {v1}, Laop;->a()LaoL;

    move-result-object v1

    invoke-virtual {v1}, LaoL;->a()Ljava/lang/Class;

    move-result-object v3

    iget-boolean v4, p0, Lapn;->b:Z

    iget-object v5, p0, Lapn;->a:Laqz;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lapo;->a(Lapu;LapX;Ljava/lang/Class;ZLaqz;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
