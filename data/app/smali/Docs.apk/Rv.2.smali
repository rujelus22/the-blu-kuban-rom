.class LRv;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"

# interfaces
.implements LQT;


# instance fields
.field final synthetic a:I

.field final synthetic a:LRp;


# direct methods
.method constructor <init>(LRp;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 278
    iput-object p1, p0, LRv;->a:LRp;

    iput p2, p0, LRv;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    .line 288
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRv;->a:LRp;

    invoke-static {v2}, LRp;->b(LRp;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSlideContentRequestFailed: slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRv;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v0, p0, LRv;->a:LRp;

    iget-object v0, v0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LRw;

    invoke-direct {v1, p0}, LRw;-><init>(LRv;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 296
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 281
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRv;->a:LRp;

    invoke-static {v2}, LRp;->b(LRp;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Received content: slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRv;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v0, p0, LRv;->a:LRp;

    invoke-static {v0, p1, p2}, LRp;->a(LRp;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    return-void
.end method
