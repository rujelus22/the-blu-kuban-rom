.class public LCQ;
.super Ljava/lang/Object;
.source "XmlHttpRequestRelay.java"


# static fields
.field private static a:LCZ;


# instance fields
.field private a:LCW;

.field private a:LCY;

.field private final a:LKl;

.field private final a:LKt;

.field private final a:LNS;

.field private final a:LNj;

.field private a:Les;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/concurrent/Semaphore;

.field private a:Lorg/apache/http/client/methods/HttpRequestBase;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 221
    new-instance v0, LCZ;

    invoke-direct {v0}, LCZ;-><init>()V

    sput-object v0, LCQ;->a:LCZ;

    return-void
.end method

.method public constructor <init>(LNj;Ljava/lang/String;LNS;Ljava/lang/String;LCW;LKl;LKt;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput-object v0, p0, LCQ;->a:Les;

    .line 231
    iput-object v0, p0, LCQ;->a:LCY;

    .line 232
    iput-object v0, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 233
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, LCQ;->a:Ljava/util/concurrent/Semaphore;

    .line 241
    iput-object p1, p0, LCQ;->a:LNj;

    .line 242
    iput-object p2, p0, LCQ;->b:Ljava/lang/String;

    .line 243
    iput-object p4, p0, LCQ;->a:Ljava/lang/String;

    .line 244
    iput-object p5, p0, LCQ;->a:LCW;

    .line 245
    iput-object p7, p0, LCQ;->a:LKt;

    .line 246
    iput-object p3, p0, LCQ;->a:LNS;

    .line 247
    iput-object p6, p0, LCQ;->a:LKl;

    .line 248
    return-void
.end method

.method static synthetic a(LCQ;)LCW;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LCQ;->a:LCW;

    return-object v0
.end method

.method static synthetic a(LCQ;)LCY;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LCQ;->a:LCY;

    return-object v0
.end method

.method public static a()LCZ;
    .registers 1

    .prologue
    .line 727
    sget-object v0, LCQ;->a:LCZ;

    invoke-virtual {v0}, LCZ;->a()LCZ;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LCQ;)LNj;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LCQ;->a:LNj;

    return-object v0
.end method

.method static synthetic a(LCQ;)Les;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LCQ;->a:Les;

    return-object v0
.end method

.method static synthetic a(LCQ;Lorg/apache/http/HttpEntity;)Ljava/io/InputStreamReader;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1}, LCQ;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStreamReader;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStreamReader;
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 308
    .line 309
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    invoke-direct {p0, v0}, LCQ;->a(Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_6a

    .line 312
    :try_start_b
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    :try_end_e
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_b .. :try_end_e} :catch_42

    move-result-object v0

    .line 320
    :goto_f
    if-nez v0, :cond_15

    .line 321
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    .line 327
    :cond_15
    :try_start_15
    iget-object v1, p0, LCQ;->a:LNj;

    invoke-interface {v1, p1}, LNj;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;
    :try_end_1a
    .catchall {:try_start_15 .. :try_end_1a} :catchall_5d

    move-result-object v1

    .line 328
    :try_start_1b
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    :try_end_20
    .catchall {:try_start_1b .. :try_end_20} :catchall_68

    .line 330
    if-nez v3, :cond_27

    if-eqz v1, :cond_27

    .line 331
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 335
    :cond_27
    if-nez v3, :cond_67

    .line 336
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to create stream reader. Encoding = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 313
    :catch_42
    move-exception v0

    .line 314
    const-string v0, "XmlHttpRequestRelay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported character set returned: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 316
    goto :goto_f

    .line 330
    :catchall_5d
    move-exception v0

    move-object v1, v2

    :goto_5f
    if-nez v2, :cond_66

    if-eqz v1, :cond_66

    .line 331
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_66
    throw v0

    .line 339
    :cond_67
    return-object v3

    .line 330
    :catchall_68
    move-exception v0

    goto :goto_5f

    :cond_6a
    move-object v0, v2

    goto :goto_f
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 464
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 465
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 467
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 468
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_19

    .line 471
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 474
    :cond_19
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2e

    iget-object v3, p0, LCQ;->a:LKt;

    invoke-virtual {v3}, LKt;->a()Z

    move-result v3

    if-eqz v3, :cond_35

    invoke-virtual {v0}, Landroid/net/Uri;->getPort()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_35

    .line 475
    :cond_2e
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 478
    :cond_35
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/apache/http/Header;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 292
    if-nez p1, :cond_4

    .line 304
    :cond_3
    :goto_3
    return-object v0

    .line 296
    :cond_4
    invoke-interface {p1}, Lorg/apache/http/Header;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v2

    .line 297
    array-length v3, v2

    const/4 v1, 0x0

    :goto_a
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 298
    const-string v5, "charset"

    invoke-interface {v4, v5}, Lorg/apache/http/HeaderElement;->getParameterByName(Ljava/lang/String;)Lorg/apache/http/NameValuePair;

    move-result-object v4

    .line 299
    if-eqz v4, :cond_1b

    .line 300
    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 297
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_a
.end method

.method public static a([Lorg/apache/http/Header;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 629
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 630
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    array-length v0, p0

    if-ge v1, v0, :cond_44

    .line 631
    aget-object v3, p0, v1

    .line 632
    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 633
    if-eqz v0, :cond_38

    .line 635
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 636
    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 637
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    :goto_34
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 640
    :cond_38
    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_34

    .line 643
    :cond_44
    const-string v0, "Set-Cookie"

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 644
    const-string v0, "Set-Cookie2"

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LCQ;)Ljava/util/concurrent/Semaphore;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LCQ;->a:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic a(LCQ;LCY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct/range {p0 .. p5}, LCQ;->a(LCY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method private a(LCY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v7, 0xea60

    const/4 v1, 0x0

    .line 395
    invoke-static {p4}, LCQ;->a(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v3

    move v2, v1

    .line 396
    :goto_9
    const/4 v0, 0x4

    if-ge v2, v0, :cond_118

    .line 397
    invoke-direct {p0}, LCQ;->c()V

    .line 399
    const-string v0, "POST"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 400
    const-string v0, "XmlHttpRequestRelay"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "POST("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, LCY;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\npayload: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 402
    if-eqz p5, :cond_56

    .line 403
    new-instance v4, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v4, p5}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 406
    :cond_56
    sget-object v4, LCQ;->a:LCZ;

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, LCZ;->b(I)V

    .line 407
    invoke-direct {p0, v0}, LCQ;->a(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 415
    :goto_62
    iget-object v0, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 416
    invoke-static {v0, v7}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 417
    invoke-static {v0, v7}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 418
    iget-object v4, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v4, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 420
    array-length v4, v3

    move v0, v1

    :goto_75
    if-ge v0, v4, :cond_b0

    aget-object v5, v3, v0

    .line 421
    iget-object v6, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v6, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Lorg/apache/http/Header;)V

    .line 420
    add-int/lit8 v0, v0, 0x1

    goto :goto_75

    .line 409
    :cond_81
    const-string v0, "XmlHttpRequestRelay"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GET("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, LCY;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, LCQ;->a(Lorg/apache/http/client/methods/HttpRequestBase;)V

    goto :goto_62

    .line 424
    :cond_b0
    iget-object v0, p0, LCQ;->a:LNj;

    iget-object v4, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {v0, v4}, LNj;->a(Lorg/apache/http/HttpRequest;)V

    .line 426
    sget-object v0, LCQ;->a:LCZ;

    iget-object v4, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpRequestBase;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    invoke-virtual {v0, v4}, LCZ;->b([Lorg/apache/http/Header;)V

    .line 429
    iget-object v0, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    const-string v4, "User-Agent"

    iget-object v5, p0, LCQ;->a:LNS;

    const-string v6, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.32 Safari/535.1"

    invoke-interface {v5, v6}, LNS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iget-object v4, p0, LCQ;->a:LNj;

    iget-object v0, p0, LCQ;->a:LKt;

    invoke-virtual {v0}, LKt;->a()Z

    move-result v0

    if-eqz v0, :cond_107

    const/4 v0, 0x0

    :goto_dc
    iget-object v5, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    iget-object v6, p0, LCQ;->b:Ljava/lang/String;

    invoke-interface {v4, v0, v5, v6}, LNj;->a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 434
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    .line 436
    invoke-static {v4}, LCQ;->c(I)Z

    move-result v4

    if-eqz v4, :cond_120

    .line 438
    const-string v4, "Location"

    invoke-interface {v0, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 439
    iget-object v4, p0, LCQ;->a:LNj;

    invoke-interface {v4}, LNj;->a()V

    .line 440
    if-nez v0, :cond_10a

    .line 441
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No redirect location available"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 432
    :cond_107
    iget-object v0, p0, LCQ;->a:Ljava/lang/String;

    goto :goto_dc

    .line 443
    :cond_10a
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object p3

    .line 444
    iget-object v0, p0, LCQ;->a:LNj;

    invoke-interface {v0}, LNj;->b()V

    .line 396
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_9

    .line 450
    :cond_118
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Too many redirects"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446
    :cond_120
    return-object v0
.end method

.method static synthetic a(LCQ;)Lorg/apache/http/client/methods/HttpRequestBase;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    return-object v0
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 692
    iget-object v0, p0, LCQ;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 694
    new-instance v0, LCV;

    invoke-direct {v0, p0, p1}, LCV;-><init>(LCQ;I)V

    .line 715
    iget-object v1, p0, LCQ;->a:LCY;

    invoke-virtual {v1}, LCY;->a()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 716
    iget-object v1, p0, LCQ;->a:LKl;

    sget-object v2, LKs;->b:LKs;

    invoke-virtual {v1, v0, v2}, LKl;->a(LKp;LKs;)V

    .line 720
    :goto_19
    return-void

    .line 718
    :cond_1a
    invoke-interface {v0}, LKp;->a()V

    goto :goto_19
.end method

.method static synthetic a(LCQ;)V
    .registers 1
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, LCQ;->c()V

    return-void
.end method

.method static synthetic a(LCQ;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1}, LCQ;->a(I)V

    return-void
.end method

.method static synthetic a(LCQ;Ljava/io/InputStreamReader;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1}, LCQ;->a(Ljava/io/InputStreamReader;)V

    return-void
.end method

.method static synthetic a(LCQ;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, LCQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/io/InputStreamReader;)V
    .registers 6
    .parameter

    .prologue
    .line 352
    .line 354
    const/16 v0, 0x4000

    new-array v0, v0, [C

    .line 355
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 356
    monitor-enter p0

    .line 357
    :try_start_a
    iget-object v1, p0, LCQ;->a:LCY;

    sget-object v2, LCX;->d:LCX;

    invoke-virtual {v1, v2}, LCY;->a(LCX;)LCY;

    .line 358
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_a .. :try_end_12} :catchall_34

    .line 359
    const-string v1, ""

    const-string v2, ""

    invoke-direct {p0, v1, v2}, LCQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :goto_19
    invoke-virtual {p1, v0}, Ljava/io/InputStreamReader;->read([C)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_37

    .line 363
    invoke-direct {p0}, LCQ;->c()V

    .line 366
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Ljava/lang/String;-><init>([CII)V

    const-string v3, ""

    invoke-direct {p0, v2, v3}, LCQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    sget-object v2, LCQ;->a:LCZ;

    invoke-virtual {v2, v1}, LCZ;->a(I)V

    goto :goto_19

    .line 358
    :catchall_34
    move-exception v0

    :try_start_35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    throw v0

    .line 374
    :cond_37
    monitor-enter p0

    .line 375
    :try_start_38
    iget-object v0, p0, LCQ;->a:LCY;

    sget-object v1, LCX;->e:LCX;

    invoke-virtual {v0, v1}, LCY;->a(LCX;)LCY;

    .line 376
    monitor-exit p0
    :try_end_40
    .catchall {:try_start_38 .. :try_end_40} :catchall_4b

    .line 378
    invoke-direct {p0}, LCQ;->c()V

    .line 384
    const-string v0, ""

    const-string v1, ""

    invoke-direct {p0, v0, v1}, LCQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    return-void

    .line 376
    :catchall_4b
    move-exception v0

    :try_start_4c
    monitor-exit p0
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 653
    iget-object v0, p0, LCQ;->a:LCY;

    invoke-virtual {v0}, LCY;->a()LCX;

    move-result-object v3

    .line 654
    iget-object v0, p0, LCQ;->a:LCY;

    invoke-virtual {v0}, LCY;->b()I

    move-result v4

    .line 655
    iget-object v0, p0, LCQ;->a:LCY;

    invoke-virtual {v0}, LCY;->a()Ljava/lang/String;

    move-result-object v5

    .line 656
    iget-object v0, p0, LCQ;->a:LCY;

    invoke-virtual {v0}, LCY;->a()I

    move-result v2

    .line 658
    iget-object v0, p0, LCQ;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 662
    new-instance v0, LCU;

    move-object v1, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LCU;-><init>(LCQ;ILCX;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    iget-object v1, p0, LCQ;->a:LCY;

    invoke-virtual {v1}, LCY;->a()Z

    move-result v1

    if-eqz v1, :cond_35

    .line 685
    iget-object v1, p0, LCQ;->a:LKl;

    sget-object v2, LKs;->b:LKs;

    invoke-virtual {v1, v0, v2}, LKl;->a(LKp;LKs;)V

    .line 689
    :goto_34
    return-void

    .line 687
    :cond_35
    invoke-interface {v0}, LKp;->a()V

    goto :goto_34
.end method

.method private declared-synchronized a(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .registers 3
    .parameter

    .prologue
    .line 454
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, LCQ;->c()V

    .line 455
    iput-object p1, p0, LCQ;->a:Lorg/apache/http/client/methods/HttpRequestBase;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 456
    monitor-exit p0

    return-void

    .line 454
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(I)Z
    .registers 2
    .parameter

    .prologue
    .line 56
    invoke-static {p0}, LCQ;->b(I)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)[Lorg/apache/http/Header;
    .registers 7
    .parameter

    .prologue
    .line 606
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 608
    :try_start_5
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 609
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 610
    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 611
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 612
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 613
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    invoke-direct {v5, v0, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_26
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_26} :catch_27

    goto :goto_e

    .line 615
    :catch_27
    move-exception v0

    .line 616
    const-string v0, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse request headers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    :cond_40
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/http/Header;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/http/Header;

    return-object v0
.end method

.method static synthetic b()LCZ;
    .registers 1

    .prologue
    .line 56
    sget-object v0, LCQ;->a:LCZ;

    return-object v0
.end method

.method public static b()V
    .registers 1

    .prologue
    .line 723
    sget-object v0, LCQ;->a:LCZ;

    invoke-virtual {v0}, LCZ;->a()V

    .line 724
    return-void
.end method

.method private static b(I)Z
    .registers 2
    .parameter

    .prologue
    .line 252
    const/16 v0, 0xc8

    if-lt p0, v0, :cond_8

    const/16 v0, 0x190

    if-lt p0, v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private c()V
    .registers 2

    .prologue
    .line 260
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, LCQ;->a:LCW;

    if-nez v0, :cond_10

    .line 261
    :cond_a
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 263
    :cond_10
    return-void
.end method

.method private static c(I)Z
    .registers 2
    .parameter

    .prologue
    .line 256
    const/16 v0, 0x12c

    if-lt p0, v0, :cond_a

    const/16 v0, 0x190

    if-ge p0, v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 4

    .prologue
    .line 269
    monitor-enter p0

    :try_start_1
    const-string v0, "XmlHttpRequestRelay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Aborting HTTP request ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LCQ;->a:LCY;

    invoke-virtual {v2}, LCY;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, LCQ;->a:LCW;

    .line 274
    new-instance v0, LCR;

    invoke-direct {v0, p0}, LCR;-><init>(LCQ;)V

    invoke-virtual {v0}, LCR;->start()V
    :try_end_30
    .catchall {:try_start_1 .. :try_end_30} :catchall_32

    .line 289
    monitor-exit p0

    return-void

    .line 269
    :catchall_32
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 495
    new-instance v0, LCY;

    invoke-direct {v0, p6, p2}, LCY;-><init>(ZI)V

    iput-object v0, p0, LCQ;->a:LCY;

    .line 497
    invoke-direct {p0, p4, p1}, LCQ;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 498
    new-instance v0, LCS;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p5

    move-object v5, p7

    move v6, p2

    invoke-direct/range {v0 .. v6}, LCS;-><init>(LCQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 576
    if-eqz p6, :cond_24

    .line 577
    new-instance v1, LCT;

    invoke-direct {v1, p0, v0}, LCT;-><init>(LCQ;Ljava/lang/Runnable;)V

    iput-object v1, p0, LCQ;->a:Les;

    .line 584
    iget-object v0, p0, LCQ;->a:Les;

    invoke-virtual {v0}, Les;->start()V

    .line 588
    :goto_23
    return-void

    .line 586
    :cond_24
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_23
.end method
