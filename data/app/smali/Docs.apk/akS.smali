.class LakS;
.super Ljava/lang/ref/SoftReference;
.source "MapMakerInternalMap.java"

# interfaces
.implements LakQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/SoftReference",
        "<TK;>;",
        "LakQ",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field final a:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile a:Lalh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lalh",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILakQ;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1225
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1292
    invoke-static {}, Lakn;->a()Lalh;

    move-result-object v0

    iput-object v0, p0, LakS;->a:Lalh;

    .line 1226
    iput p3, p0, LakS;->a:I

    .line 1227
    iput-object p4, p0, LakS;->a:LakQ;

    .line 1228
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 1308
    iget v0, p0, LakS;->a:I

    return v0
.end method

.method public a()J
    .registers 2

    .prologue
    .line 1238
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1313
    iget-object v0, p0, LakS;->a:LakQ;

    return-object v0
.end method

.method public a()Lalh;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lalh",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1296
    iget-object v0, p0, LakS;->a:Lalh;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1232
    invoke-virtual {p0}, LakS;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1243
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1253
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lalh;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lalh",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1301
    iget-object v0, p0, LakS;->a:Lalh;

    .line 1302
    iput-object p1, p0, LakS;->a:Lalh;

    .line 1303
    invoke-interface {v0, p1}, Lalh;->a(Lalh;)V

    .line 1304
    return-void
.end method

.method public b()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1248
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1263
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1258
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1275
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1270
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1285
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1280
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
