.class public LnA;
.super Ljava/lang/Object;
.source "DeleteDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LkB;

.field final synthetic a:Lot;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;LkB;Ljava/lang/String;Lot;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 196
    iput-object p1, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    iput-object p2, p0, LnA;->a:LkB;

    iput-object p3, p0, LnA;->a:Ljava/lang/String;

    iput-object p4, p0, LnA;->a:Lot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 199
    iget-object v0, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Laoo;

    move-result-object v0

    iget-object v1, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 203
    iget-object v0, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_61

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 204
    iget-object v4, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    iget-object v4, v4, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Llf;

    iget-object v5, p0, LnA;->a:LkB;

    invoke-interface {v4, v5, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v4

    .line 205
    if-nez v4, :cond_4f

    .line 206
    add-int/lit8 v1, v1, 0x1

    .line 207
    const-string v4, "DeleteDialogFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Already deleted entry with resourceId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1c

    .line 211
    :cond_4f
    iget-object v0, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Llf;

    iget-object v5, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    iget-object v5, v5, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:LlS;

    iget-object v6, p0, LnA;->a:Ljava/lang/String;

    iget-object v7, p0, LnA;->a:Lot;

    invoke-static {v0, v5, v4, v6, v7}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Llf;LlS;LkO;Ljava/lang/String;LlK;)Z

    move-result v0

    if-nez v0, :cond_73

    .line 218
    :cond_61
    iget-object v0, p0, LnA;->a:Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v1, v0, :cond_77

    .line 219
    iget-object v0, p0, LnA;->a:Lot;

    invoke-virtual {v0, v2, v8}, Lot;->a(ILjava/lang/Throwable;)V

    .line 223
    :goto_72
    return-void

    .line 215
    :cond_73
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 216
    goto :goto_1c

    .line 221
    :cond_77
    iget-object v0, p0, LnA;->a:Lot;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8}, Lot;->a(ILjava/lang/Throwable;)V

    goto :goto_72
.end method
