.class public LIB;
.super Ljava/lang/Object;
.source "GridViewModelHelper.java"


# instance fields
.field private final a:LHT;

.field private final a:LIL;

.field private final a:LIy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LIy",
            "<",
            "LHR;",
            ">;"
        }
    .end annotation
.end field

.field private a:LJa;

.field private final a:LJb;

.field private a:LJs;

.field private final a:LJv;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LIF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LJv;LIL;LIy;LJb;LHT;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LJv;",
            "LIL;",
            "LIy",
            "<",
            "LHR;",
            ">;",
            "LJb;",
            "LHT;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LIB;->a:Ljava/util/List;

    .line 46
    iput-object v1, p0, LIB;->a:LJa;

    .line 47
    iput-object v1, p0, LIB;->a:LJs;

    .line 53
    iput-object p1, p0, LIB;->a:LJv;

    .line 54
    iput-object p2, p0, LIB;->a:LIL;

    .line 55
    iput-object p3, p0, LIB;->a:LIy;

    .line 56
    iput-object p4, p0, LIB;->a:LJb;

    .line 57
    iput-object p5, p0, LIB;->a:LHT;

    .line 59
    new-instance v0, LJa;

    new-instance v1, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    new-instance v2, LIC;

    invoke-direct {v2, p0}, LIC;-><init>(LIB;)V

    invoke-direct {v1, v2}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, p4, v1}, LJa;-><init>(LJb;LIR;)V

    iput-object v0, p0, LIB;->a:LJa;

    .line 71
    new-instance v0, LJs;

    new-instance v1, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    new-instance v2, LID;

    invoke-direct {v2, p0}, LID;-><init>(LIB;)V

    invoke-direct {v1, v2}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, p4, v1}, LJs;-><init>(LJt;LIR;)V

    iput-object v0, p0, LIB;->a:LJs;

    .line 82
    return-void
.end method

.method public static a(LJv;LIL;LHT;)LIB;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 163
    new-instance v3, LIy;

    new-instance v0, LIE;

    invoke-direct {v0}, LIE;-><init>()V

    invoke-direct {v3, v0}, LIy;-><init>(LIA;)V

    .line 176
    invoke-virtual {p1}, LIL;->a()LJw;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    invoke-direct {v1, v3}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, v0, v1}, LJv;->a(LJw;LIR;)LJb;

    move-result-object v4

    .line 179
    new-instance v0, LIB;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LIB;-><init>(LJv;LIL;LIy;LJb;LHT;)V

    return-object v0
.end method

.method static synthetic a(LIB;)LJa;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, LIB;->a:LJa;

    return-object v0
.end method

.method static synthetic a(LIB;)LJs;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, LIB;->a:LJs;

    return-object v0
.end method

.method static synthetic a(LIB;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, LIB;->a:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, LIB;->a:LJv;

    invoke-virtual {v0}, LJv;->a()I

    move-result v0

    return v0
.end method

.method public a(II)LHR;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, LIB;->a:LIy;

    invoke-virtual {v0, p1, p2}, LIy;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHR;

    .line 108
    if-eqz v0, :cond_b

    .line 115
    :goto_a
    return-object v0

    .line 114
    :cond_b
    iget-object v0, p0, LIB;->a:LJb;

    new-instance v1, LJJ;

    invoke-direct {v1, p1, p2}, LJJ;-><init>(II)V

    invoke-static {v0, v1}, LJB;->a(LJb;LJJ;)I

    move-result v0

    .line 115
    iget-object v1, p0, LIB;->a:LIy;

    invoke-virtual {v1, v0}, LIy;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHR;

    goto :goto_a
.end method

.method public a()LHT;
    .registers 2

    .prologue
    .line 149
    iget-object v0, p0, LIB;->a:LHT;

    return-object v0
.end method

.method public a()LJb;
    .registers 2

    .prologue
    .line 119
    iget-object v0, p0, LIB;->a:LJb;

    return-object v0
.end method

.method public a()LKj;
    .registers 4

    .prologue
    .line 127
    invoke-virtual {p0}, LIB;->b()I

    move-result v1

    .line 128
    iget-object v0, p0, LIB;->a:LIL;

    invoke-virtual {v0}, LIL;->e()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 131
    new-instance v0, LKj;

    iget-object v2, p0, LIB;->a:LJb;

    invoke-virtual {v2}, LJb;->a()LJJ;

    move-result-object v2

    invoke-virtual {v2}, LJJ;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    .line 133
    :goto_1b
    return-object v0

    :cond_1c
    if-eqz v1, :cond_25

    new-instance v0, LKj;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, LKj;-><init>(II)V

    goto :goto_1b

    :cond_25
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, LIB;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 95
    iget-object v0, p0, LIB;->a:LJa;

    if-eqz v0, :cond_11

    .line 96
    iget-object v0, p0, LIB;->a:LJa;

    invoke-virtual {v0}, LJa;->a()V

    .line 97
    iput-object v1, p0, LIB;->a:LJa;

    .line 100
    :cond_11
    iget-object v0, p0, LIB;->a:LJs;

    if-eqz v0, :cond_1c

    .line 101
    iget-object v0, p0, LIB;->a:LJs;

    invoke-virtual {v0}, LJs;->a()V

    .line 102
    iput-object v1, p0, LIB;->a:LJs;

    .line 104
    :cond_1c
    return-void
.end method

.method public a(LIF;)V
    .registers 3
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, LIB;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 157
    iget-object v0, p0, LIB;->a:LJv;

    invoke-virtual {v0}, LJv;->b()I

    move-result v0

    return v0
.end method

.method public b()LKj;
    .registers 4

    .prologue
    .line 138
    invoke-virtual {p0}, LIB;->a()I

    move-result v1

    .line 139
    iget-object v0, p0, LIB;->a:LIL;

    invoke-virtual {v0}, LIL;->d()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 142
    new-instance v0, LKj;

    iget-object v2, p0, LIB;->a:LJb;

    invoke-virtual {v2}, LJb;->a()LJJ;

    move-result-object v2

    invoke-virtual {v2}, LJJ;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    .line 144
    :goto_1b
    return-object v0

    :cond_1c
    if-eqz v1, :cond_25

    new-instance v0, LKj;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, LKj;-><init>(II)V

    goto :goto_1b

    :cond_25
    const/4 v0, 0x0

    goto :goto_1b
.end method
