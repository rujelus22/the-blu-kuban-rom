.class public LAo;
.super Ljava/lang/Object;
.source "SectionChildFactory.java"

# interfaces
.implements LAb;


# annotations
.annotation runtime LaoJ;
.end annotation


# instance fields
.field private final a:LAh;

.field private a:Lvz;

.field private a:LwL;

.field private final a:LxX;

.field private final a:Z


# direct methods
.method public constructor <init>(LAh;LxX;Lgl;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v0, p0, LAo;->a:Lvz;

    .line 39
    iput-object v0, p0, LAo;->a:LwL;

    .line 44
    iput-object p1, p0, LAo;->a:LAh;

    .line 45
    iput-object p2, p0, LAo;->a:LxX;

    .line 46
    sget-object v0, Lgi;->v:Lgi;

    invoke-interface {p3, v0}, Lgl;->a(Lgi;)Z

    move-result v0

    iput-boolean v0, p0, LAo;->a:Z

    .line 48
    return-void
.end method

.method private a(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    new-instance v0, LAp;

    const-class v3, LAF;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAp;-><init>(LAo;ILjava/lang/Class;Lzd;Lxu;)V

    return-object v0
.end method

.method static synthetic a(LAo;)LAh;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LAo;->a:LAh;

    return-object v0
.end method

.method static synthetic a(LAo;)Lvz;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LAo;->a:Lvz;

    return-object v0
.end method

.method static synthetic a(LAo;)LwL;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LAo;->a:LwL;

    return-object v0
.end method

.method static synthetic a(LAo;)LxX;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LAo;->a:LxX;

    return-object v0
.end method

.method private b(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    iget-boolean v0, p0, LAo;->a:Z

    if-eqz v0, :cond_1c

    iget-object v0, p0, LAo;->a:Lvz;

    if-eqz v0, :cond_1c

    iget-object v0, p0, LAo;->a:Lvz;

    invoke-interface {v0}, Lvz;->a()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 63
    new-instance v0, LAq;

    const-class v3, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAq;-><init>(LAo;ILjava/lang/Class;Lzd;Lxu;)V

    .line 73
    :goto_1b
    return-object v0

    :cond_1c
    new-instance v0, LAr;

    const-class v3, LAN;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAr;-><init>(LAo;ILjava/lang/Class;Lzd;Lxu;)V

    goto :goto_1b
.end method

.method private c(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    new-instance v0, LAs;

    const-class v3, LAt;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAs;-><init>(LAo;ILjava/lang/Class;Lzd;Lxu;)V

    return-object v0
.end method


# virtual methods
.method public a(IILzd;Lxu;)LAc;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    invoke-interface {p3, p1, p1}, Lzd;->a(II)Lvs;

    move-result-object v0

    .line 102
    invoke-interface {v0}, Lvs;->a()I

    move-result v1

    .line 103
    invoke-interface {v0}, Lvs;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 104
    invoke-static {v1}, LxH;->a(I)LxH;

    move-result-object v1

    .line 106
    sget-object v2, LxH;->f:LxH;

    if-ne v1, v2, :cond_1f

    .line 107
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 108
    invoke-direct {p0, v0, p3, p4}, LAo;->c(ILzd;Lxu;)LAc;

    move-result-object v0

    .line 114
    :goto_1e
    return-object v0

    .line 109
    :cond_1f
    sget-object v2, LxH;->c:LxH;

    if-ne v1, v2, :cond_2c

    .line 110
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 111
    invoke-direct {p0, v0, p3, p4}, LAo;->b(ILzd;Lxu;)LAc;

    move-result-object v0

    goto :goto_1e

    .line 113
    :cond_2c
    invoke-interface {p3, p1}, Lzd;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 114
    invoke-direct {p0, v0, p3, p4}, LAo;->a(ILzd;Lxu;)LAc;

    move-result-object v0

    goto :goto_1e
.end method

.method public a(Lvz;)V
    .registers 2
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, LAo;->a:Lvz;

    .line 123
    return-void
.end method

.method public a(LwL;)V
    .registers 2
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, LAo;->a:LwL;

    .line 131
    return-void
.end method
