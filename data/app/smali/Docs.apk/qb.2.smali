.class public Lqb;
.super LoX;
.source "RemoteOpenerSelector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LoX",
        "<",
        "LkP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Laoz;Laoz;Laoz;Laoz;Laoz;Laoz;Laoz;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DrawingDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/GViewDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/SiteDocumentOpener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static/range {p1 .. p7}, Lqb;->a(Laoz;Laoz;Laoz;Laoz;Laoz;Laoz;Laoz;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, LoX;-><init>(Ljava/util/Map;)V

    .line 31
    return-void
.end method

.method static a(Laoz;Laoz;Laoz;Laoz;Laoz;Laoz;Laoz;)Ljava/util/Map;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DrawingDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/GViewDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/SiteDocumentOpener;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "LkP;",
            "Laoz",
            "<+",
            "LoZ;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 46
    sget-object v1, LkP;->c:LkP;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v1, LkP;->a:LkP;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v1, LkP;->d:LkP;

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v1, LkP;->e:LkP;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v1, LkP;->b:LkP;

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v1, LkP;->j:LkP;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v1, LkP;->k:LkP;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v1, LkP;->i:LkP;

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-object v0
.end method
