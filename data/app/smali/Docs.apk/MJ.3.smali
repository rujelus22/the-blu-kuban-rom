.class public final LMJ;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LME;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 32
    iput-object p1, p0, LMJ;->a:LYD;

    .line 33
    const-class v0, LME;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LMJ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LMJ;->a:LZb;

    .line 36
    const-class v0, LMG;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LMJ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LMJ;->b:LZb;

    .line 39
    return-void
.end method

.method static synthetic a(LMJ;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LMJ;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LMJ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LMJ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 46
    const-class v0, LME;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LMJ;->a:LZb;

    invoke-virtual {p0, v0, v1}, LMJ;->a(Laop;LZb;)V

    .line 47
    const-class v0, LMG;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LMJ;->b:LZb;

    invoke-virtual {p0, v0, v1}, LMJ;->a(Laop;LZb;)V

    .line 48
    iget-object v0, p0, LMJ;->a:LZb;

    iget-object v1, p0, LMJ;->a:LYD;

    iget-object v1, v1, LYD;->a:LMJ;

    iget-object v1, v1, LMJ;->b:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 50
    iget-object v0, p0, LMJ;->b:LZb;

    new-instance v1, LMK;

    invoke-direct {v1, p0}, LMK;-><init>(LMJ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 69
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 73
    return-void
.end method
