.class public LRm;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"

# interfaces
.implements LRk;


# static fields
.field private static final b:I


# instance fields
.field a:I
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "punchThumbnailCacheSize"
    .end annotation
.end field

.field private final a:LKS;

.field private final a:LQR;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 64
    const/4 v0, 0x7

    const/4 v1, 0x2

    const/4 v2, 0x3

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, LRm;->b:I

    return-void
.end method

.method public constructor <init>(Laoz;LQR;LKS;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LQR;",
            "LKS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471
    iput-object p1, p0, LRm;->a:Laoz;

    .line 472
    iput-object p2, p0, LRm;->a:LQR;

    .line 473
    iput-object p3, p0, LRm;->a:LKS;

    .line 474
    return-void
.end method

.method static synthetic a()I
    .registers 1

    .prologue
    .line 46
    sget v0, LRm;->b:I

    return v0
.end method

.method static synthetic a(LRm;)LKS;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LRm;->a:LKS;

    return-object v0
.end method

.method static synthetic a(LRm;)LQR;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LRm;->a:LQR;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 46
    invoke-static {p0}, LRm;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 486
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Character;

    const/4 v1, 0x0

    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x27

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LajX;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 487
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\\"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1d

    .line 489
    :cond_58
    return-object p0
.end method


# virtual methods
.method public a(LRl;)LRj;
    .registers 5
    .parameter

    .prologue
    .line 478
    iget-object v0, p0, LRm;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 479
    new-instance v1, LRo;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, LRo;-><init>(LRm;LRl;Landroid/content/Context;LRn;)V

    return-object v1
.end method
