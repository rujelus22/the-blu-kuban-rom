.class public LoO;
.super Ljava/lang/Object;
.source "RenameDialogFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, LoO;->a:Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    const-string v0, "RenameDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "actionId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    if-nez p2, :cond_20

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_26

    :cond_20
    const/4 v0, 0x6

    if-eq p2, v0, :cond_26

    const/4 v0, 0x5

    if-ne p2, v0, :cond_2d

    .line 114
    :cond_26
    iget-object v0, p0, LoO;->a:Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->p()V

    .line 115
    const/4 v0, 0x1

    .line 117
    :goto_2c
    return v0

    :cond_2d
    const/4 v0, 0x0

    goto :goto_2c
.end method
