.class public final LarG;
.super Ljava/lang/Object;
.source "ProviderLookup.java"

# interfaces
.implements Lari;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lari;"
    }
.end annotation


# instance fields
.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<TT;>;"
        }
    .end annotation
.end field

.field private a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Laop;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Laop",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "source"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LarG;->a:Ljava/lang/Object;

    .line 43
    const-string v0, "key"

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laop;

    iput-object v0, p0, LarG;->a:Laop;

    .line 44
    return-void
.end method

.method static synthetic a(LarG;)Laop;
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, LarG;->a:Laop;

    return-object v0
.end method

.method static synthetic a(LarG;)Laoz;
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, LarG;->a:Laoz;

    return-object v0
.end method


# virtual methods
.method public a()Laop;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laop",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, LarG;->a:Laop;

    return-object v0
.end method

.method public a()Laoz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, LarH;

    invoke-direct {v0, p0}, LarH;-><init>(LarG;)V

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, LarG;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Laoz;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, LarG;->a:Laoz;

    if-nez v0, :cond_15

    const/4 v0, 0x1

    :goto_5
    const-string v1, "delegate already initialized"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 65
    const-string v0, "delegate"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p0, LarG;->a:Laoz;

    .line 66
    return-void

    .line 64
    :cond_15
    const/4 v0, 0x0

    goto :goto_5
.end method
