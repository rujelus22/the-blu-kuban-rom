.class public LqO;
.super Ljava/lang/Object;
.source "ThirdPartyDocumentOpenerImpl.java"

# interfaces
.implements LqN;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

.field private final a:Lgl;

.field private final a:Lqr;

.field private final a:Lqv;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;Lqv;Lqr;Lgl;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, LqO;->a:Landroid/content/Context;

    .line 71
    iput-object p2, p0, LqO;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    .line 72
    iput-object p3, p0, LqO;->a:Lqv;

    .line 73
    iput-object p4, p0, LqO;->a:Lqr;

    .line 74
    iput-object p5, p0, LqO;->a:Lgl;

    .line 75
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;
    .registers 9
    .parameter

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    iget-object v1, p0, LqO;->a:Landroid/content/Context;

    iget-object v2, p0, LqO;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v3, p0, LqO;->a:Lqv;

    iget-object v4, p0, LqO;->a:Lqr;

    iget-object v5, p0, LqO;->a:Lgl;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;-><init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;Lqv;Lqr;Lgl;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)V

    return-object v0
.end method
