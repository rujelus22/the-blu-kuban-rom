.class public Lfa;
.super Ljava/lang/Object;
.source "AccountsActivity.java"

# interfaces
.implements LMF;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/AccountsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/AccountsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 167
    iput-object p1, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 180
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .registers 5
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LdL;

    iget-object v1, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 176
    iget-object v0, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LZM;

    iget-object v1, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    sget v2, Len;->error_failed_to_create_account:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/app/AccountsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LdL;

    iget-object v1, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 171
    iget-object v0, p0, Lfa;->a:Lcom/google/android/apps/docs/app/AccountsActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->b(Lcom/google/android/apps/docs/app/AccountsActivity;)V

    .line 172
    return-void
.end method
