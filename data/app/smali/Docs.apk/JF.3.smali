.class public final LJF;
.super Ljava/lang/Object;
.source "UserSessionEvent.java"


# static fields
.field private static a:I

.field public static final a:LJF;

.field private static a:[LJF;

.field public static final b:LJF;

.field public static final c:LJF;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 55
    new-instance v0, LJF;

    const-string v1, "kUserSessionAdded"

    invoke-direct {v0, v1}, LJF;-><init>(Ljava/lang/String;)V

    sput-object v0, LJF;->a:LJF;

    .line 56
    new-instance v0, LJF;

    const-string v1, "kUserSessionRemoved"

    invoke-direct {v0, v1}, LJF;-><init>(Ljava/lang/String;)V

    sput-object v0, LJF;->b:LJF;

    .line 57
    new-instance v0, LJF;

    const-string v1, "kUserSessionChanged"

    invoke-direct {v0, v1}, LJF;-><init>(Ljava/lang/String;)V

    sput-object v0, LJF;->c:LJF;

    .line 93
    const/4 v0, 0x3

    new-array v0, v0, [LJF;

    sget-object v1, LJF;->a:LJF;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, LJF;->b:LJF;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LJF;->c:LJF;

    aput-object v2, v0, v1

    sput-object v0, LJF;->a:[LJF;

    .line 94
    sput v3, LJF;->a:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, LJF;->a:Ljava/lang/String;

    .line 78
    sget v0, LJF;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LJF;->a:I

    iput v0, p0, LJF;->b:I

    .line 79
    return-void
.end method

.method public static a(I)LJF;
    .registers 4
    .parameter

    .prologue
    .line 68
    sget-object v0, LJF;->a:[LJF;

    array-length v0, v0

    if-ge p0, v0, :cond_14

    if-ltz p0, :cond_14

    sget-object v0, LJF;->a:[LJF;

    aget-object v0, v0, p0

    iget v0, v0, LJF;->b:I

    if-ne v0, p0, :cond_14

    .line 69
    sget-object v0, LJF;->a:[LJF;

    aget-object v0, v0, p0

    .line 72
    :goto_13
    return-object v0

    .line 70
    :cond_14
    const/4 v0, 0x0

    :goto_15
    sget-object v1, LJF;->a:[LJF;

    array-length v1, v1

    if-ge v0, v1, :cond_2a

    .line 71
    sget-object v1, LJF;->a:[LJF;

    aget-object v1, v1, v0

    iget v1, v1, LJF;->b:I

    if-ne v1, p0, :cond_27

    .line 72
    sget-object v1, LJF;->a:[LJF;

    aget-object v0, v1, v0

    goto :goto_13

    .line 70
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 73
    :cond_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LJF;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, LJF;->a:Ljava/lang/String;

    return-object v0
.end method
