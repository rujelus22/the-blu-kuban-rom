.class public Labw;
.super LmH;
.source "DocListView.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/view/DocListView;

.field final synthetic b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/view/DocListView;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[ILqT;I)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 548
    iput-object p1, p0, Labw;->a:Lcom/google/android/apps/docs/view/DocListView;

    iput p8, p0, Labw;->b:I

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LmH;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[ILqT;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 586
    iget-object v0, p0, Labw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2a

    const/4 v0, 0x1

    .line 588
    :goto_e
    sget v2, Leh;->group_title_horizontal_rule:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 589
    if-eqz v3, :cond_1c

    .line 590
    if-eqz v0, :cond_2c

    move v2, v1

    :goto_19
    invoke-static {v3, v2}, LacF;->b(Landroid/view/View;I)V

    .line 593
    :cond_1c
    sget v2, Leh;->entry_horizontal_rule:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 594
    if-eqz v2, :cond_29

    .line 595
    if-eqz v0, :cond_2f

    :goto_26
    invoke-static {v2, v1}, LacF;->b(Landroid/view/View;I)V

    .line 597
    :cond_29
    return-void

    :cond_2a
    move v0, v1

    .line 586
    goto :goto_e

    .line 590
    :cond_2c
    iget v2, p0, Labw;->b:I

    goto :goto_19

    .line 595
    :cond_2f
    iget v1, p0, Labw;->b:I

    goto :goto_26
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 551
    invoke-super {p0, p1, p2, p3}, LmH;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 553
    invoke-static {p3}, LPW;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Labw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v2}, Lcom/google/android/apps/docs/view/DocListView;->a(Lcom/google/android/apps/docs/view/DocListView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 554
    sget v2, Leh;->doc_entry_container:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 555
    if-eqz v2, :cond_1d

    .line 558
    invoke-virtual {v2, v1}, Landroid/view/View;->setActivated(Z)V

    .line 561
    :cond_1d
    iget-object v1, p0, Labw;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 562
    invoke-direct {p0, p1}, Labw;->a(Landroid/view/View;)V

    .line 565
    :cond_2c
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 566
    invoke-virtual {p0, v1}, Labw;->getItemId(I)J

    move-result-wide v2

    .line 567
    const/4 v4, 0x2

    new-array v4, v4, [I

    sget v5, Leh;->preview_button:I

    aput v5, v4, v0

    const/4 v5, 0x1

    sget v6, Leh;->doc_entry_row_root:I

    aput v6, v4, v5

    .line 568
    new-instance v5, Labx;

    invoke-direct {v5, p0, v1, v2, v3}, Labx;-><init>(Labw;IJ)V

    .line 577
    array-length v1, v4

    :goto_46
    if-ge v0, v1, :cond_56

    aget v2, v4, v0

    .line 578
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 579
    if-eqz v2, :cond_53

    .line 580
    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    :cond_53
    add-int/lit8 v0, v0, 0x1

    goto :goto_46

    .line 583
    :cond_56
    return-void
.end method
