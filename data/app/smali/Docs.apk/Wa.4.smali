.class public LWa;
.super Latd;
.source "AclGDataParser.java"


# instance fields
.field private final a:Lgl;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;Lgl;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Latd;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 47
    iput-object p3, p0, LWa;->a:Lgl;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()LVS;
    .registers 2

    .prologue
    .line 52
    new-instance v0, LVS;

    invoke-direct {v0}, LVS;-><init>()V

    return-object v0
.end method

.method public a(LasT;)LVS;
    .registers 3
    .parameter

    .prologue
    .line 88
    invoke-super {p0, p1}, Latd;->a(LasT;)LasT;

    move-result-object v0

    check-cast v0, LVS;

    return-object v0
.end method

.method protected bridge synthetic a()LasT;
    .registers 2

    .prologue
    .line 26
    invoke-virtual {p0}, LWa;->a()LVS;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LasT;)LasT;
    .registers 3
    .parameter

    .prologue
    .line 26
    invoke-virtual {p0, p1}, LWa;->a(LasT;)LVS;

    move-result-object v0

    return-object v0
.end method

.method protected a(LasT;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 57
    instance-of v0, p1, LVS;

    const-string v1, "The entry is NOT AclEntry!"

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, LWa;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 60
    check-cast p1, LVS;

    .line 61
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v2

    .line 63
    const-string v3, "http://schemas.google.com/acl/2007"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 64
    const-string v2, "role"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_48

    .line 65
    const-string v1, "value"

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string v1, "AclGDataParser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "role = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-virtual {p1, v0}, LVS;->b(Ljava/lang/String;)V

    .line 84
    :cond_47
    :goto_47
    return-void

    .line 68
    :cond_48
    const-string v2, "scope"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_85

    .line 69
    sget-object v1, Latf;->l:Ljava/lang/String;

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    const-string v2, "value"

    invoke-interface {v0, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-virtual {p1, v1}, LVS;->c(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1, v0}, LVS;->a(Ljava/lang/String;)V

    .line 73
    const-string v2, "AclGDataParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scope = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_47

    .line 74
    :cond_85
    const-string v2, "additionalRole"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_47

    iget-object v1, p0, LWa;->a:Lgl;

    sget-object v2, Lgi;->a:Lgi;

    invoke-interface {v1, v2}, Lgl;->a(Lgi;)Z

    move-result v1

    if-eqz v1, :cond_47

    .line 76
    const-string v1, "value"

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    const-string v1, "AclGDataParser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "additional role = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-static {v0}, LeD;->a(Ljava/lang/String;)LeD;

    move-result-object v0

    .line 79
    sget-object v1, LeD;->b:LeD;

    if-eq v0, v1, :cond_47

    .line 80
    invoke-virtual {p1, v0}, LVS;->a(LeD;)V

    goto :goto_47
.end method
