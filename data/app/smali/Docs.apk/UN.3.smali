.class public LUN;
.super Ljava/lang/Object;
.source "DocumentFileManagerImpl.java"

# interfaces
.implements LUL;


# instance fields
.field private a:J

.field private final a:LUS;

.field private final a:LUX;

.field private final a:LaaJ;

.field private final a:LaaZ;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LUQ;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/locks/Lock;

.field private final a:Llf;


# direct methods
.method public constructor <init>(LUX;LUS;Llf;LaaJ;Landroid/content/Context;LaaZ;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LUN;->a:Ljava/util/Map;

    .line 157
    iput-object p2, p0, LUN;->a:LUS;

    .line 158
    iput-object p3, p0, LUN;->a:Llf;

    .line 159
    iput-object p1, p0, LUN;->a:LUX;

    .line 160
    iput-object p4, p0, LUN;->a:LaaJ;

    .line 161
    iput-object p5, p0, LUN;->a:Landroid/content/Context;

    .line 162
    iput-object p6, p0, LUN;->a:LaaZ;

    .line 164
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LUN;->a:J

    .line 165
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LUN;->a:Ljava/util/concurrent/locks/Lock;

    .line 166
    return-void
.end method

.method private declared-synchronized a(J)J
    .registers 9
    .parameter

    .prologue
    const-wide/16 v0, 0x0

    .line 281
    monitor-enter p0

    :try_start_3
    iget-object v2, p0, LUN;->a:Llf;

    invoke-interface {v2, p1, p2}, Llf;->a(J)LkN;

    move-result-object v2

    .line 282
    if-nez v2, :cond_25

    .line 283
    const-string v2, "DocumentFileManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Content instance in not found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_4a

    .line 301
    :goto_23
    monitor-exit p0

    return-wide v0

    .line 287
    :cond_25
    :try_start_25
    invoke-direct {p0, v2}, LUN;->a(LkN;)LUQ;

    move-result-object v3

    .line 288
    invoke-virtual {v3}, LUQ;->a()LUT;

    move-result-object v4

    sget-object v5, LUT;->c:LUT;

    if-eq v4, v5, :cond_4d

    .line 289
    const-string v2, "DocumentFileManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Content instance in use: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_25 .. :try_end_49} :catchall_4a

    goto :goto_23

    .line 281
    :catchall_4a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 293
    :cond_4d
    :try_start_4d
    invoke-virtual {v3}, LUQ;->b()J

    move-result-wide v0

    .line 295
    iget-object v4, p0, LUN;->a:Llf;

    invoke-interface {v4, v2}, Llf;->a(LkN;)V

    .line 296
    invoke-virtual {v3}, LUQ;->d()V

    .line 298
    iget-object v2, p0, LUN;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    const-string v2, "DocumentFileManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Deleting content instance: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_84
    .catchall {:try_start_4d .. :try_end_84} :catchall_4a

    goto :goto_23
.end method

.method private declared-synchronized a(LkN;)LUM;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 269
    monitor-enter p0

    :try_start_2
    invoke-direct {p0, p1}, LUN;->a(LkN;)LUQ;

    move-result-object v3

    .line 270
    invoke-virtual {v3}, LUQ;->a()V

    .line 271
    invoke-virtual {v3}, LUQ;->a()Z

    move-result v1

    if-nez v1, :cond_14

    .line 272
    invoke-virtual {v3}, LUQ;->c()V
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_22

    .line 277
    :goto_12
    monitor-exit p0

    return-object v0

    .line 275
    :cond_14
    :try_start_14
    invoke-direct {p0, v3}, LUN;->a(LUQ;)V

    .line 277
    new-instance v0, LUO;

    const/4 v2, 0x0

    const/4 v4, 0x0

    sget-object v5, LUK;->a:LUK;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LUO;-><init>(LUN;LkM;LUQ;Ljava/lang/String;LUK;)V
    :try_end_21
    .catchall {:try_start_14 .. :try_end_21} :catchall_22

    goto :goto_12

    .line 269
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LkN;)LUQ;
    .registers 5
    .parameter

    .prologue
    .line 305
    iget-object v0, p0, LUN;->a:Ljava/util/Map;

    invoke-virtual {p1}, LkN;->c()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUQ;

    .line 306
    if-nez v0, :cond_18

    .line 307
    iget-object v0, p0, LUN;->a:LUS;

    invoke-virtual {v0, p1}, LUS;->a(LkN;)LUQ;

    move-result-object v0

    .line 309
    :cond_18
    return-object v0
.end method

.method static synthetic a(LUN;)LUX;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, LUN;->a:LUX;

    return-object v0
.end method

.method static synthetic a(LUN;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, LUN;->a:Ljava/util/Map;

    return-object v0
.end method

.method private a(LUQ;)V
    .registers 5
    .parameter

    .prologue
    .line 313
    iget-object v0, p0, LUN;->a:Ljava/util/Map;

    invoke-virtual {p1}, LUQ;->a()LkN;

    move-result-object v1

    invoke-virtual {v1}, LkN;->c()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    return-void
.end method

.method private a(Ljava/io/File;)V
    .registers 8
    .parameter

    .prologue
    .line 470
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_26

    aget-object v3, v1, v0

    .line 471
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 472
    invoke-direct {p0, v3}, LUN;->a(Ljava/io/File;)V

    .line 470
    :cond_13
    :goto_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 474
    :cond_16
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 475
    iget-object v5, p0, LUN;->a:Llf;

    invoke-interface {v5, v4}, Llf;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 476
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_13

    .line 480
    :cond_26
    return-void
.end method

.method private a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 353
    if-eqz p1, :cond_2f

    .line 355
    :try_start_2
    iget-object v0, p0, LUN;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_7} :catch_3f

    .line 366
    :cond_7
    :try_start_7
    invoke-direct {p0}, LUN;->c()V

    .line 367
    invoke-direct {p0, p1}, LUN;->b(Z)V

    .line 369
    if-eqz p1, :cond_18

    .line 371
    iget-object v0, p0, LUN;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->b()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, LUN;->a(Ljava/io/File;)V
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_61

    .line 374
    :cond_18
    :try_start_18
    iget-object v0, p0, LUN;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 377
    monitor-enter p0
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_1e} :catch_3f

    .line 378
    :try_start_1e
    iget-object v0, p0, LUN;->a:LUX;

    invoke-interface {v0}, LUX;->a()J

    move-result-wide v0

    iget-object v2, p0, LUN;->a:LUX;

    invoke-interface {v2}, LUX;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LUN;->a:J

    .line 379
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_1e .. :try_end_2e} :catchall_68

    .line 386
    :goto_2e
    return-void

    .line 359
    :cond_2f
    :try_start_2f
    iget-object v0, p0, LUN;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_7

    .line 360
    const-string v0, "DocumentFileManager"

    const-string v1, "Garbage collection already in progress."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_3e} :catch_3f

    goto :goto_2e

    .line 380
    :catch_3f
    move-exception v0

    .line 383
    const-string v1, "DocumentFileManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Garbage collection failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LUN;->a:J

    goto :goto_2e

    .line 374
    :catchall_61
    move-exception v0

    :try_start_62
    iget-object v1, p0, LUN;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_68
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_68} :catch_3f

    .line 379
    :catchall_68
    move-exception v0

    :try_start_69
    monitor-exit p0
    :try_end_6a
    .catchall {:try_start_69 .. :try_end_6a} :catchall_68

    :try_start_6a
    throw v0
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_6b} :catch_3f
.end method

.method private declared-synchronized a(LkN;)Z
    .registers 5
    .parameter

    .prologue
    .line 389
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LUN;->a:Ljava/util/Map;

    invoke-virtual {p1}, LkN;->c()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_16

    move-result-object v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_12
    monitor-exit p0

    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_12

    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Z)V
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 414
    const-wide v2, 0x7fffffffffffffffL

    .line 417
    if-nez p1, :cond_8a

    .line 424
    iget-object v0, p0, LUN;->a:LUX;

    invoke-interface {v0}, LUX;->a()J

    move-result-wide v2

    .line 425
    iget-object v0, p0, LUN;->a:LUX;

    invoke-interface {v0}, LUX;->b()J

    move-result-wide v4

    .line 426
    cmp-long v0, v4, v2

    if-gtz v0, :cond_3b

    .line 427
    const-string v0, "DocumentFileManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Garbage collection skipped "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    :cond_3a
    :goto_3a
    return-void

    .line 432
    :cond_3b
    sub-long v2, v4, v2

    .line 434
    iget-object v0, p0, LUN;->a:LUX;

    invoke-interface {v0}, LUX;->a()I

    move-result v0

    .line 437
    :goto_43
    iget-object v4, p0, LUN;->a:Llf;

    invoke-interface {v4}, Llf;->a()Landroid/database/Cursor;

    move-result-object v4

    .line 438
    if-eqz v4, :cond_3a

    .line 443
    :try_start_4b
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_85

    move-result v5

    if-nez v5, :cond_55

    .line 465
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_3a

    .line 447
    :cond_55
    :try_start_55
    invoke-static {}, LPS;->a()LPS;

    move-result-object v5

    invoke-virtual {v5}, LPS;->f()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 450
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 451
    :goto_65
    sub-int v7, v6, v0

    if-ge v1, v7, :cond_78

    .line 452
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 453
    invoke-direct {p0, v7, v8}, LUN;->a(J)J
    :try_end_70
    .catchall {:try_start_55 .. :try_end_70} :catchall_85

    move-result-wide v7

    .line 454
    sub-long/2addr v2, v7

    .line 455
    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-gtz v7, :cond_7c

    .line 465
    :cond_78
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_3a

    .line 460
    :cond_7c
    :try_start_7c
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_7f
    .catchall {:try_start_7c .. :try_end_7f} :catchall_85

    move-result v7

    if-eqz v7, :cond_78

    .line 451
    add-int/lit8 v1, v1, 0x1

    goto :goto_65

    .line 465
    :catchall_85
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_8a
    move v0, v1

    goto :goto_43
.end method

.method private c()V
    .registers 7

    .prologue
    .line 393
    iget-object v0, p0, LUN;->a:Llf;

    invoke-interface {v0}, Llf;->a()Ljava/util/List;

    move-result-object v0

    .line 394
    if-nez v0, :cond_9

    .line 411
    :cond_8
    return-void

    .line 398
    :cond_9
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkN;

    .line 399
    invoke-direct {p0, v0}, LUN;->a(LkN;)Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 402
    const-string v2, "DocumentFileManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to garbage collect instance: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, LkN;->c()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 408
    :cond_3c
    const-string v2, "DocumentFileManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Deleting unreferenced content instance: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, LkN;->c()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    invoke-direct {p0, v0}, LUN;->a(LkN;)LUQ;

    move-result-object v0

    invoke-virtual {v0}, LUQ;->d()V

    goto :goto_d
.end method


# virtual methods
.method public declared-synchronized a(LkM;LUK;)LUM;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 230
    monitor-enter p0

    :try_start_2
    invoke-virtual {p0, p1, p2}, LUN;->c(LkM;LUK;)Z
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_25

    move-result v1

    if-nez v1, :cond_a

    .line 254
    :goto_8
    monitor-exit p0

    return-object v0

    .line 235
    :cond_a
    :try_start_a
    invoke-virtual {p1, p2}, LkM;->a(LUK;)J

    move-result-wide v1

    .line 236
    iget-object v3, p0, LUN;->a:Llf;

    invoke-interface {v3, v1, v2}, Llf;->a(J)LkN;

    move-result-object v1

    .line 237
    invoke-direct {p0, v1}, LUN;->a(LkN;)LUQ;

    move-result-object v3

    .line 238
    invoke-virtual {v3}, LUQ;->a()V

    .line 240
    invoke-virtual {v3}, LUQ;->a()Z

    move-result v2

    if-nez v2, :cond_28

    .line 241
    invoke-virtual {v3}, LUQ;->c()V
    :try_end_24
    .catchall {:try_start_a .. :try_end_24} :catchall_25

    goto :goto_8

    .line 230
    :catchall_25
    move-exception v0

    monitor-exit p0

    throw v0

    .line 245
    :cond_28
    :try_start_28
    invoke-direct {p0, v3}, LUN;->a(LUQ;)V

    .line 248
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 249
    invoke-virtual {v1, v0}, LkN;->a(Ljava/util/Date;)V

    .line 250
    iget-object v2, p0, LUN;->a:LUX;

    invoke-interface {v2, v1}, LUX;->a(LkN;)V

    .line 251
    invoke-virtual {p1, v0}, LkM;->c(Ljava/util/Date;)V

    .line 252
    iget-object v0, p0, LUN;->a:LUX;

    invoke-interface {v0, p1}, LUX;->a(LkM;)V

    .line 254
    new-instance v0, LUO;

    invoke-virtual {v1}, LkN;->a()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LUO;-><init>(LUN;LkM;LUQ;Ljava/lang/String;LUK;)V
    :try_end_4c
    .catchall {:try_start_28 .. :try_end_4c} :catchall_25

    goto :goto_8
.end method

.method public declared-synchronized a(LkM;Ljava/lang/String;Ljava/lang/String;LUK;Ljava/lang/String;)LUM;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 207
    monitor-enter p0

    :try_start_1
    invoke-static {p5}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, LUK;->a:LUK;

    if-ne p4, v0, :cond_39

    invoke-virtual {p1}, LkM;->g()Z

    move-result v0

    if-eqz v0, :cond_39

    const/4 v0, 0x1

    .line 210
    :goto_f
    if-eqz v0, :cond_3b

    iget-object v1, p0, LUN;->a:LaaZ;

    invoke-interface {v1}, LaaZ;->b()Z

    move-result v1

    if-nez v1, :cond_3b

    .line 211
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "External storage not ready for writing pinned file:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LkM;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_36
    .catchall {:try_start_1 .. :try_end_36} :catchall_36

    .line 207
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0

    .line 209
    :cond_39
    const/4 v0, 0x0

    goto :goto_f

    .line 215
    :cond_3b
    :try_start_3b
    iget-object v1, p0, LUN;->a:LUS;

    invoke-virtual {v1, p2, v0, p5}, LUS;->a(Ljava/lang/String;ZLjava/lang/String;)LUQ;

    move-result-object v3

    .line 216
    invoke-virtual {v3}, LUQ;->b()V

    .line 217
    invoke-direct {p0, v3}, LUN;->a(LUQ;)V

    .line 219
    invoke-virtual {v3}, LUQ;->a()LkN;

    .line 221
    new-instance v0, LUO;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LUO;-><init>(LUN;LkM;LUQ;Ljava/lang/String;LUK;)V
    :try_end_53
    .catchall {:try_start_3b .. :try_end_53} :catchall_36

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized a(LkD;)Ljava/util/List;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkD;",
            ")",
            "Ljava/util/List",
            "<",
            "LUM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LUN;->a:Llf;

    invoke-interface {v0, p1}, Llf;->a(LkD;)Ljava/util/Set;

    move-result-object v0

    .line 260
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 261
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkE;

    .line 262
    iget-object v3, p0, LUN;->a:Llf;

    invoke-virtual {v0}, LkE;->a()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Llf;->a(J)LkN;

    move-result-object v0

    .line 263
    invoke-direct {p0, v0}, LUN;->a(LkN;)LUM;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2d
    .catchall {:try_start_1 .. :try_end_2d} :catchall_2e

    goto :goto_10

    .line 259
    :catchall_2e
    move-exception v0

    monitor-exit p0

    throw v0

    .line 265
    :cond_31
    monitor-exit p0

    return-object v1
.end method

.method public a()V
    .registers 3

    .prologue
    .line 339
    const-string v0, "DocumentFileManager"

    const-string v1, "GC started"

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LUN;->a(Z)V

    .line 341
    const-string v0, "DocumentFileManager"

    const-string v1, "GC completed"

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    return-void
.end method

.method a(J)V
    .registers 8
    .parameter

    .prologue
    .line 321
    const/4 v0, 0x0

    .line 322
    monitor-enter p0

    .line 323
    :try_start_2
    iget-wide v1, p0, LUN;->a:J

    sub-long/2addr v1, p1

    iput-wide v1, p0, LUN;->a:J

    .line 324
    iget-wide v1, p0, LUN;->a:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gez v1, :cond_10

    .line 325
    const/4 v0, 0x1

    .line 327
    :cond_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_2 .. :try_end_11} :catchall_17

    .line 329
    if-eqz v0, :cond_16

    .line 330
    invoke-virtual {p0}, LUN;->a()V

    .line 332
    :cond_16
    return-void

    .line 327
    :catchall_17
    move-exception v0

    :try_start_18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_18 .. :try_end_19} :catchall_17

    throw v0
.end method

.method public a(LkM;LUK;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 184
    if-eqz p2, :cond_b

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 185
    invoke-virtual {p1, p2}, LkM;->a(LUK;)Z

    move-result v0

    .line 186
    return v0

    .line 184
    :cond_b
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public b()V
    .registers 3

    .prologue
    .line 346
    const-string v0, "DocumentFileManager"

    const-string v1, "Cache cleanup started"

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LUN;->a(Z)V

    .line 348
    const-string v0, "DocumentFileManager"

    const-string v1, "Cache cleanup completed"

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    return-void
.end method

.method public b(LkM;LUK;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 191
    if-eqz p2, :cond_14

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Lagu;->a(Z)V

    .line 192
    invoke-virtual {p1, p2}, LkM;->a(LUK;)J

    move-result-wide v2

    .line 193
    iget-object v0, p0, LUN;->a:Llf;

    invoke-interface {v0, v2, v3}, Llf;->a(J)LkN;

    move-result-object v0

    .line 194
    if-nez v0, :cond_16

    .line 197
    :goto_13
    return v1

    :cond_14
    move v0, v1

    .line 191
    goto :goto_4

    .line 197
    :cond_16
    invoke-direct {p0, v0}, LUN;->a(LkN;)LUQ;

    move-result-object v0

    invoke-virtual {v0}, LUQ;->a()Z

    move-result v1

    goto :goto_13
.end method

.method public c(LkM;LUK;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    if-eqz p2, :cond_2d

    move v0, v1

    :goto_8
    invoke-static {v0}, Lagu;->a(Z)V

    .line 173
    invoke-virtual {p0, p1, p2}, LUN;->b(LkM;LUK;)Z

    move-result v3

    .line 174
    sget-object v0, LUK;->a:LUK;

    if-ne p2, v0, :cond_2f

    invoke-virtual {p1}, LkM;->g()Z

    move-result v0

    if-eqz v0, :cond_2f

    iget-object v0, p0, LUN;->a:Landroid/content/Context;

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2f

    move v0, v1

    .line 177
    :goto_22
    invoke-virtual {p0, p1, p2}, LUN;->a(LkM;LUK;)Z

    move-result v4

    .line 179
    if-eqz v3, :cond_31

    if-nez v0, :cond_2c

    if-eqz v4, :cond_31

    :cond_2c
    :goto_2c
    return v1

    :cond_2d
    move v0, v2

    .line 171
    goto :goto_8

    :cond_2f
    move v0, v2

    .line 174
    goto :goto_22

    :cond_31
    move v1, v2

    .line 179
    goto :goto_2c
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 484
    const-string v0, "DocumentFileManagerImpl[%d files]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LUN;->a:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
