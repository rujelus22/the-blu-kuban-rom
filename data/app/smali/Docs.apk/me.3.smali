.class Lme;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lmb;


# direct methods
.method constructor <init>(Lmb;)V
    .registers 2
    .parameter

    .prologue
    .line 974
    iput-object p1, p0, Lme;->a:Lmb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    .prologue
    .line 978
    :try_start_0
    iget-object v0, p0, Lme;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions;->a()Lcom/google/api/services/discussions/Discussions$Authors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$Authors;->a()Lcom/google/api/services/discussions/Discussions$Authors$Get;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$Authors$Get;->a()Lcom/google/api/services/discussions/model/Author;

    move-result-object v0

    .line 979
    new-instance v1, Lmy;

    invoke-direct {v1, v0}, Lmy;-><init>(Lcom/google/api/services/discussions/model/Author;)V

    .line 980
    iget-object v0, p0, Lme;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)LlW;

    move-result-object v0

    invoke-virtual {v0, v1}, LlW;->a(Ljava/lang/Object;)V
    :try_end_20
    .catch LadQ; {:try_start_0 .. :try_end_20} :catch_21
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_20} :catch_42

    .line 991
    :goto_20
    return-void

    .line 982
    :catch_21
    move-exception v0

    .line 983
    iget-object v1, p0, Lme;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    .line 984
    iget-object v1, p0, Lme;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LlW;

    move-result-object v1

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    .line 988
    :goto_30
    iget-object v0, p0, Lme;->a:Lmb;

    invoke-static {v0}, Lmb;->d(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 989
    :try_start_37
    iget-object v0, p0, Lme;->a:Lmb;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lmb;->a(Lmb;LlW;)LlW;

    .line 990
    monitor-exit v1

    goto :goto_20

    :catchall_3f
    move-exception v0

    monitor-exit v1
    :try_end_41
    .catchall {:try_start_37 .. :try_end_41} :catchall_3f

    throw v0

    .line 985
    :catch_42
    move-exception v0

    .line 986
    iget-object v1, p0, Lme;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LlW;

    move-result-object v1

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_30
.end method
