.class public Latx;
.super Ljava/lang/Object;


# direct methods
.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3

    instance-of v0, p0, [B

    if-eqz v0, :cond_15

    instance-of v0, p1, [B

    if-eqz v0, :cond_15

    check-cast p0, [B

    check-cast p0, [B

    check-cast p1, [B

    check-cast p1, [B

    invoke-static {p0, p1}, Latx;->a([B[B)Z

    move-result v0

    :goto_14
    return v0

    :cond_15
    instance-of v0, p0, [Ljava/lang/Object;

    if-eqz v0, :cond_2a

    instance-of v0, p1, [Ljava/lang/Object;

    if-eqz v0, :cond_2a

    check-cast p0, [Ljava/lang/Object;

    check-cast p0, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    invoke-static {p0, p1}, Latx;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_14

    :cond_2a
    instance-of v0, p0, Ljava/util/Vector;

    if-eqz v0, :cond_3b

    instance-of v0, p1, Ljava/util/Vector;

    if-eqz v0, :cond_3b

    check-cast p0, Ljava/util/Vector;

    check-cast p1, Ljava/util/Vector;

    invoke-static {p0, p1}, Latx;->a(Ljava/util/Vector;Ljava/util/Vector;)Z

    move-result v0

    goto :goto_14

    :cond_3b
    if-nez p0, :cond_43

    if-nez p1, :cond_41

    const/4 v0, 0x1

    goto :goto_14

    :cond_41
    const/4 v0, 0x0

    goto :goto_14

    :cond_43
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_14
.end method

.method public static a(Ljava/util/Vector;Ljava/util/Vector;)Z
    .registers 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_5

    :cond_4
    :goto_4
    return v0

    :cond_5
    if-eqz p0, :cond_9

    if-nez p1, :cond_b

    :cond_9
    move v0, v1

    goto :goto_4

    :cond_b
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3

    if-eq v2, v3, :cond_17

    move v0, v1

    goto :goto_4

    :cond_17
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v3

    move v2, v1

    :goto_1c
    if-ge v2, v3, :cond_4

    invoke-virtual {p0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, Latx;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2e

    move v0, v1

    goto :goto_4

    :cond_2e
    add-int/lit8 v2, v2, 0x1

    goto :goto_1c
.end method

.method public static a([B[B)Z
    .registers 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_6

    move v1, v2

    :cond_5
    :goto_5
    return v1

    :cond_6
    if-eqz p0, :cond_5

    if-eqz p1, :cond_5

    array-length v0, p0

    array-length v3, p1

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_f
    array-length v3, p0

    if-ge v0, v3, :cond_1b

    aget-byte v3, p0, v0

    aget-byte v4, p1, v0

    if-ne v3, v4, :cond_5

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_1b
    move v1, v2

    goto :goto_5
.end method

.method public static a([Ljava/lang/Object;[Ljava/lang/Object;)Z
    .registers 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_6

    move v1, v2

    :cond_5
    :goto_5
    return v1

    :cond_6
    if-eqz p0, :cond_5

    if-eqz p1, :cond_5

    array-length v0, p0

    array-length v3, p1

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_f
    array-length v3, p0

    if-ge v0, v3, :cond_1f

    aget-object v3, p0, v0

    aget-object v4, p1, v0

    invoke-static {v3, v4}, Latx;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_1f
    move v1, v2

    goto :goto_5
.end method
