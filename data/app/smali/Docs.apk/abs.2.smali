.class final Labs;
.super Ljava/lang/Object;
.source "Views.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Landroid/app/Dialog;)V
    .registers 2
    .parameter

    .prologue
    .line 121
    iput-object p1, p0, Labs;->a:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 125
    if-eqz p2, :cond_2b

    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_2b

    .line 126
    check-cast p1, Landroid/widget/EditText;

    .line 127
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 129
    if-gtz v0, :cond_2c

    .line 130
    invoke-virtual {p1}, Landroid/widget/EditText;->selectAll()V

    .line 134
    :goto_1b
    iget-object v0, p0, Labs;->a:Landroid/app/Dialog;

    if-eqz v0, :cond_2b

    .line 135
    iget-object v0, p0, Labs;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_2b

    .line 137
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 141
    :cond_2b
    return-void

    .line 132
    :cond_2c
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_1b
.end method
