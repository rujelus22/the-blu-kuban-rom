.class LNh;
.super Ljava/lang/Object;
.source "AuthTokenManagerImpl.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LNf;

.field final synthetic a:LNg;


# direct methods
.method constructor <init>(LNg;LNf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 303
    iput-object p1, p0, LNh;->a:LNg;

    iput-object p2, p0, LNh;->a:LNf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 307
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 309
    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 310
    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 311
    iget-object v1, p0, LNh;->a:LNf;

    invoke-interface {v1, v0}, LNf;->a(Landroid/content/Intent;)V
    :try_end_1b
    .catchall {:try_start_0 .. :try_end_1b} :catchall_80
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_1b} :catch_50
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_1b} :catch_60
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1b} :catch_70

    .line 327
    iget-object v0, p0, LNh;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V

    .line 329
    :goto_24
    return-void

    .line 314
    :cond_25
    :try_start_25
    const-string v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 315
    if-nez v0, :cond_41

    .line 316
    iget-object v0, p0, LNh;->a:LNf;

    new-instance v1, LNt;

    invoke-direct {v1}, LNt;-><init>()V

    invoke-interface {v0, v1}, LNf;->a(Ljava/lang/Throwable;)V
    :try_end_37
    .catchall {:try_start_25 .. :try_end_37} :catchall_80
    .catch Landroid/accounts/OperationCanceledException; {:try_start_25 .. :try_end_37} :catch_50
    .catch Landroid/accounts/AuthenticatorException; {:try_start_25 .. :try_end_37} :catch_60
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_37} :catch_70

    .line 327
    iget-object v0, p0, LNh;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V

    goto :goto_24

    .line 319
    :cond_41
    :try_start_41
    iget-object v0, p0, LNh;->a:LNf;

    invoke-interface {v0}, LNf;->a()V
    :try_end_46
    .catchall {:try_start_41 .. :try_end_46} :catchall_80
    .catch Landroid/accounts/OperationCanceledException; {:try_start_41 .. :try_end_46} :catch_50
    .catch Landroid/accounts/AuthenticatorException; {:try_start_41 .. :try_end_46} :catch_60
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_46} :catch_70

    .line 327
    iget-object v0, p0, LNh;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V

    goto :goto_24

    .line 320
    :catch_50
    move-exception v0

    .line 321
    :try_start_51
    iget-object v1, p0, LNh;->a:LNf;

    invoke-interface {v1, v0}, LNf;->a(Ljava/lang/Throwable;)V
    :try_end_56
    .catchall {:try_start_51 .. :try_end_56} :catchall_80

    .line 327
    iget-object v0, p0, LNh;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V

    goto :goto_24

    .line 322
    :catch_60
    move-exception v0

    .line 323
    :try_start_61
    iget-object v1, p0, LNh;->a:LNf;

    invoke-interface {v1, v0}, LNf;->a(Ljava/lang/Throwable;)V
    :try_end_66
    .catchall {:try_start_61 .. :try_end_66} :catchall_80

    .line 327
    iget-object v0, p0, LNh;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V

    goto :goto_24

    .line 324
    :catch_70
    move-exception v0

    .line 325
    :try_start_71
    iget-object v1, p0, LNh;->a:LNf;

    invoke-interface {v1, v0}, LNf;->a(Ljava/lang/Throwable;)V
    :try_end_76
    .catchall {:try_start_71 .. :try_end_76} :catchall_80

    .line 327
    iget-object v0, p0, LNh;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V

    goto :goto_24

    :catchall_80
    move-exception v0

    iget-object v1, p0, LNh;->a:LNg;

    invoke-static {v1}, LNg;->a(LNg;)LNb;

    move-result-object v1

    invoke-interface {v1}, LNb;->b()V

    throw v0
.end method
