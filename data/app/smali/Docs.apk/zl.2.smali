.class Lzl;
.super Landroid/text/Editable$Factory;
.source "RedrawManager.java"


# instance fields
.field final synthetic a:Lzj;

.field private a:Z


# direct methods
.method constructor <init>(Lzj;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iput-object p1, p0, Lzl;->a:Lzj;

    invoke-direct {p0}, Landroid/text/Editable$Factory;-><init>()V

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzl;->a:Z

    return-void
.end method


# virtual methods
.method public newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 181
    if-eqz p1, :cond_2d

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_2d

    .line 182
    const-string v0, "RedrawManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New editable called with \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 185
    :cond_2d
    iget-boolean v0, p0, Lzl;->a:Z

    if-nez v0, :cond_3e

    move v0, v1

    :goto_32
    invoke-static {v0}, Lagu;->b(Z)V

    .line 186
    iput-boolean v1, p0, Lzl;->a:Z

    .line 187
    iget-object v0, p0, Lzl;->a:Lzj;

    invoke-static {v0}, Lzj;->a(Lzj;)LDp;

    move-result-object v0

    return-object v0

    .line 185
    :cond_3e
    const/4 v0, 0x0

    goto :goto_32
.end method
