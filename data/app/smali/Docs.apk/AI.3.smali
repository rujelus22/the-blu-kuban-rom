.class public LAI;
.super LDA;
.source "TableElement.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LDA",
        "<",
        "LAx;",
        "LzU;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/kix/elements/TableElement;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)V
    .registers 2
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, LAI;->a:Lcom/google/android/apps/docs/editors/kix/elements/TableElement;

    invoke-direct {p0}, LDA;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/Object;)C
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 115
    sget-object v0, LDv;->a:Ljava/lang/Object;

    if-ne p2, v0, :cond_19

    .line 116
    iget-object v0, p0, LAI;->a:Lcom/google/android/apps/docs/editors/kix/elements/TableElement;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)LDI;

    move-result-object v0

    invoke-virtual {v0}, LDI;->a()I

    move-result v0

    if-ge p1, v0, :cond_14

    .line 117
    const v0, 0xfffc

    .line 122
    :goto_13
    return v0

    .line 119
    :cond_14
    invoke-super {p0, p1}, LDA;->charAt(I)C

    move-result v0

    goto :goto_13

    .line 122
    :cond_19
    invoke-super {p0, p1, p2}, LDA;->a(ILjava/lang/Object;)C

    move-result v0

    goto :goto_13
.end method

.method public a(IILjava/lang/Class;Ljava/lang/Object;)I
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    sget-object v0, LDv;->a:Ljava/lang/Object;

    if-ne p4, v0, :cond_9

    .line 140
    invoke-super {p0, p1, p2, p3}, LDA;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v0

    .line 142
    :goto_8
    return v0

    :cond_9
    invoke-super {p0, p1, p2, p3, p4}, LDA;->a(IILjava/lang/Class;Ljava/lang/Object;)I

    move-result v0

    goto :goto_8
.end method

.method public a(II[CILjava/lang/Object;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    sget-object v0, LDv;->a:Ljava/lang/Object;

    if-ne p5, v0, :cond_30

    .line 96
    iget-object v0, p0, LAI;->a:Lcom/google/android/apps/docs/editors/kix/elements/TableElement;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)LDI;

    move-result-object v0

    invoke-virtual {v0}, LDI;->a()I

    move-result v0

    .line 97
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 98
    const/4 v1, 0x0

    sub-int/2addr v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 99
    if-lez v0, :cond_22

    .line 100
    add-int v1, p4, v0

    const v2, 0xfffc

    invoke-static {p3, p4, v1, v2}, Ljava/util/Arrays;->fill([CIIC)V

    .line 103
    :cond_22
    add-int v1, p1, v0

    .line 104
    if-ge v1, p2, :cond_2f

    .line 105
    add-int v4, p4, v0

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-super/range {v0 .. v5}, LDA;->a(II[CILjava/lang/Object;)V

    .line 111
    :cond_2f
    :goto_2f
    return-void

    .line 109
    :cond_30
    invoke-super/range {p0 .. p5}, LDA;->a(II[CILjava/lang/Object;)V

    goto :goto_2f
.end method

.method public a(Ljava/util/List;IILjava/lang/Class;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;II",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 129
    sget-object v0, LDv;->a:Ljava/lang/Object;

    if-ne p5, v0, :cond_8

    .line 130
    invoke-super {p0, p1, p2, p3, p4}, LDA;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 134
    :goto_7
    return-void

    .line 133
    :cond_8
    invoke-super/range {p0 .. p5}, LDA;->a(Ljava/util/List;IILjava/lang/Class;Ljava/lang/Object;)V

    goto :goto_7
.end method
