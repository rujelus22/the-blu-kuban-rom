.class public LTb;
.super LTd;
.source "UploadDialogFragment.java"


# instance fields
.field private final a:I

.field private final a:LTa;

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LTa;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 123
    sget v0, Len;->upload_folder:I

    invoke-direct {p0, v0}, LTd;-><init>(I)V

    .line 124
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTa;

    iput-object v0, p0, LTb;->a:LTa;

    .line 125
    iput p2, p0, LTb;->a:I

    .line 126
    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, LTb;->b:Ljava/lang/String;

    return-object v0
.end method

.method a(Landroid/support/v4/app/FragmentActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, LTb;->a:LTa;

    invoke-virtual {v0}, LTa;->b()Ljava/lang/String;

    move-result-object v0

    .line 136
    invoke-static {p1, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a(Landroid/content/Context;Ljava/lang/String;)LhX;

    move-result-object v0

    iget-object v1, p0, LTb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LhX;->a(Ljava/lang/String;)LhX;

    move-result-object v0

    sget-object v1, LkP;->h:LkP;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LhX;->a(Ljava/util/EnumSet;)LhX;

    move-result-object v0

    invoke-virtual {v0}, LhX;->a()Landroid/content/Intent;

    move-result-object v0

    .line 140
    iget v1, p0, LTb;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 141
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LTb;->a:Ljava/lang/String;

    .line 145
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LTb;->b:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 149
    iget-object v0, p0, LTb;->a:Ljava/lang/String;

    return-object v0
.end method
