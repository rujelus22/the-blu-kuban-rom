.class public LKj;
.super Ljava/lang/Object;
.source "Range.java"


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method public constructor <init>(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, LKj;->a:I

    .line 23
    iput p2, p0, LKj;->b:I

    .line 24
    return-void
.end method

.method static synthetic a(LKj;)I
    .registers 2
    .parameter

    .prologue
    .line 16
    iget v0, p0, LKj;->a:I

    return v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 56
    const/4 v0, 0x0

    .line 57
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    .line 58
    if-nez v2, :cond_1b

    :goto_19
    move-object v2, v0

    .line 66
    goto :goto_b

    .line 60
    :cond_1b
    invoke-virtual {v2}, LKj;->b()I

    move-result v1

    invoke-virtual {v0}, LKj;->a()I

    move-result v5

    if-ne v1, v5, :cond_34

    .line 62
    new-instance v1, LKj;

    invoke-virtual {v2}, LKj;->a()I

    move-result v2

    invoke-virtual {v0}, LKj;->b()I

    move-result v0

    invoke-direct {v1, v2, v0}, LKj;-><init>(II)V

    move-object v0, v1

    goto :goto_19

    .line 65
    :cond_34
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 69
    :cond_38
    if-eqz v2, :cond_3d

    .line 70
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_3d
    return-object v3
.end method

.method public static a(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, LKk;

    invoke-direct {v0}, LKk;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 93
    return-void
.end method

.method static synthetic b(LKj;)I
    .registers 2
    .parameter

    .prologue
    .line 16
    iget v0, p0, LKj;->b:I

    return v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 28
    iget v0, p0, LKj;->a:I

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 33
    iget v0, p0, LKj;->b:I

    return v0
.end method

.method public c()I
    .registers 3

    .prologue
    .line 38
    iget v0, p0, LKj;->b:I

    iget v1, p0, LKj;->a:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 43
    instance-of v1, p1, LKj;

    if-eqz v1, :cond_14

    .line 44
    check-cast p1, LKj;

    .line 45
    iget v1, p1, LKj;->a:I

    iget v2, p0, LKj;->a:I

    if-ne v1, v2, :cond_14

    iget v1, p1, LKj;->b:I

    iget v2, p0, LKj;->b:I

    if-ne v1, v2, :cond_14

    const/4 v0, 0x1

    .line 47
    :cond_14
    return v0
.end method
