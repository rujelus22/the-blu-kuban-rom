.class LRo;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"

# interfaces
.implements LRj;


# instance fields
.field private a:F

.field private a:I

.field private final a:LRl;

.field final synthetic a:LRm;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LRp;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LRm;LRl;Landroid/content/Context;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 403
    iput-object p1, p0, LRo;->a:LRm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385
    iput v1, p0, LRo;->a:I

    .line 388
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LRo;->a:Landroid/os/Handler;

    .line 391
    const/4 v0, 0x0

    iput v0, p0, LRo;->a:F

    .line 393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LRo;->a:Ljava/util/List;

    .line 399
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LRo;->a:Ljava/util/SortedMap;

    .line 404
    iput-object p2, p0, LRo;->a:LRl;

    .line 405
    iput-object p3, p0, LRo;->a:Landroid/content/Context;

    move v0, v1

    .line 407
    :goto_25
    const/4 v2, 0x6

    if-ge v0, v2, :cond_46

    .line 408
    new-instance v2, Landroid/webkit/WebView;

    invoke-direct {v2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 409
    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    .line 410
    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 411
    iget-object v3, p0, LRo;->a:Ljava/util/List;

    new-instance v4, LRp;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v2, v0, v5}, LRp;-><init>(LRo;Landroid/webkit/WebView;ILRn;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 413
    :cond_46
    return-void
.end method

.method synthetic constructor <init>(LRm;LRl;Landroid/content/Context;LRn;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1, p2, p3}, LRo;-><init>(LRm;LRl;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(LRo;)F
    .registers 2
    .parameter

    .prologue
    .line 104
    iget v0, p0, LRo;->a:F

    return v0
.end method

.method private a()I
    .registers 2

    .prologue
    .line 455
    const/16 v0, 0xc8

    return v0
.end method

.method static synthetic a(LRo;)I
    .registers 2
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, LRo;->a()I

    move-result v0

    return v0
.end method

.method static synthetic a(LRo;)LRl;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LRo;->a:LRl;

    return-object v0
.end method

.method static synthetic a(LRo;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LRo;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LRo;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LRo;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(LRo;)Ljava/util/SortedMap;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LRo;->a:Ljava/util/SortedMap;

    return-object v0
.end method

.method private b()I
    .registers 5

    .prologue
    .line 459
    const/high16 v0, 0x4348

    iget v1, p0, LRo;->a:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 460
    return v0
.end method

.method static synthetic b(LRo;)I
    .registers 2
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, LRo;->b()I

    move-result v0

    return v0
.end method

.method static synthetic c(LRo;)I
    .registers 2
    .parameter

    .prologue
    .line 104
    iget v0, p0, LRo;->a:I

    return v0
.end method


# virtual methods
.method public a(I)Landroid/graphics/Bitmap;
    .registers 6
    .parameter

    .prologue
    .line 417
    iget-object v0, p0, LRo;->a:LRm;

    invoke-static {v0}, LRm;->a(LRm;)LQR;

    move-result-object v0

    invoke-interface {v0}, LQR;->c()I

    move-result v0

    invoke-static {p1, v0}, Lagu;->a(II)I

    .line 419
    iget-object v0, p0, LRo;->a:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 420
    const-string v1, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Getting thumbnail: slideIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bitmap="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 431
    iget-object v0, p0, LRo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRp;

    .line 432
    invoke-static {v0}, LRp;->b(LRp;)V

    goto :goto_6

    .line 434
    :cond_16
    return-void
.end method

.method public a(F)V
    .registers 4
    .parameter

    .prologue
    .line 438
    iput p1, p0, LRo;->a:F

    .line 439
    iget-object v0, p0, LRo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRp;

    .line 440
    invoke-static {v0}, LRp;->c(LRp;)V

    goto :goto_8

    .line 442
    :cond_18
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 426
    iput p1, p0, LRo;->a:I

    .line 427
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 446
    iget-object v0, p0, LRo;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 447
    if-eqz v0, :cond_a

    .line 448
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_a

    .line 451
    :cond_1c
    iget-object v0, p0, LRo;->a:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    .line 452
    return-void
.end method
