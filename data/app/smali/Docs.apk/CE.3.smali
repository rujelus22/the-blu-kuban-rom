.class LCE;
.super Landroid/graphics/drawable/shapes/Shape;
.source "NativeBaseRenderer.java"


# instance fields
.field private a:I

.field private a:Landroid/graphics/Rect;

.field private final a:Landroid/graphics/RectF;

.field private b:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/Shape;-><init>()V

    .line 30
    iput v0, p0, LCE;->a:I

    .line 31
    iput v0, p0, LCE;->b:I

    .line 32
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LCE;->a:Landroid/graphics/RectF;

    .line 35
    return-void
.end method


# virtual methods
.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 76
    iput p1, p0, LCE;->b:I

    .line 77
    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .registers 2
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, LCE;->a:Landroid/graphics/Rect;

    .line 85
    return-void
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 80
    iput p1, p0, LCE;->a:I

    .line 81
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/high16 v7, 0x4000

    const/4 v2, 0x0

    .line 39
    iget v0, p0, LCE;->b:I

    if-eqz v0, :cond_16

    .line 40
    iget v0, p0, LCE;->b:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 42
    iget-object v0, p0, LCE;->a:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 44
    :cond_16
    iget v0, p0, LCE;->a:I

    if-eqz v0, :cond_aa

    .line 45
    iget v0, p0, LCE;->a:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 47
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-lez v0, :cond_43

    .line 48
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 49
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    div-float v1, v0, v7

    .line 50
    invoke-virtual {p0}, LCE;->getHeight()F

    move-result v4

    move-object v0, p1

    move v3, v1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 52
    :cond_43
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-lez v0, :cond_67

    .line 53
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 54
    invoke-virtual {p0}, LCE;->getWidth()F

    move-result v0

    iget-object v1, p0, LCE;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    div-float/2addr v1, v7

    sub-float v1, v0, v1

    .line 55
    invoke-virtual {p0}, LCE;->getHeight()F

    move-result v4

    move-object v0, p1

    move v3, v1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 57
    :cond_67
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_86

    .line 58
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 59
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    div-float v3, v0, v7

    .line 60
    invoke-virtual {p0}, LCE;->getWidth()F

    move-result v4

    move-object v1, p1

    move v5, v3

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 62
    :cond_86
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-lez v0, :cond_aa

    .line 63
    iget-object v0, p0, LCE;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 64
    invoke-virtual {p0}, LCE;->getHeight()F

    move-result v0

    iget-object v1, p0, LCE;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    div-float/2addr v1, v7

    sub-float v3, v0, v1

    .line 65
    invoke-virtual {p0}, LCE;->getWidth()F

    move-result v4

    move-object v1, p1

    move v5, v3

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 68
    :cond_aa
    return-void
.end method

.method protected onResize(FF)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, LCE;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 73
    return-void
.end method
