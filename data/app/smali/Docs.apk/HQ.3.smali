.class public final LHQ;
.super Ljava/lang/Object;
.source "CellRendererUtil.java"


# direct methods
.method public static a(LIT;)I
    .registers 5
    .parameter

    .prologue
    .line 84
    invoke-virtual {p0}, LIT;->d()S

    move-result v0

    invoke-virtual {p0}, LIT;->a()S

    move-result v1

    invoke-virtual {p0}, LIT;->b()S

    move-result v2

    invoke-virtual {p0}, LIT;->c()S

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;LIU;LJc;ZLIT;D)LEW;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v6, 0x3f80

    .line 61
    const/high16 v0, -0x100

    .line 64
    if-eqz p4, :cond_a

    .line 65
    invoke-static {p4}, LHQ;->a(LIT;)I

    move-result v0

    .line 69
    :cond_a
    invoke-virtual {p1}, LIU;->a()F

    move-result v1

    .line 71
    double-to-float v2, p5

    const/high16 v3, 0x4080

    sub-float/2addr v2, v3

    .line 73
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    .line 74
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 75
    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 76
    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 77
    invoke-virtual {v3, p3}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 78
    invoke-static {p1}, LHQ;->a(LIU;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 79
    new-instance v0, LEW;

    new-instance v1, LDV;

    invoke-direct {v1}, LDV;-><init>()V

    float-to-int v4, v2

    invoke-static {p2}, LHQ;->a(LJc;)Landroid/text/Layout$Alignment;

    move-result-object v5

    const/4 v8, 0x0

    move-object v2, p0

    move v7, v6

    invoke-direct/range {v0 .. v8}, LEW;-><init>(LFg;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0
.end method

.method public static a(LIS;)LHP;
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 36
    invoke-virtual {p0}, LIS;->a()LJI;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, LJI;->a()D

    move-result-wide v1

    double-to-float v1, v1

    .line 38
    invoke-virtual {v0}, LJI;->b()D

    move-result-wide v0

    double-to-float v0, v0

    .line 40
    new-instance v8, LHP;

    invoke-direct {v8}, LHP;-><init>()V

    .line 41
    invoke-virtual {p0}, LIS;->a()LIW;

    move-result-object v4

    .line 42
    invoke-virtual {p0}, LIS;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, LIW;->a()LIU;

    move-result-object v1

    invoke-virtual {v4}, LIW;->a()LJc;

    move-result-object v2

    invoke-virtual {v4}, LIW;->a()S

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_4b

    const/4 v3, 0x1

    :goto_2d
    invoke-virtual {v4}, LIW;->a()LIT;

    move-result-object v4

    invoke-virtual {p0}, LIS;->a()LJI;

    move-result-object v5

    invoke-virtual {v5}, LJI;->a()D

    move-result-wide v5

    invoke-static/range {v0 .. v6}, LHQ;->a(Ljava/lang/String;LIU;LJc;ZLIT;D)LEW;

    move-result-object v0

    .line 47
    const v1, 0x7fffffff

    invoke-virtual {v0, v8, v7, v1}, LEW;->a(LFf;II)V

    .line 48
    invoke-virtual {v0}, LEW;->c()I

    move-result v0

    invoke-virtual {v8, v0}, LHP;->b(I)V

    .line 49
    return-object v8

    :cond_4b
    move v3, v7

    .line 42
    goto :goto_2d
.end method

.method public static a(LIU;)Landroid/graphics/Typeface;
    .registers 4
    .parameter

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 100
    invoke-virtual {p0}, LIU;->b()LJC;

    move-result-object v1

    sget-object v2, LJC;->b:LJC;

    if-ne v1, v2, :cond_a

    .line 101
    const/4 v0, 0x2

    .line 103
    :cond_a
    invoke-virtual {p0}, LIU;->a()LJC;

    move-result-object v1

    sget-object v2, LJC;->b:LJC;

    if-ne v1, v2, :cond_14

    .line 104
    or-int/lit8 v0, v0, 0x1

    .line 107
    :cond_14
    invoke-virtual {p0}, LIU;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method private static a(LJc;)Landroid/text/Layout$Alignment;
    .registers 2
    .parameter

    .prologue
    .line 89
    sget-object v0, LJc;->a:LJc;

    if-ne p0, v0, :cond_7

    .line 90
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 94
    :goto_6
    return-object v0

    .line 91
    :cond_7
    sget-object v0, LJc;->c:LJc;

    if-ne p0, v0, :cond_e

    .line 92
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_6

    .line 94
    :cond_e
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_6
.end method
