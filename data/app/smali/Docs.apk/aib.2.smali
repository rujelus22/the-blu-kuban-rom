.class public abstract enum Laib;
.super Ljava/lang/Enum;
.source "RemovalCause.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laib;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laib;

.field private static final synthetic a:[Laib;

.field public static final enum b:Laib;

.field public static final enum c:Laib;

.field public static final enum d:Laib;

.field public static final enum e:Laib;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Laic;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Laic;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laib;->a:Laib;

    .line 51
    new-instance v0, Laid;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Laid;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laib;->b:Laib;

    .line 63
    new-instance v0, Laie;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Laie;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laib;->c:Laib;

    .line 74
    new-instance v0, Laif;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Laif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laib;->d:Laib;

    .line 85
    new-instance v0, Laig;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Laig;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laib;->e:Laib;

    .line 31
    const/4 v0, 0x5

    new-array v0, v0, [Laib;

    sget-object v1, Laib;->a:Laib;

    aput-object v1, v0, v2

    sget-object v1, Laib;->b:Laib;

    aput-object v1, v0, v3

    sget-object v1, Laib;->c:Laib;

    aput-object v1, v0, v4

    sget-object v1, Laib;->d:Laib;

    aput-object v1, v0, v5

    sget-object v1, Laib;->e:Laib;

    aput-object v1, v0, v6

    sput-object v0, Laib;->a:[Laib;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILaic;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Laib;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laib;
    .registers 2
    .parameter

    .prologue
    .line 31
    const-class v0, Laib;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laib;

    return-object v0
.end method

.method public static values()[Laib;
    .registers 1

    .prologue
    .line 31
    sget-object v0, Laib;->a:[Laib;

    invoke-virtual {v0}, [Laib;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laib;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
