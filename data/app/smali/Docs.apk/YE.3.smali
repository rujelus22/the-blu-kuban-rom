.class public final LYE;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public i:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public k:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public l:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public n:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljavax/crypto/KeyGenerator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 44
    iput-object p1, p0, LYE;->a:LYD;

    .line 45
    new-instance v0, LYF;

    invoke-direct {v0, p0}, LYF;-><init>(LYE;)V

    const-string v1, "DocListActivity"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(LaoL;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->a:LZb;

    .line 48
    const-class v0, Ljava/lang/String;

    const-string v1, "DiscussionTrackerLabel"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->b:LZb;

    .line 51
    new-instance v0, LYG;

    invoke-direct {v0, p0}, LYG;-><init>(LYE;)V

    const-string v1, "StartingActivityOnLaunch"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(LaoL;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->c:LZb;

    .line 54
    const-class v0, Ljava/lang/String;

    const-string v1, "versionFlag"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->d:LZb;

    .line 57
    const-class v0, Ljava/lang/String;

    const-class v1, LeT;

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->e:LZb;

    .line 60
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldMessage"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->f:LZb;

    .line 63
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldClose"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->g:LZb;

    .line 66
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldTitle"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->h:LZb;

    .line 69
    const-class v0, Ljava/lang/String;

    const-string v1, "marketFlag"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->i:LZb;

    .line 72
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldUpgrade"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->j:LZb;

    .line 75
    const-class v0, Ljava/lang/Integer;

    const-string v1, "punchThumbnailCacheSize"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->k:LZb;

    .line 78
    const-class v0, Ljava/lang/String;

    const-class v1, LPM;

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->l:LZb;

    .line 81
    const-class v0, Ljava/util/concurrent/Executor;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->m:LZb;

    .line 84
    const-class v0, Ljavax/crypto/KeyGenerator;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYE;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYE;->n:LZb;

    .line 87
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 94
    new-instance v0, LYH;

    invoke-direct {v0, p0}, LYH;-><init>(LYE;)V

    const-string v1, "DocListActivity"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(LaoL;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->a:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 95
    const-class v0, Ljava/lang/String;

    const-string v1, "DiscussionTrackerLabel"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->b:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 96
    new-instance v0, LYI;

    invoke-direct {v0, p0}, LYI;-><init>(LYE;)V

    const-string v1, "StartingActivityOnLaunch"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(LaoL;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->c:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 97
    const-class v0, Ljava/lang/String;

    const-string v1, "versionFlag"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->d:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 98
    const-class v0, Ljava/lang/String;

    const-class v1, LeT;

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->e:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 99
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldMessage"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->f:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 100
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldClose"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->g:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 101
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldTitle"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->h:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 102
    const-class v0, Ljava/lang/String;

    const-string v1, "marketFlag"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->i:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 103
    const-class v0, Ljava/lang/Integer;

    const-string v1, "tooOldUpgrade"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->j:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 104
    const-class v0, Ljava/lang/Integer;

    const-string v1, "punchThumbnailCacheSize"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->k:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 105
    const-class v0, Ljava/lang/String;

    const-class v1, LPM;

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->l:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 106
    const-class v0, Ljava/util/concurrent/Executor;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->m:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 107
    const-class v0, Ljavax/crypto/KeyGenerator;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LYE;->n:LZb;

    invoke-virtual {p0, v0, v1}, LYE;->a(Laop;LZb;)V

    .line 108
    iget-object v0, p0, LYE;->a:LZb;

    iget-object v1, p0, LYE;->a:LYD;

    iget-object v1, v1, LYD;->a:LdR;

    iget-object v1, v1, LdR;->c:LZb;

    invoke-static {v1}, LYE;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 111
    iget-object v0, p0, LYE;->b:LZb;

    iget-object v1, p0, LYE;->a:LYD;

    iget-object v1, v1, LYD;->a:Lms;

    iget-object v1, v1, Lms;->a:LZb;

    invoke-static {v1}, LYE;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 114
    iget-object v0, p0, LYE;->c:LZb;

    iget-object v1, p0, LYE;->a:LYD;

    iget-object v1, v1, LYD;->a:LdR;

    iget-object v1, v1, LdR;->d:LZb;

    invoke-static {v1}, LYE;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 117
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 121
    return-void
.end method
