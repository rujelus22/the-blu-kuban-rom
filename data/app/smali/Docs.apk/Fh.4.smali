.class public LFh;
.super Ljava/lang/Object;
.source "TextScaler.java"


# instance fields
.field public a:F

.field private final a:LFd;

.field private a:LFi;

.field public final a:Landroid/graphics/Paint$FontMetricsInt;

.field public final a:Landroid/graphics/Paint;

.field public b:F

.field private c:F


# direct methods
.method public constructor <init>(LFd;Landroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;FF)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, LFh;->a:LFi;

    .line 55
    const/high16 v0, 0x3f80

    iput v0, p0, LFh;->c:F

    .line 120
    iput-object p1, p0, LFh;->a:LFd;

    .line 121
    iput-object p2, p0, LFh;->a:Landroid/graphics/Paint;

    .line 122
    iput-object p3, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    .line 123
    iput p4, p0, LFh;->a:F

    .line 124
    iput p5, p0, LFh;->b:F

    .line 125
    return-void
.end method

.method private a(LFf;F)F
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint;

    iget-object v1, p0, LFh;->a:LFd;

    invoke-static {v0, p2, v1}, LFh;->a(Landroid/graphics/Paint;FLFd;)F

    move-result v0

    .line 179
    iget-object v1, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    if-eqz v1, :cond_11

    .line 180
    iget-object v1, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    invoke-static {v1, p2}, LFh;->a(Landroid/graphics/Paint$FontMetricsInt;F)V

    .line 183
    :cond_11
    iget v1, p0, LFh;->a:F

    mul-float/2addr v1, p2

    iput v1, p0, LFh;->a:F

    .line 184
    iget v1, p0, LFh;->b:F

    mul-float/2addr v1, p2

    iput v1, p0, LFh;->b:F

    .line 186
    return v0
.end method

.method private static a(Landroid/graphics/Paint;FLFd;)F
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x40a0

    .line 74
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    mul-float v1, v0, p1

    .line 75
    div-float v0, v1, v2

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    mul-float/2addr v0, v2

    .line 78
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    sub-float v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v2, v1

    if-gez v1, :cond_26

    .line 80
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    .line 83
    :cond_26
    invoke-interface {p2, p0}, LFd;->a(Landroid/graphics/Paint;)F

    move-result v1

    .line 84
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_38

    .line 85
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 86
    invoke-interface {p2, p0}, LFd;->a(Landroid/graphics/Paint;)F

    move-result v0

    .line 87
    div-float/2addr v0, v1

    .line 90
    :goto_37
    return v0

    :cond_38
    const/high16 v0, 0x3f80

    goto :goto_37
.end method

.method private a()V
    .registers 3

    .prologue
    .line 193
    iget-object v0, p0, LFh;->a:LFi;

    if-eqz v0, :cond_b

    .line 194
    const-string v0, "TextScaler"

    const-string v1, "Saving state when a saved state already exists."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_b
    new-instance v0, LFi;

    invoke-direct {v0, p0}, LFi;-><init>(LFh;)V

    iput-object v0, p0, LFh;->a:LFi;

    .line 197
    return-void
.end method

.method private static a(Landroid/graphics/Paint$FontMetricsInt;F)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 101
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 102
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 103
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 104
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    .line 105
    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 106
    return-void
.end method


# virtual methods
.method public a(LFf;)V
    .registers 6
    .parameter

    .prologue
    const/high16 v2, 0x3f80

    .line 132
    iget v0, p0, LFh;->c:F

    div-float v0, v2, v0

    iget v1, p0, LFh;->c:F

    div-float v1, v2, v1

    iget v2, p0, LFh;->a:F

    iget v3, p0, LFh;->b:F

    invoke-interface {p1, v0, v1, v2, v3}, LFf;->a(FFFF)V

    .line 134
    iget-object v0, p0, LFh;->a:LFi;

    if-nez v0, :cond_1d

    .line 135
    const-string v0, "TextScaler"

    const-string v1, "Cannot restore a non-existent TextScaler state."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :goto_1c
    return-void

    .line 139
    :cond_1d
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint;

    iget-object v1, p0, LFh;->a:LFi;

    iget v1, v1, LFi;->a:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 141
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    if-eqz v0, :cond_62

    iget-object v0, p0, LFh;->a:LFi;

    iget-object v0, v0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    if-eqz v0, :cond_62

    .line 142
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p0, LFh;->a:LFi;

    iget-object v1, v1, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 143
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p0, LFh;->a:LFi;

    iget-object v1, v1, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 144
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p0, LFh;->a:LFi;

    iget-object v1, v1, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 145
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p0, LFh;->a:LFi;

    iget-object v1, v1, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    .line 146
    iget-object v0, p0, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p0, LFh;->a:LFi;

    iget-object v1, v1, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 149
    :cond_62
    iget-object v0, p0, LFh;->a:LFi;

    iget v0, v0, LFi;->b:F

    iput v0, p0, LFh;->a:F

    .line 150
    iget-object v0, p0, LFh;->a:LFi;

    iget v0, v0, LFi;->c:F

    iput v0, p0, LFh;->b:F

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, LFh;->a:LFi;

    goto :goto_1c
.end method

.method public a(LFf;F)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 162
    invoke-direct {p0}, LFh;->a()V

    .line 164
    invoke-direct {p0, p1, p2}, LFh;->a(LFf;F)F

    move-result v0

    .line 167
    div-float v0, p2, v0

    iput v0, p0, LFh;->c:F

    .line 168
    iget v0, p0, LFh;->c:F

    iget v1, p0, LFh;->c:F

    iget v2, p0, LFh;->a:F

    iget v3, p0, LFh;->b:F

    invoke-interface {p1, v0, v1, v2, v3}, LFf;->a(FFFF)V

    .line 169
    return-void
.end method
