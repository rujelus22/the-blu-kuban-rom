.class Lmy;
.super Ljava/lang/Object;
.source "StreamAuthorImpl.java"

# interfaces
.implements Lmx;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/model/Author;)V
    .registers 3
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmy;->a:Z

    .line 38
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Author;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmy;->a:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Author;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmy;->b:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Author;->a()Lcom/google/api/services/discussions/model/Author$Image;

    move-result-object v0

    if-eqz v0, :cond_34

    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Author;->a()Lcom/google/api/services/discussions/model/Author$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Author$Image;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 42
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Author;->a()Lcom/google/api/services/discussions/model/Author$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Author$Image;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lmy;->a:Landroid/net/Uri;

    .line 46
    :goto_33
    return-void

    .line 44
    :cond_34
    const/4 v0, 0x0

    iput-object v0, p0, Lmy;->a:Landroid/net/Uri;

    goto :goto_33
.end method

.method constructor <init>(Lcom/google/api/services/discussions/model/Discussion$Actor;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    if-nez p1, :cond_3a

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lmy;->a:Z

    .line 25
    iget-boolean v0, p0, Lmy;->a:Z

    if-eqz v0, :cond_3c

    move-object v0, v1

    :goto_e
    iput-object v0, p0, Lmy;->a:Ljava/lang/String;

    .line 26
    iget-boolean v0, p0, Lmy;->a:Z

    if-eqz v0, :cond_41

    move-object v0, v1

    :goto_15
    iput-object v0, p0, Lmy;->b:Ljava/lang/String;

    .line 28
    iget-boolean v0, p0, Lmy;->a:Z

    if-nez v0, :cond_46

    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion$Actor;->a()Lcom/google/api/services/discussions/model/Discussion$Actor$Image;

    move-result-object v0

    if-eqz v0, :cond_46

    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion$Actor;->a()Lcom/google/api/services/discussions/model/Discussion$Actor$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$Actor$Image;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_46

    .line 29
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion$Actor;->a()Lcom/google/api/services/discussions/model/Discussion$Actor$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$Actor$Image;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lmy;->a:Landroid/net/Uri;

    .line 33
    :goto_39
    return-void

    .line 24
    :cond_3a
    const/4 v0, 0x0

    goto :goto_7

    .line 25
    :cond_3c
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion$Actor;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 26
    :cond_41
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion$Actor;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    .line 31
    :cond_46
    iput-object v1, p0, Lmy;->a:Landroid/net/Uri;

    goto :goto_39
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Lmy;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lmy;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 65
    iget-boolean v0, p0, Lmy;->a:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, Lmy;->b:Ljava/lang/String;

    return-object v0
.end method
