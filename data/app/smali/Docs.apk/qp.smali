.class abstract Lqp;
.super LqH;
.source "AbstractDriveAppOpenerOption.java"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final a:Ljava/lang/String;

.field protected final a:Lqr;


# direct methods
.method constructor <init>(LqJ;Landroid/content/Context;Lqr;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, LqH;-><init>(LqJ;)V

    .line 32
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lqp;->a:Landroid/content/Context;

    .line 33
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqr;

    iput-object v0, p0, Lqp;->a:Lqr;

    .line 34
    invoke-static {p4}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lqp;->a:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Lpa;LkM;Landroid/os/Bundle;)LamQ;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpa;",
            "LkM;",
            "Landroid/os/Bundle;",
            ")",
            "LamQ",
            "<",
            "Lnm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p2}, LkM;->i()Ljava/lang/String;

    .line 43
    invoke-virtual {p2}, LkM;->c()Ljava/lang/String;

    move-result-object v0

    .line 44
    new-instance v1, Lqq;

    invoke-direct {v1, p0, v0, p1, p2}, Lqq;-><init>(Lqp;Ljava/lang/String;Lpa;LkM;)V

    .line 77
    invoke-static {v1}, LamF;->a(Ljava/lang/Object;)LamQ;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(LkM;Landroid/net/Uri;)Landroid/content/Intent;
.end method

.method final a()Z
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method
