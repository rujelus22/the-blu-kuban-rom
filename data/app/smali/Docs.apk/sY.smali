.class public LsY;
.super Ljava/lang/Object;
.source "DiscussionFragmentUtils.java"


# static fields
.field private static a:I

.field private static final a:Landroid/text/SpannableStringBuilder;

.field private static a:Landroid/view/View$OnClickListener;

.field private static a:Landroid/view/View$OnLongClickListener;

.field private static a:Landroid/widget/ImageView;

.field private static a:Landroid/widget/TextView;

.field private static b:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 36
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    sput-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/widget/AdapterView$OnItemClickListener;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter

    .prologue
    .line 53
    new-instance v0, LsZ;

    invoke-direct {v0, p0}, LsZ;-><init>(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v0
.end method

.method public static a(Landroid/widget/AdapterView$OnItemClickListener;)Landroid/view/View$OnLongClickListener;
    .registers 2
    .parameter

    .prologue
    .line 73
    new-instance v0, Lta;

    invoke-direct {v0, p0}, Lta;-><init>(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;LAQ;Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lmz;ZI)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    sput-object p3, LsY;->a:Landroid/view/View$OnClickListener;

    .line 88
    sput-object p4, LsY;->a:Landroid/view/View$OnLongClickListener;

    .line 89
    sput p7, LsY;->a:I

    .line 91
    sget v0, LsD;->contact_picture:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sput-object v0, LsY;->a:Landroid/widget/ImageView;

    .line 92
    sget v0, LsD;->comment_author_date:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, LsY;->a:Landroid/widget/TextView;

    .line 93
    sget v0, LsD;->comment_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, LsY;->b:Landroid/widget/TextView;

    .line 95
    sget v0, LsD;->adapter_position_tag:I

    sget v1, LsY;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 96
    sget-object v0, LsY;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    sget-object v0, LsY;->a:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 99
    invoke-static {p0, p1, p5}, LsY;->a(Landroid/content/Context;LAQ;Lmz;)V

    .line 100
    invoke-static {p0, p5, p6}, LsY;->a(Landroid/content/Context;Lmz;Z)V

    .line 101
    return-void
.end method

.method private static a(Landroid/content/Context;LAQ;Lmz;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x21

    const/16 v1, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 105
    invoke-interface {p2}, Lmz;->a()Lmx;

    move-result-object v0

    invoke-interface {v0}, Lmx;->a()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 106
    sget-object v0, LsY;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    :goto_1a
    return-void

    .line 110
    :cond_1b
    sget-object v0, LsY;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 111
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    invoke-interface {p2}, Lmz;->a()Lmx;

    move-result-object v0

    invoke-interface {v0}, Lmx;->a()Landroid/net/Uri;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_48

    if-eqz p1, :cond_48

    .line 116
    sget-object v1, LsY;->a:Landroid/widget/ImageView;

    invoke-virtual {p1, v1, v0}, LAQ;->a(Landroid/widget/ImageView;Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_48

    .line 117
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsC;->contact_android:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 119
    sget-object v1, LsY;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    :cond_48
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 127
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-interface {p2}, Lmz;->a()Lmx;

    move-result-object v1

    invoke-interface {v1}, Lmx;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 128
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    sget-object v2, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 132
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 133
    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lmz;->a()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 135
    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const v3, -0x777778

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sget-object v3, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 139
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 142
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    sget v1, LsD;->adapter_position_tag:I

    sget v2, LsY;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 143
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    sget-object v1, LsY;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    sget-object v1, LsY;->a:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 145
    sget-object v0, LsY;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    goto/16 :goto_1a
.end method

.method private static a(Landroid/content/Context;Lmz;Z)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0xc8

    const/16 v4, 0x21

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 151
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 153
    invoke-interface {p1}, Lmz;->a()Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 155
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    sget v1, LsH;->discussion_marked_as_resolved:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 156
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    sget-object v2, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 165
    :cond_2c
    :goto_2c
    invoke-interface {p1}, Lmz;->c()Ljava/lang/String;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_c1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_c1

    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_c1

    .line 167
    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 168
    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 174
    :cond_50
    :goto_50
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-le v0, v5, :cond_6c

    if-eqz p2, :cond_6c

    .line 175
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v5, v1}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 176
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    const-string v1, " \u2026 "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 178
    :cond_6c
    sget-object v0, LsY;->b:Landroid/widget/TextView;

    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    sget-object v0, LsY;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 181
    sget-object v0, LsY;->b:Landroid/widget/TextView;

    sget v1, LsD;->adapter_position_tag:I

    sget v2, LsY;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 182
    sget-object v0, LsY;->b:Landroid/widget/TextView;

    sget-object v1, LsY;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    sget-object v0, LsY;->b:Landroid/widget/TextView;

    sget-object v1, LsY;->a:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 184
    sget-object v0, LsY;->b:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 185
    return-void

    .line 158
    :cond_9e
    invoke-interface {p1}, Lmz;->b()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 160
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    sget v1, LsH;->discussion_re_opened:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 161
    sget-object v0, LsY;->a:Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    sget-object v2, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2c

    .line 169
    :cond_c1
    if-eqz v0, :cond_50

    .line 171
    sget-object v1, LsY;->a:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_50
.end method

.method public static a(Landroid/view/View;Landroid/content/res/Resources;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 198
    if-nez p0, :cond_3

    .line 208
    :cond_2
    :goto_2
    return-void

    .line 201
    :cond_3
    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 202
    if-eqz v2, :cond_2

    .line 203
    sget v0, LsC;->discussion_all_discussions_tile_background:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v0, v1

    .line 205
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    .line 206
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method
