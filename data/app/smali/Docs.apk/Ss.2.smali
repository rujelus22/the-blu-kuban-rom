.class public final LSs;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSD;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSB;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSy;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 34
    iput-object p1, p0, LSs;->a:LYD;

    .line 35
    const-class v0, LSD;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LSs;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LSs;->a:LZb;

    .line 38
    const-class v0, LSB;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LSs;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LSs;->b:LZb;

    .line 41
    const-class v0, LSy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LSs;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LSs;->c:LZb;

    .line 44
    const-class v0, LSq;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LSs;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LSs;->d:LZb;

    .line 47
    return-void
.end method

.method static synthetic a(LSs;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LSs;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LSs;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LSs;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LSs;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LSs;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 54
    const-class v0, LSD;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LSs;->a:LZb;

    invoke-virtual {p0, v0, v1}, LSs;->a(Laop;LZb;)V

    .line 55
    const-class v0, LSB;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LSs;->b:LZb;

    invoke-virtual {p0, v0, v1}, LSs;->a(Laop;LZb;)V

    .line 56
    const-class v0, LSy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LSs;->c:LZb;

    invoke-virtual {p0, v0, v1}, LSs;->a(Laop;LZb;)V

    .line 57
    const-class v0, LSq;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LSs;->d:LZb;

    invoke-virtual {p0, v0, v1}, LSs;->a(Laop;LZb;)V

    .line 58
    iget-object v0, p0, LSs;->a:LZb;

    iget-object v1, p0, LSs;->a:LYD;

    iget-object v1, v1, LYD;->a:LSs;

    iget-object v1, v1, LSs;->c:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 60
    iget-object v0, p0, LSs;->b:LZb;

    iget-object v1, p0, LSs;->a:LYD;

    iget-object v1, v1, LYD;->a:LSs;

    iget-object v1, v1, LSs;->d:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 62
    iget-object v0, p0, LSs;->c:LZb;

    new-instance v1, LSt;

    invoke-direct {v1, p0}, LSt;-><init>(LSs;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 76
    iget-object v0, p0, LSs;->d:LZb;

    new-instance v1, LSu;

    invoke-direct {v1, p0}, LSu;-><init>(LSs;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 100
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 104
    return-void
.end method
