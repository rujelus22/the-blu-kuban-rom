.class public LAB;
.super LAM;
.source "Image.java"


# instance fields
.field private a:LCl;

.field private final a:Lxu;

.field private a:LzI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LzI",
            "<",
            "LCl;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lzd;


# direct methods
.method public constructor <init>(LDb;Lzd;Lxu;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1}, LAM;-><init>(LDb;)V

    .line 41
    iput-object p2, p0, LAB;->a:Lzd;

    .line 42
    iput-object p3, p0, LAB;->a:Lxu;

    .line 43
    return-void
.end method


# virtual methods
.method public a(IILjava/util/List;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, LAB;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    if-gt p1, v0, :cond_1e

    iget-object v0, p0, LAB;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    if-lt p2, v0, :cond_1e

    .line 87
    iget-object v0, p0, LAB;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    .line 88
    new-instance v1, LKj;

    invoke-direct {v1, v0, v0}, LKj;-><init>(II)V

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_1e
    return-void
.end method

.method public a(Lza;)V
    .registers 5
    .parameter

    .prologue
    .line 47
    invoke-super {p0, p1}, LAM;->a(Lza;)V

    .line 49
    iget-object v0, p0, LAB;->a:LzI;

    iget-object v1, p0, LAB;->a:LDI;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 50
    iget-object v0, p0, LAB;->a:LDI;

    const/4 v1, 0x0

    iget-object v2, p0, LAB;->a:LDI;

    invoke-virtual {v2}, LDI;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LDI;->a(II)LDI;

    .line 51
    return-void
.end method

.method protected b()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 55
    iget-object v0, p0, LAB;->a:Lzd;

    iget-object v1, p0, LAB;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    invoke-interface {v0, v1}, Lzd;->a(I)[Lvu;

    move-result-object v0

    .line 56
    array-length v1, v0

    if-nez v1, :cond_18

    .line 57
    const-string v0, "Image"

    const-string v1, "Image element did not find image at location in model."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :goto_17
    return-void

    .line 60
    :cond_18
    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_23

    .line 61
    const-string v1, "Image"

    const-string v2, "Image element found more than one image at location in model."

    invoke-static {v1, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_23
    iget-object v1, p0, LAB;->a:Lxu;

    invoke-interface {v1}, Lxu;->a()LAS;

    move-result-object v1

    .line 64
    new-instance v2, LCl;

    aget-object v0, v0, v4

    iget-object v3, p0, LAB;->a:Lxu;

    invoke-direct {v2, v1, v0, v3}, LCl;-><init>(LAS;Lvu;Lxu;)V

    .line 65
    iget-object v0, p0, LAB;->a:LCl;

    invoke-virtual {v2, v0}, LCl;->a(LCt;)Z

    move-result v0

    if-nez v0, :cond_4f

    .line 66
    iput-object v2, p0, LAB;->a:LCl;

    .line 67
    iget-object v0, p0, LAB;->a:LzI;

    if-eqz v0, :cond_47

    .line 69
    iget-object v0, p0, LAB;->a:LzI;

    iget-object v1, p0, LAB;->a:LDI;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 71
    :cond_47
    iget-object v0, p0, LAB;->a:LCl;

    invoke-virtual {v0}, LCl;->a()LzI;

    move-result-object v0

    iput-object v0, p0, LAB;->a:LzI;

    .line 75
    :cond_4f
    iget-object v0, p0, LAB;->a:LDI;

    invoke-virtual {v0}, LDI;->length()I

    move-result v0

    if-nez v0, :cond_5e

    .line 76
    iget-object v0, p0, LAB;->a:LDI;

    const-string v1, "Image"

    invoke-virtual {v0, v1}, LDI;->a(Ljava/lang/CharSequence;)LDI;

    .line 78
    :cond_5e
    iget-object v0, p0, LAB;->a:LzI;

    iget-object v1, p0, LAB;->a:LDI;

    iget-object v2, p0, LAB;->a:LDI;

    invoke-virtual {v2}, LDI;->length()I

    move-result v2

    iget-object v3, p0, LAB;->a:Lxu;

    invoke-interface {v3}, Lxu;->a()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v1, v4, v2, v3}, LzI;->a(Landroid/text/Spannable;IILandroid/content/Context;)V

    goto :goto_17
.end method
