.class public Lzt;
.super Ljava/lang/Object;
.source "RendererProvidersImpl.java"

# interfaces
.implements LyR;


# instance fields
.field private final a:LCA;

.field private final a:Landroid/content/Context;

.field private final a:Lzs;


# direct methods
.method public constructor <init>(Lxu;Lvw;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-interface {p1}, Lxu;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lzt;->a:Landroid/content/Context;

    .line 27
    invoke-interface {p1}, Lxu;->a()Lzs;

    move-result-object v0

    iput-object v0, p0, Lzt;->a:Lzs;

    .line 28
    invoke-interface {p1}, Lxu;->a()LCA;

    move-result-object v0

    iput-object v0, p0, Lzt;->a:LCA;

    .line 29
    return-void
.end method


# virtual methods
.method public a()LvO;
    .registers 5

    .prologue
    .line 49
    iget-object v0, p0, Lzt;->a:LCA;

    invoke-interface {v0}, LCA;->a()Ljava/lang/String;

    move-result-object v0

    .line 50
    new-instance v1, LCF;

    iget-object v2, p0, Lzt;->a:Lzs;

    iget-object v3, p0, Lzt;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2, v3}, LCF;-><init>(Ljava/lang/String;Lzs;Landroid/content/Context;)V

    .line 51
    iget-object v2, p0, Lzt;->a:Lzs;

    invoke-interface {v2, v0, v1}, Lzs;->a(Ljava/lang/String;LCJ;)V

    .line 52
    return-object v1
.end method

.method public a()LvW;
    .registers 5

    .prologue
    .line 33
    iget-object v0, p0, Lzt;->a:LCA;

    invoke-interface {v0}, LCA;->a()Ljava/lang/String;

    move-result-object v0

    .line 34
    new-instance v1, LCG;

    iget-object v2, p0, Lzt;->a:Lzs;

    iget-object v3, p0, Lzt;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2, v3}, LCG;-><init>(Ljava/lang/String;Lzs;Landroid/content/Context;)V

    .line 35
    iget-object v2, p0, Lzt;->a:Lzs;

    invoke-interface {v2, v0, v1}, Lzs;->a(Ljava/lang/String;LCJ;)V

    .line 36
    return-object v1
.end method

.method public a()LwR;
    .registers 5

    .prologue
    .line 41
    iget-object v0, p0, Lzt;->a:LCA;

    invoke-interface {v0}, LCA;->a()Ljava/lang/String;

    move-result-object v0

    .line 42
    new-instance v1, LCK;

    iget-object v2, p0, Lzt;->a:Lzs;

    iget-object v3, p0, Lzt;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2, v3}, LCK;-><init>(Ljava/lang/String;Lzs;Landroid/content/Context;)V

    .line 43
    iget-object v2, p0, Lzt;->a:Lzs;

    invoke-interface {v2, v0, v1}, Lzs;->a(Ljava/lang/String;LCJ;)V

    .line 44
    return-object v1
.end method

.method public b()LvW;
    .registers 5

    .prologue
    .line 57
    iget-object v0, p0, Lzt;->a:LCA;

    invoke-interface {v0}, LCA;->a()Ljava/lang/String;

    move-result-object v0

    .line 58
    new-instance v1, LCG;

    iget-object v2, p0, Lzt;->a:Lzs;

    iget-object v3, p0, Lzt;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2, v3}, LCG;-><init>(Ljava/lang/String;Lzs;Landroid/content/Context;)V

    .line 59
    iget-object v2, p0, Lzt;->a:Lzs;

    invoke-interface {v2, v0, v1}, Lzs;->a(Ljava/lang/String;LCJ;)V

    .line 60
    return-object v1
.end method
