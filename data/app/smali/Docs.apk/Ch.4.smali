.class public LCh;
.super Ljava/lang/Object;
.source "AggregateStyle.java"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LCi;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LCi;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LCh;->a:Ljava/util/Map;

    .line 40
    return-void
.end method

.method public constructor <init>(Lvm;)V
    .registers 4
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LCi;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LCh;->a:Ljava/util/Map;

    .line 48
    sget-object v0, LCi;->a:LCi;

    invoke-interface {p1}, Lvm;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 49
    sget-object v0, LCi;->b:LCi;

    invoke-interface {p1}, Lvm;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 50
    sget-object v0, LCi;->c:LCi;

    invoke-interface {p1}, Lvm;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 51
    sget-object v0, LCi;->d:LCi;

    invoke-interface {p1}, Lvm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 52
    sget-object v0, LCi;->e:LCi;

    invoke-interface {p1}, Lvm;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 53
    sget-object v0, LCi;->h:LCi;

    invoke-interface {p1}, Lvm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 54
    sget-object v0, LCi;->i:LCi;

    invoke-interface {p1}, Lvm;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, LCh;->a:Z

    .line 56
    return-void
.end method

.method private a(LCi;)V
    .registers 3
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, LCh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    return-void
.end method

.method private a(LCi;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 126
    iget-object v0, p0, LCh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_11

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 128
    invoke-direct {p0, p1}, LCh;->a(LCi;)V

    .line 130
    :cond_11
    return-void
.end method

.method private b(LCi;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, LCh;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method


# virtual methods
.method public a(LCi;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 149
    iget-object v0, p0, LCh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 141
    iget-boolean v0, p0, LCh;->a:Z

    if-eqz v0, :cond_10

    iget-boolean v0, p0, LCh;->b:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, LCh;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public a(LCq;)Z
    .registers 4
    .parameter

    .prologue
    .line 113
    iget-boolean v0, p0, LCh;->b:Z

    if-nez v0, :cond_1d

    .line 114
    sget-object v0, LCi;->f:LCi;

    invoke-virtual {p1}, LCq;->a()Lxr;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 115
    sget-object v0, LCi;->g:LCi;

    invoke-virtual {p1}, LCq;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, LCh;->b:Z

    .line 119
    :cond_1d
    sget-object v0, LCi;->f:LCi;

    invoke-virtual {p1}, LCq;->a()Lxr;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 120
    sget-object v0, LCi;->g:LCi;

    invoke-virtual {p1}, LCq;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 122
    invoke-virtual {p0}, LCh;->a()Z

    move-result v0

    return v0
.end method

.method public a(LCw;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 79
    iget-boolean v1, p0, LCh;->a:Z

    if-nez v1, :cond_53

    .line 82
    sget-object v1, LCi;->a:LCi;

    invoke-virtual {p1}, LCw;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 83
    sget-object v1, LCi;->b:LCi;

    invoke-virtual {p1}, LCw;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 84
    sget-object v1, LCi;->c:LCi;

    invoke-virtual {p1}, LCw;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 85
    sget-object v1, LCi;->d:LCi;

    invoke-virtual {p1}, LCw;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 86
    sget-object v1, LCi;->e:LCi;

    invoke-virtual {p1}, LCw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 87
    sget-object v1, LCi;->h:LCi;

    invoke-virtual {p1}, LCw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 88
    sget-object v1, LCi;->i:LCi;

    invoke-virtual {p1}, LCw;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LCh;->b(LCi;Ljava/lang/Object;)V

    .line 89
    iput-boolean v0, p0, LCh;->a:Z

    .line 103
    :goto_52
    return v0

    .line 95
    :cond_53
    sget-object v0, LCi;->a:LCi;

    invoke-virtual {p1}, LCw;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 96
    sget-object v0, LCi;->b:LCi;

    invoke-virtual {p1}, LCw;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 97
    sget-object v0, LCi;->c:LCi;

    invoke-virtual {p1}, LCw;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 98
    sget-object v0, LCi;->d:LCi;

    invoke-virtual {p1}, LCw;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 99
    sget-object v0, LCi;->e:LCi;

    invoke-virtual {p1}, LCw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 100
    sget-object v0, LCi;->h:LCi;

    invoke-virtual {p1}, LCw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 101
    sget-object v0, LCi;->i:LCi;

    invoke-virtual {p1}, LCw;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LCh;->a(LCi;Ljava/lang/Object;)V

    .line 103
    invoke-virtual {p0}, LCh;->a()Z

    move-result v0

    goto :goto_52
.end method
