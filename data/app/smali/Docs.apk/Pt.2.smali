.class public final enum LPt;
.super Ljava/lang/Enum;
.source "AccountTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPt;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPt;

.field private static final synthetic a:[LPt;

.field public static final enum b:LPt;

.field public static final enum c:LPt;

.field public static final enum d:LPt;

.field public static final enum e:LPt;

.field public static final enum f:LPt;

.field public static final enum g:LPt;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0xe

    .line 43
    new-instance v0, LPt;

    const-string v1, "ACCOUNT_HOLDER_NAME"

    invoke-static {}, LPs;->b()LPs;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "accountHolderName"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, LQa;->a([Ljava/lang/String;)LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPt;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPt;->a:LPt;

    .line 47
    new-instance v0, LPt;

    const-string v1, "LAST_SYNC_TIME"

    invoke-static {}, LPs;->b()LPs;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "lastSyncTime"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(Ljava/lang/Object;)LQa;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LPt;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPt;->b:LPt;

    .line 50
    new-instance v0, LPt;

    const-string v1, "FOLDER_SYNC_CLIP_TIME"

    invoke-static {}, LPs;->b()LPs;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "folderSyncClipTime"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LPt;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPt;->c:LPt;

    .line 53
    new-instance v0, LPt;

    const-string v1, "DOCUMENT_SYNC_CLIP_TIME"

    invoke-static {}, LPs;->b()LPs;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "documentSyncClipTime"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LPt;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPt;->d:LPt;

    .line 56
    new-instance v0, LPt;

    const-string v1, "LAST_SYNC_ETAG"

    const/4 v2, 0x4

    invoke-static {}, LPs;->b()LPs;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    new-instance v4, LQa;

    const-string v5, "lastSyncEtag"

    sget-object v6, LQc;->c:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3, v7, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPt;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPt;->e:LPt;

    .line 59
    new-instance v0, LPt;

    const-string v1, "LAST_SYNC_CHANGE_STAMP"

    const/4 v2, 0x5

    invoke-static {}, LPs;->b()LPs;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    new-instance v4, LQa;

    const-string v5, "lastSyncChangeStamp"

    sget-object v6, LQc;->a:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v4}, LQa;->b()LQa;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LQa;->a(Ljava/lang/Object;)LQa;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPt;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPt;->f:LPt;

    .line 63
    new-instance v0, LPt;

    const-string v1, "SYNC_IN_PROGRESS"

    const/4 v2, 0x6

    invoke-static {}, LPs;->b()LPs;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    new-instance v4, LQa;

    const-string v5, "syncInProgress"

    sget-object v6, LQc;->a:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v4}, LQa;->b()LQa;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LQa;->a(Ljava/lang/Object;)LQa;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPt;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPt;->g:LPt;

    .line 42
    const/4 v0, 0x7

    new-array v0, v0, [LPt;

    sget-object v1, LPt;->a:LPt;

    aput-object v1, v0, v8

    sget-object v1, LPt;->b:LPt;

    aput-object v1, v0, v9

    sget-object v1, LPt;->c:LPt;

    aput-object v1, v0, v10

    sget-object v1, LPt;->d:LPt;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, LPt;->e:LPt;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LPt;->f:LPt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LPt;->g:LPt;

    aput-object v2, v0, v1

    sput-object v0, LPt;->a:[LPt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPt;->a:LPI;

    .line 71
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPt;
    .registers 2
    .parameter

    .prologue
    .line 42
    const-class v0, LPt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPt;

    return-object v0
.end method

.method public static values()[LPt;
    .registers 1

    .prologue
    .line 42
    sget-object v0, LPt;->a:[LPt;

    invoke-virtual {v0}, [LPt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPt;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, LPt;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 42
    invoke-virtual {p0}, LPt;->a()LPI;

    move-result-object v0

    return-object v0
.end method
