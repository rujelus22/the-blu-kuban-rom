.class public LFI;
.super LFu;
.source "TextView.java"


# instance fields
.field final synthetic b:Lcom/google/android/apps/docs/editors/text/TextView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 2
    .parameter

    .prologue
    .line 8406
    iput-object p1, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-direct {p0, p1}, LFu;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 8406
    invoke-direct {p0, p1}, LFI;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 8418
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    return v0
.end method

.method protected a()V
    .registers 4

    .prologue
    .line 8409
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_21

    .line 8410
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v2}, Lcom/google/android/apps/docs/editors/text/TextView;->h(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 8412
    :cond_21
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LFI;->a:Landroid/graphics/drawable/Drawable;

    .line 8413
    iget-object v0, p0, LFI;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x4040

    mul-float/2addr v0, v1

    const/high16 v1, 0x4080

    div-float/2addr v0, v1

    iput v0, p0, LFI;->a:F

    .line 8414
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 8423
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Lcom/google/android/apps/docs/editors/text/TextView;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    iget-object v1, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v1

    invoke-static {v0, p1, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 8426
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()V

    .line 8427
    return-void
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 8431
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v1

    .line 8432
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v2

    .line 8434
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v0

    .line 8437
    if-ne v0, v1, :cond_15

    .line 8451
    :goto_14
    return-void

    .line 8439
    :cond_15
    if-lt v0, v2, :cond_2b

    .line 8440
    iget-object v0, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    invoke-interface {v0, v2}, LEj;->g(I)I

    move-result v0

    .line 8441
    iget-object v1, p0, LFI;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;II)I

    move-result v0

    .line 8443
    if-lt v0, v2, :cond_2b

    .line 8444
    add-int/lit8 v0, v2, -0x1

    .line 8450
    :cond_2b
    invoke-virtual {p0, v0}, LFI;->a(I)V

    goto :goto_14
.end method
