.class public LUp;
.super Ljava/lang/Object;
.source "SharingAcl.java"


# instance fields
.field private final a:LeB;

.field private final a:LeG;

.field private final a:Z

.field private final b:Z


# direct methods
.method public constructor <init>(LUp;LeG;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, LeF;

    invoke-direct {v0}, LeF;-><init>()V

    invoke-virtual {p1}, LUp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LeF;->a(Ljava/lang/String;)LeF;

    move-result-object v0

    invoke-virtual {p1}, LUp;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LeF;->b(Ljava/lang/String;)LeF;

    move-result-object v0

    invoke-virtual {p1}, LUp;->a()LeK;

    move-result-object v1

    invoke-virtual {v0, v1}, LeF;->a(LeK;)LeF;

    move-result-object v0

    invoke-virtual {v0, p2}, LeF;->a(LeG;)LeF;

    move-result-object v0

    invoke-virtual {v0, p3}, LeF;->a(Z)LeF;

    move-result-object v0

    invoke-virtual {v0}, LeF;->a()LeB;

    move-result-object v0

    iput-object v0, p0, LUp;->a:LeB;

    .line 38
    iget-object v0, p1, LUp;->a:LeG;

    iput-object v0, p0, LUp;->a:LeG;

    .line 39
    iget-boolean v0, p1, LUp;->a:Z

    iput-boolean v0, p0, LUp;->a:Z

    .line 40
    iget-object v0, p0, LUp;->a:LeG;

    if-ne v0, p2, :cond_3e

    iget-boolean v0, p0, LUp;->a:Z

    if-eq v0, p3, :cond_42

    :cond_3e
    const/4 v0, 0x1

    :goto_3f
    iput-boolean v0, p0, LUp;->b:Z

    .line 42
    return-void

    .line 40
    :cond_42
    const/4 v0, 0x0

    goto :goto_3f
.end method

.method public constructor <init>(LeB;)V
    .registers 3
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, LeF;

    invoke-direct {v0}, LeF;-><init>()V

    invoke-virtual {v0, p1}, LeF;->a(LeB;)LeF;

    move-result-object v0

    invoke-virtual {v0}, LeF;->a()LeB;

    move-result-object v0

    iput-object v0, p0, LUp;->a:LeB;

    .line 26
    invoke-virtual {p1}, LeB;->a()LeG;

    move-result-object v0

    iput-object v0, p0, LUp;->a:LeG;

    .line 27
    invoke-virtual {p1}, LeB;->a()Z

    move-result v0

    iput-boolean v0, p0, LUp;->a:Z

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, LUp;->b:Z

    .line 29
    return-void
.end method


# virtual methods
.method public a()LeB;
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, LUp;->a:LeB;

    return-object v0
.end method

.method public a()LeG;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, LUp;->a:LeB;

    invoke-virtual {v0}, LeB;->a()LeG;

    move-result-object v0

    return-object v0
.end method

.method public a()LeI;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, LUp;->a:LeB;

    invoke-virtual {v0}, LeB;->a()LeI;

    move-result-object v0

    return-object v0
.end method

.method public a()LeK;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, LUp;->a:LeB;

    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, LUp;->a:LeB;

    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 45
    iget-boolean v0, p0, LUp;->b:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, LUp;->a:LeB;

    invoke-virtual {v0}, LeB;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, LUp;->a:LeB;

    invoke-virtual {v0}, LeB;->a()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 78
    instance-of v1, p1, LUp;

    if-nez v1, :cond_6

    .line 86
    :cond_5
    :goto_5
    return v0

    .line 82
    :cond_6
    check-cast p1, LUp;

    .line 83
    iget-object v1, p0, LUp;->a:LeB;

    iget-object v2, p1, LUp;->a:LeB;

    invoke-virtual {v1, v2}, LeB;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUp;->a:LeG;

    iget-object v2, p1, LUp;->a:LeG;

    invoke-virtual {v1, v2}, LeG;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, LUp;->a:Z

    iget-boolean v2, p1, LUp;->a:Z

    if-ne v1, v2, :cond_5

    iget-boolean v1, p0, LUp;->b:Z

    iget-boolean v2, p1, LUp;->b:Z

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 90
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LUp;->a:LeB;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LUp;->a:LeG;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LUp;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, LUp;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
