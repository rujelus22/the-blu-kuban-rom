.class abstract enum Lahg;
.super Ljava/lang/Enum;
.source "LocalCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lahg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lahg;

.field static final a:[Lahg;

.field public static final enum b:Lahg;

.field private static final synthetic b:[Lahg;

.field public static final enum c:Lahg;

.field public static final enum d:Lahg;

.field public static final enum e:Lahg;

.field public static final enum f:Lahg;

.field public static final enum g:Lahg;

.field public static final enum h:Lahg;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 438
    new-instance v0, Lahh;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lahh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->a:Lahg;

    .line 445
    new-instance v0, Lahi;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1, v4}, Lahi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->b:Lahg;

    .line 460
    new-instance v0, Lahj;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1, v5}, Lahj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->c:Lahg;

    .line 475
    new-instance v0, Lahk;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1, v6}, Lahk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->d:Lahg;

    .line 492
    new-instance v0, Lahl;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, Lahl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->e:Lahg;

    .line 499
    new-instance v0, Lahm;

    const-string v1, "WEAK_ACCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lahm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->f:Lahg;

    .line 514
    new-instance v0, Lahn;

    const-string v1, "WEAK_WRITE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lahn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->g:Lahg;

    .line 529
    new-instance v0, Laho;

    const-string v1, "WEAK_ACCESS_WRITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Laho;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lahg;->h:Lahg;

    .line 437
    const/16 v0, 0x8

    new-array v0, v0, [Lahg;

    sget-object v1, Lahg;->a:Lahg;

    aput-object v1, v0, v3

    sget-object v1, Lahg;->b:Lahg;

    aput-object v1, v0, v4

    sget-object v1, Lahg;->c:Lahg;

    aput-object v1, v0, v5

    sget-object v1, Lahg;->d:Lahg;

    aput-object v1, v0, v6

    sget-object v1, Lahg;->e:Lahg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lahg;->f:Lahg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lahg;->g:Lahg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lahg;->h:Lahg;

    aput-object v2, v0, v1

    sput-object v0, Lahg;->b:[Lahg;

    .line 556
    const/16 v0, 0x8

    new-array v0, v0, [Lahg;

    sget-object v1, Lahg;->a:Lahg;

    aput-object v1, v0, v3

    sget-object v1, Lahg;->b:Lahg;

    aput-object v1, v0, v4

    sget-object v1, Lahg;->c:Lahg;

    aput-object v1, v0, v5

    sget-object v1, Lahg;->d:Lahg;

    aput-object v1, v0, v6

    sget-object v1, Lahg;->e:Lahg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lahg;->f:Lahg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lahg;->g:Lahg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lahg;->h:Lahg;

    aput-object v2, v0, v1

    sput-object v0, Lahg;->a:[Lahg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILaha;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 437
    invoke-direct {p0, p1, p2}, Lahg;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(LahD;ZZ)Lahg;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 563
    sget-object v1, LahD;->c:LahD;

    if-ne p0, v1, :cond_14

    const/4 v1, 0x4

    move v2, v1

    :goto_7
    if-eqz p1, :cond_16

    const/4 v1, 0x1

    :goto_a
    or-int/2addr v1, v2

    if-eqz p2, :cond_e

    const/4 v0, 0x2

    :cond_e
    or-int/2addr v0, v1

    .line 566
    sget-object v1, Lahg;->a:[Lahg;

    aget-object v0, v1, v0

    return-object v0

    :cond_14
    move v2, v0

    .line 563
    goto :goto_7

    :cond_16
    move v1, v0

    goto :goto_a
.end method

.method public static valueOf(Ljava/lang/String;)Lahg;
    .registers 2
    .parameter

    .prologue
    .line 437
    const-class v0, Lahg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lahg;

    return-object v0
.end method

.method public static values()[Lahg;
    .registers 1

    .prologue
    .line 437
    sget-object v0, Lahg;->b:[Lahg;

    invoke-virtual {v0}, [Lahg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lahg;

    return-object v0
.end method


# virtual methods
.method a(LahA;Lahz;Lahz;)Lahz;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LahA",
            "<TK;TV;>;",
            "Lahz",
            "<TK;TV;>;",
            "Lahz",
            "<TK;TV;>;)",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 589
    invoke-interface {p2}, Lahz;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lahz;->a()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lahg;->a(LahA;Ljava/lang/Object;ILahz;)Lahz;

    move-result-object v0

    return-object v0
.end method

.method abstract a(LahA;Ljava/lang/Object;ILahz;)Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LahA",
            "<TK;TV;>;TK;I",
            "Lahz",
            "<TK;TV;>;)",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method a(Lahz;Lahz;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lahz",
            "<TK;TV;>;",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 596
    invoke-interface {p1}, Lahz;->a()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lahz;->a(J)V

    .line 598
    invoke-interface {p1}, Lahz;->c()Lahz;

    move-result-object v0

    invoke-static {v0, p2}, LagZ;->a(Lahz;Lahz;)V

    .line 599
    invoke-interface {p1}, Lahz;->b()Lahz;

    move-result-object v0

    invoke-static {p2, v0}, LagZ;->a(Lahz;Lahz;)V

    .line 601
    invoke-static {p1}, LagZ;->b(Lahz;)V

    .line 602
    return-void
.end method

.method b(Lahz;Lahz;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lahz",
            "<TK;TV;>;",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 608
    invoke-interface {p1}, Lahz;->b()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lahz;->b(J)V

    .line 610
    invoke-interface {p1}, Lahz;->e()Lahz;

    move-result-object v0

    invoke-static {v0, p2}, LagZ;->b(Lahz;Lahz;)V

    .line 611
    invoke-interface {p1}, Lahz;->d()Lahz;

    move-result-object v0

    invoke-static {p2, v0}, LagZ;->b(Lahz;Lahz;)V

    .line 613
    invoke-static {p1}, LagZ;->c(Lahz;)V

    .line 614
    return-void
.end method
