.class public Lzx;
.super Ljava/lang/Object;
.source "SimpleFastScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field final synthetic a:Lzw;

.field b:J


# direct methods
.method public constructor <init>(Lzw;)V
    .registers 2
    .parameter

    .prologue
    .line 448
    iput-object p1, p0, Lzx;->a:Lzw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()I
    .registers 9

    .prologue
    const-wide/16 v6, 0xd0

    .line 462
    iget-object v0, p0, Lzx;->a:Lzw;

    invoke-virtual {v0}, Lzw;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_e

    .line 463
    const/16 v0, 0xd0

    .line 472
    :goto_d
    return v0

    .line 466
    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 467
    iget-wide v2, p0, Lzx;->a:J

    iget-wide v4, p0, Lzx;->b:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1d

    .line 468
    const/4 v0, 0x0

    goto :goto_d

    .line 470
    :cond_1d
    iget-wide v2, p0, Lzx;->a:J

    sub-long/2addr v0, v2

    mul-long/2addr v0, v6

    iget-wide v2, p0, Lzx;->b:J

    div-long/2addr v0, v2

    sub-long v0, v6, v0

    long-to-int v0, v0

    goto :goto_d
.end method

.method a()V
    .registers 3

    .prologue
    .line 456
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lzx;->b:J

    .line 457
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lzx;->a:J

    .line 458
    iget-object v0, p0, Lzx;->a:Lzw;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lzw;->a(I)V

    .line 459
    return-void
.end method

.method public run()V
    .registers 3

    .prologue
    .line 477
    iget-object v0, p0, Lzx;->a:Lzw;

    invoke-virtual {v0}, Lzw;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_d

    .line 478
    invoke-virtual {p0}, Lzx;->a()V

    .line 487
    :goto_c
    return-void

    .line 482
    :cond_d
    invoke-virtual {p0}, Lzx;->a()I

    move-result v0

    if-lez v0, :cond_1d

    .line 483
    iget-object v0, p0, Lzx;->a:Lzw;

    invoke-static {v0}, Lzw;->a(Lzw;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_c

    .line 485
    :cond_1d
    iget-object v0, p0, Lzx;->a:Lzw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lzw;->a(I)V

    goto :goto_c
.end method
