.class public final LtQ;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;


# direct methods
.method public constructor <init>(LYD;)V
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 30
    iput-object p1, p0, LtQ;->a:LYD;

    .line 31
    return-void
.end method

.method static synthetic a(LtQ;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LtQ;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 78
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    new-instance v1, LtR;

    invoke-direct {v1, p0}, LtR;-><init>(LtQ;)V

    invoke-virtual {p0, v0, v1}, LtQ;->a(Ljava/lang/Class;Laou;)V

    .line 86
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    new-instance v1, LtS;

    invoke-direct {v1, p0}, LtS;-><init>(LtQ;)V

    invoke-virtual {p0, v0, v1}, LtQ;->a(Ljava/lang/Class;Laou;)V

    .line 94
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;

    new-instance v1, LtT;

    invoke-direct {v1, p0}, LtT;-><init>(LtQ;)V

    invoke-virtual {p0, v0, v1}, LtQ;->a(Ljava/lang/Class;Laou;)V

    .line 102
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    new-instance v1, LtU;

    invoke-direct {v1, p0}, LtU;-><init>(LtQ;)V

    invoke-virtual {p0, v0, v1}, LtQ;->a(Ljava/lang/Class;Laou;)V

    .line 110
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;

    new-instance v1, LtV;

    invoke-direct {v1, p0}, LtV;-><init>(LtQ;)V

    invoke-virtual {p0, v0, v1}, LtQ;->a(Ljava/lang/Class;Laou;)V

    .line 118
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, LtQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LtQ;

    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;)V

    .line 39
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, LtQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Ltf;

    iget-object v0, v0, Ltf;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LtQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsT;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    .line 55
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, LtQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 67
    iget-object v0, p0, LtQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Ltf;

    iget-object v0, v0, Ltf;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LtQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsT;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a:LsT;

    .line 73
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LtQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LtQ;

    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;)V

    .line 45
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, LtQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LtQ;

    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;)V

    .line 61
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 122
    return-void
.end method
