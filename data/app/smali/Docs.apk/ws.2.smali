.class public Lws;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lwp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lwp;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3782
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 3783
    return-void
.end method

.method public synthetic constructor <init>(Lvw;JLva;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3774
    invoke-direct {p0, p1, p2, p3}, Lws;-><init>(Lvw;J)V

    return-void
.end method

.method static a(Lvw;J)Lws;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3778
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lws;

    invoke-direct {v0, p0, p1, p2}, Lws;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 3830
    invoke-virtual {p0}, Lws;->a()J

    move-result-wide v0

    .line 3831
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->h(J)I

    move-result v0

    .line 3834
    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3861
    invoke-virtual {p0}, Lws;->a()J

    move-result-wide v0

    .line 3862
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 3865
    return-object v0
.end method

.method public a(IIDDLwL;Z)Lwl;
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3796
    invoke-virtual {p0}, Lws;->a()J

    move-result-wide v0

    .line 3797
    invoke-static/range {p7 .. p7}, LuZ;->a(LuY;)J

    move-result-wide v8

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v10, p8

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JIIDDJZ)J

    move-result-wide v1

    .line 3800
    iget-object v0, p0, Lws;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lwo;->a(Lvw;J)Lwo;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 3847
    invoke-virtual {p0}, Lws;->a()J

    move-result-wide v0

    .line 3848
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(JI)V

    .line 3849
    return-void
.end method
