.class LCS;
.super Ljava/lang/Object;
.source "XmlHttpRequestRelay.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic a:LCQ;

.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(LCQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 498
    iput-object p1, p0, LCS;->a:LCQ;

    iput-object p2, p0, LCS;->a:Ljava/lang/String;

    iput-object p3, p0, LCS;->b:Ljava/lang/String;

    iput-object p4, p0, LCS;->c:Ljava/lang/String;

    iput-object p5, p0, LCS;->d:Ljava/lang/String;

    iput p6, p0, LCS;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    .prologue
    .line 502
    const/4 v6, 0x0

    .line 504
    :try_start_1
    iget-object v0, p0, LCS;->a:LCQ;

    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1}, LCQ;->a(LCQ;)LCY;

    move-result-object v1

    iget-object v2, p0, LCS;->a:Ljava/lang/String;

    iget-object v3, p0, LCS;->b:Ljava/lang/String;

    iget-object v4, p0, LCS;->c:Ljava/lang/String;

    iget-object v5, p0, LCS;->d:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, LCQ;->a(LCQ;LCY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 506
    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1}, LCQ;->a(LCQ;)V

    .line 508
    monitor-enter p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_2d2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1b} :catch_d9
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1b} :catch_17c
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1b} :catch_1c0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1b} :catch_204
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1b} :catch_248
    .catch LNt; {:try_start_1 .. :try_end_1b} :catch_28d

    .line 509
    :try_start_1b
    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1}, LCQ;->a(LCQ;)LCY;

    move-result-object v1

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, LCY;->a(I)LCY;

    move-result-object v1

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LCY;->a(Ljava/lang/String;)LCY;

    move-result-object v1

    sget-object v2, LCX;->c:LCX;

    invoke-virtual {v1, v2}, LCY;->a(LCX;)LCY;

    .line 512
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_1b .. :try_end_3f} :catchall_d6

    .line 514
    :try_start_3f
    const-string v1, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HTTP Request status ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LCS;->a:LCQ;

    invoke-static {v3}, LCQ;->a(LCQ;)LCY;

    move-result-object v3

    invoke-virtual {v3}, LCY;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LCS;->a:LCQ;

    invoke-static {v3}, LCQ;->a(LCQ;)LCY;

    move-result-object v3

    invoke-virtual {v3}, LCY;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LCS;->a:LCQ;

    invoke-static {v3}, LCQ;->a(LCQ;)LCY;

    move-result-object v3

    invoke-virtual {v3}, LCY;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v1

    .line 518
    iget-object v2, p0, LCS;->a:LCQ;

    const-string v3, ""

    invoke-static {v1}, LCQ;->a([Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, LCQ;->a(LCQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-static {}, LCQ;->b()LCZ;

    move-result-object v2

    invoke-virtual {v2, v1}, LCZ;->a([Lorg/apache/http/Header;)V

    .line 521
    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1}, LCQ;->a(LCQ;)LCY;

    move-result-object v1

    invoke-virtual {v1}, LCY;->b()I

    move-result v1

    invoke-static {v1}, LCQ;->a(I)Z

    move-result v1

    if-eqz v1, :cond_143

    .line 523
    iget-object v0, p0, LCS;->a:LCQ;

    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1}, LCQ;->a(LCQ;)LCY;

    move-result-object v1

    invoke-virtual {v1}, LCY;->b()I

    move-result v1

    invoke-static {v0, v1}, LCQ;->a(LCQ;I)V
    :try_end_be
    .catchall {:try_start_3f .. :try_end_be} :catchall_2d2
    .catch Ljava/net/SocketException; {:try_start_3f .. :try_end_be} :catch_d9
    .catch Ljavax/net/ssl/SSLException; {:try_start_3f .. :try_end_be} :catch_17c
    .catch Ljava/io/InterruptedIOException; {:try_start_3f .. :try_end_be} :catch_1c0
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_be} :catch_204
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3f .. :try_end_be} :catch_248
    .catch LNt; {:try_start_3f .. :try_end_be} :catch_28d

    .line 556
    if-eqz v6, :cond_c3

    .line 558
    :try_start_c0
    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V
    :try_end_c3
    .catch Ljava/io/IOException; {:try_start_c0 .. :try_end_c3} :catch_2eb
    .catch Ljava/lang/InterruptedException; {:try_start_c0 .. :try_end_c3} :catch_11d

    .line 566
    :cond_c3
    :goto_c3
    :try_start_c3
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_d5
    .catch Ljava/lang/InterruptedException; {:try_start_c3 .. :try_end_d5} :catch_11d

    .line 573
    :goto_d5
    return-void

    .line 512
    :catchall_d6
    move-exception v0

    :try_start_d7
    monitor-exit p0
    :try_end_d8
    .catchall {:try_start_d7 .. :try_end_d8} :catchall_d6

    :try_start_d8
    throw v0
    :try_end_d9
    .catchall {:try_start_d8 .. :try_end_d9} :catchall_2d2
    .catch Ljava/net/SocketException; {:try_start_d8 .. :try_end_d9} :catch_d9
    .catch Ljavax/net/ssl/SSLException; {:try_start_d8 .. :try_end_d9} :catch_17c
    .catch Ljava/io/InterruptedIOException; {:try_start_d8 .. :try_end_d9} :catch_1c0
    .catch Ljava/io/IOException; {:try_start_d8 .. :try_end_d9} :catch_204
    .catch Landroid/accounts/AuthenticatorException; {:try_start_d8 .. :try_end_d9} :catch_248
    .catch LNt; {:try_start_d8 .. :try_end_d9} :catch_28d

    .line 534
    :catch_d9
    move-exception v0

    move-object v1, v6

    .line 535
    :goto_db
    :try_start_db
    const-string v2, "XmlHttpRequestRelay"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failure to send request ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, LCS;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v0, p0, LCS;->a:LCQ;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LCQ;->a(LCQ;I)V
    :try_end_105
    .catchall {:try_start_db .. :try_end_105} :catchall_304

    .line 556
    if-eqz v1, :cond_10a

    .line 558
    :try_start_107
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V
    :try_end_10a
    .catch Ljava/io/IOException; {:try_start_107 .. :try_end_10a} :catch_2f1
    .catch Ljava/lang/InterruptedException; {:try_start_107 .. :try_end_10a} :catch_11d

    .line 566
    :cond_10a
    :goto_10a
    :try_start_10a
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_11c
    .catch Ljava/lang/InterruptedException; {:try_start_10a .. :try_end_11c} :catch_11d

    goto :goto_d5

    .line 569
    :catch_11d
    move-exception v0

    .line 571
    const-string v1, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread interrupted ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LCS;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d5

    .line 527
    :cond_143
    :try_start_143
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 528
    if-eqz v0, :cond_31d

    .line 529
    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1, v0}, LCQ;->a(LCQ;Lorg/apache/http/HttpEntity;)Ljava/io/InputStreamReader;
    :try_end_14e
    .catchall {:try_start_143 .. :try_end_14e} :catchall_2d2
    .catch Ljava/net/SocketException; {:try_start_143 .. :try_end_14e} :catch_d9
    .catch Ljavax/net/ssl/SSLException; {:try_start_143 .. :try_end_14e} :catch_17c
    .catch Ljava/io/InterruptedIOException; {:try_start_143 .. :try_end_14e} :catch_1c0
    .catch Ljava/io/IOException; {:try_start_143 .. :try_end_14e} :catch_204
    .catch Landroid/accounts/AuthenticatorException; {:try_start_143 .. :try_end_14e} :catch_248
    .catch LNt; {:try_start_143 .. :try_end_14e} :catch_28d

    move-result-object v1

    .line 530
    :try_start_14f
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0, v1}, LCQ;->a(LCQ;Ljava/io/InputStreamReader;)V

    .line 533
    :goto_154
    iget-object v0, p0, LCS;->a:LCQ;

    iget-object v2, p0, LCS;->a:LCQ;

    invoke-static {v2}, LCQ;->a(LCQ;)LCY;

    move-result-object v2

    invoke-virtual {v2}, LCY;->b()I

    move-result v2

    invoke-static {v0, v2}, LCQ;->a(LCQ;I)V
    :try_end_163
    .catchall {:try_start_14f .. :try_end_163} :catchall_304
    .catch Ljava/net/SocketException; {:try_start_14f .. :try_end_163} :catch_31a
    .catch Ljavax/net/ssl/SSLException; {:try_start_14f .. :try_end_163} :catch_316
    .catch Ljava/io/InterruptedIOException; {:try_start_14f .. :try_end_163} :catch_312
    .catch Ljava/io/IOException; {:try_start_14f .. :try_end_163} :catch_30e
    .catch Landroid/accounts/AuthenticatorException; {:try_start_14f .. :try_end_163} :catch_30a
    .catch LNt; {:try_start_14f .. :try_end_163} :catch_307

    .line 556
    if-eqz v1, :cond_168

    .line 558
    :try_start_165
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V
    :try_end_168
    .catch Ljava/io/IOException; {:try_start_165 .. :try_end_168} :catch_2ee
    .catch Ljava/lang/InterruptedException; {:try_start_165 .. :try_end_168} :catch_11d

    .line 566
    :cond_168
    :goto_168
    :try_start_168
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_17a
    .catch Ljava/lang/InterruptedException; {:try_start_168 .. :try_end_17a} :catch_11d

    goto/16 :goto_d5

    .line 537
    :catch_17c
    move-exception v0

    .line 539
    :goto_17d
    :try_start_17d
    const-string v1, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failure to send request ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LCS;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    iget-object v0, p0, LCS;->a:LCQ;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LCQ;->a(LCQ;I)V
    :try_end_1a7
    .catchall {:try_start_17d .. :try_end_1a7} :catchall_2d2

    .line 556
    if-eqz v6, :cond_1ac

    .line 558
    :try_start_1a9
    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V
    :try_end_1ac
    .catch Ljava/io/IOException; {:try_start_1a9 .. :try_end_1ac} :catch_2f4
    .catch Ljava/lang/InterruptedException; {:try_start_1a9 .. :try_end_1ac} :catch_11d

    .line 566
    :cond_1ac
    :goto_1ac
    :try_start_1ac
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_1be
    .catch Ljava/lang/InterruptedException; {:try_start_1ac .. :try_end_1be} :catch_11d

    goto/16 :goto_d5

    .line 541
    :catch_1c0
    move-exception v0

    .line 542
    :goto_1c1
    :try_start_1c1
    const-string v1, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failure to send request ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LCS;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-object v0, p0, LCS;->a:LCQ;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LCQ;->a(LCQ;I)V
    :try_end_1eb
    .catchall {:try_start_1c1 .. :try_end_1eb} :catchall_2d2

    .line 556
    if-eqz v6, :cond_1f0

    .line 558
    :try_start_1ed
    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V
    :try_end_1f0
    .catch Ljava/io/IOException; {:try_start_1ed .. :try_end_1f0} :catch_2f7
    .catch Ljava/lang/InterruptedException; {:try_start_1ed .. :try_end_1f0} :catch_11d

    .line 566
    :cond_1f0
    :goto_1f0
    :try_start_1f0
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_202
    .catch Ljava/lang/InterruptedException; {:try_start_1f0 .. :try_end_202} :catch_11d

    goto/16 :goto_d5

    .line 544
    :catch_204
    move-exception v0

    .line 545
    :goto_205
    :try_start_205
    const-string v1, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failure to send request ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LCS;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v0, p0, LCS;->a:LCQ;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LCQ;->a(LCQ;I)V
    :try_end_22f
    .catchall {:try_start_205 .. :try_end_22f} :catchall_2d2

    .line 556
    if-eqz v6, :cond_234

    .line 558
    :try_start_231
    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V
    :try_end_234
    .catch Ljava/io/IOException; {:try_start_231 .. :try_end_234} :catch_2fa
    .catch Ljava/lang/InterruptedException; {:try_start_231 .. :try_end_234} :catch_11d

    .line 566
    :cond_234
    :goto_234
    :try_start_234
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_246
    .catch Ljava/lang/InterruptedException; {:try_start_234 .. :try_end_246} :catch_11d

    goto/16 :goto_d5

    .line 549
    :catch_248
    move-exception v0

    .line 550
    :goto_249
    :try_start_249
    const-string v1, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failure to send request ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LCS;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    iget-object v0, p0, LCS;->a:LCQ;

    const/16 v1, 0x193

    invoke-static {v0, v1}, LCQ;->a(LCQ;I)V
    :try_end_274
    .catchall {:try_start_249 .. :try_end_274} :catchall_2d2

    .line 556
    if-eqz v6, :cond_279

    .line 558
    :try_start_276
    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V
    :try_end_279
    .catch Ljava/io/IOException; {:try_start_276 .. :try_end_279} :catch_2fd
    .catch Ljava/lang/InterruptedException; {:try_start_276 .. :try_end_279} :catch_11d

    .line 566
    :cond_279
    :goto_279
    :try_start_279
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_28b
    .catch Ljava/lang/InterruptedException; {:try_start_279 .. :try_end_28b} :catch_11d

    goto/16 :goto_d5

    .line 552
    :catch_28d
    move-exception v0

    .line 553
    :goto_28e
    :try_start_28e
    const-string v1, "XmlHttpRequestRelay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failure to send request ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LCS;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    iget-object v0, p0, LCS;->a:LCQ;

    const/16 v1, 0x193

    invoke-static {v0, v1}, LCQ;->a(LCQ;I)V
    :try_end_2b9
    .catchall {:try_start_28e .. :try_end_2b9} :catchall_2d2

    .line 556
    if-eqz v6, :cond_2be

    .line 558
    :try_start_2bb
    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V
    :try_end_2be
    .catch Ljava/io/IOException; {:try_start_2bb .. :try_end_2be} :catch_300
    .catch Ljava/lang/InterruptedException; {:try_start_2bb .. :try_end_2be} :catch_11d

    .line 566
    :cond_2be
    :goto_2be
    :try_start_2be
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 567
    iget-object v0, p0, LCS;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V
    :try_end_2d0
    .catch Ljava/lang/InterruptedException; {:try_start_2be .. :try_end_2d0} :catch_11d

    goto/16 :goto_d5

    .line 556
    :catchall_2d2
    move-exception v0

    :goto_2d3
    if-eqz v6, :cond_2d8

    .line 558
    :try_start_2d5
    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V
    :try_end_2d8
    .catch Ljava/io/IOException; {:try_start_2d5 .. :try_end_2d8} :catch_302
    .catch Ljava/lang/InterruptedException; {:try_start_2d5 .. :try_end_2d8} :catch_11d

    .line 566
    :cond_2d8
    :goto_2d8
    :try_start_2d8
    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1}, LCQ;->a(LCQ;)LNj;

    move-result-object v1

    invoke-interface {v1}, LNj;->a()V

    .line 567
    iget-object v1, p0, LCS;->a:LCQ;

    invoke-static {v1}, LCQ;->a(LCQ;)LNj;

    move-result-object v1

    invoke-interface {v1}, LNj;->b()V

    throw v0
    :try_end_2eb
    .catch Ljava/lang/InterruptedException; {:try_start_2d8 .. :try_end_2eb} :catch_11d

    .line 559
    :catch_2eb
    move-exception v0

    goto/16 :goto_c3

    :catch_2ee
    move-exception v0

    goto/16 :goto_168

    :catch_2f1
    move-exception v0

    goto/16 :goto_10a

    :catch_2f4
    move-exception v0

    goto/16 :goto_1ac

    :catch_2f7
    move-exception v0

    goto/16 :goto_1f0

    :catch_2fa
    move-exception v0

    goto/16 :goto_234

    :catch_2fd
    move-exception v0

    goto/16 :goto_279

    :catch_300
    move-exception v0

    goto :goto_2be

    :catch_302
    move-exception v1

    goto :goto_2d8

    .line 556
    :catchall_304
    move-exception v0

    move-object v6, v1

    goto :goto_2d3

    .line 552
    :catch_307
    move-exception v0

    move-object v6, v1

    goto :goto_28e

    .line 549
    :catch_30a
    move-exception v0

    move-object v6, v1

    goto/16 :goto_249

    .line 544
    :catch_30e
    move-exception v0

    move-object v6, v1

    goto/16 :goto_205

    .line 541
    :catch_312
    move-exception v0

    move-object v6, v1

    goto/16 :goto_1c1

    .line 537
    :catch_316
    move-exception v0

    move-object v6, v1

    goto/16 :goto_17d

    .line 534
    :catch_31a
    move-exception v0

    goto/16 :goto_db

    :cond_31d
    move-object v1, v6

    goto/16 :goto_154
.end method
