.class public LDU;
.super Ljava/lang/Object;
.source "CanvasTextRenderer.java"

# interfaces
.implements LFf;


# instance fields
.field private a:LFe;

.field protected final a:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LDU;->a:Landroid/graphics/Canvas;

    .line 28
    new-instance v0, LDT;

    invoke-direct {v0, p1}, LDT;-><init>(Landroid/graphics/Canvas;)V

    iput-object v0, p0, LDU;->a:LFe;

    .line 29
    return-void
.end method


# virtual methods
.method public a()LFe;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, LDU;->a:LFe;

    return-object v0
.end method

.method public a(FFFF)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, LDU;->a:Landroid/graphics/Canvas;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 62
    return-void
.end method

.method public a(FFFFLandroid/graphics/Paint;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    iget-object v0, p0, LDU;->a:Landroid/graphics/Canvas;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 143
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 163
    return-void
.end method

.method public a(IF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 158
    return-void
.end method

.method public a(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 152
    iget-object v0, p0, LDU;->a:Landroid/graphics/Canvas;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 153
    return-void
.end method

.method public a(Landroid/text/style/ReplacementSpan;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 175
    iget-object v1, p0, LDU;->a:Landroid/graphics/Canvas;

    move-object v0, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Landroid/text/style/ReplacementSpan;->draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V

    .line 176
    return-void
.end method

.method public a(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-virtual {p0}, LDU;->a()LFe;

    move-result-object v0

    invoke-interface {v0, p7, p6}, LFe;->a(Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Paint;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 68
    invoke-static/range {p1 .. p7}, LFa;->a(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Path;

    move-result-object v0

    .line 69
    invoke-virtual {p0, v0, p6}, LDU;->a(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 73
    :goto_11
    return-void

    .line 71
    :cond_12
    iget-object v0, p0, LDU;->a:Landroid/graphics/Canvas;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    goto :goto_11
.end method

.method public a([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 78
    invoke-virtual {p0}, LDU;->a()LFe;

    move-result-object v0

    invoke-interface {v0, p7, p6}, LFe;->a(Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Paint;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 79
    invoke-static/range {p1 .. p7}, LFa;->a([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Path;

    move-result-object v0

    .line 82
    invoke-virtual {p0, v0, p6}, LDU;->a(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 86
    :goto_11
    return-void

    .line 84
    :cond_12
    iget-object v0, p0, LDU;->a:Landroid/graphics/Canvas;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    goto :goto_11
.end method

.method public b(FFFFLandroid/graphics/Paint;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, LDU;->a:Landroid/graphics/Canvas;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 148
    return-void
.end method
