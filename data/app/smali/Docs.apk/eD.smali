.class public enum LeD;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LeD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LeD;

.field private static final synthetic a:[LeD;

.field public static final enum b:LeD;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    new-instance v0, LeD;

    const-string v1, "COMMENTER"

    const-string v2, "commenter"

    invoke-direct {v0, v1, v3, v2}, LeD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LeD;->a:LeD;

    .line 77
    new-instance v0, LeE;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, LeE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LeD;->b:LeD;

    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [LeD;

    sget-object v1, LeD;->a:LeD;

    aput-object v1, v0, v3

    sget-object v1, LeD;->b:LeD;

    aput-object v1, v0, v4

    sput-object v0, LeD;->a:[LeD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 86
    iput-object p3, p0, LeD;->a:Ljava/lang/String;

    .line 87
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;LeC;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, LeD;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LeD;
    .registers 7
    .parameter

    .prologue
    .line 102
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-static {}, LeD;->values()[LeD;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v4, :cond_1c

    aget-object v0, v3, v1

    .line 104
    iget-object v5, v0, LeD;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 108
    :goto_17
    return-object v0

    .line 103
    :cond_18
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 108
    :cond_1c
    sget-object v0, LeD;->b:LeD;

    goto :goto_17
.end method

.method public static valueOf(Ljava/lang/String;)LeD;
    .registers 2
    .parameter

    .prologue
    .line 75
    const-class v0, LeD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LeD;

    return-object v0
.end method

.method public static values()[LeD;
    .registers 1

    .prologue
    .line 75
    sget-object v0, LeD;->a:[LeD;

    invoke-virtual {v0}, [LeD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LeD;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, LeD;->a:Ljava/lang/String;

    return-object v0
.end method
