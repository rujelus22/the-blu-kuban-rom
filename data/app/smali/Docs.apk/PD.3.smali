.class public final enum LPD;
.super Ljava/lang/Enum;
.source "CachedSearchTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPD;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPD;

.field private static final synthetic a:[LPD;

.field public static final enum b:LPD;

.field public static final enum c:LPD;

.field public static final enum d:LPD;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xe

    .line 38
    new-instance v0, LPD;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, LPC;->b()LPC;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "accountId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-static {}, LPs;->a()LPs;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPD;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPD;->a:LPD;

    .line 43
    new-instance v0, LPD;

    const-string v1, "QUERY"

    invoke-static {}, LPC;->b()LPC;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "query"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPD;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPD;->b:LPD;

    .line 46
    new-instance v0, LPD;

    const-string v1, "TIMESTAMP"

    invoke-static {}, LPC;->b()LPC;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "timestamp"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LPD;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPD;->c:LPD;

    .line 50
    new-instance v0, LPD;

    const-string v1, "COMPLETED"

    invoke-static {}, LPC;->b()LPC;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "completed"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LPD;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPD;->d:LPD;

    .line 37
    const/4 v0, 0x4

    new-array v0, v0, [LPD;

    sget-object v1, LPD;->a:LPD;

    aput-object v1, v0, v7

    sget-object v1, LPD;->b:LPD;

    aput-object v1, v0, v8

    sget-object v1, LPD;->c:LPD;

    aput-object v1, v0, v9

    sget-object v1, LPD;->d:LPD;

    aput-object v1, v0, v10

    sput-object v0, LPD;->a:[LPD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPD;->a:LPI;

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPD;
    .registers 2
    .parameter

    .prologue
    .line 37
    const-class v0, LPD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPD;

    return-object v0
.end method

.method public static values()[LPD;
    .registers 1

    .prologue
    .line 37
    sget-object v0, LPD;->a:[LPD;

    invoke-virtual {v0}, [LPD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPD;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, LPD;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, LPD;->a()LPI;

    move-result-object v0

    return-object v0
.end method
