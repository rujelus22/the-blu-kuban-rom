.class final enum LanY;
.super Ljava/lang/Enum;
.source "JsonScope.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LanY;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LanY;

.field private static final synthetic a:[LanY;

.field public static final enum b:LanY;

.field public static final enum c:LanY;

.field public static final enum d:LanY;

.field public static final enum e:LanY;

.field public static final enum f:LanY;

.field public static final enum g:LanY;

.field public static final enum h:LanY;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, LanY;

    const-string v1, "EMPTY_ARRAY"

    invoke-direct {v0, v1, v3}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->a:LanY;

    .line 37
    new-instance v0, LanY;

    const-string v1, "NONEMPTY_ARRAY"

    invoke-direct {v0, v1, v4}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->b:LanY;

    .line 43
    new-instance v0, LanY;

    const-string v1, "EMPTY_OBJECT"

    invoke-direct {v0, v1, v5}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->c:LanY;

    .line 49
    new-instance v0, LanY;

    const-string v1, "DANGLING_NAME"

    invoke-direct {v0, v1, v6}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->d:LanY;

    .line 55
    new-instance v0, LanY;

    const-string v1, "NONEMPTY_OBJECT"

    invoke-direct {v0, v1, v7}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->e:LanY;

    .line 60
    new-instance v0, LanY;

    const-string v1, "EMPTY_DOCUMENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->f:LanY;

    .line 65
    new-instance v0, LanY;

    const-string v1, "NONEMPTY_DOCUMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->g:LanY;

    .line 70
    new-instance v0, LanY;

    const-string v1, "CLOSED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LanY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanY;->h:LanY;

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [LanY;

    sget-object v1, LanY;->a:LanY;

    aput-object v1, v0, v3

    sget-object v1, LanY;->b:LanY;

    aput-object v1, v0, v4

    sget-object v1, LanY;->c:LanY;

    aput-object v1, v0, v5

    sget-object v1, LanY;->d:LanY;

    aput-object v1, v0, v6

    sget-object v1, LanY;->e:LanY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LanY;->f:LanY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LanY;->g:LanY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LanY;->h:LanY;

    aput-object v2, v0, v1

    sput-object v0, LanY;->a:[LanY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LanY;
    .registers 2
    .parameter

    .prologue
    .line 25
    const-class v0, LanY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LanY;

    return-object v0
.end method

.method public static values()[LanY;
    .registers 1

    .prologue
    .line 25
    sget-object v0, LanY;->a:[LanY;

    invoke-virtual {v0}, [LanY;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LanY;

    return-object v0
.end method
