.class LaqA;
.super LarK;
.source "ProvisionListenerStackCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LarK",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:I

.field a:LapA;

.field final a:LapX;

.field final a:Lapu;

.field final a:LaqB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaqB",
            "<TT;>;"
        }
    .end annotation
.end field

.field final synthetic a:Laqz;

.field a:LarJ;

.field a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laqz;Lapu;LapX;LaqB;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapu;",
            "LapX;",
            "LaqB",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    iput-object p1, p0, LaqA;->a:Laqz;

    invoke-direct {p0}, LarK;-><init>()V

    .line 84
    const/4 v0, -0x1

    iput v0, p0, LaqA;->a:I

    .line 90
    iput-object p4, p0, LaqA;->a:LaqB;

    .line 91
    iput-object p3, p0, LaqA;->a:LapX;

    .line 92
    iput-object p2, p0, LaqA;->a:Lapu;

    .line 93
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 97
    iget v0, p0, LaqA;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaqA;->a:I

    .line 98
    iget v0, p0, LaqA;->a:I

    iget-object v1, p0, LaqA;->a:Laqz;

    invoke-static {v1}, Laqz;->a(Laqz;)[LarJ;

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_33

    .line 100
    :try_start_11
    iget-object v0, p0, LaqA;->a:LaqB;

    invoke-interface {v0}, LaqB;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LaqA;->a:Ljava/lang/Object;
    :try_end_19
    .catch LapA; {:try_start_11 .. :try_end_19} :catch_1c

    .line 121
    :cond_19
    :goto_19
    iget-object v0, p0, LaqA;->a:Ljava/lang/Object;

    return-object v0

    .line 101
    :catch_1c
    move-exception v0

    .line 102
    iput-object v0, p0, LaqA;->a:LapA;

    .line 103
    new-instance v1, LaoB;

    iget-object v2, p0, LaqA;->a:Lapu;

    invoke-virtual {v0}, LapA;->a()Lapu;

    move-result-object v0

    invoke-virtual {v2, v0}, Lapu;->a(Lapu;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, LaoB;-><init>(Ljava/lang/Iterable;)V

    throw v1

    .line 105
    :cond_33
    iget v0, p0, LaqA;->a:I

    iget-object v1, p0, LaqA;->a:Laqz;

    invoke-static {v1}, Laqz;->a(Laqz;)[LarJ;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_61

    .line 106
    iget v1, p0, LaqA;->a:I

    .line 109
    :try_start_40
    iget-object v0, p0, LaqA;->a:Laqz;

    invoke-static {v0}, Laqz;->a(Laqz;)[LarJ;

    move-result-object v0

    iget v2, p0, LaqA;->a:I

    aget-object v0, v0, v2

    invoke-interface {v0, p0}, LarJ;->a(LarK;)V
    :try_end_4d
    .catch Ljava/lang/RuntimeException; {:try_start_40 .. :try_end_4d} :catch_55

    .line 114
    iget v0, p0, LaqA;->a:I

    if-ne v1, v0, :cond_19

    .line 116
    invoke-virtual {p0}, LaqA;->a()Ljava/lang/Object;

    goto :goto_19

    .line 110
    :catch_55
    move-exception v0

    .line 111
    iget-object v2, p0, LaqA;->a:Laqz;

    invoke-static {v2}, Laqz;->a(Laqz;)[LarJ;

    move-result-object v2

    aget-object v1, v2, v1

    iput-object v1, p0, LaqA;->a:LarJ;

    .line 112
    throw v0

    .line 119
    :cond_61
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already provisioned in this listener."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
