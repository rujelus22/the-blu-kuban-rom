.class public LTw;
.super LdG;
.source "AddCollaboratorTextDialogFragment.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 452
    iput-object p1, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-direct {p0}, LdG;-><init>()V

    return-void
.end method


# virtual methods
.method public c()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 456
    :try_start_1
    iget-object v0, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LTI;

    invoke-interface {v0}, LTI;->a()Ljava/util/List;

    move-result-object v0

    .line 457
    invoke-static {}, LdG;->b()Z
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_56

    move-result v1

    if-eqz v1, :cond_15

    .line 500
    iget-object v0, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LdG;)LdG;

    .line 502
    :goto_14
    return-void

    .line 461
    :cond_15
    :try_start_15
    iget-object v1, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 465
    invoke-static {}, LdG;->b()Z
    :try_end_22
    .catchall {:try_start_15 .. :try_end_22} :catchall_56

    move-result v1

    if-eqz v1, :cond_2b

    .line 500
    iget-object v0, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LdG;)LdG;

    goto :goto_14

    .line 471
    :cond_2b
    :try_start_2b
    invoke-static {v0}, LTD;->a(Ljava/util/List;)Labc;
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_56
    .catch Ljava/lang/InterruptedException; {:try_start_2b .. :try_end_2e} :catch_3b

    move-result-object v0

    .line 479
    :try_start_2f
    invoke-static {}, LdG;->b()Z
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_56

    move-result v1

    if-eqz v1, :cond_42

    .line 500
    iget-object v0, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LdG;)LdG;

    goto :goto_14

    .line 473
    :catch_3b
    move-exception v0

    .line 500
    iget-object v0, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LdG;)LdG;

    goto :goto_14

    .line 482
    :cond_42
    :try_start_42
    iget-object v1, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, LTx;

    invoke-direct {v2, p0, v0}, LTx;-><init>(LTw;Labc;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_50
    .catchall {:try_start_42 .. :try_end_50} :catchall_56

    .line 500
    iget-object v0, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LdG;)LdG;

    goto :goto_14

    :catchall_56
    move-exception v0

    iget-object v1, p0, LTw;->a:Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-static {v1, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LdG;)LdG;

    throw v0
.end method
