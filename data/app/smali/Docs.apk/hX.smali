.class public LhX;
.super Ljava/lang/Object;
.source "PickEntryActivity.java"


# instance fields
.field private final a:Landroid/content/Intent;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LhX;->a:Ljava/util/ArrayList;

    .line 620
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LhX;->a:Landroid/content/Intent;

    .line 621
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/PickEntryActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 622
    const-string v0, "Account name not specified"

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 624
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;LhW;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 615
    invoke-direct {p0, p1, p2}, LhX;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .registers 4

    .prologue
    .line 678
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    const-string v1, "disabledAncestors"

    iget-object v2, p0, LhX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 679
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a()LhX;
    .registers 4

    .prologue
    .line 658
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    const-string v1, "disablePreselectedEntry"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 659
    return-object p0
.end method

.method public a(I)LhX;
    .registers 4
    .parameter

    .prologue
    .line 651
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    const-string v1, "selectButtonText"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 652
    return-object p0
.end method

.method public a(Ljava/lang/String;)LhX;
    .registers 4
    .parameter

    .prologue
    .line 636
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    const-string v1, "resourceId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 637
    return-object p0
.end method

.method public a(Ljava/util/EnumSet;)LhX;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "LkP;",
            ">;)",
            "LhX;"
        }
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, LhX;->a:Landroid/content/Intent;

    const-string v1, "enabledKinds"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 642
    return-object p0
.end method

.method public b(Ljava/lang/String;)LhX;
    .registers 3
    .parameter

    .prologue
    .line 663
    iget-object v0, p0, LhX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 664
    return-object p0
.end method
