.class public LJf;
.super Ljava/lang/Object;
.source "LayoutInfo.java"


# instance fields
.field private a:J

.field protected a:Z


# direct methods
.method public constructor <init>(JZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, LJf;->a:Z

    .line 17
    iput-wide p1, p0, LJf;->a:J

    .line 18
    return-void
.end method


# virtual methods
.method public a()LIU;
    .registers 5

    .prologue
    .line 51
    new-instance v0, LIU;

    iget-wide v1, p0, LJf;->a:J

    invoke-static {v1, v2, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->LayoutInfo_getFont(JLJf;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LIU;-><init>(JZ)V

    return-object v0
.end method

.method public a()LJc;
    .registers 3

    .prologue
    .line 55
    iget-wide v0, p0, LJf;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->LayoutInfo_getHorizontalAlignment(JLJf;)I

    move-result v0

    invoke-static {v0}, LJc;->a(I)LJc;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 47
    iget-wide v0, p0, LJf;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->LayoutInfo_getText(JLJf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_3
    iget-wide v0, p0, LJf;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_19

    .line 30
    iget-boolean v0, p0, LJf;->a:Z

    if-eqz v0, :cond_15

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, LJf;->a:Z

    .line 32
    iget-wide v0, p0, LJf;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->delete_LayoutInfo(J)V

    .line 34
    :cond_15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LJf;->a:J
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    .line 36
    :cond_19
    monitor-exit p0

    return-void

    .line 29
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .registers 1

    .prologue
    .line 25
    invoke-virtual {p0}, LJf;->a()V

    .line 26
    return-void
.end method
