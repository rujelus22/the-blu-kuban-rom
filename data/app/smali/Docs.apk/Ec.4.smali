.class LEc;
.super Ljava/lang/Object;
.source "DynamicLayout.java"

# interfaces
.implements LEQ;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LEa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LEa;)V
    .registers 3
    .parameter

    .prologue
    .line 440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LEc;->a:Ljava/lang/ref/WeakReference;

    .line 442
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 456
    iget-object v0, p0, LEc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEa;

    .line 458
    if-eqz v0, :cond_d

    .line 459
    invoke-virtual {v0}, LEa;->a()V

    .line 461
    :cond_d
    return-void
.end method

.method public a(Ljava/lang/CharSequence;III)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 446
    iget-object v0, p0, LEc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEa;

    .line 448
    if-eqz v0, :cond_e

    .line 449
    invoke-static {v0, p1, p2, p3, p4}, LEa;->a(LEa;Ljava/lang/CharSequence;III)V

    .line 452
    :cond_d
    :goto_d
    return-void

    .line 450
    :cond_e
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_d

    .line 451
    check-cast p1, Landroid/text/Spannable;

    invoke-interface {p1, p0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    goto :goto_d
.end method
