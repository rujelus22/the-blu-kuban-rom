.class public LHg;
.super Ljava/lang/Object;
.source "TrixKeyboardBarFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, LHg;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 5
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, LHg;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)LGK;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 123
    iget-object v0, p0, LHg;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)LGK;

    move-result-object v0

    iget-object v1, p0, LHg;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, LHg;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-static {v2}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->getSelectionStart()I

    move-result v2

    invoke-interface {v0, v1, v2}, LGK;->a(Ljava/lang/CharSequence;I)V

    .line 126
    :cond_25
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 129
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 132
    return-void
.end method
