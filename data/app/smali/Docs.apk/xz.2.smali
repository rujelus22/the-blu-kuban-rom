.class public Lxz;
.super Ljava/lang/Object;
.source "DocumentWalker.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LDI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDI",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LDb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDb",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lxu;

.field private final a:Lzd;

.field private final a:Lzz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lzz",
            "<",
            "LCo;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:Lzz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lzz",
            "<",
            "LCw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LDb;LDI;Lzd;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "LDI",
            "<",
            "LAx;",
            ">;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lzz;

    invoke-direct {v0}, Lzz;-><init>()V

    iput-object v0, p0, Lxz;->a:Lzz;

    .line 48
    new-instance v0, Lzz;

    invoke-direct {v0}, Lzz;-><init>()V

    iput-object v0, p0, Lxz;->b:Lzz;

    .line 55
    iput-object p3, p0, Lxz;->a:Lzd;

    .line 56
    iput-object p4, p0, Lxz;->a:Lxu;

    .line 57
    iput-object p1, p0, Lxz;->a:LDb;

    .line 58
    iput-object p2, p0, Lxz;->a:LDI;

    .line 59
    invoke-interface {p4}, Lxu;->a()Z

    move-result v0

    iput-boolean v0, p0, Lxz;->a:Z

    .line 60
    return-void
.end method

.method static synthetic a(Lxz;)LDI;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lxz;->a:LDI;

    return-object v0
.end method

.method private a(IILjava/util/Iterator;)Ljava/lang/Iterable;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "LKj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, LxB;

    invoke-direct {v0, p0, p1, p3, p2}, LxB;-><init>(Lxz;ILjava/util/Iterator;I)V

    return-object v0
.end method

.method private a(Lxw;II)Ljava/lang/Iterable;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lxw;",
            "II)",
            "Ljava/lang/Iterable",
            "<",
            "LKj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-boolean v0, p0, Lxz;->a:Z

    if-eqz v0, :cond_25

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 110
    invoke-interface {p1, p2, p3}, Lxw;->a(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-interface {p1, p2, p3}, Lxw;->c(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v1, LxA;

    invoke-direct {v1, p0}, LxA;-><init>(Lxz;)V

    invoke-static {v0, v1}, LajH;->a(Ljava/lang/Iterable;Ljava/util/Comparator;)Laml;

    move-result-object v0

    .line 121
    :goto_20
    invoke-direct {p0, p2, p3, v0}, Lxz;->a(IILjava/util/Iterator;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0

    .line 119
    :cond_25
    invoke-interface {p1, p2, p3}, Lxw;->a(II)Ljava/util/Iterator;

    move-result-object v0

    goto :goto_20
.end method

.method static synthetic a(Lxz;)Lxu;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lxz;->a:Lxu;

    return-object v0
.end method

.method private a(IILCw;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lxz;->b:Lzz;

    iget-object v1, p0, Lxz;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1, p2, p3}, Lzz;->a(IILCt;)V

    .line 64
    return-void
.end method

.method private a(IILxw;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-interface {p3, p1}, Lxw;->a(I)Lxi;

    move-result-object v0

    .line 97
    new-instance v1, LCo;

    if-eqz v0, :cond_1d

    invoke-interface {v0}, Lxi;->a()Ljava/lang/String;

    move-result-object v0

    :goto_c
    invoke-direct {v1, v0}, LCo;-><init>(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lxz;->a:Lzz;

    iget-object v2, p0, Lxz;->a:LDb;

    invoke-virtual {v2}, LDb;->c()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2, p2, v1}, Lzz;->a(IILCt;)V

    .line 99
    return-void

    .line 97
    :cond_1d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private a(IILxw;I)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 79
    iget-boolean v0, p0, Lxz;->a:Z

    if-eqz v0, :cond_38

    invoke-interface {p3, p1}, Lxw;->a(I)Lvo;

    move-result-object v2

    .line 81
    :goto_9
    add-int v0, p1, p2

    if-ne v0, p4, :cond_3a

    .line 82
    if-le p2, v4, :cond_1f

    .line 83
    add-int/lit8 v0, p2, -0x1

    new-instance v1, LCw;

    invoke-interface {p3, p1}, Lxw;->a(I)Lxg;

    move-result-object v3

    iget-object v5, p0, Lxz;->a:Lxu;

    invoke-direct {v1, v3, v2, v5}, LCw;-><init>(Lxg;Lvo;Lxu;)V

    invoke-direct {p0, p1, v0, v1}, Lxz;->a(IILCw;)V

    .line 86
    :cond_1f
    add-int v0, p1, p2

    add-int/lit8 v6, v0, -0x1

    new-instance v0, LCw;

    invoke-interface {p3, p1}, Lxw;->a(I)Lxg;

    move-result-object v1

    iget-object v3, p0, Lxz;->a:Lxu;

    iget-object v5, p0, Lxz;->a:Lxu;

    invoke-interface {v5}, Lxu;->a()Landroid/text/Editable;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LCw;-><init>(Lxg;Lvo;Lxu;ZLandroid/text/Editable;)V

    invoke-direct {p0, v6, v4, v0}, Lxz;->a(IILCw;)V

    .line 93
    :goto_37
    return-void

    .line 79
    :cond_38
    const/4 v2, 0x0

    goto :goto_9

    .line 90
    :cond_3a
    new-instance v0, LCw;

    invoke-interface {p3, p1}, Lxw;->a(I)Lxg;

    move-result-object v1

    iget-object v3, p0, Lxz;->a:Lxu;

    invoke-direct {v0, v1, v2, v3}, LCw;-><init>(Lxg;Lvo;Lxu;)V

    invoke-direct {p0, p1, p2, v0}, Lxz;->a(IILCw;)V

    goto :goto_37
.end method

.method private a(Lzz;II)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LCt;",
            ">(",
            "Lzz",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lxz;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    sub-int v0, p2, v0

    .line 221
    new-instance v1, LxD;

    invoke-direct {v1, p0}, LxD;-><init>(Lxz;)V

    invoke-virtual {p1, v0, p3, v1}, Lzz;->a(IILzE;)Z

    .line 233
    return-void
.end method

.method private b(Lxw;II)Ljava/lang/Iterable;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lxw;",
            "II)",
            "Ljava/lang/Iterable",
            "<",
            "LKj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    invoke-interface {p1, p2, p3}, Lxw;->b(II)Ljava/util/Iterator;

    move-result-object v0

    .line 131
    invoke-direct {p0, p2, p3, v0}, Lxz;->a(IILjava/util/Iterator;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method private b(IILxw;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 187
    add-int v0, p1, p2

    add-int/lit8 v1, v0, -0x1

    .line 188
    iget-object v0, p0, Lxz;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v2

    .line 192
    invoke-direct {p0, p3, p1, v1}, Lxz;->a(Lxw;II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_12
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    .line 193
    invoke-virtual {v0}, LKj;->b()I

    move-result v4

    invoke-virtual {v0}, LKj;->a()I

    move-result v5

    sub-int/2addr v4, v5

    .line 194
    invoke-virtual {v0}, LKj;->a()I

    move-result v0

    invoke-direct {p0, v0, v4, p3, v2}, Lxz;->a(IILxw;I)V

    goto :goto_12

    .line 199
    :cond_2f
    invoke-direct {p0, p3, p1, v1}, Lxz;->b(Lxw;II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_37
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_54

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    .line 200
    invoke-virtual {v0}, LKj;->b()I

    move-result v2

    invoke-virtual {v0}, LKj;->a()I

    move-result v3

    sub-int/2addr v2, v3

    .line 201
    invoke-virtual {v0}, LKj;->a()I

    move-result v0

    invoke-direct {p0, v0, v2, p3}, Lxz;->a(IILxw;)V

    goto :goto_37

    .line 203
    :cond_54
    return-void
.end method

.method private d(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lxz;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    .line 211
    iget-object v1, p0, Lxz;->b:Lzz;

    sub-int v2, p1, v0

    iget-object v3, p0, Lxz;->a:LDI;

    invoke-virtual {v1, v2, p2, v3}, Lzz;->a(IILandroid/text/Spannable;)V

    .line 212
    iget-object v1, p0, Lxz;->a:Lzz;

    sub-int v0, p1, v0

    iget-object v2, p0, Lxz;->a:LDI;

    invoke-virtual {v1, v0, p2, v2}, Lzz;->a(IILandroid/text/Spannable;)V

    .line 213
    return-void
.end method

.method private e(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 240
    iget-object v0, p0, Lxz;->a:Lzz;

    invoke-direct {p0, v0, p1, p2}, Lxz;->a(Lzz;II)V

    .line 241
    iget-object v0, p0, Lxz;->b:Lzz;

    invoke-direct {p0, v0, p1, p2}, Lxz;->a(Lzz;II)V

    .line 242
    return-void
.end method


# virtual methods
.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 250
    if-ltz p2, :cond_28

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 251
    iget-object v0, p0, Lxz;->a:Lzd;

    invoke-interface {v0, p1, p2}, Lzd;->a(II)Lxw;

    move-result-object v0

    .line 252
    invoke-direct {p0, p1, p2, v0}, Lxz;->b(IILxw;)V

    .line 254
    add-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, p1, v1}, Lxw;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lxz;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    sub-int v1, p1, v1

    .line 256
    iget-object v2, p0, Lxz;->a:LDI;

    invoke-virtual {v2, v1, v0}, LDI;->a(ILjava/lang/String;)LDI;

    .line 258
    invoke-direct {p0, p1, p2}, Lxz;->e(II)V

    .line 259
    return-void

    .line 250
    :cond_28
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public a(IILCh;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 301
    iget-object v0, p0, Lxz;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    sub-int v0, p1, v0

    .line 302
    iget-object v1, p0, Lxz;->b:Lzz;

    new-instance v2, LxE;

    invoke-direct {v2, p0, v0, p2, p3}, LxE;-><init>(Lxz;IILCh;)V

    invoke-virtual {v1, v0, p2, v2}, Lzz;->a(IILzE;)Z

    move-result v0

    return v0
.end method

.method public b(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 266
    if-ltz p2, :cond_16

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 270
    invoke-direct {p0, p1, p2}, Lxz;->d(II)V

    .line 271
    iget-object v0, p0, Lxz;->a:Lzd;

    invoke-interface {v0, p1, p2}, Lzd;->a(II)Lxw;

    move-result-object v0

    .line 272
    invoke-direct {p0, p1, p2, v0}, Lxz;->b(IILxw;)V

    .line 274
    invoke-direct {p0, p1, p2}, Lxz;->e(II)V

    .line 275
    return-void

    .line 266
    :cond_16
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public c(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 282
    invoke-direct {p0, p1, p2}, Lxz;->d(II)V

    .line 284
    iget-object v0, p0, Lxz;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    sub-int v0, p1, v0

    .line 285
    iget-object v1, p0, Lxz;->a:LDI;

    add-int v2, v0, p2

    invoke-virtual {v1, v0, v2}, LDI;->a(II)LDI;

    .line 287
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lxz;->e(II)V

    .line 288
    return-void
.end method
