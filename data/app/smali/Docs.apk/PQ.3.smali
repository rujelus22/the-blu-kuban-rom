.class public final enum LPQ;
.super Ljava/lang/Enum;
.source "DocListDatabase.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPQ;",
        ">;",
        "LagF",
        "<",
        "LPN;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPQ;

.field private static final synthetic a:[LPQ;

.field public static final enum b:LPQ;

.field public static final enum c:LPQ;

.field public static final enum d:LPQ;

.field public static final enum e:LPQ;

.field public static final enum f:LPQ;

.field public static final enum g:LPQ;

.field public static final enum h:LPQ;

.field public static final enum i:LPQ;

.field public static final enum j:LPQ;

.field public static final enum k:LPQ;

.field public static final enum l:LPQ;

.field public static final enum m:LPQ;

.field public static final enum n:LPQ;


# instance fields
.field private final a:LPN;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 102
    new-instance v0, LPQ;

    const-string v1, "ACCOUNT"

    invoke-static {}, LPs;->a()LPs;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->a:LPQ;

    .line 103
    new-instance v0, LPQ;

    const-string v1, "ACCOUNT_METADATA"

    invoke-static {}, LPq;->a()LPq;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->b:LPQ;

    .line 104
    new-instance v0, LPQ;

    const-string v1, "DOCUMENT_CONTENT"

    invoke-static {}, LPS;->a()LPS;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->c:LPQ;

    .line 105
    new-instance v0, LPQ;

    const-string v1, "ENTRY"

    invoke-static {}, LPW;->a()LPW;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->d:LPQ;

    .line 106
    new-instance v0, LPQ;

    const-string v1, "COLLECTION"

    invoke-static {}, LPE;->a()LPE;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->e:LPQ;

    .line 107
    new-instance v0, LPQ;

    const-string v1, "DOCUMENT"

    const/4 v2, 0x5

    invoke-static {}, LPU;->a()LPU;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->f:LPQ;

    .line 108
    new-instance v0, LPQ;

    const-string v1, "CONTAINS_ID"

    const/4 v2, 0x6

    invoke-static {}, LPG;->a()LPG;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->g:LPQ;

    .line 109
    new-instance v0, LPQ;

    const-string v1, "APP_CACHE"

    const/4 v2, 0x7

    invoke-static {}, LPw;->a()LPw;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->h:LPQ;

    .line 110
    new-instance v0, LPQ;

    const-string v1, "CACHE_LIST"

    const/16 v2, 0x8

    invoke-static {}, LPy;->a()LPy;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->i:LPQ;

    .line 111
    new-instance v0, LPQ;

    const-string v1, "ACL"

    const/16 v2, 0x9

    invoke-static {}, LPu;->a()LPu;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->j:LPQ;

    .line 112
    new-instance v0, LPQ;

    const-string v1, "PENDING_OPERATION"

    const/16 v2, 0xa

    invoke-static {}, LQi;->a()LQi;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->k:LPQ;

    .line 113
    new-instance v0, LPQ;

    const-string v1, "CACHED_SEARCH"

    const/16 v2, 0xb

    invoke-static {}, LPC;->a()LPC;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->l:LPQ;

    .line 114
    new-instance v0, LPQ;

    const-string v1, "CACHED_SEARCH_RESULT"

    const/16 v2, 0xc

    invoke-static {}, LPA;->a()LPA;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->m:LPQ;

    .line 115
    new-instance v0, LPQ;

    const-string v1, "PARTIAL_FEED"

    const/16 v2, 0xd

    invoke-static {}, LQg;->a()LQg;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPQ;-><init>(Ljava/lang/String;ILPN;)V

    sput-object v0, LPQ;->n:LPQ;

    .line 101
    const/16 v0, 0xe

    new-array v0, v0, [LPQ;

    sget-object v1, LPQ;->a:LPQ;

    aput-object v1, v0, v4

    sget-object v1, LPQ;->b:LPQ;

    aput-object v1, v0, v5

    sget-object v1, LPQ;->c:LPQ;

    aput-object v1, v0, v6

    sget-object v1, LPQ;->d:LPQ;

    aput-object v1, v0, v7

    sget-object v1, LPQ;->e:LPQ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LPQ;->f:LPQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LPQ;->g:LPQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LPQ;->h:LPQ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LPQ;->i:LPQ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LPQ;->j:LPQ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LPQ;->k:LPQ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LPQ;->l:LPQ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LPQ;->m:LPQ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LPQ;->n:LPQ;

    aput-object v2, v0, v1

    sput-object v0, LPQ;->a:[LPQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPN;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121
    iput-object p3, p0, LPQ;->a:LPN;

    .line 122
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPQ;
    .registers 2
    .parameter

    .prologue
    .line 101
    const-class v0, LPQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPQ;

    return-object v0
.end method

.method public static values()[LPQ;
    .registers 1

    .prologue
    .line 101
    sget-object v0, LPQ;->a:[LPQ;

    invoke-virtual {v0}, [LPQ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPQ;

    return-object v0
.end method


# virtual methods
.method public a()LPN;
    .registers 2

    .prologue
    .line 126
    iget-object v0, p0, LPQ;->a:LPN;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 101
    invoke-virtual {p0}, LPQ;->a()LPN;

    move-result-object v0

    return-object v0
.end method
