.class public LVG;
.super Ljava/lang/Object;
.source "ChangedMetadataSyncerImpl.java"

# interfaces
.implements LVE;


# instance fields
.field private final a:LVH;

.field private final a:LlK;


# direct methods
.method public constructor <init>(LlK;LVH;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LVG;->a:LlK;

    .line 28
    iput-object p2, p0, LVG;->a:LVH;

    .line 29
    return-void
.end method


# virtual methods
.method public a(LasT;Ljava/lang/String;)LVV;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 43
    .line 45
    :try_start_2
    const-string v0, "ChangedMetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start uploadEntryInfo for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, LasT;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v0, p0, LVG;->a:LVH;

    invoke-interface {v0, p1, p2}, LVH;->a(LasT;Ljava/lang/String;)LasT;

    move-result-object v0

    .line 47
    instance-of v2, v0, LVV;

    if-nez v2, :cond_38

    .line 48
    const-string v0, "ChangedMetadataSyncer"

    const-string v2, "Returned type not of kind DocEntry"

    invoke-static {v0, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v0, p0, LVG;->a:LlK;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LlK;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 67
    :goto_37
    return-object v0

    .line 52
    :cond_38
    check-cast v0, LVV;
    :try_end_3a
    .catch LasH; {:try_start_2 .. :try_end_3a} :catch_57
    .catch Latc; {:try_start_2 .. :try_end_3a} :catch_7b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_3a} :catch_9f

    .line 66
    const-string v1, "ChangedMetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Finished uploadEntryInfo for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, LasT;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_37

    .line 53
    :catch_57
    move-exception v0

    .line 54
    const-string v2, "ChangedMetadataSyncer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authentication error while uploading: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, LasT;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 55
    iget-object v2, p0, LVG;->a:LlK;

    invoke-interface {v2, v5, v0}, LlK;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 56
    goto :goto_37

    .line 57
    :catch_7b
    move-exception v0

    .line 58
    const-string v2, "ChangedMetadataSyncer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Conflict error while uploading: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, LasT;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 59
    iget-object v2, p0, LVG;->a:LlK;

    invoke-interface {v2, v5, v0}, LlK;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 60
    goto :goto_37

    .line 61
    :catch_9f
    move-exception v0

    .line 62
    const-string v2, "ChangedMetadataSyncer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Network error while uploading: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, LasT;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 63
    iget-object v2, p0, LVG;->a:LlK;

    const/4 v3, 0x3

    invoke-interface {v2, v3, v0}, LlK;->a(ILjava/lang/Throwable;)V

    move-object v0, v1

    .line 64
    goto/16 :goto_37
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)LVV;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 76
    :try_start_0
    iget-object v0, p0, LVG;->a:LVH;

    invoke-static {p2}, LVI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LVH;->a(Ljava/lang/String;Ljava/lang/String;)LVV;
    :try_end_9
    .catch LasH; {:try_start_0 .. :try_end_9} :catch_b
    .catch Latc; {:try_start_0 .. :try_end_9} :catch_30
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_54

    move-result-object v0

    .line 89
    :goto_a
    return-object v0

    .line 79
    :catch_b
    move-exception v0

    .line 80
    const-string v1, "ChangedMetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Authentication error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LasH;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-object v1, p0, LVG;->a:LlK;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, LlK;->a(ILjava/lang/Throwable;)V

    .line 89
    :goto_2e
    const/4 v0, 0x0

    goto :goto_a

    .line 82
    :catch_30
    move-exception v0

    .line 83
    const-string v1, "ChangedMetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to parse document feed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Latc;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v1, p0, LVG;->a:LlK;

    const/4 v2, 0x2

    invoke-interface {v1, v2, v0}, LlK;->a(ILjava/lang/Throwable;)V

    goto :goto_2e

    .line 85
    :catch_54
    move-exception v0

    .line 86
    const-string v1, "ChangedMetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Network error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v1, p0, LVG;->a:LlK;

    const/4 v2, 0x3

    invoke-interface {v1, v2, v0}, LlK;->a(ILjava/lang/Throwable;)V

    goto :goto_2e
.end method
