.class public LQV;
.super Ljava/lang/Object;
.source "PunchModelImpl.java"

# interfaces
.implements LQY;


# instance fields
.field private a:I

.field private a:LQW;

.field private a:Ljava/lang/Boolean;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LQX;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LQS;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private b:Ljava/lang/Boolean;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LQV;->a:Ljava/util/Set;

    .line 65
    iput v1, p0, LQV;->a:I

    .line 69
    iput-boolean v1, p0, LQV;->a:Z

    return-void
.end method

.method private a(I)LQX;
    .registers 3
    .parameter

    .prologue
    .line 149
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Lagu;->b(II)I

    .line 151
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQX;

    .line 152
    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 130
    iget v0, p0, LQV;->b:I

    return v0
.end method

.method public a()Ljava/lang/Boolean;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, LQV;->a:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, LQV;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 170
    invoke-direct {p0, p1}, LQV;->a(I)LQX;

    move-result-object v0

    .line 171
    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-static {v0}, LQX;->a(LQX;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method a()V
    .registers 3

    .prologue
    .line 204
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQS;

    .line 205
    invoke-interface {v0}, LQS;->c()V

    goto :goto_6

    .line 207
    :cond_16
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Lagu;->b(II)I

    .line 184
    iget-object v0, p0, LQV;->a:LQW;

    if-eqz v0, :cond_19

    .line 185
    iget-object v0, p0, LQV;->a:LQW;

    invoke-interface {v0, p1}, LQW;->a(I)V

    .line 187
    :cond_19
    return-void
.end method

.method public a(ILQT;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, LQV;->a:LQW;

    if-eqz v0, :cond_9

    .line 164
    iget-object v0, p0, LQV;->a:LQW;

    invoke-interface {v0, p1, p2}, LQW;->a(ILQT;)V

    .line 166
    :cond_9
    return-void
.end method

.method a(ILQX;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 274
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 275
    invoke-virtual {p0, p1}, LQV;->b(I)V

    .line 276
    return-void
.end method

.method public a(LQS;)V
    .registers 3
    .parameter

    .prologue
    .line 101
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0}, Lagu;->b(Z)V

    .line 103
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 104
    return-void

    .line 102
    :cond_15
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public a(LQW;)V
    .registers 2
    .parameter

    .prologue
    .line 76
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iput-object p1, p0, LQV;->a:LQW;

    .line 78
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 341
    iput-object p1, p0, LQV;->a:Ljava/lang/String;

    .line 342
    invoke-virtual {p0}, LQV;->b()V

    .line 343
    return-void
.end method

.method public a(Ljava/lang/String;IIII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 246
    if-ge p5, p4, :cond_23

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Lagu;->a(Z)V

    .line 248
    iput-object p1, p0, LQV;->a:Ljava/lang/String;

    .line 249
    iput p5, p0, LQV;->d:I

    .line 250
    iput p4, p0, LQV;->a:I

    .line 251
    iput p2, p0, LQV;->b:I

    .line 252
    iput p3, p0, LQV;->c:I

    .line 254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LQV;->a:Ljava/util/List;

    .line 255
    :goto_18
    if-ge v1, p4, :cond_25

    .line 256
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    add-int/lit8 v1, v1, 0x1

    goto :goto_18

    :cond_23
    move v0, v1

    .line 246
    goto :goto_4

    .line 259
    :cond_25
    invoke-virtual {p0}, LQV;->e()V

    .line 260
    invoke-virtual {p0}, LQV;->b()V

    .line 261
    invoke-virtual {p0}, LQV;->c()V

    .line 263
    iget v0, p0, LQV;->d:I

    if-ltz v0, :cond_35

    .line 264
    invoke-virtual {p0}, LQV;->d()V

    .line 266
    :cond_35
    return-void
.end method

.method a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 284
    iput-boolean p1, p0, LQV;->a:Z

    .line 285
    return-void
.end method

.method public a(ZZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 319
    const/4 v0, 0x0

    .line 320
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, LQV;->b:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 322
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LQV;->b:Ljava/lang/Boolean;

    move v0, v1

    .line 326
    :cond_15
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, LQV;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2d

    .line 328
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LQV;->a:Ljava/lang/Boolean;

    .line 332
    :goto_27
    if-eqz v1, :cond_2c

    .line 333
    invoke-virtual {p0}, LQV;->a()V

    .line 335
    :cond_2c
    return-void

    :cond_2d
    move v1, v0

    goto :goto_27
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 291
    iget-boolean v0, p0, LQV;->a:Z

    return v0
.end method

.method public a(I)Z
    .registers 3
    .parameter

    .prologue
    .line 157
    invoke-direct {p0, p1}, LQV;->a(I)LQX;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public b()I
    .registers 2

    .prologue
    .line 135
    iget v0, p0, LQV;->c:I

    return v0
.end method

.method public b()Ljava/lang/Boolean;
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, LQV;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 176
    invoke-direct {p0, p1}, LQV;->a(I)LQX;

    move-result-object v0

    .line 177
    invoke-static {v0}, LQX;->b(LQX;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b()V
    .registers 3

    .prologue
    .line 210
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQS;

    .line 211
    invoke-interface {v0}, LQS;->a()V

    goto :goto_6

    .line 213
    :cond_16
    return-void
.end method

.method b(I)V
    .registers 4
    .parameter

    .prologue
    .line 222
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQS;

    .line 223
    invoke-interface {v0, p1}, LQS;->a(I)V

    goto :goto_6

    .line 225
    :cond_16
    return-void
.end method

.method public b(LQS;)V
    .registers 3
    .parameter

    .prologue
    .line 108
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 110
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public b(LQW;)V
    .registers 3
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, LQV;->a:LQW;

    if-ne v0, p1, :cond_7

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, LQV;->a:LQW;

    .line 97
    :cond_7
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 140
    iget v0, p0, LQV;->a:I

    return v0
.end method

.method c()V
    .registers 3

    .prologue
    .line 216
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQS;

    .line 217
    invoke-interface {v0}, LQS;->g()V

    goto :goto_6

    .line 219
    :cond_16
    return-void
.end method

.method c(I)V
    .registers 3
    .parameter

    .prologue
    .line 300
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    iget-object v0, p0, LQV;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p1, v0}, Lagu;->a(II)I

    .line 303
    iget v0, p0, LQV;->d:I

    if-eq v0, p1, :cond_1b

    const/4 v0, 0x1

    .line 304
    :goto_13
    iput p1, p0, LQV;->d:I

    .line 306
    if-eqz v0, :cond_1a

    .line 307
    invoke-virtual {p0}, LQV;->d()V

    .line 309
    :cond_1a
    return-void

    .line 303
    :cond_1b
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public d()I
    .registers 2

    .prologue
    .line 145
    iget v0, p0, LQV;->d:I

    return v0
.end method

.method d()V
    .registers 3

    .prologue
    .line 228
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQS;

    .line 229
    invoke-interface {v0}, LQS;->i()V

    goto :goto_6

    .line 231
    :cond_16
    return-void
.end method

.method e()V
    .registers 3

    .prologue
    .line 234
    iget-object v0, p0, LQV;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQS;

    .line 235
    invoke-interface {v0}, LQS;->e()V

    goto :goto_6

    .line 237
    :cond_16
    return-void
.end method
