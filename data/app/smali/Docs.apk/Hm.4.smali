.class final enum LHm;
.super Ljava/lang/Enum;
.source "TrixGLConfigChooser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LHm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:LHm;

.field public static final enum B:LHm;

.field public static final enum C:LHm;

.field public static final enum D:LHm;

.field public static final enum E:LHm;

.field public static final enum F:LHm;

.field public static final enum G:LHm;

.field public static final enum a:LHm;

.field private static final synthetic a:[LHm;

.field public static final enum b:LHm;

.field public static final enum c:LHm;

.field public static final enum d:LHm;

.field public static final enum e:LHm;

.field public static final enum f:LHm;

.field public static final enum g:LHm;

.field public static final enum h:LHm;

.field public static final enum i:LHm;

.field public static final enum j:LHm;

.field public static final enum k:LHm;

.field public static final enum l:LHm;

.field public static final enum m:LHm;

.field public static final enum n:LHm;

.field public static final enum o:LHm;

.field public static final enum p:LHm;

.field public static final enum q:LHm;

.field public static final enum r:LHm;

.field public static final enum s:LHm;

.field public static final enum t:LHm;

.field public static final enum u:LHm;

.field public static final enum v:LHm;

.field public static final enum w:LHm;

.field public static final enum x:LHm;

.field public static final enum y:LHm;

.field public static final enum z:LHm;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 102
    new-instance v0, LHm;

    const-string v1, "EGL_BUFFER_SIZE"

    const/16 v2, 0x3020

    invoke-direct {v0, v1, v4, v2}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->a:LHm;

    .line 103
    new-instance v0, LHm;

    const-string v1, "EGL_ALPHA_SIZE"

    const/16 v2, 0x3021

    invoke-direct {v0, v1, v5, v2}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->b:LHm;

    .line 104
    new-instance v0, LHm;

    const-string v1, "EGL_BLUE_SIZE"

    const/16 v2, 0x3022

    invoke-direct {v0, v1, v6, v2}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->c:LHm;

    .line 105
    new-instance v0, LHm;

    const-string v1, "EGL_GREEN_SIZE"

    const/16 v2, 0x3023

    invoke-direct {v0, v1, v7, v2}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->d:LHm;

    .line 106
    new-instance v0, LHm;

    const-string v1, "EGL_RED_SIZE"

    const/16 v2, 0x3024

    invoke-direct {v0, v1, v8, v2}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->e:LHm;

    .line 107
    new-instance v0, LHm;

    const-string v1, "EGL_DEPTH_SIZE"

    const/4 v2, 0x5

    const/16 v3, 0x3025

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->f:LHm;

    .line 108
    new-instance v0, LHm;

    const-string v1, "EGL_STENCIL_SIZE"

    const/4 v2, 0x6

    const/16 v3, 0x3026

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->g:LHm;

    .line 109
    new-instance v0, LHm;

    const-string v1, "EGL_CONFIG_CAVEAT"

    const/4 v2, 0x7

    const/16 v3, 0x3027

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->h:LHm;

    .line 110
    new-instance v0, LHm;

    const-string v1, "EGL_CONFIG_ID"

    const/16 v2, 0x8

    const/16 v3, 0x3028

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->i:LHm;

    .line 111
    new-instance v0, LHm;

    const-string v1, "EGL_LEVEL"

    const/16 v2, 0x9

    const/16 v3, 0x3029

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->j:LHm;

    .line 112
    new-instance v0, LHm;

    const-string v1, "EGL_MAX_PBUFFER_HEIGHT"

    const/16 v2, 0xa

    const/16 v3, 0x302a

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->k:LHm;

    .line 113
    new-instance v0, LHm;

    const-string v1, "EGL_MAX_PBUFFER_PIXELS"

    const/16 v2, 0xb

    const/16 v3, 0x302b

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->l:LHm;

    .line 114
    new-instance v0, LHm;

    const-string v1, "EGL_MAX_PBUFFER_WIDTH"

    const/16 v2, 0xc

    const/16 v3, 0x302c

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->m:LHm;

    .line 115
    new-instance v0, LHm;

    const-string v1, "EGL_NATIVE_RENDERABLE"

    const/16 v2, 0xd

    const/16 v3, 0x302d

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->n:LHm;

    .line 116
    new-instance v0, LHm;

    const-string v1, "EGL_NATIVE_VISUAL_ID"

    const/16 v2, 0xe

    const/16 v3, 0x302e

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->o:LHm;

    .line 117
    new-instance v0, LHm;

    const-string v1, "EGL_NATIVE_VISUAL_TYPE"

    const/16 v2, 0xf

    const/16 v3, 0x302f

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->p:LHm;

    .line 118
    new-instance v0, LHm;

    const-string v1, "EGL_PRESERVED_RESOURCES"

    const/16 v2, 0x10

    const/16 v3, 0x3030

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->q:LHm;

    .line 119
    new-instance v0, LHm;

    const-string v1, "EGL_SAMPLES"

    const/16 v2, 0x11

    const/16 v3, 0x3031

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->r:LHm;

    .line 120
    new-instance v0, LHm;

    const-string v1, "EGL_SAMPLE_BUFFERS"

    const/16 v2, 0x12

    const/16 v3, 0x3032

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->s:LHm;

    .line 121
    new-instance v0, LHm;

    const-string v1, "EGL_SURFACE_TYPE"

    const/16 v2, 0x13

    const/16 v3, 0x3033

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->t:LHm;

    .line 122
    new-instance v0, LHm;

    const-string v1, "EGL_TRANSPARENT_TYPE"

    const/16 v2, 0x14

    const/16 v3, 0x3034

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->u:LHm;

    .line 123
    new-instance v0, LHm;

    const-string v1, "EGL_TRANSPARENT_RED_VALUE"

    const/16 v2, 0x15

    const/16 v3, 0x3037

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->v:LHm;

    .line 124
    new-instance v0, LHm;

    const-string v1, "EGL_TRANSPARENT_GREEN_VALUE"

    const/16 v2, 0x16

    const/16 v3, 0x3036

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->w:LHm;

    .line 125
    new-instance v0, LHm;

    const-string v1, "EGL_TRANSPARENT_BLUE_VALUE"

    const/16 v2, 0x17

    const/16 v3, 0x3035

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->x:LHm;

    .line 126
    new-instance v0, LHm;

    const-string v1, "EGL_BIND_TO_TEXTURE_RGB"

    const/16 v2, 0x18

    const/16 v3, 0x3039

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->y:LHm;

    .line 127
    new-instance v0, LHm;

    const-string v1, "EGL_BIND_TO_TEXTURE_RGBA"

    const/16 v2, 0x19

    const/16 v3, 0x303a

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->z:LHm;

    .line 128
    new-instance v0, LHm;

    const-string v1, "EGL_MIN_SWAP_INTERVAL"

    const/16 v2, 0x1a

    const/16 v3, 0x303b

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->A:LHm;

    .line 129
    new-instance v0, LHm;

    const-string v1, "EGL_MAX_SWAP_INTERVAL"

    const/16 v2, 0x1b

    const/16 v3, 0x303c

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->B:LHm;

    .line 130
    new-instance v0, LHm;

    const-string v1, "EGL_LUMINANCE_SIZE"

    const/16 v2, 0x1c

    const/16 v3, 0x303d

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->C:LHm;

    .line 131
    new-instance v0, LHm;

    const-string v1, "EGL_ALPHA_MASK_SIZE"

    const/16 v2, 0x1d

    const/16 v3, 0x303e

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->D:LHm;

    .line 132
    new-instance v0, LHm;

    const-string v1, "EGL_COLOR_BUFFER_TYPE"

    const/16 v2, 0x1e

    const/16 v3, 0x303f

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->E:LHm;

    .line 133
    new-instance v0, LHm;

    const-string v1, "EGL_RENDERABLE_TYPE"

    const/16 v2, 0x1f

    const/16 v3, 0x3040

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->F:LHm;

    .line 134
    new-instance v0, LHm;

    const-string v1, "EGL_CONFORMANT"

    const/16 v2, 0x20

    const/16 v3, 0x3042

    invoke-direct {v0, v1, v2, v3}, LHm;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHm;->G:LHm;

    .line 101
    const/16 v0, 0x21

    new-array v0, v0, [LHm;

    sget-object v1, LHm;->a:LHm;

    aput-object v1, v0, v4

    sget-object v1, LHm;->b:LHm;

    aput-object v1, v0, v5

    sget-object v1, LHm;->c:LHm;

    aput-object v1, v0, v6

    sget-object v1, LHm;->d:LHm;

    aput-object v1, v0, v7

    sget-object v1, LHm;->e:LHm;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LHm;->f:LHm;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LHm;->g:LHm;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LHm;->h:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LHm;->i:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LHm;->j:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LHm;->k:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LHm;->l:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LHm;->m:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LHm;->n:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LHm;->o:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LHm;->p:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LHm;->q:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LHm;->r:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LHm;->s:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LHm;->t:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LHm;->u:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LHm;->v:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LHm;->w:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LHm;->x:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LHm;->y:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LHm;->z:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LHm;->A:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LHm;->B:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LHm;->C:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LHm;->D:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LHm;->E:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LHm;->F:LHm;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LHm;->G:LHm;

    aput-object v2, v0, v1

    sput-object v0, LHm;->a:[LHm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 138
    iput p3, p0, LHm;->a:I

    .line 139
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LHm;
    .registers 2
    .parameter

    .prologue
    .line 101
    const-class v0, LHm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LHm;

    return-object v0
.end method

.method public static values()[LHm;
    .registers 1

    .prologue
    .line 101
    sget-object v0, LHm;->a:[LHm;

    invoke-virtual {v0}, [LHm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LHm;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 142
    iget v0, p0, LHm;->a:I

    return v0
.end method
