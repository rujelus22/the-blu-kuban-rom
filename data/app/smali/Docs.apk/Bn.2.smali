.class public LBn;
.super Ljava/lang/Object;
.source "MenuHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V
    .registers 2
    .parameter

    .prologue
    .line 160
    iput-object p1, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 163
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 164
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/view/View;)V

    .line 199
    :cond_12
    :goto_12
    return-void

    .line 165
    :cond_13
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_undo_button:I

    if-ne v0, v1, :cond_21

    .line 166
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    goto :goto_12

    .line 167
    :cond_21
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_redo_button:I

    if-ne v0, v1, :cond_2f

    .line 168
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    goto :goto_12

    .line 169
    :cond_2f
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_font_button:I

    if-ne v0, v1, :cond_62

    .line 170
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    if-eqz v0, :cond_5a

    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 171
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    invoke-interface {v0}, LuK;->p()V

    .line 172
    check-cast p1, Landroid/widget/ToggleButton;

    invoke-virtual {p1, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto :goto_12

    .line 174
    :cond_5a
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    check-cast p1, Landroid/widget/ToggleButton;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    goto :goto_12

    .line 176
    :cond_62
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_indent_button:I

    if-ne v0, v1, :cond_77

    .line 177
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->j()V

    .line 178
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->q()V

    goto :goto_12

    .line 179
    :cond_77
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_outdent_button:I

    if-ne v0, v1, :cond_8c

    .line 180
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->k()V

    .line 181
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->q()V

    goto :goto_12

    .line 182
    :cond_8c
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_done_button:I

    if-ne v0, v1, :cond_9b

    .line 183
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->p()V

    goto/16 :goto_12

    .line 184
    :cond_9b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_color_button:I

    if-ne v0, v1, :cond_d0

    .line 185
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    if-eqz v0, :cond_c7

    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 186
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    invoke-interface {v0}, LuK;->p()V

    .line 187
    check-cast p1, Landroid/widget/ToggleButton;

    invoke-virtual {p1, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto/16 :goto_12

    .line 189
    :cond_c7
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    check-cast p1, Landroid/widget/ToggleButton;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    goto/16 :goto_12

    .line 191
    :cond_d0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->toolbar_alignment_button:I

    if-ne v0, v1, :cond_12

    .line 192
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    if-eqz v0, :cond_fc

    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_fc

    .line 193
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;

    move-result-object v0

    invoke-interface {v0}, LuK;->p()V

    .line 194
    check-cast p1, Landroid/widget/ToggleButton;

    invoke-virtual {p1, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto/16 :goto_12

    .line 196
    :cond_fc
    iget-object v0, p0, LBn;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    check-cast p1, Landroid/widget/ToggleButton;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    goto/16 :goto_12
.end method
