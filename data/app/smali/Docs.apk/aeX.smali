.class LaeX;
.super LaeQ;
.source "GsonGenerator.java"


# instance fields
.field private final a:LaeW;

.field private final a:Laoa;


# direct methods
.method constructor <init>(LaeW;Laoa;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, LaeQ;-><init>()V

    .line 42
    iput-object p1, p0, LaeX;->a:LaeW;

    .line 43
    iput-object p2, p0, LaeX;->a:Laoa;

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Laoa;->a(Z)V

    .line 46
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0}, Laoa;->a()V

    .line 51
    return-void
.end method

.method public a(D)V
    .registers 4
    .parameter

    .prologue
    .line 115
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1, p2}, Laoa;->a(D)Laoa;

    .line 116
    return-void
.end method

.method public a(F)V
    .registers 5
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, LaeX;->a:Laoa;

    float-to-double v1, p1

    invoke-virtual {v0, v1, v2}, Laoa;->a(D)Laoa;

    .line 121
    return-void
.end method

.method public a(I)V
    .registers 5
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LaeX;->a:Laoa;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Laoa;->a(J)Laoa;

    .line 91
    return-void
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1, p2}, Laoa;->a(J)Laoa;

    .line 96
    return-void
.end method

.method public a(Lamt;)V
    .registers 3
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1}, Laoa;->a(Ljava/lang/Number;)Laoa;

    .line 106
    return-void
.end method

.method public a(Lamv;)V
    .registers 3
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1}, Laoa;->a(Ljava/lang/Number;)Laoa;

    .line 111
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1}, Laoa;->a(Ljava/lang/String;)Laoa;

    .line 81
    return-void
.end method

.method public a(Ljava/math/BigDecimal;)V
    .registers 3
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1}, Laoa;->a(Ljava/lang/Number;)Laoa;

    .line 126
    return-void
.end method

.method public a(Ljava/math/BigInteger;)V
    .registers 3
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1}, Laoa;->a(Ljava/lang/Number;)Laoa;

    .line 101
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1}, Laoa;->a(Z)Laoa;

    .line 66
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 175
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0}, Laoa;->a()Laoa;

    .line 176
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0, p1}, Laoa;->b(Ljava/lang/String;)Laoa;

    .line 186
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0}, Laoa;->b()Laoa;

    .line 71
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 180
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0}, Laoa;->c()Laoa;

    .line 181
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0}, Laoa;->d()Laoa;

    .line 76
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, LaeX;->a:Laoa;

    invoke-virtual {v0}, Laoa;->e()Laoa;

    .line 86
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 190
    iget-object v0, p0, LaeX;->a:Laoa;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Laoa;->a(Ljava/lang/String;)V

    .line 191
    return-void
.end method
