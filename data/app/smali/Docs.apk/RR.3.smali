.class public final enum LRR;
.super Ljava/lang/Enum;
.source "PunchWebViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LRR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LRR;

.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LRR;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LRR;

.field public static final enum b:LRR;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 85
    new-instance v1, LRR;

    const-string v2, "PORTRAIT"

    const-string v3, "webViewPunchPortraitDuration"

    invoke-direct {v1, v2, v0, v3, v4}, LRR;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, LRR;->a:LRR;

    .line 86
    new-instance v1, LRR;

    const-string v2, "LANDSCAPE"

    const-string v3, "webViewPunchLandscapeDuration"

    invoke-direct {v1, v2, v4, v3, v5}, LRR;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v1, LRR;->b:LRR;

    .line 84
    new-array v1, v5, [LRR;

    sget-object v2, LRR;->a:LRR;

    aput-object v2, v1, v0

    sget-object v2, LRR;->b:LRR;

    aput-object v2, v1, v4

    sput-object v1, LRR;->a:[LRR;

    .line 91
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, LRR;->a:Ljava/util/Map;

    .line 94
    invoke-static {}, LRR;->values()[LRR;

    move-result-object v1

    array-length v2, v1

    :goto_31
    if-ge v0, v2, :cond_43

    aget-object v3, v1, v0

    .line 95
    sget-object v4, LRR;->a:Ljava/util/Map;

    iget v5, v3, LRR;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_31

    .line 97
    :cond_43
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 100
    iput-object p3, p0, LRR;->a:Ljava/lang/String;

    .line 101
    iput p4, p0, LRR;->a:I

    .line 102
    return-void
.end method

.method public static synthetic a(I)LRR;
    .registers 2
    .parameter

    .prologue
    .line 84
    invoke-static {p0}, LRR;->b(I)LRR;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, LRR;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(LRR;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 84
    invoke-direct {p0}, LRR;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)LRR;
    .registers 3
    .parameter

    .prologue
    .line 109
    sget-object v0, LRR;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRR;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LRR;
    .registers 2
    .parameter

    .prologue
    .line 84
    const-class v0, LRR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LRR;

    return-object v0
.end method

.method public static values()[LRR;
    .registers 1

    .prologue
    .line 84
    sget-object v0, LRR;->a:[LRR;

    invoke-virtual {v0}, [LRR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LRR;

    return-object v0
.end method
