.class LDP;
.super Ljava/lang/Object;
.source "BaseLayout.java"

# interfaces
.implements Landroid/text/GetChars;
.implements Ljava/lang/CharSequence;


# instance fields
.field a:I

.field a:LDO;

.field a:Landroid/text/TextUtils$TruncateAt;

.field a:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter

    .prologue
    .line 1556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1557
    iput-object p1, p0, LDP;->a:Ljava/lang/CharSequence;

    .line 1558
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1562
    const/4 v0, 0x1

    invoke-static {v0}, LEq;->a(I)[C

    move-result-object v0

    .line 1563
    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, p1, v1, v0, v2}, LDP;->getChars(II[CI)V

    .line 1564
    aget-char v1, v0, v2

    .line 1566
    invoke-static {v0}, LEq;->a([C)V

    .line 1567
    return v1
.end method

.method public getChars(II[CI)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1572
    iget-object v0, p0, LDP;->a:LDO;

    invoke-virtual {v0, p1}, LDO;->g(I)I

    move-result v3

    .line 1573
    iget-object v0, p0, LDP;->a:LDO;

    invoke-virtual {v0, p2}, LDO;->g(I)I

    move-result v6

    .line 1575
    iget-object v0, p0, LDP;->a:Ljava/lang/CharSequence;

    invoke-static {v0, p1, p2, p3, p4}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    .line 1577
    :goto_11
    if-gt v3, v6, :cond_1f

    .line 1578
    iget-object v0, p0, LDP;->a:LDO;

    move v1, p1

    move v2, p2

    move-object v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, LDO;->a(LDO;III[CI)V

    .line 1577
    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    .line 1580
    :cond_1f
    return-void
.end method

.method public length()I
    .registers 2

    .prologue
    .line 1584
    iget-object v0, p0, LDP;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1589
    sub-int v0, p2, p1

    new-array v0, v0, [C

    .line 1590
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LDP;->getChars(II[CI)V

    .line 1591
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1596
    invoke-virtual {p0}, LDP;->length()I

    move-result v0

    new-array v0, v0, [C

    .line 1597
    invoke-virtual {p0}, LDP;->length()I

    move-result v1

    invoke-virtual {p0, v2, v1, v0, v2}, LDP;->getChars(II[CI)V

    .line 1598
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method
