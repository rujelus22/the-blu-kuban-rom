.class public final enum LaaE;
.super Ljava/lang/Enum;
.source "OcrImageEvaluator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaaE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaaE;

.field private static final synthetic a:[LaaE;

.field public static final enum b:LaaE;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, LaaE;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, LaaE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaaE;->a:LaaE;

    new-instance v0, LaaE;

    const-string v1, "BLURRED"

    invoke-direct {v0, v1, v3}, LaaE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaaE;->b:LaaE;

    const/4 v0, 0x2

    new-array v0, v0, [LaaE;

    sget-object v1, LaaE;->a:LaaE;

    aput-object v1, v0, v2

    sget-object v1, LaaE;->b:LaaE;

    aput-object v1, v0, v3

    sput-object v0, LaaE;->a:[LaaE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaaE;
    .registers 2
    .parameter

    .prologue
    .line 38
    const-class v0, LaaE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaaE;

    return-object v0
.end method

.method public static values()[LaaE;
    .registers 1

    .prologue
    .line 38
    sget-object v0, LaaE;->a:[LaaE;

    invoke-virtual {v0}, [LaaE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaaE;

    return-object v0
.end method
