.class public final enum LxH;
.super Ljava/lang/Enum;
.source "Feature.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LxH;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LxH;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum a:LxH;

.field private static final synthetic a:[LxH;

.field public static final enum b:LxH;

.field public static final enum c:LxH;

.field public static final enum d:LxH;

.field public static final enum e:LxH;

.field public static final enum f:LxH;

.field public static final enum g:LxH;

.field public static final enum h:LxH;

.field public static final enum i:LxH;


# instance fields
.field private final a:I

.field private final a:LCy;


# direct methods
.method static constructor <clinit>()V
    .registers 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 23
    new-instance v0, LxH;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v7, v7, v9}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->a:LxH;

    .line 24
    new-instance v0, LxH;

    const-string v1, "FOOTNOTE_NUMBER"

    invoke-direct {v0, v1, v8, v8, v9}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->b:LxH;

    .line 25
    new-instance v0, LxH;

    const-string v1, "TABLE"

    invoke-direct {v0, v1, v10, v10, v9}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->c:LxH;

    .line 26
    new-instance v0, LxH;

    const-string v1, "EQUATION"

    new-instance v2, LCy;

    sget v3, LsC;->uneditable_equation:I

    sget v4, LsH;->uneditable_equation:I

    invoke-direct {v2, v3, v4, v8}, LCy;-><init>(IIZ)V

    invoke-direct {v0, v1, v11, v11, v2}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->d:LxH;

    .line 28
    new-instance v0, LxH;

    const-string v1, "INLINE_ENTITY"

    const/4 v2, 0x4

    const/4 v3, 0x4

    new-instance v4, LCy;

    sget v5, LsC;->uneditable_drawing:I

    sget v6, LsH;->uneditable_drawing:I

    invoke-direct {v4, v5, v6, v8}, LCy;-><init>(IIZ)V

    invoke-direct {v0, v1, v2, v3, v4}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->e:LxH;

    .line 30
    new-instance v0, LxH;

    const-string v1, "AUTOGEN_REGION"

    const/4 v2, 0x5

    const/4 v3, 0x5

    new-instance v4, LCy;

    sget v5, LsC;->uneditable_item:I

    sget v6, LsH;->uneditable_item:I

    invoke-direct {v4, v5, v6, v7}, LCy;-><init>(IIZ)V

    invoke-direct {v0, v1, v2, v3, v4}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->f:LxH;

    .line 32
    new-instance v0, LxH;

    const-string v1, "LINE_BREAK"

    const/4 v2, 0x6

    const/4 v3, 0x6

    new-instance v4, LCy;

    sget v5, LsC;->uneditable_item:I

    sget v6, LsH;->uneditable_item:I

    invoke-direct {v4, v5, v6, v7}, LCy;-><init>(IIZ)V

    invoke-direct {v0, v1, v2, v3, v4}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->g:LxH;

    .line 33
    new-instance v0, LxH;

    const-string v1, "HORIZONTAL_RULE"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3, v9}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->h:LxH;

    .line 34
    new-instance v0, LxH;

    const-string v1, "CELL_FEATURE"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3, v9}, LxH;-><init>(Ljava/lang/String;IILCy;)V

    sput-object v0, LxH;->i:LxH;

    .line 20
    const/16 v0, 0x9

    new-array v0, v0, [LxH;

    sget-object v1, LxH;->a:LxH;

    aput-object v1, v0, v7

    sget-object v1, LxH;->b:LxH;

    aput-object v1, v0, v8

    sget-object v1, LxH;->c:LxH;

    aput-object v1, v0, v10

    sget-object v1, LxH;->d:LxH;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, LxH;->e:LxH;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LxH;->f:LxH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LxH;->g:LxH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LxH;->h:LxH;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LxH;->i:LxH;

    aput-object v2, v0, v1

    sput-object v0, LxH;->a:[LxH;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LxH;->a:Ljava/util/Map;

    .line 40
    const-class v0, LxH;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxH;

    .line 41
    sget-object v2, LxH;->a:Ljava/util/Map;

    invoke-virtual {v0}, LxH;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c8

    .line 43
    :cond_e2
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILCy;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LCy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput p3, p0, LxH;->a:I

    .line 50
    iput-object p4, p0, LxH;->a:LCy;

    .line 51
    return-void
.end method

.method public static a(I)LxH;
    .registers 3
    .parameter

    .prologue
    .line 73
    sget-object v0, LxH;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxH;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LxH;
    .registers 2
    .parameter

    .prologue
    .line 20
    const-class v0, LxH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LxH;

    return-object v0
.end method

.method public static values()[LxH;
    .registers 1

    .prologue
    .line 20
    sget-object v0, LxH;->a:[LxH;

    invoke-virtual {v0}, [LxH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LxH;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 58
    iget v0, p0, LxH;->a:I

    return v0
.end method

.method public a()LCy;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, LxH;->a:LCy;

    return-object v0
.end method
