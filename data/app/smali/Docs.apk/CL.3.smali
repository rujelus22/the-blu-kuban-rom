.class public LCL;
.super Ljava/lang/Object;
.source "OptionsMenu.java"


# instance fields
.field private a:Landroid/view/MenuItem;

.field private final a:Lcom/google/android/apps/docs/RoboFragmentActivity;

.field private final a:Z

.field private b:Landroid/view/MenuItem;

.field private b:Z

.field private c:Landroid/view/MenuItem;

.field private c:Z

.field private d:Landroid/view/MenuItem;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/RoboFragmentActivity;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, LCL;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    .line 38
    iput-boolean p2, p0, LCL;->a:Z

    .line 39
    return-void
.end method

.method private a()V
    .registers 4

    .prologue
    .line 78
    iget-object v0, p0, LCL;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->a()Lo;

    move-result-object v0

    const-string v1, "SharingFragment"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 80
    if-nez v0, :cond_f

    .line 92
    :cond_e
    :goto_e
    return-void

    .line 83
    :cond_f
    check-cast v0, Lcom/google/android/apps/docs/editors/SharingFragment;

    .line 84
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()LUq;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()LUq;

    move-result-object v0

    invoke-interface {v0}, LUq;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 89
    new-instance v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;-><init>()V

    .line 90
    iget-object v1, p0, LCL;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->a()Lo;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Lo;Ljava/lang/String;)V

    goto :goto_e
.end method


# virtual methods
.method public a(Landroid/view/Menu;)V
    .registers 4
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LCL;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    iget-boolean v0, p0, LCL;->a:Z

    if-eqz v0, :cond_52

    sget v0, LsG;->menu_editor_debug:I

    :goto_c
    invoke-virtual {v1, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 45
    sget v0, LsD;->menu_edit:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LCL;->a:Landroid/view/MenuItem;

    .line 46
    iget-object v0, p0, LCL;->a:Landroid/view/MenuItem;

    iget-boolean v1, p0, LCL;->b:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 48
    sget v0, LsD;->menu_discussion:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LCL;->b:Landroid/view/MenuItem;

    .line 49
    iget-object v0, p0, LCL;->b:Landroid/view/MenuItem;

    iget-boolean v1, p0, LCL;->c:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 51
    sget v0, LsD;->menu_add_collaborator:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LCL;->c:Landroid/view/MenuItem;

    .line 52
    iget-object v0, p0, LCL;->c:Landroid/view/MenuItem;

    iget-boolean v1, p0, LCL;->d:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 54
    sget v0, LsD;->menu_webview_mode:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LCL;->d:Landroid/view/MenuItem;

    .line 55
    iget-object v0, p0, LCL;->d:Landroid/view/MenuItem;

    iget-boolean v1, p0, LCL;->e:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, LCL;->e:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 56
    return-void

    .line 42
    :cond_52
    sget v0, LsG;->menu_editor:I

    goto :goto_c
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 95
    iput-boolean p1, p0, LCL;->b:Z

    .line 96
    iget-object v0, p0, LCL;->a:Landroid/view/MenuItem;

    if-eqz v0, :cond_b

    .line 97
    iget-object v0, p0, LCL;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 99
    :cond_b
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    .line 59
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 61
    sget v1, LsD;->menu_send_feedback:I

    if-ne v0, v1, :cond_16

    .line 62
    new-instance v0, LZP;

    iget-object v1, p0, LCL;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    const-string v2, "android_docs_editors"

    invoke-direct {v0, v1, v2}, LZP;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 64
    invoke-virtual {v0}, LZP;->a()V

    .line 71
    :goto_14
    const/4 v0, 0x1

    :goto_15
    return v0

    .line 65
    :cond_16
    sget v1, LsD;->menu_add_collaborator:I

    if-ne v0, v1, :cond_1e

    .line 66
    invoke-direct {p0}, LCL;->a()V

    goto :goto_14

    .line 68
    :cond_1e
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 102
    iput-boolean p1, p0, LCL;->c:Z

    .line 103
    iget-object v0, p0, LCL;->b:Landroid/view/MenuItem;

    if-eqz v0, :cond_b

    .line 104
    iget-object v0, p0, LCL;->b:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 106
    :cond_b
    return-void
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 109
    iput-boolean p1, p0, LCL;->d:Z

    .line 110
    iget-object v0, p0, LCL;->c:Landroid/view/MenuItem;

    if-eqz v0, :cond_b

    .line 111
    iget-object v0, p0, LCL;->c:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 113
    :cond_b
    return-void
.end method

.method public d(Z)V
    .registers 3
    .parameter

    .prologue
    .line 116
    iput-boolean p1, p0, LCL;->e:Z

    .line 117
    iget-object v0, p0, LCL;->d:Landroid/view/MenuItem;

    if-eqz v0, :cond_f

    .line 118
    iget-object v0, p0, LCL;->d:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 120
    :cond_f
    return-void
.end method
