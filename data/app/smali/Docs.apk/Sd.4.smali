.class public final enum LSd;
.super Ljava/lang/Enum;
.source "PunchWebViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LSd;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LSd;

.field private static final synthetic a:[LSd;

.field public static final enum b:LSd;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 114
    new-instance v0, LSd;

    const-string v1, "FULLSCREEN"

    const-string v2, "webViewPunchFullscreenDuration"

    invoke-direct {v0, v1, v3, v2}, LSd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LSd;->a:LSd;

    .line 115
    new-instance v0, LSd;

    const-string v1, "EMBEDDED"

    const-string v2, "webViewPunchEmbeddedDuration"

    invoke-direct {v0, v1, v4, v2}, LSd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LSd;->b:LSd;

    .line 113
    const/4 v0, 0x2

    new-array v0, v0, [LSd;

    sget-object v1, LSd;->a:LSd;

    aput-object v1, v0, v3

    sget-object v1, LSd;->b:LSd;

    aput-object v1, v0, v4

    sput-object v0, LSd;->a:[LSd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 120
    iput-object p3, p0, LSd;->a:Ljava/lang/String;

    .line 121
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 124
    iget-object v0, p0, LSd;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(LSd;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 113
    invoke-direct {p0}, LSd;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LSd;
    .registers 2
    .parameter

    .prologue
    .line 113
    const-class v0, LSd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LSd;

    return-object v0
.end method

.method public static values()[LSd;
    .registers 1

    .prologue
    .line 113
    sget-object v0, LSd;->a:[LSd;

    invoke-virtual {v0}, [LSd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LSd;

    return-object v0
.end method
