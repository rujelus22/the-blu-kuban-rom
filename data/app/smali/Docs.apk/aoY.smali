.class public abstract LaoY;
.super Ljava/lang/Object;
.source "BindingImpl.java"

# interfaces
.implements Laof;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laof",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:LapL;

.field private final a:LapY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LapY",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final a:LaqC;

.field private final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Object;",
            "LapY",
            "<+TT;>;",
            "LaqC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, LaoY;->a:LapL;

    .line 41
    iput-object p2, p0, LaoY;->a:Laop;

    .line 42
    iput-object p3, p0, LaoY;->a:Ljava/lang/Object;

    .line 43
    iput-object p4, p0, LaoY;->a:LapY;

    .line 44
    iput-object p5, p0, LaoY;->a:LaqC;

    .line 45
    return-void
.end method

.method protected constructor <init>(Ljava/lang/Object;Laop;LaqC;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Laop",
            "<TT;>;",
            "LaqC;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object v0, p0, LaoY;->a:LapY;

    .line 49
    iput-object v0, p0, LaoY;->a:LapL;

    .line 50
    iput-object p1, p0, LaoY;->a:Ljava/lang/Object;

    .line 51
    iput-object p2, p0, LaoY;->a:Laop;

    .line 52
    iput-object p3, p0, LaoY;->a:LaqC;

    .line 53
    return-void
.end method


# virtual methods
.method protected a(Laop;)LaoY;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<TT;>;)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected a(LaqC;)LaoY;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaqC;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public a()Laop;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laop",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, LaoY;->a:Laop;

    return-object v0
.end method

.method public a()Laoz;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, LaoY;->a:Laoz;

    if-nez v0, :cond_1a

    .line 67
    iget-object v0, p0, LaoY;->a:LapL;

    if-nez v0, :cond_10

    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getProvider() not supported for module bindings"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_10
    iget-object v0, p0, LaoY;->a:LapL;

    iget-object v1, p0, LaoY;->a:Laop;

    invoke-virtual {v0, v1}, LapL;->a(Laop;)Laoz;

    move-result-object v0

    iput-object v0, p0, LaoY;->a:Laoz;

    .line 73
    :cond_1a
    iget-object v0, p0, LaoY;->a:Laoz;

    return-object v0
.end method

.method public a()LapY;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LapY",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, LaoY;->a:LapY;

    return-object v0
.end method

.method public a()LaqC;
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, LaoY;->a:LaqC;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, LaoY;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(LaqZ;)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LaqZ",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, LaoY;->a:LaqC;

    invoke-virtual {v0, p1}, LaqC;->a(LaqZ;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 89
    instance-of v0, p0, Lary;

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 109
    const-class v0, Laof;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "key"

    iget-object v2, p0, LaoY;->a:Laop;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "scope"

    iget-object v2, p0, LaoY;->a:LaqC;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "source"

    iget-object v2, p0, LaoY;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
