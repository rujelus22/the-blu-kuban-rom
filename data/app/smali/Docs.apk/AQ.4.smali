.class public LAQ;
.super Ljava/lang/Object;
.source "AuthorImageManager.java"


# instance fields
.field private final a:I

.field private a:LBd;

.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method public constructor <init>(LBd;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/16 v0, 0x3a

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v0, p0, LAQ;->a:I

    .line 23
    iput v0, p0, LAQ;->b:I

    .line 26
    iput-object p1, p0, LAQ;->a:LBd;

    .line 27
    iput-object p2, p0, LAQ;->a:Ljava/lang/String;

    .line 28
    return-void
.end method

.method static synthetic a(LAQ;)LBd;
    .registers 2
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, LAQ;->a:LBd;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 79
    iget-object v0, p0, LAQ;->a:LBd;

    invoke-virtual {v0}, LBd;->b()V

    .line 80
    return-void
.end method

.method public a(Landroid/widget/ImageView;Landroid/net/Uri;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x3a

    const/4 v0, 0x1

    .line 43
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    iget-object v2, p0, LAQ;->a:LBd;

    invoke-virtual {v2, v1, v3, v3}, LBd;->a(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 45
    if-eqz v2, :cond_13

    .line 48
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 74
    :goto_12
    return v0

    .line 52
    :cond_13
    new-instance v2, LAR;

    invoke-direct {v2, p0, p1}, LAR;-><init>(LAQ;Landroid/widget/ImageView;)V

    .line 66
    iget-object v3, p0, LAQ;->a:LBd;

    invoke-virtual {v3, v1, v2}, LBd;->a(Ljava/lang/String;LBh;)V

    .line 67
    iget-object v2, p0, LAQ;->a:LBd;

    invoke-virtual {v2, v1, v0}, LBd;->a(Ljava/lang/String;Z)V

    .line 68
    iget-object v0, p0, LAQ;->a:LBd;

    iget-object v2, p0, LAQ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LBd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 74
    const/4 v0, 0x0

    goto :goto_12
.end method
