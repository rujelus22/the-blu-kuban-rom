.class public abstract enum LaaM;
.super Ljava/lang/Enum;
.source "PreferenceUtils.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaaM;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaaM;

.field private static final synthetic a:[LaaM;

.field public static final enum b:LaaM;

.field public static final enum c:LaaM;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, LaaN;

    const-string v1, "DISABLE"

    const-string v3, "disable"

    sget v4, Len;->prefs_pinned_files_auto_sync_options_never:I

    sget v5, Len;->prefs_pinned_files_auto_sync_summary_disable:I

    invoke-direct/range {v0 .. v5}, LaaN;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, LaaM;->a:LaaM;

    .line 35
    new-instance v3, LaaO;

    const-string v4, "WIFI"

    const-string v6, "wifi"

    sget v7, Len;->prefs_pinned_files_auto_sync_options_wifi:I

    sget v8, Len;->prefs_pinned_files_auto_sync_summary_wifi:I

    move v5, v9

    invoke-direct/range {v3 .. v8}, LaaO;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, LaaM;->b:LaaM;

    .line 44
    new-instance v3, LaaP;

    const-string v4, "ALWAYS"

    const-string v6, "always"

    sget v7, Len;->prefs_pinned_files_auto_sync_options_always:I

    sget v8, Len;->prefs_pinned_files_auto_sync_summary_always:I

    move v5, v10

    invoke-direct/range {v3 .. v8}, LaaP;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, LaaM;->c:LaaM;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [LaaM;

    sget-object v1, LaaM;->a:LaaM;

    aput-object v1, v0, v2

    sget-object v1, LaaM;->b:LaaM;

    aput-object v1, v0, v9

    sget-object v1, LaaM;->c:LaaM;

    aput-object v1, v0, v10

    sput-object v0, LaaM;->a:[LaaM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaaM;->a:Ljava/lang/String;

    .line 58
    iput p4, p0, LaaM;->a:I

    .line 59
    iput p5, p0, LaaM;->b:I

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;IILaaK;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct/range {p0 .. p5}, LaaM;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LaaM;
    .registers 6
    .parameter

    .prologue
    .line 77
    invoke-static {}, LaaM;->values()[LaaM;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_16

    aget-object v3, v1, v0

    .line 78
    iget-object v4, v3, LaaM;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 79
    return-object v3

    .line 77
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 82
    :cond_16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal auto-sync option value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a()[Ljava/lang/CharSequence;
    .registers 4

    .prologue
    .line 96
    invoke-static {}, LaaM;->values()[LaaM;

    move-result-object v0

    array-length v1, v0

    .line 97
    new-array v2, v1, [Ljava/lang/CharSequence;

    .line 98
    const/4 v0, 0x0

    :goto_8
    if-ge v0, v1, :cond_19

    .line 99
    invoke-static {}, LaaM;->values()[LaaM;

    move-result-object v3

    aget-object v3, v3, v0

    .line 100
    invoke-virtual {v3}, LaaM;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 102
    :cond_19
    return-object v2
.end method

.method public static a(Landroid/content/Context;)[Ljava/lang/CharSequence;
    .registers 5
    .parameter

    .prologue
    .line 86
    invoke-static {}, LaaM;->values()[LaaM;

    move-result-object v0

    array-length v1, v0

    .line 87
    new-array v2, v1, [Ljava/lang/CharSequence;

    .line 88
    const/4 v0, 0x0

    :goto_8
    if-ge v0, v1, :cond_19

    .line 89
    invoke-static {}, LaaM;->values()[LaaM;

    move-result-object v3

    aget-object v3, v3, v0

    .line 90
    invoke-virtual {v3, p0}, LaaM;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 92
    :cond_19
    return-object v2
.end method

.method public static valueOf(Ljava/lang/String;)LaaM;
    .registers 2
    .parameter

    .prologue
    .line 24
    const-class v0, LaaM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaaM;

    return-object v0
.end method

.method public static values()[LaaM;
    .registers 1

    .prologue
    .line 24
    sget-object v0, LaaM;->a:[LaaM;

    invoke-virtual {v0}, [LaaM;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaaM;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, LaaM;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 63
    iget v0, p0, LaaM;->b:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LZK;)Z
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 67
    iget v0, p0, LaaM;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
