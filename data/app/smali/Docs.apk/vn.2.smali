.class public Lvn;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lvm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lvm;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2122
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 2123
    return-void
.end method

.method static a(Lvw;J)Lvn;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2118
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lvn;

    invoke-direct {v0, p0, p1, p2}, Lvn;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 2152
    invoke-virtual {p0}, Lvn;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(J)I

    move-result v0

    .line 2154
    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2128
    invoke-virtual {p0}, Lvn;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 2130
    return-object v0
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 2136
    invoke-virtual {p0}, Lvn;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(J)Z

    move-result v0

    .line 2138
    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2144
    invoke-virtual {p0}, Lvn;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 2146
    return-object v0
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 2168
    invoke-virtual {p0}, Lvn;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(J)Z

    move-result v0

    .line 2170
    return v0
.end method

.method public c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2160
    invoke-virtual {p0}, Lvn;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(J)Ljava/lang/String;

    move-result-object v0

    .line 2162
    return-object v0
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 2184
    invoke-virtual {p0}, Lvn;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(J)Z

    move-result v0

    .line 2186
    return v0
.end method
