.class public LHB;
.super Ljava/lang/Object;
.source "SheetTabBarFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .registers 3

    .prologue
    .line 184
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 185
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V

    .line 186
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;Z)Z

    .line 189
    :cond_13
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)I

    move-result v0

    iget-object v1, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    if-eq v0, v1, :cond_48

    .line 190
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    iget-object v1, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;I)I

    .line 191
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LHG;

    move-result-object v0

    invoke-interface {v0}, LHG;->a()V

    .line 192
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V

    .line 193
    iget-object v0, p0, LHB;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;Z)Z

    .line 195
    :cond_48
    return-void
.end method
