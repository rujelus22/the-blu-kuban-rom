.class public Lxp;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Trix.java"

# interfaces
.implements Lxo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lxl;",
        ">;",
        "Lxo;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lxl;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3158
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 3159
    return-void
.end method

.method static a(Lxl;J)Lxp;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3154
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lxp;

    invoke-direct {v0, p0, p1, p2}, Lxp;-><init>(Lxl;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a(D)I
    .registers 5
    .parameter

    .prologue
    .line 3214
    invoke-virtual {p0}, Lxp;->a()J

    move-result-wide v0

    .line 3215
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->a(JD)I

    move-result v0

    .line 3218
    return v0
.end method

.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3200
    invoke-virtual {p0}, Lxp;->a()J

    move-result-wide v0

    .line 3201
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->a(JII)V

    .line 3202
    return-void
.end method

.method public a(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3186
    invoke-virtual {p0}, Lxp;->a()J

    move-result-wide v0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    .line 3187
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->a(JIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3188
    return-void
.end method
