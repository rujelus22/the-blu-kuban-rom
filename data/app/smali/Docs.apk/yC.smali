.class LyC;
.super Ljava/lang/Object;
.source "KixJSVM.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LyB;

.field final synthetic a:[B


# direct methods
.method constructor <init>(LyB;[BLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 459
    iput-object p1, p0, LyC;->a:LyB;

    iput-object p2, p0, LyC;->a:[B

    iput-object p3, p0, LyC;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 462
    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Lyz;

    #getter for: Lyz;->b:Z
    invoke-static {v1}, Lyz;->access$800(Lyz;)Z

    move-result v1

    if-nez v1, :cond_b3

    .line 463
    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Lyz;

    #getter for: Lyz;->d:Z
    invoke-static {v1}, Lyz;->access$900(Lyz;)Z

    move-result v1

    if-eqz v1, :cond_23

    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Lyz;

    #getter for: Lyz;->a:LKt;
    invoke-static {v1}, Lyz;->access$1000(Lyz;)LKt;

    move-result-object v1

    invoke-virtual {v1}, LKt;->a()Z

    move-result v1

    if-eqz v1, :cond_33

    :cond_23
    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Lyz;

    #getter for: Lyz;->a:LKS;
    invoke-static {v1}, Lyz;->access$1100(Lyz;)LKS;

    move-result-object v1

    const-string v2, "kixEnableJsFlags"

    invoke-interface {v1, v2, v0}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_34

    :cond_33
    const/4 v0, 0x1

    .line 465
    :cond_34
    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Lyz;

    new-instance v2, Lvw;

    iget-object v3, p0, LyC;->a:[B

    iget-object v4, p0, LyC;->a:LyB;

    iget-object v4, v4, LyB;->a:Lyz;

    if-eqz v0, :cond_b1

    iget-object v0, p0, LyC;->a:Ljava/lang/String;

    :goto_44
    invoke-direct {v2, v3, v4, v0}, Lvw;-><init>([BLuU;Ljava/lang/String;)V

    #setter for: Lyz;->a:Lvw;
    invoke-static {v1, v2}, Lyz;->access$002(Lyz;Lvw;)Lvw;

    .line 466
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Time to initialize the model "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, LyC;->a:LyB;

    iget-wide v4, v4, LyB;->a:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    iget-object v0, p0, LyC;->a:LyB;

    iget-object v0, v0, LyB;->a:Lyz;

    #getter for: Lyz;->a:LeQ;
    invoke-static {v0}, Lyz;->access$1300(Lyz;)LeQ;

    move-result-object v0

    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Lyz;

    #getter for: Lyz;->a:Ljava/lang/Object;
    invoke-static {v1}, Lyz;->access$1200(Lyz;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "kixModelInitTime"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, LyC;->a:LyB;

    iget-object v0, v0, LyB;->a:Lyz;

    #getter for: Lyz;->a:LeQ;
    invoke-static {v0}, Lyz;->access$1300(Lyz;)LeQ;

    move-result-object v0

    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Lyz;

    #getter for: Lyz;->b:Ljava/lang/Object;
    invoke-static {v1}, Lyz;->access$1400(Lyz;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LeQ;->a(Ljava/lang/Object;)V

    .line 471
    iget-object v0, p0, LyC;->a:LyB;

    iget-object v0, v0, LyB;->a:Lyz;

    iget-object v1, p0, LyC;->a:LyB;

    iget-object v1, v1, LyB;->a:Ljava/lang/String;

    iget-object v2, p0, LyC;->a:LyB;

    iget-object v2, v2, LyB;->b:Ljava/lang/String;

    iget-object v3, p0, LyC;->a:LyB;

    iget-object v3, v3, LyB;->c:Ljava/lang/String;

    iget-object v4, p0, LyC;->a:LyB;

    iget-boolean v4, v4, LyB;->a:Z

    #calls: Lyz;->reallyOpenDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2, v3, v4}, Lyz;->access$1500(Lyz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 475
    :goto_b0
    return-void

    .line 465
    :cond_b1
    const/4 v0, 0x0

    goto :goto_44

    .line 473
    :cond_b3
    const-string v0, "Model"

    const-string v1, "Ignoring JS fetching callback as JSVM already deleted."

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b0
.end method
