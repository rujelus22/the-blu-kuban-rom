.class LDF;
.super LDn;
.source "SpannableGroup.java"

# interfaces
.implements LDz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeOwner:",
        "Ljava/lang/Object;",
        ">",
        "LDn",
        "<",
        "LDG",
        "<TNodeOwner;>;>;",
        "LDz",
        "<TNodeOwner;>;"
    }
.end annotation


# instance fields
.field private final a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<*TNodeOwner;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LDA;LKh;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDA",
            "<*TNodeOwner;>;",
            "LKh",
            "<",
            "LDG",
            "<TNodeOwner;>;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p2}, LDn;-><init>(LKh;)V

    .line 48
    iput-object p1, p0, LDF;->a:LDA;

    .line 49
    return-void
.end method


# virtual methods
.method public a()LDJ;
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, LDF;->a:LDA;

    invoke-virtual {v0}, LDA;->a()LDJ;

    move-result-object v0

    return-object v0
.end method

.method public a(LDG;)LDz;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDG",
            "<TNodeOwner;>;)",
            "LDz",
            "<TNodeOwner;>;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, LDF;

    iget-object v1, p0, LDF;->a:LDA;

    iget-object v2, p0, LDF;->a:LKh;

    const/4 v3, 0x0

    invoke-interface {v2, p1, v3}, LKh;->a(Ljava/lang/Object;I)LKh;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LDF;-><init>(LDA;LKh;)V

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-virtual {p0}, LDF;->b()I

    move-result v0

    invoke-virtual {p0, v1, v0, v1}, LDF;->a(III)V

    .line 79
    iget-object v0, p0, LDF;->a:LKh;

    invoke-interface {v0}, LKh;->a()V

    .line 80
    return-void
.end method

.method public a(III)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, LDF;->a:LKh;

    sub-int v1, p3, p2

    invoke-interface {v0, v1}, LKh;->a(I)V

    .line 59
    invoke-virtual {p0}, LDF;->a()I

    move-result v0

    add-int/2addr v0, p1

    .line 60
    iget-object v1, p0, LDF;->a:LDA;

    invoke-virtual {v1, v0, p2, p3}, LDA;->a(III)V

    .line 61
    iget-object v1, p0, LDF;->a:LDA;

    invoke-virtual {v1}, LDA;->a()LDz;

    move-result-object v1

    invoke-interface {v1, v0, p2, p3}, LDz;->a(III)V

    .line 62
    return-void
.end method

.method public a(Ljava/util/List;II)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDt;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, LDF;->a()I

    move-result v0

    .line 86
    iget-object v1, p0, LDF;->a:LDA;

    add-int v2, v0, p2

    add-int v3, v0, p3

    invoke-virtual {v1, p1, v2, v3}, LDA;->a(Ljava/util/List;II)V

    .line 87
    iget-object v1, p0, LDF;->a:LDA;

    invoke-virtual {v1}, LDA;->a()LDz;

    move-result-object v1

    add-int v2, v0, p2

    add-int/2addr v0, p3

    invoke-interface {v1, p1, v2, v0}, LDz;->a(Ljava/util/List;II)V

    .line 89
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 102
    if-eqz p1, :cond_8

    .line 103
    iget-object v0, p0, LDF;->a:LDA;

    invoke-static {v0}, LDA;->a(LDA;)V

    .line 107
    :goto_7
    return-void

    .line 105
    :cond_8
    iget-object v0, p0, LDF;->a:LDA;

    invoke-static {v0}, LDA;->b(LDA;)V

    goto :goto_7
.end method

.method public b(Ljava/util/List;II)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, LDF;->a()I

    move-result v0

    .line 95
    iget-object v1, p0, LDF;->a:LDA;

    add-int v2, v0, p2

    add-int v3, v0, p3

    invoke-virtual {v1, p1, v2, v3}, LDA;->b(Ljava/util/List;II)V

    .line 96
    iget-object v1, p0, LDF;->a:LDA;

    invoke-virtual {v1}, LDA;->a()LDz;

    move-result-object v1

    add-int v2, v0, p2

    add-int/2addr v0, p3

    invoke-interface {v1, p1, v2, v0}, LDz;->b(Ljava/util/List;II)V

    .line 98
    return-void
.end method

.method public c()I
    .registers 3

    .prologue
    .line 53
    iget-object v0, p0, LDF;->a:LDA;

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    invoke-virtual {p0}, LDF;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
