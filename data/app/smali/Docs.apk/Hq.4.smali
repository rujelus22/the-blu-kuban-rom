.class public LHq;
.super Ljava/lang/Object;
.source "TrixJSVM.java"

# interfaces
.implements LCW;
.implements LKb;
.implements LuU;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;


# instance fields
.field private final a:LJY;

.field private final a:LKl;

.field private final a:LKt;

.field private final a:LNS;

.field private final a:LNj;

.field private final a:LZS;

.field private final a:Landroid/os/Handler;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LCQ;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lxl;

.field private a:Lxo;

.field private a:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 45
    const-string v0, "TrixJSVM"

    sput-object v0, LHq;->a:Ljava/lang/String;

    .line 46
    const-string v0, "trix_mobilenative_android.js"

    sput-object v0, LHq;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Laoz;LNj;LNS;LZS;LKt;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LNj;",
            "LNS;",
            "LZS;",
            "LKt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LHq;->a:Landroid/os/Handler;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LHq;->a:Ljava/util/Map;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, LHq;->a:Z

    .line 71
    iput-object p1, p0, LHq;->a:Laoz;

    .line 72
    iput-object p2, p0, LHq;->a:LNj;

    .line 73
    iput-object p3, p0, LHq;->a:LNS;

    .line 74
    iput-object p4, p0, LHq;->a:LZS;

    .line 75
    iput-object p5, p0, LHq;->a:LKt;

    .line 79
    new-instance v0, LKl;

    iget-object v1, p0, LHq;->a:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v2

    sget-object v3, LKq;->a:LKq;

    invoke-direct {v0, v1, v2, v3}, LKl;-><init>(Landroid/os/Handler;Landroid/os/MessageQueue;LKq;)V

    iput-object v0, p0, LHq;->a:LKl;

    .line 81
    new-instance v0, LJY;

    iget-object v1, p0, LHq;->a:LKl;

    invoke-direct {v0, p0, v1}, LJY;-><init>(LKb;LKl;)V

    iput-object v0, p0, LHq;->a:LJY;

    .line 82
    return-void
.end method

.method static synthetic a(LHq;)Lxl;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LHq;->a:Lxl;

    return-object v0
.end method

.method private a(Ljava/lang/String;)[B
    .registers 4
    .parameter

    .prologue
    .line 117
    iget-object v1, p0, LHq;->a:LZS;

    iget-object v0, p0, LHq;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-interface {v1, v0}, LZS;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()LKl;
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, LHq;->a:LKl;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 125
    iget-boolean v0, p0, LHq;->a:Z

    if-eqz v0, :cond_5

    .line 131
    :goto_4
    return-void

    .line 129
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LHq;->a:Z

    .line 130
    iget-object v0, p0, LHq;->a:LKl;

    invoke-virtual {v0}, LKl;->b()V

    goto :goto_4
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LHs;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, LHq;->c:Ljava/lang/String;

    .line 89
    iput-object p2, p0, LHq;->d:Ljava/lang/String;

    .line 92
    :try_start_4
    sget-object v0, LHq;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, LHq;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 93
    new-instance v1, Lxl;

    const-string v2, ""

    invoke-direct {v1, v0, p0, v2}, Lxl;-><init>([BLuU;Ljava/lang/String;)V

    iput-object v1, p0, LHq;->a:Lxl;

    .line 94
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->b()V

    .line 96
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->a()Lxn;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Lxm;->a()Lxo;

    move-result-object v0

    iput-object v0, p0, LHq;->a:Lxo;

    .line 98
    iget-object v0, p0, LHq;->a:Lxo;

    invoke-interface {v0}, Lxo;->b()V

    .line 101
    iget-object v0, p0, LHq;->a:Landroid/os/Handler;

    new-instance v1, LHr;

    invoke-direct {v1, p0, p3}, LHr;-><init>(LHq;LHs;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_33
    .catchall {:try_start_4 .. :try_end_33} :catchall_44
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_33} :catch_3d

    .line 110
    iget-object v0, p0, LHq;->a:Lxl;

    if-eqz v0, :cond_3c

    .line 111
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->c()V

    .line 114
    :cond_3c
    return-void

    .line 107
    :catch_3d
    move-exception v0

    .line 108
    :try_start_3e
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_44
    .catchall {:try_start_3e .. :try_end_44} :catchall_44

    .line 110
    :catchall_44
    move-exception v0

    iget-object v1, p0, LHq;->a:Lxl;

    if-eqz v1, :cond_4e

    .line 111
    iget-object v1, p0, LHq;->a:Lxl;

    invoke-virtual {v1}, Lxl;->c()V

    :cond_4e
    throw v0
.end method

.method public abortHttpRequest(I)V
    .registers 4
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, LHq;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCQ;

    .line 205
    if-eqz v0, :cond_11

    .line 206
    invoke-virtual {v0}, LCQ;->a()V

    .line 208
    :cond_11
    iget-object v0, p0, LHq;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 134
    iget-boolean v0, p0, LHq;->a:Z

    if-nez v0, :cond_5

    .line 140
    :goto_4
    return-void

    .line 138
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, LHq;->a:Z

    .line 139
    iget-object v0, p0, LHq;->a:LKl;

    invoke-virtual {v0}, LKl;->a()V

    goto :goto_4
.end method

.method public c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 143
    sget-object v0, LHq;->a:Ljava/lang/String;

    const-string v1, "JSVM deleted."

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, LHq;->a:Z

    .line 146
    iget-object v0, p0, LHq;->a:Lxl;

    if-nez v0, :cond_10

    .line 169
    :goto_f
    return-void

    .line 150
    :cond_10
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->b()V

    .line 152
    :try_start_15
    iget-object v0, p0, LHq;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCQ;

    .line 153
    invoke-virtual {v0}, LCQ;->a()V
    :try_end_2e
    .catchall {:try_start_15 .. :try_end_2e} :catchall_2f

    goto :goto_1f

    .line 164
    :catchall_2f
    move-exception v0

    iget-object v1, p0, LHq;->a:Lxl;

    invoke-virtual {v1}, Lxl;->c()V

    throw v0

    .line 155
    :cond_36
    :try_start_36
    iget-object v0, p0, LHq;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 157
    iget-object v0, p0, LHq;->a:LJY;

    invoke-virtual {v0}, LJY;->a()V

    .line 159
    iget-object v0, p0, LHq;->a:Lxo;

    if-eqz v0, :cond_4c

    .line 160
    iget-object v0, p0, LHq;->a:Lxo;

    invoke-interface {v0}, Lxo;->a()V

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, LHq;->a:Lxo;
    :try_end_4c
    .catchall {:try_start_36 .. :try_end_4c} :catchall_2f

    .line 164
    :cond_4c
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->c()V

    .line 167
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->a()V

    .line 168
    iput-object v2, p0, LHq;->a:Lxl;

    goto :goto_f
.end method

.method public getTime()D
    .registers 3
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, LHq;->a:LJY;

    invoke-virtual {v0}, LJY;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method public invokeTimer(D)I
    .registers 5
    .parameter

    .prologue
    .line 242
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->b()V

    .line 244
    :try_start_5
    iget-object v0, p0, LHq;->a:Lxo;

    invoke-interface {v0, p1, p2}, Lxo;->a(D)I
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_11

    move-result v0

    .line 246
    iget-object v1, p0, LHq;->a:Lxl;

    invoke-virtual {v1}, Lxl;->c()V

    return v0

    :catchall_11
    move-exception v0

    iget-object v1, p0, LHq;->a:Lxl;

    invoke-virtual {v1}, Lxl;->c()V

    throw v0
.end method

.method public onDataReceived(ILCX;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->b()V

    .line 177
    :try_start_5
    iget-object v0, p0, LHq;->a:Lxo;

    invoke-virtual {p2}, LCX;->ordinal()I

    move-result v2

    move v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lxo;->a(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_5 .. :try_end_13} :catchall_19

    .line 180
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->c()V

    .line 182
    return-void

    .line 180
    :catchall_19
    move-exception v0

    iget-object v1, p0, LHq;->a:Lxl;

    invoke-virtual {v1}, Lxl;->c()V

    throw v0
.end method

.method public onRequestCompleted(I)V
    .registers 4
    .parameter

    .prologue
    .line 198
    iget-object v0, p0, LHq;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    return-void
.end method

.method public onRequestFailed(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->b()V

    .line 188
    :try_start_5
    iget-object v0, p0, LHq;->a:Lxo;

    invoke-interface {v0, p1, p2}, Lxo;->a(II)V

    .line 189
    iget-object v0, p0, LHq;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, LHq;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP request failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b
    .catchall {:try_start_5 .. :try_end_2b} :catchall_31

    .line 192
    iget-object v0, p0, LHq;->a:Lxl;

    invoke-virtual {v0}, Lxl;->c()V

    .line 194
    return-void

    .line 192
    :catchall_31
    move-exception v0

    iget-object v1, p0, LHq;->a:Lxl;

    invoke-virtual {v1}, Lxl;->c()V

    throw v0
.end method

.method public sendHttpRequest(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 215
    sget-object v0, LHq;->a:Ljava/lang/String;

    const-string v1, "Sending HTTP request"

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :try_start_7
    new-instance v0, LCQ;

    iget-object v1, p0, LHq;->a:LNj;

    const-string v2, "wise"

    iget-object v3, p0, LHq;->a:LNS;

    iget-object v4, p0, LHq;->c:Ljava/lang/String;

    iget-object v6, p0, LHq;->a:LKl;

    iget-object v7, p0, LHq;->a:LKt;

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, LCQ;-><init>(LNj;Ljava/lang/String;LNS;Ljava/lang/String;LCW;LKl;LKt;)V

    .line 221
    iget-object v1, p0, LHq;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v1, p0, LHq;->d:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, LCQ;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_2d} :catch_2e

    .line 226
    :goto_2d
    return-void

    .line 223
    :catch_2e
    move-exception v0

    .line 224
    sget-object v1, LHq;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SendHttpRequest: Exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2d
.end method

.method public startTimer(I)V
    .registers 3
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, LHq;->a:LJY;

    invoke-virtual {v0, p1}, LJY;->a(I)V

    .line 238
    return-void
.end method
