.class public LGt;
.super Ljava/lang/Object;
.source "Touch.java"


# direct methods
.method public static a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 249
    invoke-static {p1}, LGt;->a(Landroid/text/Spannable;)[LGu;

    move-result-object v0

    .line 250
    array-length v1, v0

    if-lez v1, :cond_d

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, LGu;->a:I

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public static a(Lcom/google/android/apps/docs/editors/text/TextView;LEj;)Landroid/util/Pair;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/editors/text/TextView;",
            "LEj;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->j()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v1

    sub-int v2, v1, v0

    .line 49
    invoke-interface {p1}, LEj;->a()I

    move-result v3

    .line 51
    const/4 v1, 0x0

    .line 52
    sub-int v0, v3, v2

    .line 54
    if-le v2, v3, :cond_1d

    .line 55
    sub-int v0, v3, v2

    div-int/lit8 v0, v0, 0x2

    move v1, v0

    .line 59
    :cond_1d
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private static a(Landroid/text/Spannable;LGu;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 292
    const/16 v0, 0x11

    invoke-interface {p0, p1, v1, v1, v0}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 293
    return-void
.end method

.method public static a(Lcom/google/android/apps/docs/editors/text/TextView;LEj;II)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->j()Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->j()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()I

    move-result v2

    add-int/2addr v0, v2

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v2

    sub-int v5, v2, v0

    .line 100
    invoke-interface {p1, p3}, LEj;->a(I)I

    move-result v2

    .line 101
    invoke-interface {p1, v2}, LEj;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v6

    .line 102
    invoke-interface {p1, v2}, LEj;->o(I)I

    move-result v0

    if-lez v0, :cond_5a

    const/4 v0, 0x1

    .line 105
    :goto_25
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->l()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()I

    move-result v4

    add-int/2addr v3, v4

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v4

    add-int/2addr v4, p3

    sub-int v3, v4, v3

    invoke-interface {p1, v3}, LEj;->a(I)I

    move-result v7

    .line 109
    const v3, 0x7fffffff

    move v8, v1

    move v1, v3

    move v3, v8

    .line 112
    :goto_3f
    if-gt v2, v7, :cond_5c

    .line 113
    int-to-float v1, v1

    invoke-interface {p1, v2}, LEj;->c(I)F

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-int v4, v1

    .line 114
    int-to-float v1, v3

    invoke-interface {p1, v2}, LEj;->d(I)F

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v3, v1

    .line 112
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v4

    goto :goto_3f

    :cond_5a
    move v0, v1

    .line 102
    goto :goto_25

    .line 117
    :cond_5c
    sub-int v2, v3, v1

    .line 119
    if-ge v2, v5, :cond_92

    .line 120
    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    if-ne v6, v3, :cond_85

    .line 121
    sub-int v0, v5, v2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    :goto_6a
    move p2, v0

    .line 144
    :cond_6b
    :goto_6b
    invoke-static {p0, p1}, LGt;->b(Lcom/google/android/apps/docs/editors/text/TextView;LEj;)Landroid/util/Pair;

    move-result-object v1

    .line 145
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p3, v0, :cond_c9

    .line 146
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p3

    .line 151
    :cond_81
    :goto_81
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/docs/editors/text/TextView;->scrollTo(II)V

    .line 152
    return-void

    .line 122
    :cond_85
    if-eqz v0, :cond_90

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    if-ne v6, v0, :cond_90

    .line 125
    sub-int v0, v5, v2

    sub-int v0, v1, v0

    goto :goto_6a

    :cond_90
    move v0, v1

    .line 127
    goto :goto_6a

    .line 130
    :cond_92
    sub-int v0, v3, v5

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 131
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_6a

    .line 134
    :cond_9d
    invoke-static {p0, p1}, LGt;->a(Lcom/google/android/apps/docs/editors/text/TextView;LEj;)Landroid/util/Pair;

    move-result-object v1

    .line 137
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p2, v0, :cond_b5

    .line 138
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move p2, v0

    goto :goto_6b

    .line 139
    :cond_b5
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le p2, v0, :cond_6b

    .line 140
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move p2, v0

    goto :goto_6b

    .line 147
    :cond_c9
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le p3, v0, :cond_81

    .line 148
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_81
.end method

.method private static a(Lcom/google/android/apps/docs/editors/text/TextView;LGE;F)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 261
    invoke-virtual {p1}, LGE;->a()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p2

    float-to-int v6, v0

    .line 262
    invoke-virtual {p1}, LGE;->a()I

    move-result v7

    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->j()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()I

    move-result v2

    sub-int v8, v0, v2

    move-object v0, p1

    move-object v2, v1

    move v4, v3

    move-object v5, v1

    .line 267
    invoke-virtual/range {v0 .. v5}, LGE;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    sub-int/2addr v0, v8

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 269
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 271
    invoke-virtual {p1, v0}, LGE;->a(I)V

    .line 273
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/Object;)V

    .line 274
    if-eq v0, v7, :cond_38

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->cancelLongPress()V

    .line 277
    :cond_38
    return-void
.end method

.method public static a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 162
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_132

    .line 245
    :cond_a
    :goto_a
    return v0

    .line 164
    :pswitch_b
    invoke-static {p1}, LGt;->a(Landroid/text/Spannable;)[LGu;

    move-result-object v1

    .line 166
    :goto_f
    array-length v2, v1

    if-ge v0, v2, :cond_1a

    .line 167
    aget-object v2, v1, v0

    invoke-static {p1, v2}, LGt;->b(Landroid/text/Spannable;LGu;)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 170
    :cond_1a
    new-instance v0, LGu;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v5

    invoke-direct {v0, v1, v2, v4, v5}, LGu;-><init>(FFII)V

    invoke-static {p1, v0}, LGt;->a(Landroid/text/Spannable;LGu;)V

    move v0, v3

    .line 172
    goto :goto_a

    .line 175
    :pswitch_34
    invoke-static {p1}, LGt;->a(Landroid/text/Spannable;)[LGu;

    move-result-object v2

    move v1, v0

    .line 177
    :goto_39
    array-length v4, v2

    if-ge v1, v4, :cond_44

    .line 178
    aget-object v4, v2, v1

    invoke-static {p1, v4}, LGt;->b(Landroid/text/Spannable;LGu;)V

    .line 177
    add-int/lit8 v1, v1, 0x1

    goto :goto_39

    .line 181
    :cond_44
    array-length v1, v2

    if-lez v1, :cond_a

    aget-object v1, v2, v0

    iget-boolean v1, v1, LGu;->b:Z

    if-eqz v1, :cond_a

    move v0, v3

    .line 182
    goto :goto_a

    .line 188
    :pswitch_4f
    invoke-static {p1}, LGt;->a(Landroid/text/Spannable;)[LGu;

    move-result-object v4

    .line 189
    array-length v1, v4

    if-lez v1, :cond_a

    .line 190
    aget-object v1, v4, v0

    iget-boolean v1, v1, LGu;->a:Z

    if-nez v1, :cond_90

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 193
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    aget-object v5, v4, v0

    iget v5, v5, LGu;->a:F

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float v5, v1

    cmpl-float v2, v2, v5

    if-gez v2, :cond_8c

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    aget-object v5, v4, v0

    iget v5, v5, LGu;->b:F

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float v1, v1

    cmpl-float v1, v2, v1

    if-ltz v1, :cond_90

    .line 195
    :cond_8c
    aget-object v1, v4, v0

    iput-boolean v3, v1, LGu;->a:Z

    .line 199
    :cond_90
    aget-object v1, v4, v0

    iget-boolean v1, v1, LGu;->a:Z

    if-eqz v1, :cond_a

    .line 200
    aget-object v1, v4, v0

    iput-boolean v3, v1, LGu;->b:Z

    .line 201
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_b0

    invoke-static {p1, v3}, LFY;->a(Ljava/lang/CharSequence;I)I

    move-result v1

    if-eq v1, v3, :cond_b0

    const/16 v1, 0x800

    invoke-static {p1, v1}, LFY;->a(Ljava/lang/CharSequence;I)I

    move-result v1

    if-eqz v1, :cond_11c

    :cond_b0
    move v1, v3

    .line 208
    :goto_b1
    if-eqz v1, :cond_11e

    .line 211
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    aget-object v2, v4, v0

    iget v2, v2, LGu;->a:F

    sub-float v2, v1, v2

    .line 212
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    aget-object v5, v4, v0

    iget v5, v5, LGu;->b:F

    sub-float/2addr v1, v5

    .line 217
    :goto_c6
    aget-object v5, v4, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iput v6, v5, LGu;->a:F

    .line 218
    aget-object v0, v4, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, v0, LGu;->b:F

    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v0

    float-to-int v4, v2

    add-int/2addr v0, v4

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v4

    float-to-int v5, v1

    add-int/2addr v4, v5

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v5

    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v6

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v7

    invoke-static {p0, v7, v0, v4}, LGt;->a(Lcom/google/android/apps/docs/editors/text/TextView;LEj;II)V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v0

    if-ne v5, v0, :cond_105

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v0

    if-ne v6, v0, :cond_105

    cmpl-float v0, v2, v8

    if-nez v0, :cond_105

    cmpl-float v0, v1, v8

    if-eqz v0, :cond_108

    .line 231
    :cond_105
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->cancelLongPress()V

    .line 234
    :cond_108
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LGE;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_119

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v1

    sub-int/2addr v1, v5

    int-to-float v1, v1

    sub-float v1, v2, v1

    invoke-static {p0, v0, v1}, LGt;->a(Lcom/google/android/apps/docs/editors/text/TextView;LGE;F)V

    :cond_119
    move v0, v3

    .line 240
    goto/16 :goto_a

    :cond_11c
    move v1, v0

    .line 201
    goto :goto_b1

    .line 214
    :cond_11e
    aget-object v1, v4, v0

    iget v1, v1, LGu;->a:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float v2, v1, v2

    .line 215
    aget-object v1, v4, v0

    iget v1, v1, LGu;->b:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v1, v5

    goto :goto_c6

    .line 162
    :pswitch_data_132
    .packed-switch 0x0
        :pswitch_b
        :pswitch_34
        :pswitch_4f
    .end packed-switch
.end method

.method private static a(Landroid/text/Spannable;)[LGu;
    .registers 3
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 285
    const-class v0, LGu;

    invoke-interface {p0, v1, v1, v0}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGu;

    return-object v0
.end method

.method public static b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 254
    invoke-static {p1}, LGt;->a(Landroid/text/Spannable;)[LGu;

    move-result-object v0

    .line 255
    array-length v1, v0

    if-lez v1, :cond_d

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, LGu;->b:I

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public static b(Lcom/google/android/apps/docs/editors/text/TextView;LEj;)Landroid/util/Pair;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/editors/text/TextView;",
            "LEj;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->l()I

    move-result v2

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()I

    move-result v1

    sub-int v3, v0, v1

    .line 77
    const/4 v1, 0x0

    .line 78
    invoke-interface {p1}, LEj;->c()I

    move-result v0

    add-int/2addr v0, v2

    sub-int/2addr v0, v3

    .line 80
    invoke-interface {p1}, LEj;->c()I

    move-result v4

    add-int/2addr v4, v2

    if-le v3, v4, :cond_25

    .line 81
    invoke-interface {p1}, LEj;->c()I

    move-result v0

    add-int/2addr v0, v2

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    move v1, v0

    .line 85
    :cond_25
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private static b(Landroid/text/Spannable;LGu;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 299
    invoke-interface {p0, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 300
    return-void
.end method
