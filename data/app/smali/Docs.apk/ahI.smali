.class final LahI;
.super LahJ;
.source "LocalCache.java"

# interfaces
.implements Lahz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LahJ",
        "<TK;TV;>;",
        "Lahz",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field a:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile b:J

.field b:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILahz;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v1, 0x7fffffffffffffffL

    .line 1258
    invoke-direct {p0, p1, p2, p3}, LahJ;-><init>(Ljava/lang/Object;ILahz;)V

    .line 1263
    iput-wide v1, p0, LahI;->a:J

    .line 1275
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahI;->a:Lahz;

    .line 1288
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahI;->b:Lahz;

    .line 1303
    iput-wide v1, p0, LahI;->b:J

    .line 1315
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahI;->c:Lahz;

    .line 1328
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahI;->d:Lahz;

    .line 1259
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 1267
    iget-wide v0, p0, LahI;->a:J

    return-wide v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 1272
    iput-wide p1, p0, LahI;->a:J

    .line 1273
    return-void
.end method

.method public a(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1285
    iput-object p1, p0, LahI;->a:Lahz;

    .line 1286
    return-void
.end method

.method public b()J
    .registers 3

    .prologue
    .line 1307
    iget-wide v0, p0, LahI;->b:J

    return-wide v0
.end method

.method public b()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1280
    iget-object v0, p0, LahI;->a:Lahz;

    return-object v0
.end method

.method public b(J)V
    .registers 3
    .parameter

    .prologue
    .line 1312
    iput-wide p1, p0, LahI;->b:J

    .line 1313
    return-void
.end method

.method public b(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1298
    iput-object p1, p0, LahI;->b:Lahz;

    .line 1299
    return-void
.end method

.method public c()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1293
    iget-object v0, p0, LahI;->b:Lahz;

    return-object v0
.end method

.method public c(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1325
    iput-object p1, p0, LahI;->c:Lahz;

    .line 1326
    return-void
.end method

.method public d()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1320
    iget-object v0, p0, LahI;->c:Lahz;

    return-object v0
.end method

.method public d(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1338
    iput-object p1, p0, LahI;->d:Lahz;

    .line 1339
    return-void
.end method

.method public e()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1333
    iget-object v0, p0, LahI;->d:Lahz;

    return-object v0
.end method
