.class LfF;
.super Ljava/lang/Object;
.source "DocsPreferencesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

.field final synthetic a:LfC;


# direct methods
.method constructor <init>(LfC;Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 547
    iput-object p1, p0, LfF;->a:LfC;

    iput-object p2, p0, LfF;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 550
    iget-object v0, p0, LfF;->a:LfC;

    iget-object v0, v0, LfC;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->l(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;

    move-result-object v0

    iget-object v1, p0, LfF;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 552
    iget-object v0, p0, LfF;->a:LfC;

    invoke-static {v0}, LfC;->a(LfC;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 553
    iget-object v0, p0, LfF;->a:LfC;

    invoke-static {v0}, LfC;->b(LfC;)V

    .line 555
    :cond_1a
    iget-object v0, p0, LfF;->a:LfC;

    iget-object v0, v0, LfC;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 556
    iget-object v0, p0, LfF;->a:LfC;

    iget-object v0, v0, LfC;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, LfF;->a:LfC;

    iget-object v1, v1, LfC;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-static {v1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 558
    :cond_3b
    iget-object v0, p0, LfF;->a:LfC;

    invoke-static {v0}, LfC;->c(LfC;)V

    .line 559
    return-void
.end method
