.class public LDK;
.super Ljava/lang/Object;
.source "SpannableTreeInfoImpl.java"

# interfaces
.implements LDJ;


# instance fields
.field private final a:LDG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDG",
            "<*>;"
        }
    .end annotation
.end field

.field private final a:LDy;

.field private final a:Landroid/text/Editable;


# direct methods
.method public constructor <init>(LDG;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDG",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, LDK;->a:Landroid/text/Editable;

    .line 16
    new-instance v0, LDy;

    invoke-direct {v0}, LDy;-><init>()V

    iput-object v0, p0, LDK;->a:LDy;

    .line 20
    iput-object p1, p0, LDK;->a:LDG;

    .line 21
    return-void
.end method


# virtual methods
.method public a()LDG;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, LDK;->a:LDG;

    return-object v0
.end method

.method public a()LDy;
    .registers 2

    .prologue
    .line 30
    iget-object v0, p0, LDK;->a:LDy;

    return-object v0
.end method

.method public a()Landroid/text/Editable;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, LDK;->a:Landroid/text/Editable;

    return-object v0
.end method
