.class public LvA;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lvz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lvz;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2469
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 2470
    return-void
.end method

.method static a(Lvw;J)LvA;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2465
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, LvA;

    invoke-direct {v0, p0, p1, p2}, LvA;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()Lwp;
    .registers 4

    .prologue
    .line 2483
    invoke-virtual {p0}, LvA;->a()J

    move-result-wide v0

    .line 2484
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(J)J

    move-result-wide v1

    .line 2487
    iget-object v0, p0, LvA;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lws;->a(Lvw;J)Lws;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 5

    .prologue
    .line 2473
    iget-object v0, p0, LvA;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->a(Lvw;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 2474
    iget-object v0, p0, LvA;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LvA;->a()J

    move-result-wide v1

    const/4 v3, 0x5

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->a(Lvw;Z)Z

    .line 2477
    iget-object v0, p0, LvA;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->b(Lvw;Z)Z

    .line 2479
    :cond_22
    iget-object v0, p0, LvA;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->b(Lvw;)Z

    move-result v0

    return v0
.end method
