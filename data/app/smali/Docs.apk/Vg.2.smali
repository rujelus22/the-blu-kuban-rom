.class public LVg;
.super Ljava/lang/Object;
.source "InMemoryFileSource.java"

# interfaces
.implements LVb;


# instance fields
.field private final a:LVb;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LVi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LVb;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LVg;->a:Ljava/util/Map;

    .line 43
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVb;

    iput-object v0, p0, LVg;->a:LVb;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, LVg;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 64
    iget-object v0, p0, LVg;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVi;

    invoke-static {v0}, LVi;->a(LVi;)Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_14
    return-object v0

    :cond_15
    iget-object v0, p0, LVg;->a:LVb;

    invoke-interface {v0, p1}, LVb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_14
.end method

.method public a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, LVg;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 49
    iget-object v0, p0, LVg;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVi;

    invoke-static {v0}, LVi;->a(LVi;)[B

    move-result-object v0

    .line 50
    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V

    .line 54
    :goto_1d
    return-void

    .line 52
    :cond_1e
    iget-object v0, p0, LVg;->a:LVb;

    invoke-interface {v0, p1, p2}, LVb;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    goto :goto_1d
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, LVg;->a:Ljava/util/Map;

    new-instance v1, LVi;

    const/4 v2, 0x0

    invoke-direct {v1, p2, p3, v2}, LVi;-><init>([BLjava/lang/String;LVh;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method
