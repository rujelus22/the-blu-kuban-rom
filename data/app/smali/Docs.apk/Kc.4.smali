.class public LKc;
.super Ljava/lang/Object;
.source "JniIdleQueue.java"


# instance fields
.field private final a:LKl;


# direct methods
.method private constructor <init>(LKl;)V
    .registers 2
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, LKc;->a:LKl;

    .line 54
    return-void
.end method

.method public static a(LKl;)LIR;
    .registers 3
    .parameter

    .prologue
    .line 48
    new-instance v0, LKc;

    invoke-direct {v0, p0}, LKc;-><init>(LKl;)V

    .line 49
    new-instance v1, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    return-object v1
.end method

.method static synthetic a(LKc;LKd;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1}, LKc;->a(LKd;)V

    return-void
.end method

.method private a(LKd;)V
    .registers 4
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, LKc;->a:LKl;

    sget-object v1, LKs;->c:LKs;

    invoke-virtual {v0, p1, v1}, LKl;->a(LKp;LKs;)V

    .line 64
    return-void
.end method


# virtual methods
.method scheduleIdle(J)V
    .registers 5
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 58
    new-instance v0, LJe;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, LJe;-><init>(JZ)V

    .line 59
    new-instance v1, LKd;

    invoke-direct {v1, p0, v0}, LKd;-><init>(LKc;LJe;)V

    invoke-direct {p0, v1}, LKc;->a(LKd;)V

    .line 60
    return-void
.end method
