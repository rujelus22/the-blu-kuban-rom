.class LRY;
.super Ljava/lang/Object;
.source "PunchWebViewFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic a:LRV;

.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(LRV;IILjava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 316
    iput-object p1, p0, LRY;->a:LRV;

    iput p2, p0, LRY;->a:I

    iput p3, p0, LRY;->b:I

    iput-object p4, p0, LRY;->a:Ljava/lang/String;

    iput-object p5, p0, LRY;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    .line 319
    iget-object v0, p0, LRY;->a:LRV;

    iget-object v0, v0, LRV;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LRY;->a:LRV;

    iget-object v1, v1, LRV;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 321
    iget v0, p0, LRY;->a:I

    invoke-static {v0}, LRT;->a(I)LRT;

    move-result-object v0

    .line 323
    const-string v1, "PunchWebViewFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing onSlideLoadStateChange(slideIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LRY;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", loadState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    sget-object v1, LRT;->b:LRT;

    if-ne v0, v1, :cond_5b

    .line 332
    new-instance v0, LQX;

    iget-object v1, p0, LRY;->a:Ljava/lang/String;

    iget-object v2, p0, LRY;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LQX;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, LRY;->a:LRV;

    iget-object v1, v1, LRV;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    iget v2, p0, LRY;->b:I

    invoke-virtual {v1, v2, v0}, LQV;->a(ILQX;)V

    .line 336
    :cond_5b
    return-void
.end method
