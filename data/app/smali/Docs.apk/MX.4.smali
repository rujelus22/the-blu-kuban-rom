.class LMX;
.super Ljava/lang/Object;
.source "HoneycombActionBarHelper.java"

# interfaces
.implements Landroid/widget/SearchView$OnSuggestionListener;


# instance fields
.field final synthetic a:LMS;

.field final synthetic a:Landroid/view/MenuItem;


# direct methods
.method constructor <init>(LMS;Landroid/view/MenuItem;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 293
    iput-object p1, p0, LMX;->a:LMS;

    iput-object p2, p0, LMX;->a:Landroid/view/MenuItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuggestionClick(I)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 296
    iget-object v1, p0, LMX;->a:LMS;

    invoke-static {v1}, LMS;->a(LMS;)Landroid/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SearchView;->getSuggestionsAdapter()Landroid/widget/CursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 297
    if-eqz v2, :cond_2f

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 298
    const-string v1, "suggest_intent_action"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 299
    const/4 v1, 0x0

    .line 300
    if-ltz v3, :cond_24

    .line 301
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 305
    :cond_24
    if-eqz v1, :cond_30

    const-string v3, "android.intent.action.SEARCH"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_30

    .line 306
    const/4 v0, 0x0

    .line 314
    :cond_2f
    :goto_2f
    return v0

    .line 309
    :cond_30
    const-string v1, "suggest_intent_query"

    invoke-static {v2, v1}, LPI;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 310
    iget-object v2, p0, LMX;->a:LMS;

    iget-object v3, p0, LMX;->a:Landroid/view/MenuItem;

    invoke-static {v2, v1, v3}, LMS;->a(LMS;Ljava/lang/String;Landroid/view/MenuItem;)V

    goto :goto_2f
.end method

.method public onSuggestionSelect(I)Z
    .registers 3
    .parameter

    .prologue
    .line 320
    const/4 v0, 0x0

    return v0
.end method
