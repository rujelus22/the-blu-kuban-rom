.class public final enum LPx;
.super Ljava/lang/Enum;
.source "AppCacheTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPx;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPx;

.field private static final synthetic a:[LPx;

.field public static final enum b:LPx;

.field public static final enum c:LPx;

.field public static final enum d:LPx;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v6, 0xe

    const/4 v7, 0x0

    .line 41
    new-instance v0, LPx;

    const-string v1, "APP_NAME"

    invoke-static {}, LPw;->b()LPw;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "appName"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPx;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPx;->a:LPx;

    .line 44
    new-instance v0, LPx;

    const-string v1, "APP_VERSION"

    invoke-static {}, LPw;->b()LPw;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "appVersion"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPx;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPx;->b:LPx;

    .line 47
    new-instance v0, LPx;

    const-string v1, "MANIFEST_ETAG"

    invoke-static {}, LPw;->b()LPw;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "manifestETag"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LPx;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPx;->c:LPx;

    .line 50
    new-instance v0, LPx;

    const-string v1, "EXPIRY_DATE"

    invoke-static {}, LPw;->b()LPw;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    const/16 v3, 0x17

    new-instance v4, LQa;

    const-string v5, "expiryDate"

    sget-object v6, LQc;->a:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v4}, LQa;->b()LQa;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LQa;->a(Ljava/lang/Object;)LQa;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LPx;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPx;->d:LPx;

    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [LPx;

    sget-object v1, LPx;->a:LPx;

    aput-object v1, v0, v7

    sget-object v1, LPx;->b:LPx;

    aput-object v1, v0, v8

    sget-object v1, LPx;->c:LPx;

    aput-object v1, v0, v9

    sget-object v1, LPx;->d:LPx;

    aput-object v1, v0, v10

    sput-object v0, LPx;->a:[LPx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPx;->a:LPI;

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPx;
    .registers 2
    .parameter

    .prologue
    .line 40
    const-class v0, LPx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPx;

    return-object v0
.end method

.method public static values()[LPx;
    .registers 1

    .prologue
    .line 40
    sget-object v0, LPx;->a:[LPx;

    invoke-virtual {v0}, [LPx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPx;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, LPx;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 40
    invoke-virtual {p0}, LPx;->a()LPI;

    move-result-object v0

    return-object v0
.end method
