.class public final enum LRE;
.super Ljava/lang/Enum;
.source "PunchUiModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LRE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LRE;

.field private static final synthetic a:[LRE;

.field public static final enum b:LRE;

.field public static final enum c:LRE;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, LRE;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LRE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LRE;->a:LRE;

    new-instance v0, LRE;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v3}, LRE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LRE;->b:LRE;

    new-instance v0, LRE;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v4}, LRE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LRE;->c:LRE;

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [LRE;

    sget-object v1, LRE;->a:LRE;

    aput-object v1, v0, v2

    sget-object v1, LRE;->b:LRE;

    aput-object v1, v0, v3

    sget-object v1, LRE;->c:LRE;

    aput-object v1, v0, v4

    sput-object v0, LRE;->a:[LRE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LRE;
    .registers 2
    .parameter

    .prologue
    .line 18
    const-class v0, LRE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LRE;

    return-object v0
.end method

.method public static values()[LRE;
    .registers 1

    .prologue
    .line 18
    sget-object v0, LRE;->a:[LRE;

    invoke-virtual {v0}, [LRE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LRE;

    return-object v0
.end method
