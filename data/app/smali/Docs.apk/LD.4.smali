.class public LLD;
.super Ljava/lang/Object;
.source "DocListFragment.java"

# interfaces
.implements LWQ;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/fragment/DocListFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/DocListFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 760
    iput-object p1, p0, LLD;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LkY;LWX;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 763
    invoke-virtual {p2}, LWX;->a()LWW;

    move-result-object v0

    .line 764
    sget-object v1, LLE;->a:[I

    invoke-virtual {v0}, LWW;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_58

    .line 784
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected synchronization status received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 766
    :pswitch_28
    iget-object v0, p0, LLD;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    .line 786
    :goto_2d
    return-void

    .line 769
    :pswitch_2e
    invoke-virtual {p2}, LWX;->b()LWW;

    move-result-object v0

    sget-object v1, LWW;->a:LWW;

    if-ne v0, v1, :cond_3b

    .line 770
    iget-object v0, p0, LLD;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    .line 772
    :cond_3b
    iget-object v0, p0, LLD;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Lcom/google/android/apps/docs/fragment/DocListFragment;)Lcom/google/android/apps/docs/view/DocListView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/view/DocListView;->a(LkY;LWX;)V

    goto :goto_2d

    .line 775
    :pswitch_45
    iget-object v0, p0, LLD;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    goto :goto_2d

    .line 778
    :pswitch_4b
    iget-object v0, p0, LLD;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    goto :goto_2d

    .line 781
    :pswitch_51
    iget-object v0, p0, LLD;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    goto :goto_2d

    .line 764
    nop

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_28
        :pswitch_2e
        :pswitch_45
        :pswitch_4b
        :pswitch_51
    .end packed-switch
.end method
