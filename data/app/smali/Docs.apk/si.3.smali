.class public final Lsi;
.super Les;
.source "UploadQueueService.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V
    .registers 3
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    .line 76
    const-string v0, "UploadQueueService thread"

    invoke-direct {p0, v0}, Les;-><init>(Ljava/lang/String;)V

    .line 77
    return-void
.end method


# virtual methods
.method public c()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    .line 86
    new-instance v0, Landroid/app/Notification;

    const v1, 0x1080088

    iget-object v2, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    sget v3, Len;->upload_notification_started_ticker_drivev2:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 90
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 91
    iget-object v1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    const-string v2, ""

    invoke-static {v1, v2}, LrY;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 92
    iget-object v1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v2, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    const-class v5, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 94
    iget-object v1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v1, v1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Landroid/app/NotificationManager;

    invoke-virtual {v1, v6, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 96
    iget-object v1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v1, v1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrR;

    iget-object v2, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    invoke-interface {v1, v2, v6, v0}, LrR;->a(Landroid/app/Service;ILandroid/app/Notification;)V

    .line 99
    iget-object v0, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v0, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LaaB;

    iget-object v1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    const-string v2, "UploadQueueService"

    new-instance v3, Lsj;

    invoke-direct {v3, p0}, Lsj;-><init>(Lsi;)V

    invoke-interface {v0, v1, v2, v3}, LaaB;->a(Landroid/content/Context;Ljava/lang/String;LdX;)V

    .line 106
    iget-object v1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    monitor-enter v1

    .line 107
    :try_start_5b
    iget-object v0, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lsi;

    .line 108
    monitor-exit v1
    :try_end_61
    .catchall {:try_start_5b .. :try_end_61} :catchall_8b

    .line 109
    iget-object v0, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v0, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrR;

    iget-object v1, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    invoke-interface {v0, v1, v6}, LrR;->a(Landroid/app/Service;Z)V

    .line 111
    iget-object v0, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v0, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    invoke-virtual {v0}, LrZ;->a()Z

    move-result v0

    if-eqz v0, :cond_8e

    iget-object v0, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v0, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    invoke-virtual {v0}, LrZ;->b()Z

    move-result v0

    if-nez v0, :cond_8e

    .line 112
    const-string v0, "UploadQueueService"

    const-string v1, "stopping service"

    invoke-static {v0, v1}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v0, p0, Lsi;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->stopSelf()V

    .line 121
    :goto_8a
    return-void

    .line 108
    :catchall_8b
    move-exception v0

    :try_start_8c
    monitor-exit v1
    :try_end_8d
    .catchall {:try_start_8c .. :try_end_8d} :catchall_8b

    throw v0

    .line 119
    :cond_8e
    const-string v0, "UploadQueueService"

    const-string v1, "not stopping service."

    invoke-static {v0, v1}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8a
.end method
