.class public final enum LCX;
.super Ljava/lang/Enum;
.source "XmlHttpRequestRelay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LCX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LCX;

.field private static final synthetic a:[LCX;

.field public static final enum b:LCX;

.field public static final enum c:LCX;

.field public static final enum d:LCX;

.field public static final enum e:LCX;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, LCX;

    const-string v1, "UNSENT"

    invoke-direct {v0, v1, v2}, LCX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCX;->a:LCX;

    .line 66
    new-instance v0, LCX;

    const-string v1, "OPENED"

    invoke-direct {v0, v1, v3}, LCX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCX;->b:LCX;

    .line 67
    new-instance v0, LCX;

    const-string v1, "HEADER_RECEIVED"

    invoke-direct {v0, v1, v4}, LCX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCX;->c:LCX;

    .line 68
    new-instance v0, LCX;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v5}, LCX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCX;->d:LCX;

    .line 69
    new-instance v0, LCX;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v6}, LCX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCX;->e:LCX;

    .line 64
    const/4 v0, 0x5

    new-array v0, v0, [LCX;

    sget-object v1, LCX;->a:LCX;

    aput-object v1, v0, v2

    sget-object v1, LCX;->b:LCX;

    aput-object v1, v0, v3

    sget-object v1, LCX;->c:LCX;

    aput-object v1, v0, v4

    sget-object v1, LCX;->d:LCX;

    aput-object v1, v0, v5

    sget-object v1, LCX;->e:LCX;

    aput-object v1, v0, v6

    sput-object v0, LCX;->a:[LCX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LCX;
    .registers 2
    .parameter

    .prologue
    .line 64
    const-class v0, LCX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LCX;

    return-object v0
.end method

.method public static values()[LCX;
    .registers 1

    .prologue
    .line 64
    sget-object v0, LCX;->a:[LCX;

    invoke-virtual {v0}, [LCX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LCX;

    return-object v0
.end method
