.class public final enum LhD;
.super Ljava/lang/Enum;
.source "MainProxyLogic.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LhD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LhD;

.field private static final synthetic a:[LhD;

.field public static final enum b:LhD;

.field public static final enum c:LhD;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    new-instance v0, LhD;

    const-string v1, "CHECK_DRIVE_ENABLE_STATUS_SUCCEED"

    invoke-direct {v0, v1, v2}, LhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhD;->a:LhD;

    .line 82
    new-instance v0, LhD;

    const-string v1, "CHECK_DRIVE_ENABLE_STATUS_FAILED"

    invoke-direct {v0, v1, v3}, LhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhD;->b:LhD;

    .line 87
    new-instance v0, LhD;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v4}, LhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhD;->c:LhD;

    .line 80
    const/4 v0, 0x3

    new-array v0, v0, [LhD;

    sget-object v1, LhD;->a:LhD;

    aput-object v1, v0, v2

    sget-object v1, LhD;->b:LhD;

    aput-object v1, v0, v3

    sget-object v1, LhD;->c:LhD;

    aput-object v1, v0, v4

    sput-object v0, LhD;->a:[LhD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LhD;
    .registers 2
    .parameter

    .prologue
    .line 80
    const-class v0, LhD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhD;

    return-object v0
.end method

.method public static values()[LhD;
    .registers 1

    .prologue
    .line 80
    sget-object v0, LhD;->a:[LhD;

    invoke-virtual {v0}, [LhD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LhD;

    return-object v0
.end method
