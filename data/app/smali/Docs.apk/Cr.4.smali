.class LCr;
.super LzG;
.source "ParagraphSpanStyle.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LzG",
        "<",
        "LCq;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LCq;


# direct methods
.method constructor <init>(LCq;)V
    .registers 2
    .parameter

    .prologue
    .line 136
    iput-object p1, p0, LCr;->a:LCq;

    invoke-direct {p0}, LzG;-><init>()V

    return-void
.end method


# virtual methods
.method public a()LCq;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, LCr;->a:LCq;

    return-object v0
.end method

.method public bridge synthetic a()LCt;
    .registers 2

    .prologue
    .line 136
    invoke-virtual {p0}, LCr;->a()LCq;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/util/Collection;
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 140
    iget-object v0, p0, LCr;->a:LCq;

    invoke-static {v0}, LCq;->a(LCq;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 141
    new-instance v0, LGy;

    invoke-direct {v0}, LGy;-><init>()V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    :goto_15
    new-instance v0, LCd;

    iget-object v1, p0, LCr;->a:LCq;

    invoke-static {v1}, LCq;->a(LCq;)D

    move-result-wide v1

    iget-object v3, p0, LCr;->a:LCq;

    invoke-static {v3}, LCq;->b(LCq;)D

    move-result-wide v3

    iget-object v5, p0, LCr;->a:LCq;

    invoke-static {v5}, LCq;->a(LCq;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LCr;->a:LCq;

    invoke-static {v6}, LCq;->a(LCq;)LCw;

    move-result-object v6

    iget-object v7, p0, LCr;->a:LCq;

    invoke-static {v7}, LCq;->a(LCq;)Lxu;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LCd;-><init>(DDLjava/lang/String;LCw;Lxu;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    new-instance v0, LCc;

    iget-object v1, p0, LCr;->a:LCq;

    invoke-static {v1}, LCq;->c(LCq;)D

    move-result-wide v1

    iget-object v3, p0, LCr;->a:LCq;

    invoke-static {v3}, LCq;->d(LCq;)D

    move-result-wide v3

    iget-object v5, p0, LCr;->a:LCq;

    invoke-static {v5}, LCq;->e(LCq;)D

    move-result-wide v5

    iget-object v7, p0, LCr;->a:LCq;

    invoke-static {v7}, LCq;->a(LCq;)Lxu;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LCc;-><init>(DDDLxu;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v0, LCs;

    invoke-direct {v0, p0}, LCs;-><init>(LCr;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    return-object v8

    .line 143
    :cond_64
    new-instance v0, Landroid/text/style/AlignmentSpan$Standard;

    iget-object v1, p0, LCr;->a:LCq;

    invoke-static {v1}, LCq;->a(LCq;)Landroid/text/Layout$Alignment;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_15
.end method
