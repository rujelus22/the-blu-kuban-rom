.class public Lxh;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lxg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lxg;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 5084
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 5085
    return-void
.end method

.method static a(Lvw;J)Lxh;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 5080
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lxh;

    invoke-direct {v0, p0, p1, p2}, Lxh;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 5114
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->l(J)I

    move-result v0

    .line 5116
    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 5090
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->f(J)Ljava/lang/String;

    move-result-object v0

    .line 5092
    return-object v0
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 5098
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->e(J)Z

    move-result v0

    .line 5100
    return v0
.end method

.method public b()I
    .registers 3

    .prologue
    .line 5154
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->m(J)I

    move-result v0

    .line 5156
    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 5106
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->g(J)Ljava/lang/String;

    move-result-object v0

    .line 5108
    return-object v0
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 5130
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->f(J)Z

    move-result v0

    .line 5132
    return v0
.end method

.method public c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 5122
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->h(J)Ljava/lang/String;

    move-result-object v0

    .line 5124
    return-object v0
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 5138
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->g(J)Z

    move-result v0

    .line 5140
    return v0
.end method

.method public d()Z
    .registers 3

    .prologue
    .line 5146
    invoke-virtual {p0}, Lxh;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->h(J)Z

    move-result v0

    .line 5148
    return v0
.end method
