.class public LQm;
.super LQk;
.source "BaseSlidePickerFragment.java"


# instance fields
.field final synthetic a:Landroid/os/Handler;

.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 129
    iput-object p1, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iput-object p2, p0, LQm;->a:Landroid/os/Handler;

    invoke-direct {p0}, LQk;-><init>()V

    return-void
.end method


# virtual methods
.method protected b(I)V
    .registers 3
    .parameter

    .prologue
    .line 149
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    invoke-interface {v0}, LRj;->a()V

    .line 150
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRc;

    invoke-virtual {v0}, LRc;->notifyDataSetChanged()V

    .line 151
    return-void
.end method

.method protected f()V
    .registers 3

    .prologue
    .line 133
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    invoke-interface {v0}, LQR;->a()I

    move-result v0

    if-lez v0, :cond_2e

    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    invoke-interface {v0}, LQR;->b()I

    move-result v0

    if-lez v0, :cond_2e

    .line 134
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    invoke-interface {v0}, LQR;->a()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    invoke-interface {v1}, LQR;->b()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 135
    iget-object v1, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    invoke-interface {v1, v0}, LRj;->a(F)V

    .line 137
    :cond_2e
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    invoke-interface {v0}, LRj;->a()V

    .line 138
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRc;

    invoke-virtual {v0}, LRc;->notifyDataSetChanged()V

    .line 139
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    invoke-interface {v0}, LRj;->a()V

    .line 144
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRc;

    invoke-virtual {v0}, LRc;->notifyDataSetChanged()V

    .line 145
    return-void
.end method

.method protected j()V
    .registers 3

    .prologue
    .line 155
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    invoke-interface {v0}, LRj;->a()V

    .line 156
    iget-object v0, p0, LQm;->a:Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRc;

    invoke-virtual {v0}, LRc;->notifyDataSetChanged()V

    .line 158
    iget-object v0, p0, LQm;->a:Landroid/os/Handler;

    new-instance v1, LQn;

    invoke-direct {v1, p0}, LQn;-><init>(LQm;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 164
    return-void
.end method
