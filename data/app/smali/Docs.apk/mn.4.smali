.class final Lmn;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements LaeL;


# instance fields
.field private final a:Lafo;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lafo;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lmn;->a:Ljava/lang/String;

    .line 81
    iput-object p2, p0, Lmn;->b:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lmn;->a:Lafo;

    .line 83
    return-void
.end method


# virtual methods
.method public a(LaeK;)V
    .registers 4
    .parameter

    .prologue
    .line 87
    check-cast p1, Lcom/google/api/services/discussions/DiscussionsRequest;

    .line 88
    iget-object v0, p0, Lmn;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/DiscussionsRequest;

    .line 89
    iget-object v0, p0, Lmn;->b:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 90
    const-string v0, "pageToken"

    iget-object v1, p0, Lmn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    :cond_12
    iget-object v0, p0, Lmn;->a:Lafo;

    if-eqz v0, :cond_21

    .line 93
    const-string v0, "startFrom"

    iget-object v1, p0, Lmn;->a:Lafo;

    invoke-virtual {v1}, Lafo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 95
    :cond_21
    return-void
.end method
