.class LEP;
.super Ljava/lang/Object;
.source "RecordingRenderer.java"

# interfaces
.implements LEN;


# instance fields
.field private a:F

.field private final a:Landroid/graphics/Paint;

.field private a:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/high16 v0, 0x3f80

    iput v0, p0, LEP;->a:F

    .line 46
    iput-object p1, p0, LEP;->a:Landroid/graphics/Path;

    .line 47
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, LEP;->a:Landroid/graphics/Paint;

    .line 48
    return-void
.end method


# virtual methods
.method public a(LFf;F)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x3f80

    const/4 v2, 0x0

    .line 52
    iget v0, p0, LEP;->a:F

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_17

    .line 53
    iget-object v0, p0, LEP;->a:Landroid/graphics/Path;

    iget v1, p0, LEP;->a:F

    div-float v1, p2, v1

    invoke-static {v0, v1}, LFa;->a(Landroid/graphics/Path;F)Landroid/graphics/Path;

    move-result-object v0

    .line 55
    iput p2, p0, LEP;->a:F

    .line 56
    iput-object v0, p0, LEP;->a:Landroid/graphics/Path;

    .line 59
    :cond_17
    div-float v0, v3, p2

    div-float v1, v3, p2

    invoke-interface {p1, v0, v1, v2, v2}, LFf;->a(FFFF)V

    .line 60
    iget-object v0, p0, LEP;->a:Landroid/graphics/Path;

    iget-object v1, p0, LEP;->a:Landroid/graphics/Paint;

    invoke-interface {p1, v0, v1}, LFf;->a(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 61
    invoke-interface {p1, p2, p2, v2, v2}, LFf;->a(FFFF)V

    .line 62
    return-void
.end method
