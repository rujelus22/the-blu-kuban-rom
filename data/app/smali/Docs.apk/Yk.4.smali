.class public final LYk;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LYn;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LYp;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LYh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 33
    iput-object p1, p0, LYk;->a:LYD;

    .line 34
    const-class v0, LYn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYk;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYk;->a:LZb;

    .line 37
    const-class v0, LYp;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LYk;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYk;->b:LZb;

    .line 40
    const-class v0, LYh;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LYk;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LYk;->c:LZb;

    .line 43
    return-void
.end method

.method static synthetic a(LYk;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LYk;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LYk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 50
    const-class v0, LYn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LYk;->a:LZb;

    invoke-virtual {p0, v0, v1}, LYk;->a(Laop;LZb;)V

    .line 51
    const-class v0, LYp;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LYk;->b:LZb;

    invoke-virtual {p0, v0, v1}, LYk;->a(Laop;LZb;)V

    .line 52
    const-class v0, LYh;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LYk;->c:LZb;

    invoke-virtual {p0, v0, v1}, LYk;->a(Laop;LZb;)V

    .line 53
    iget-object v0, p0, LYk;->a:LZb;

    iget-object v1, p0, LYk;->a:LYD;

    iget-object v1, v1, LYD;->a:LYk;

    iget-object v1, v1, LYk;->b:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 55
    iget-object v0, p0, LYk;->b:LZb;

    new-instance v1, LYl;

    invoke-direct {v1, p0}, LYl;-><init>(LYk;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 69
    iget-object v0, p0, LYk;->c:LZb;

    new-instance v1, LYm;

    invoke-direct {v1, p0}, LYm;-><init>(LYk;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 113
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 117
    return-void
.end method
