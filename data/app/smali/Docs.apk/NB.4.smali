.class public LNB;
.super LNL;
.source "DefaultHttpIssuer.java"


# annotations
.annotation runtime LaoJ;
.end annotation


# instance fields
.field private final a:LNS;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LNS;Laoz;)V
    .registers 3
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LNS;",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, LNL;-><init>()V

    .line 37
    iput-object p1, p0, LNB;->a:LNS;

    .line 38
    iput-object p2, p0, LNB;->a:Laoz;

    .line 39
    return-void
.end method

.method static synthetic a(LNB;)LNS;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, LNB;->a:LNS;

    return-object v0
.end method

.method static synthetic a(LNB;)Laoz;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, LNB;->a:Laoz;

    return-object v0
.end method


# virtual methods
.method protected a()LNO;
    .registers 2

    .prologue
    .line 43
    new-instance v0, LNC;

    invoke-direct {v0, p0}, LNC;-><init>(LNB;)V

    return-object v0
.end method

.method public a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;
    .registers 3
    .parameter

    .prologue
    .line 71
    invoke-static {p1}, Landroid/net/http/AndroidHttpClient;->getUngzippedContent(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/apache/http/HttpRequest;)V
    .registers 2
    .parameter

    .prologue
    .line 66
    invoke-static {p1}, Landroid/net/http/AndroidHttpClient;->modifyRequestToAcceptGzipResponse(Lorg/apache/http/HttpRequest;)V

    .line 67
    return-void
.end method
