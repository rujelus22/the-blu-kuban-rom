.class public final LLL;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMt;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMf;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMe;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMg;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LLG;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LLK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 36
    iput-object p1, p0, LLL;->a:LYD;

    .line 37
    const-class v0, LMt;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LLL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLL;->a:LZb;

    .line 40
    const-class v0, LMf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LLL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLL;->b:LZb;

    .line 43
    const-class v0, LMe;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LLL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLL;->c:LZb;

    .line 46
    const-class v0, LMg;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LLL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLL;->d:LZb;

    .line 49
    const-class v0, LLG;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LLL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLL;->e:LZb;

    .line 52
    const-class v0, LLK;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LLL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLL;->f:LZb;

    .line 55
    return-void
.end method

.method static synthetic a(LLL;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LLL;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 424
    const-class v0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;

    new-instance v1, LLM;

    invoke-direct {v1, p0}, LLM;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 432
    const-class v0, Lcom/google/android/apps/docs/fragment/DocsUpgradedToDriveDialogFragment;

    new-instance v1, LLQ;

    invoke-direct {v1, p0}, LLQ;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 440
    const-class v0, Lcom/google/android/apps/docs/fragment/DriveWelcomeDialogFragment;

    new-instance v1, LLR;

    invoke-direct {v1, p0}, LLR;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 448
    const-class v0, Lcom/google/android/apps/docs/fragment/CommentStreamThreadFragment;

    new-instance v1, LLS;

    invoke-direct {v1, p0}, LLS;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 456
    const-class v0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    new-instance v1, LLT;

    invoke-direct {v1, p0}, LLT;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 464
    const-class v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;

    new-instance v1, LLU;

    invoke-direct {v1, p0}, LLU;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 472
    const-class v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;

    new-instance v1, LLV;

    invoke-direct {v1, p0}, LLV;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 480
    const-class v0, Lcom/google/android/apps/docs/fragment/CommentStreamFragment;

    new-instance v1, LLW;

    invoke-direct {v1, p0}, LLW;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 488
    const-class v0, Lcom/google/android/apps/docs/fragment/DocListFragment;

    new-instance v1, LLX;

    invoke-direct {v1, p0}, LLX;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 496
    const-class v0, Lcom/google/android/apps/docs/fragment/ErrorFragment;

    new-instance v1, LLN;

    invoke-direct {v1, p0}, LLN;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 504
    const-class v0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    new-instance v1, LLO;

    invoke-direct {v1, p0}, LLO;-><init>(LLL;)V

    invoke-virtual {p0, v0, v1}, LLL;->a(Ljava/lang/Class;Laou;)V

    .line 512
    const-class v0, LMt;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLL;->a:LZb;

    invoke-virtual {p0, v0, v1}, LLL;->a(Laop;LZb;)V

    .line 513
    const-class v0, LMf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLL;->b:LZb;

    invoke-virtual {p0, v0, v1}, LLL;->a(Laop;LZb;)V

    .line 514
    const-class v0, LMe;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLL;->c:LZb;

    invoke-virtual {p0, v0, v1}, LLL;->a(Laop;LZb;)V

    .line 515
    const-class v0, LMg;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLL;->d:LZb;

    invoke-virtual {p0, v0, v1}, LLL;->a(Laop;LZb;)V

    .line 516
    const-class v0, LLG;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLL;->e:LZb;

    invoke-virtual {p0, v0, v1}, LLL;->a(Laop;LZb;)V

    .line 517
    const-class v0, LLK;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLL;->f:LZb;

    invoke-virtual {p0, v0, v1}, LLL;->a(Laop;LZb;)V

    .line 518
    iget-object v0, p0, LLL;->a:LZb;

    iget-object v1, p0, LLL;->a:LYD;

    iget-object v1, v1, LYD;->a:Lju;

    iget-object v1, v1, Lju;->a:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 520
    iget-object v0, p0, LLL;->b:LZb;

    iget-object v1, p0, LLL;->a:LYD;

    iget-object v1, v1, LYD;->a:LLL;

    iget-object v1, v1, LLL;->d:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 522
    iget-object v0, p0, LLL;->c:LZb;

    iget-object v1, p0, LLL;->a:LYD;

    iget-object v1, v1, LYD;->a:Lju;

    iget-object v1, v1, Lju;->a:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 524
    iget-object v0, p0, LLL;->d:LZb;

    new-instance v1, LLP;

    invoke-direct {v1, p0}, LLP;-><init>(LLL;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 538
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/CommentStreamFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 293
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamFragment;->a:Laoz;

    .line 299
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Ld;

    iget-object v0, v0, Ld;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamFragment;->a:Landroid/os/Handler;

    .line 305
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lms;

    iget-object v0, v0, Lms;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlX;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamFragment;->a:LlX;

    .line 311
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamFragment;->a:Llf;

    .line 317
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/CommentStreamThreadFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamThreadFragment;->a:Laoz;

    .line 85
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Ld;

    iget-object v0, v0, Ld;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamThreadFragment;->a:Landroid/os/Handler;

    .line 91
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lms;

    iget-object v0, v0, Lms;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlX;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamThreadFragment;->a:LlX;

    .line 97
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/CommentStreamThreadFragment;->a:Llf;

    .line 103
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/DocListFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 321
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    .line 327
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiX;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LiX;

    .line 333
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Laoz;

    .line 339
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LKS;

    .line 345
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->l:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liv;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    .line 351
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llz;

    .line 357
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlE;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LlE;

    .line 363
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LPm;

    .line 369
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lez;

    iget-object v0, v0, Lez;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lev;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lev;

    .line 375
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lja;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lja;

    .line 381
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lgl;

    .line 387
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LUL;

    .line 393
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLL;

    iget-object v0, v0, LLL;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLG;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LLG;

    .line 399
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/DocsUpgradedToDriveDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 69
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/DragKnobFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 413
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLL;

    iget-object v0, v0, LLL;->f:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLK;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:LLK;

    .line 419
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/DriveWelcomeDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 75
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/ErrorFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 403
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a:LZM;

    .line 409
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLL;

    iget-object v0, v0, LLL;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMe;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LMe;

    .line 135
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->l:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liv;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Liv;

    .line 141
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Laoz;

    .line 147
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljl;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljl;

    .line 153
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXX;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LXX;

    .line 159
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Llf;

    .line 165
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiG;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LiG;

    .line 171
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->q:LZb;

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->b:Laoz;

    .line 177
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaaJ;

    .line 183
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Llf;

    .line 113
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LSs;

    iget-object v0, v0, LSs;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSB;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LSB;

    .line 119
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->F:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZE;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LZE;

    .line 125
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;)V
    .registers 3
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    invoke-virtual {v0, p1}, LTQ;->a(Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;)V

    .line 63
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->l:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liv;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Liv;

    .line 193
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LVM;

    iget-object v0, v0, LVM;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVH;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LVH;

    .line 199
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWY;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LWY;

    .line 205
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Llz;

    .line 211
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLL;

    iget-object v0, v0, LLL;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMt;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMt;

    .line 217
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->i:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LaaZ;

    .line 223
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Llf;

    .line 229
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lgl;

    .line 235
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lju;

    iget-object v0, v0, Lju;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LjY;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LjY;

    .line 241
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLL;

    iget-object v0, v0, LLL;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLG;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LLG;

    .line 247
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LeQ;

    .line 253
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiX;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    .line 259
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTn;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LTn;

    .line 265
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->n:LZb;

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Laoz;

    .line 271
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUr;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUr;

    .line 277
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LZM;

    .line 283
    iget-object v0, p0, LLL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LLL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LZj;

    .line 289
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 542
    return-void
.end method
