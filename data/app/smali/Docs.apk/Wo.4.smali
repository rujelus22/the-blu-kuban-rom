.class public LWo;
.super Latj;
.source "XmlDocsListGDataSerializer.java"


# instance fields
.field private final a:LVV;

.field private final a:Latg;


# direct methods
.method constructor <init>(Latg;LVV;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Latj;-><init>(Latg;LasT;)V

    .line 40
    iput-object p1, p0, LWo;->a:Latg;

    .line 41
    iput-object p2, p0, LWo;->a:LVV;

    .line 42
    return-void
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-static {p1}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 108
    :goto_7
    return-void

    .line 105
    :cond_8
    sget-object v0, Latf;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 106
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 107
    sget-object v0, Latf;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_7
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 135
    invoke-static {p1}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {p2}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 150
    :goto_d
    return-void

    .line 139
    :cond_e
    sget-object v0, Latf;->w:Ljava/lang/String;

    invoke-interface {p0, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 140
    invoke-static {p1}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 141
    sget-object v0, Latf;->x:Ljava/lang/String;

    invoke-interface {p0, v3, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 143
    :cond_1e
    invoke-static {p2}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_29

    .line 144
    sget-object v0, Latf;->y:Ljava/lang/String;

    invoke-interface {p0, v3, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 146
    :cond_29
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p0, v3, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_31

    .line 149
    :cond_4d
    sget-object v0, Latf;->w:Ljava/lang/String;

    invoke-interface {p0, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_d
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    const-string v0, "authorizedApp"

    .line 113
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 114
    const-string v2, "http://schemas.google.com/docs/2007"

    const-string v3, "authorizedApp"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 118
    invoke-interface {p0, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 119
    const-string v0, "http://schemas.google.com/docs/2007"

    const-string v2, "authorizedApp"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_6

    .line 121
    :cond_24
    return-void
.end method

.method private b(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 4
    .parameter

    .prologue
    .line 125
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "gd"

    const-string v1, "http://schemas.google.com/g/2005"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0, p1}, LWo;->a(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 128
    return-void
.end method

.method private b(Lorg/xmlpull/v1/XmlSerializer;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 78
    const/4 v0, 0x1

    if-ne p2, v0, :cond_c

    .line 79
    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LWo;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 81
    :cond_c
    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LWo;->b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LWo;->a:LVV;

    invoke-virtual {v1}, LVV;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, LWo;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 85
    const-string v1, "label"

    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->a()Z

    move-result v0

    if-eqz v0, :cond_75

    const-string v0, "starred"

    :goto_34
    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 87
    const-string v2, "label"

    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->c()Z

    move-result v0

    if-eqz v0, :cond_78

    const-string v0, "hidden"

    :goto_44
    invoke-static {v2, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 90
    const-string v2, "http://schemas.google.com/g/2005/labels#starred"

    const-string v3, "http://schemas.google.com/g/2005/labels"

    invoke-static {p1, v2, v3, v1}, LWo;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 92
    const-string v1, "http://schemas.google.com/g/2005/labels#hidden"

    const-string v2, "http://schemas.google.com/g/2005/labels"

    invoke-static {p1, v1, v2, v0}, LWo;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 94
    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LWo;->c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LWo;->d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, LWo;->a:LVV;

    invoke-virtual {v0}, LVV;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, v0}, LWo;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V

    .line 97
    invoke-virtual {p0, p1, p2}, LWo;->a(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 98
    return-void

    .line 85
    :cond_75
    const-string v0, ""

    goto :goto_34

    .line 87
    :cond_78
    const-string v0, ""

    goto :goto_44
.end method

.method private static b(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-static {p1}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 159
    :goto_7
    return-void

    .line 156
    :cond_8
    sget-object v0, Latf;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 157
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 158
    sget-object v0, Latf;->q:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_7
.end method

.method private static c(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-static {p1}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 169
    :goto_7
    return-void

    .line 166
    :cond_8
    sget-object v0, Latf;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 167
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 168
    sget-object v0, Latf;->A:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_7
.end method

.method private static d(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 173
    invoke-static {p1}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 179
    :goto_6
    return-void

    .line 176
    :cond_7
    const-string v0, "http://schemas.google.com/g/2005"

    const-string v1, "lastViewed"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 177
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 178
    const-string v0, "http://schemas.google.com/g/2005"

    const-string v1, "lastViewed"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_6
.end method


# virtual methods
.method public a(Ljava/io/OutputStream;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x3

    .line 53
    .line 55
    :try_start_1
    iget-object v0, p0, LWo;->a:Latg;

    invoke-interface {v0}, Latg;->a()Lorg/xmlpull/v1/XmlSerializer;
    :try_end_6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_6} :catch_32

    move-result-object v0

    .line 60
    sget-object v1, Latf;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 61
    if-eq p2, v3, :cond_18

    .line 62
    sget-object v1, Latf;->e:Ljava/lang/String;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 63
    invoke-direct {p0, v0}, LWo;->b(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 66
    :cond_18
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Latf;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 67
    invoke-direct {p0, v0, p2}, LWo;->b(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 68
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Latf;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 70
    if-eq p2, v3, :cond_2e

    .line 71
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 73
    :cond_2e
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 74
    return-void

    .line 56
    :catch_32
    move-exception v0

    .line 57
    new-instance v1, Latc;

    const-string v2, "Unable to create XmlSerializer."

    invoke-direct {v1, v2, v0}, Latc;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
