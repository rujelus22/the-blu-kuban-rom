.class public enum LeK;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LeK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LeK;

.field private static final synthetic a:[LeK;

.field public static final enum b:LeK;

.field public static final enum c:LeK;

.field public static final enum d:LeK;

.field public static final enum e:LeK;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 183
    new-instance v0, LeK;

    const-string v1, "USER"

    const-string v2, "user"

    invoke-direct {v0, v1, v3, v2}, LeK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LeK;->a:LeK;

    .line 184
    new-instance v0, LeK;

    const-string v1, "GROUP"

    const-string v2, "group"

    invoke-direct {v0, v1, v4, v2}, LeK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LeK;->b:LeK;

    .line 185
    new-instance v0, LeK;

    const-string v1, "DOMAIN"

    const-string v2, "domain"

    invoke-direct {v0, v1, v5, v2}, LeK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LeK;->c:LeK;

    .line 186
    new-instance v0, LeK;

    const-string v1, "DEFAULT"

    const-string v2, "default"

    invoke-direct {v0, v1, v6, v2}, LeK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LeK;->d:LeK;

    .line 187
    new-instance v0, LeL;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v7, v2}, LeL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LeK;->e:LeK;

    .line 182
    const/4 v0, 0x5

    new-array v0, v0, [LeK;

    sget-object v1, LeK;->a:LeK;

    aput-object v1, v0, v3

    sget-object v1, LeK;->b:LeK;

    aput-object v1, v0, v4

    sget-object v1, LeK;->c:LeK;

    aput-object v1, v0, v5

    sget-object v1, LeK;->d:LeK;

    aput-object v1, v0, v6

    sget-object v1, LeK;->e:LeK;

    aput-object v1, v0, v7

    sput-object v0, LeK;->a:[LeK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 196
    iput-object p3, p0, LeK;->a:Ljava/lang/String;

    .line 197
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;LeC;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-direct {p0, p1, p2, p3}, LeK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LeK;
    .registers 7
    .parameter

    .prologue
    .line 212
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 213
    invoke-static {}, LeK;->values()[LeK;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v4, :cond_1c

    aget-object v0, v3, v1

    .line 214
    iget-object v5, v0, LeK;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 218
    :goto_17
    return-object v0

    .line 213
    :cond_18
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 218
    :cond_1c
    sget-object v0, LeK;->e:LeK;

    goto :goto_17
.end method

.method public static valueOf(Ljava/lang/String;)LeK;
    .registers 2
    .parameter

    .prologue
    .line 182
    const-class v0, LeK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LeK;

    return-object v0
.end method

.method public static values()[LeK;
    .registers 1

    .prologue
    .line 182
    sget-object v0, LeK;->a:[LeK;

    invoke-virtual {v0}, [LeK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LeK;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, LeK;->a:Ljava/lang/String;

    return-object v0
.end method
