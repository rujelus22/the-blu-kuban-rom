.class public final enum LabA;
.super Ljava/lang/Enum;
.source "DocListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LabA;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LabA;

.field private static final synthetic a:[LabA;

.field public static final enum b:LabA;

.field public static final enum c:LabA;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 104
    new-instance v0, LabA;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, LabA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LabA;->a:LabA;

    .line 106
    new-instance v0, LabA;

    const-string v1, "SYNCING"

    invoke-direct {v0, v1, v3}, LabA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LabA;->b:LabA;

    .line 108
    new-instance v0, LabA;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4}, LabA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LabA;->c:LabA;

    .line 99
    const/4 v0, 0x3

    new-array v0, v0, [LabA;

    sget-object v1, LabA;->a:LabA;

    aput-object v1, v0, v2

    sget-object v1, LabA;->b:LabA;

    aput-object v1, v0, v3

    sget-object v1, LabA;->c:LabA;

    aput-object v1, v0, v4

    sput-object v0, LabA;->a:[LabA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LabA;
    .registers 2
    .parameter

    .prologue
    .line 99
    const-class v0, LabA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LabA;

    return-object v0
.end method

.method public static values()[LabA;
    .registers 1

    .prologue
    .line 99
    sget-object v0, LabA;->a:[LabA;

    invoke-virtual {v0}, [LabA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LabA;

    return-object v0
.end method
