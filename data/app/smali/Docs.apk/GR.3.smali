.class public LGR;
.super Ljava/lang/Object;
.source "TrixDataFragment.java"

# interfaces
.implements LNf;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 88
    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 89
    const-string v0, "TrixDataFragment"

    const-string v1, "Access confirmed."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)Z

    move-result v0

    if-nez v0, :cond_31

    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)Z

    move-result v0

    if-nez v0, :cond_31

    .line 91
    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;Z)Z

    .line 92
    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V

    .line 94
    :cond_31
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 104
    const-string v0, "TrixDataFragment"

    const-string v1, "Access requested."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)Z

    move-result v0

    if-nez v0, :cond_2b

    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 106
    iget-object v0, p0, LGR;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Landroid/content/Intent;)V

    .line 108
    :cond_2b
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 4
    .parameter

    .prologue
    .line 98
    const-string v0, "TrixDataFragment"

    const-string v1, "Access denied: "

    invoke-static {v0, v1, p1}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 99
    return-void
.end method
