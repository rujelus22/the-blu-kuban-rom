.class public final Laqb;
.super LaoY;
.source "LinkedBindingImpl.java"

# interfaces
.implements Larn;
.implements Larz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LaoY",
        "<TT;>;",
        "Larn;",
        "Larz",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Laop;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Object;",
            "LapY",
            "<+TT;>;",
            "LaqC;",
            "Laop",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct/range {p0 .. p5}, LaoY;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;)V

    .line 38
    iput-object p6, p0, Laqb;->a:Laop;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Laop;LaqC;Laop;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Laop",
            "<TT;>;",
            "LaqC;",
            "Laop",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, LaoY;-><init>(Ljava/lang/Object;Laop;LaqC;)V

    .line 43
    iput-object p4, p0, Laqb;->a:Laop;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Laop;)LaoY;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<TT;>;)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Laqb;

    invoke-virtual {p0}, Laqb;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Laqb;->a()LaqC;

    move-result-object v2

    iget-object v3, p0, Laqb;->a:Laop;

    invoke-direct {v0, v1, p1, v2, v3}, Laqb;-><init>(Ljava/lang/Object;Laop;LaqC;Laop;)V

    return-object v0
.end method

.method public a(LaqC;)LaoY;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaqC;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Laqb;

    invoke-virtual {p0}, Laqb;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Laqb;->a()Laop;

    move-result-object v2

    iget-object v3, p0, Laqb;->a:Laop;

    invoke-direct {v0, v1, v2, p1, v3}, Laqb;-><init>(Ljava/lang/Object;Laop;LaqC;Laop;)V

    return-object v0
.end method

.method public c()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Laqb;->a:Laop;

    invoke-static {v0}, Larg;->a(Laop;)Larg;

    move-result-object v0

    invoke-static {v0}, Lajm;->a(Ljava/lang/Object;)Lajm;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 81
    instance-of v1, p1, Laqb;

    if-eqz v1, :cond_2e

    .line 82
    check-cast p1, Laqb;

    .line 83
    invoke-virtual {p0}, Laqb;->a()Laop;

    move-result-object v1

    invoke-virtual {p1}, Laqb;->a()Laop;

    move-result-object v2

    invoke-virtual {v1, v2}, Laop;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    invoke-virtual {p0}, Laqb;->a()LaqC;

    move-result-object v1

    invoke-virtual {p1}, Laqb;->a()LaqC;

    move-result-object v2

    invoke-virtual {v1, v2}, LaqC;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, Laqb;->a:Laop;

    iget-object v2, p1, Laqb;->a:Laop;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/4 v0, 0x1

    .line 87
    :cond_2e
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 93
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Laqb;->a()Laop;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Laqb;->a()LaqC;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Laqb;->a:Laop;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 71
    const-class v0, Larz;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {p0}, Laqb;->a()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {p0}, Laqb;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "scope"

    invoke-virtual {p0}, Laqb;->a()LaqC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "target"

    iget-object v2, p0, Laqb;->a:Laop;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
