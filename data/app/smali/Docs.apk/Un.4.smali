.class LUn;
.super Ljava/lang/Object;
.source "ServerAclManager.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/Set",
        "<",
        "LeB;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LUm;

.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(LUm;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, LUn;->a:LUm;

    iput-object p2, p0, LUn;->a:Ljava/lang/String;

    iput-object p3, p0, LUn;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LeB;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 54
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 91
    :cond_f
    :goto_f
    return-object v0

    .line 58
    :cond_10
    :try_start_10
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 59
    iget-object v1, p0, LUn;->a:LUm;

    invoke-static {v1}, LUm;->a(LUm;)LVy;

    move-result-object v1

    iget-object v3, p0, LUn;->a:Ljava/lang/String;

    invoke-static {v3}, LeM;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LUn;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, LVy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LWa;
    :try_end_2b
    .catchall {:try_start_10 .. :try_end_2b} :catchall_11b
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_2b} :catch_124
    .catch Latc; {:try_start_10 .. :try_end_2b} :catch_af
    .catch LasH; {:try_start_10 .. :try_end_2b} :catch_ce
    .catch Landroid/accounts/AuthenticatorException; {:try_start_10 .. :try_end_2b} :catch_f7

    move-result-object v1

    .line 61
    :try_start_2c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-eqz v3, :cond_40

    .line 62
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;
    :try_end_39
    .catchall {:try_start_2c .. :try_end_39} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_39} :catch_83
    .catch Latc; {:try_start_2c .. :try_end_39} :catch_122
    .catch LasH; {:try_start_2c .. :try_end_39} :catch_120
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2c .. :try_end_39} :catch_11e

    move-result-object v0

    .line 87
    if-eqz v1, :cond_f

    .line 88
    invoke-virtual {v1}, LWa;->a()V

    goto :goto_f

    .line 65
    :cond_40
    :try_start_40
    invoke-virtual {v1}, LWa;->b()LasU;

    .line 67
    :goto_43
    invoke-virtual {v1}, LWa;->a()Z

    move-result v3

    if-eqz v3, :cond_a8

    .line 68
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-eqz v3, :cond_5d

    .line 69
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;
    :try_end_56
    .catchall {:try_start_40 .. :try_end_56} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_56} :catch_83
    .catch Latc; {:try_start_40 .. :try_end_56} :catch_122
    .catch LasH; {:try_start_40 .. :try_end_56} :catch_120
    .catch Landroid/accounts/AuthenticatorException; {:try_start_40 .. :try_end_56} :catch_11e

    move-result-object v0

    .line 87
    if-eqz v1, :cond_f

    .line 88
    invoke-virtual {v1}, LWa;->a()V

    goto :goto_f

    .line 71
    :cond_5d
    :try_start_5d
    invoke-virtual {v1, v2}, LWa;->a(LasT;)LVS;

    move-result-object v2

    .line 72
    iget-object v3, p0, LUn;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LVS;->a(Ljava/lang/String;)LeB;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 73
    const-string v3, "ServerAclLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Got ACL entry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_82
    .catchall {:try_start_5d .. :try_end_82} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_82} :catch_83
    .catch Latc; {:try_start_5d .. :try_end_82} :catch_122
    .catch LasH; {:try_start_5d .. :try_end_82} :catch_120
    .catch Landroid/accounts/AuthenticatorException; {:try_start_5d .. :try_end_82} :catch_11e

    goto :goto_43

    .line 76
    :catch_83
    move-exception v0

    .line 77
    :goto_84
    :try_start_84
    new-instance v2, LTm;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_a1
    .catchall {:try_start_84 .. :try_end_a1} :catchall_a1

    .line 87
    :catchall_a1
    move-exception v0

    :goto_a2
    if-eqz v1, :cond_a7

    .line 88
    invoke-virtual {v1}, LWa;->a()V

    :cond_a7
    throw v0

    .line 87
    :cond_a8
    if-eqz v1, :cond_f

    .line 88
    invoke-virtual {v1}, LWa;->a()V

    goto/16 :goto_f

    .line 78
    :catch_af
    move-exception v0

    move-object v1, v2

    .line 79
    :goto_b1
    :try_start_b1
    new-instance v2, LTm;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parse Exception in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Latc;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 80
    :catch_ce
    move-exception v0

    move-object v1, v2

    .line 81
    :goto_d0
    new-instance v2, LTm;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authentication Exception in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, LasH;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_ec
    .catchall {:try_start_b1 .. :try_end_ec} :catchall_a1

    .line 87
    if-eqz v1, :cond_f1

    .line 88
    invoke-virtual {v1}, LWa;->a()V

    .line 91
    :cond_f1
    :goto_f1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto/16 :goto_f

    .line 83
    :catch_f7
    move-exception v0

    move-object v1, v2

    .line 84
    :goto_f9
    :try_start_f9
    new-instance v2, LTm;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authenticator Exception in ACL loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_115
    .catchall {:try_start_f9 .. :try_end_115} :catchall_a1

    .line 87
    if-eqz v1, :cond_f1

    .line 88
    invoke-virtual {v1}, LWa;->a()V

    goto :goto_f1

    .line 87
    :catchall_11b
    move-exception v0

    move-object v1, v2

    goto :goto_a2

    .line 83
    :catch_11e
    move-exception v0

    goto :goto_f9

    .line 80
    :catch_120
    move-exception v0

    goto :goto_d0

    .line 78
    :catch_122
    move-exception v0

    goto :goto_b1

    .line 76
    :catch_124
    move-exception v0

    move-object v1, v2

    goto/16 :goto_84
.end method

.method public synthetic call()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 50
    invoke-virtual {p0}, LUn;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
