.class public LcN;
.super Ljava/lang/Object;
.source "GoogleTracker.java"

# interfaces
.implements Ldj;


# static fields
.field private static final a:Ljava/text/DecimalFormat;


# instance fields
.field private final a:LcP;

.field private final a:Ldk;

.field private a:Z

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 24
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.######"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    sput-object v0, LcN;->a:Ljava/text/DecimalFormat;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ldk;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v0, p0, LcN;->a:Z

    .line 31
    iput-boolean v0, p0, LcN;->b:Z

    .line 34
    if-nez p1, :cond_12

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackingId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_12
    iput-object p2, p0, LcN;->a:Ldk;

    .line 38
    new-instance v0, LcP;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LcP;-><init>(LcO;)V

    iput-object v0, p0, LcN;->a:LcP;

    .line 40
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "trackingId"

    invoke-virtual {v0, v1, p1}, LcP;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "sampleRate"

    const-string v2, "100"

    invoke-virtual {v0, v1, v2}, LcP;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "sessionControl"

    const-string v2, "start"

    invoke-virtual {v0, v1, v2}, LcP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Ljava/util/Map;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 156
    const-string v1, "eventCategory"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const-string v1, "eventAction"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    const-string v1, "eventLabel"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    if-eqz p3, :cond_23

    .line 160
    const-string v1, "eventValue"

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    :cond_23
    return-object v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 59
    iget-boolean v0, p0, LcN;->a:Z

    if-eqz v0, :cond_c

    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tracker closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_c
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 244
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "trackingId"

    invoke-virtual {v0, v1}, LcP;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Z)Ljava/util/Map;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 202
    const-string v1, "exDescription"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    const-string v1, "exFatal"

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 91
    invoke-direct {p0}, LcN;->b()V

    .line 92
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "description"

    invoke-virtual {v0, v1}, LcP;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "trackerEnterScreen requires a appScreen to be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_19
    const-string v0, "appview"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LcN;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 96
    return-void
.end method

.method public a(D)V
    .registers 6
    .parameter

    .prologue
    .line 254
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "sampleRate"

    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LcP;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 292
    iget-boolean v0, p0, LcN;->b:Z

    if-eqz v0, :cond_a

    .line 293
    const-string v0, "Tracking already started, setAppName call ignored"

    invoke-static {v0}, LcT;->i(Ljava/lang/String;)I

    .line 302
    :goto_9
    return-void

    .line 296
    :cond_a
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 297
    const-string v0, "setting appName to empty value not allowed, call ignored"

    invoke-static {v0}, LcT;->i(Ljava/lang/String;)I

    goto :goto_9

    .line 300
    :cond_16
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "appName"

    invoke-virtual {v0, v1, p1}, LcP;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    const-string v0, "event"

    invoke-static {p1, p2, p3, p4}, LcN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LcN;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 108
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/Map;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 66
    iput-boolean v1, p0, LcN;->b:Z

    .line 67
    invoke-direct {p0}, LcN;->b()V

    .line 68
    if-nez p2, :cond_d

    .line 69
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 71
    :cond_d
    const-string v0, "hitType"

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, LcN;->a:LcP;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LcP;->a(Ljava/util/Map;Ljava/lang/Boolean;)V

    .line 73
    iget-object v0, p0, LcN;->a:Ldk;

    iget-object v1, p0, LcN;->a:LcP;

    invoke-virtual {v1}, LcP;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ldk;->a(Ljava/util/Map;)V

    .line 74
    iget-object v0, p0, LcN;->a:LcP;

    invoke-virtual {v0}, LcP;->a()V

    .line 75
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 121
    const-string v0, "exception"

    invoke-virtual {p0, p1, p2}, LcN;->a(Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LcN;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 122
    return-void
.end method

.method public a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, LcN;->b()V

    .line 80
    iget-object v1, p0, LcN;->a:LcP;

    const-string v2, "sessionControl"

    if-eqz p1, :cond_f

    const-string v0, "start"

    :goto_b
    invoke-virtual {v1, v2, v0}, LcP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    return-void

    .line 80
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 309
    iget-boolean v0, p0, LcN;->b:Z

    if-eqz v0, :cond_a

    .line 310
    const-string v0, "Tracking already started, setAppVersion call ignored"

    invoke-static {v0}, LcT;->i(Ljava/lang/String;)I

    .line 314
    :goto_9
    return-void

    .line 313
    :cond_a
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "appVersion"

    invoke-virtual {v0, v1, p1}, LcP;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9
.end method

.method public b(Z)V
    .registers 5
    .parameter

    .prologue
    .line 249
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "anonymizeIp"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LcP;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 100
    invoke-direct {p0}, LcN;->b()V

    .line 101
    invoke-virtual {p0, p1}, LcN;->e(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, LcN;->a()V

    .line 103
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 269
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "referrer"

    invoke-virtual {v0, v1, p1}, LcP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, LcN;->b()V

    .line 86
    iget-object v0, p0, LcN;->a:LcP;

    const-string v1, "description"

    invoke-virtual {v0, v1, p1}, LcP;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method
