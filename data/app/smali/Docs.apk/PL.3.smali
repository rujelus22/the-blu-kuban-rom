.class public LPL;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHelper.java"


# instance fields
.field private final a:I

.field private final a:Landroid/content/Context;

.field private final a:Ljava/lang/String;

.field private final a:[LagF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LagF",
            "<",
            "LPN;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[LagF;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "[",
            "LagF",
            "<",
            "LPN;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 42
    iput-object p1, p0, LPL;->a:Landroid/content/Context;

    .line 43
    iput-object p3, p0, LPL;->a:[LagF;

    .line 44
    iput p4, p0, LPL;->a:I

    .line 45
    iput p5, p0, LPL;->b:I

    .line 46
    iput-object p2, p0, LPL;->a:Ljava/lang/String;

    .line 47
    return-void
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 132
    iget-object v2, p0, LPL;->a:[LagF;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_1c

    aget-object v0, v2, v1

    .line 133
    invoke-interface {v0}, LagF;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPN;

    .line 134
    invoke-virtual {v0, p2}, LPN;->a(I)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 135
    invoke-virtual {v0, p1, p2}, LPN;->c(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 132
    :cond_18
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 138
    :cond_1c
    return-void
.end method

.method private e(Landroid/database/sqlite/SQLiteDatabase;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 179
    iget-object v2, p0, LPL;->a:[LagF;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_1c

    aget-object v0, v2, v1

    .line 180
    invoke-interface {v0}, LagF;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPN;

    .line 181
    invoke-virtual {v0, p2}, LPN;->a(I)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 182
    invoke-virtual {v0, p1, p2}, LPN;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 179
    :cond_18
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 185
    :cond_1c
    return-void
.end method


# virtual methods
.method protected a(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 2
    .parameter

    .prologue
    .line 77
    return-void
.end method

.method a(Landroid/database/sqlite/SQLiteDatabase;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 63
    const-string v0, "DatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating a new database at version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v2, p0, LPL;->a:[LagF;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_2b
    if-ge v1, v3, :cond_42

    aget-object v0, v2, v1

    .line 65
    invoke-interface {v0}, LagF;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPN;

    .line 66
    invoke-virtual {v0, p2}, LPN;->a(I)Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 67
    invoke-virtual {v0, p1, p2}, LPN;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 64
    :cond_3e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2b

    .line 70
    :cond_42
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, LPL;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 203
    return-void
.end method

.method b(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 10
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 161
    const-string v1, "SQLITE_MASTER"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v2, v3

    const-string v3, "type == \'view\'"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 163
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 165
    :goto_18
    :try_start_18
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_46

    .line 166
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 167
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DROP VIEW "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, LPI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 168
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_40
    .catchall {:try_start_18 .. :try_end_40} :catchall_41

    goto :goto_18

    .line 171
    :catchall_41
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_46
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 173
    return-void
.end method

.method b(Landroid/database/sqlite/SQLiteDatabase;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 146
    iget-object v2, p0, LPL;->a:[LagF;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_1c

    aget-object v0, v2, v1

    .line 147
    invoke-interface {v0}, LagF;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPN;

    .line 148
    invoke-virtual {v0, p2}, LPN;->a(I)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 151
    invoke-virtual {v0, p1, p2}, LPN;->d(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146
    :cond_18
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 154
    :cond_1c
    return-void
.end method

.method c(Landroid/database/sqlite/SQLiteDatabase;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 192
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    .line 193
    invoke-direct {p0, p1, v0}, LPL;->e(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 194
    invoke-virtual {p0, p1}, LPL;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 195
    invoke-virtual {p0, p1, p2}, LPL;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 196
    iget v0, p0, LPL;->a:I

    if-ne p2, v0, :cond_14

    .line 197
    invoke-virtual {p0, p1}, LPL;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 199
    :cond_14
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 53
    :try_start_3
    iget v0, p0, LPL;->a:I

    invoke-virtual {p0, p1, v0}, LPL;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 54
    invoke-virtual {p0, p1}, LPL;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 55
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_12

    .line 57
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 59
    return-void

    .line 57
    :catchall_12
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 81
    const-string v0, "DatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upgrading database "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " databaseName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LPL;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget v0, p0, LPL;->a:I

    if-ne p3, v0, :cond_78

    const/4 v0, 0x1

    :goto_41
    const-string v1, "Cannot upgrade database to version other than latest."

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 86
    iget v0, p0, LPL;->b:I

    if-ge p2, v0, :cond_7a

    .line 87
    const-string v0, "DatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current database is too old to upgrade ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LPL;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Wiping all existing data."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {p0, p1, p3}, LPL;->c(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 125
    :goto_77
    return-void

    .line 83
    :cond_78
    const/4 v0, 0x0

    goto :goto_41

    .line 93
    :cond_7a
    const-string v0, "PRAGMA foreign_keys=OFF;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 96
    :try_start_82
    invoke-virtual {p0, p1}, LPL;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 97
    invoke-virtual {p0, p1, p3}, LPL;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 100
    add-int/lit8 v0, p2, 0x1

    :goto_8a
    if-ge v0, p3, :cond_92

    .line 101
    invoke-virtual {p0, p1, v0}, LPL;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_8a

    .line 104
    :cond_92
    invoke-direct {p0, p1, p3}, LPL;->d(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 106
    invoke-virtual {p0, p1}, LPL;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 107
    invoke-direct {p0, p1, p2}, LPL;->e(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 109
    invoke-virtual {p0, p1}, LPL;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_9e
    .catch Ljava/lang/RuntimeException; {:try_start_82 .. :try_end_9e} :catch_aa

    .line 122
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 123
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 124
    const-string v0, "PRAGMA foreign_keys=ON;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_77

    .line 116
    :catch_aa
    move-exception v0

    .line 117
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 118
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 119
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 120
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 207
    const-string v0, "DatabaseHelper[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LPL;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
