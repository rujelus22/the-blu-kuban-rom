.class Lajx;
.super Lajd;
.source "ImmutableSortedMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lajd",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lajs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajs",
            "<*TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lajs;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lajs",
            "<*TV;>;)V"
        }
    .end annotation

    .prologue
    .line 575
    invoke-direct {p0}, Lajd;-><init>()V

    .line 576
    iput-object p1, p0, Lajx;->a:Lajs;

    .line 577
    return-void
.end method


# virtual methods
.method public a()Laml;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laml",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 585
    iget-object v0, p0, Lajx;->a:Lajs;

    invoke-virtual {v0}, Lajs;->a()Laml;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .registers 2

    .prologue
    .line 593
    const/4 v0, 0x1

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 589
    iget-object v0, p0, Lajx;->a:Lajs;

    invoke-virtual {v0, p1}, Lajs;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 571
    invoke-virtual {p0}, Lajx;->a()Laml;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 581
    iget-object v0, p0, Lajx;->a:Lajs;

    invoke-virtual {v0}, Lajs;->size()I

    move-result v0

    return v0
.end method
