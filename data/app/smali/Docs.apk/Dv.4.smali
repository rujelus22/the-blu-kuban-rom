.class public LDv;
.super LDp;
.source "ReplacementFixingEditable.java"


# static fields
.field public static final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LDv;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LDG;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDG",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1}, LDp;-><init>(LDG;)V

    .line 20
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .registers 3
    .parameter

    .prologue
    .line 24
    sget-object v0, LDv;->a:Ljava/lang/Object;

    invoke-super {p0, p1, v0}, LDp;->a(ILjava/lang/Object;)C

    move-result v0

    return v0
.end method

.method public getChars(II[CI)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    sget-object v5, LDv;->a:Ljava/lang/Object;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-super/range {v0 .. v5}, LDp;->a(II[CILjava/lang/Object;)V

    .line 30
    return-void
.end method

.method public getSpans(IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    .line 52
    sget-object v0, LDv;->a:Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, p3, v0}, LDv;->a(IILjava/lang/Class;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextSpanTransition(IILjava/lang/Class;)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    sget-object v0, LDv;->a:Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, p3, v0}, LDv;->a(IILjava/lang/Class;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 34
    sub-int v0, p2, p1

    new-array v0, v0, [C

    .line 36
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LDv;->getChars(II[CI)V

    .line 37
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-virtual {p0}, LDv;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LDv;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
