.class final LapW;
.super LaoY;
.source "InstanceBindingImpl.java"

# interfaces
.implements Lary;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LaoY",
        "<TT;>;",
        "Lary",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lajm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajm",
            "<",
            "Larp;",
            ">;"
        }
    .end annotation
.end field

.field final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<TT;>;"
        }
    .end annotation
.end field

.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LapL;Laop;Ljava/lang/Object;LapY;Ljava/util/Set;Ljava/lang/Object;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Object;",
            "LapY",
            "<+TT;>;",
            "Ljava/util/Set",
            "<",
            "Larp;",
            ">;TT;)V"
        }
    .end annotation

    .prologue
    .line 42
    sget-object v5, LaqC;->d:LaqC;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LaoY;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;)V

    .line 43
    invoke-static {p5}, Lajm;->a(Ljava/util/Collection;)Lajm;

    move-result-object v0

    iput-object v0, p0, LapW;->a:Lajm;

    .line 44
    iput-object p6, p0, LapW;->a:Ljava/lang/Object;

    .line 45
    invoke-static {p6}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v0

    iput-object v0, p0, LapW;->a:Laoz;

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Laop;LaqC;Ljava/util/Set;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Laop",
            "<TT;>;",
            "LaqC;",
            "Ljava/util/Set",
            "<",
            "Larp;",
            ">;TT;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, LaoY;-><init>(Ljava/lang/Object;Laop;LaqC;)V

    .line 51
    invoke-static {p4}, Lajm;->a(Ljava/util/Collection;)Lajm;

    move-result-object v0

    iput-object v0, p0, LapW;->a:Lajm;

    .line 52
    iput-object p5, p0, LapW;->a:Ljava/lang/Object;

    .line 53
    invoke-static {p5}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v0

    iput-object v0, p0, LapW;->a:Laoz;

    .line 54
    return-void
.end method


# virtual methods
.method public a(Laop;)LaoY;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<TT;>;)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, LapW;

    invoke-virtual {p0}, LapW;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, LapW;->a()LaqC;

    move-result-object v3

    iget-object v4, p0, LapW;->a:Lajm;

    iget-object v5, p0, LapW;->a:Ljava/lang/Object;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LapW;-><init>(Ljava/lang/Object;Laop;LaqC;Ljava/util/Set;Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(LaqC;)LaoY;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaqC;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, LapW;

    invoke-virtual {p0}, LapW;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, LapW;->a()Laop;

    move-result-object v2

    iget-object v4, p0, LapW;->a:Lajm;

    iget-object v5, p0, LapW;->a:Ljava/lang/Object;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LapW;-><init>(Ljava/lang/Object;Laop;LaqC;Ljava/util/Set;Ljava/lang/Object;)V

    return-object v0
.end method

.method public a()Laoz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, LapW;->a:Laoz;

    return-object v0
.end method

.method public b()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, LapW;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public c()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, LapW;->a:Ljava/lang/Object;

    instance-of v0, v0, Larn;

    if-eqz v0, :cond_13

    iget-object v0, p0, LapW;->a:Ljava/lang/Object;

    check-cast v0, Larn;

    invoke-interface {v0}, Larn;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lajm;->a(Ljava/util/Collection;)Lajm;

    move-result-object v0

    :goto_12
    return-object v0

    :cond_13
    iget-object v0, p0, LapW;->a:Lajm;

    invoke-static {v0}, Larg;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_12
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 101
    instance-of v1, p1, LapW;

    if-eqz v1, :cond_2e

    .line 102
    check-cast p1, LapW;

    .line 103
    invoke-virtual {p0}, LapW;->a()Laop;

    move-result-object v1

    invoke-virtual {p1}, LapW;->a()Laop;

    move-result-object v2

    invoke-virtual {v1, v2}, Laop;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    invoke-virtual {p0}, LapW;->a()LaqC;

    move-result-object v1

    invoke-virtual {p1}, LapW;->a()LaqC;

    move-result-object v2

    invoke-virtual {v1, v2}, LaqC;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, LapW;->a:Ljava/lang/Object;

    iget-object v2, p1, LapW;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/4 v0, 0x1

    .line 107
    :cond_2e
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 113
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, LapW;->a()Laop;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, LapW;->a()LaqC;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 92
    const-class v0, Lary;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {p0}, LapW;->a()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {p0}, LapW;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "instance"

    iget-object v2, p0, LapW;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
