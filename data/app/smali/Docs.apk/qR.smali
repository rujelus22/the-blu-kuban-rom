.class public LqR;
.super LqT;
.source "DateGrouper.java"


# static fields
.field private static a:[Ljava/lang/CharSequence;


# instance fields
.field private final a:LPI;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/Calendar;

.field private final a:Lrs;

.field private a:[Lnp;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput-object v0, LqR;->a:[Ljava/lang/CharSequence;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;LPI;LqU;J)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 200
    invoke-virtual {p2}, LPI;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, LqT;-><init>(Ljava/lang/String;LqU;)V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, LqR;->a:[Lnp;

    .line 201
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 202
    invoke-virtual {v0, p4, p5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 203
    new-instance v1, Lrs;

    invoke-direct {v1, v0}, Lrs;-><init>(Ljava/util/Calendar;)V

    iput-object v1, p0, LqR;->a:Lrs;

    .line 204
    iput-object v0, p0, LqR;->a:Ljava/util/Calendar;

    .line 205
    iput-object p1, p0, LqR;->a:Landroid/content/Context;

    .line 206
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPI;

    iput-object v0, p0, LqR;->a:LPI;

    .line 207
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;II)Ljava/util/Calendar;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnp;",
            ">;",
            "Landroid/content/res/Resources;",
            "Ljava/util/Calendar;",
            "II)",
            "Ljava/util/Calendar;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 88
    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move v1, v2

    .line 89
    :goto_8
    if-ge v1, p3, :cond_28

    .line 90
    add-int v3, p4, v1

    add-int/lit8 v3, v3, 0x1

    .line 91
    sget v4, Lel;->fast_scroll_time_grouper_n_days_ago:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p1, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 93
    invoke-static {p0, v3, v0}, LqR;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 94
    const/4 v3, 0x5

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 97
    :cond_28
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/Calendar;I)Ljava/util/Calendar;
    .registers 11
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnp;",
            ">;",
            "Ljava/util/Calendar;",
            "I)",
            "Ljava/util/Calendar;"
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x2

    .line 117
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 120
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 121
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 123
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_22

    .line 124
    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->add(II)V

    .line 127
    :cond_22
    const/4 v2, 0x0

    :goto_23
    if-ge v2, p2, :cond_43

    .line 128
    const-string v3, ""

    .line 129
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 130
    sget-object v4, LqR;->a:[Ljava/lang/CharSequence;

    aget-object v3, v4, v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 131
    invoke-static {p0, v3, v0}, LqR;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 132
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 133
    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->add(II)V

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_23

    .line 136
    :cond_43
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/Calendar;ILjava/lang/String;)Ljava/util/Calendar;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnp;",
            ">;",
            "Ljava/util/Calendar;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Calendar;"
        }
    .end annotation

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 142
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 145
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 146
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 148
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-nez v2, :cond_22

    .line 149
    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->add(II)V

    :cond_22
    move v2, v3

    .line 152
    :goto_23
    if-ge v2, p2, :cond_47

    .line 153
    const-string v4, "yyyy"

    invoke-static {v4, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 154
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v4, v5, v3

    invoke-static {p3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v0}, LqR;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 155
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 156
    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->add(II)V

    .line 152
    add-int/lit8 v2, v2, 0x1

    goto :goto_23

    .line 159
    :cond_47
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnp;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Calendar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v2, Lnp;

    if-eqz p2, :cond_13

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    :goto_8
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, p1, v0}, Lnp;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    return-void

    .line 82
    :cond_13
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_8
.end method

.method private static a(Ljava/util/Calendar;Landroid/content/res/Resources;Ljava/util/ArrayList;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Landroid/content/res/Resources;",
            "Ljava/util/ArrayList",
            "<",
            "Lnp;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 166
    sget v0, Len;->fast_scroll_time_grouper_today:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, LqR;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 169
    invoke-virtual {p0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 170
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 171
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 172
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 173
    sget v1, Len;->fast_scroll_time_grouper_yesterday:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1, v0}, LqR;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 176
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 177
    const/4 v1, -0x1

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->add(II)V

    .line 180
    invoke-static {p2, p1, v0, v4, v3}, LqR;->a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;II)Ljava/util/Calendar;

    move-result-object v0

    .line 183
    invoke-static {p2, p1, v0, v3, v3}, LqR;->b(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;II)Ljava/util/Calendar;

    move-result-object v0

    .line 186
    const/4 v1, 0x4

    invoke-static {p2, v0, v1}, LqR;->a(Ljava/util/ArrayList;Ljava/util/Calendar;I)Ljava/util/Calendar;

    move-result-object v0

    .line 189
    sget v1, Len;->fast_scroll_time_grouper_in_year:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 190
    const/4 v2, 0x3

    invoke-static {p2, v0, v2, v1}, LqR;->a(Ljava/util/ArrayList;Ljava/util/Calendar;ILjava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 193
    sget v1, Len;->fast_scroll_time_grouper_earlier:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1, v0}, LqR;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 194
    return-void
.end method

.method private static b(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;II)Ljava/util/Calendar;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnp;",
            ">;",
            "Landroid/content/res/Resources;",
            "Ljava/util/Calendar;",
            "II)",
            "Ljava/util/Calendar;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 102
    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move v1, v2

    .line 103
    :goto_8
    if-ge v1, p3, :cond_28

    .line 104
    add-int v3, p4, v1

    add-int/lit8 v3, v3, 0x1

    .line 105
    sget v4, Lel;->fast_scroll_time_grouper_n_weeks_ago:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p1, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 107
    invoke-static {p0, v3, v0}, LqR;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 108
    const/4 v3, 0x5

    const/4 v4, -0x7

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 103
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 111
    :cond_28
    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 229
    const/16 v0, 0x16

    return v0
.end method

.method public a()LPI;
    .registers 2

    .prologue
    .line 239
    iget-object v0, p0, LqR;->a:LPI;

    return-object v0
.end method

.method public a()Landroid/widget/SectionIndexer;
    .registers 5

    .prologue
    .line 224
    new-instance v0, Lnn;

    iget-object v1, p0, LqR;->a:Landroid/database/Cursor;

    invoke-virtual {p0}, LqR;->c()I

    move-result v2

    invoke-virtual {p0}, LqR;->a()[Lnp;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lnn;-><init>(Landroid/database/Cursor;I[Lnp;)V

    return-object v0
.end method

.method public a()Lri;
    .registers 6

    .prologue
    .line 211
    const-wide/16 v0, -0x1

    .line 212
    iget-object v2, p0, LqR;->a:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 213
    iget-object v0, p0, LqR;->a:Landroid/database/Cursor;

    invoke-virtual {p0}, LqR;->c()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 214
    iget-object v2, p0, LqR;->a:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 219
    :goto_19
    iget-object v2, p0, LqR;->a:Lrs;

    iget-object v3, p0, LqR;->a:Landroid/database/Cursor;

    invoke-virtual {p0}, LqR;->c()I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lrs;->a(JJ)Lri;

    move-result-object v0

    return-object v0

    .line 216
    :cond_2a
    iget-object v2, p0, LqR;->a:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_19
.end method

.method public declared-synchronized a()[Lnp;
    .registers 4

    .prologue
    .line 59
    monitor-enter p0

    :try_start_1
    sget-object v0, LqR;->a:[Ljava/lang/CharSequence;

    if-nez v0, :cond_25

    .line 60
    iget-object v0, p0, LqR;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Led;->fast_scroll_time_grouper_in_month:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    sput-object v0, LqR;->a:[Ljava/lang/CharSequence;

    .line 63
    sget-object v0, LqR;->a:[Ljava/lang/CharSequence;

    array-length v0, v0

    iget-object v1, p0, LqR;->a:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    if-le v0, v1, :cond_4a

    const/4 v0, 0x1

    :goto_20
    const-string v1, "Insufficient number of months in the resources"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 68
    :cond_25
    iget-object v0, p0, LqR;->a:[Lnp;

    if-nez v0, :cond_46

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 72
    iget-object v1, p0, LqR;->a:Ljava/util/Calendar;

    iget-object v2, p0, LqR;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2, v0}, LqR;->a(Ljava/util/Calendar;Landroid/content/res/Resources;Ljava/util/ArrayList;)V

    .line 74
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lnp;

    iput-object v1, p0, LqR;->a:[Lnp;

    .line 75
    iget-object v1, p0, LqR;->a:[Lnp;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 77
    :cond_46
    iget-object v0, p0, LqR;->a:[Lnp;
    :try_end_48
    .catchall {:try_start_1 .. :try_end_48} :catchall_4c

    monitor-exit p0

    return-object v0

    .line 63
    :cond_4a
    const/4 v0, 0x0

    goto :goto_20

    .line 59
    :catchall_4c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 234
    const/16 v0, 0x140

    return v0
.end method
