.class public LlQ;
.super LlC;
.source "OrganizeOperation.java"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LkO;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    const-string v0, "moveOperation"

    invoke-direct {p0, p1, v0}, LlC;-><init>(LkO;Ljava/lang/String;)V

    .line 51
    if-nez p2, :cond_9

    if-eqz p3, :cond_12

    :cond_9
    const/4 v0, 0x1

    :goto_a
    invoke-static {v0}, Lagu;->a(Z)V

    .line 52
    iput-object p3, p0, LlQ;->c:Ljava/lang/String;

    .line 53
    iput-object p2, p0, LlQ;->d:Ljava/lang/String;

    .line 54
    return-void

    .line 51
    :cond_12
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 164
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 165
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 167
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public static a(LkO;Lorg/json/JSONObject;)LlQ;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 171
    const-string v0, "fromCollectionResourceId"

    invoke-static {p1, v0}, LlQ;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    const-string v1, "toCollectionResourceId"

    invoke-static {p1, v1}, LlQ;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    new-instance v2, LlQ;

    invoke-direct {v2, p0, v0, v1}, LlQ;-><init>(LkO;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    if-eqz p2, :cond_5

    .line 155
    invoke-virtual {p0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 157
    :cond_5
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LlR;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 116
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LlQ;->a(Ljava/lang/String;Ljava/lang/String;LlR;Z)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LlR;Z)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 127
    invoke-virtual {p3}, LlR;->a()LlS;

    move-result-object v0

    .line 128
    invoke-virtual {p0}, LlQ;->c()Ljava/lang/String;

    move-result-object v1

    .line 131
    if-eqz p4, :cond_15

    .line 132
    invoke-static {v1, p1}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    sget-object v2, LlL;->a:LlL;

    invoke-interface {v0, v1, p2, v2}, LlS;->a(LkY;Ljava/lang/String;LlK;)Z

    move-result v0

    .line 140
    :goto_14
    return v0

    .line 136
    :cond_15
    invoke-static {v1, p1}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    const-string v2, "*"

    sget-object v3, LlL;->a:LlL;

    invoke-interface {v0, v1, p2, v2, v3}, LlS;->a(LkY;Ljava/lang/String;Ljava/lang/String;LlK;)Z

    move-result v0

    goto :goto_14
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;LlR;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LlQ;->a(Ljava/lang/String;Ljava/lang/String;LlR;Z)Z

    move-result v0

    return v0
.end method

.method private e()Ljava/lang/String;
    .registers 5

    .prologue
    .line 62
    const-string v0, "organize: remove from: %s, add to: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LlQ;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LlQ;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, LlC;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, LlQ;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Llf;LkO;)LlB;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-virtual {p0}, LlQ;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 69
    iget-object v0, p0, LlQ;->c:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 70
    iget-object v0, p0, LlQ;->c:Ljava/lang/String;

    invoke-interface {p1, v1, v0}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_1b

    .line 72
    invoke-interface {p1, p2, v0}, Llf;->a(LkO;LkH;)LkX;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, LkX;->c()V

    .line 76
    :cond_1b
    iget-object v0, p0, LlQ;->d:Ljava/lang/String;

    if-eqz v0, :cond_55

    .line 77
    invoke-interface {p1, p2}, Llf;->a(LkO;)Ljava/util/Map;

    move-result-object v2

    .line 78
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2b
    :goto_2b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 79
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p1, v1, v4, v5}, Llf;->a(LkB;J)LkH;

    move-result-object v4

    .line 80
    invoke-virtual {v4}, LkH;->i()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LlQ;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 81
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkX;

    invoke-virtual {v0}, LkX;->b()V

    goto :goto_2b

    .line 85
    :cond_55
    new-instance v0, LlQ;

    iget-object v1, p0, LlQ;->c:Ljava/lang/String;

    iget-object v2, p0, LlQ;->d:Ljava/lang/String;

    invoke-direct {v0, p2, v1, v2}, LlQ;-><init>(LkO;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .registers 4

    .prologue
    .line 145
    invoke-super {p0}, LlC;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 146
    const-string v1, "operationName"

    const-string v2, "moveOperation"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 147
    const-string v1, "fromCollectionResourceId"

    iget-object v2, p0, LlQ;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LlQ;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v1, "toCollectionResourceId"

    iget-object v2, p0, LlQ;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LlQ;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    return-object v0
.end method

.method public a(LlK;LlR;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 96
    .line 99
    invoke-virtual {p0}, LlQ;->b()Ljava/lang/String;

    move-result-object v2

    .line 100
    iget-object v0, p0, LlQ;->c:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 101
    iget-object v0, p0, LlQ;->c:Ljava/lang/String;

    invoke-direct {p0, v2, v0, p2}, LlQ;->a(Ljava/lang/String;Ljava/lang/String;LlR;)Z

    move-result v0

    .line 102
    if-nez v0, :cond_14

    .line 103
    const/4 v0, 0x0

    .line 111
    :cond_12
    :goto_12
    return v0

    :cond_13
    move v0, v1

    .line 106
    :cond_14
    iget-object v3, p0, LlQ;->d:Ljava/lang/String;

    if-eqz v3, :cond_1e

    .line 107
    iget-object v1, p0, LlQ;->d:Ljava/lang/String;

    invoke-direct {p0, v2, v1, p2}, LlQ;->b(Ljava/lang/String;Ljava/lang/String;LlR;)Z

    move-result v1

    .line 111
    :cond_1e
    iget-object v2, p0, LlQ;->c:Ljava/lang/String;

    if-nez v2, :cond_12

    move v0, v1

    goto :goto_12
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LlQ;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, LlQ;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    return-object v0
.end method
