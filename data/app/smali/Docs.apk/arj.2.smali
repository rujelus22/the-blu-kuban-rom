.class public final Larj;
.super Ljava/lang/Object;
.source "Elements.java"


# static fields
.field private static final a:Lara;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lara",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 65
    new-instance v0, Lark;

    invoke-direct {v0}, Lark;-><init>()V

    sput-object v0, Larj;->a:Lara;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    return-void
.end method

.method public static a(LaoK;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaoK;",
            "Ljava/lang/Iterable",
            "<+",
            "Laov;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lari;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v1, Larl;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Larl;-><init>(LaoK;Lark;)V

    .line 102
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laov;

    .line 103
    invoke-virtual {v1, v0}, Larl;->a(Laov;)V

    goto :goto_a

    .line 105
    :cond_1a
    invoke-static {v1}, Larl;->a(Larl;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/List;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Laov;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lari;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, LaoK;->b:LaoK;

    invoke-static {v0, p0}, Larj;->a(LaoK;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
