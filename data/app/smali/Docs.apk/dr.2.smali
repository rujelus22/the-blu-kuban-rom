.class public Ldr;
.super Ljava/lang/Object;
.source "ReporterDefault.java"

# interfaces
.implements Ldq;


# instance fields
.field private a:I

.field private a:Ldu;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ldy;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/ExecutorService;

.field private b:I

.field private b:Ljava/lang/String;

.field private c:I

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldu;III)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldr;->a:Ljava/util/LinkedHashMap;

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Ldr;->b:I

    .line 77
    iput-object p1, p0, Ldr;->a:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Ldr;->b:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Ldr;->c:Ljava/lang/String;

    .line 80
    iput-object p4, p0, Ldr;->a:Ldu;

    .line 81
    iput p5, p0, Ldr;->a:I

    .line 82
    iput p6, p0, Ldr;->c:I

    .line 83
    iput p7, p0, Ldr;->d:I

    .line 84
    invoke-direct {p0}, Ldr;->b()V

    .line 85
    return-void
.end method

.method private a(I)Ljava/util/List;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ldy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 147
    const/4 v0, 0x0

    :goto_6
    if-ge v0, p1, :cond_14

    .line 148
    iget-object v2, p0, Ldr;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 150
    :cond_14
    return-object v1
.end method

.method private b()V
    .registers 3

    .prologue
    .line 91
    const-string v0, "v"

    iget-object v1, p0, Ldr;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Ldr;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v0, "s"

    iget-object v1, p0, Ldr;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Ldr;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    iget v1, p0, Ldr;->a:I

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Ldr;->a:Ljava/util/concurrent/BlockingQueue;

    .line 94
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ldr;->a:Ljava/util/concurrent/ExecutorService;

    .line 95
    iget-object v0, p0, Ldr;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lds;

    invoke-direct {v1, p0}, Lds;-><init>(Ldr;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 101
    return-void
.end method


# virtual methods
.method a(Ljava/util/List;)Ljava/util/Map;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ldy;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 179
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 180
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 181
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldy;

    .line 182
    invoke-virtual {v0}, Ldy;->a()Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 184
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 186
    :cond_2e
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 187
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-interface {v2, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_e

    .line 191
    :cond_3a
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_42
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 192
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 193
    new-instance v5, Ljava/util/LinkedHashMap;

    iget-object v2, p0, Ldr;->a:Ljava/util/LinkedHashMap;

    invoke-direct {v5, v2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 196
    const/4 v2, 0x0

    :try_start_5c
    new-array v2, v2, [Ldy;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ldy;

    invoke-static {v2}, Ldy;->a([Ldy;)Ljava/util/Map;
    :try_end_67
    .catch Ldm; {:try_start_5c .. :try_end_67} :catch_73

    move-result-object v1

    .line 201
    invoke-interface {v5, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 202
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_42

    .line 197
    :catch_73
    move-exception v0

    .line 198
    const-string v2, "ReporterDefault"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to merge tickers:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_42

    .line 204
    :cond_8d
    return-object v3
.end method

.method a()V
    .registers 5

    .prologue
    .line 117
    .line 118
    :cond_0
    const/4 v1, 0x0

    .line 120
    :try_start_1
    iget v0, p0, Ldr;->b:I

    invoke-direct {p0, v0}, Ldr;->a(I)Ljava/util/List;
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_6} :catch_25

    move-result-object v0

    .line 125
    :goto_7
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0, v0}, Ldr;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 130
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 131
    invoke-virtual {p0, v0}, Ldr;->a(Ljava/util/Map;)Z

    goto :goto_15

    .line 121
    :catch_25
    move-exception v0

    .line 122
    const-string v2, "ReporterDefault"

    const-string v3, "reporter interrupted"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move-object v0, v1

    goto :goto_7
.end method

.method public a(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 306
    if-ge p1, v0, :cond_22

    .line 307
    const-string v1, "ReporterDefault"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "too small batch size :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", changed to 1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v0

    .line 311
    :cond_22
    iget v0, p0, Ldr;->a:I

    if-le p1, v0, :cond_44

    .line 312
    const-string v0, "ReporterDefault"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "batch size :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bigger than buffer size, change to buffer limit"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_44
    iput p1, p0, Ldr;->b:I

    .line 315
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 286
    iget-object v0, p0, Ldr;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    return-void
.end method

.method public a(Ldy;)Z
    .registers 3
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Ldr;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method a(Ljava/util/Map;)Z
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 214
    .line 215
    iget v0, p0, Ldr;->c:I

    move v3, v0

    move v0, v1

    :goto_5
    if-nez v0, :cond_4c

    if-lez v3, :cond_4c

    .line 217
    :try_start_9
    iget v2, p0, Ldr;->d:I

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 218
    iget-object v2, p0, Ldr;->a:Ldu;

    iget-object v4, p0, Ldr;->a:Ljava/lang/String;

    invoke-interface {v2, v4, p1}, Ldu;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_16
    .catch Ldv; {:try_start_9 .. :try_end_16} :catch_1b
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_16} :catch_3c

    .line 219
    const/4 v0, 0x1

    .line 215
    :goto_17
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_5

    .line 220
    :catch_1b
    move-exception v0

    .line 222
    const-string v2, "ReporterDefault"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed to send report"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 226
    goto :goto_17

    .line 223
    :catch_3c
    move-exception v2

    .line 224
    const-string v4, "ReporterDefault"

    const-string v5, "interrupted in sendReport()"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 225
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_17

    .line 228
    :cond_4c
    return v0
.end method
