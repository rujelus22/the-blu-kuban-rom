.class public Lnu;
.super Ljava/lang/Object;
.source "CreateEntryDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/app/AlertDialog;

.field final synthetic a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

.field final synthetic a:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;Lcom/google/android/apps/docs/app/CreateNewDocActivity;Ljava/util/List;Landroid/app/AlertDialog;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 249
    iput-object p1, p0, Lnu;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    iput-object p2, p0, Lnu;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iput-object p3, p0, Lnu;->a:Ljava/util/List;

    iput-object p4, p0, Lnu;->a:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lnu;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->b(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, Lnu;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 253
    iget-object v0, p0, Lnu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p3, v0}, Lagu;->a(II)I

    .line 254
    iget-object v0, p0, Lnu;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnw;

    iget-object v1, p0, Lnu;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-interface {v0, v1}, Lnw;->a(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V

    .line 257
    iget-object v0, p0, Lnu;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 258
    return-void
.end method
