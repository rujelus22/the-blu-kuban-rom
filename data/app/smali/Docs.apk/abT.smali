.class public LabT;
.super Landroid/widget/HorizontalScrollView;
.source "LinearLayoutListView.java"


# instance fields
.field a:I

.field final synthetic a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

.field b:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 184
    iput-object p1, p0, LabT;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    .line 185
    invoke-direct {p0, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 186
    invoke-virtual {p0}, LabT;->getScrollX()I

    move-result v0

    iput v0, p0, LabT;->a:I

    .line 187
    invoke-virtual {p0}, LabT;->getScrollY()I

    move-result v0

    iput v0, p0, LabT;->b:I

    .line 188
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 192
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 193
    invoke-virtual {p0}, LabT;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 197
    iget v0, p0, LabT;->a:I

    if-ne v0, p1, :cond_12

    iget v0, p0, LabT;->b:I

    if-eq v0, p2, :cond_1b

    .line 198
    :cond_12
    iput p1, p0, LabT;->a:I

    .line 199
    iput p2, p0, LabT;->b:I

    .line 200
    iget-object v0, p0, LabT;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(IIII)V

    .line 202
    :cond_1b
    return-void
.end method
