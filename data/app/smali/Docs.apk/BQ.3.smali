.class public LBQ;
.super Ljava/lang/Object;
.source "TextSelectionPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)V
    .registers 2
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lcom/google/android/apps/docs/editors/kix/KixEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, LsD;->follow_link:I

    if-ne v1, v2, :cond_2e

    if-eqz v0, :cond_2e

    .line 48
    iget-object v1, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lzc;

    move-result-object v1

    if-eqz v1, :cond_26

    .line 49
    iget-object v1, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lzc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lzc;->a(Ljava/lang/String;)V

    .line 78
    :goto_25
    return-void

    .line 51
    :cond_26
    const-string v0, "TextSelectionPopup"

    const-string v1, "The link navigator is not set."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_25

    .line 53
    :cond_2e
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->comment:I

    if-ne v0, v1, :cond_7b

    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lcom/google/android/apps/docs/editors/kix/KixEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->g()Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 56
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lth;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 57
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lth;

    move-result-object v0

    invoke-virtual {v0}, Lth;->a()Ljava/lang/String;

    move-result-object v1

    .line 63
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    .line 64
    if-eqz v1, :cond_73

    .line 66
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Ljava/lang/String;)V

    .line 71
    :goto_65
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->v()V

    goto :goto_25

    .line 59
    :cond_6b
    const-string v0, "TextSelectionPopup"

    const-string v1, "The discussion anchor manager is not set."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_25

    .line 69
    :cond_73
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->v()V

    goto :goto_65

    .line 72
    :cond_7b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->editPopup:I

    if-ne v0, v1, :cond_9e

    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lcom/google/android/apps/docs/editors/kix/KixEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 73
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lcom/google/android/apps/docs/editors/kix/KixEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c()V

    .line 74
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->v()V

    goto :goto_25

    .line 76
    :cond_9e
    iget-object v0, p0, LBQ;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lcom/google/android/apps/docs/editors/kix/KixEditText;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b(I)Z

    goto/16 :goto_25
.end method
