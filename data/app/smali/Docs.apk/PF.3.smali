.class public final enum LPF;
.super Ljava/lang/Enum;
.source "CollectionTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPF;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPF;

.field private static final synthetic a:[LPF;

.field public static final enum b:LPF;

.field public static final enum c:LPF;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0xe

    const/4 v6, 0x0

    .line 121
    new-instance v0, LPF;

    const-string v1, "DAYS_TO_SYNC"

    invoke-static {}, LPE;->b()LPE;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "daysToSync"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LPF;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPF;->a:LPF;

    .line 124
    new-instance v0, LPF;

    const-string v1, "SYNC_NEW_DOCS_BY_DEFAULT"

    invoke-static {}, LPE;->b()LPE;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "syncNewDocsByDefault"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPF;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPF;->b:LPF;

    .line 127
    new-instance v0, LPF;

    const-string v1, "ENTRY_ID"

    invoke-static {}, LPE;->b()LPE;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "entryId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {v3, v4}, LQa;->a([Ljava/lang/String;)LQa;

    move-result-object v3

    invoke-static {}, LPW;->a()LPW;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LPF;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPF;->c:LPF;

    .line 120
    const/4 v0, 0x3

    new-array v0, v0, [LPF;

    sget-object v1, LPF;->a:LPF;

    aput-object v1, v0, v6

    sget-object v1, LPF;->b:LPF;

    aput-object v1, v0, v8

    sget-object v1, LPF;->c:LPF;

    aput-object v1, v0, v9

    sput-object v0, LPF;->a:[LPF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 135
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPF;->a:LPI;

    .line 136
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPF;
    .registers 2
    .parameter

    .prologue
    .line 120
    const-class v0, LPF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPF;

    return-object v0
.end method

.method public static values()[LPF;
    .registers 1

    .prologue
    .line 120
    sget-object v0, LPF;->a:[LPF;

    invoke-virtual {v0}, [LPF;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPF;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, LPF;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 120
    invoke-virtual {p0}, LPF;->a()LPI;

    move-result-object v0

    return-object v0
.end method
