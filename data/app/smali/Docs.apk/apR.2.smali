.class final enum LapR;
.super Ljava/lang/Enum;
.source "InjectorImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LapR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LapR;

.field private static final synthetic a:[LapR;

.field public static final enum b:LapR;

.field public static final enum c:LapR;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    new-instance v0, LapR;

    const-string v1, "NO_JIT"

    invoke-direct {v0, v1, v2}, LapR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapR;->a:LapR;

    .line 100
    new-instance v0, LapR;

    const-string v1, "EXISTING_JIT"

    invoke-direct {v0, v1, v3}, LapR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapR;->b:LapR;

    .line 102
    new-instance v0, LapR;

    const-string v1, "NEW_OR_EXISTING_JIT"

    invoke-direct {v0, v1, v4}, LapR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LapR;->c:LapR;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [LapR;

    sget-object v1, LapR;->a:LapR;

    aput-object v1, v0, v2

    sget-object v1, LapR;->b:LapR;

    aput-object v1, v0, v3

    sget-object v1, LapR;->c:LapR;

    aput-object v1, v0, v4

    sput-object v0, LapR;->a:[LapR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LapR;
    .registers 2
    .parameter

    .prologue
    .line 96
    const-class v0, LapR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LapR;

    return-object v0
.end method

.method public static values()[LapR;
    .registers 1

    .prologue
    .line 96
    sget-object v0, LapR;->a:[LapR;

    invoke-virtual {v0}, [LapR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LapR;

    return-object v0
.end method
