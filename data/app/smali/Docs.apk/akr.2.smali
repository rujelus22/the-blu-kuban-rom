.class abstract enum Lakr;
.super Ljava/lang/Enum;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lakr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lakr;

.field private static final synthetic a:[Lakr;

.field static final a:[[Lakr;

.field public static final enum b:Lakr;

.field public static final enum c:Lakr;

.field public static final enum d:Lakr;

.field public static final enum e:Lakr;

.field public static final enum f:Lakr;

.field public static final enum g:Lakr;

.field public static final enum h:Lakr;

.field public static final enum i:Lakr;

.field public static final enum j:Lakr;

.field public static final enum k:Lakr;

.field public static final enum l:Lakr;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 353
    new-instance v0, Laks;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Laks;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->a:Lakr;

    .line 360
    new-instance v0, Lakw;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, Lakw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->b:Lakr;

    .line 375
    new-instance v0, Lakx;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, Lakx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->c:Lakr;

    .line 390
    new-instance v0, Laky;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, Laky;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->d:Lakr;

    .line 407
    new-instance v0, Lakz;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v7}, Lakz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->e:Lakr;

    .line 414
    new-instance v0, LakA;

    const-string v1, "SOFT_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LakA;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->f:Lakr;

    .line 429
    new-instance v0, LakB;

    const-string v1, "SOFT_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LakB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->g:Lakr;

    .line 444
    new-instance v0, LakC;

    const-string v1, "SOFT_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LakC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->h:Lakr;

    .line 461
    new-instance v0, LakD;

    const-string v1, "WEAK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LakD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->i:Lakr;

    .line 468
    new-instance v0, Lakt;

    const-string v1, "WEAK_EXPIRABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lakt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->j:Lakr;

    .line 483
    new-instance v0, Laku;

    const-string v1, "WEAK_EVICTABLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Laku;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->k:Lakr;

    .line 498
    new-instance v0, Lakv;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lakv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakr;->l:Lakr;

    .line 352
    const/16 v0, 0xc

    new-array v0, v0, [Lakr;

    sget-object v1, Lakr;->a:Lakr;

    aput-object v1, v0, v3

    sget-object v1, Lakr;->b:Lakr;

    aput-object v1, v0, v4

    sget-object v1, Lakr;->c:Lakr;

    aput-object v1, v0, v5

    sget-object v1, Lakr;->d:Lakr;

    aput-object v1, v0, v6

    sget-object v1, Lakr;->e:Lakr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lakr;->f:Lakr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lakr;->g:Lakr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lakr;->h:Lakr;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lakr;->i:Lakr;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lakr;->j:Lakr;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lakr;->k:Lakr;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lakr;->l:Lakr;

    aput-object v2, v0, v1

    sput-object v0, Lakr;->a:[Lakr;

    .line 525
    new-array v0, v6, [[Lakr;

    new-array v1, v7, [Lakr;

    sget-object v2, Lakr;->a:Lakr;

    aput-object v2, v1, v3

    sget-object v2, Lakr;->b:Lakr;

    aput-object v2, v1, v4

    sget-object v2, Lakr;->c:Lakr;

    aput-object v2, v1, v5

    sget-object v2, Lakr;->d:Lakr;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v7, [Lakr;

    sget-object v2, Lakr;->e:Lakr;

    aput-object v2, v1, v3

    sget-object v2, Lakr;->f:Lakr;

    aput-object v2, v1, v4

    sget-object v2, Lakr;->g:Lakr;

    aput-object v2, v1, v5

    sget-object v2, Lakr;->h:Lakr;

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Lakr;

    sget-object v2, Lakr;->i:Lakr;

    aput-object v2, v1, v3

    sget-object v2, Lakr;->j:Lakr;

    aput-object v2, v1, v4

    sget-object v2, Lakr;->k:Lakr;

    aput-object v2, v1, v5

    sget-object v2, Lakr;->l:Lakr;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lakr;->a:[[Lakr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILako;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Lakr;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(LakX;ZZ)Lakr;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 533
    if-eqz p1, :cond_13

    const/4 v1, 0x1

    :goto_4
    if-eqz p2, :cond_7

    const/4 v0, 0x2

    :cond_7
    or-int/2addr v0, v1

    .line 534
    sget-object v1, Lakr;->a:[[Lakr;

    invoke-virtual {p0}, LakX;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_13
    move v1, v0

    .line 533
    goto :goto_4
.end method

.method public static valueOf(Ljava/lang/String;)Lakr;
    .registers 2
    .parameter

    .prologue
    .line 352
    const-class v0, Lakr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lakr;

    return-object v0
.end method

.method public static values()[Lakr;
    .registers 1

    .prologue
    .line 352
    sget-object v0, Lakr;->a:[Lakr;

    invoke-virtual {v0}, [Lakr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lakr;

    return-object v0
.end method


# virtual methods
.method a(LakR;LakQ;LakQ;)LakQ;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LakR",
            "<TK;TV;>;",
            "LakQ",
            "<TK;TV;>;",
            "LakQ",
            "<TK;TV;>;)",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 557
    invoke-interface {p2}, LakQ;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, LakQ;->a()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lakr;->a(LakR;Ljava/lang/Object;ILakQ;)LakQ;

    move-result-object v0

    return-object v0
.end method

.method abstract a(LakR;Ljava/lang/Object;ILakQ;)LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LakR",
            "<TK;TV;>;TK;I",
            "LakQ",
            "<TK;TV;>;)",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method a(LakQ;LakQ;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LakQ",
            "<TK;TV;>;",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 564
    invoke-interface {p1}, LakQ;->a()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, LakQ;->a(J)V

    .line 566
    invoke-interface {p1}, LakQ;->c()LakQ;

    move-result-object v0

    invoke-static {v0, p2}, Lakn;->a(LakQ;LakQ;)V

    .line 567
    invoke-interface {p1}, LakQ;->b()LakQ;

    move-result-object v0

    invoke-static {p2, v0}, Lakn;->a(LakQ;LakQ;)V

    .line 569
    invoke-static {p1}, Lakn;->b(LakQ;)V

    .line 570
    return-void
.end method

.method b(LakQ;LakQ;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LakQ",
            "<TK;TV;>;",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 576
    invoke-interface {p1}, LakQ;->e()LakQ;

    move-result-object v0

    invoke-static {v0, p2}, Lakn;->b(LakQ;LakQ;)V

    .line 577
    invoke-interface {p1}, LakQ;->d()LakQ;

    move-result-object v0

    invoke-static {p2, v0}, Lakn;->b(LakQ;LakQ;)V

    .line 579
    invoke-static {p1}, Lakn;->c(LakQ;)V

    .line 580
    return-void
.end method
