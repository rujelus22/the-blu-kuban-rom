.class public final enum LSC;
.super Ljava/lang/Enum;
.source "SearchHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LSC;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LSC;

.field private static final synthetic a:[LSC;

.field public static final enum b:LSC;

.field public static final enum c:LSC;

.field public static final enum d:LSC;

.field public static final enum e:LSC;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, LSC;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LSC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LSC;->a:LSC;

    .line 14
    new-instance v0, LSC;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LSC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LSC;->b:LSC;

    .line 15
    new-instance v0, LSC;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LSC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LSC;->c:LSC;

    .line 16
    new-instance v0, LSC;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v5}, LSC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LSC;->d:LSC;

    .line 17
    new-instance v0, LSC;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v6}, LSC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LSC;->e:LSC;

    .line 12
    const/4 v0, 0x5

    new-array v0, v0, [LSC;

    sget-object v1, LSC;->a:LSC;

    aput-object v1, v0, v2

    sget-object v1, LSC;->b:LSC;

    aput-object v1, v0, v3

    sget-object v1, LSC;->c:LSC;

    aput-object v1, v0, v4

    sget-object v1, LSC;->d:LSC;

    aput-object v1, v0, v5

    sget-object v1, LSC;->e:LSC;

    aput-object v1, v0, v6

    sput-object v0, LSC;->a:[LSC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LSC;
    .registers 2
    .parameter

    .prologue
    .line 12
    const-class v0, LSC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LSC;

    return-object v0
.end method

.method public static values()[LSC;
    .registers 1

    .prologue
    .line 12
    sget-object v0, LSC;->a:[LSC;

    invoke-virtual {v0}, [LSC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LSC;

    return-object v0
.end method
