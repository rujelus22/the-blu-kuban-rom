.class public Lti;
.super Ljava/lang/Object;
.source "KixDiscussionInitializationListener.java"

# interfaces
.implements LyH;


# instance fields
.field private a:Lth;

.field private a:LvZ;

.field private a:LwF;

.field private a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lti;->a:Lth;

    .line 28
    iput-object v0, p0, Lti;->a:LwF;

    .line 29
    iput-object v0, p0, Lti;->a:LvZ;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lti;->a:Z

    .line 31
    return-void
.end method

.method private b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 62
    iget-boolean v0, p0, Lti;->a:Z

    if-eqz v0, :cond_c

    .line 63
    iput-object v3, p0, Lti;->a:Lth;

    .line 64
    iput-object v3, p0, Lti;->a:LwF;

    .line 65
    iput-object v3, p0, Lti;->a:LvZ;

    .line 74
    :cond_b
    :goto_b
    return-void

    .line 68
    :cond_c
    iget-object v0, p0, Lti;->a:Lth;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lti;->a:LwF;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lti;->a:LvZ;

    if-eqz v0, :cond_b

    .line 69
    iget-object v0, p0, Lti;->a:Lth;

    iget-object v1, p0, Lti;->a:LwF;

    iget-object v2, p0, Lti;->a:LvZ;

    invoke-virtual {v0, v1, v2}, Lth;->a(LwF;LvZ;)V

    .line 70
    iput-object v3, p0, Lti;->a:Lth;

    .line 71
    iput-object v3, p0, Lti;->a:LwF;

    .line 72
    iput-object v3, p0, Lti;->a:LvZ;

    goto :goto_b
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lti;->a:Z

    .line 78
    return-void
.end method

.method public a(Lth;)V
    .registers 2
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lti;->a:Lth;

    .line 53
    invoke-direct {p0}, Lti;->b()V

    .line 54
    return-void
.end method

.method public a(LwF;)V
    .registers 3
    .parameter

    .prologue
    .line 40
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwF;

    iput-object v0, p0, Lti;->a:LwF;

    .line 41
    invoke-direct {p0}, Lti;->b()V

    .line 42
    return-void
.end method

.method public a(LwF;LvZ;Lxa;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvZ;

    iput-object v0, p0, Lti;->a:LvZ;

    .line 36
    return-void
.end method
