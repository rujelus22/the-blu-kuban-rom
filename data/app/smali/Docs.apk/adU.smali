.class public abstract LadU;
.super Ljava/lang/Object;
.source "AbstractHttpContent.java"

# interfaces
.implements Laec;


# instance fields
.field private a:J

.field private a:Laeg;


# direct methods
.method protected constructor <init>(Laeg;)V
    .registers 4
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LadU;->a:J

    .line 55
    iput-object p1, p0, LadU;->a:Laeg;

    .line 56
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 47
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-direct {p0, v0}, LadU;-><init>(Laeg;)V

    .line 48
    return-void

    .line 47
    :cond_7
    new-instance v0, Laeg;

    invoke-direct {v0, p1}, Laeg;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method


# virtual methods
.method public a()J
    .registers 5

    .prologue
    .line 68
    iget-wide v0, p0, LadU;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_e

    .line 69
    invoke-virtual {p0}, LadU;->b()J

    move-result-wide v0

    iput-wide v0, p0, LadU;->a:J

    .line 71
    :cond_e
    iget-wide v0, p0, LadU;->a:J

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a()Ljava/nio/charset/Charset;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, LadU;->a:Laeg;

    if-eqz v0, :cond_c

    iget-object v0, p0, LadU;->a:Laeg;

    invoke-virtual {v0}, Laeg;->a()Ljava/nio/charset/Charset;

    move-result-object v0

    if-nez v0, :cond_f

    :cond_c
    sget-object v0, Lagg;->c:Ljava/nio/charset/Charset;

    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, LadU;->a:Laeg;

    invoke-virtual {v0}, Laeg;->a()Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_e
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method

.method protected b()J
    .registers 3

    .prologue
    .line 123
    invoke-virtual {p0}, LadU;->a()Z

    move-result v0

    if-nez v0, :cond_9

    .line 124
    const-wide/16 v0, -0x1

    .line 132
    :goto_8
    return-wide v0

    .line 126
    :cond_9
    new-instance v0, LadY;

    invoke-direct {v0}, LadY;-><init>()V

    .line 128
    :try_start_e
    invoke-virtual {p0, v0}, LadU;->a(Ljava/io/OutputStream;)V
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_17

    .line 130
    invoke-virtual {v0}, LadY;->close()V

    .line 132
    iget-wide v0, v0, LadY;->a:J

    goto :goto_8

    .line 130
    :catchall_17
    move-exception v1

    invoke-virtual {v0}, LadY;->close()V

    throw v1
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, LadU;->a:Laeg;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LadU;->a:Laeg;

    invoke-virtual {v0}, Laeg;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
