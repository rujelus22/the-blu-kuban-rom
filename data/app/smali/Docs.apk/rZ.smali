.class public LrZ;
.super Ljava/lang/Object;
.source "UploadQueue.java"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsm;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<",
            "Lsl;",
            "Lsm;",
            ">;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsl;",
            "Lsm;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsl;",
            "Lsm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LrZ;->a:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LrZ;->a:Ljava/util/Map;

    .line 47
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LrZ;->b:Ljava/util/Map;

    .line 48
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 102
    :goto_0
    iget-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_1e

    iget-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;

    invoke-virtual {v0}, Lsm;->a()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 103
    iget-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    invoke-virtual {p0, v0}, LrZ;->a(Lsl;)V

    goto :goto_0

    .line 105
    :cond_1e
    return-void
.end method

.method private b()Ljava/util/Map$Entry;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<",
            "Lsl;",
            "Lsm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, LrZ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 131
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    return-object v0
.end method


# virtual methods
.method a()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LrZ;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public declared-synchronized a()Ljava/util/Map$Entry;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<",
            "Lsl;",
            "Lsm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, LrZ;->a()V

    .line 90
    iget-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    monitor-exit p0

    return-object v0

    .line 89
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(I)Lsm;
    .registers 3
    .parameter

    .prologue
    .line 94
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LrZ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    monitor-exit p0

    return-object v0

    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/Collection;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lsm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, LrZ;->a()V

    .line 57
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;

    .line 59
    invoke-virtual {v0}, Lsm;->a()Lsl;

    move-result-object v2

    .line 62
    iget-object v3, p0, LrZ;->b:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 63
    iget-object v0, p0, LrZ;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;

    .line 64
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lsm;->a(Z)V

    .line 66
    iget-object v3, p0, LrZ;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_31
    .catchall {:try_start_1 .. :try_end_31} :catchall_32

    goto :goto_8

    .line 55
    :catchall_32
    move-exception v0

    monitor-exit p0

    throw v0

    .line 67
    :cond_35
    :try_start_35
    iget-object v3, p0, LrZ;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 69
    iget-object v3, p0, LrZ;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v3, p0, LrZ;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 74
    :cond_48
    invoke-direct {p0}, LrZ;->b()Ljava/util/Map$Entry;

    move-result-object v0

    iput-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;
    :try_end_4e
    .catchall {:try_start_35 .. :try_end_4e} :catchall_32

    .line 75
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(Lsl;)V
    .registers 4
    .parameter

    .prologue
    .line 114
    monitor-enter p0

    :try_start_1
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, LrZ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 117
    iget-object v0, p0, LrZ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;

    .line 118
    if-eqz v0, :cond_21

    invoke-virtual {v0}, Lsm;->c()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 119
    iget-object v1, p0, LrZ;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    :cond_21
    iget-object v0, p0, LrZ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_41

    .line 127
    :cond_2c
    :goto_2c
    monitor-exit p0

    return-void

    .line 124
    :cond_2e
    :try_start_2e
    iget-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lsl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 125
    invoke-direct {p0}, LrZ;->b()Ljava/util/Map$Entry;

    move-result-object v0

    iput-object v0, p0, LrZ;->a:Ljava/util/Map$Entry;
    :try_end_40
    .catchall {:try_start_2e .. :try_end_40} :catchall_41

    goto :goto_2c

    .line 114
    :catchall_41
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Z
    .registers 2

    .prologue
    .line 78
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, LrZ;->a()Ljava/util/Map$Entry;
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_c

    move-result-object v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_8
    monitor-exit p0

    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Z
    .registers 2

    .prologue
    .line 82
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LrZ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_e

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_a
    monitor-exit p0

    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_a

    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0
.end method
