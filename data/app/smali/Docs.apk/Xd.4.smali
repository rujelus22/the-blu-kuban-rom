.class public LXd;
.super LXo;
.source "EntryMonitorProcessor.java"


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LkB;


# direct methods
.method public constructor <init>(LkB;Ljava/util/Set;LXg;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkB;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LXg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p3}, LXo;-><init>(LXg;)V

    .line 32
    iput-object p2, p0, LXd;->a:Ljava/util/Set;

    .line 33
    iput-object p1, p0, LXd;->a:LkB;

    .line 34
    return-void
.end method


# virtual methods
.method public a(LVU;LVV;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LXd;->a:Ljava/util/Set;

    invoke-virtual {p2}, LVV;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 47
    invoke-super {p0, p1, p2}, LXo;->a(LVU;LVV;)V

    .line 48
    return-void
.end method

.method public b(LVU;)V
    .registers 4
    .parameter

    .prologue
    .line 38
    invoke-super {p0, p1}, LXo;->b(LVU;)V

    .line 39
    iget-object v0, p0, LXd;->a:LkB;

    invoke-virtual {p1}, LVU;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LkB;->a(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, LXd;->a:LkB;

    invoke-virtual {v0}, LkB;->c()V

    .line 41
    return-void
.end method
