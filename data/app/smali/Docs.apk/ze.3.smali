.class public Lze;
.super Ljava/lang/Object;
.source "ModelCacheImpl.java"

# interfaces
.implements Lzd;


# instance fields
.field private a:I

.field private final a:LwF;

.field private final a:Z

.field private a:[I

.field private b:I

.field private final b:Z

.field private b:[I

.field private final c:Z


# direct methods
.method public constructor <init>(LwF;)V
    .registers 3
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lze;->a:LwF;

    .line 41
    invoke-interface {p1}, LwF;->e()Z

    move-result v0

    iput-boolean v0, p0, Lze;->a:Z

    .line 42
    invoke-interface {p1}, LwF;->f()Z

    move-result v0

    iput-boolean v0, p0, Lze;->b:Z

    .line 43
    invoke-interface {p1}, LwF;->g()Z

    move-result v0

    iput-boolean v0, p0, Lze;->c:Z

    .line 45
    invoke-virtual {p0}, Lze;->a()V

    .line 46
    return-void
.end method

.method static synthetic a(Lze;)I
    .registers 2
    .parameter

    .prologue
    .line 25
    iget v0, p0, Lze;->b:I

    return v0
.end method

.method private a()[I
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lze;->b:[I

    if-nez v0, :cond_c

    .line 99
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0}, LwF;->c()[I

    move-result-object v0

    iput-object v0, p0, Lze;->b:[I

    .line 101
    :cond_c
    iget-object v0, p0, Lze;->b:[I

    return-object v0
.end method

.method private a(II)[I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 74
    iget-boolean v0, p0, Lze;->a:Z

    if-eqz v0, :cond_b

    .line 75
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1, p2}, LwF;->c(II)[I

    move-result-object v0

    .line 80
    :goto_a
    return-object v0

    .line 77
    :cond_b
    iget-object v0, p0, Lze;->a:[I

    if-nez v0, :cond_17

    .line 78
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0}, LwF;->b()[I

    move-result-object v0

    iput-object v0, p0, Lze;->a:[I

    .line 80
    :cond_17
    iget-object v0, p0, Lze;->a:[I

    goto :goto_a
.end method


# virtual methods
.method public a(I)I
    .registers 5
    .parameter

    .prologue
    .line 106
    iget-boolean v0, p0, Lze;->b:Z

    if-eqz v0, :cond_b

    .line 107
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)I

    move-result v0

    .line 123
    :cond_a
    :goto_a
    return v0

    .line 109
    :cond_b
    invoke-direct {p0}, Lze;->a()[I

    move-result-object v1

    .line 110
    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 111
    if-gez v0, :cond_18

    .line 112
    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 115
    :cond_18
    array-length v2, v1

    if-lt v0, v2, :cond_2b

    .line 116
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0}, LwF;->a()I

    move-result v0

    .line 117
    if-lt p1, v0, :cond_a

    .line 118
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "Requested paragraph index past model end"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_2b
    aget v0, v1, v0

    goto :goto_a
.end method

.method public a(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()LuV;
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0}, LwF;->a()LuV;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lvo;
    .registers 3
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)Lvo;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lvq;
    .registers 3
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0}, LwF;->i()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)Lvq;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public a(II)Lvs;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 143
    if-ltz p1, :cond_14

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 146
    iget v0, p0, Lze;->a:I

    if-lt p1, v0, :cond_16

    iget v0, p0, Lze;->b:I

    if-gt p1, v0, :cond_16

    .line 148
    new-instance v0, Lzf;

    invoke-direct {v0, p0}, Lzf;-><init>(Lze;)V

    .line 193
    :cond_13
    :goto_13
    return-object v0

    .line 143
    :cond_14
    const/4 v0, 0x0

    goto :goto_3

    .line 181
    :cond_16
    iget-boolean v0, p0, Lze;->c:Z

    if-eqz v0, :cond_21

    .line 182
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1, p2}, LwF;->a(II)Lvs;

    move-result-object v0

    goto :goto_13

    .line 186
    :cond_21
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)Lvs;

    move-result-object v0

    .line 187
    invoke-interface {v0}, Lvs;->a()I

    move-result v1

    invoke-static {v1}, LxH;->a(I)LxH;

    move-result-object v1

    sget-object v2, LxH;->a:LxH;

    if-ne v1, v2, :cond_13

    .line 189
    iput p1, p0, Lze;->a:I

    .line 190
    invoke-interface {v0}, Lvs;->b()I

    move-result v1

    iput v1, p0, Lze;->b:I

    goto :goto_13
.end method

.method public a(I)Lxc;
    .registers 3
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)Lxc;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lxg;
    .registers 3
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)Lxg;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Lxw;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 58
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, v0}, Lze;->a(II)[I

    move-result-object v4

    .line 59
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0}, LwF;->a()Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lze;->a:LwF;

    add-int v1, p1, p2

    invoke-interface {v0, p1, v1}, LwF;->a(II)[I

    move-result-object v5

    .line 62
    :goto_19
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0}, LwF;->c()Z

    move-result v0

    if-eqz v0, :cond_36

    iget-object v0, p0, Lze;->a:LwF;

    add-int v1, p1, p2

    invoke-interface {v0, p1, v1}, LwF;->b(II)[I

    move-result-object v6

    .line 65
    :goto_29
    new-instance v0, Lxx;

    iget-object v1, p0, Lze;->a:LwF;

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v6}, Lxx;-><init>(LwF;II[I[I[I)V

    return-object v0

    .line 59
    :cond_33
    new-array v5, v2, [I

    goto :goto_19

    .line 62
    :cond_36
    new-array v6, v2, [I

    goto :goto_29
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 50
    iput-object v1, p0, Lze;->a:[I

    .line 51
    iput-object v1, p0, Lze;->b:[I

    .line 52
    iput v0, p0, Lze;->a:I

    .line 53
    iput v0, p0, Lze;->b:I

    .line 54
    return-void
.end method

.method public a(I)[Lvu;
    .registers 3
    .parameter

    .prologue
    .line 203
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->a(I)[Lvu;

    move-result-object v0

    return-object v0
.end method

.method public b(I)I
    .registers 3
    .parameter

    .prologue
    .line 198
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->b(I)I

    move-result v0

    return v0
.end method

.method public b(I)Lxg;
    .registers 3
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lze;->a:LwF;

    invoke-interface {v0, p1}, LwF;->b(I)Lxg;

    move-result-object v0

    return-object v0
.end method
