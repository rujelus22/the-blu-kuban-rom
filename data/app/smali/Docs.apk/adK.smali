.class public final enum LadK;
.super Ljava/lang/Enum;
.source "Presence.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LadK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LadK;

.field private static final synthetic a:[LadK;

.field public static final enum b:LadK;

.field public static final enum c:LadK;

.field public static final enum d:LadK;

.field public static final enum e:LadK;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, LadK;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LadK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LadK;->a:LadK;

    .line 43
    new-instance v0, LadK;

    const-string v1, "AWAY"

    invoke-direct {v0, v1, v3}, LadK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LadK;->b:LadK;

    .line 44
    new-instance v0, LadK;

    const-string v1, "EXTENDED_AWAY"

    invoke-direct {v0, v1, v4}, LadK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LadK;->c:LadK;

    .line 45
    new-instance v0, LadK;

    const-string v1, "DND"

    invoke-direct {v0, v1, v5}, LadK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LadK;->d:LadK;

    .line 46
    new-instance v0, LadK;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v6}, LadK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LadK;->e:LadK;

    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [LadK;

    sget-object v1, LadK;->a:LadK;

    aput-object v1, v0, v2

    sget-object v1, LadK;->b:LadK;

    aput-object v1, v0, v3

    sget-object v1, LadK;->c:LadK;

    aput-object v1, v0, v4

    sget-object v1, LadK;->d:LadK;

    aput-object v1, v0, v5

    sget-object v1, LadK;->e:LadK;

    aput-object v1, v0, v6

    sput-object v0, LadK;->a:[LadK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LadK;
    .registers 2
    .parameter

    .prologue
    .line 41
    const-class v0, LadK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LadK;

    return-object v0
.end method

.method public static final values()[LadK;
    .registers 1

    .prologue
    .line 41
    sget-object v0, LadK;->a:[LadK;

    invoke-virtual {v0}, [LadK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LadK;

    return-object v0
.end method
