.class public abstract enum LRy;
.super Ljava/lang/Enum;
.source "PunchTransitionPolicy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LRy;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LRy;

.field private static final synthetic a:[LRy;

.field public static final enum b:LRy;


# instance fields
.field private final a:LRB;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, LRz;

    const-string v1, "DISABLED"

    sget-object v2, LRB;->b:LRB;

    invoke-direct {v0, v1, v3, v2}, LRz;-><init>(Ljava/lang/String;ILRB;)V

    sput-object v0, LRy;->a:LRy;

    .line 28
    new-instance v0, LRA;

    const-string v1, "ENABLED"

    sget-object v2, LRB;->a:LRB;

    invoke-direct {v0, v1, v4, v2}, LRA;-><init>(Ljava/lang/String;ILRB;)V

    sput-object v0, LRy;->b:LRy;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [LRy;

    sget-object v1, LRy;->a:LRy;

    aput-object v1, v0, v3

    sget-object v1, LRy;->b:LRy;

    aput-object v1, v0, v4

    sput-object v0, LRy;->a:[LRy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILRB;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LRB;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput-object p3, p0, LRy;->a:LRB;

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILRB;LRz;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, LRy;-><init>(Ljava/lang/String;ILRB;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LRy;
    .registers 2
    .parameter

    .prologue
    .line 18
    const-class v0, LRy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LRy;

    return-object v0
.end method

.method public static values()[LRy;
    .registers 1

    .prologue
    .line 18
    sget-object v0, LRy;->a:[LRy;

    invoke-virtual {v0}, [LRy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LRy;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;LQR;LRC;Laoz;LeQ;)Landroid/view/View$OnTouchListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LQR;",
            "LRC;",
            "Laoz",
            "<",
            "LRb;",
            ">;",
            "LeQ;",
            ")",
            "Landroid/view/View$OnTouchListener;"
        }
    .end annotation
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 58
    iget-object v0, p0, LRy;->a:LRB;

    sget-object v1, LRB;->a:LRB;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method
