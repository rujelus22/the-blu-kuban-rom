.class public abstract Lxq;
.super LzG;
.source "BackgroundDocosSpanStyleRange.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LCt;",
        ">",
        "LzG",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

.field private final a:Ljava/lang/String;

.field private final a:Lth;

.field private final a:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lth;[Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, LzG;-><init>()V

    .line 33
    iput-object p1, p0, Lxq;->a:Lth;

    .line 34
    iput-object p2, p0, Lxq;->a:[Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lxq;->a:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lxq;->a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/util/Collection;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 42
    iget-object v0, p0, Lxq;->a:[Ljava/lang/String;

    if-eqz v0, :cond_29

    iget-object v0, p0, Lxq;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_29

    .line 43
    iget-object v0, p0, Lxq;->a:Ljava/lang/String;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lxq;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 44
    :goto_18
    new-instance v2, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    iget-object v3, p0, Lxq;->a:[Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;-><init>(I[Ljava/lang/String;)V

    iput-object v2, p0, Lxq;->a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    .line 45
    iget-object v0, p0, Lxq;->a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_26
    :goto_26
    return-object v1

    .line 43
    :cond_27
    const/4 v0, 0x0

    goto :goto_18

    .line 46
    :cond_29
    iget-object v0, p0, Lxq;->a:Ljava/lang/String;

    if-eqz v0, :cond_26

    .line 47
    new-instance v0, Landroid/text/style/BackgroundColorSpan;

    iget-object v2, p0, Lxq;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_26
.end method

.method public a(Landroid/text/Spannable;)V
    .registers 4
    .parameter

    .prologue
    .line 62
    invoke-super {p0, p1}, LzG;->a(Landroid/text/Spannable;)V

    .line 63
    iget-object v0, p0, Lxq;->a:Lth;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lxq;->a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    if-eqz v0, :cond_12

    .line 64
    iget-object v0, p0, Lxq;->a:Lth;

    iget-object v1, p0, Lxq;->a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    invoke-virtual {v0, v1}, Lth;->b(Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;)V

    .line 66
    :cond_12
    return-void
.end method

.method public a(Landroid/text/Spannable;IILandroid/content/Context;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3, p4}, LzG;->a(Landroid/text/Spannable;IILandroid/content/Context;)V

    .line 55
    iget-object v0, p0, Lxq;->a:Lth;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lxq;->a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    if-eqz v0, :cond_12

    .line 56
    iget-object v0, p0, Lxq;->a:Lth;

    iget-object v1, p0, Lxq;->a:Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    invoke-virtual {v0, v1}, Lth;->a(Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;)V

    .line 58
    :cond_12
    return-void
.end method
