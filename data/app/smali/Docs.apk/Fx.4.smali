.class public LFx;
.super LFu;
.source "TextView.java"


# instance fields
.field private a:J

.field private a:LDY;

.field private a:Ljava/lang/Runnable;

.field private b:F

.field final synthetic b:Lcom/google/android/apps/docs/editors/text/TextView;

.field private b:Ljava/lang/Runnable;

.field private c:F


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 2
    .parameter

    .prologue
    .line 8243
    iput-object p1, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-direct {p0, p1}, LFu;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 8243
    invoke-direct {p0, p1}, LFx;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    return-void
.end method

.method private i()V
    .registers 5

    .prologue
    .line 8289
    invoke-direct {p0}, LFx;->j()V

    .line 8290
    iget-object v0, p0, LFx;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_e

    .line 8291
    new-instance v0, LFz;

    invoke-direct {v0, p0}, LFz;-><init>(LFx;)V

    iput-object v0, p0, LFx;->a:Ljava/lang/Runnable;

    .line 8298
    :cond_e
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, p0, LFx;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 8299
    return-void
.end method

.method private j()V
    .registers 3

    .prologue
    .line 8302
    iget-object v0, p0, LFx;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_b

    .line 8303
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, p0, LFx;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 8305
    :cond_b
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 8358
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    return v0
.end method

.method protected a()V
    .registers 4

    .prologue
    .line 8309
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_21

    .line 8310
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v2}, Lcom/google/android/apps/docs/editors/text/TextView;->d(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 8312
    :cond_21
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LFx;->a:Landroid/graphics/drawable/Drawable;

    .line 8313
    iget-object v0, p0, LFx;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    iput v0, p0, LFx;->a:F

    .line 8314
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 8363
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Lcom/google/android/apps/docs/editors/text/TextView;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 8366
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()V

    .line 8367
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 8371
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, LFx;->a(I)V

    .line 8372
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 8257
    invoke-super {p0}, LFu;->b()V

    .line 8258
    invoke-direct {p0}, LFx;->i()V

    .line 8259
    invoke-virtual {p0}, LFx;->h()V

    .line 8260
    return-void
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 8284
    invoke-super {p0}, LFu;->c()V

    .line 8285
    invoke-virtual {p0}, LFx;->f()V

    .line 8286
    return-void
.end method

.method public c(I)V
    .registers 6
    .parameter

    .prologue
    .line 8263
    invoke-virtual {p0}, LFx;->b()V

    .line 8265
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 8266
    const-wide/16 v2, 0x3a98

    cmp-long v0, v0, v2

    if-gez v0, :cond_13

    .line 8267
    const/4 p1, 0x0

    .line 8269
    :cond_13
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 8270
    iget-object v0, p0, LFx;->b:Ljava/lang/Runnable;

    if-nez v0, :cond_26

    .line 8271
    new-instance v0, LFy;

    invoke-direct {v0, p0}, LFy;-><init>(LFx;)V

    iput-object v0, p0, LFx;->b:Ljava/lang/Runnable;

    .line 8278
    :cond_26
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, p0, LFx;->b:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 8280
    :cond_2e
    return-void
.end method

.method e()V
    .registers 2

    .prologue
    .line 8384
    invoke-virtual {p0}, LFx;->a()Z

    move-result v0

    if-nez v0, :cond_c

    .line 8385
    invoke-direct {p0}, LFx;->j()V

    .line 8386
    invoke-virtual {p0}, LFx;->h()V

    .line 8388
    :cond_c
    return-void
.end method

.method public f()V
    .registers 1

    .prologue
    .line 8401
    invoke-direct {p0}, LFx;->j()V

    .line 8402
    invoke-virtual {p0}, LFx;->h()V

    .line 8403
    return-void
.end method

.method g()V
    .registers 2

    .prologue
    .line 8375
    iget-object v0, p0, LFx;->a:LDY;

    if-nez v0, :cond_c

    .line 8376
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LDY;

    move-result-object v0

    iput-object v0, p0, LFx;->a:LDY;

    .line 8379
    :cond_c
    iget-object v0, p0, LFx;->a:LDY;

    invoke-interface {v0}, LDY;->u()V

    .line 8380
    return-void
.end method

.method h()V
    .registers 3

    .prologue
    .line 8391
    iget-object v0, p0, LFx;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_b

    .line 8392
    iget-object v0, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, p0, LFx;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 8394
    :cond_b
    iget-object v0, p0, LFx;->a:LDY;

    if-eqz v0, :cond_14

    .line 8395
    iget-object v0, p0, LFx;->a:LDY;

    invoke-interface {v0}, LDY;->r()V

    .line 8397
    :cond_14
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter

    .prologue
    .line 8318
    invoke-super {p0, p1}, LFu;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 8320
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_6a

    .line 8353
    :goto_b
    :pswitch_b
    return v0

    .line 8322
    :pswitch_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, p0, LFx;->b:F

    .line 8323
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, LFx;->c:F

    .line 8324
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, LFx;->a:J

    goto :goto_b

    .line 8328
    :pswitch_1f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, LFx;->a:J

    sub-long/2addr v1, v3

    .line 8329
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v3

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_5c

    .line 8330
    iget v1, p0, LFx;->b:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    sub-float/2addr v1, v2

    .line 8331
    iget v2, p0, LFx;->c:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    sub-float/2addr v2, v3

    .line 8332
    mul-float/2addr v1, v1

    mul-float/2addr v2, v2

    add-float/2addr v1, v2

    .line 8333
    iget-object v2, p0, LFx;->b:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v2}, Lcom/google/android/apps/docs/editors/text/TextView;->g(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5c

    .line 8334
    iget-object v1, p0, LFx;->a:LDY;

    if-eqz v1, :cond_60

    iget-object v1, p0, LFx;->a:LDY;

    invoke-interface {v1}, LDY;->j()Z

    move-result v1

    if-eqz v1, :cond_60

    .line 8336
    iget-object v1, p0, LFx;->a:LDY;

    invoke-interface {v1}, LDY;->r()V

    .line 8342
    :cond_5c
    :goto_5c
    invoke-direct {p0}, LFx;->i()V

    goto :goto_b

    .line 8338
    :cond_60
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LFx;->c(I)V

    goto :goto_5c

    .line 8346
    :pswitch_65
    invoke-direct {p0}, LFx;->i()V

    goto :goto_b

    .line 8320
    nop

    :pswitch_data_6a
    .packed-switch 0x0
        :pswitch_c
        :pswitch_1f
        :pswitch_b
        :pswitch_65
    .end packed-switch
.end method
