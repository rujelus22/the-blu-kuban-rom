.class public Lns;
.super Landroid/widget/ArrayAdapter;
.source "CreateEntryDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lnw;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/LayoutInflater;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 224
    iput-object p1, p0, Lns;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    iput-object p5, p0, Lns;->a:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 227
    invoke-virtual {p0, p1}, Lns;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnw;

    .line 228
    iget-object v1, p0, Lns;->a:Landroid/view/LayoutInflater;

    sget v2, Lej;->create_entry_dialog_row:I

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 230
    invoke-interface {v0}, Lnw;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 231
    invoke-interface {v0}, Lnw;->b()I

    move-result v0

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 232
    return-object v1
.end method
