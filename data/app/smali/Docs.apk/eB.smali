.class public LeB;
.super Ljava/lang/Object;
.source "AclType.java"


# instance fields
.field private final a:LeG;

.field private final a:LeK;

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;LeK;LeG;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447
    iput-object p1, p0, LeB;->a:Ljava/lang/String;

    .line 448
    iput-object p2, p0, LeB;->b:Ljava/lang/String;

    .line 449
    iput-object p3, p0, LeB;->a:LeK;

    .line 450
    iput-object p4, p0, LeB;->a:LeG;

    .line 451
    iput-boolean p5, p0, LeB;->a:Z

    .line 452
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;LeK;LeG;ZLeC;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct/range {p0 .. p5}, LeB;-><init>(Ljava/lang/String;Ljava/lang/String;LeK;LeG;Z)V

    return-void
.end method

.method static synthetic a(LeB;)LeK;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, LeB;->a:LeK;

    return-object v0
.end method

.method static synthetic a(LeB;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, LeB;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LeB;)Z
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-boolean v0, p0, LeB;->a:Z

    return v0
.end method

.method static synthetic b(LeB;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, LeB;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()LeG;
    .registers 2

    .prologue
    .line 471
    iget-object v0, p0, LeB;->a:LeG;

    return-object v0
.end method

.method public a()LeH;
    .registers 4

    .prologue
    .line 487
    iget-object v0, p0, LeB;->a:LeG;

    iget-object v1, p0, LeB;->a:LeK;

    iget-boolean v2, p0, LeB;->a:Z

    invoke-static {v0, v1, v2}, LeH;->a(LeG;LeK;Z)LeH;

    move-result-object v0

    return-object v0
.end method

.method public a()LeI;
    .registers 2

    .prologue
    .line 467
    iget-object v0, p0, LeB;->a:LeG;

    invoke-virtual {v0}, LeG;->a()LeI;

    move-result-object v0

    return-object v0
.end method

.method public a()LeK;
    .registers 2

    .prologue
    .line 463
    iget-object v0, p0, LeB;->a:LeK;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 455
    iget-object v0, p0, LeB;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 475
    iget-object v0, p0, LeB;->a:LeG;

    invoke-virtual {v0}, LeG;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 483
    iget-boolean v0, p0, LeB;->a:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 459
    iget-object v0, p0, LeB;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 320
    instance-of v1, p1, LeB;

    if-nez v1, :cond_6

    .line 324
    :cond_5
    :goto_5
    return v0

    .line 323
    :cond_6
    check-cast p1, LeB;

    .line 324
    iget-object v1, p0, LeB;->a:Ljava/lang/String;

    iget-object v2, p1, LeB;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LeB;->b:Ljava/lang/String;

    iget-object v2, p1, LeB;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LeB;->a:LeK;

    iget-object v2, p1, LeB;->a:LeK;

    invoke-virtual {v1, v2}, LeK;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LeB;->a:LeG;

    iget-object v2, p1, LeB;->a:LeG;

    invoke-virtual {v1, v2}, LeG;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, LeB;->a:Z

    iget-boolean v2, p1, LeB;->a:Z

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 332
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LeB;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LeB;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LeB;->a:LeK;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LeB;->a:LeG;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LeB;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 492
    const-string v1, "AclType[%s %s %s%s]"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v3, p0, LeB;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget-object v3, p0, LeB;->a:LeG;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-object v3, p0, LeB;->a:LeK;

    aput-object v3, v2, v0

    const/4 v3, 0x3

    iget-boolean v0, p0, LeB;->a:Z

    if-eqz v0, :cond_22

    const-string v0, "+link"

    :goto_1b
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_22
    const-string v0, ""

    goto :goto_1b
.end method
