.class final LapL;
.super Ljava/lang/Object;
.source "InjectorImpl.java"

# interfaces
.implements Laoo;


# static fields
.field public static final a:LaoL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaoL",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:LapL;

.field final a:LapQ;

.field final a:Lapq;

.field final a:LaqL;

.field a:Laqf;

.field a:Laqx;

.field final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Laop",
            "<*>;",
            "LaoY",
            "<*>;>;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Laop",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 67
    const-class v0, Ljava/lang/String;

    invoke-static {v0}, LaoL;->b(Ljava/lang/Class;)LaoL;

    move-result-object v0

    sput-object v0, LapL;->a:LaoL;

    return-void
.end method

.method private a(Laop;Lapu;)LaoY;
    .registers 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<",
            "Laou",
            "<TT;>;>;",
            "Lapu;",
            ")",
            "LaoY",
            "<",
            "Laou",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 319
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 320
    instance-of v1, v0, Ljava/lang/reflect/ParameterizedType;

    if-nez v1, :cond_15

    .line 321
    invoke-virtual {p2}, Lapu;->d()Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 325
    :cond_15
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, LaoL;->a(Ljava/lang/reflect/Type;)LaoL;

    move-result-object v0

    .line 327
    iget-object v1, p0, LapL;->a:Laqf;

    invoke-virtual {v1, v0, p2}, Laqf;->a(LaoL;Lapu;)Laqd;

    move-result-object v6

    .line 329
    new-instance v4, Lapj;

    invoke-static {v6}, LapH;->a(Ljava/lang/Object;)LapG;

    move-result-object v0

    invoke-direct {v4, v0}, Lapj;-><init>(LapG;)V

    .line 333
    new-instance v0, LapW;

    sget-object v3, LaqS;->a:Ljava/lang/Object;

    invoke-static {}, Lajm;->a()Lajm;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, LapW;-><init>(LapL;Laop;Ljava/lang/Object;LapY;Ljava/util/Set;Ljava/lang/Object;)V

    return-object v0
.end method

.method private a(Laop;Lapu;ZLapR;)LaoY;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Lapu;",
            "Z",
            "LapR;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 782
    iget-object v0, p0, LapL;->a:LapL;

    if-eqz v0, :cond_1d

    .line 784
    :try_start_4
    iget-object v1, p0, LapL;->a:LapL;

    new-instance v2, Lapu;

    invoke-direct {v2}, Lapu;-><init>()V

    iget-object v0, p0, LapL;->a:LapL;

    iget-object v0, v0, LapL;->a:LapQ;

    iget-boolean v0, v0, LapQ;->a:Z

    if-eqz v0, :cond_1a

    sget-object v0, LapR;->a:LapR;

    :goto_15
    invoke-direct {v1, p1, v2, p3, v0}, LapL;->a(Laop;Lapu;ZLapR;)LaoY;
    :try_end_18
    .catch LapA; {:try_start_4 .. :try_end_18} :catch_1c

    move-result-object v0

    .line 798
    :goto_19
    return-object v0

    :cond_1a
    move-object v0, p4

    .line 784
    goto :goto_15

    .line 786
    :catch_1c
    move-exception v0

    .line 790
    :cond_1d
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0, p1}, LaqL;->a(Laop;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 791
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0, p1}, LaqL;->a(Laop;)Ljava/util/Set;

    move-result-object v0

    .line 792
    invoke-virtual {p2, p1, v0}, Lapu;->a(Laop;Ljava/util/Set;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 795
    :cond_34
    invoke-direct {p0, p1, p2, p3, p4}, LapL;->b(Laop;Lapu;ZLapR;)LaoY;

    move-result-object v0

    .line 796
    iget-object v1, p0, LapL;->a:LaqL;

    invoke-interface {v1}, LaqL;->a()LaqL;

    move-result-object v1

    invoke-virtual {v0}, LaoY;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, p1, v2}, LaqL;->a(Laop;Ljava/lang/Object;)V

    .line 797
    iget-object v1, p0, LapL;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_19
.end method

.method private a(Laop;LaqC;Laom;Lapu;)LaoY;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "LaqC;",
            "Laom;",
            "Lapu;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 732
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v3

    .line 733
    invoke-interface {p3}, Laom;->a()Ljava/lang/Class;

    move-result-object v0

    .line 736
    if-ne v0, v3, :cond_17

    .line 737
    invoke-virtual {p4}, Lapu;->a()Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 741
    :cond_17
    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_26

    .line 742
    invoke-virtual {p4, v0, v3}, Lapu;->b(Ljava/lang/Class;Ljava/lang/Class;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 749
    :cond_26
    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v6

    .line 750
    sget-object v0, LapR;->c:LapR;

    invoke-virtual {p0, v6, p4, v0}, LapL;->a(Laop;Lapu;LapR;)LaoY;

    move-result-object v0

    .line 752
    new-instance v1, LapM;

    invoke-direct {v1, p0, v6, v0}, LapM;-><init>(LapL;Laop;LaoY;)V

    .line 766
    new-instance v0, Laqb;

    invoke-static {p1, p0, v1, v3, p2}, LaqC;->a(Laop;LapL;LapY;Ljava/lang/Object;LaqC;)LapY;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Laqb;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Laop;)V

    return-object v0
.end method

.method private static a(Laop;Lapu;)Laop;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<",
            "Laoz",
            "<TT;>;>;",
            "Lapu;",
            ")",
            "Laop",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 297
    invoke-virtual {p0}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 300
    instance-of v1, v0, Ljava/lang/reflect/ParameterizedType;

    if-nez v1, :cond_15

    .line 301
    invoke-virtual {p1}, Lapu;->c()Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 304
    :cond_15
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 307
    invoke-virtual {p0, v0}, Laop;->b(Ljava/lang/reflect/Type;)Laop;

    move-result-object v0

    .line 308
    return-object v0
.end method

.method private a(LaoY;)Ljava/util/Set;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaoY",
            "<*>;)",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 613
    instance-of v0, p1, Lapm;

    if-eqz v0, :cond_b

    .line 614
    check-cast p1, Lapm;

    invoke-virtual {p1}, Lapm;->a()Ljava/util/Set;

    move-result-object v0

    .line 618
    :goto_a
    return-object v0

    .line 615
    :cond_b
    instance-of v0, p1, Larn;

    if-eqz v0, :cond_16

    .line 616
    check-cast p1, Larn;

    invoke-interface {p1}, Larn;->c()Ljava/util/Set;

    move-result-object v0

    goto :goto_a

    .line 618
    :cond_16
    invoke-static {}, Lajm;->a()Lajm;

    move-result-object v0

    goto :goto_a
.end method

.method private a(Laof;Larp;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laof",
            "<*>;",
            "Larp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 601
    iget-object v0, p0, LapL;->a:Ljava/util/Set;

    invoke-interface {p1}, Laof;->a()Laop;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 602
    iget-object v0, p0, LapL;->a:Ljava/util/Map;

    invoke-interface {p1}, Laof;->a()Laop;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 603
    iget-object v0, p0, LapL;->a:Laqf;

    invoke-interface {p1}, Laof;->a()Laop;

    move-result-object v1

    invoke-virtual {v1}, Laop;->a()LaoL;

    move-result-object v1

    invoke-virtual {v0, v1}, Laqf;->a(LaoL;)Z

    .line 604
    iget-object v0, p0, LapL;->a:Laqx;

    invoke-virtual {v0, p1}, Laqx;->a(Laof;)Z

    .line 605
    if-eqz p2, :cond_2b

    .line 606
    iget-object v0, p0, LapL;->a:Lapq;

    invoke-virtual {v0, p2}, Lapq;->a(Larp;)Z

    .line 608
    :cond_2b
    return-void
.end method

.method private a(LaoY;Ljava/util/Set;)Z
    .registers 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaoY",
            "<*>;",
            "Ljava/util/Set",
            "<",
            "Laop;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 569
    const/4 v0, 0x0

    .line 570
    invoke-direct {p0, p1}, LapL;->a(LaoY;)Ljava/util/Set;

    move-result-object v1

    .line 571
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v0

    :goto_b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    .line 572
    invoke-virtual {v0}, Larg;->a()Laop;

    move-result-object v4

    .line 573
    invoke-virtual {v0}, Larg;->a()Larp;

    move-result-object v1

    .line 574
    invoke-interface {p2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 575
    iget-object v0, p0, LapL;->a:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaoY;

    .line 576
    if-eqz v0, :cond_4f

    .line 577
    invoke-direct {p0, v0, p2}, LapL;->a(LaoY;Ljava/util/Set;)Z

    move-result v4

    .line 578
    instance-of v5, v0, Lapm;

    if-eqz v5, :cond_5f

    move-object v1, v0

    .line 579
    check-cast v1, Lapm;

    .line 580
    invoke-virtual {v1}, Lapm;->a()Larp;

    move-result-object v5

    .line 581
    invoke-virtual {v1}, Lapm;->b()Z

    move-result v1

    if-nez v1, :cond_5c

    move v1, v3

    move-object v4, v5

    .line 585
    :goto_46
    if-eqz v1, :cond_4c

    .line 586
    invoke-direct {p0, v0, v4}, LapL;->a(Laof;Larp;)V

    move v2, v3

    :cond_4c
    move v0, v2

    :goto_4d
    move v2, v0

    .line 595
    goto :goto_b

    .line 589
    :cond_4f
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0, v4}, LaqL;->a(Laop;)LaoY;

    move-result-object v0

    if-nez v0, :cond_5a

    move v0, v3

    .line 592
    goto :goto_4d

    .line 596
    :cond_59
    return v2

    :cond_5a
    move v0, v2

    goto :goto_4d

    :cond_5c
    move v1, v4

    move-object v4, v5

    goto :goto_46

    :cond_5f
    move v7, v4

    move-object v4, v1

    move v1, v7

    goto :goto_46
.end method

.method private static a(Laop;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 289
    invoke-virtual {p0}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Laoz;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private b(Laop;Lapu;)LaoY;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<",
            "Laoz",
            "<TT;>;>;",
            "Lapu;",
            ")",
            "LaoY",
            "<",
            "Laoz",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 343
    invoke-static {p1, p2}, LapL;->a(Laop;Lapu;)Laop;

    move-result-object v0

    .line 344
    sget-object v1, LapR;->a:LapR;

    invoke-virtual {p0, v0, p2, v1}, LapL;->a(Laop;Lapu;LapR;)LaoY;

    move-result-object v0

    .line 345
    new-instance v1, LapS;

    invoke-direct {v1, p0, p1, v0}, LapS;-><init>(LapL;Laop;Laof;)V

    return-object v1
.end method

.method private b(Laop;Lapu;LapR;)LaoY;
    .registers 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Lapu;",
            "LapR;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 245
    invoke-static {p1}, LapL;->a(Laop;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p1}, LapL;->b(Laop;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p1}, LapL;->c(Laop;)Z

    move-result v0

    if-eqz v0, :cond_44

    :cond_12
    const/4 v0, 0x1

    move v1, v0

    .line 246
    :goto_14
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0}, LaqL;->a()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    move-object v2, p0

    .line 248
    :goto_1c
    if-eqz v2, :cond_4d

    .line 250
    :try_start_1e
    iget-object v0, v2, LapL;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaoY;

    .line 252
    if-eqz v0, :cond_49

    .line 255
    iget-object v2, p0, LapL;->a:LapQ;

    iget-boolean v2, v2, LapQ;->a:Z

    if-eqz v2, :cond_47

    sget-object v2, LapR;->a:LapR;

    if-ne p3, v2, :cond_47

    if-nez v1, :cond_47

    instance-of v1, v0, LapP;

    if-nez v1, :cond_47

    .line 259
    invoke-virtual {p2, p1}, Lapu;->b(Laop;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 284
    :catchall_41
    move-exception v0

    monitor-exit v3
    :try_end_43
    .catchall {:try_start_1e .. :try_end_43} :catchall_41

    throw v0

    .line 245
    :cond_44
    const/4 v0, 0x0

    move v1, v0

    goto :goto_14

    .line 261
    :cond_47
    :try_start_47
    monitor-exit v3

    .line 283
    :goto_48
    return-object v0

    .line 248
    :cond_49
    iget-object v0, v2, LapL;->a:LapL;

    move-object v2, v0

    goto :goto_1c

    .line 280
    :cond_4d
    iget-object v0, p0, LapL;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_60

    invoke-virtual {p2}, Lapu;->a()Z

    move-result v0

    if-eqz v0, :cond_60

    .line 281
    invoke-virtual {p2}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 283
    :cond_60
    iget-object v0, p0, LapL;->a:LapQ;

    iget-boolean v0, v0, LapQ;->a:Z

    invoke-direct {p0, p1, p2, v0, p3}, LapL;->a(Laop;Lapu;ZLapR;)LaoY;

    move-result-object v0

    monitor-exit v3
    :try_end_69
    .catchall {:try_start_47 .. :try_end_69} :catchall_41

    goto :goto_48
.end method

.method private b(Laop;Lapu;ZLapR;)LaoY;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Lapu;",
            "Z",
            "LapR;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 816
    invoke-virtual {p2}, Lapu;->a()I

    move-result v6

    .line 818
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0, p1}, LaqL;->a(Laop;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 819
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0, p1}, LaqL;->a(Laop;)Ljava/util/Set;

    move-result-object v0

    .line 820
    invoke-virtual {p2, p1, v0}, Lapu;->a(Laop;Ljava/util/Set;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 824
    :cond_1b
    invoke-static {p1}, LapL;->a(Laop;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 828
    invoke-direct {p0, p1, p2}, LapL;->b(Laop;Lapu;)LaoY;

    move-result-object v0

    .line 871
    :cond_25
    :goto_25
    return-object v0

    .line 833
    :cond_26
    invoke-static {p1}, LapL;->c(Laop;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 837
    invoke-direct {p0, p1, p2}, LapL;->a(Laop;Lapu;)LaoY;

    move-result-object v0

    goto :goto_25

    .line 842
    :cond_31
    invoke-direct {p0, p1, p2}, LapL;->c(Laop;Lapu;)LaoY;

    move-result-object v0

    .line 843
    if-nez v0, :cond_25

    .line 847
    invoke-static {p1}, LapL;->b(Laop;)Z

    move-result v0

    if-nez v0, :cond_4c

    if-eqz p3, :cond_4c

    sget-object v0, LapR;->c:LapR;

    if-eq p4, v0, :cond_4c

    .line 850
    invoke-virtual {p2, p1}, Lapu;->b(Laop;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 854
    :cond_4c
    invoke-virtual {p1}, Laop;->a()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_72

    .line 856
    invoke-virtual {p1}, Laop;->a()Z

    move-result v0

    if-eqz v0, :cond_69

    .line 858
    :try_start_58
    new-instance v0, Lapu;

    invoke-direct {v0}, Lapu;-><init>()V

    .line 859
    invoke-virtual {p1}, Laop;->a()Laop;

    move-result-object v1

    sget-object v2, LapR;->a:LapR;

    invoke-virtual {p0, v1, v0, v2}, LapL;->a(Laop;Lapu;LapR;)LaoY;
    :try_end_66
    .catch LapA; {:try_start_58 .. :try_end_66} :catch_68

    move-result-object v0

    goto :goto_25

    .line 860
    :catch_68
    move-exception v0

    .line 864
    :cond_69
    invoke-virtual {p2, p1}, Lapu;->a(Laop;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 867
    :cond_72
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v3

    .line 868
    sget-object v2, LaqC;->a:LaqC;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LapL;->a(Laop;LaqC;Ljava/lang/Object;Lapu;Z)LaoY;

    move-result-object v0

    .line 869
    invoke-virtual {p2, v6}, Lapu;->a(I)V

    .line 870
    invoke-virtual {p0, v0, p2}, LapL;->a(LaoY;Lapu;)V

    goto :goto_25
.end method

.method private static b(Laop;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p0}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    const-class v1, LaoL;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(Laop;Lapu;)LaoY;
    .registers 15
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Lapu;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 417
    sget-object v0, LapL;->a:LaoL;

    invoke-virtual {p1, v0}, Laop;->b(LaoL;)Laop;

    move-result-object v0

    .line 418
    iget-object v1, p0, LapL;->a:LaqL;

    invoke-interface {v1, v0}, LaqL;->a(Laop;)LaoY;

    move-result-object v10

    .line 419
    if-eqz v10, :cond_15

    invoke-virtual {v10}, LaoY;->a()Z

    move-result v0

    if-nez v0, :cond_16

    .line 450
    :cond_15
    :goto_15
    return-object v6

    .line 423
    :cond_16
    invoke-virtual {v10}, LaoY;->a()Laoz;

    move-result-object v0

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 424
    invoke-virtual {v10}, LaoY;->a()Ljava/lang/Object;

    move-result-object v2

    .line 427
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v3

    .line 428
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0, v1, v3, p2, v2}, LaqL;->a(Ljava/lang/String;LaoL;Lapu;Ljava/lang/Object;)LarP;

    move-result-object v4

    .line 430
    if-eqz v4, :cond_15

    .line 438
    :try_start_30
    invoke-virtual {v4}, LarP;->a()LarO;

    move-result-object v0

    invoke-interface {v0, v1, v3}, LarO;->a(Ljava/lang/String;LaoL;)Ljava/lang/Object;

    move-result-object v5

    .line 440
    if-nez v5, :cond_45

    .line 441
    invoke-virtual {p2, v1, v2, v3, v4}, Lapu;->a(Ljava/lang/String;Ljava/lang/Object;LaoL;LarP;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0
    :try_end_43
    .catch LapA; {:try_start_30 .. :try_end_43} :catch_43
    .catch Ljava/lang/RuntimeException; {:try_start_30 .. :try_end_43} :catch_59

    .line 452
    :catch_43
    move-exception v0

    .line 453
    throw v0

    .line 445
    :cond_45
    :try_start_45
    invoke-virtual {v3}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_64

    move-object v0, p2

    .line 446
    invoke-virtual/range {v0 .. v5}, Lapu;->a(Ljava/lang/String;Ljava/lang/Object;LaoL;LarP;Ljava/lang/Object;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0
    :try_end_59
    .catch LapA; {:try_start_45 .. :try_end_59} :catch_43
    .catch Ljava/lang/RuntimeException; {:try_start_45 .. :try_end_59} :catch_59

    .line 454
    :catch_59
    move-exception v5

    move-object v0, p2

    .line 455
    invoke-virtual/range {v0 .. v5}, Lapu;->a(Ljava/lang/String;Ljava/lang/Object;LaoL;LarP;Ljava/lang/RuntimeException;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 450
    :cond_64
    :try_start_64
    new-instance v6, LapP;

    move-object v7, p0

    move-object v8, p1

    move-object v9, v5

    move-object v11, v4

    invoke-direct/range {v6 .. v11}, LapP;-><init>(LapL;Laop;Ljava/lang/Object;Laof;LarP;)V
    :try_end_6d
    .catch LapA; {:try_start_64 .. :try_end_6d} :catch_43
    .catch Ljava/lang/RuntimeException; {:try_start_64 .. :try_end_6d} :catch_59

    goto :goto_15
.end method

.method private static c(Laop;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 313
    invoke-virtual {p0}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Laou;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p0}, Laop;->a()Ljava/lang/Class;

    move-result-object v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private d(Laop;Lapu;)LaoY;
    .registers 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<",
            "LaoL",
            "<TT;>;>;",
            "Lapu;",
            ")",
            "LaoY",
            "<",
            "LaoL",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 674
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 675
    instance-of v1, v0, Ljava/lang/reflect/ParameterizedType;

    if-nez v1, :cond_15

    .line 676
    invoke-virtual {p2}, Lapu;->e()Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 679
    :cond_15
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 680
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 684
    instance-of v1, v0, Ljava/lang/Class;

    if-nez v1, :cond_33

    instance-of v1, v0, Ljava/lang/reflect/GenericArrayType;

    if-nez v1, :cond_33

    instance-of v1, v0, Ljava/lang/reflect/ParameterizedType;

    if-nez v1, :cond_33

    .line 687
    invoke-virtual {p2, v0}, Lapu;->a(Ljava/lang/reflect/Type;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 691
    :cond_33
    invoke-static {v0}, LaoL;->a(Ljava/lang/reflect/Type;)LaoL;

    move-result-object v6

    .line 692
    new-instance v4, Lapj;

    invoke-static {v6}, LapH;->a(Ljava/lang/Object;)LapG;

    move-result-object v0

    invoke-direct {v4, v0}, Lapj;-><init>(LapG;)V

    .line 694
    new-instance v0, LapW;

    sget-object v3, LaqS;->a:Ljava/lang/Object;

    invoke-static {}, Lajm;->a()Lajm;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, LapW;-><init>(LapL;Laop;Ljava/lang/Object;LapY;Ljava/util/Set;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method a(Laop;Lapu;LapR;)LaoY;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Lapu;",
            "LapR;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, LapL;->a:LaqL;

    invoke-interface {v0, p1}, LaqL;->a(Laop;)LaoY;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_9

    .line 215
    :goto_8
    return-object v0

    :cond_9
    invoke-direct {p0, p1, p2, p3}, LapL;->b(Laop;Lapu;LapR;)LaoY;

    move-result-object v0

    goto :goto_8
.end method

.method a(Laop;LaqC;Laoy;Lapu;)LaoY;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "LaqC;",
            "Laoy;",
            "Lapu;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 701
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v2

    .line 702
    invoke-interface {p3}, Laoy;->a()Ljava/lang/Class;

    move-result-object v1

    .line 705
    if-ne v1, v2, :cond_17

    .line 706
    invoke-virtual {p4}, Lapu;->b()Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 711
    :cond_17
    invoke-static {v1}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v5

    .line 712
    new-instance v6, Laqo;

    iget-object v0, p0, LapL;->a:LapQ;

    iget-boolean v0, v0, LapQ;->b:Z

    if-nez v0, :cond_3c

    const/4 v0, 0x1

    :goto_24
    invoke-direct {v6, v2, v1, v5, v0}, Laqo;-><init>(Ljava/lang/Class;Ljava/lang/Class;Laop;Z)V

    .line 716
    invoke-static {p1, p0, v6, v2, p2}, LaqC;->a(Laop;LapL;LapY;Ljava/lang/Object;LaqC;)LapY;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-static/range {v0 .. v6}, Laqc;->a(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Laop;Laps;)Laqc;

    move-result-object v0

    .line 724
    iget-object v1, p0, LapL;->a:Laqx;

    invoke-virtual {v1, v0}, Laqx;->a(Laof;)Laqz;

    move-result-object v1

    invoke-virtual {v6, v1}, Laqo;->a(Laqz;)V

    .line 725
    return-object v0

    .line 712
    :cond_3c
    const/4 v0, 0x0

    goto :goto_24
.end method

.method a(Laop;LaqC;Ljava/lang/Object;Lapu;Z)LaoY;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "LaqC;",
            "Ljava/lang/Object;",
            "Lapu;",
            "Z)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 628
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v1

    .line 631
    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-virtual {v1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 632
    :cond_14
    invoke-virtual {p4, p1}, Lapu;->a(Laop;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 636
    :cond_1d
    const-class v0, LaoL;

    if-ne v1, v0, :cond_26

    .line 638
    invoke-direct {p0, p1, p4}, LapL;->d(Laop;Lapu;)LaoY;

    move-result-object v0

    .line 658
    :goto_25
    return-object v0

    .line 644
    :cond_26
    const-class v0, Laom;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Laom;

    .line 645
    if-eqz v0, :cond_38

    .line 646
    invoke-static {v1, p3, p4}, LaoU;->a(Ljava/lang/Class;Ljava/lang/Object;Lapu;)V

    .line 647
    invoke-direct {p0, p1, p2, v0, p4}, LapL;->a(Laop;LaqC;Laom;Lapu;)LaoY;

    move-result-object v0

    goto :goto_25

    .line 651
    :cond_38
    const-class v0, Laoy;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Laoy;

    .line 652
    if-eqz v0, :cond_4a

    .line 653
    invoke-static {v1, p3, p4}, LaoU;->a(Ljava/lang/Class;Ljava/lang/Object;Lapu;)V

    .line 654
    invoke-virtual {p0, p1, p2, v0, p4}, LapL;->a(Laop;LaqC;Laoy;Lapu;)LaoY;

    move-result-object v0

    goto :goto_25

    .line 658
    :cond_4a
    const/4 v2, 0x0

    if-eqz p5, :cond_62

    iget-object v0, p0, LapL;->a:LapQ;

    iget-boolean v0, v0, LapQ;->a:Z

    if-eqz v0, :cond_62

    const/4 v6, 0x1

    :goto_54
    iget-object v0, p0, LapL;->a:LapQ;

    iget-boolean v7, v0, LapQ;->c:Z

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-static/range {v0 .. v7}, Lapm;->a(LapL;Laop;Larp;Ljava/lang/Object;LaqC;Lapu;ZZ)Lapm;

    move-result-object v0

    goto :goto_25

    :cond_62
    const/4 v6, 0x0

    goto :goto_54
.end method

.method public a(LaoL;)Laou;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LaoL",
            "<TT;>;)",
            "Laou",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 974
    new-instance v1, Lapu;

    invoke-direct {v1, p1}, Lapu;-><init>(Ljava/lang/Object;)V

    .line 976
    :try_start_5
    iget-object v0, p0, LapL;->a:Laqf;

    invoke-virtual {v0, p1, v1}, Laqf;->a(LaoL;Lapu;)Laqd;
    :try_end_a
    .catch LapA; {:try_start_5 .. :try_end_a} :catch_c

    move-result-object v0

    return-object v0

    .line 977
    :catch_c
    move-exception v0

    .line 978
    new-instance v2, Laoh;

    invoke-virtual {v0}, LapA;->a()Lapu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapu;->a(Lapu;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Laoh;-><init>(Ljava/lang/Iterable;)V

    throw v2
.end method

.method public a(Ljava/lang/Class;)Laou;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Laou",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 983
    invoke-static {p1}, LaoL;->b(Ljava/lang/Class;)LaoL;

    move-result-object v0

    invoke-virtual {p0, v0}, LapL;->a(LaoL;)Laou;

    move-result-object v0

    return-object v0
.end method

.method public a(Laop;)Laoz;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1022
    new-instance v1, Lapu;

    invoke-direct {v1, p1}, Lapu;-><init>(Ljava/lang/Object;)V

    .line 1024
    :try_start_5
    invoke-virtual {p0, p1, v1}, LapL;->a(Laop;Lapu;)Laoz;

    move-result-object v0

    .line 1025
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lapu;->a(I)V
    :try_end_d
    .catch LapA; {:try_start_5 .. :try_end_d} :catch_e

    .line 1026
    return-object v0

    .line 1027
    :catch_e
    move-exception v0

    .line 1028
    new-instance v2, Laoh;

    invoke-virtual {v0}, LapA;->a()Lapu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapu;->a(Lapu;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Laoh;-><init>(Ljava/lang/Iterable;)V

    throw v2
.end method

.method a(Laop;Lapu;)Laoz;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Lapu;",
            ")",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 991
    sget-object v0, LapR;->a:LapR;

    invoke-virtual {p0, p1, p2, v0}, LapL;->a(Laop;Lapu;LapR;)LaoY;

    move-result-object v0

    .line 992
    invoke-static {p1}, Larg;->a(Laop;)Larg;

    move-result-object v1

    .line 994
    new-instance v2, LapN;

    invoke-direct {v2, p0, v1, v0}, LapN;-><init>(LapL;Larg;LaoY;)V

    return-object v2
.end method

.method public a(Ljava/lang/Class;)Laoz;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 987
    invoke-static {p1}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-virtual {p0, v0}, LapL;->a(Laop;)Laoz;

    move-result-object v0

    return-object v0
.end method

.method a(Lapr;)Ljava/lang/Object;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lapr",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1044
    iget-object v0, p0, LapL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 1045
    aget-object v1, v0, v2

    if-nez v1, :cond_26

    .line 1046
    new-instance v1, LapX;

    invoke-direct {v1}, LapX;-><init>()V

    aput-object v1, v0, v2

    .line 1048
    const/4 v1, 0x0

    :try_start_16
    aget-object v1, v0, v1

    check-cast v1, LapX;

    invoke-interface {p1, v1}, Lapr;->a(LapX;)Ljava/lang/Object;
    :try_end_1d
    .catchall {:try_start_16 .. :try_end_1d} :catchall_22

    move-result-object v1

    .line 1051
    aput-object v3, v0, v2

    move-object v0, v1

    .line 1055
    :goto_21
    return-object v0

    .line 1051
    :catchall_22
    move-exception v1

    aput-object v3, v0, v2

    throw v1

    .line 1055
    :cond_26
    aget-object v0, v0, v2

    check-cast v0, LapX;

    invoke-interface {p1, v0}, Lapr;->a(LapX;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_21
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1037
    invoke-virtual {p0, p1}, LapL;->a(Ljava/lang/Class;)Laoz;

    move-result-object v0

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method a(LaoY;Lapu;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LaoY",
            "<TT;>;",
            "Lapu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 542
    instance-of v0, p1, Laps;

    if-eqz v0, :cond_13

    .line 543
    invoke-virtual {p1}, LaoY;->a()Laop;

    move-result-object v0

    .line 544
    iget-object v1, p0, LapL;->a:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, p1

    .line 546
    check-cast v0, Laps;

    .line 548
    :try_start_10
    invoke-interface {v0, p0, p2}, Laps;->a(LapL;Lapu;)V
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_14

    .line 560
    :cond_13
    return-void

    .line 551
    :catchall_14
    move-exception v0

    .line 555
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, LapL;->a(Laof;Larp;)V

    .line 556
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, p1, v1}, LapL;->a(LaoY;Ljava/util/Set;)Z

    throw v0
.end method

.method public a(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 969
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, LapL;->a(Ljava/lang/Class;)Laou;

    move-result-object v0

    .line 970
    invoke-interface {v0, p1}, Laou;->a(Ljava/lang/Object;)V

    .line 971
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 1061
    const-class v0, Laoo;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "bindings"

    iget-object v2, p0, LapL;->a:LaqL;

    invoke-interface {v2}, LaqL;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
