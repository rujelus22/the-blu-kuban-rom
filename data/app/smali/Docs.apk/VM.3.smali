.class public final LVM;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LVK;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LVH;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LVF;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LVL;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LVy;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LVI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 36
    iput-object p1, p0, LVM;->a:LYD;

    .line 37
    const-class v0, LVK;

    const-string v1, "DocFeed"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LVM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LVM;->a:LZb;

    .line 40
    const-class v0, LVH;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LVM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LVM;->b:LZb;

    .line 43
    const-class v0, LVF;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LVM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LVM;->c:LZb;

    .line 46
    const-class v0, LVL;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LVM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LVM;->d:LZb;

    .line 49
    const-class v0, LVy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LVM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LVM;->e:LZb;

    .line 52
    const-class v0, LVI;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LVM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LVM;->f:LZb;

    .line 55
    return-void
.end method

.method static synthetic a(LVM;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LVM;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 78
    const-class v0, LVL;

    new-instance v1, LVN;

    invoke-direct {v1, p0}, LVN;-><init>(LVM;)V

    invoke-virtual {p0, v0, v1}, LVM;->a(Ljava/lang/Class;Laou;)V

    .line 86
    const-class v0, LVK;

    const-string v1, "DocFeed"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LVM;->a:LZb;

    invoke-virtual {p0, v0, v1}, LVM;->a(Laop;LZb;)V

    .line 87
    const-class v0, LVH;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LVM;->b:LZb;

    invoke-virtual {p0, v0, v1}, LVM;->a(Laop;LZb;)V

    .line 88
    const-class v0, LVF;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LVM;->c:LZb;

    invoke-virtual {p0, v0, v1}, LVM;->a(Laop;LZb;)V

    .line 89
    const-class v0, LVL;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LVM;->d:LZb;

    invoke-virtual {p0, v0, v1}, LVM;->a(Laop;LZb;)V

    .line 90
    const-class v0, LVy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LVM;->e:LZb;

    invoke-virtual {p0, v0, v1}, LVM;->a(Laop;LZb;)V

    .line 91
    const-class v0, LVI;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LVM;->f:LZb;

    invoke-virtual {p0, v0, v1}, LVM;->a(Laop;LZb;)V

    .line 92
    iget-object v0, p0, LVM;->a:LZb;

    iget-object v1, p0, LVM;->a:LYD;

    iget-object v1, v1, LYD;->a:LVM;

    iget-object v1, v1, LVM;->f:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 94
    iget-object v0, p0, LVM;->b:LZb;

    iget-object v1, p0, LVM;->a:LYD;

    iget-object v1, v1, LYD;->a:LVM;

    iget-object v1, v1, LVM;->f:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 96
    iget-object v0, p0, LVM;->c:LZb;

    iget-object v1, p0, LVM;->a:LYD;

    iget-object v1, v1, LYD;->a:LVv;

    iget-object v1, v1, LVv;->a:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 98
    iget-object v0, p0, LVM;->d:LZb;

    new-instance v1, LVO;

    invoke-direct {v1, p0}, LVO;-><init>(LVM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 109
    iget-object v0, p0, LVM;->e:LZb;

    new-instance v1, LVP;

    invoke-direct {v1, p0}, LVP;-><init>(LVM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 133
    iget-object v0, p0, LVM;->f:LZb;

    new-instance v1, LVQ;

    invoke-direct {v1, p0}, LVQ;-><init>(LVM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 157
    return-void
.end method

.method public a(LVL;)V
    .registers 3
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, LVM;->a:LYD;

    iget-object v0, v0, LYD;->a:LNE;

    iget-object v0, v0, LNE;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNS;

    iput-object v0, p1, LVL;->a:LNS;

    .line 67
    iget-object v0, p0, LVM;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LVM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, LVL;->a:Laoz;

    .line 73
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 161
    return-void
.end method
