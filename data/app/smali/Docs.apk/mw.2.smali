.class Lmw;
.super Ljava/lang/Object;
.source "PostEntry.java"

# interfaces
.implements Lmz;


# instance fields
.field private final a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

.field private final a:Lcom/google/api/services/discussions/model/Post;

.field private final a:Lmx;

.field private final a:Lmz;


# direct methods
.method constructor <init>(Lmz;Lcom/google/api/services/discussions/model/Post;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p2, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    .line 26
    iput-object p1, p0, Lmw;->a:Lmz;

    .line 27
    invoke-virtual {p2}, Lcom/google/api/services/discussions/model/Post;->a()Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    move-result-object v0

    iput-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    .line 28
    new-instance v0, Lmy;

    invoke-virtual {p2}, Lcom/google/api/services/discussions/model/Post;->a()Lcom/google/api/services/discussions/model/Author;

    move-result-object v1

    invoke-direct {v0, v1}, Lmy;-><init>(Lcom/google/api/services/discussions/model/Author;)V

    iput-object v0, p0, Lmw;->a:Lmx;

    .line 29
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 58
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post;->b()Lafo;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 59
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post;->b()Lafo;

    move-result-object v0

    invoke-virtual {v0}, Lafo;->a()J

    move-result-wide v0

    .line 66
    :goto_12
    return-wide v0

    .line 61
    :cond_13
    const-string v0, "PostEntry"

    const-string v1, "getPublishedMs: post without valid published time."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post;->a()Lafo;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 63
    const-string v0, "PostEntry"

    const-string v1, "getPublishedMs: post without a valid updated time."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post;->a()Lafo;

    move-result-object v0

    invoke-virtual {v0}, Lafo;->a()J

    move-result-wide v0

    goto :goto_12

    .line 66
    :cond_34
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    goto :goto_12
.end method

.method a()Lcom/google/api/services/discussions/model/Post;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post;->a()LaeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Post;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Lmx;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lmw;->a:Lmx;

    return-object v0
.end method

.method public a()Lmz;
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, Lmw;->a:Lmz;

    return-object v0
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 71
    const-string v0, "resolve"

    iget-object v1, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v1}, Lcom/google/api/services/discussions/model/Post;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 76
    const-string v0, "reopen"

    iget-object v1, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v1}, Lcom/google/api/services/discussions/model/Post;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 92
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_1e
    return-object v0

    :cond_1f
    invoke-virtual {p0}, Lmw;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1e
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->b()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->b()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 101
    iget-object v0, p0, Lmw;->a:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->b()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_1e
    return-object v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lmw;->a:Lmz;

    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method
