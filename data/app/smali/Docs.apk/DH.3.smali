.class LDH;
.super Ljava/lang/Object;
.source "SpannableRootContainer.java"

# interfaces
.implements LDz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeOwner:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LDz",
        "<TNodeOwner;>;"
    }
.end annotation


# instance fields
.field private final a:LDJ;


# direct methods
.method public constructor <init>(LDJ;)V
    .registers 2
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, LDH;->a:LDJ;

    .line 19
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public a()LDJ;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, LDH;->a:LDJ;

    return-object v0
.end method

.method public a(LDG;)LDz;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDG",
            "<TNodeOwner;>;)",
            "LDz",
            "<TNodeOwner;>;"
        }
    .end annotation

    .prologue
    .line 23
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot add siblings to root"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .registers 1

    .prologue
    .line 69
    return-void
.end method

.method public a(III)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    return-void
.end method

.method public a(Ljava/util/List;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDt;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 39
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 49
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, LDH;->a:LDJ;

    invoke-interface {v0}, LDJ;->a()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    return v0
.end method

.method public b(Ljava/util/List;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 45
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method
