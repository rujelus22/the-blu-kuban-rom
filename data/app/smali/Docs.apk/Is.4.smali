.class public LIs;
.super Ljava/lang/Object;
.source "TrixNativeGridView.java"

# interfaces
.implements LIu;


# instance fields
.field final synthetic a:F

.field final synthetic a:I

.field final synthetic a:Landroid/graphics/Canvas;

.field final synthetic a:Landroid/graphics/Paint;

.field final synthetic a:Landroid/graphics/Rect;

.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic a:Ljava/util/Map;

.field final synthetic b:F

.field final synthetic b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;Landroid/graphics/Rect;IIFLandroid/graphics/Canvas;Landroid/graphics/Paint;FLjava/util/Map;Ljava/util/ArrayList;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 575
    iput-object p1, p0, LIs;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iput-object p2, p0, LIs;->a:Landroid/graphics/Rect;

    iput p3, p0, LIs;->a:I

    iput p4, p0, LIs;->b:I

    iput p5, p0, LIs;->a:F

    iput-object p6, p0, LIs;->a:Landroid/graphics/Canvas;

    iput-object p7, p0, LIs;->a:Landroid/graphics/Paint;

    iput p8, p0, LIs;->b:F

    iput-object p9, p0, LIs;->a:Ljava/util/Map;

    iput-object p10, p0, LIs;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IIFFLHR;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 580
    sget-object v6, LHS;->a:LHS;

    .line 581
    iget-object v0, p0, LIs;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_18

    iget-object v0, p0, LIs;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, p2, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 582
    iget v0, p0, LIs;->a:I

    if-ne v0, p1, :cond_5f

    iget v0, p0, LIs;->b:I

    if-ne v0, p2, :cond_5f

    .line 583
    sget-object v6, LHS;->c:LHS;

    .line 589
    :cond_18
    :goto_18
    iget v0, p0, LIs;->a:F

    mul-float v1, p3, v0

    iget v0, p0, LIs;->a:F

    mul-float v2, p4, v0

    iget v3, p0, LIs;->a:F

    iget-object v4, p0, LIs;->a:Landroid/graphics/Canvas;

    iget-object v5, p0, LIs;->a:Landroid/graphics/Paint;

    move-object v0, p5

    invoke-virtual/range {v0 .. v6}, LHR;->a(FFFLandroid/graphics/Canvas;Landroid/graphics/Paint;LHS;)V

    .line 595
    iget-object v0, p0, LIs;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LHT;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, LHT;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 596
    new-instance v2, Landroid/graphics/Rect;

    iget v1, p0, LIs;->b:F

    mul-float/2addr v1, p3

    float-to-int v1, v1

    iget v3, p0, LIs;->b:F

    mul-float/2addr v3, p4

    float-to-int v3, v3

    float-to-double v4, p3

    invoke-virtual {p5}, LHR;->a()D

    move-result-wide v6

    add-double/2addr v4, v6

    iget v6, p0, LIs;->b:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    float-to-double v5, p4

    invoke-virtual {p5}, LHR;->b()D

    move-result-wide v7

    add-double/2addr v5, v7

    iget v7, p0, LIs;->b:F

    float-to-double v7, v7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-direct {v2, v1, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 601
    if-eqz v0, :cond_62

    .line 602
    iget-object v1, p0, LIs;->a:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    :goto_5e
    return-void

    .line 585
    :cond_5f
    sget-object v6, LHS;->b:LHS;

    goto :goto_18

    .line 604
    :cond_62
    iget-object v3, p0, LIs;->a:Ljava/util/ArrayList;

    new-instance v4, LIt;

    invoke-virtual {p5}, LHR;->a()Z

    move-result v0

    if-eqz v0, :cond_7e

    const/high16 v0, -0x100

    :goto_6e
    invoke-virtual {p5}, LHR;->b()Z

    move-result v1

    if-eqz v1, :cond_82

    const/high16 v1, -0x100

    :goto_76
    const/4 v5, 0x0

    invoke-direct {v4, v2, v0, v1, v5}, LIt;-><init>(Landroid/graphics/Rect;IILIo;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5e

    :cond_7e
    const v0, -0x333334

    goto :goto_6e

    :cond_82
    const v1, -0x333334

    goto :goto_76
.end method
