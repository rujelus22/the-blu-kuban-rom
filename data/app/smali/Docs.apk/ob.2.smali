.class public final Lob;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LoT;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LnF;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LnS;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Loa;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Low;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LoW;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LoQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 37
    iput-object p1, p0, Lob;->a:LYD;

    .line 38
    const-class v0, LoT;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lob;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lob;->a:LZb;

    .line 41
    const-class v0, LnF;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lob;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lob;->b:LZb;

    .line 44
    const-class v0, LnS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lob;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lob;->c:LZb;

    .line 47
    const-class v0, Loa;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lob;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lob;->d:LZb;

    .line 50
    const-class v0, Low;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lob;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lob;->e:LZb;

    .line 53
    const-class v0, LoW;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lob;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lob;->f:LZb;

    .line 56
    const-class v0, LoQ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lob;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lob;->g:LZb;

    .line 59
    return-void
.end method

.method static synthetic a(Lob;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lob;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 294
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    new-instance v1, Loc;

    invoke-direct {v1, p0}, Loc;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 302
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/IntroDialogFragment;

    new-instance v1, Loh;

    invoke-direct {v1, p0}, Loh;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 310
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    new-instance v1, Loi;

    invoke-direct {v1, p0}, Loi;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 318
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;

    new-instance v1, Loj;

    invoke-direct {v1, p0}, Loj;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 326
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;

    new-instance v1, Lok;

    invoke-direct {v1, p0}, Lok;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 334
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;

    new-instance v1, Lol;

    invoke-direct {v1, p0}, Lol;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 342
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/TermsOfServiceDialogFragment;

    new-instance v1, Lom;

    invoke-direct {v1, p0}, Lom;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 350
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;

    new-instance v1, Lon;

    invoke-direct {v1, p0}, Lon;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 358
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    new-instance v1, Loo;

    invoke-direct {v1, p0}, Loo;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 366
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    new-instance v1, Lod;

    invoke-direct {v1, p0}, Lod;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 374
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    new-instance v1, Loe;

    invoke-direct {v1, p0}, Loe;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 382
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;

    new-instance v1, Lof;

    invoke-direct {v1, p0}, Lof;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 390
    const-class v0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    new-instance v1, Log;

    invoke-direct {v1, p0}, Log;-><init>(Lob;)V

    invoke-virtual {p0, v0, v1}, Lob;->a(Ljava/lang/Class;Laou;)V

    .line 398
    const-class v0, LoT;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lob;->a:LZb;

    invoke-virtual {p0, v0, v1}, Lob;->a(Laop;LZb;)V

    .line 399
    const-class v0, LnF;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lob;->b:LZb;

    invoke-virtual {p0, v0, v1}, Lob;->a(Laop;LZb;)V

    .line 400
    const-class v0, LnS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lob;->c:LZb;

    invoke-virtual {p0, v0, v1}, Lob;->a(Laop;LZb;)V

    .line 401
    const-class v0, Loa;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lob;->d:LZb;

    invoke-virtual {p0, v0, v1}, Lob;->a(Laop;LZb;)V

    .line 402
    const-class v0, Low;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lob;->e:LZb;

    invoke-virtual {p0, v0, v1}, Lob;->a(Laop;LZb;)V

    .line 403
    const-class v0, LoW;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lob;->f:LZb;

    invoke-virtual {p0, v0, v1}, Lob;->a(Laop;LZb;)V

    .line 404
    const-class v0, LoQ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lob;->g:LZb;

    invoke-virtual {p0, v0, v1}, Lob;->a(Laop;LZb;)V

    .line 405
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 103
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXX;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LXX;

    .line 109
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LaaJ;

    .line 115
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LME;

    .line 121
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:Llf;

    .line 127
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 223
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a:LeQ;

    .line 229
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    invoke-virtual {v0, p1}, Lob;->a(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 67
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->b:LZb;

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->b:Laoz;

    .line 73
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/concurrent/Executor;

    .line 79
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlS;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:LlS;

    .line 85
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 145
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->b:Laoz;

    .line 151
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LnS;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LnS;

    .line 157
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LUL;

    .line 163
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Llf;

    .line 169
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 97
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 133
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loa;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a:Loa;

    .line 139
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/IntroDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 91
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 239
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 241
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlE;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a:LlE;

    .line 247
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a:Llf;

    .line 253
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 197
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 199
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LZM;

    .line 205
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LME;

    .line 211
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Low;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Low;

    .line 217
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 259
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LKS;

    .line 265
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LNe;

    .line 271
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Ljava/lang/Class;

    .line 277
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LPm;

    .line 283
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LZM;

    .line 289
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    invoke-virtual {v0, p1}, Lob;->a(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 235
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 187
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->g:LZb;

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/SortSelectionDialogFragment;->b:Laoz;

    .line 193
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/dialogs/TermsOfServiceDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 175
    iget-object v0, p0, Lob;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->f:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lob;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LoW;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/dialogs/TermsOfServiceDialogFragment;->a:LoW;

    .line 181
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 409
    return-void
.end method
