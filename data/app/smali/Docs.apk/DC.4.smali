.class LDC;
.super Ljava/lang/Object;
.source "SpannableGroup.java"

# interfaces
.implements LDE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LDE",
        "<",
        "LDG",
        "<TChildOwner;>;>;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic a:LDA;

.field a:LKh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LKh",
            "<",
            "LDG",
            "<TChildOwner;>;>;"
        }
    .end annotation
.end field

.field final synthetic b:I

.field final synthetic b:LKh;

.field final synthetic c:I


# direct methods
.method constructor <init>(LDA;LKh;II)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 243
    iput-object p1, p0, LDC;->a:LDA;

    iput-object p2, p0, LDC;->b:LKh;

    iput p3, p0, LDC;->b:I

    iput p4, p0, LDC;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iget-object v0, p0, LDC;->b:LKh;

    iput-object v0, p0, LDC;->a:LKh;

    .line 245
    iget v0, p0, LDC;->b:I

    iput v0, p0, LDC;->a:I

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 270
    iget v0, p0, LDC;->a:I

    return v0
.end method

.method public a()LDG;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDG",
            "<TChildOwner;>;"
        }
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, LDC;->a:LKh;

    invoke-interface {v0}, LKh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 258
    iget v1, p0, LDC;->a:I

    iget-object v2, p0, LDC;->a:LKh;

    invoke-interface {v2}, LKh;->b()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, LDC;->a:I

    .line 259
    iget-object v1, p0, LDC;->a:LKh;

    invoke-interface {v1}, LKh;->a()LKh;

    move-result-object v1

    iput-object v1, p0, LDC;->a:LKh;

    .line 260
    return-object v0
.end method

.method public hasNext()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 249
    iget-object v1, p0, LDC;->a:LKh;

    if-eqz v1, :cond_13

    iget-object v1, p0, LDC;->a:LKh;

    iget-object v2, p0, LDC;->a:LDA;

    invoke-static {v2}, LDA;->a(LDA;)LJO;

    move-result-object v2

    invoke-virtual {v2}, LJO;->b()LJR;

    move-result-object v2

    if-ne v1, v2, :cond_14

    .line 252
    :cond_13
    :goto_13
    return v0

    :cond_14
    iget v1, p0, LDC;->a:I

    iget v2, p0, LDC;->c:I

    if-ge v1, v2, :cond_13

    const/4 v0, 0x1

    goto :goto_13
.end method

.method public synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 243
    invoke-virtual {p0}, LDC;->a()LDG;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 265
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
