.class public LjV;
.super Ljava/lang/Object;
.source "TabletDocListActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 504
    iput-object p1, p0, LjV;->a:Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()LmK;
    .registers 4

    .prologue
    .line 521
    iget-object v0, p0, LjV;->a:Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)Ljc;

    move-result-object v0

    invoke-interface {v0}, Ljc;->a()LiQ;

    move-result-object v0

    .line 522
    invoke-interface {v0}, LiQ;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiE;

    .line 523
    instance-of v2, v0, LiW;

    if-eqz v2, :cond_e

    .line 524
    check-cast v0, LiW;

    .line 525
    invoke-virtual {v0}, LiW;->b()Z

    move-result v2

    if-nez v2, :cond_e

    .line 527
    invoke-virtual {v0}, LiW;->a()LmK;

    move-result-object v0

    .line 532
    :goto_2a
    return-object v0

    :cond_2b
    sget-object v0, LmK;->a:LmK;

    goto :goto_2a
.end method


# virtual methods
.method public run()V
    .registers 8

    .prologue
    .line 507
    sget-object v0, LmK;->e:LmK;

    sget-object v1, LmK;->h:LmK;

    sget-object v2, LmK;->g:LmK;

    sget-object v3, LmK;->f:LmK;

    sget-object v4, LmK;->j:LmK;

    sget-object v5, LmK;->k:LmK;

    sget-object v6, LmK;->l:LmK;

    invoke-static/range {v0 .. v6}, Laji;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Laji;

    move-result-object v0

    .line 516
    iget-object v1, p0, LjV;->a:Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v1

    invoke-direct {p0}, LjV;->a()LmK;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a(Lo;LmK;Laji;)V

    .line 518
    return-void
.end method
