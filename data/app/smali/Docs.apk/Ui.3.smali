.class public final enum LUi;
.super Ljava/lang/Enum;
.source "ModifySharingActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LUi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LUi;

.field private static final synthetic a:[LUi;

.field public static final enum b:LUi;

.field public static final enum c:LUi;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    new-instance v0, LUi;

    const-string v1, "POPULATING_ACLS"

    invoke-direct {v0, v1, v2}, LUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LUi;->a:LUi;

    .line 99
    new-instance v0, LUi;

    const-string v1, "SAVING_ACLS"

    invoke-direct {v0, v1, v3}, LUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LUi;->b:LUi;

    .line 100
    new-instance v0, LUi;

    const-string v1, "EDITING_ACLS"

    invoke-direct {v0, v1, v4}, LUi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LUi;->c:LUi;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [LUi;

    sget-object v1, LUi;->a:LUi;

    aput-object v1, v0, v2

    sget-object v1, LUi;->b:LUi;

    aput-object v1, v0, v3

    sget-object v1, LUi;->c:LUi;

    aput-object v1, v0, v4

    sput-object v0, LUi;->a:[LUi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LUi;
    .registers 2
    .parameter

    .prologue
    .line 96
    const-class v0, LUi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LUi;

    return-object v0
.end method

.method public static values()[LUi;
    .registers 1

    .prologue
    .line 96
    sget-object v0, LUi;->a:[LUi;

    invoke-virtual {v0}, [LUi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LUi;

    return-object v0
.end method
