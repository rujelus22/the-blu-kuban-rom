.class Lmq;
.super Ljava/lang/Object;
.source "DiscussionStreamEntry.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Lmz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lmq;",
        ">;",
        "Lmz;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

.field private final a:Lcom/google/api/services/discussions/model/Discussion;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lmx;

.field private final a:Z


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/model/Discussion;)V
    .registers 6
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion;

    .line 38
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v0

    iput-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    .line 39
    new-instance v0, Lmy;

    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion$Actor;

    move-result-object v1

    invoke-direct {v0, v1}, Lmy;-><init>(Lcom/google/api/services/discussions/model/Discussion$Actor;)V

    iput-object v0, p0, Lmq;->a:Lmx;

    .line 40
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_63

    const-string v1, "resolved"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    const/4 v0, 0x1

    :goto_25
    iput-boolean v0, p0, Lmq;->a:Z

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 44
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;

    move-result-object v0

    if-eqz v0, :cond_65

    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_65

    .line 45
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Post;

    .line 46
    new-instance v3, Lmw;

    invoke-direct {v3, p0, v0}, Lmw;-><init>(Lmz;Lcom/google/api/services/discussions/model/Post;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4e

    .line 41
    :cond_63
    const/4 v0, 0x0

    goto :goto_25

    .line 49
    :cond_65
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmq;->a:Ljava/util/List;

    .line 50
    return-void
.end method

.method private constructor <init>(Lcom/google/api/services/discussions/model/Discussion;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/discussions/model/Discussion;",
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion;

    .line 68
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v0

    iput-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    .line 69
    new-instance v0, Lmy;

    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion$Actor;

    move-result-object v1

    invoke-direct {v0, v1}, Lmy;-><init>(Lcom/google/api/services/discussions/model/Discussion$Actor;)V

    iput-object v0, p0, Lmq;->a:Lmx;

    .line 70
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_2e

    const-string v1, "resolved"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    const/4 v0, 0x1

    :goto_25
    iput-boolean v0, p0, Lmq;->a:Z

    .line 73
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmq;->a:Ljava/util/List;

    .line 74
    return-void

    .line 71
    :cond_2e
    const/4 v0, 0x0

    goto :goto_25
.end method

.method private a(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 77
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lmq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_21

    .line 78
    iget-object v0, p0, Lmq;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 82
    :goto_1c
    return v1

    .line 77
    :cond_1d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 82
    :cond_21
    const/4 v1, -0x1

    goto :goto_1c
.end method

.method static a(Lmq;Lcom/google/api/services/discussions/model/Post;)Landroid/util/Pair;
    .registers 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmq;",
            "Lcom/google/api/services/discussions/model/Post;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lmq;",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lmq;->a(Ljava/lang/String;)I

    move-result v1

    .line 140
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lmq;->a:Ljava/util/List;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 142
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()LaeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion;

    .line 143
    new-instance v3, Lmw;

    invoke-direct {v3, p0, p1}, Lmw;-><init>(Lmz;Lcom/google/api/services/discussions/model/Post;)V

    .line 144
    const/4 v4, -0x1

    if-eq v1, v4, :cond_4a

    .line 146
    invoke-interface {v2, v1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 182
    :cond_25
    :goto_25
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->a()Lafo;

    move-result-object v1

    invoke-virtual {v1}, Lafo;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lafo;

    move-result-object v1

    invoke-virtual {v1}, Lafo;->a()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-lez v1, :cond_40

    .line 183
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->a()Lafo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Discussion;->a(Lafo;)Lcom/google/api/services/discussions/model/Discussion;

    .line 185
    :cond_40
    new-instance v1, Lmq;

    invoke-direct {v1, v0, v2}, Lmq;-><init>(Lcom/google/api/services/discussions/model/Discussion;Ljava/util/List;)V

    invoke-static {v1, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 147
    :cond_4a
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lmq;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8d

    .line 149
    new-instance v1, Lcom/google/api/services/discussions/model/MimedcontentJson;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/MimedcontentJson;-><init>()V

    invoke-interface {v3}, Lmz;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v1

    const-string v4, "text/plain"

    invoke-virtual {v1, v4}, Lcom/google/api/services/discussions/model/MimedcontentJson;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v1

    .line 151
    new-instance v4, Lcom/google/api/services/discussions/model/MimedcontentJson;

    invoke-direct {v4}, Lcom/google/api/services/discussions/model/MimedcontentJson;-><init>()V

    invoke-interface {v3}, Lmz;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v4

    const-string v5, "text/plain"

    invoke-virtual {v4, v5}, Lcom/google/api/services/discussions/model/MimedcontentJson;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v4

    .line 153
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->b(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    .line 154
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    goto :goto_25

    .line 157
    :cond_8d
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b5

    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "resolve"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b5

    .line 161
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_e4

    .line 162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 164
    const-string v4, "resolved"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Discussion;->a(Ljava/util/List;)Lcom/google/api/services/discussions/model/Discussion;

    .line 172
    :cond_b5
    :goto_b5
    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c7

    invoke-virtual {p1}, Lcom/google/api/services/discussions/model/Post;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "resolve"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_25

    .line 174
    :cond_c7
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_25

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v1

    const-string v4, "resolved"

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 177
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v1

    const-string v4, "resolved"

    invoke-interface {v1, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_25

    .line 166
    :cond_e4
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v1

    const-string v4, "resolved"

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b5

    .line 168
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Ljava/util/List;

    move-result-object v1

    const-string v4, "resolved"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b5
.end method

.method static a(Lmq;Ljava/lang/String;)Lmq;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 108
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()LaeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion;

    .line 111
    new-instance v2, Ljava/util/ArrayList;

    iget-object v1, p0, Lmq;->a:Ljava/util/List;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 112
    invoke-direct {p0, p1}, Lmq;->a(Ljava/lang/String;)I

    move-result v3

    .line 114
    const/4 v1, -0x1

    if-ne v3, v1, :cond_2b

    .line 116
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a(Ljava/lang/Boolean;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    .line 126
    :goto_25
    new-instance v1, Lmq;

    invoke-direct {v1, v0, v2}, Lmq;-><init>(Lcom/google/api/services/discussions/model/Discussion;Ljava/util/List;)V

    return-object v1

    .line 119
    :cond_2b
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmz;

    .line 120
    check-cast v1, Lmw;

    invoke-virtual {v1}, Lmw;->a()Lcom/google/api/services/discussions/model/Post;

    move-result-object v1

    .line 121
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/api/services/discussions/model/Post;->a(Ljava/lang/Boolean;)Lcom/google/api/services/discussions/model/Post;

    .line 122
    new-instance v4, Lmw;

    invoke-direct {v4, p0, v1}, Lmw;-><init>(Lmz;Lcom/google/api/services/discussions/model/Post;)V

    .line 124
    invoke-interface {v2, v3, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_25
.end method


# virtual methods
.method public a(Lmq;)I
    .registers 5
    .parameter

    .prologue
    .line 54
    invoke-virtual {p0}, Lmq;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lmq;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    .line 56
    if-nez v0, :cond_38

    .line 57
    invoke-virtual {p1}, Lmq;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0}, Lmq;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    .line 58
    if-nez v0, :cond_38

    .line 60
    invoke-virtual {p0}, Lmq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lmq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 63
    :cond_38
    return v0
.end method

.method public a()J
    .registers 3

    .prologue
    .line 215
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->b()Lafo;

    move-result-object v0

    invoke-virtual {v0}, Lafo;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lmq;->a:Ljava/util/List;

    return-object v0
.end method

.method public a()Lmx;
    .registers 2

    .prologue
    .line 200
    iget-object v0, p0, Lmq;->a:Lmx;

    return-object v0
.end method

.method public a()Lmz;
    .registers 2

    .prologue
    .line 205
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method b()J
    .registers 3

    .prologue
    .line 269
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lafo;

    move-result-object v0

    invoke-virtual {v0}, Lafo;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 195
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 227
    const/4 v0, 0x0

    return v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 242
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->b()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->b()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 243
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->b()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    .line 245
    :goto_1e
    return-object v0

    :cond_1f
    invoke-virtual {p0}, Lmq;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1e
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 232
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 23
    check-cast p1, Lmq;

    invoke-virtual {p0, p1}, Lmq;->a(Lmq;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 250
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 251
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    .line 253
    :goto_1e
    return-object v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 237
    iget-boolean v0, p0, Lmq;->a:Z

    return v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 258
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->c()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->c()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 259
    iget-object v0, p0, Lmq;->a:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->c()Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b()Ljava/lang/String;

    move-result-object v0

    .line 261
    :goto_1e
    return-object v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1e
.end method
