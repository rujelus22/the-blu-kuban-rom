.class public final enum LhC;
.super Ljava/lang/Enum;
.source "MainProxyLogic.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LhC;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LhC;

.field private static final synthetic a:[LhC;

.field public static final enum b:LhC;

.field public static final enum c:LhC;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    new-instance v0, LhC;

    const-string v1, "OPEN_DOC_LIST"

    invoke-direct {v0, v1, v2}, LhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhC;->a:LhC;

    .line 98
    new-instance v0, LhC;

    const-string v1, "OPEN_ENTRY"

    invoke-direct {v0, v1, v3}, LhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhC;->b:LhC;

    .line 102
    new-instance v0, LhC;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v4}, LhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhC;->c:LhC;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [LhC;

    sget-object v1, LhC;->a:LhC;

    aput-object v1, v0, v2

    sget-object v1, LhC;->b:LhC;

    aput-object v1, v0, v3

    sget-object v1, LhC;->c:LhC;

    aput-object v1, v0, v4

    sput-object v0, LhC;->a:[LhC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LhC;
    .registers 2
    .parameter

    .prologue
    .line 96
    const-class v0, LhC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhC;

    return-object v0
.end method

.method public static values()[LhC;
    .registers 1

    .prologue
    .line 96
    sget-object v0, LhC;->a:[LhC;

    invoke-virtual {v0}, [LhC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LhC;

    return-object v0
.end method
