.class public final LxM;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LxX;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LxI;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LxJ;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LyZ;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LxY;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lxt;",
            ">;"
        }
    .end annotation
.end field

.field public h:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lyz;",
            ">;"
        }
    .end annotation
.end field

.field public i:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lcom/google/android/apps/docs/editors/kix/EditorStatusFragment;",
            ">;"
        }
    .end annotation
.end field

.field public j:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LzO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 40
    iput-object p1, p0, LxM;->a:LYD;

    .line 41
    const-class v0, LxX;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->a:LZb;

    .line 44
    const-class v0, LxI;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->b:LZb;

    .line 47
    const-class v0, LxJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->c:LZb;

    .line 50
    const-class v0, Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->d:LZb;

    .line 53
    const-class v0, LyZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->e:LZb;

    .line 56
    const-class v0, LxY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->f:LZb;

    .line 59
    const-class v0, Lxt;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->g:LZb;

    .line 62
    const-class v0, Lyz;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->h:LZb;

    .line 65
    const-class v0, Lcom/google/android/apps/docs/editors/kix/EditorStatusFragment;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->i:LZb;

    .line 68
    const-class v0, LzO;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LxM;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LxM;->j:LZb;

    .line 71
    return-void
.end method

.method static synthetic a(LxM;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LxM;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 269
    const-class v0, Lcom/google/android/apps/docs/editors/kix/UnsavedChangesDialogFragment;

    new-instance v1, LxN;

    invoke-direct {v1, p0}, LxN;-><init>(LxM;)V

    invoke-virtual {p0, v0, v1}, LxM;->a(Ljava/lang/Class;Laou;)V

    .line 277
    const-class v0, Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    new-instance v1, LxP;

    invoke-direct {v1, p0}, LxP;-><init>(LxM;)V

    invoke-virtual {p0, v0, v1}, LxM;->a(Ljava/lang/Class;Laou;)V

    .line 285
    const-class v0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    new-instance v1, LxQ;

    invoke-direct {v1, p0}, LxQ;-><init>(LxM;)V

    invoke-virtual {p0, v0, v1}, LxM;->a(Ljava/lang/Class;Laou;)V

    .line 293
    const-class v0, LxX;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->a:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 294
    const-class v0, LxI;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->b:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 295
    const-class v0, LxJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->c:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 296
    const-class v0, Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->d:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 297
    const-class v0, LyZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->e:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 298
    const-class v0, LxY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->f:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 299
    const-class v0, Lxt;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->g:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 300
    const-class v0, Lyz;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->h:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 301
    const-class v0, Lcom/google/android/apps/docs/editors/kix/EditorStatusFragment;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->i:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 302
    const-class v0, LzO;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LxM;->j:LZb;

    invoke-virtual {p0, v0, v1}, LxM;->a(Laop;LZb;)V

    .line 303
    iget-object v0, p0, LxM;->a:LZb;

    iget-object v1, p0, LxM;->a:LYD;

    iget-object v1, v1, LYD;->a:LxM;

    iget-object v1, v1, LxM;->f:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 305
    iget-object v0, p0, LxM;->b:LZb;

    iget-object v1, p0, LxM;->a:LYD;

    iget-object v1, v1, LYD;->a:LxM;

    iget-object v1, v1, LxM;->c:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 307
    iget-object v0, p0, LxM;->c:LZb;

    new-instance v1, LxR;

    invoke-direct {v1, p0}, LxR;-><init>(LxM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 321
    iget-object v0, p0, LxM;->d:LZb;

    new-instance v1, LxS;

    invoke-direct {v1, p0}, LxS;-><init>(LxM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 332
    iget-object v0, p0, LxM;->e:LZb;

    new-instance v1, LxT;

    invoke-direct {v1, p0}, LxT;-><init>(LxM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 351
    iget-object v0, p0, LxM;->f:LZb;

    new-instance v1, LxU;

    invoke-direct {v1, p0}, LxU;-><init>(LxM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 360
    iget-object v0, p0, LxM;->g:LZb;

    new-instance v1, LxV;

    invoke-direct {v1, p0}, LxV;-><init>(LxM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 379
    iget-object v0, p0, LxM;->h:LZb;

    new-instance v1, LxW;

    invoke-direct {v1, p0}, LxW;-><init>(LxM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 428
    iget-object v0, p0, LxM;->i:LZb;

    new-instance v1, LxO;

    invoke-direct {v1, p0}, LxO;-><init>(LxM;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 437
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    iget-object v0, v0, LxM;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxI;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LxI;

    .line 105
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    iget-object v0, v0, LxM;->g:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxt;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lxt;

    .line 111
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:Lms;

    iget-object v0, v0, Lms;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlX;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LlX;

    .line 117
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LAZ;

    iget-object v0, v0, LAZ;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBd;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    .line 123
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    iget-object v0, v0, LxM;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LyZ;

    .line 129
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LCO;

    iget-object v0, v0, LCO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    .line 135
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ljava/lang/String;

    .line 141
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    iget-object v0, v0, LxM;->i:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/EditorStatusFragment;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/EditorStatusFragment;

    .line 147
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    iget-object v0, v0, LxM;->h:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyz;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    .line 153
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lil;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lil;

    .line 159
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKS;

    .line 165
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:Lkr;

    iget-object v0, v0, Lkr;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkj;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lkj;

    .line 171
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LAZ;

    iget-object v0, v0, LAZ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAS;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LAS;

    .line 177
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LBJ;

    iget-object v0, v0, LBJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    .line 183
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LsO;

    iget-object v0, v0, LsO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;

    .line 189
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    iget-object v0, v0, LxM;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    .line 195
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LPm;

    .line 201
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLc;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LLc;

    .line 207
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LNe;

    .line 213
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LUL;

    .line 219
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    .line 225
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LBJ;

    iget-object v0, v0, LBJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    .line 231
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lgl;

    .line 237
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LJT;

    iget-object v0, v0, LJT;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKt;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    .line 243
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LJT;

    iget-object v0, v0, LJT;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKt;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;->a:LKt;

    .line 95
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/kix/UnsavedChangesDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 79
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    iget-object v0, v0, LxM;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzO;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/kix/UnsavedChangesDialogFragment;->a:LzO;

    .line 85
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 441
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LxM;

    invoke-virtual {v0}, LxM;->c()V

    .line 443
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 246
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    sput-object v0, Lzj;->a:LeQ;

    .line 252
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LAe;

    iget-object v0, v0, LAe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAo;

    sput-object v0, Lzj;->a:LAo;

    .line 258
    iget-object v0, p0, LxM;->a:LYD;

    iget-object v0, v0, LYD;->a:LAe;

    iget-object v0, v0, LAe;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LxM;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAh;

    sput-object v0, Lzj;->a:LAh;

    .line 264
    return-void
.end method
