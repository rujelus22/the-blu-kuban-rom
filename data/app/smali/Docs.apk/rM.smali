.class public LrM;
.super Ljava/lang/Object;
.source "ItemToUpload.java"


# instance fields
.field private a:Landroid/content/ContentResolver;

.field private final a:LrK;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Laoz;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Laoz",
            "<+",
            "LZS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    new-instance v0, LrK;

    invoke-direct {v0, p2}, LrK;-><init>(Laoz;)V

    iput-object v0, p0, LrM;->a:LrK;

    .line 228
    iput-object p1, p0, LrM;->a:Landroid/content/ContentResolver;

    .line 229
    return-void
.end method

.method public constructor <init>(LrK;)V
    .registers 4
    .parameter

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    new-instance v0, LrK;

    invoke-static {p1}, LrK;->a(LrK;)Laoz;

    move-result-object v1

    invoke-direct {v0, v1}, LrK;-><init>(Laoz;)V

    iput-object v0, p0, LrM;->a:LrK;

    .line 234
    invoke-static {p1}, LrK;->c(LrK;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LrM;->a(Ljava/lang/String;)LrM;

    move-result-object v0

    invoke-static {p1}, LrK;->a(LrK;)Laay;

    move-result-object v1

    invoke-virtual {v0, v1}, LrM;->a(Laay;)LrM;

    move-result-object v0

    invoke-static {p1}, LrK;->a(LrK;)LrN;

    move-result-object v1

    invoke-direct {v0, v1}, LrM;->a(LrN;)LrM;

    move-result-object v0

    invoke-static {p1}, LrK;->b(LrK;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LrM;->c(Ljava/lang/String;)LrM;

    move-result-object v0

    invoke-static {p1}, LrK;->b(LrK;)Z

    move-result v1

    invoke-virtual {v0, v1}, LrM;->a(Z)LrM;

    move-result-object v0

    invoke-static {p1}, LrK;->a(LrK;)Z

    move-result v1

    invoke-virtual {v0, v1}, LrM;->b(Z)LrM;

    move-result-object v0

    invoke-static {p1}, LrK;->a(LrK;)I

    move-result v1

    invoke-virtual {v0, v1}, LrM;->a(I)LrM;

    move-result-object v0

    invoke-static {p1}, LrK;->a(LrK;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LrM;->d(Ljava/lang/String;)LrM;

    .line 242
    return-void
.end method

.method private a(LrN;)LrM;
    .registers 3
    .parameter

    .prologue
    .line 309
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->a(LrK;LrN;)LrN;

    .line 310
    return-object p0
.end method


# virtual methods
.method public a()LrK;
    .registers 2

    .prologue
    .line 303
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0}, LrK;->a(LrK;)LrN;

    move-result-object v0

    if-nez v0, :cond_10

    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0}, LrK;->a(LrK;)Laay;

    move-result-object v0

    if-eqz v0, :cond_17

    :cond_10
    const/4 v0, 0x1

    :goto_11
    invoke-static {v0}, Lagu;->b(Z)V

    .line 305
    iget-object v0, p0, LrM;->a:LrK;

    return-object v0

    .line 303
    :cond_17
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public a(I)LrM;
    .registers 3
    .parameter

    .prologue
    .line 293
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->a(LrK;I)I

    .line 294
    return-object p0
.end method

.method public a(Laay;)LrM;
    .registers 3
    .parameter

    .prologue
    .line 252
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->a(LrK;Laay;)Laay;

    .line 253
    return-object p0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;)LrM;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 262
    iget-object v0, p0, LrM;->a:LrK;

    new-instance v1, LrP;

    iget-object v2, p0, LrM;->a:Landroid/content/ContentResolver;

    invoke-direct {v1, p1, p2, v2}, LrP;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentResolver;)V

    invoke-static {v0, v1}, LrK;->a(LrK;LrN;)LrN;

    .line 263
    return-object p0
.end method

.method public a(Ljava/lang/String;)LrM;
    .registers 3
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->a(LrK;Ljava/lang/String;)Ljava/lang/String;

    .line 247
    return-object p0
.end method

.method public a(Z)LrM;
    .registers 3
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->a(LrK;Z)Z

    .line 277
    return-object p0
.end method

.method public b(Ljava/lang/String;)LrM;
    .registers 4
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, LrM;->a:LrK;

    new-instance v1, LrO;

    invoke-direct {v1, p1}, LrO;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, LrK;->a(LrK;LrN;)LrN;

    .line 258
    return-object p0
.end method

.method public b(Z)LrM;
    .registers 3
    .parameter

    .prologue
    .line 284
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->b(LrK;Z)Z

    .line 285
    return-object p0
.end method

.method public c(Ljava/lang/String;)LrM;
    .registers 3
    .parameter

    .prologue
    .line 268
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->b(LrK;Ljava/lang/String;)Ljava/lang/String;

    .line 269
    return-object p0
.end method

.method public d(Ljava/lang/String;)LrM;
    .registers 3
    .parameter

    .prologue
    .line 298
    iget-object v0, p0, LrM;->a:LrK;

    invoke-static {v0, p1}, LrK;->c(LrK;Ljava/lang/String;)Ljava/lang/String;

    .line 299
    return-object p0
.end method
