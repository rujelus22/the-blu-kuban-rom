.class LajZ;
.super Ljava/util/AbstractList;
.source "Lists.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 741
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 742
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LajZ;->a:Ljava/util/List;

    .line 743
    return-void
.end method

.method private a(I)I
    .registers 3
    .parameter

    .prologue
    .line 750
    invoke-virtual {p0}, LajZ;->size()I

    move-result v0

    .line 751
    invoke-static {p1, v0}, Lagu;->a(II)I

    .line 752
    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method static synthetic a(LajZ;I)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 738
    invoke-direct {p0, p1}, LajZ;->b(I)I

    move-result v0

    return v0
.end method

.method private b(I)I
    .registers 3
    .parameter

    .prologue
    .line 756
    invoke-virtual {p0}, LajZ;->size()I

    move-result v0

    .line 757
    invoke-static {p1, v0}, Lagu;->b(II)I

    .line 758
    sub-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 746
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    return-object v0
.end method

.method public add(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 762
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LajZ;->b(I)I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 763
    return-void
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 766
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 767
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 794
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 798
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 782
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LajZ;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 808
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    .line 809
    if-ltz v0, :cond_d

    invoke-direct {p0, v0}, LajZ;->a(I)I

    move-result v0

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 786
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 818
    invoke-virtual {p0}, LajZ;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 813
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 814
    if-ltz v0, :cond_d

    invoke-direct {p0, v0}, LajZ;->a(I)I

    move-result v0

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 822
    invoke-direct {p0, p1}, LajZ;->b(I)I

    move-result v0

    .line 823
    iget-object v1, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 824
    new-instance v1, Laka;

    invoke-direct {v1, p0, v0}, Laka;-><init>(LajZ;Ljava/util/ListIterator;)V

    return-object v1
.end method

.method public remove(I)Ljava/lang/Object;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 770
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LajZ;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected removeRange(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 774
    invoke-virtual {p0, p1, p2}, LajZ;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 775
    return-void
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation

    .prologue
    .line 778
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LajZ;->a(I)I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 790
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 802
    invoke-virtual {p0}, LajZ;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lagu;->a(III)V

    .line 803
    iget-object v0, p0, LajZ;->a:Ljava/util/List;

    invoke-direct {p0, p2}, LajZ;->b(I)I

    move-result v1

    invoke-direct {p0, p1}, LajZ;->b(I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LajX;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
