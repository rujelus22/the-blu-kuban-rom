.class public final LEv;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final account_bar_height:I = 0x7f07002d

.field public static final action_bar_height:I = 0x7f070042

.field public static final collaborator_padding_top:I = 0x7f070009

.field public static final color_pallete_width:I = 0x7f07000a

.field public static final default_list_divider_height:I = 0x7f07002f

.field public static final default_list_item_height:I = 0x7f07002e

.field public static final discussion_holder_bottom_height:I = 0x7f070015

.field public static final discussion_holder_right_width:I = 0x7f070014

.field public static final doclist_entry_height:I = 0x7f070029

.field public static final doclist_group_separator_height:I = 0x7f07002a

.field public static final doclist_group_title_height:I = 0x7f07002b

.field public static final doclist_group_title_padding:I = 0x7f07002c

.field public static final doubletap_slop:I = 0x7f070000

.field public static final drag_knob_below_view_fix:I = 0x7f07004c

.field public static final drag_knob_extra_touch_zone_per_side:I = 0x7f070049

.field public static final drag_knob_pic_size:I = 0x7f07004a

.field public static final drag_knob_width_visible_size:I = 0x7f07004b

.field public static final drag_shadow_height:I = 0x7f07004f

.field public static final drag_shadow_text_size:I = 0x7f07004d

.field public static final drag_shadow_width:I = 0x7f07004e

.field public static final fastscroll_overlay_size:I = 0x7f070034

.field public static final fastscroll_thumb_height:I = 0x7f070008

.field public static final fastscroll_thumb_width:I = 0x7f070007

.field public static final font_picker_width:I = 0x7f07000b

.field public static final gf_list_item_height:I = 0x7f070005

.field public static final gf_list_item_padding:I = 0x7f070002

.field public static final gf_one_half_padding:I = 0x7f070004

.field public static final gf_padding:I = 0x7f070001

.field public static final gf_standard_text_padding:I = 0x7f070006

.field public static final gf_twice_padding:I = 0x7f070003

.field public static final grouper_title_bar_height:I = 0x7f070027

.field public static final home_screen_filters_top_padding:I = 0x7f070025

.field public static final home_screen_top_padding:I = 0x7f070024

.field public static final kix_edit_bottom_padding:I = 0x7f07000c

.field public static final kix_edit_horizontal_scrollbar_bottom_padding:I = 0x7f070011

.field public static final kix_edit_horizontal_scrollbar_height:I = 0x7f070012

.field public static final kix_edit_left_padding:I = 0x7f07000d

.field public static final kix_edit_left_padding_tablet:I = 0x7f07000f

.field public static final kix_edit_right_padding:I = 0x7f07000e

.field public static final kix_edit_right_padding_tablet:I = 0x7f070010

.field public static final kix_edit_scrollbar_edge_offset:I = 0x7f070013

.field public static final max_document_title_width:I = 0x7f070050

.field public static final navigation_panel_narrow_width:I = 0x7f070036

.field public static final navigation_panel_width:I = 0x7f070035

.field public static final pin_row_button_height:I = 0x7f070037

.field public static final pin_row_button_width:I = 0x7f070038

.field public static final preview_panel_width:I = 0x7f070039

.field public static final punch_main_webview_frame_width_thick:I = 0x7f07003a

.field public static final punch_main_webview_frame_width_thin:I = 0x7f07003b

.field public static final punch_main_webview_left_margin:I = 0x7f07003c

.field public static final punch_main_webview_side_margin:I = 0x7f07003d

.field public static final punch_slide_picker_height_tablet:I = 0x7f07003e

.field public static final punch_slide_picker_width_tablet:I = 0x7f070052

.field public static final punch_speaker_notes_content_padding_left:I = 0x7f07003f

.field public static final punch_speaker_notes_content_width:I = 0x7f070040

.field public static final punch_speaker_notes_presence_width:I = 0x7f070041

.field public static final quick_actions_bar_border:I = 0x7f070030

.field public static final select_entry_row_height:I = 0x7f070051

.field public static final sharing_grouper_height:I = 0x7f070028

.field public static final speaker_notes_content_close_button_side_margin:I = 0x7f070045

.field public static final speaker_notes_content_left_margin:I = 0x7f070044

.field public static final speaker_notes_content_right_margin:I = 0x7f070043

.field public static final tablet_doclist_entry_height:I = 0x7f070046

.field public static final tablet_doclist_group_separator_height:I = 0x7f070047

.field public static final tablet_doclist_padding:I = 0x7f070048

.field public static final title_bar_height:I = 0x7f070026

.field public static final trix_cell_edge_margin:I = 0x7f070023

.field public static final trix_keyboard_bar_height:I = 0x7f070016

.field public static final trix_sheets_tab_add_button_width:I = 0x7f07001a

.field public static final trix_sheets_tab_bar_left_margin:I = 0x7f070017

.field public static final trix_sheets_tab_height:I = 0x7f070019

.field public static final trix_sheets_tab_menu_item_height:I = 0x7f07001c

.field public static final trix_sheets_tab_menu_item_text_size:I = 0x7f070020

.field public static final trix_sheets_tab_menu_item_vertical_margin:I = 0x7f070021

.field public static final trix_sheets_tab_menu_title_text_size:I = 0x7f07001f

.field public static final trix_sheets_tab_menu_width:I = 0x7f07001b

.field public static final trix_sheets_tab_menu_window_padding:I = 0x7f07001d

.field public static final trix_sheets_tab_name_text_size:I = 0x7f07001e

.field public static final trix_sheets_tab_width:I = 0x7f070018

.field public static final trix_vertical_overscroll:I = 0x7f070022

.field public static final widget_button_width:I = 0x7f070031

.field public static final widget_logo_width:I = 0x7f070032

.field public static final widget_margin_left_right:I = 0x7f070033


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
