.class public Lfw;
.super Les;
.source "CreateNewDocActivity.java"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic a:Landroid/os/Handler;

.field final synthetic a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic a:LkB;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/CreateNewDocActivity;Ljava/lang/String;Landroid/os/Handler;Ljava/util/concurrent/atomic/AtomicReference;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;LkB;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 219
    iput-object p1, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iput-object p3, p0, Lfw;->a:Landroid/os/Handler;

    iput-object p4, p0, Lfw;->a:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p5, p0, Lfw;->a:Landroid/app/Activity;

    iput-object p6, p0, Lfw;->a:Ljava/lang/String;

    iput-object p7, p0, Lfw;->b:Ljava/lang/String;

    iput-object p8, p0, Lfw;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    iput-object p9, p0, Lfw;->a:LkB;

    invoke-direct {p0, p2}, Les;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .registers 4
    .parameter

    .prologue
    .line 286
    iget-object v0, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iget-object v1, p0, Lfw;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 287
    iget-object v1, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LZM;

    invoke-interface {v1, v0, p1}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 288
    iget-object v0, p0, Lfw;->a:Landroid/os/Handler;

    new-instance v1, Lfz;

    invoke-direct {v1, p0}, Lfz;-><init>(Lfw;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 304
    return-void
.end method


# virtual methods
.method public c()V
    .registers 6

    .prologue
    const/4 v3, 0x1

    .line 222
    iget-object v0, p0, Lfw;->a:Landroid/os/Handler;

    new-instance v1, Lfx;

    invoke-direct {v1, p0}, Lfx;-><init>(Lfw;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 230
    new-instance v1, LVV;

    invoke-direct {v1}, LVV;-><init>()V

    .line 231
    iget-object v0, p0, Lfw;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, LVV;->v(Ljava/lang/String;)V

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://schemas.google.com/docs/2007#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lfw;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    iget-object v2, v2, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:LkP;

    invoke-virtual {v2}, LkP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LVV;->l(Ljava/lang/String;)V

    .line 233
    const-string v0, "http://schemas.google.com/g/2005#kind"

    invoke-virtual {v1, v0}, LVV;->m(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a8

    .line 238
    iget-object v0, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, LWB;->a(Ljava/lang/String;Z)LWB;

    move-result-object v0

    const-string v2, "https://docs.google.com/feeds/default/private/full/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, LWB;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 244
    :goto_54
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "showdeleted=true&showroot=true"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 248
    :try_start_62
    iget-object v2, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iget-object v2, v2, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LVK;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-static {v3}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3, v1}, LVK;->a(Ljava/lang/String;Ljava/lang/String;LasT;)LasT;

    move-result-object v0

    .line 250
    instance-of v1, v0, LVV;

    if-eqz v1, :cond_af

    .line 251
    check-cast v0, LVV;

    .line 252
    iget-object v1, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-static {v1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LVV;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    .line 253
    iget-object v2, p0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iget-object v2, v2, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LWY;

    iget-object v3, p0, Lfw;->a:LkB;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v0, v4}, LWY;->a(LkB;LVV;Ljava/lang/Boolean;)V

    .line 254
    iget-object v0, p0, Lfw;->a:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Context;LkY;Z)Landroid/content/Intent;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lfw;->a:Landroid/os/Handler;

    new-instance v2, Lfy;

    invoke-direct {v2, p0, v0}, Lfy;-><init>(Lfw;Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_a7
    .catch Latc; {:try_start_62 .. :try_end_a7} :catch_cc
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_a7} :catch_d1
    .catch LasH; {:try_start_62 .. :try_end_a7} :catch_d6
    .catch Ljava/text/ParseException; {:try_start_62 .. :try_end_a7} :catch_db
    .catch Ljava/lang/RuntimeException; {:try_start_62 .. :try_end_a7} :catch_e0

    .line 283
    :goto_a7
    return-void

    .line 241
    :cond_a8
    const-string v0, "https://docs.google.com/feeds/default/private/full/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_54

    .line 270
    :cond_af
    :try_start_af
    new-instance v1, Latc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected Entry class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Latc;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_cc
    .catch Latc; {:try_start_af .. :try_end_cc} :catch_cc
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_cc} :catch_d1
    .catch LasH; {:try_start_af .. :try_end_cc} :catch_d6
    .catch Ljava/text/ParseException; {:try_start_af .. :try_end_cc} :catch_db
    .catch Ljava/lang/RuntimeException; {:try_start_af .. :try_end_cc} :catch_e0

    .line 272
    :catch_cc
    move-exception v0

    .line 273
    invoke-direct {p0, v0}, Lfw;->a(Ljava/lang/Exception;)V

    goto :goto_a7

    .line 274
    :catch_d1
    move-exception v0

    .line 275
    invoke-direct {p0, v0}, Lfw;->a(Ljava/lang/Exception;)V

    goto :goto_a7

    .line 276
    :catch_d6
    move-exception v0

    .line 277
    invoke-direct {p0, v0}, Lfw;->a(Ljava/lang/Exception;)V

    goto :goto_a7

    .line 278
    :catch_db
    move-exception v0

    .line 279
    invoke-direct {p0, v0}, Lfw;->a(Ljava/lang/Exception;)V

    goto :goto_a7

    .line 280
    :catch_e0
    move-exception v0

    .line 281
    invoke-direct {p0, v0}, Lfw;->a(Ljava/lang/Exception;)V

    goto :goto_a7
.end method
