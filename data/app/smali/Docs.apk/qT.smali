.class public abstract LqT;
.super Ljava/lang/Object;
.source "EntriesGrouper.java"

# interfaces
.implements Lrj;


# instance fields
.field private a:I

.field a:Landroid/database/Cursor;

.field protected final a:Ljava/lang/String;

.field private final a:LqU;


# direct methods
.method constructor <init>(Ljava/lang/String;LqU;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, LqT;->a:Ljava/lang/String;

    .line 35
    iput-object p2, p0, LqT;->a:LqU;

    .line 36
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a()LPI;
    .registers 2

    .prologue
    .line 55
    sget-object v0, LPX;->e:LPX;

    invoke-virtual {v0}, LPX;->a()LPI;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()Landroid/widget/SectionIndexer;
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, LqT;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .registers 3
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, LqT;->a:Landroid/database/Cursor;

    .line 83
    invoke-virtual {p0}, LqT;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LqT;->a:I

    .line 84
    return-void
.end method

.method public abstract b()I
.end method

.method protected b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, LqT;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 47
    iget v0, p0, LqT;->a:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LqT;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LqT;->a:LqU;

    invoke-virtual {v1}, LqU;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
