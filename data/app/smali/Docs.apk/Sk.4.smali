.class public final LSk;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSj;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 32
    iput-object p1, p0, LSk;->a:LYD;

    .line 33
    const-class v0, LSj;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LSk;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LSk;->a:LZb;

    .line 36
    const-class v0, LSn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LSk;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LSk;->b:LZb;

    .line 39
    return-void
.end method

.method static synthetic a(LSk;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LSk;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 78
    const-class v0, Lcom/google/android/apps/docs/receivers/NetworkChangeReceiver;

    new-instance v1, LSl;

    invoke-direct {v1, p0}, LSl;-><init>(LSk;)V

    invoke-virtual {p0, v0, v1}, LSk;->a(Ljava/lang/Class;Laou;)V

    .line 86
    const-class v0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;

    new-instance v1, LSm;

    invoke-direct {v1, p0}, LSm;-><init>(LSk;)V

    invoke-virtual {p0, v0, v1}, LSk;->a(Ljava/lang/Class;Laou;)V

    .line 94
    const-class v0, LSj;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LSk;->a:LZb;

    invoke-virtual {p0, v0, v1}, LSk;->a(Laop;LZb;)V

    .line 95
    const-class v0, LSn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LSk;->b:LZb;

    invoke-virtual {p0, v0, v1}, LSk;->a(Laop;LZb;)V

    .line 96
    iget-object v0, p0, LSk;->a:LZb;

    iget-object v1, p0, LSk;->a:LYD;

    iget-object v1, v1, LYD;->a:LacL;

    iget-object v1, v1, LacL;->a:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 98
    iget-object v0, p0, LSk;->b:LZb;

    iget-object v1, p0, LSk;->a:LYD;

    iget-object v1, v1, LYD;->a:Llm;

    iget-object v1, v1, Llm;->f:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 100
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;)V
    .registers 3
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, LSk;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LME;

    .line 61
    iget-object v0, p0, LSk;->a:LYD;

    iget-object v0, v0, LYD;->a:LSk;

    iget-object v0, v0, LSk;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSj;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LSj;

    .line 67
    iget-object v0, p0, LSk;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:Llf;

    .line 73
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/receivers/NetworkChangeReceiver;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, LSk;->a:LYD;

    iget-object v0, v0, LYD;->a:LSk;

    iget-object v0, v0, LSk;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSk;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSn;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/NetworkChangeReceiver;->a:LSn;

    .line 51
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 104
    return-void
.end method
