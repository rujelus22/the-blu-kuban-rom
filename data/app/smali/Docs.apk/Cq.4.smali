.class public LCq;
.super Ljava/lang/Object;
.source "ParagraphSpanStyle.java"

# interfaces
.implements LCt;


# instance fields
.field private final a:D

.field private final a:I

.field private final a:LCw;

.field private final a:Landroid/text/Layout$Alignment;

.field private final a:Ljava/lang/String;

.field private final a:Lxr;

.field private final a:Lxu;

.field private final a:Z

.field private final b:D

.field private final b:Z

.field private final c:D

.field private final d:D

.field private final e:D


# direct methods
.method public constructor <init>(Lxc;Lxu;Ljava/lang/String;Lxg;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-interface {p1}, Lxc;->a()Z

    move-result v0

    iput-boolean v0, p0, LCq;->b:Z

    .line 66
    invoke-interface {p1}, Lxc;->a()I

    move-result v0

    iput v0, p0, LCq;->a:I

    .line 67
    iget v0, p0, LCq;->a:I

    packed-switch v0, :pswitch_data_8c

    .line 97
    iput-boolean v2, p0, LCq;->a:Z

    .line 98
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    .line 100
    :goto_1c
    invoke-interface {p1}, Lxc;->c()D

    move-result-wide v2

    iput-wide v2, p0, LCq;->c:D

    .line 101
    invoke-interface {p1}, Lxc;->d()D

    move-result-wide v2

    iput-wide v2, p0, LCq;->b:D

    .line 102
    invoke-interface {p1}, Lxc;->e()D

    move-result-wide v2

    iput-wide v2, p0, LCq;->a:D

    .line 103
    invoke-interface {p1}, Lxc;->b()D

    move-result-wide v2

    iput-wide v2, p0, LCq;->d:D

    .line 104
    invoke-interface {p1}, Lxc;->a()D

    move-result-wide v2

    iput-wide v2, p0, LCq;->e:D

    .line 105
    invoke-interface {p1}, Lxc;->b()I

    move-result v0

    invoke-static {v0}, Lxr;->a(I)Lxr;

    move-result-object v0

    iput-object v0, p0, LCq;->a:Lxr;

    .line 106
    iput-object p3, p0, LCq;->a:Ljava/lang/String;

    .line 107
    iput-object p2, p0, LCq;->a:Lxu;

    .line 108
    if-eqz p4, :cond_8a

    new-instance v0, LCw;

    invoke-direct {v0, p4, v1, p2}, LCw;-><init>(Lxg;Lvo;Lxu;)V

    :goto_4f
    iput-object v0, p0, LCq;->a:LCw;

    .line 110
    return-void

    .line 69
    :pswitch_52
    iget-boolean v0, p0, LCq;->b:Z

    if-eqz v0, :cond_5d

    .line 70
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    .line 74
    :goto_5a
    iput-boolean v2, p0, LCq;->a:Z

    goto :goto_1c

    .line 72
    :cond_5d
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    goto :goto_5a

    .line 77
    :pswitch_62
    iget-boolean v0, p0, LCq;->b:Z

    if-eqz v0, :cond_6e

    .line 78
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    .line 82
    :goto_6a
    const/4 v0, 0x1

    iput-boolean v0, p0, LCq;->a:Z

    goto :goto_1c

    .line 80
    :cond_6e
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    goto :goto_6a

    .line 85
    :pswitch_73
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    .line 86
    iput-boolean v2, p0, LCq;->a:Z

    goto :goto_1c

    .line 89
    :pswitch_7a
    iget-boolean v0, p0, LCq;->b:Z

    if-eqz v0, :cond_85

    .line 90
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    .line 94
    :goto_82
    iput-boolean v2, p0, LCq;->a:Z

    goto :goto_1c

    .line 92
    :cond_85
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    goto :goto_82

    :cond_8a
    move-object v0, v1

    .line 108
    goto :goto_4f

    .line 67
    :pswitch_data_8c
    .packed-switch 0x0
        :pswitch_52
        :pswitch_73
        :pswitch_7a
        :pswitch_62
    .end packed-switch
.end method

.method static synthetic a(LCq;)D
    .registers 3
    .parameter

    .prologue
    .line 30
    iget-wide v0, p0, LCq;->d:D

    return-wide v0
.end method

.method static synthetic a(LCq;)LCw;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LCq;->a:LCw;

    return-object v0
.end method

.method static synthetic a(LCq;)Landroid/text/Layout$Alignment;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LCq;->a:Landroid/text/Layout$Alignment;

    return-object v0
.end method

.method static synthetic a(LCq;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LCq;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LCq;)Lxu;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LCq;->a:Lxu;

    return-object v0
.end method

.method static synthetic a(LCq;)Z
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-boolean v0, p0, LCq;->a:Z

    return v0
.end method

.method static synthetic b(LCq;)D
    .registers 3
    .parameter

    .prologue
    .line 30
    iget-wide v0, p0, LCq;->e:D

    return-wide v0
.end method

.method static synthetic b(LCq;)Z
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-boolean v0, p0, LCq;->b:Z

    return v0
.end method

.method static synthetic c(LCq;)D
    .registers 3
    .parameter

    .prologue
    .line 30
    iget-wide v0, p0, LCq;->a:D

    return-wide v0
.end method

.method static synthetic d(LCq;)D
    .registers 3
    .parameter

    .prologue
    .line 30
    iget-wide v0, p0, LCq;->b:D

    return-wide v0
.end method

.method static synthetic e(LCq;)D
    .registers 3
    .parameter

    .prologue
    .line 30
    iget-wide v0, p0, LCq;->c:D

    return-wide v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 170
    iget v0, p0, LCq;->a:I

    return v0
.end method

.method public a()Lxr;
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, LCq;->a:Lxr;

    return-object v0
.end method

.method public a()LzI;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LzI",
            "<",
            "LCq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, LCr;

    invoke-direct {v0, p0}, LCr;-><init>(LCq;)V

    return-object v0
.end method

.method public a(LCt;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 114
    instance-of v1, p1, LCq;

    if-eqz v1, :cond_64

    .line 115
    check-cast p1, LCq;

    .line 116
    iget-object v1, p1, LCq;->a:Landroid/text/Layout$Alignment;

    iget-object v2, p0, LCq;->a:Landroid/text/Layout$Alignment;

    if-ne v1, v2, :cond_64

    iget-boolean v1, p1, LCq;->a:Z

    iget-boolean v2, p0, LCq;->a:Z

    if-ne v1, v2, :cond_64

    iget-wide v1, p1, LCq;->a:D

    iget-wide v3, p0, LCq;->a:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_64

    iget-wide v1, p1, LCq;->b:D

    iget-wide v3, p0, LCq;->b:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_64

    iget-wide v1, p1, LCq;->c:D

    iget-wide v3, p0, LCq;->c:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_64

    iget-wide v1, p1, LCq;->d:D

    iget-wide v3, p0, LCq;->d:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_64

    iget-wide v1, p1, LCq;->e:D

    iget-wide v3, p0, LCq;->e:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_64

    iget-object v1, p1, LCq;->a:Lxr;

    iget-object v2, p0, LCq;->a:Lxr;

    if-ne v1, v2, :cond_64

    iget-boolean v1, p1, LCq;->b:Z

    iget-boolean v2, p0, LCq;->b:Z

    if-ne v1, v2, :cond_64

    iget-object v1, p0, LCq;->a:Ljava/lang/String;

    if-eqz v1, :cond_65

    iget-object v1, p0, LCq;->a:Ljava/lang/String;

    iget-object v2, p1, LCq;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_64

    :goto_55
    iget-object v1, p0, LCq;->a:LCw;

    if-eqz v1, :cond_6a

    iget-object v1, p0, LCq;->a:LCw;

    iget-object v2, p1, LCq;->a:LCw;

    invoke-virtual {v1, v2}, LCw;->a(LCt;)Z

    move-result v1

    if-eqz v1, :cond_64

    :goto_63
    const/4 v0, 0x1

    .line 131
    :cond_64
    return v0

    .line 116
    :cond_65
    iget-object v1, p1, LCq;->a:Ljava/lang/String;

    if-nez v1, :cond_64

    goto :goto_55

    :cond_6a
    iget-object v1, p1, LCq;->a:LCw;

    if-nez v1, :cond_64

    goto :goto_63
.end method
