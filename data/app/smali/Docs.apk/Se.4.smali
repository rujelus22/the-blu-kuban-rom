.class public LSe;
.super Laoe;
.source "PunchWebViewModule.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Laoe;-><init>()V

    .line 20
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, LQk;

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, LSe;->a([Ljava/lang/Class;)V

    .line 30
    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, LQl;

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, LSe;->a([Ljava/lang/Class;)V

    .line 32
    const-class v0, LQN;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LSf;

    invoke-interface {v0, v1}, LaoM;->b(Ljava/lang/Class;)LaoR;

    .line 33
    const-class v0, LQV;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LQV;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 34
    const-class v0, LRC;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LRC;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 35
    const-class v0, LQY;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LQV;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 36
    const-class v0, LQR;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LQY;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 37
    const-class v0, LQU;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LRJ;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 38
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-string v1, "punchThumbnailCacheSize"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/annotation/Annotation;)LaoQ;

    move-result-object v1

    invoke-static {}, LZL;->c()Z

    move-result v0

    if-eqz v0, :cond_df

    const/16 v0, 0x14

    :goto_76
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, LaoQ;->a(Ljava/lang/Object;)V

    .line 41
    const-class v0, LQo;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LSi;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 42
    const-class v0, LLK;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LLK;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 44
    const-class v0, LRU;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LRU;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 46
    const-class v0, LSi;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LSi;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 48
    const-class v0, LSg;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LSg;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 49
    const-class v0, LRb;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LRb;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 51
    const-class v0, LRk;

    invoke-virtual {p0, v0}, LSe;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LRm;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 53
    return-void

    .line 38
    :cond_df
    const/16 v0, 0x50

    goto :goto_76
.end method
