.class public enum Lgi;
.super Ljava/lang/Enum;
.source "Feature.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lgi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lgi;

.field public static final enum B:Lgi;

.field public static final enum C:Lgi;

.field public static final enum a:Lgi;

.field private static final synthetic a:[Lgi;

.field public static final enum b:Lgi;

.field public static final enum c:Lgi;

.field public static final enum d:Lgi;

.field public static final enum e:Lgi;

.field public static final enum f:Lgi;

.field public static final enum g:Lgi;

.field public static final enum h:Lgi;

.field public static final enum i:Lgi;

.field public static final enum j:Lgi;

.field public static final enum k:Lgi;

.field public static final enum l:Lgi;

.field public static final enum m:Lgi;

.field public static final enum n:Lgi;

.field public static final enum o:Lgi;

.field public static final enum p:Lgi;

.field public static final enum q:Lgi;

.field public static final enum r:Lgi;

.field public static final enum s:Lgi;

.field public static final enum t:Lgi;

.field public static final enum u:Lgi;

.field public static final enum v:Lgi;

.field public static final enum w:Lgi;

.field public static final enum x:Lgi;

.field public static final enum y:Lgi;

.field public static final enum z:Lgi;


# instance fields
.field public final a:Lfo;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25
    new-instance v0, Lgi;

    const-string v1, "ACL_ADDITIONAL_ROLE"

    sget-object v2, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v4, v2}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->a:Lgi;

    .line 30
    new-instance v0, Lgi;

    const-string v1, "COMMENTS"

    sget-object v2, Lfo;->d:Lfo;

    invoke-direct {v0, v1, v5, v2}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->b:Lgi;

    .line 35
    new-instance v0, Lgi;

    const-string v1, "DRIVE"

    sget-object v2, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v6, v2}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->c:Lgi;

    .line 40
    new-instance v0, Lgj;

    const-string v1, "DRIVE_INVITATION_FLOW"

    sget-object v2, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v7, v2}, Lgj;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->d:Lgi;

    .line 50
    new-instance v0, Lgk;

    const-string v1, "DRIVE_SDK_APP"

    sget-object v2, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v8, v2}, Lgk;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->e:Lgi;

    .line 60
    new-instance v0, Lgi;

    const-string v1, "GSYNC"

    const/4 v2, 0x5

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->f:Lgi;

    .line 66
    new-instance v0, Lgi;

    const-string v1, "NOTIFICATIONS"

    const/4 v2, 0x6

    sget-object v3, Lfo;->c:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->g:Lgi;

    .line 71
    new-instance v0, Lgi;

    const-string v1, "PARANOID_CHECKS"

    const/4 v2, 0x7

    sget-object v3, Lfo;->c:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->h:Lgi;

    .line 76
    new-instance v0, Lgi;

    const-string v1, "PINNING"

    const/16 v2, 0x8

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->i:Lgi;

    .line 81
    new-instance v0, Lgi;

    const-string v1, "SEND_LINK"

    const/16 v2, 0x9

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->j:Lgi;

    .line 86
    new-instance v0, Lgi;

    const-string v1, "PRINT"

    const/16 v2, 0xa

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->k:Lgi;

    .line 91
    new-instance v0, Lgi;

    const-string v1, "PRINT_STATS_TO_LOG"

    const/16 v2, 0xb

    sget-object v3, Lfo;->c:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->l:Lgi;

    .line 96
    new-instance v0, Lgi;

    const-string v1, "PUNCH_PROFILING"

    const/16 v2, 0xc

    sget-object v3, Lfo;->d:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->m:Lgi;

    .line 101
    new-instance v0, Lgi;

    const-string v1, "PUNCH_WEB_VIEW"

    const/16 v2, 0xd

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->n:Lgi;

    .line 107
    new-instance v0, Lgi;

    const-string v1, "READ_UNREAD"

    const/16 v2, 0xe

    sget-object v3, Lfo;->c:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->o:Lgi;

    .line 112
    new-instance v0, Lgi;

    const-string v1, "STREAMING_DECRYPTION"

    const/16 v2, 0xf

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->p:Lgi;

    .line 117
    new-instance v0, Lgi;

    const-string v1, "SYNC_MORE"

    const/16 v2, 0x10

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->q:Lgi;

    .line 122
    new-instance v0, Lgi;

    const-string v1, "KIX_PROFILING"

    const/16 v2, 0x11

    sget-object v3, Lfo;->d:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->r:Lgi;

    .line 127
    new-instance v0, Lgi;

    const-string v1, "KIX_COMMENTS"

    const/16 v2, 0x12

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->s:Lgi;

    .line 132
    new-instance v0, Lgi;

    const-string v1, "KIX_FONT_PICKER"

    const/16 v2, 0x13

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->t:Lgi;

    .line 137
    new-instance v0, Lgi;

    const-string v1, "KIX_OPTICAL_ZOOM"

    const/16 v2, 0x14

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->u:Lgi;

    .line 142
    new-instance v0, Lgi;

    const-string v1, "KIX_TABLES"

    const/16 v2, 0x15

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->v:Lgi;

    .line 147
    new-instance v0, Lgi;

    const-string v1, "TRIX_NATIVE_EDITOR"

    const/16 v2, 0x16

    sget-object v3, Lfo;->d:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->w:Lgi;

    .line 152
    new-instance v0, Lgi;

    const-string v1, "TRIX_ENABLE_NATIVE_EDITING"

    const/16 v2, 0x17

    sget-object v3, Lfo;->d:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->x:Lgi;

    .line 157
    new-instance v0, Lgi;

    const-string v1, "TRIX_SHOW_DEBUG_INFO"

    const/16 v2, 0x18

    sget-object v3, Lfo;->d:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->y:Lgi;

    .line 162
    new-instance v0, Lgi;

    const-string v1, "_TEST_DOGFOOD"

    const/16 v2, 0x19

    sget-object v3, Lfo;->b:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->z:Lgi;

    .line 167
    new-instance v0, Lgi;

    const-string v1, "_TEST_RELEASE"

    const/16 v2, 0x1a

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->A:Lgi;

    .line 172
    new-instance v0, Lgi;

    const-string v1, "DRIVE_CSI"

    const/16 v2, 0x1b

    sget-object v3, Lfo;->b:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->B:Lgi;

    .line 177
    new-instance v0, Lgi;

    const-string v1, "MOVE_TO_FOLDER"

    const/16 v2, 0x1c

    sget-object v3, Lfo;->a:Lfo;

    invoke-direct {v0, v1, v2, v3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    sput-object v0, Lgi;->C:Lgi;

    .line 20
    const/16 v0, 0x1d

    new-array v0, v0, [Lgi;

    sget-object v1, Lgi;->a:Lgi;

    aput-object v1, v0, v4

    sget-object v1, Lgi;->b:Lgi;

    aput-object v1, v0, v5

    sget-object v1, Lgi;->c:Lgi;

    aput-object v1, v0, v6

    sget-object v1, Lgi;->d:Lgi;

    aput-object v1, v0, v7

    sget-object v1, Lgi;->e:Lgi;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lgi;->f:Lgi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgi;->g:Lgi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgi;->h:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lgi;->i:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lgi;->j:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lgi;->k:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lgi;->l:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lgi;->m:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lgi;->n:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lgi;->o:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lgi;->p:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lgi;->q:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lgi;->r:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lgi;->s:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lgi;->t:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lgi;->u:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lgi;->v:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lgi;->w:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lgi;->x:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lgi;->y:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lgi;->z:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lgi;->A:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lgi;->B:Lgi;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lgi;->C:Lgi;

    aput-object v2, v0, v1

    sput-object v0, Lgi;->a:[Lgi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILfo;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 181
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 182
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfo;

    iput-object v0, p0, Lgi;->a:Lfo;

    .line 183
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILfo;Lgj;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lgi;-><init>(Ljava/lang/String;ILfo;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgi;
    .registers 2
    .parameter

    .prologue
    .line 20
    const-class v0, Lgi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgi;

    return-object v0
.end method

.method public static values()[Lgi;
    .registers 1

    .prologue
    .line 20
    sget-object v0, Lgi;->a:[Lgi;

    invoke-virtual {v0}, [Lgi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgi;

    return-object v0
.end method


# virtual methods
.method protected a(Lgl;)Z
    .registers 3
    .parameter

    .prologue
    .line 190
    const/4 v0, 0x1

    return v0
.end method
