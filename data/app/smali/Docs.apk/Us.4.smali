.class public LUs;
.super Ljava/lang/Object;
.source "SharingInfoManagerImpl.java"

# interfaces
.implements LUr;


# instance fields
.field a:LTI;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LTl;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Landroid/content/Context;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method static synthetic a(LUs;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)LUq;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3, p4}, LUs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)LUq;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)LUq;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "LeB;",
            ">;)",
            "LUq;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 286
    new-instance v4, LUw;

    invoke-direct {v4, v1}, LUw;-><init>(LUt;)V

    .line 287
    sget-object v3, LeH;->n:LeH;

    .line 290
    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v1

    :goto_d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_73

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeB;

    .line 291
    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v6

    sget-object v7, LeK;->b:LeK;

    if-eq v6, v7, :cond_43

    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v6

    sget-object v7, LeK;->a:LeK;

    if-eq v6, v7, :cond_43

    .line 292
    invoke-virtual {v0}, LeB;->a()LeH;

    move-result-object v3

    .line 293
    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v2

    .line 294
    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v6

    sget-object v7, LeK;->c:LeK;

    if-ne v6, v7, :cond_84

    .line 295
    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    move-object v2, v3

    :goto_3f
    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 304
    goto :goto_d

    .line 298
    :cond_43
    iget-object v6, p0, LUs;->a:LTI;

    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v8

    invoke-interface {v6, v7, v8}, LTI;->a(Ljava/lang/String;LeK;)LTG;

    move-result-object v6

    .line 300
    new-instance v7, LUB;

    new-instance v8, LUp;

    invoke-direct {v8, v0}, LUp;-><init>(LeB;)V

    invoke-direct {v7, v6, v8}, LUB;-><init>(LTG;LUp;)V

    invoke-virtual {v4, v7}, LUw;->add(Ljava/lang/Object;)Z

    .line 301
    invoke-virtual {v0}, LeB;->a()LeI;

    move-result-object v6

    sget-object v7, LeI;->a:LeI;

    if-ne v6, v7, :cond_84

    if-nez v1, :cond_84

    .line 302
    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LUD;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_3f

    .line 306
    :cond_73
    new-instance v0, LMu;

    invoke-direct {v0}, LMu;-><init>()V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 307
    new-instance v0, LUx;

    invoke-direct {v0, p0, p1, p2, p3}, LUx;-><init>(LUs;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-static {v0, v4, v3, v2, v1}, LUx;->a(LUx;LUw;LeH;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    return-object v0

    :cond_84
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_3f
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LUq;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 314
    new-instance v0, LUx;

    invoke-direct {v0, p0, p1, p2, p3}, LUx;-><init>(LUs;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(LUq;)LamQ;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LUq;",
            ")",
            "LamQ",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    iget-object v0, p0, LUs;->a:Landroid/content/Context;

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 339
    new-instance v0, LKw;

    invoke-direct {v0}, LKw;-><init>()V

    invoke-static {v0}, LamF;->a(Ljava/lang/Throwable;)LamQ;

    move-result-object v0

    .line 372
    :goto_11
    return-object v0

    .line 341
    :cond_12
    invoke-interface {p1}, LUq;->a()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 342
    const/4 v0, 0x0

    invoke-static {v0}, LamF;->a(Ljava/lang/Object;)LamQ;

    move-result-object v0

    goto :goto_11

    .line 344
    :cond_1e
    invoke-interface {p1}, LUq;->a()Ljava/util/List;

    move-result-object v0

    .line 345
    invoke-static {}, LalO;->a()Ljava/util/HashSet;

    move-result-object v5

    .line 346
    invoke-static {}, LalO;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 347
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2e
    :goto_2e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 348
    invoke-virtual {v0}, LUB;->a()LeB;

    move-result-object v3

    invoke-virtual {v3}, LeB;->a()LeG;

    move-result-object v3

    sget-object v4, LeG;->f:LeG;

    invoke-virtual {v3, v4}, LeG;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_51

    .line 349
    invoke-virtual {v0}, LUB;->a()LeB;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 351
    :cond_51
    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v3

    invoke-virtual {v3}, LUp;->a()Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 352
    invoke-virtual {v0}, LUB;->a()LeB;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .line 356
    :cond_63
    invoke-interface {p1}, LUq;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 357
    invoke-interface {p1}, LUq;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 359
    invoke-interface {p1}, LUq;->e()Ljava/lang/String;

    move-result-object v2

    .line 360
    invoke-interface {p1}, LUq;->c()Ljava/lang/String;

    move-result-object v3

    .line 361
    invoke-interface {p1}, LUq;->d()Ljava/lang/String;

    move-result-object v4

    .line 363
    iget-object v0, p0, LUs;->a:LTl;

    invoke-interface {v0, v2, v1}, LTl;->a(Ljava/lang/String;Ljava/util/Set;)LamQ;

    move-result-object v6

    .line 365
    new-instance v0, LUu;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LUu;-><init>(LUs;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 372
    invoke-static {v6, v0}, LamF;->a(LamQ;Lagl;)LamQ;

    move-result-object v0

    goto :goto_11
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LamQ;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LamQ",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, LUs;->a:Landroid/content/Context;

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 321
    new-instance v0, LKw;

    invoke-direct {v0}, LKw;-><init>()V

    invoke-static {v0}, LamF;->a(Ljava/lang/Throwable;)LamQ;

    move-result-object v0

    .line 333
    :goto_11
    return-object v0

    .line 324
    :cond_12
    iget-object v0, p0, LUs;->a:LTl;

    invoke-interface {v0, p1, p2}, LTl;->a(Ljava/lang/String;Ljava/lang/String;)LamQ;

    move-result-object v0

    .line 326
    new-instance v1, LUt;

    invoke-direct {v1, p0, p1, p2, p3}, LUt;-><init>(LUs;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-static {v0, v1}, LamF;->a(LamQ;Lagl;)LamQ;

    move-result-object v0

    goto :goto_11
.end method
