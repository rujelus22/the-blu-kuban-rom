.class public LTh;
.super Ljava/lang/Object;
.source "UploadSharedItemActivityDelegate.java"

# interfaces
.implements LZA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LZA",
        "<",
        "Lcom/google/android/apps/docs/docsuploader/UploadQueueService;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

.field final synthetic a:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/util/List;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 669
    iput-object p1, p0, LTh;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iput-object p2, p0, LTh;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V
    .registers 6
    .parameter

    .prologue
    .line 672
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 674
    iget-object v0, p0, LTh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LrK;

    .line 675
    new-instance v3, Lsm;

    invoke-direct {v3, v0}, Lsm;-><init>(LrK;)V

    .line 676
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 679
    :cond_20
    invoke-virtual {p1, v1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Ljava/util/Collection;)V

    .line 680
    invoke-virtual {p1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a()Ljava/lang/Thread;

    .line 682
    iget-object v0, p0, LTh;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 683
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 669
    check-cast p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    invoke-virtual {p0, p1}, LTh;->a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V

    return-void
.end method
