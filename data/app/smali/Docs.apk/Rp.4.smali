.class LRp;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"


# instance fields
.field private final a:I

.field final synthetic a:LRo;

.field private final a:Landroid/webkit/WebView;

.field private a:Z

.field private b:I

.field private volatile b:Z

.field private c:I

.field private volatile c:Z

.field private d:Z


# direct methods
.method private constructor <init>(LRo;Landroid/webkit/WebView;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    iput-object p1, p0, LRp;->a:LRo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, LRp;->a:Z

    .line 132
    iput-object p2, p0, LRp;->a:Landroid/webkit/WebView;

    .line 133
    iput p3, p0, LRp;->a:I

    .line 134
    iget-object v0, p0, LRp;->a:Landroid/webkit/WebView;

    invoke-static {}, LRm;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 137
    iget-object v0, p0, LRp;->a:Landroid/webkit/WebView;

    new-instance v1, LRq;

    invoke-direct {v1, p0, p1, p3}, LRq;-><init>(LRp;LRo;I)V

    const-string v2, "ThumbnailApi"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    new-instance v0, LRr;

    invoke-direct {v0, p0, p1, p3}, LRr;-><init>(LRp;LRo;I)V

    .line 174
    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    .line 177
    new-instance v0, LRt;

    invoke-direct {v0, p0, p1, p3}, LRt;-><init>(LRp;LRo;I)V

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 192
    new-instance v0, LRu;

    invoke-direct {v0, p0, p1}, LRu;-><init>(LRp;LRo;)V

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 198
    return-void
.end method

.method synthetic constructor <init>(LRo;Landroid/webkit/WebView;ILRn;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, LRp;-><init>(LRo;Landroid/webkit/WebView;I)V

    return-void
.end method

.method private a()I
    .registers 8

    .prologue
    const/4 v2, -0x1

    .line 231
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->c(LRo;)I

    move-result v0

    .line 232
    iget-object v1, p0, LRp;->a:LRo;

    iget-object v1, v1, LRo;->a:LRm;

    iget v1, v1, LRm;->a:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 233
    iget-object v1, p0, LRp;->a:LRo;

    iget-object v1, v1, LRo;->a:LRm;

    invoke-static {v1}, LRm;->a(LRm;)LQR;

    move-result-object v1

    invoke-interface {v1}, LQR;->c()I

    move-result v1

    iget-object v3, p0, LRp;->a:LRo;

    iget-object v3, v3, LRo;->a:LRm;

    iget v3, v3, LRm;->a:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 237
    const/4 v0, 0x0

    :goto_2f
    iget-object v1, p0, LRp;->a:LRo;

    iget-object v1, v1, LRo;->a:LRm;

    iget v1, v1, LRm;->a:I

    if-ge v0, v1, :cond_81

    move v3, v2

    .line 239
    :goto_38
    const/4 v1, 0x1

    if-gt v3, v1, :cond_7e

    .line 240
    iget-object v1, p0, LRp;->a:LRo;

    invoke-static {v1}, LRo;->c(LRo;)I

    move-result v1

    mul-int v5, v3, v0

    add-int/2addr v1, v5

    .line 241
    if-ltz v1, :cond_54

    iget-object v5, p0, LRp;->a:LRo;

    iget-object v5, v5, LRo;->a:LRm;

    invoke-static {v5}, LRm;->a(LRm;)LQR;

    move-result-object v5

    invoke-interface {v5}, LQR;->c()I

    move-result v5

    if-lt v1, v5, :cond_58

    .line 239
    :cond_54
    add-int/lit8 v1, v3, 0x2

    move v3, v1

    goto :goto_38

    .line 245
    :cond_58
    sub-int v5, v1, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    iget-object v6, p0, LRp;->a:LRo;

    iget-object v6, v6, LRo;->a:LRm;

    iget v6, v6, LRm;->a:I

    div-int/lit8 v6, v6, 0x2

    if-ge v5, v6, :cond_54

    .line 249
    invoke-direct {p0, v1}, LRp;->a(I)Z

    move-result v5

    if-eqz v5, :cond_54

    iget-object v5, p0, LRp;->a:LRo;

    iget-object v5, v5, LRo;->a:LRm;

    invoke-static {v5}, LRm;->a(LRm;)LQR;

    move-result-object v5

    invoke-interface {v5, v1}, LQR;->a(I)Z

    move-result v5

    if-eqz v5, :cond_54

    move v0, v1

    .line 255
    :goto_7d
    return v0

    .line 238
    :cond_7e
    add-int/lit8 v0, v0, 0x1

    goto :goto_2f

    :cond_81
    move v0, v2

    .line 255
    goto :goto_7d
.end method

.method static synthetic a(LRp;)I
    .registers 2
    .parameter

    .prologue
    .line 106
    iget v0, p0, LRp;->b:I

    return v0
.end method

.method private a()Landroid/graphics/Bitmap;
    .registers 5

    .prologue
    .line 301
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    iget-object v1, p0, LRp;->a:LRo;

    iget-object v1, v1, LRo;->a:LRm;

    iget v1, v1, LRm;->a:I

    if-lt v0, v1, :cond_7c

    .line 302
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 303
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 304
    iget-object v2, p0, LRp;->a:LRo;

    invoke-static {v2}, LRo;->c(LRo;)I

    move-result v2

    sub-int v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget-object v3, p0, LRp;->a:LRo;

    invoke-static {v3}, LRo;->c(LRo;)I

    move-result v3

    sub-int v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v2, v3, :cond_78

    const/4 v2, 0x1

    .line 306
    :goto_4d
    if-eqz v2, :cond_7a

    .line 307
    :goto_4f
    const-string v1, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removing thumbnail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-object v1, p0, LRp;->a:LRo;

    invoke-static {v1}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 311
    :goto_77
    return-object v0

    .line 304
    :cond_78
    const/4 v2, 0x0

    goto :goto_4d

    :cond_7a
    move v0, v1

    .line 306
    goto :goto_4f

    .line 311
    :cond_7c
    const/4 v0, 0x0

    goto :goto_77
.end method

.method private a()V
    .registers 3

    .prologue
    .line 201
    iget-boolean v0, p0, LRp;->a:Z

    if-nez v0, :cond_f

    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_10

    .line 208
    :cond_f
    :goto_f
    return-void

    .line 205
    :cond_10
    const/4 v0, 0x1

    iput-boolean v0, p0, LRp;->a:Z

    .line 207
    invoke-direct {p0}, LRp;->c()V

    goto :goto_f
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 263
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "generateThumbnail(t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRp;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): cache.size()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRp;->a:LRo;

    invoke-static {v2}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRp;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " slideIndexToGenerate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iput p1, p0, LRp;->b:I

    .line 266
    invoke-direct {p0}, LRp;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_4f

    .line 268
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 273
    :cond_4f
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 274
    if-eqz v0, :cond_65

    .line 275
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 278
    :cond_65
    iget-object v0, p0, LRp;->a:LRo;

    iget-object v0, v0, LRo;->a:LRm;

    invoke-static {v0}, LRm;->a(LRm;)LQR;

    move-result-object v0

    new-instance v1, LRv;

    invoke-direct {v1, p0, p1}, LRv;-><init>(LRp;I)V

    invoke-interface {v0, p1, v1}, LQR;->a(ILQT;)V

    .line 298
    return-void
.end method

.method static synthetic a(LRp;)V
    .registers 1
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, LRp;->d()V

    return-void
.end method

.method static synthetic a(LRp;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0, p1}, LRp;->a(I)V

    return-void
.end method

.method static synthetic a(LRp;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, LRp;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 315
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRp;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Creating thumbnail: slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRp;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    iput-boolean v3, p0, LRp;->b:Z

    .line 318
    iput-boolean v3, p0, LRp;->d:Z

    .line 320
    const-string v0, "text/html"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 321
    if-eqz p2, :cond_7e

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, LRm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 325
    iget-object v1, p0, LRp;->a:LRo;

    iget-object v1, v1, LRo;->a:LRm;

    invoke-static {v1}, LRm;->a(LRm;)LKS;

    move-result-object v1

    const-string v2, "punchThumbnailHtmlTemplate"

    const-string v3, "<head>\n  <script type=\"text/javascript\">\n    function initialize() {\n      var imagesRemaining = -1;\n\n      document.getElementById(\"container\").innerHTML =\n        __ESCAPED_CONTENT__;\n\n      document.body.style.margin = \'0px\';\n      document.body.style.padding = \'0px\';\n\n      imagesRemaining = 0;\n      var i;\n      for (i in document.images) {\n        var img = document.images[i]\n        if (img.src != undefined) {\n          imagesRemaining++;\n          img.style.width = \'100%\'\n          img.style.height = \'100%\'\n          img.onload = function() {\n            imagesRemaining--;\n            if (imagesRemaining == 0) {\n              window.ThumbnailApi.onLoadImage();\n            }\n          };        }\n      }\n    }\n  </script>\n</head>\n<body onload=\"initialize();\">\n  <div id=\"container\" />\n</body>\n"

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327
    const-string v2, "__ESCAPED_CONTENT__"

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    move-object v2, p2

    .line 334
    :goto_6a
    iput-boolean v4, p0, LRp;->c:Z

    .line 335
    iget-object v0, p0, LRp;->a:Landroid/webkit/WebView;

    const-string v1, "fake-url"

    sget-object v3, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-virtual {v3}, Landroid/util/Xml$Encoding;->name()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    return-void

    .line 330
    :cond_7c
    iput-boolean v4, p0, LRp;->d:Z

    :cond_7e
    move-object v2, p2

    goto :goto_6a
.end method

.method private a(I)Z
    .registers 4
    .parameter

    .prologue
    .line 259
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method static synthetic a(LRp;)Z
    .registers 2
    .parameter

    .prologue
    .line 106
    iget-boolean v0, p0, LRp;->c:Z

    return v0
.end method

.method static synthetic a(LRp;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 106
    iput-boolean p1, p0, LRp;->b:Z

    return p1
.end method

.method static synthetic b(LRp;)I
    .registers 2
    .parameter

    .prologue
    .line 106
    iget v0, p0, LRp;->a:I

    return v0
.end method

.method private b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 211
    iget-object v0, p0, LRp;->a:Landroid/webkit/WebView;

    iget-object v1, p0, LRp;->a:LRo;

    invoke-static {v1}, LRo;->a(LRo;)I

    move-result v1

    iget-object v2, p0, LRp;->a:LRo;

    invoke-static {v2}, LRo;->b(LRo;)I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/webkit/WebView;->layout(IIII)V

    .line 212
    return-void
.end method

.method static synthetic b(LRp;)V
    .registers 1
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, LRp;->a()V

    return-void
.end method

.method static synthetic b(LRp;)Z
    .registers 2
    .parameter

    .prologue
    .line 106
    iget-boolean v0, p0, LRp;->b:Z

    return v0
.end method

.method static synthetic b(LRp;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 106
    iput-boolean p1, p0, LRp;->c:Z

    return p1
.end method

.method private c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 215
    iput v2, p0, LRp;->c:I

    .line 217
    invoke-direct {p0}, LRp;->a()I

    move-result v0

    .line 218
    const/4 v1, -0x1

    if-ne v0, v1, :cond_d

    .line 219
    iput-boolean v2, p0, LRp;->a:Z

    .line 224
    :goto_c
    return-void

    .line 223
    :cond_d
    invoke-direct {p0, v0}, LRp;->a(I)V

    goto :goto_c
.end method

.method static synthetic c(LRp;)V
    .registers 1
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, LRp;->b()V

    return-void
.end method

.method static synthetic c(LRp;)Z
    .registers 2
    .parameter

    .prologue
    .line 106
    iget-boolean v0, p0, LRp;->d:Z

    return v0
.end method

.method private d()V
    .registers 6

    .prologue
    .line 339
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRp;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " content ready: slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRp;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)I

    move-result v0

    .line 342
    iget-object v1, p0, LRp;->a:LRo;

    invoke-static {v1}, LRo;->b(LRo;)I

    move-result v1

    .line 343
    iget-object v2, p0, LRp;->a:LRo;

    invoke-static {v2}, LRo;->a(LRo;)I

    move-result v2

    iget-object v3, p0, LRp;->a:LRo;

    invoke-static {v3}, LRo;->b(LRo;)I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 344
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 345
    iget-object v4, p0, LRp;->a:Landroid/webkit/WebView;

    invoke-virtual {v4, v3}, Landroid/webkit/WebView;->draw(Landroid/graphics/Canvas;)V

    .line 347
    iget v3, p0, LRp;->c:I

    const/16 v4, 0xa

    if-ge v3, v4, :cond_97

    .line 354
    add-int/lit8 v0, v0, -0xa

    add-int/lit8 v1, v1, -0xa

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_97

    .line 355
    iget v0, p0, LRp;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LRp;->c:I

    .line 356
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get a thumbnail. Retrying: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, LRp;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 358
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LRx;

    invoke-direct {v1, p0}, LRx;-><init>(LRp;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 373
    :goto_96
    return-void

    .line 368
    :cond_97
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Ljava/util/SortedMap;

    move-result-object v0

    iget v1, p0, LRp;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    iget-object v0, p0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)LRl;

    move-result-object v0

    iget v1, p0, LRp;->b:I

    invoke-interface {v0, v1}, LRl;->d(I)V

    .line 371
    invoke-direct {p0}, LRp;->e()V

    .line 372
    invoke-direct {p0}, LRp;->c()V

    goto :goto_96
.end method

.method private e()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 376
    iput-boolean v0, p0, LRp;->d:Z

    .line 377
    iput-boolean v0, p0, LRp;->b:Z

    .line 378
    return-void
.end method
