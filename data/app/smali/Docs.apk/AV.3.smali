.class public LAV;
.super LdX;
.source "DownloadImageTask.java"


# static fields
.field private static final a:Landroid/os/Handler;


# instance fields
.field private final a:LNj;

.field private final a:LZS;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LBd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, LAV;->a:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LBd;LNj;Ljava/lang/String;LZS;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0}, LdX;-><init>()V

    .line 88
    iput-object p1, p0, LAV;->a:Ljava/lang/String;

    .line 89
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LAV;->a:Ljava/lang/ref/WeakReference;

    .line 90
    iput-object p3, p0, LAV;->a:LNj;

    .line 91
    iput-object p4, p0, LAV;->b:Ljava/lang/String;

    .line 92
    iput-object p5, p0, LAV;->a:LZS;

    .line 93
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;LBd;LNj;Ljava/lang/String;LZS;LAW;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct/range {p0 .. p5}, LAV;-><init>(Ljava/lang/String;LBd;LNj;Ljava/lang/String;LZS;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 97
    .line 100
    :try_start_1
    const-string v0, "DownloadImageTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Started loading image "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v0, p0, LAV;->a:LNj;

    iget-object v2, p0, LAV;->b:Ljava/lang/String;

    iget-object v3, p0, LAV;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v3

    const/4 v4, 0x5

    const/4 v5, 0x1

    invoke-static {v0, v2, v3, v4, v5}, LNu;->a(LNj;Ljava/lang/String;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 103
    iget-object v2, p0, LAV;->a:LNj;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v2, v0}, LNj;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_266
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_34} :catch_93
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_34} :catch_e0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_34} :catch_12e
    .catch LNt; {:try_start_1 .. :try_end_34} :catch_17c
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_34} :catch_1ca
    .catch LNv; {:try_start_1 .. :try_end_34} :catch_218

    move-result-object v2

    .line 105
    :try_start_35
    iget-object v0, p0, LAV;->a:LZS;

    invoke-interface {v0, v2}, LZS;->a(Ljava/io/InputStream;)[B
    :try_end_3a
    .catchall {:try_start_35 .. :try_end_3a} :catchall_2a7
    .catch Ljava/net/MalformedURLException; {:try_start_35 .. :try_end_3a} :catch_2d6
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_3a} :catch_2cd
    .catch Landroid/accounts/AuthenticatorException; {:try_start_35 .. :try_end_3a} :catch_2c4
    .catch LNt; {:try_start_35 .. :try_end_3a} :catch_2bb
    .catch Ljava/net/URISyntaxException; {:try_start_35 .. :try_end_3a} :catch_2b2
    .catch LNv; {:try_start_35 .. :try_end_3a} :catch_2a9

    move-result-object v0

    .line 106
    :try_start_3b
    const-string v1, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loaded image "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_55
    .catchall {:try_start_3b .. :try_end_55} :catchall_2a7
    .catch Ljava/net/MalformedURLException; {:try_start_3b .. :try_end_55} :catch_2dc
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_55} :catch_2d3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3b .. :try_end_55} :catch_2ca
    .catch LNt; {:try_start_3b .. :try_end_55} :catch_2c1
    .catch Ljava/net/URISyntaxException; {:try_start_3b .. :try_end_55} :catch_2b8
    .catch LNv; {:try_start_3b .. :try_end_55} :catch_2af

    .line 120
    if-eqz v2, :cond_5a

    .line 122
    :try_start_57
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5a
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_5a} :catch_75

    .line 127
    :cond_5a
    :goto_5a
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_2df

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    move-object v1, v0

    .line 132
    :goto_64
    iget-object v0, p0, LAV;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBd;

    .line 133
    if-eqz v0, :cond_74

    invoke-virtual {v0}, LBd;->a()Z

    move-result v0

    if-nez v0, :cond_295

    .line 143
    :cond_74
    :goto_74
    return-void

    .line 123
    :catch_75
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5a

    .line 107
    :catch_93
    move-exception v0

    move-object v2, v1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 108
    :goto_98
    :try_start_98
    const-string v3, "DownloadImageTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Marlformed image URL "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b2
    .catchall {:try_start_98 .. :try_end_b2} :catchall_2a7

    .line 120
    if-eqz v2, :cond_b7

    .line 122
    :try_start_b4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_b7
    .catch Ljava/io/IOException; {:try_start_b4 .. :try_end_b7} :catch_c2

    .line 127
    :cond_b7
    :goto_b7
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_2df

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    move-object v1, v0

    goto :goto_64

    .line 123
    :catch_c2
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b7

    .line 109
    :catch_e0
    move-exception v0

    move-object v2, v1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 110
    :goto_e5
    :try_start_e5
    const-string v3, "DownloadImageTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not retrieve image "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ff
    .catchall {:try_start_e5 .. :try_end_ff} :catchall_2a7

    .line 120
    if-eqz v2, :cond_104

    .line 122
    :try_start_101
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_104
    .catch Ljava/io/IOException; {:try_start_101 .. :try_end_104} :catch_110

    .line 127
    :cond_104
    :goto_104
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_2df

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    move-object v1, v0

    goto/16 :goto_64

    .line 123
    :catch_110
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_104

    .line 111
    :catch_12e
    move-exception v0

    move-object v2, v1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 112
    :goto_133
    :try_start_133
    const-string v3, "DownloadImageTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not retrieve image "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_14d
    .catchall {:try_start_133 .. :try_end_14d} :catchall_2a7

    .line 120
    if-eqz v2, :cond_152

    .line 122
    :try_start_14f
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_152
    .catch Ljava/io/IOException; {:try_start_14f .. :try_end_152} :catch_15e

    .line 127
    :cond_152
    :goto_152
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_2df

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    move-object v1, v0

    goto/16 :goto_64

    .line 123
    :catch_15e
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_152

    .line 113
    :catch_17c
    move-exception v0

    move-object v2, v1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 114
    :goto_181
    :try_start_181
    const-string v3, "DownloadImageTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not retrieve image "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_19b
    .catchall {:try_start_181 .. :try_end_19b} :catchall_2a7

    .line 120
    if-eqz v2, :cond_1a0

    .line 122
    :try_start_19d
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1a0
    .catch Ljava/io/IOException; {:try_start_19d .. :try_end_1a0} :catch_1ac

    .line 127
    :cond_1a0
    :goto_1a0
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_2df

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    move-object v1, v0

    goto/16 :goto_64

    .line 123
    :catch_1ac
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1a0

    .line 115
    :catch_1ca
    move-exception v0

    move-object v2, v1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 116
    :goto_1cf
    :try_start_1cf
    const-string v3, "DownloadImageTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not retrieve image "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1e9
    .catchall {:try_start_1cf .. :try_end_1e9} :catchall_2a7

    .line 120
    if-eqz v2, :cond_1ee

    .line 122
    :try_start_1eb
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1ee
    .catch Ljava/io/IOException; {:try_start_1eb .. :try_end_1ee} :catch_1fa

    .line 127
    :cond_1ee
    :goto_1ee
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_2df

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    move-object v1, v0

    goto/16 :goto_64

    .line 123
    :catch_1fa
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1ee

    .line 117
    :catch_218
    move-exception v0

    move-object v2, v1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 118
    :goto_21d
    :try_start_21d
    const-string v3, "DownloadImageTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not retrieve image "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LAV;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_237
    .catchall {:try_start_21d .. :try_end_237} :catchall_2a7

    .line 120
    if-eqz v2, :cond_23c

    .line 122
    :try_start_239
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_23c
    .catch Ljava/io/IOException; {:try_start_239 .. :try_end_23c} :catch_248

    .line 127
    :cond_23c
    :goto_23c
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_2df

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    move-object v1, v0

    goto/16 :goto_64

    .line 123
    :catch_248
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_23c

    .line 120
    :catchall_266
    move-exception v0

    move-object v2, v1

    :goto_268
    if-eqz v2, :cond_26d

    .line 122
    :try_start_26a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_26d
    .catch Ljava/io/IOException; {:try_start_26a .. :try_end_26d} :catch_277

    .line 127
    :cond_26d
    :goto_26d
    iget-object v1, p0, LAV;->a:LNj;

    if-eqz v1, :cond_276

    .line 128
    iget-object v1, p0, LAV;->a:LNj;

    invoke-interface {v1}, LNj;->b()V

    :cond_276
    throw v0

    .line 123
    :catch_277
    move-exception v1

    .line 124
    const-string v2, "DownloadImageTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_26d

    .line 140
    :cond_295
    if-eqz v1, :cond_74

    .line 141
    sget-object v0, LAV;->a:Landroid/os/Handler;

    new-instance v2, LBi;

    iget-object v3, p0, LAV;->a:Ljava/lang/String;

    iget-object v4, p0, LAV;->a:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3, v4, v1}, LBi;-><init>(Ljava/lang/String;Ljava/lang/ref/WeakReference;[B)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_74

    .line 120
    :catchall_2a7
    move-exception v0

    goto :goto_268

    .line 117
    :catch_2a9
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_21d

    :catch_2af
    move-exception v1

    goto/16 :goto_21d

    .line 115
    :catch_2b2
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_1cf

    :catch_2b8
    move-exception v1

    goto/16 :goto_1cf

    .line 113
    :catch_2bb
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_181

    :catch_2c1
    move-exception v1

    goto/16 :goto_181

    .line 111
    :catch_2c4
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_133

    :catch_2ca
    move-exception v1

    goto/16 :goto_133

    .line 109
    :catch_2cd
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_e5

    :catch_2d3
    move-exception v1

    goto/16 :goto_e5

    .line 107
    :catch_2d6
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_98

    :catch_2dc
    move-exception v1

    goto/16 :goto_98

    :cond_2df
    move-object v1, v0

    goto/16 :goto_64
.end method
