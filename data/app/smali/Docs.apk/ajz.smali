.class public abstract Lajz;
.super LajA;
.source "ImmutableSortedSet.java"

# interfaces
.implements LalU;
.implements Ljava/util/SortedSet;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LajA",
        "<TE;>;",
        "LalU",
        "<TE;>;",
        "Ljava/util/SortedSet",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Lajz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajz",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final transient a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 92
    invoke-static {}, Lalw;->b()Lalw;

    move-result-object v0

    sput-object v0, Lajz;->b:Ljava/util/Comparator;

    .line 95
    new-instance v0, LaiU;

    sget-object v1, Lajz;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, LaiU;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lajz;->a:Lajz;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 566
    invoke-direct {p0}, LajA;-><init>()V

    .line 567
    iput-object p1, p0, Lajz;->a:Ljava/util/Comparator;

    .line 568
    return-void
.end method

.method static a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    .line 560
    .line 561
    invoke-interface {p0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static a()Lajz;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 100
    sget-object v0, Lajz;->a:Lajz;

    return-object v0
.end method

.method static a(Ljava/util/Comparator;)Lajz;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lajz;->b:Ljava/util/Comparator;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 106
    invoke-static {}, Lajz;->a()Lajz;

    move-result-object v0

    .line 108
    :goto_c
    return-object v0

    :cond_d
    new-instance v0, LaiU;

    invoke-direct {v0, p0}, LaiU;-><init>(Ljava/util/Comparator;)V

    goto :goto_c
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)I
.end method

.method a(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 551
    iget-object v0, p0, Lajz;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, p2}, Lajz;->a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;)Lajz;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 598
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lajz;->c(Ljava/lang/Object;Z)Lajz;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lajz;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;TE;)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 620
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lajz;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lajz;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/lang/Object;Z)Lajz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation
.end method

.method abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lajz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract a()Laml;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laml",
            "<TE;>;"
        }
    .end annotation
.end method

.method public b(Ljava/lang/Object;)Lajz;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 644
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lajz;->d(Ljava/lang/Object;Z)Lajz;

    move-result-object v0

    return-object v0
.end method

.method abstract b(Ljava/lang/Object;Z)Lajz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation
.end method

.method b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lajz;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 625
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 626
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    iget-object v0, p0, Lajz;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_17

    const/4 v0, 0x1

    :goto_f
    invoke-static {v0}, Lagu;->a(Z)V

    .line 628
    invoke-virtual {p0, p1, p2, p3, p4}, Lajz;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lajz;

    move-result-object v0

    return-object v0

    .line 627
    :cond_17
    const/4 v0, 0x0

    goto :goto_f
.end method

.method c(Ljava/lang/Object;Z)Lajz;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 602
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lajz;->a(Ljava/lang/Object;Z)Lajz;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 579
    iget-object v0, p0, Lajz;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method d(Ljava/lang/Object;Z)Lajz;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lajz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 648
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lajz;->b(Ljava/lang/Object;Z)Lajz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lajz;->a(Ljava/lang/Object;)Lajz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lajz;->a()Laml;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0, p1, p2}, Lajz;->a(Ljava/lang/Object;Ljava/lang/Object;)Lajz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lajz;->b(Ljava/lang/Object;)Lajz;

    move-result-object v0

    return-object v0
.end method
