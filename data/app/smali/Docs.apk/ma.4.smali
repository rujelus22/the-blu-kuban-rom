.class public interface abstract Lma;
.super Ljava/lang/Object;
.source "DiscussionSession.java"


# virtual methods
.method public abstract a()Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LlV",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/net/Uri;)LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LlV",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lmz;)LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lmz;Ljava/lang/String;)LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            "Ljava/lang/String;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()Z
.end method

.method public abstract a(Ljava/lang/Runnable;)Z
.end method

.method public abstract b()LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LlV",
            "<",
            "Lmx;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lmz;)LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lmz;Ljava/lang/String;)LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            "Ljava/lang/String;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Runnable;)Z
.end method

.method public abstract c(Lmz;)LlV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            ")",
            "LlV",
            "<*>;"
        }
    .end annotation
.end method
