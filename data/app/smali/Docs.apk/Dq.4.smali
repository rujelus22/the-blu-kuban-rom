.class LDq;
.super Ljava/lang/Object;
.source "PresentedEditable.java"

# interfaces
.implements LDt;


# instance fields
.field final synthetic a:LDp;

.field final synthetic a:Landroid/text/TextWatcher;


# direct methods
.method constructor <init>(LDp;Landroid/text/TextWatcher;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 338
    iput-object p1, p0, LDq;->a:LDp;

    iput-object p2, p0, LDq;->a:Landroid/text/TextWatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 354
    iget-object v0, p0, LDq;->a:Landroid/text/TextWatcher;

    iget-object v1, p0, LDq;->a:LDp;

    invoke-interface {v0, v1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 355
    return-void
.end method

.method public a(III)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 343
    iget-object v0, p0, LDq;->a:LDp;

    invoke-static {v0}, LDp;->a(LDp;)LDG;

    move-result-object v0

    invoke-virtual {v0}, LDG;->b()I

    move-result v0

    .line 344
    iget-object v1, p0, LDq;->a:Landroid/text/TextWatcher;

    iget-object v2, p0, LDq;->a:LDp;

    sub-int v0, p1, v0

    invoke-interface {v1, v2, v0, p2, p3}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 345
    return-void
.end method

.method public b(III)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 349
    iget-object v0, p0, LDq;->a:Landroid/text/TextWatcher;

    iget-object v1, p0, LDq;->a:LDp;

    iget-object v2, p0, LDq;->a:LDp;

    invoke-static {v2}, LDp;->a(LDp;)LDG;

    move-result-object v2

    invoke-virtual {v2}, LDG;->b()I

    move-result v2

    sub-int v2, p1, v2

    invoke-interface {v0, v1, v2, p2, p3}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 350
    return-void
.end method
