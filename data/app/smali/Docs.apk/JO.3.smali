.class public final LJO;
.super Ljava/lang/Object;
.source "EvaluableOffsetList.java"

# interfaces
.implements LKg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LKg",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LJQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LJQ",
            "<-TT;TV;>;"
        }
    .end annotation
.end field

.field private final a:LJS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LJS",
            "<TT;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 475
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LJO;-><init>(LJQ;)V

    .line 476
    return-void
.end method

.method public constructor <init>(LJQ;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LJQ",
            "<-TT;TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    new-instance v0, LJS;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v1, v2}, LJS;-><init>(Ljava/lang/Object;ILJP;)V

    iput-object v0, p0, LJO;->a:LJS;

    .line 483
    iget-object v0, p0, LJO;->a:LJS;

    iget-object v1, p0, LJO;->a:LJS;

    invoke-static {v0, v1}, LJS;->a(LJS;LJS;)LJS;

    .line 484
    iget-object v0, p0, LJO;->a:LJS;

    iget-object v1, p0, LJO;->a:LJS;

    invoke-static {v0, v1}, LJS;->b(LJS;LJS;)LJS;

    .line 485
    iput-object p1, p0, LJO;->a:LJQ;

    .line 486
    return-void
.end method

.method static synthetic a(LJS;)I
    .registers 2
    .parameter

    .prologue
    .line 18
    invoke-static {p0}, LJO;->b(LJS;)I

    move-result v0

    return v0
.end method

.method static synthetic a(LJO;)LJS;
    .registers 2
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, LJO;->a:LJS;

    return-object v0
.end method

.method private static b(LJS;)I
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LJS",
            "<TT;TV;>;)I"
        }
    .end annotation

    .prologue
    .line 585
    if-nez p0, :cond_4

    const/4 v0, -0x1

    :goto_3
    return v0

    :cond_4
    invoke-static {p0}, LJS;->c(LJS;)I

    move-result v0

    goto :goto_3
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 518
    iget-object v0, p0, LJO;->a:LJS;

    invoke-static {v0}, LJS;->a(LJS;)I

    move-result v0

    return v0
.end method

.method public a()LJR;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LJR",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 490
    iget-object v0, p0, LJO;->a:LJS;

    invoke-static {v0}, LJS;->a(LJS;)LJS;

    move-result-object v0

    return-object v0
.end method

.method public a(ILKi;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(I",
            "LKi",
            "<TT;TR;>;)TR;"
        }
    .end annotation

    .prologue
    .line 500
    iget-object v0, p0, LJO;->a:LJS;

    .line 501
    :goto_2
    if-eqz v0, :cond_2b

    .line 502
    invoke-static {v0}, LJS;->a(LJS;)I

    move-result v1

    if-ge p1, v1, :cond_f

    .line 503
    invoke-static {v0}, LJS;->b(LJS;)LJS;

    move-result-object v0

    goto :goto_2

    .line 505
    :cond_f
    invoke-static {v0}, LJS;->a(LJS;)I

    move-result v1

    sub-int v1, p1, v1

    .line 506
    invoke-static {v0}, LJS;->b(LJS;)I

    move-result v2

    if-ge v1, v2, :cond_20

    .line 507
    invoke-interface {p2, v0, v1}, LKi;->a(LKh;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 509
    :cond_20
    invoke-static {v0}, LJS;->b(LJS;)I

    move-result v2

    sub-int p1, v1, v2

    .line 510
    invoke-static {v0}, LJS;->c(LJS;)LJS;

    move-result-object v0

    goto :goto_2

    .line 513
    :cond_2b
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid offest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LJO;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()LJR;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LJR",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 495
    iget-object v0, p0, LJO;->a:LJS;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 523
    new-instance v0, LJP;

    invoke-direct {v0, p0}, LJP;-><init>(LJO;)V

    return-object v0
.end method
