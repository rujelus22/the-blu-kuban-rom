.class abstract Laqq;
.super Ljava/lang/Object;
.source "ProviderInternalFactory.java"

# interfaces
.implements LapY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LapY",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final a:Ljava/lang/Object;

.field private final a:Z


# direct methods
.method constructor <init>(Ljava/lang/Object;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "source"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laqq;->a:Ljava/lang/Object;

    .line 40
    iput-boolean p2, p0, Laqq;->a:Z

    .line 41
    return-void
.end method


# virtual methods
.method protected a(LatG;Lapu;LapX;Larg;ZLaqz;)Ljava/lang/Object;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LatG",
            "<+TT;>;",
            "Lapu;",
            "LapX;",
            "Larg",
            "<*>;Z",
            "Laqz",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p4}, Larg;->a()Laop;

    move-result-object v0

    invoke-virtual {v0}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    .line 48
    invoke-virtual {p3, p0}, LapX;->a(Ljava/lang/Object;)Lapk;

    move-result-object v5

    .line 51
    invoke-virtual {v5}, Lapk;->a()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 52
    iget-boolean v1, p0, Laqq;->a:Z

    if-nez v1, :cond_23

    .line 53
    invoke-virtual {p2, v0}, Lapu;->g(Ljava/lang/Class;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 57
    :cond_23
    invoke-virtual {v5, p2, v0}, Lapk;->a(Lapu;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 76
    :goto_27
    return-object v0

    .line 63
    :cond_28
    invoke-virtual {v5}, Lapk;->b()V

    .line 65
    :try_start_2b
    invoke-virtual {p6}, Laqz;->a()Z

    move-result v0

    if-nez v0, :cond_3c

    .line 66
    invoke-virtual {p0, p1, p2, p4, v5}, Laqq;->a(LatG;Lapu;Larg;Lapk;)Ljava/lang/Object;
    :try_end_34
    .catchall {:try_start_2b .. :try_end_34} :catchall_50

    move-result-object v0

    .line 75
    invoke-virtual {v5}, Lapk;->a()V

    .line 76
    invoke-virtual {v5}, Lapk;->c()V

    goto :goto_27

    .line 68
    :cond_3c
    :try_start_3c
    new-instance v0, Laqr;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Laqr;-><init>(Laqq;LatG;Lapu;Larg;Lapk;)V

    invoke-virtual {p6, p2, p3, v0}, Laqz;->a(Lapu;LapX;LaqB;)Ljava/lang/Object;
    :try_end_48
    .catchall {:try_start_3c .. :try_end_48} :catchall_50

    move-result-object v0

    .line 75
    invoke-virtual {v5}, Lapk;->a()V

    .line 76
    invoke-virtual {v5}, Lapk;->c()V

    goto :goto_27

    .line 75
    :catchall_50
    move-exception v0

    invoke-virtual {v5}, Lapk;->a()V

    .line 76
    invoke-virtual {v5}, Lapk;->c()V

    throw v0
.end method

.method protected a(LatG;Lapu;Larg;Lapk;)Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LatG",
            "<+TT;>;",
            "Lapu;",
            "Larg",
            "<*>;",
            "Lapk",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 86
    invoke-interface {p1}, LatG;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laqq;->a:Ljava/lang/Object;

    invoke-virtual {p2, v0, v1, p3}, Lapu;->a(Ljava/lang/Object;Ljava/lang/Object;Larg;)Ljava/lang/Object;

    move-result-object v0

    .line 87
    invoke-virtual {p4, v0}, Lapk;->b(Ljava/lang/Object;)V

    .line 88
    return-object v0
.end method
