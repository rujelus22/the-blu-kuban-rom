.class public Lll;
.super Ljava/lang/Object;
.source "AppliedOperation.java"


# instance fields
.field protected final a:Ljava/lang/String;

.field private final a:LlB;

.field private final b:LlB;


# direct methods
.method public constructor <init>(LlB;LlB;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lll;->a:LlB;

    .line 36
    iput-object p2, p0, Lll;->b:LlB;

    .line 37
    iput-object p3, p0, Lll;->a:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static a(LkB;Llf;Lorg/json/JSONObject;)Lll;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    const-string v0, "Forward"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {p0, p1, v0}, LlN;->a(LkB;Llf;Lorg/json/JSONObject;)LlB;

    move-result-object v0

    .line 81
    const-string v1, "Reverse"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {p0, p1, v1}, LlN;->a(LkB;Llf;Lorg/json/JSONObject;)LlB;

    move-result-object v1

    .line 83
    new-instance v2, Lll;

    const-string v3, "eTag"

    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, Lll;-><init>(LlB;LlB;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public a()LlB;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lll;->a:LlB;

    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .registers 4

    .prologue
    .line 87
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 88
    const-string v1, "eTag"

    iget-object v2, p0, Lll;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 89
    const-string v1, "Forward"

    invoke-virtual {p0}, Lll;->a()LlB;

    move-result-object v2

    invoke-interface {v2}, LlB;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 90
    const-string v1, "Reverse"

    invoke-virtual {p0}, Lll;->b()LlB;

    move-result-object v2

    invoke-interface {v2}, LlB;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 91
    return-object v0
.end method

.method public b()LlB;
    .registers 2

    .prologue
    .line 44
    iget-object v0, p0, Lll;->b:LlB;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 96
    const-string v0, "AppliedOperation[%s, undo=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lll;->a:LlB;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lll;->b:LlB;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
