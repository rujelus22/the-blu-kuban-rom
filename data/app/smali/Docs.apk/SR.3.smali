.class public LSR;
.super Ljava/lang/Object;
.source "OcrEvaluator.java"


# instance fields
.field private final a:LaaD;

.field private final a:Landroid/app/Activity;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;LaaD;Laoz;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LaaD;",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, LSR;->a:Landroid/app/Activity;

    .line 40
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaD;

    iput-object v0, p0, LSR;->a:LaaD;

    .line 41
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p0, LSR;->a:Laoz;

    .line 42
    return-void
.end method

.method static synthetic a(LSR;)LaaD;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, LSR;->a:LaaD;

    return-object v0
.end method

.method static synthetic a(LSR;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, LSR;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(LSR;)Laoz;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, LSR;->a:Laoz;

    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    .line 74
    iget-object v0, p0, LSR;->a:Landroid/app/Activity;

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 75
    sget v1, Len;->camera_ocr_blur_warning:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Len;->camera_ocr_blur_title:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x108008a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Len;->camera_ocr_warning_continue:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Len;->camera_ocr_warning_retake:I

    new-instance v3, LSU;

    invoke-direct {v3, p0}, LSU;-><init>(LSR;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 95
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 97
    return-void
.end method

.method static synthetic a(LSR;)V
    .registers 1
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, LSR;->a()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .registers 6
    .parameter

    .prologue
    .line 45
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v0, p0, LSR;->a:Landroid/app/Activity;

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 47
    iget-object v0, p0, LSR;->a:Landroid/app/Activity;

    sget v2, Len;->camera_ocr_evaluating_message:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 49
    const/4 v2, -0x2

    iget-object v0, p0, LSR;->a:Landroid/app/Activity;

    sget v3, Len;->camera_ocr_evaluating_skip_button:I

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 52
    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 56
    new-instance v2, LSS;

    invoke-direct {v2, p0, p1, v0, v1}, LSS;-><init>(LSR;Landroid/net/Uri;Landroid/os/Handler;Landroid/app/ProgressDialog;)V

    invoke-virtual {v2}, LSS;->start()V

    .line 71
    return-void
.end method
