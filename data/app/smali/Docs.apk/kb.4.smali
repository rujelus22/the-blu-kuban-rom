.class public Lkb;
.super Ljava/lang/Object;
.source "TabletDocListControllerImpl.java"

# interfaces
.implements LjY;


# instance fields
.field private final a:LME;

.field private final a:LMf;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LeQ;

.field private final a:LiA;

.field private final a:LiG;

.field private final a:LiX;

.field private final a:Liv;

.field private final a:LjZ;

.field private final a:Ljc;

.field private final a:Ljl;

.field private final a:Ljo;

.field private final a:Lka;

.field private final a:Llf;


# direct methods
.method public constructor <init>(Llf;LME;Liv;LiX;Ljl;Ljc;LjZ;LMf;LiG;Ljo;LiA;LeQ;Laoz;Lka;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Llf;",
            "LME;",
            "Liv;",
            "LiX;",
            "Ljl;",
            "Ljc;",
            "LjZ;",
            "LMf;",
            "LiG;",
            "Ljo;",
            "LiA;",
            "LeQ;",
            "Laoz",
            "<",
            "Lfb;",
            ">;",
            "Lka;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lkb;->a:Llf;

    .line 117
    iput-object p2, p0, Lkb;->a:LME;

    .line 118
    iput-object p3, p0, Lkb;->a:Liv;

    .line 119
    iput-object p4, p0, Lkb;->a:LiX;

    .line 120
    iput-object p5, p0, Lkb;->a:Ljl;

    .line 121
    iput-object p6, p0, Lkb;->a:Ljc;

    .line 122
    iput-object p7, p0, Lkb;->a:LjZ;

    .line 123
    iput-object p8, p0, Lkb;->a:LMf;

    .line 124
    iput-object p9, p0, Lkb;->a:LiG;

    .line 125
    iput-object p10, p0, Lkb;->a:Ljo;

    .line 126
    iput-object p11, p0, Lkb;->a:LiA;

    .line 127
    iput-object p12, p0, Lkb;->a:LeQ;

    .line 128
    iput-object p13, p0, Lkb;->a:Laoz;

    .line 129
    iput-object p14, p0, Lkb;->a:Lka;

    .line 130
    return-void
.end method

.method private a(Ljava/lang/String;)LiQ;
    .registers 6
    .parameter

    .prologue
    .line 768
    iget-object v0, p0, Lkb;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    .line 769
    new-instance v1, LiR;

    invoke-direct {v1}, LiR;-><init>()V

    .line 770
    iget-object v2, p0, Lkb;->a:LiG;

    iget-object v3, p0, Lkb;->a:Liv;

    invoke-interface {v3}, Liv;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v2

    invoke-virtual {v1, v2}, LiR;->a(LiE;)LiR;

    .line 772
    iget-object v2, p0, Lkb;->a:LiG;

    invoke-interface {v2, p1, v0}, LiG;->a(Ljava/lang/String;Ljava/lang/String;)LiE;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LiE;)LiR;

    .line 774
    iget-object v0, p0, Lkb;->a:LiG;

    invoke-interface {v0}, LiG;->a()LiE;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LiE;)LiR;

    .line 775
    invoke-virtual {v1}, LiR;->a()LiQ;

    move-result-object v0

    return-object v0
.end method

.method private a(LSF;Ljava/util/List;)Ljava/util/List;
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LSF;",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624
    iget-object v0, p0, Lkb;->a:LiG;

    iget-object v1, p0, Lkb;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LiG;->a(Ljava/lang/String;LSF;)LiE;

    move-result-object v1

    .line 626
    invoke-static {p2}, LajB;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    .line 627
    new-instance v2, LiR;

    invoke-direct {v2}, LiR;-><init>()V

    .line 628
    invoke-interface {v0}, LiQ;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1b
    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiE;

    .line 629
    instance-of v4, v0, Ljs;

    if-nez v4, :cond_1b

    .line 630
    invoke-virtual {v2, v0}, LiR;->a(LiE;)LiR;

    goto :goto_1b

    .line 634
    :cond_2f
    invoke-virtual {v2, v1}, LiR;->a(LiE;)LiR;

    .line 635
    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2}, LiR;->a()LiQ;

    move-result-object v1

    invoke-static {v0, v1}, Ljr;->a(Ljava/util/List;LiQ;)Ljava/util/List;

    move-result-object v0

    .line 637
    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    new-instance v0, LiR;

    invoke-direct {v0}, LiR;-><init>()V

    .line 562
    iget-object v1, p0, Lkb;->a:LiG;

    invoke-interface {v1, p1}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v1

    invoke-virtual {v0, v1}, LiR;->a(LiE;)LiR;

    .line 563
    iget-object v1, p0, Lkb;->a:LiG;

    invoke-direct {p0}, Lkb;->a()LmK;

    move-result-object v2

    invoke-interface {v1, v2, p1}, LiG;->b(LmK;Ljava/lang/String;)LiE;

    move-result-object v1

    invoke-virtual {v0, v1}, LiR;->a(LiE;)LiR;

    .line 565
    const/4 v1, 0x1

    new-array v1, v1, [LiQ;

    const/4 v2, 0x0

    invoke-virtual {v0}, LiR;->a()LiQ;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkb;LSF;Ljava/util/List;)Ljava/util/List;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lkb;->a(LSF;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a()LkB;
    .registers 3

    .prologue
    .line 703
    iget-object v0, p0, Lkb;->a:Llf;

    iget-object v1, p0, Lkb;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 704
    if-nez v0, :cond_14

    .line 705
    const-string v0, "Failed to load account"

    invoke-direct {p0, v0}, Lkb;->c(Ljava/lang/String;)V

    .line 706
    const/4 v0, 0x0

    .line 708
    :cond_14
    return-object v0
.end method

.method private a(Ljava/lang/String;)LkO;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 712
    iget-object v1, p0, Lkb;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 714
    invoke-direct {p0}, Lkb;->a()LkB;

    move-result-object v1

    .line 715
    if-nez v1, :cond_11

    .line 724
    :goto_10
    return-object v0

    .line 718
    :cond_11
    iget-object v2, p0, Lkb;->a:Llf;

    invoke-interface {v2, v1, p1}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v1

    .line 719
    if-nez v1, :cond_30

    .line 720
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load entry with resource ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lkb;->c(Ljava/lang/String;)V

    goto :goto_10

    :cond_30
    move-object v0, v1

    .line 724
    goto :goto_10
.end method

.method private a()LmK;
    .registers 2

    .prologue
    .line 759
    iget-object v0, p0, Lkb;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    invoke-virtual {v0}, Lfb;->a()LmK;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 370
    if-nez p1, :cond_9

    .line 374
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 378
    :cond_9
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_135

    .line 379
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 380
    const-string v2, "app_data"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    .line 381
    if-nez p1, :cond_24

    .line 382
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 386
    :cond_24
    :goto_24
    const-string v2, "accountName"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 387
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8c

    .line 388
    iget-object v3, p0, Lkb;->a:Liv;

    invoke-interface {v3, v2}, Liv;->a(Ljava/lang/String;)V

    .line 405
    :cond_35
    :goto_35
    iget-object v2, p0, Lkb;->a:LiX;

    invoke-interface {v2, p1}, LiX;->a(Landroid/os/Bundle;)V

    .line 409
    :try_start_3a
    iget-object v2, p0, Lkb;->a:Ljo;

    invoke-interface {v2, p1}, Ljo;->a(Landroid/os/Bundle;)Ljava/util/List;
    :try_end_3f
    .catch Ljp; {:try_start_3a .. :try_end_3f} :catch_c0

    move-result-object v2

    .line 416
    :goto_40
    if-nez p2, :cond_5a

    if-eqz v2, :cond_5a

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5a

    .line 417
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    invoke-interface {v0}, LiQ;->b()Ljava/lang/String;

    move-result-object v0

    .line 421
    :cond_5a
    const-string v3, "collectionResourceId"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 423
    if-eqz v0, :cond_cf

    .line 424
    if-eqz p2, :cond_cd

    :goto_64
    invoke-direct {p0, v0, v1}, Lkb;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 443
    :goto_67
    invoke-direct {p0}, Lkb;->c()Z

    move-result v0

    if-eqz v0, :cond_78

    .line 444
    iget-object v0, p0, Lkb;->a:LME;

    iget-object v1, p0, Lkb;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LME;->a(Ljava/lang/String;)V

    .line 447
    :cond_78
    if-eqz p2, :cond_8b

    invoke-direct {p0}, Lkb;->c()Z

    move-result v0

    if-eqz v0, :cond_8b

    const-string v0, "triggerSync"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 449
    invoke-direct {p0}, Lkb;->g()V

    .line 451
    :cond_8b
    return-void

    .line 390
    :cond_8c
    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2, p1}, Liv;->a(Landroid/os/Bundle;)V

    .line 392
    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 393
    iget-object v2, p0, Lkb;->a:LME;

    invoke-interface {v2}, LME;->a()Ljava/lang/String;

    move-result-object v2

    .line 394
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_af

    .line 395
    iget-object v3, p0, Lkb;->a:Liv;

    invoke-interface {v3, v2}, Liv;->a(Ljava/lang/String;)V

    goto :goto_35

    .line 397
    :cond_af
    iget-object v2, p0, Lkb;->a:LME;

    invoke-static {v2}, LMG;->b(LME;)Landroid/accounts/Account;

    move-result-object v2

    .line 398
    if-eqz v2, :cond_35

    .line 399
    iget-object v3, p0, Lkb;->a:Liv;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v2}, Liv;->a(Ljava/lang/String;)V

    goto/16 :goto_35

    .line 410
    :catch_c0
    move-exception v2

    .line 411
    const-string v3, "TabletDocListController"

    invoke-virtual {v2}, Ljp;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    goto/16 :goto_40

    :cond_cd
    move-object v1, v2

    .line 424
    goto :goto_64

    .line 425
    :cond_cf
    if-eqz v2, :cond_db

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_db

    .line 426
    invoke-direct {p0, v2}, Lkb;->c(Ljava/util/List;)V

    goto :goto_67

    .line 427
    :cond_db
    if-eqz p2, :cond_f0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f0

    .line 428
    invoke-direct {p0, v3}, Lkb;->a(Ljava/lang/String;)LiQ;

    move-result-object v0

    .line 429
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lkb;->c(Ljava/util/List;)V

    goto/16 :goto_67

    .line 431
    :cond_f0
    const-string v0, "mainFilter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LmK;

    .line 432
    if-nez v0, :cond_fe

    .line 433
    invoke-direct {p0}, Lkb;->a()LmK;

    move-result-object v0

    .line 436
    :cond_fe
    const/4 v1, 0x1

    new-array v1, v1, [LiQ;

    new-instance v2, LiR;

    invoke-direct {v2}, LiR;-><init>()V

    iget-object v3, p0, Lkb;->a:LiG;

    iget-object v4, p0, Lkb;->a:Liv;

    invoke-interface {v4}, Liv;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v3

    invoke-virtual {v2, v3}, LiR;->a(LiE;)LiR;

    move-result-object v2

    iget-object v3, p0, Lkb;->a:LiG;

    iget-object v4, p0, Lkb;->a:Liv;

    invoke-interface {v4}, Liv;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, LiG;->b(LmK;Ljava/lang/String;)LiE;

    move-result-object v0

    invoke-virtual {v2, v0}, LiR;->a(LiE;)LiR;

    move-result-object v0

    invoke-virtual {v0}, LiR;->a()LiQ;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lkb;->c(Ljava/util/List;)V

    goto/16 :goto_67

    :cond_135
    move-object v0, v1

    goto/16 :goto_24
.end method

.method private a(Ljava/lang/Exception;)V
    .registers 3
    .parameter

    .prologue
    .line 745
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkb;->c(Ljava/lang/String;)V

    .line 746
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 301
    iget-object v0, p0, Lkb;->a:LiX;

    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, LiX;->a(Ljava/util/Set;)V

    .line 302
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 588
    if-eqz p2, :cond_2a

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2a

    .line 597
    :goto_8
    const-wide/16 v0, -0x1

    invoke-static {p1, v0, v1}, LSF;->a(Ljava/lang/String;J)LSF;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lkb;->a(LSF;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 599
    invoke-direct {p0, v0}, Lkb;->c(Ljava/util/List;)V

    .line 601
    iget-object v1, p0, Lkb;->a:LMf;

    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, LMf;->a(Ljava/lang/String;Ljava/lang/String;)LamQ;

    move-result-object v1

    .line 603
    new-instance v2, Lkc;

    invoke-direct {v2, p0, v0}, Lkc;-><init>(Lkb;Ljava/util/List;)V

    invoke-static {v1, v2}, LamF;->a(LamQ;LamE;)V

    .line 617
    return-void

    .line 591
    :cond_2a
    new-instance v0, LiR;

    invoke-direct {v0}, LiR;-><init>()V

    .line 592
    iget-object v1, p0, Lkb;->a:LiG;

    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v1

    invoke-virtual {v0, v1}, LiR;->a(LiE;)LiR;

    .line 594
    const/4 v1, 0x1

    new-array v1, v1, [LiQ;

    const/4 v2, 0x0

    invoke-virtual {v0}, LiR;->a()LiQ;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    goto :goto_8
.end method

.method private a(Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 524
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 525
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 528
    const-string v2, "accountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const-string v2, "triggerSync"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 530
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 532
    iget-object v1, p0, Lkb;->a:LjZ;

    invoke-interface {v1, v0}, LjZ;->c(Landroid/content/Intent;)V

    .line 533
    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 458
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lkb;->a:Ljl;

    invoke-interface {v2}, Ljl;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-gt v0, v2, :cond_33

    const/4 v0, 0x1

    :goto_17
    invoke-static {v0}, Lagu;->a(Z)V

    .line 462
    iget-object v0, p0, Lkb;->a:Ljl;

    invoke-interface {v0}, Ljl;->a()Ljava/util/List;

    move-result-object v0

    .line 464
    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_35

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 489
    :goto_32
    return-void

    :cond_33
    move v0, v1

    .line 461
    goto :goto_17

    .line 469
    :cond_35
    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_59

    invoke-direct {p0}, Lkb;->c()Z

    move-result v2

    if-nez v2, :cond_59

    .line 471
    invoke-direct {p0, p1}, Lkb;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_55

    .line 472
    invoke-direct {p0, p2}, Lkb;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p2, v1}, Lkb;->a(Ljava/util/List;Ljava/lang/String;Z)V

    goto :goto_32

    .line 474
    :cond_55
    invoke-direct {p0, p1, p2, v1}, Lkb;->a(Ljava/util/List;Ljava/lang/String;Z)V

    goto :goto_32

    .line 479
    :cond_59
    invoke-direct {p0}, Lkb;->e()V

    .line 481
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_6a

    .line 482
    invoke-direct {p0, p1, p2}, Lkb;->b(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_32

    .line 483
    :cond_6a
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_78

    .line 484
    invoke-direct {p0, p1, p2, v1}, Lkb;->a(Ljava/util/List;Ljava/lang/String;Z)V

    goto :goto_32

    .line 486
    :cond_78
    invoke-direct {p0, p2}, Lkb;->b(Ljava/lang/String;)V

    .line 487
    invoke-direct {p0, p1}, Lkb;->c(Ljava/util/List;)V

    goto :goto_32
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 544
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 547
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 549
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 550
    const-string v2, "accountName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const-string v2, "triggerSync"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 552
    iget-object v2, p0, Lkb;->a:Ljo;

    invoke-interface {v2, v1, p1}, Ljo;->a(Landroid/os/Bundle;Ljava/util/List;)V

    .line 553
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 555
    iget-object v1, p0, Lkb;->a:LjZ;

    invoke-interface {v1, v0}, LjZ;->a(Landroid/content/Intent;)V

    .line 556
    return-void
.end method

.method static synthetic a(Lkb;Ljava/util/List;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lkb;->c(Ljava/util/List;)V

    return-void
.end method

.method private a()Z
    .registers 7

    .prologue
    .line 280
    iget-object v0, p0, Lkb;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 281
    iget-object v1, p0, Lkb;->a:Llf;

    invoke-direct {p0}, Lkb;->a()LkB;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v1

    .line 282
    if-nez v1, :cond_1e

    .line 283
    const/4 v0, 0x0

    .line 293
    :goto_1d
    return v0

    .line 286
    :cond_1e
    iget-object v1, p0, Lkb;->a:Ljl;

    invoke-interface {v1}, Ljl;->a()LiQ;

    move-result-object v1

    .line 287
    invoke-interface {v1}, LiQ;->a()Lnh;

    move-result-object v1

    sget-object v2, Lnk;->a:Lnk;

    new-instance v3, Lnh;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LPX;->n:LPX;

    invoke-virtual {v5}, LPX;->a()LPI;

    move-result-object v5

    invoke-virtual {v5}, LPI;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lnh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lnh;->a(Lnk;Lnh;)Lnh;

    move-result-object v0

    .line 289
    iget-object v1, p0, Lkb;->a:Llf;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Llf;->a(Lnh;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 290
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 291
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1d
.end method

.method private a(LiQ;)Z
    .registers 5
    .parameter

    .prologue
    .line 690
    invoke-interface {p1}, LiQ;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiE;

    .line 691
    instance-of v2, v0, LiW;

    if-eqz v2, :cond_4

    .line 692
    check-cast v0, LiW;

    .line 693
    invoke-virtual {v0}, LiW;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 694
    const/4 v0, 0x1

    .line 699
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private a(Ljava/util/List;)Z
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 732
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_22

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    invoke-interface {v0}, LiQ;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_22

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    invoke-interface {v0}, LiQ;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_22

    move v0, v1

    :goto_21
    return v0

    :cond_22
    move v0, v2

    goto :goto_21
.end method

.method private b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 569
    iget-object v0, p0, Lkb;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 571
    invoke-direct {p0}, Lkb;->c()Z

    move-result v0

    .line 572
    iget-object v1, p0, Lkb;->a:Liv;

    invoke-interface {v1, p1}, Liv;->a(Ljava/lang/String;)V

    .line 574
    if-eqz v0, :cond_1c

    .line 575
    iget-object v0, p0, Lkb;->a:LME;

    invoke-interface {v0, p1}, LME;->a(Ljava/lang/String;)V

    .line 578
    :cond_1c
    invoke-direct {p0}, Lkb;->g()V

    .line 580
    :cond_1f
    return-void
.end method

.method private b(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 454
    iget-object v0, p0, Lkb;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lkb;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 455
    return-void
.end method

.method private b(Ljava/util/List;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 500
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 501
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 504
    const-string v2, "accountName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    iget-object v2, p0, Lkb;->a:Ljo;

    invoke-interface {v2, v1, p1}, Ljo;->a(Landroid/os/Bundle;Ljava/util/List;)V

    .line 506
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 508
    iget-object v1, p0, Lkb;->a:LjZ;

    invoke-interface {v1, v0}, LjZ;->b(Landroid/content/Intent;)V

    .line 509
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 309
    iget-object v0, p0, Lkb;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 749
    const-string v0, "TabletDocListController"

    invoke-static {v0, p1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    invoke-direct {p0}, Lkb;->h()V

    .line 751
    return-void
.end method

.method private c(Ljava/lang/String;LfS;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 159
    invoke-direct {p0, p1}, Lkb;->a(Ljava/lang/String;)LkO;

    move-result-object v0

    .line 160
    if-nez v0, :cond_7

    .line 173
    :goto_6
    return-void

    .line 162
    :cond_7
    invoke-virtual {v0}, LkO;->o()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 163
    iget-object v0, p0, Lkb;->a:LeQ;

    if-eqz v0, :cond_1c

    .line 164
    iget-object v0, p0, Lkb;->a:LeQ;

    const-string v1, "tabletDoclist"

    const-string v2, "collections"

    const-string v3, "FromDoclist"

    invoke-virtual {v0, v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_1c
    invoke-direct {p0, p1}, Lkb;->a(Ljava/lang/String;)LiQ;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lkb;->a:Ljl;

    invoke-interface {v1}, Ljl;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1, v0}, Ljr;->a(Ljava/util/List;LiQ;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lkb;->b(Ljava/util/List;)V

    goto :goto_6

    .line 171
    :cond_2e
    iget-object v0, p0, Lkb;->a:LjZ;

    invoke-interface {v0, p1, p2}, LjZ;->b(Ljava/lang/String;LfS;)V

    goto :goto_6
.end method

.method private c(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 649
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    .line 650
    invoke-interface {v0}, LiQ;->a()V
    :try_end_13
    .catch LiF; {:try_start_0 .. :try_end_13} :catch_14

    goto :goto_4

    .line 652
    :catch_14
    move-exception v0

    .line 653
    invoke-direct {p0, v0}, Lkb;->a(Ljava/lang/Exception;)V

    .line 661
    :goto_18
    return-void

    .line 657
    :cond_19
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    .line 658
    iget-object v1, p0, Lkb;->a:Ljl;

    invoke-interface {v1, p1}, Ljl;->a(Ljava/util/List;)V

    .line 659
    iget-object v1, p0, Lkb;->a:Ljc;

    invoke-interface {v1, v0}, Ljc;->a(LiQ;)V

    .line 660
    invoke-direct {p0, p1}, Lkb;->d(Ljava/util/List;)V

    goto :goto_18
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 728
    iget-object v0, p0, Lkb;->a:Ljl;

    invoke-interface {v0}, Ljl;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lkb;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method private d(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 664
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 665
    invoke-static {p1}, LajX;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v6, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 666
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 667
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    .line 668
    invoke-interface {v0}, LiQ;->a()LmK;

    move-result-object v3

    if-eqz v3, :cond_36

    .line 685
    :cond_2c
    :goto_2c
    invoke-static {v1}, LajX;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 686
    iget-object v1, p0, Lkb;->a:LiA;

    invoke-interface {v1, v0}, LiA;->a(Ljava/util/List;)V

    .line 687
    return-void

    .line 673
    :cond_36
    invoke-interface {v0}, LiQ;->a()Ljava/lang/String;

    move-result-object v3

    .line 674
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-interface {p1, v6, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    .line 676
    new-instance v5, LiC;

    invoke-direct {v5, v3, v4}, LiC;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 678
    invoke-interface {v0}, LiQ;->b()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2c

    invoke-direct {p0, v0}, Lkb;->a(LiQ;)Z

    move-result v0

    if-eqz v0, :cond_1a

    goto :goto_2c
.end method

.method private e()V
    .registers 3

    .prologue
    .line 297
    iget-object v0, p0, Lkb;->a:LiX;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, LiX;->a(Ljava/util/Set;)V

    .line 298
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 305
    iget-object v0, p0, Lkb;->a:LiX;

    invoke-interface {v0}, LiX;->a()V

    .line 306
    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    .line 325
    const-string v0, "TabletDocListController"

    const-string v1, "in resync"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lkb;->a:Llf;

    iget-object v1, p0, Lkb;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 327
    iget-object v1, p0, Lkb;->a:LME;

    invoke-interface {v1, v0}, LME;->a(LkB;)V

    .line 328
    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 515
    iget-object v0, p0, Lkb;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lkb;->a(Ljava/lang/String;Z)V

    .line 516
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 266
    iget-object v0, p0, Lkb;->a:Ljc;

    invoke-interface {v0}, Ljc;->a()V

    .line 267
    iget-object v0, p0, Lkb;->a:Ljl;

    invoke-interface {v0}, Ljl;->a()V

    .line 269
    iget-object v0, p0, Lkb;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 270
    invoke-direct {p0}, Lkb;->a()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 271
    invoke-direct {p0}, Lkb;->f()V

    .line 276
    :cond_1f
    :goto_1f
    return-void

    .line 273
    :cond_20
    invoke-direct {p0}, Lkb;->e()V

    goto :goto_1f
.end method

.method public a(Landroid/content/Intent;)V
    .registers 5
    .parameter

    .prologue
    .line 238
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_9

    .line 257
    :cond_8
    :goto_8
    return-void

    .line 242
    :cond_9
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 245
    :try_start_d
    iget-object v1, p0, Lkb;->a:Ljo;

    invoke-interface {v1, v0}, Ljo;->a(Landroid/os/Bundle;)Ljava/util/List;
    :try_end_12
    .catch Ljp; {:try_start_d .. :try_end_12} :catch_29

    move-result-object v1

    .line 250
    const-string v2, "accountName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 253
    iget-object v0, p0, Lkb;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    .line 256
    :cond_25
    invoke-direct {p0, v1, v0}, Lkb;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_8

    .line 246
    :catch_29
    move-exception v0

    .line 247
    const-string v1, "TabletDocListController"

    invoke-virtual {v0}, Ljp;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method

.method public a(Landroid/os/Bundle;Landroid/content/Intent;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 135
    if-nez p1, :cond_f

    const/4 v0, 0x1

    .line 136
    :goto_3
    if-eqz v0, :cond_b

    if-eqz p2, :cond_b

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    :cond_b
    invoke-direct {p0, p1, v0}, Lkb;->a(Landroid/os/Bundle;Z)V

    .line 138
    return-void

    .line 135
    :cond_f
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public a(Ljava/lang/Iterable;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LmK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lkb;->a:Ljl;

    invoke-interface {v0}, Ljl;->a()LiQ;

    .line 197
    new-instance v1, LiR;

    invoke-direct {v1}, LiR;-><init>()V

    .line 198
    iget-object v0, p0, Lkb;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    .line 199
    iget-object v0, p0, Lkb;->a:LiG;

    invoke-interface {v0, v2}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LiE;)LiR;

    .line 201
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LmK;

    .line 202
    iget-object v4, p0, Lkb;->a:LiG;

    invoke-interface {v4, v0, v2}, LiG;->a(LmK;Ljava/lang/String;)LiE;

    move-result-object v0

    .line 203
    invoke-virtual {v1, v0}, LiR;->a(LiE;)LiR;

    goto :goto_1d

    .line 206
    :cond_33
    invoke-virtual {v1}, LiR;->a()LiQ;

    move-result-object v0

    .line 207
    iget-object v2, p0, Lkb;->a:Ljl;

    invoke-interface {v2}, Ljl;->a()LiQ;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 217
    :goto_43
    return-void

    .line 211
    :cond_44
    invoke-static {}, Laji;->a()Lajj;

    move-result-object v0

    iget-object v2, p0, Lkb;->a:Ljl;

    invoke-interface {v2}, Ljl;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lajj;->a(Ljava/lang/Iterable;)Lajj;

    move-result-object v0

    invoke-virtual {v1}, LiR;->a()LiQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    invoke-virtual {v0}, Lajj;->a()Laji;

    move-result-object v0

    .line 216
    invoke-direct {p0, v0}, Lkb;->b(Ljava/util/List;)V

    goto :goto_43
.end method

.method public a(Ljava/lang/String;LfS;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 142
    invoke-direct {p0}, Lkb;->b()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 143
    if-eqz p2, :cond_c

    sget-object v0, LfS;->a:LfS;

    if-ne p2, v0, :cond_10

    .line 144
    :cond_c
    invoke-direct {p0, p1}, Lkb;->a(Ljava/lang/String;)V

    .line 151
    :goto_f
    return-void

    .line 146
    :cond_10
    iget-object v0, p0, Lkb;->a:LjZ;

    invoke-interface {v0, p1, p2}, LjZ;->b(Ljava/lang/String;LfS;)V

    goto :goto_f

    .line 149
    :cond_16
    invoke-direct {p0, p1, p2}, Lkb;->c(Ljava/lang/String;LfS;)V

    goto :goto_f
.end method

.method public a(Ljava/util/List;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lkb;->a:LeQ;

    if-eqz v0, :cond_f

    .line 222
    iget-object v0, p0, Lkb;->a:LeQ;

    const-string v1, "tabletDoclist"

    const-string v2, "collections"

    const-string v3, "NotFromDoclist"

    invoke-virtual {v0, v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_f
    invoke-direct {p0, p1}, Lkb;->b(Ljava/util/List;)V

    .line 227
    return-void
.end method

.method public a(LmK;)V
    .registers 5
    .parameter

    .prologue
    .line 179
    invoke-virtual {p1}, LmK;->a()Ljava/lang/String;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_11

    iget-object v1, p0, Lkb;->a:LeQ;

    if-eqz v1, :cond_11

    .line 181
    iget-object v1, p0, Lkb;->a:LeQ;

    const-string v2, "tabletDoclist"

    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_11
    new-instance v0, LiR;

    invoke-direct {v0}, LiR;-><init>()V

    .line 186
    iget-object v1, p0, Lkb;->a:LiG;

    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v1

    invoke-virtual {v0, v1}, LiR;->a(LiE;)LiR;

    .line 188
    iget-object v1, p0, Lkb;->a:LiG;

    iget-object v2, p0, Lkb;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, LiG;->b(LmK;Ljava/lang/String;)LiE;

    move-result-object v1

    invoke-virtual {v0, v1}, LiR;->a(LiE;)LiR;

    .line 191
    const/4 v1, 0x1

    new-array v1, v1, [LiQ;

    const/4 v2, 0x0

    invoke-virtual {v0}, LiR;->a()LiQ;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lkb;->b(Ljava/util/List;)V

    .line 192
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 231
    invoke-direct {p0}, Lkb;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lkb;->a:LjZ;

    invoke-interface {v0}, LjZ;->e()Z

    move-result v0

    if-nez v0, :cond_11

    .line 232
    :cond_e
    invoke-direct {p0}, Lkb;->h()V

    .line 234
    :cond_11
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .registers 5
    .parameter

    .prologue
    .line 341
    iget-object v0, p0, Lkb;->a:LeQ;

    if-eqz v0, :cond_d

    .line 342
    iget-object v0, p0, Lkb;->a:LeQ;

    const-string v1, "tabletDoclist"

    const-string v2, "search"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_d
    iget-object v0, p0, Lkb;->a:LjZ;

    invoke-interface {v0, p1}, LjZ;->b(Landroid/content/Intent;)V

    .line 347
    return-void
.end method

.method public b(Ljava/lang/String;LfS;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Lkb;->c(Ljava/lang/String;LfS;)V

    .line 156
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 764
    iget-object v0, p0, Lkb;->a:Lka;

    invoke-interface {v0}, Lka;->p()V

    .line 765
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 332
    invoke-static {}, LZL;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 333
    invoke-direct {p0}, Lkb;->e()V

    .line 337
    :goto_9
    return-void

    .line 335
    :cond_a
    iget-object v0, p0, Lkb;->a:LjZ;

    invoke-interface {v0}, LjZ;->q()V

    goto :goto_9
.end method

.method public e(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 261
    invoke-direct {p0, p1}, Lkb;->a(Ljava/lang/String;)V

    .line 262
    return-void
.end method
