.class public LdB;
.super Laoe;
.source "ApplicationModule.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Laoe;-><init>()V

    .line 58
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, LdX;

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, LdB;->a([Ljava/lang/Class;)V

    .line 71
    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, Les;

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, LdB;->a([Ljava/lang/Class;)V

    .line 73
    const-class v0, LeW;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LeX;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 74
    const-class v0, Lo;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lo;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 76
    new-instance v0, LdC;

    invoke-direct {v0, p0}, LdC;-><init>(LdB;)V

    invoke-virtual {p0, v0}, LdB;->a(LaoL;)LaoM;

    move-result-object v0

    const-string v1, "StartingActivityOnLaunch"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/annotation/Annotation;)LaoQ;

    move-result-object v0

    const-class v1, LdF;

    invoke-interface {v0, v1}, LaoQ;->b(Ljava/lang/Class;)LaoR;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-interface {v0, v1}, LaoR;->a(Ljava/lang/Class;)V

    .line 80
    new-instance v0, LdD;

    invoke-direct {v0, p0}, LdD;-><init>(LdB;)V

    invoke-virtual {p0, v0}, LdB;->a(LaoL;)LaoM;

    move-result-object v0

    const-string v1, "DocListActivity"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/annotation/Annotation;)LaoQ;

    move-result-object v0

    const-class v1, LdE;

    invoke-interface {v0, v1}, LaoQ;->b(Ljava/lang/Class;)LaoR;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-interface {v0, v1}, LaoR;->a(Ljava/lang/Class;)V

    .line 84
    const-class v0, LoT;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    sget-object v1, LoT;->b:LoT;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Object;)V

    .line 86
    const-class v0, Lgb;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lgc;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 88
    const-class v0, [Lii;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, [Lii;

    invoke-static {v1}, LfR;->a(Ljava/lang/Class;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 91
    const-class v0, Lfe;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)V

    .line 92
    const-class v0, Lfb;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lfe;

    invoke-interface {v0, v1}, LaoM;->b(Ljava/lang/Class;)LaoR;

    .line 94
    const-class v0, Lgl;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lgo;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 96
    const-class v0, Lfo;

    invoke-virtual {p0, v0}, LdB;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Object;)V

    .line 98
    new-instance v0, Lir;

    const-string v1, "minVersion"

    const-string v2, "upgradeUrl"

    sget v3, Len;->version_too_old_close:I

    sget v4, Len;->version_too_old_drivev2:I

    sget v5, Len;->version_too_old_title:I

    sget v6, Len;->version_too_old_upgrade:I

    invoke-direct/range {v0 .. v6}, Lir;-><init>(Ljava/lang/String;Ljava/lang/String;IIII)V

    invoke-virtual {p0, v0}, LdB;->a(Laov;)V

    .line 102
    return-void
.end method
