.class abstract enum LahD;
.super Ljava/lang/Enum;
.source "LocalCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LahD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LahD;

.field private static final synthetic a:[LahD;

.field public static final enum b:LahD;

.field public static final enum c:LahD;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 373
    new-instance v0, LahE;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, LahE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LahD;->a:LahD;

    .line 388
    new-instance v0, LahF;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, LahF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LahD;->b:LahD;

    .line 404
    new-instance v0, LahG;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, LahG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LahD;->c:LahD;

    .line 367
    const/4 v0, 0x3

    new-array v0, v0, [LahD;

    sget-object v1, LahD;->a:LahD;

    aput-object v1, v0, v2

    sget-object v1, LahD;->b:LahD;

    aput-object v1, v0, v3

    sget-object v1, LahD;->c:LahD;

    aput-object v1, v0, v4

    sput-object v0, LahD;->a:[LahD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 367
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILaha;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 367
    invoke-direct {p0, p1, p2}, LahD;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LahD;
    .registers 2
    .parameter

    .prologue
    .line 367
    const-class v0, LahD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LahD;

    return-object v0
.end method

.method public static values()[LahD;
    .registers 1

    .prologue
    .line 367
    sget-object v0, LahD;->a:[LahD;

    invoke-virtual {v0}, [LahD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LahD;

    return-object v0
.end method


# virtual methods
.method abstract a()Lagh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lagh",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method abstract a(LahA;Lahz;Ljava/lang/Object;I)LahN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LahA",
            "<TK;TV;>;",
            "Lahz",
            "<TK;TV;>;TV;I)",
            "LahN",
            "<TK;TV;>;"
        }
    .end annotation
.end method
