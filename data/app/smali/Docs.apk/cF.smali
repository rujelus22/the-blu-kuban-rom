.class LcF;
.super Ljava/lang/Thread;
.source "GAThread.java"

# interfaces
.implements Lci;


# static fields
.field private static a:LcF;


# instance fields
.field private final a:Landroid/content/Context;

.field private volatile a:LcU;

.field private final a:Ldg;

.field private volatile a:Ljava/lang/String;

.field private volatile a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/analytics/internal/Command;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private volatile a:Z

.field private volatile b:Ljava/lang/String;

.field private volatile b:Z

.field private volatile c:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 71
    const-string v0, "GAThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 47
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, LcF;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 50
    iput-boolean v1, p0, LcF;->a:Z

    .line 51
    iput-boolean v1, p0, LcF;->b:Z

    .line 72
    iput-object p1, p0, LcF;->a:Landroid/content/Context;

    .line 73
    new-instance v0, Lcw;

    invoke-direct {v0, p1, p0}, Lcw;-><init>(Landroid/content/Context;Lci;)V

    iput-object v0, p0, LcF;->a:Ldg;

    .line 74
    invoke-direct {p0}, LcF;->b()V

    .line 75
    return-void
.end method

.method static a(Landroid/content/Context;)LcF;
    .registers 2
    .parameter

    .prologue
    .line 63
    sget-object v0, LcF;->a:LcF;

    if-nez v0, :cond_b

    .line 64
    new-instance v0, LcF;

    invoke-direct {v0, p0}, LcF;-><init>(Landroid/content/Context;)V

    sput-object v0, LcF;->a:LcF;

    .line 66
    :cond_b
    sget-object v0, LcF;->a:LcF;

    return-object v0
.end method

.method static synthetic a(LcF;)LcU;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LcF;->a:LcU;

    return-object v0
.end method

.method static synthetic a(LcF;)Ldg;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LcF;->a:Ldg;

    return-object v0
.end method

.method static a(Landroid/content/Context;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 414
    :try_start_1
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    .line 419
    const/16 v2, 0x2000

    new-array v2, v2, [B

    .line 420
    const/4 v3, 0x0

    const/16 v4, 0x2000

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3

    .line 421
    invoke-virtual {v1}, Ljava/io/FileInputStream;->available()I

    move-result v4

    if-lez v4, :cond_26

    .line 424
    const-string v2, "Too much campaign data, ignoring it."

    invoke-static {v2}, LcT;->c(Ljava/lang/String;)I

    .line 425
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 426
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 445
    :goto_25
    return-object v0

    .line 429
    :cond_26
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 430
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 431
    if-gtz v3, :cond_38

    .line 432
    const-string v1, "Campaign file is empty."

    invoke-static {v1}, LcT;->h(Ljava/lang/String;)I

    goto :goto_25

    .line 437
    :catch_36
    move-exception v1

    goto :goto_25

    .line 435
    :cond_38
    new-instance v1, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3}, Ljava/lang/String;-><init>([BII)V
    :try_end_3e
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_3e} :catch_36
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_3e} :catch_40

    move-object v0, v1

    goto :goto_25

    .line 440
    :catch_40
    move-exception v1

    .line 443
    const-string v1, "Error reading campaign data."

    invoke-static {v1}, LcT;->c(Ljava/lang/String;)I

    .line 444
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_25
.end method

.method static synthetic a(LcF;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LcF;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LcF;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, LcF;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(LcF;Ljava/util/Map;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, LcF;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Map;)Ljava/lang/String;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 131
    const-string v0, "internalHitUrl"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 132
    if-nez v0, :cond_1a

    .line 133
    const-string v0, "useSecure"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldl;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "https://ssl.google-analytics.com/collect"

    .line 137
    :cond_1a
    :goto_1a
    return-object v0

    .line 133
    :cond_1b
    const-string v0, "http://www.google-analytics.com/collect"

    goto :goto_1a
.end method

.method static synthetic a(LcF;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LcF;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(LcF;Ljava/util/Map;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, LcF;->c(Ljava/util/Map;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 314
    iget-object v0, p0, LcF;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 315
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 220
    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 221
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    :cond_9
    return-void
.end method

.method private a()Z
    .registers 3

    .prologue
    .line 322
    iget-object v0, p0, LcF;->a:Landroid/content/Context;

    const-string v1, "gaOptOut"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method static synthetic a(LcF;)Z
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-boolean v0, p0, LcF;->c:Z

    return v0
.end method

.method static synthetic a(LcF;Ljava/util/Map;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, LcF;->a(Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 333
    :try_start_1
    iget-object v1, p0, LcF;->a:Landroid/content/Context;

    const-string v2, "gaClientId"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 335
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 336
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_14} :catch_16
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_14} :catch_1d

    .line 337
    const/4 v0, 0x1

    .line 343
    :goto_15
    return v0

    .line 338
    :catch_16
    move-exception v1

    .line 339
    const-string v1, "Error creating clientId file."

    invoke-static {v1}, LcT;->c(Ljava/lang/String;)I

    goto :goto_15

    .line 341
    :catch_1d
    move-exception v1

    .line 342
    const-string v1, "Error writing to clientId file."

    invoke-static {v1}, LcT;->c(Ljava/lang/String;)I

    goto :goto_15
.end method

.method private a(Ljava/util/Map;)Z
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const-wide/high16 v6, 0x4059

    .line 179
    const-string v0, "sampleRate"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3f

    .line 180
    const-string v0, "sampleRate"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldl;->a(Ljava/lang/String;)D

    move-result-wide v2

    .line 181
    const-wide/16 v4, 0x0

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_1f

    move v0, v1

    .line 192
    :goto_1e
    return v0

    .line 184
    :cond_1f
    cmpg-double v0, v2, v6

    if-gez v0, :cond_3f

    .line 185
    const-string v0, "clientId"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 186
    if-eqz v0, :cond_3f

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    rem-int/lit16 v0, v0, 0x2710

    int-to-double v4, v0

    mul-double/2addr v2, v6

    cmpl-double v0, v4, v2

    if-ltz v0, :cond_3f

    move v0, v1

    .line 188
    goto :goto_1e

    .line 192
    :cond_3f
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method static synthetic b(LcF;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LcF;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 88
    iget-object v0, p0, LcF;->a:Ldg;

    invoke-interface {v0}, Ldg;->e()V

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LcF;->a:Ljava/util/List;

    .line 90
    iget-object v0, p0, LcF;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/analytics/internal/Command;

    const-string v2, "appendVersion"

    const-string v3, "_v"

    const-string v4, "ma1b2"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/analytics/internal/Command;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, LcF;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/analytics/internal/Command;

    const-string v2, "appendQueueTime"

    const-string v3, "qt"

    invoke-direct {v1, v2, v3, v5}, Lcom/google/android/gms/analytics/internal/Command;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v0, p0, LcF;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/analytics/internal/Command;

    const-string v2, "appendCacheBuster"

    const-string v3, "z"

    invoke-direct {v1, v2, v3, v5}, Lcom/google/android/gms/analytics/internal/Command;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v0, LcU;

    invoke-direct {v0}, LcU;-><init>()V

    iput-object v0, p0, LcF;->a:LcU;

    .line 94
    iget-object v0, p0, LcF;->a:LcU;

    invoke-static {v0}, LcX;->a(LcU;)V

    .line 96
    invoke-virtual {p0}, LcF;->start()V

    .line 97
    return-void
.end method

.method static synthetic b(LcF;Ljava/util/Map;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, LcF;->d(Ljava/util/Map;)V

    return-void
.end method

.method private b(Ljava/util/Map;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    const-string v0, "rawException"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145
    if-nez v0, :cond_b

    .line 174
    :cond_a
    :goto_a
    return-void

    .line 148
    :cond_b
    const-string v1, "rawException"

    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-static {v0}, Ldl;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 150
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 153
    :try_start_19
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 154
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 155
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V

    .line 156
    instance-of v1, v0, Ljava/lang/Throwable;

    if-eqz v1, :cond_a

    .line 157
    check-cast v0, Ljava/lang/Throwable;
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_2b} :catch_49
    .catch Ljava/lang/ClassNotFoundException; {:try_start_19 .. :try_end_2b} :catch_50

    .line 170
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 171
    new-instance v2, Ldi;

    iget-object v3, p0, LcF;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Ldi;-><init>(Landroid/content/Context;Ljava/util/Collection;)V

    .line 172
    const-string v3, "exDescription"

    const-string v1, "exceptionThreadName"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lcq;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 161
    :catch_49
    move-exception v0

    .line 162
    const-string v0, "IOException reading exception"

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    goto :goto_a

    .line 164
    :catch_50
    move-exception v0

    .line 165
    const-string v0, "ClassNotFoundException reading exception"

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    goto :goto_a
.end method

.method static synthetic c(LcF;Ljava/util/Map;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, LcF;->b(Ljava/util/Map;)V

    return-void
.end method

.method private c(Ljava/util/Map;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, LcF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 198
    iget-object v0, p0, LcF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 202
    const/4 v0, 0x0

    .line 204
    :try_start_11
    iget-object v4, p0, LcF;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 205
    if-eqz v4, :cond_61

    .line 206
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_27
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_11 .. :try_end_27} :catch_46

    move-result-object v1

    .line 207
    :try_start_28
    iget-object v0, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_2a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_28 .. :try_end_2a} :catch_5f

    .line 212
    :goto_2a
    const-string v4, "appName"

    invoke-direct {p0, p1, v4, v1}, LcF;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v1, "appVersion"

    invoke-direct {p0, p1, v1, v0}, LcF;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v0, "appId"

    invoke-direct {p0, p1, v0, v2}, LcF;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v0, "appInstallerId"

    invoke-direct {p0, p1, v0, v3}, LcF;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v0, "apiVersion"

    const-string v1, "1"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    return-void

    .line 209
    :catch_46
    move-exception v1

    move-object v1, v2

    .line 210
    :goto_48
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error retrieving package info: appName set to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LcT;->c(Ljava/lang/String;)I

    goto :goto_2a

    .line 209
    :catch_5f
    move-exception v4

    goto :goto_48

    :cond_61
    move-object v1, v2

    goto :goto_2a
.end method

.method private d(Ljava/util/Map;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    const-string v0, "campaign"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ldl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 246
    :goto_12
    return-void

    .line 232
    :cond_13
    invoke-static {v0}, Ldl;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 237
    const-string v1, "campaignContent"

    const-string v2, "utm_content"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    const-string v1, "campaignMedium"

    const-string v2, "utm_medium"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    const-string v1, "campaignName"

    const-string v2, "utm_campaign"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    const-string v1, "campaignSource"

    const-string v2, "utm_source"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    const-string v1, "campaignKeyword"

    const-string v2, "utm_term"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-string v1, "campaignId"

    const-string v2, "utm_id"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    const-string v1, "gclid"

    const-string v2, "gclid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const-string v1, "dclid"

    const-string v2, "dclid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    const-string v1, "gmob_t"

    const-string v2, "gmob_t"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_12
.end method


# virtual methods
.method a()Ljava/lang/String;
    .registers 5

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 361
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    and-long/2addr v0, v2

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 362
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-direct {p0, v0}, LcF;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 366
    :cond_1d
    return-object v0
.end method

.method public a()Ljava/lang/Thread;
    .registers 1

    .prologue
    .line 488
    return-object p0
.end method

.method public a()Ljava/util/concurrent/LinkedBlockingQueue;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483
    iget-object v0, p0, LcF;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 250
    new-instance v0, LcH;

    invoke-direct {v0, p0}, LcH;-><init>(LcF;)V

    invoke-direct {p0, v0}, LcF;->a(Ljava/lang/Runnable;)V

    .line 258
    return-void
.end method

.method public a(Lca;)V
    .registers 3
    .parameter

    .prologue
    .line 291
    new-instance v0, LcI;

    invoke-direct {v0, p0, p1}, LcI;-><init>(LcF;Lca;)V

    invoke-direct {p0, v0}, LcF;->a(Ljava/lang/Runnable;)V

    .line 299
    return-void
.end method

.method public a(Lcj;)V
    .registers 3
    .parameter

    .prologue
    .line 303
    new-instance v0, LcJ;

    invoke-direct {v0, p0, p1}, LcJ;-><init>(LcF;Lcj;)V

    invoke-direct {p0, v0}, LcF;->a(Ljava/lang/Runnable;)V

    .line 311
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 102
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 103
    new-instance v3, LcG;

    invoke-direct {v3, p0, v0, v1, v2}, LcG;-><init>(LcF;Ljava/util/Map;J)V

    invoke-direct {p0, v3}, LcF;->a(Ljava/lang/Runnable;)V

    .line 127
    return-void
.end method

.method b()Ljava/lang/String;
    .registers 7

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 372
    :try_start_1
    iget-object v1, p0, LcF;->a:Landroid/content/Context;

    const-string v2, "gaClientId"

    invoke-virtual {v1, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    .line 373
    const/16 v1, 0x80

    new-array v3, v1, [B

    .line 374
    const/4 v1, 0x0

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v1, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    .line 375
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v1

    if-lez v1, :cond_29

    .line 378
    const-string v1, "clientId file seems corrupted, deleting it."

    invoke-static {v1}, LcT;->c(Ljava/lang/String;)I

    .line 379
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 380
    iget-object v1, p0, LcF;->a:Landroid/content/Context;

    const-string v5, "gaInstallData"

    invoke-virtual {v1, v5}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 382
    :cond_29
    if-gtz v4, :cond_41

    .line 383
    const-string v1, "clientId file seems empty, deleting it."

    invoke-static {v1}, LcT;->c(Ljava/lang/String;)I

    .line 384
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 385
    iget-object v1, p0, LcF;->a:Landroid/content/Context;

    const-string v2, "gaInstallData"

    invoke-virtual {v1, v2}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_3a
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_3a} :catch_6e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_3a} :catch_4c
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_3a} :catch_5a

    .line 400
    :goto_3a
    if-nez v0, :cond_40

    .line 401
    invoke-virtual {p0}, LcF;->a()Ljava/lang/String;

    move-result-object v0

    .line 403
    :cond_40
    return-object v0

    .line 387
    :cond_41
    :try_start_41
    new-instance v1, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v1, v3, v5, v4}, Ljava/lang/String;-><init>([BII)V
    :try_end_47
    .catch Ljava/io/FileNotFoundException; {:try_start_41 .. :try_end_47} :catch_6e
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_47} :catch_4c
    .catch Ljava/lang/NumberFormatException; {:try_start_41 .. :try_end_47} :catch_5a

    .line 388
    :try_start_47
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4a
    .catch Ljava/io/FileNotFoundException; {:try_start_47 .. :try_end_4a} :catch_70
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_4a} :catch_6b
    .catch Ljava/lang/NumberFormatException; {:try_start_47 .. :try_end_4a} :catch_68

    move-object v0, v1

    goto :goto_3a

    .line 392
    :catch_4c
    move-exception v1

    .line 393
    :goto_4d
    const-string v1, "Error reading clientId file, deleting it."

    invoke-static {v1}, LcT;->c(Ljava/lang/String;)I

    .line 394
    iget-object v1, p0, LcF;->a:Landroid/content/Context;

    const-string v2, "gaInstallData"

    invoke-virtual {v1, v2}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_3a

    .line 395
    :catch_5a
    move-exception v1

    .line 396
    :goto_5b
    const-string v1, "cliendId file doesn\'t have long value, deleting it."

    invoke-static {v1}, LcT;->c(Ljava/lang/String;)I

    .line 397
    iget-object v1, p0, LcF;->a:Landroid/content/Context;

    const-string v2, "gaInstallData"

    invoke-virtual {v1, v2}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_3a

    .line 395
    :catch_68
    move-exception v0

    move-object v0, v1

    goto :goto_5b

    .line 392
    :catch_6b
    move-exception v0

    move-object v0, v1

    goto :goto_4d

    .line 390
    :catch_6e
    move-exception v1

    goto :goto_3a

    :catch_70
    move-exception v0

    move-object v0, v1

    goto :goto_3a
.end method

.method public run()V
    .registers 4

    .prologue
    .line 451
    invoke-direct {p0}, LcF;->a()Z

    move-result v0

    iput-boolean v0, p0, LcF;->c:Z

    .line 452
    invoke-virtual {p0}, LcF;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LcF;->b:Ljava/lang/String;

    .line 453
    iget-object v0, p0, LcF;->a:Landroid/content/Context;

    invoke-static {v0}, LcF;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LcF;->a:Ljava/lang/String;

    .line 454
    :cond_14
    :goto_14
    iget-boolean v0, p0, LcF;->b:Z

    if-nez v0, :cond_6a

    .line 460
    :try_start_18
    iget-object v0, p0, LcF;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 461
    iget-boolean v1, p0, LcF;->a:Z

    if-nez v1, :cond_14

    .line 462
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_27
    .catch Ljava/lang/InterruptedException; {:try_start_18 .. :try_end_27} :catch_28
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_27} :catch_31

    goto :goto_14

    .line 464
    :catch_28
    move-exception v0

    .line 465
    :try_start_29
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->d(Ljava/lang/String;)I
    :try_end_30
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_30} :catch_31

    goto :goto_14

    .line 467
    :catch_31
    move-exception v0

    .line 468
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 469
    new-instance v2, Ljava/io/PrintStream;

    invoke-direct {v2, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    .line 470
    invoke-virtual {v0, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 471
    invoke-virtual {v2}, Ljava/io/PrintStream;->flush()V

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error on GAThread: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    .line 475
    const-string v0, "Google Analytics is shutting down."

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    .line 476
    const/4 v0, 0x1

    iput-boolean v0, p0, LcF;->a:Z

    goto :goto_14

    .line 479
    :cond_6a
    return-void
.end method
