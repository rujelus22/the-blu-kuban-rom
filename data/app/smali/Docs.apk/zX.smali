.class public LzX;
.super Ljava/lang/Object;
.source "DrawHelper.java"


# instance fields
.field private a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LAa;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LzZ;

.field private a:Z


# direct methods
.method public constructor <init>(LzZ;)V
    .registers 3
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, LzX;->a:Ljava/util/Stack;

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, LzX;->a:Z

    .line 31
    iput-object p1, p0, LzX;->a:LzZ;

    .line 32
    return-void
.end method

.method static synthetic a(LzX;)LzZ;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, LzX;->a:LzZ;

    return-object v0
.end method

.method static synthetic a(LzX;Ljava/util/Stack;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1}, LzX;->b(Ljava/util/Stack;)V

    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, LzX;->a:Ljava/util/Stack;

    invoke-direct {p0, v0}, LzX;->b(Ljava/util/Stack;)V

    .line 92
    iget-object v0, p0, LzX;->a:Ljava/util/Stack;

    new-instance v1, LzY;

    invoke-direct {v1, p0, p1}, LzY;-><init>(LzX;Landroid/graphics/Canvas;)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-void
.end method

.method private b(Ljava/util/Stack;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack",
            "<",
            "LAa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, LzX;->a:LzZ;

    invoke-interface {v0}, LzZ;->a()Landroid/view/ViewGroup;

    move-result-object v2

    .line 74
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 75
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v3, :cond_23

    .line 76
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 77
    instance-of v4, v0, LzZ;

    if-eqz v4, :cond_1f

    .line 78
    check-cast v0, LzZ;

    invoke-interface {v0}, LzZ;->a()LzX;

    move-result-object v0

    invoke-virtual {v0, p1}, LzX;->a(Ljava/util/Stack;)V

    .line 75
    :cond_1f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 81
    :cond_23
    return-void
.end method

.method private c(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter

    .prologue
    .line 115
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    .line 118
    invoke-virtual {p0, v1}, LzX;->a(Ljava/util/Stack;)V

    .line 119
    invoke-direct {p0, p1}, LzX;->b(Landroid/graphics/Canvas;)V

    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LzX;->a(Ljava/util/Stack;)V

    .line 124
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 125
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 126
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 127
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 129
    :goto_22
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_32

    .line 130
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAa;

    .line 131
    invoke-interface {v0, p1}, LAa;->a(Landroid/graphics/Canvas;)V

    goto :goto_22

    .line 134
    :cond_32
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 135
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LzX;->a:Ljava/util/Stack;

    if-nez v0, :cond_8

    .line 47
    invoke-direct {p0, p1}, LzX;->c(Landroid/graphics/Canvas;)V

    .line 51
    :goto_7
    return-void

    .line 49
    :cond_8
    invoke-direct {p0, p1}, LzX;->b(Landroid/graphics/Canvas;)V

    goto :goto_7
.end method

.method public a(Ljava/util/Stack;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack",
            "<",
            "LAa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    iput-object p1, p0, LzX;->a:Ljava/util/Stack;

    .line 39
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 54
    iput-boolean p1, p0, LzX;->a:Z

    .line 55
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, LzX;->a:LzZ;

    invoke-interface {v1}, LzZ;->a()Landroid/view/ViewGroup;

    move-result-object v1

    .line 65
    iget-boolean v2, p0, LzX;->a:Z

    if-eqz v2, :cond_19

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1, v0, v0, v2, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    move-result v1

    if-eqz v1, :cond_1a

    :cond_19
    const/4 v0, 0x1

    :cond_1a
    return v0
.end method
