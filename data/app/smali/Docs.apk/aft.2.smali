.class public final enum Laft;
.super Ljava/lang/Enum;
.source "GenericData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laft;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laft;

.field private static final synthetic a:[Laft;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 71
    new-instance v0, Laft;

    const-string v1, "IGNORE_CASE"

    invoke-direct {v0, v1, v2}, Laft;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laft;->a:Laft;

    .line 68
    const/4 v0, 0x1

    new-array v0, v0, [Laft;

    sget-object v1, Laft;->a:Laft;

    aput-object v1, v0, v2

    sput-object v0, Laft;->a:[Laft;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laft;
    .registers 2
    .parameter

    .prologue
    .line 68
    const-class v0, Laft;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laft;

    return-object v0
.end method

.method public static values()[Laft;
    .registers 1

    .prologue
    .line 68
    sget-object v0, Laft;->a:[Laft;

    invoke-virtual {v0}, [Laft;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laft;

    return-object v0
.end method
