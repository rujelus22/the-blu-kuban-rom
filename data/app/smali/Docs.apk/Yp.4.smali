.class public LYp;
.super Ljava/lang/Object;
.source "PartialDownloadedFileStoreImpl.java"

# interfaces
.implements LYn;


# annotations
.annotation runtime LaoJ;
.end annotation


# instance fields
.field private final a:I

.field private final a:Lad;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lad",
            "<",
            "Ljava/lang/String;",
            "LYo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LKS;)V
    .registers 4
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    const/4 v0, 0x3

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    if-eqz p1, :cond_c

    const-string v1, "maxIncompleteDownloads"

    invoke-interface {p1, v1, v0}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    :cond_c
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LYp;->a:I

    .line 136
    new-instance v0, LYq;

    iget v1, p0, LYp;->a:I

    invoke-direct {v0, p0, v1}, LYq;-><init>(LYp;I)V

    iput-object v0, p0, LYp;->a:Lad;

    .line 145
    return-void
.end method

.method private a(LkM;LUK;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LkM;->c()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, LUK;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LUM;)LYo;
    .registers 3
    .parameter

    .prologue
    .line 172
    new-instance v0, LYr;

    invoke-direct {v0, p1}, LYr;-><init>(LUM;)V

    return-object v0
.end method

.method public declared-synchronized a(LkM;LUK;)LYo;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 162
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LYp;->a:Lad;

    invoke-direct {p0, p1, p2}, LYp;->a(LkM;LUK;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lad;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYo;

    .line 163
    if-eqz v0, :cond_19

    invoke-interface {v0}, LYo;->b()Z

    move-result v1

    if-nez v1, :cond_19

    .line 164
    invoke-static {v0}, Lamq;->a(Ljava/io/Closeable;)V
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1b

    .line 165
    const/4 v0, 0x0

    .line 167
    :cond_19
    monitor-exit p0

    return-object v0

    .line 162
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LkM;LUK;LYo;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 149
    monitor-enter p0

    if-eqz p3, :cond_a

    .line 151
    :try_start_3
    iget v0, p0, LYp;->a:I

    if-nez v0, :cond_c

    .line 152
    invoke-static {p3}, Lamq;->a(Ljava/io/Closeable;)V
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_1c

    .line 158
    :cond_a
    :goto_a
    monitor-exit p0

    return-void

    .line 154
    :cond_c
    :try_start_c
    invoke-direct {p0, p1, p2}, LYp;->a(LkM;LUK;)Ljava/lang/String;

    move-result-object v0

    .line 155
    iget-object v1, p0, LYp;->a:Lad;

    invoke-virtual {v1, v0, p3}, Lad;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lamq;->a(Ljava/io/Closeable;)V
    :try_end_1b
    .catchall {:try_start_c .. :try_end_1b} :catchall_1c

    goto :goto_a

    .line 149
    :catchall_1c
    move-exception v0

    monitor-exit p0

    throw v0
.end method
