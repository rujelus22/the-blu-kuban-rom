.class public final enum LPr;
.super Ljava/lang/Enum;
.source "AccountMetadataTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPr;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPr;

.field private static final synthetic a:[LPr;

.field public static final enum b:LPr;

.field public static final enum c:LPr;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xe

    .line 44
    new-instance v0, LPr;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, LPq;->b()LPq;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "accountId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-static {}, LPs;->a()LPs;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPr;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPr;->a:LPr;

    .line 48
    new-instance v0, LPr;

    const-string v1, "CAPABILITY_CONTENT"

    invoke-static {}, LPq;->b()LPq;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "capabilityContent"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPr;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPr;->b:LPr;

    .line 51
    new-instance v0, LPr;

    const-string v1, "LAST_UPDATED_TIME"

    invoke-static {}, LPq;->b()LPq;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "lastUpdatedDate"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LPr;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPr;->c:LPr;

    .line 43
    const/4 v0, 0x3

    new-array v0, v0, [LPr;

    sget-object v1, LPr;->a:LPr;

    aput-object v1, v0, v7

    sget-object v1, LPr;->b:LPr;

    aput-object v1, v0, v8

    sget-object v1, LPr;->c:LPr;

    aput-object v1, v0, v9

    sput-object v0, LPr;->a:[LPr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPr;->a:LPI;

    .line 60
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPr;
    .registers 2
    .parameter

    .prologue
    .line 43
    const-class v0, LPr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPr;

    return-object v0
.end method

.method public static values()[LPr;
    .registers 1

    .prologue
    .line 43
    sget-object v0, LPr;->a:[LPr;

    invoke-virtual {v0}, [LPr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPr;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, LPr;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 43
    invoke-virtual {p0}, LPr;->a()LPI;

    move-result-object v0

    return-object v0
.end method
