.class public abstract enum Lamc;
.super Ljava/lang/Enum;
.source "SortedLists.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lamc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lamc;

.field private static final synthetic a:[Lamc;

.field public static final enum b:Lamc;

.field public static final enum c:Lamc;

.field public static final enum d:Lamc;

.field public static final enum e:Lamc;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, Lamd;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1, v2}, Lamd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamc;->a:Lamc;

    .line 63
    new-instance v0, Lame;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1, v3}, Lame;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamc;->b:Lamc;

    .line 87
    new-instance v0, Lamf;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1, v4}, Lamf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamc;->c:Lamc;

    .line 113
    new-instance v0, Lamg;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1, v5}, Lamg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamc;->d:Lamc;

    .line 124
    new-instance v0, Lamh;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1, v6}, Lamh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamc;->e:Lamc;

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Lamc;

    sget-object v1, Lamc;->a:Lamc;

    aput-object v1, v0, v2

    sget-object v1, Lamc;->b:Lamc;

    aput-object v1, v0, v3

    sget-object v1, Lamc;->c:Lamc;

    aput-object v1, v0, v4

    sget-object v1, Lamc;->d:Lamc;

    aput-object v1, v0, v5

    sget-object v1, Lamc;->e:Lamc;

    aput-object v1, v0, v6

    sput-object v0, Lamc;->a:[Lamc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILalX;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lamc;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lamc;
    .registers 2
    .parameter

    .prologue
    .line 48
    const-class v0, Lamc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lamc;

    return-object v0
.end method

.method public static values()[Lamc;
    .registers 1

    .prologue
    .line 48
    sget-object v0, Lamc;->a:[Lamc;

    invoke-virtual {v0}, [Lamc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamc;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;",
            "Ljava/util/List",
            "<+TE;>;I)I"
        }
    .end annotation
.end method
