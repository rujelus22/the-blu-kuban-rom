.class public final Ltn;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;


# direct methods
.method public constructor <init>(LYD;)V
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 30
    iput-object p1, p0, Ltn;->a:LYD;

    .line 31
    return-void
.end method

.method static synthetic a(Ltn;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Ltn;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 72
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;

    new-instance v1, Lto;

    invoke-direct {v1, p0}, Lto;-><init>(Ltn;)V

    invoke-virtual {p0, v0, v1}, Ltn;->a(Ljava/lang/Class;Laou;)V

    .line 80
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/AllDiscussionsStateMachineFragment;

    new-instance v1, Ltp;

    invoke-direct {v1, p0}, Ltp;-><init>(Ltn;)V

    invoke-virtual {p0, v0, v1}, Ltn;->a(Ljava/lang/Class;Laou;)V

    .line 88
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;

    new-instance v1, Ltq;

    invoke-direct {v1, p0}, Ltq;-><init>(Ltn;)V

    invoke-virtual {p0, v0, v1}, Ltn;->a(Ljava/lang/Class;Laou;)V

    .line 96
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;

    new-instance v1, Ltr;

    invoke-direct {v1, p0}, Ltr;-><init>(Ltn;)V

    invoke-virtual {p0, v0, v1}, Ltn;->a(Ljava/lang/Class;Laou;)V

    .line 104
    const-class v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    new-instance v1, Lts;

    invoke-direct {v1, p0}, Lts;-><init>(Ltn;)V

    invoke-virtual {p0, v0, v1}, Ltn;->a(Ljava/lang/Class;Laou;)V

    .line 112
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/statefragments/AllDiscussionsStateMachineFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Ltn;->a:LYD;

    iget-object v0, v0, LYD;->a:Ltn;

    invoke-virtual {v0, p1}, Ltn;->a(Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;)V

    .line 45
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Ltn;->a:LYD;

    iget-object v0, v0, LYD;->a:Ltf;

    iget-object v0, v0, Ltf;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ltn;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsT;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:LsT;

    .line 67
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Ltn;->a:LYD;

    iget-object v0, v0, LYD;->a:Ltn;

    invoke-virtual {v0, p1}, Ltn;->a(Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;)V

    .line 51
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Ltn;->a:LYD;

    iget-object v0, v0, LYD;->a:Ltn;

    invoke-virtual {v0, p1}, Ltn;->a(Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;)V

    .line 39
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Ltn;->a:LYD;

    iget-object v0, v0, LYD;->a:Ltn;

    invoke-virtual {v0, p1}, Ltn;->a(Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;)V

    .line 57
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 116
    return-void
.end method
