.class public LAh;
.super Ljava/lang/Object;
.source "ParagraphChildFactory.java"

# interfaces
.implements LAb;


# annotations
.annotation runtime LaoJ;
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method private a(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    new-instance v0, LAi;

    const-class v3, LAK;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAi;-><init>(LAh;ILjava/lang/Class;Lzd;Lxu;)V

    return-object v0
.end method

.method private b(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    new-instance v0, LAj;

    const-class v3, LAB;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAj;-><init>(LAh;ILjava/lang/Class;Lzd;Lxu;)V

    return-object v0
.end method

.method private c(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    new-instance v0, LAk;

    const-class v3, LAy;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAk;-><init>(LAh;ILjava/lang/Class;Lzd;Lxu;)V

    return-object v0
.end method

.method private d(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    new-instance v0, LAl;

    const-class v3, LAE;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAl;-><init>(LAh;ILjava/lang/Class;Lzd;Lxu;)V

    return-object v0
.end method

.method private e(ILzd;Lxu;)LAc;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    new-instance v0, LAm;

    const-class v1, LAz;

    invoke-direct {v0, p0, p1, v1, p3}, LAm;-><init>(LAh;ILjava/lang/Class;Lxu;)V

    return-object v0
.end method

.method private f(ILzd;Lxu;)LAc;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 90
    new-instance v0, LAn;

    const-class v3, LAN;

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LAn;-><init>(LAh;ILjava/lang/Class;Lzd;Lxu;)V

    return-object v0
.end method


# virtual methods
.method public a(IILzd;Lxu;)LAc;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 102
    add-int/lit8 v0, p2, -0x1

    invoke-interface {p3, p1, v0}, Lzd;->a(II)Lvs;

    move-result-object v0

    .line 103
    invoke-interface {v0}, Lvs;->a()I

    move-result v1

    .line 104
    invoke-interface {v0}, Lvs;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 105
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 107
    invoke-static {v1}, LxH;->a(I)LxH;

    move-result-object v1

    .line 108
    sget-object v2, LxH;->a:LxH;

    if-ne v1, v2, :cond_21

    .line 109
    invoke-direct {p0, v0, p3, p4}, LAh;->a(ILzd;Lxu;)LAc;

    move-result-object v0

    .line 120
    :goto_20
    return-object v0

    .line 111
    :cond_21
    sget-object v2, LxH;->e:LxH;

    if-ne v1, v2, :cond_31

    invoke-interface {p3, p1}, Lzd;->a(I)[Lvu;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_31

    .line 112
    invoke-direct {p0, v0, p3, p4}, LAh;->b(ILzd;Lxu;)LAc;

    move-result-object v0

    goto :goto_20

    .line 113
    :cond_31
    sget-object v2, LxH;->b:LxH;

    if-ne v1, v2, :cond_3a

    .line 114
    invoke-direct {p0, v0, p3, p4}, LAh;->c(ILzd;Lxu;)LAc;

    move-result-object v0

    goto :goto_20

    .line 115
    :cond_3a
    sget-object v2, LxH;->g:LxH;

    if-ne v1, v2, :cond_43

    .line 116
    invoke-direct {p0, v0, p3, p4}, LAh;->d(ILzd;Lxu;)LAc;

    move-result-object v0

    goto :goto_20

    .line 117
    :cond_43
    sget-object v2, LxH;->h:LxH;

    if-ne v1, v2, :cond_4c

    .line 118
    invoke-direct {p0, v0, p3, p4}, LAh;->e(ILzd;Lxu;)LAc;

    move-result-object v0

    goto :goto_20

    .line 120
    :cond_4c
    invoke-direct {p0, v0, p3, p4}, LAh;->f(ILzd;Lxu;)LAc;

    move-result-object v0

    goto :goto_20
.end method
