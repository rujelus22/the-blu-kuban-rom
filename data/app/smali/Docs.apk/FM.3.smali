.class public LFM;
.super LFP;
.source "ArrowKeyMovementMethod.java"

# interfaces
.implements LFZ;


# static fields
.field private static a:LFM;

.field private static final a:Ljava/lang/Object;


# instance fields
.field private final a:LFJ;

.field private final a:LGw;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 400
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LFM;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 38
    invoke-direct {p0}, LFP;-><init>()V

    .line 398
    new-instance v0, LGw;

    invoke-direct {v0}, LGw;-><init>()V

    iput-object v0, p0, LFM;->a:LGw;

    .line 399
    new-instance v0, LFJ;

    invoke-direct {v0}, LFJ;-><init>()V

    iput-object v0, p0, LFM;->a:LFJ;

    return-void
.end method

.method private a(Landroid/text/Spannable;I)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 360
    iget-object v0, p0, LFM;->a:LGw;

    invoke-virtual {v0, p1}, LGw;->a(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v0, p0, LFM;->a:LGw;

    invoke-virtual {v0, p2}, LGw;->a(I)I

    move-result v0

    .line 362
    const/4 v1, -0x1

    if-ne v0, v1, :cond_f

    .line 363
    const/4 v0, 0x0

    .line 365
    :cond_f
    return v0
.end method

.method private a(Landroid/text/Spannable;LEj;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-interface {p2, v0}, LEj;->g(I)I

    move-result v0

    invoke-interface {p2, v0}, LEj;->b(I)I

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 4
    .parameter

    .prologue
    .line 52
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 53
    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public static a()LFZ;
    .registers 1

    .prologue
    .line 391
    sget-object v0, LFM;->a:LFM;

    if-nez v0, :cond_b

    .line 392
    new-instance v0, LFM;

    invoke-direct {v0}, LFM;-><init>()V

    sput-object v0, LFM;->a:LFM;

    .line 395
    :cond_b
    sget-object v0, LFM;->a:LFM;

    return-object v0
.end method

.method private static a(Landroid/text/Spannable;)Z
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 40
    invoke-static {p0, v0}, LFY;->a(Ljava/lang/CharSequence;I)I

    move-result v1

    if-eq v1, v0, :cond_f

    const/16 v1, 0x800

    invoke-static {p0, v1}, LFY;->a(Ljava/lang/CharSequence;I)I

    move-result v1

    if-eqz v1, :cond_10

    :cond_f
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 375
    const/4 v0, 0x0

    invoke-static {p2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 376
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 380
    and-int/lit16 v0, p3, 0x82

    if-eqz v0, :cond_12

    .line 381
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    if-nez v0, :cond_11

    .line 383
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v0

    invoke-static {p2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 388
    :cond_11
    :goto_11
    return-void

    .line 386
    :cond_12
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v0

    invoke-static {p2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_11
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 370
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    .line 77
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 78
    invoke-static {p2, v0}, LEn;->g(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 80
    :goto_e
    return v0

    :cond_f
    invoke-static {p2, v0}, LEn;->c(Landroid/text/Spannable;LEj;)Z

    move-result v0

    goto :goto_e
.end method

.method protected a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    packed-switch p3, :pswitch_data_28

    .line 71
    :cond_3
    invoke-super/range {p0 .. p5}, LFP;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;IILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_7
    return v0

    .line 61
    :pswitch_8
    invoke-static {p4}, LFW;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    invoke-virtual {p5}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p5}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x800

    invoke-static {p2, v0}, LFY;->a(Ljava/lang/CharSequence;I)I

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->showContextMenu()Z

    move-result v0

    goto :goto_7

    .line 59
    nop

    :pswitch_data_28
    .packed-switch 0x17
        :pswitch_8
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 261
    iget-object v0, p0, LFM;->a:LFJ;

    invoke-virtual {v0, p1, p2, p3}, LFJ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    .line 87
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 88
    invoke-static {p2, v0}, LEn;->h(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 90
    :goto_e
    return v0

    :cond_f
    invoke-static {p2, v0}, LEn;->d(Landroid/text/Spannable;LEj;)Z

    move-result v0

    goto :goto_e
.end method

.method public b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x1

    .line 266
    .line 268
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 270
    if-ne v4, v2, :cond_f4

    .line 271
    invoke-static {p1, p2}, LGt;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)I

    move-result v1

    .line 272
    invoke-static {p1, p2}, LGt;->b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)I

    move-result v0

    .line 275
    :goto_10
    invoke-static {p1, p2, p3}, LGt;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v3

    .line 277
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_44

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->n()Z

    move-result v5

    if-nez v5, :cond_44

    .line 278
    if-nez v4, :cond_46

    .line 279
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 280
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v0

    .line 282
    sget-object v1, LFM;->a:Ljava/lang/Object;

    const/16 v4, 0x22

    invoke-interface {p2, v1, v0, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 288
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_44
    move v0, v3

    .line 356
    :goto_45
    return v0

    .line 290
    :cond_46
    const/4 v5, 0x2

    if-ne v4, v5, :cond_67

    .line 291
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v0

    if-eqz v0, :cond_44

    if-eqz v3, :cond_44

    .line 298
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->cancelLongPress()V

    .line 303
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v0

    .line 305
    invoke-static {p2, v0}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    move v0, v2

    .line 306
    goto :goto_45

    .line 308
    :cond_67
    if-ne v4, v2, :cond_44

    .line 310
    if-ltz v0, :cond_71

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v3

    if-ne v0, v3, :cond_79

    :cond_71
    if-ltz v1, :cond_7b

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v0

    if-eq v1, v0, :cond_7b

    :cond_79
    move v0, v2

    .line 312
    goto :goto_45

    .line 315
    :cond_7b
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v1

    .line 316
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 317
    sget-object v0, LFM;->a:Ljava/lang/Object;

    invoke-interface {p2, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 318
    invoke-static {p2, v1}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    .line 349
    :cond_97
    :goto_97
    invoke-static {p2}, LFY;->b(Landroid/text/Spannable;)V

    .line 350
    invoke-static {p2}, LFY;->c(Landroid/text/Spannable;)V

    move v0, v2

    .line 352
    goto :goto_45

    .line 320
    :cond_9f
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 321
    const-class v0, LGD;

    invoke-interface {p2, v1, v1, v0}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGD;

    array-length v0, v0

    if-nez v0, :cond_b1

    .line 322
    invoke-static {p2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 324
    :cond_b1
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    .line 326
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v4

    .line 327
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_97

    if-eqz v4, :cond_97

    .line 328
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getSize()F

    move-result v5

    float-to-double v5, v5

    const-wide/high16 v7, 0x3fe0

    cmpl-double v5, v5, v7

    if-lez v5, :cond_dc

    .line 331
    invoke-direct {p0, p2, v1}, LFM;->a(Landroid/text/Spannable;I)I

    move-result v1

    .line 332
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v3

    invoke-static {v0, p2, v1, v3}, LEZ;->a(Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;II)V

    goto :goto_97

    .line 336
    :cond_dc
    invoke-interface {v4, v3}, LEj;->g(I)I

    move-result v0

    .line 337
    invoke-interface {v4, v1}, LEj;->g(I)I

    move-result v3

    .line 338
    if-eq v0, v3, :cond_ea

    .line 339
    invoke-static {p1, p2, v1}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_97

    .line 341
    :cond_ea
    invoke-direct {p0, p2, v1}, LFM;->a(Landroid/text/Spannable;I)I

    move-result v0

    .line 342
    iget-object v1, p0, LFM;->a:LGw;

    invoke-static {p1, p2, v1, v0}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;LGw;I)V

    goto :goto_97

    :cond_f4
    move v1, v0

    goto/16 :goto_10
.end method

.method protected c(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    .line 97
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 98
    invoke-static {p2, v0}, LEn;->e(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 103
    :goto_e
    return v0

    .line 100
    :cond_f
    invoke-static {p2, v0}, LEn;->a(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 101
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {p1, p2, v1}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_e
.end method

.method protected d(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    .line 110
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 111
    invoke-static {p2, v0}, LEn;->f(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 116
    :goto_e
    return v0

    .line 113
    :cond_f
    invoke-static {p2, v0}, LEn;->b(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 114
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {p1, p2, v1}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_e
.end method

.method protected e(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 122
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v1

    .line 123
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v2

    .line 124
    invoke-direct {p0, p2, v1}, LFM;->a(Landroid/text/Spannable;LEj;)I

    move-result v0

    invoke-direct {p0, p1}, LFM;->a(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v3

    sub-int v3, v0, v3

    .line 125
    const/4 v0, 0x0

    .line 127
    :cond_13
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v4

    .line 128
    if-eqz v2, :cond_2c

    .line 129
    invoke-static {p2, v1}, LEn;->e(Landroid/text/Spannable;LEj;)Z

    .line 133
    :goto_1c
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v5

    if-ne v5, v4, :cond_30

    .line 141
    :goto_22
    if-eqz v0, :cond_2b

    .line 142
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {p1, p2, v1}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;I)V

    .line 145
    :cond_2b
    return v0

    .line 131
    :cond_2c
    invoke-static {p2, v1}, LEn;->a(Landroid/text/Spannable;LEj;)Z

    goto :goto_1c

    .line 136
    :cond_30
    const/4 v0, 0x1

    .line 137
    invoke-direct {p0, p2, v1}, LFM;->a(Landroid/text/Spannable;LEj;)I

    move-result v4

    if-gt v4, v3, :cond_13

    goto :goto_22
.end method

.method protected f(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 150
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v1

    .line 151
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v2

    .line 152
    invoke-direct {p0, p2, v1}, LFM;->a(Landroid/text/Spannable;LEj;)I

    move-result v0

    invoke-direct {p0, p1}, LFM;->a(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v3

    add-int/2addr v3, v0

    .line 153
    const/4 v0, 0x0

    .line 155
    :cond_12
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v4

    .line 156
    if-eqz v2, :cond_2b

    .line 157
    invoke-static {p2, v1}, LEn;->f(Landroid/text/Spannable;LEj;)Z

    .line 161
    :goto_1b
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v5

    if-ne v5, v4, :cond_2f

    .line 170
    :goto_21
    if-eqz v0, :cond_2a

    .line 171
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {p1, p2, v1}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;I)V

    .line 174
    :cond_2a
    return v0

    .line 159
    :cond_2b
    invoke-static {p2, v1}, LEn;->b(Landroid/text/Spannable;LEj;)Z

    goto :goto_1b

    .line 164
    :cond_2f
    const/4 v0, 0x1

    .line 165
    invoke-direct {p0, p2, v1}, LFM;->a(Landroid/text/Spannable;LEj;)I

    move-result v4

    if-lt v4, v3, :cond_12

    goto :goto_21
.end method

.method protected g(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 179
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 180
    invoke-static {p2, v1}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    .line 187
    :goto_a
    const/4 v0, 0x1

    return v0

    .line 182
    :cond_c
    invoke-static {p2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 184
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-static {p1, p2, v0}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_a
.end method

.method protected h(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 192
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 193
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v0

    invoke-static {p2, v0}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    .line 200
    :goto_d
    const/4 v0, 0x1

    return v0

    .line 195
    :cond_f
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v0

    invoke-static {p2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 197
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-static {p1, p2, v0}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_d
.end method

.method protected i(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 205
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    .line 206
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 207
    invoke-static {p2, v0}, LEn;->i(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 212
    :goto_e
    return v0

    .line 209
    :cond_f
    invoke-static {p2, v0}, LEn;->k(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 210
    iget-object v1, p0, LFM;->a:LGw;

    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {p1, p2, v1, v2}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;LGw;I)V

    goto :goto_e
.end method

.method protected j(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 218
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    .line 219
    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 220
    invoke-static {p2, v0}, LEn;->j(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 225
    :goto_e
    return v0

    .line 222
    :cond_f
    invoke-static {p2, v0}, LEn;->l(Landroid/text/Spannable;LEj;)Z

    move-result v0

    .line 223
    iget-object v1, p0, LFM;->a:LGw;

    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {p1, p2, v1, v2}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;LGw;I)V

    goto :goto_e
.end method

.method protected k(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 232
    iget-object v0, p0, LFM;->a:LGw;

    invoke-virtual {v0, p2}, LGw;->a(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v0, p0, LFM;->a:LGw;

    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    invoke-static {p2, v0, v1}, LEn;->a(Landroid/text/Spannable;LEo;Z)Z

    move-result v0

    .line 234
    iget-object v1, p0, LFM;->a:LGw;

    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {p1, p2, v1, v2}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;LGw;I)V

    .line 236
    return v0
.end method

.method protected l(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 242
    iget-object v0, p0, LFM;->a:LGw;

    invoke-virtual {v0, p2}, LGw;->a(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v0, p0, LFM;->a:LGw;

    invoke-static {p2}, LFM;->a(Landroid/text/Spannable;)Z

    move-result v1

    invoke-static {p2, v0, v1}, LEn;->b(Landroid/text/Spannable;LEo;Z)Z

    move-result v0

    .line 244
    iget-object v1, p0, LFM;->a:LGw;

    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {p1, p2, v1, v2}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;LGw;I)V

    .line 246
    return v0
.end method

.method protected m(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 251
    invoke-virtual {p0, p1, p2}, LFM;->i(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z

    move-result v0

    return v0
.end method

.method protected n(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 256
    invoke-virtual {p0, p1, p2}, LFM;->j(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)Z

    move-result v0

    return v0
.end method
