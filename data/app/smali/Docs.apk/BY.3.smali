.class public LBY;
.super LGE;
.source "KixImageSpan.java"

# interfaces
.implements LDl;
.implements LGC;


# instance fields
.field private final a:F

.field private final a:LAS;

.field private final a:LBh;

.field private final a:Ljava/lang/String;

.field private final a:Lxu;

.field private a:Z

.field private final b:F


# direct methods
.method public constructor <init>(LAS;Ljava/lang/String;FFLxu;LBh;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, LGE;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, LBY;->a:Z

    .line 53
    iput-object p1, p0, LBY;->a:LAS;

    .line 54
    iput-object p6, p0, LBY;->a:LBh;

    .line 55
    iput-object p2, p0, LBY;->a:Ljava/lang/String;

    .line 56
    iput-object p5, p0, LBY;->a:Lxu;

    .line 57
    iput p3, p0, LBY;->a:F

    .line 58
    iput p4, p0, LBY;->b:F

    .line 59
    return-void
.end method

.method private a()Landroid/util/Pair;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, LBY;->a:Lxu;

    invoke-interface {v0}, Lxu;->a()F

    move-result v0

    .line 122
    iget v1, p0, LBY;->a:F

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 123
    iget v2, p0, LBY;->b:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 125
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, LBY;->a()Landroid/util/Pair;

    move-result-object v2

    .line 75
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 76
    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 77
    iget-object v1, p0, LBY;->a:LAS;

    iget-object v2, p0, LBY;->a:Ljava/lang/String;

    iget-object v3, p0, LBY;->a:LBh;

    iget-boolean v4, p0, LBY;->a:Z

    invoke-virtual/range {v1 .. v6}, LAS;->a(Ljava/lang/String;LBh;ZII)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 79
    const/4 v2, 0x0

    iput-boolean v2, p0, LBY;->a:Z

    .line 81
    sub-int v2, p7, v6

    int-to-float v2, v2

    .line 82
    if-eqz v1, :cond_41

    .line 83
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 84
    invoke-virtual {p0}, LBY;->a()I

    move-result v3

    int-to-float v3, v3

    sub-float v3, p5, v3

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 85
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 86
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 87
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 98
    :goto_40
    return-void

    .line 89
    :cond_41
    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v1

    .line 90
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 91
    invoke-virtual/range {p9 .. p9}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    .line 92
    const v4, -0x777778

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {p0}, LBY;->a()I

    move-result v7

    int-to-float v7, v7

    sub-float v7, p5, v7

    int-to-float v5, v5

    add-float/2addr v5, p5

    invoke-virtual {p0}, LBY;->a()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v5, v8

    int-to-float v6, v6

    add-float/2addr v6, v2

    invoke-direct {v4, v7, v2, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 94
    const/high16 v2, 0x4100

    const/high16 v5, 0x4100

    move-object/from16 v0, p9

    invoke-virtual {p1, v4, v2, v5, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 95
    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 96
    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_40
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 102
    invoke-direct {p0}, LBY;->a()Landroid/util/Pair;

    move-result-object v1

    .line 103
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 104
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 106
    if-eqz p5, :cond_22

    .line 109
    neg-int v0, v0

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 110
    iput v3, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 113
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 114
    iput v3, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 117
    :cond_22
    return v2
.end method
