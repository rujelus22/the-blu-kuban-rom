.class public final LHW;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LIb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 31
    iput-object p1, p0, LHW;->a:LYD;

    .line 32
    const-class v0, LIb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LHW;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LHW;->a:LZb;

    .line 35
    return-void
.end method

.method static synthetic a(LHW;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LHW;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 82
    const-class v0, LIb;

    new-instance v1, LHX;

    invoke-direct {v1, p0}, LHX;-><init>(LHW;)V

    invoke-virtual {p0, v0, v1}, LHW;->a(Ljava/lang/Class;Laou;)V

    .line 90
    const-class v0, LIb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LHW;->a:LZb;

    invoke-virtual {p0, v0, v1}, LHW;->a(Laop;LZb;)V

    .line 91
    iget-object v0, p0, LHW;->a:LZb;

    new-instance v1, LHY;

    invoke-direct {v1, p0}, LHY;-><init>(LHW;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 102
    return-void
.end method

.method public a(LIb;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, LHW;->a:LYD;

    iget-object v0, v0, LYD;->a:LGL;

    iget-object v0, v0, LGL;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LHW;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGY;

    iput-object v0, p1, LIb;->a:LGY;

    .line 47
    iget-object v0, p0, LHW;->a:LYD;

    iget-object v0, v0, LYD;->a:LHt;

    iget-object v0, v0, LHt;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LHW;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;

    iput-object v0, p1, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;

    .line 53
    iget-object v0, p0, LHW;->a:LYD;

    iget-object v0, v0, LYD;->a:LHt;

    iget-object v0, v0, LHt;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LHW;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;

    iput-object v0, p1, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;

    .line 59
    iget-object v0, p0, LHW;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LHW;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, LIb;->a:Laoz;

    .line 65
    iget-object v0, p0, LHW;->a:LYD;

    iget-object v0, v0, LYD;->a:LGL;

    iget-object v0, v0, LGL;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LHW;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGJ;

    iput-object v0, p1, LIb;->a:LGJ;

    .line 71
    iget-object v0, p0, LHW;->a:LYD;

    iget-object v0, v0, LYD;->a:LHt;

    iget-object v0, v0, LHt;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LHW;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    iput-object v0, p1, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    .line 77
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 106
    return-void
.end method
