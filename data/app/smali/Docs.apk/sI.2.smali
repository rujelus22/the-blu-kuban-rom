.class public LsI;
.super Ljava/lang/Object;
.source "SharingFragment.java"

# interfaces
.implements LamE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LamE",
        "<",
        "LUq;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/SharingFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/SharingFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LUq;)V
    .registers 5
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->b(Lcom/google/android/apps/docs/editors/SharingFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 68
    iget-object v0, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/editors/SharingFragment;->a(Lcom/google/android/apps/docs/editors/SharingFragment;LUq;)LUq;

    .line 77
    iget-object v0, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Len;->sharing_message_saved:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 79
    iget-object v0, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a(Lcom/google/android/apps/docs/editors/SharingFragment;)V

    .line 80
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 51
    check-cast p1, LUq;

    invoke-virtual {p0, p1}, LsI;->a(LUq;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 54
    iget-object v0, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a(Lcom/google/android/apps/docs/editors/SharingFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 55
    instance-of v0, p1, LKw;

    if-eqz v0, :cond_24

    .line 56
    iget-object v0, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Len;->sharing_error:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 63
    :cond_23
    :goto_23
    return-void

    .line 58
    :cond_24
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_23

    .line 59
    const-string v0, "SharingFragment"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 60
    iget-object v0, p0, LsI;->a:Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Len;->sharing_error:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_23
.end method
