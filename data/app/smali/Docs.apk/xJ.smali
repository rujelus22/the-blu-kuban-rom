.class public LxJ;
.super Ljava/lang/Object;
.source "FontManagerImpl.java"

# interfaces
.implements LxI;


# annotations
.annotation runtime LaoJ;
.end annotation


# static fields
.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/content/res/AssetManager;

.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LxL;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LxJ;->b:Ljava/util/Map;

    .line 62
    invoke-static {}, LxK;->values()[LxK;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v2, :cond_21

    aget-object v3, v1, v0

    .line 63
    sget-object v4, LxJ;->b:Ljava/util/Map;

    invoke-virtual {v3}, LxK;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, LxK;->a()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 65
    :cond_21
    return-void
.end method

.method public constructor <init>(Landroid/content/res/AssetManager;)V
    .registers 3
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LxJ;->a:Ljava/util/Map;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, LxJ;->a:Z

    .line 77
    iput-object p1, p0, LxJ;->a:Landroid/content/res/AssetManager;

    .line 78
    return-void
.end method

.method private a()Ljava/util/Set;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, LxJ;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private a()V
    .registers 3

    .prologue
    .line 165
    iget-boolean v0, p0, LxJ;->a:Z

    if-nez v0, :cond_e

    .line 166
    new-instance v0, Ljava/io/File;

    const-string v1, "fonts"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, LxJ;->a(Ljava/io/File;)V

    .line 169
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, LxJ;->a:Z

    .line 170
    return-void
.end method

.method private a(Ljava/io/File;)V
    .registers 7
    .parameter

    .prologue
    .line 178
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LxJ;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 180
    if-eqz v1, :cond_1c

    .line 181
    array-length v0, v1

    if-nez v0, :cond_1d

    .line 184
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".ttf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 185
    invoke-direct {p0, p1}, LxJ;->b(Ljava/io/File;)V

    .line 193
    :cond_1c
    return-void

    .line 188
    :cond_1d
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1f
    if-ge v0, v2, :cond_1c

    aget-object v3, v1, v0

    .line 189
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v4}, LxJ;->a(Ljava/io/File;)V

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f
.end method

.method private a(Ljava/lang/String;)[Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 203
    :try_start_0
    iget-object v0, p0, LxJ;->a:Landroid/content/res/AssetManager;

    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 208
    :goto_6
    return-object v0

    .line 204
    :catch_7
    move-exception v0

    .line 205
    const-string v1, "FontManagerImpl"

    const-string v2, "Error while searching for fonts."

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 208
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private b(Ljava/io/File;)V
    .registers 5
    .parameter

    .prologue
    .line 218
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_36

    .line 221
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5f

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 223
    iget-object v1, p0, LxJ;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    .line 224
    new-instance v1, LxL;

    iget-object v2, p0, LxJ;->a:Landroid/content/res/AssetManager;

    invoke-direct {v1, v0, v2}, LxL;-><init>(Ljava/lang/String;Landroid/content/res/AssetManager;)V

    .line 225
    iget-object v2, p0, LxJ;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    :cond_26
    iget-object v1, p0, LxJ;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxL;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LxL;->a(Ljava/lang/String;)V

    .line 232
    :goto_35
    return-void

    .line 230
    :cond_36
    const-string v0, "FontManagerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid font path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_35
.end method


# virtual methods
.method public a()Lajm;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lajm",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0}, LxJ;->a()V

    .line 152
    new-instance v0, Lajo;

    invoke-direct {v0}, Lajo;-><init>()V

    .line 153
    invoke-direct {p0}, LxJ;->a()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajo;->a(Ljava/lang/Iterable;)Lajo;

    move-result-object v0

    iget-object v1, p0, LxJ;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajo;->a(Ljava/lang/Iterable;)Lajo;

    move-result-object v0

    invoke-virtual {v0}, Lajo;->a()Lajm;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/Integer;)Landroid/graphics/Typeface;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 127
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, LxJ;->a()V

    .line 129
    iget-object v0, p0, LxJ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxL;

    .line 130
    if-eqz v0, :cond_1d

    .line 131
    invoke-virtual {v0}, LxL;->a()Z

    move-result v1

    if-nez v1, :cond_17

    .line 132
    invoke-virtual {v0}, LxL;->a()V

    .line 135
    :cond_17
    invoke-virtual {v0, p2}, LxL;->a(Ljava/lang/Integer;)Landroid/graphics/Typeface;
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1f

    move-result-object v0

    .line 137
    :goto_1b
    monitor-exit p0

    return-object v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1b

    .line 127
    :catchall_1f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/Integer;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 84
    .line 85
    if-eqz p2, :cond_49

    sget-object v0, LxJ;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 86
    sget-object v0, LxJ;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 92
    :goto_12
    if-nez v0, :cond_5a

    .line 93
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    move-object v1, v0

    .line 96
    :goto_19
    const/4 v0, 0x0

    .line 97
    if-eqz v1, :cond_20

    .line 98
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 101
    :cond_20
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    or-int/2addr v2, v0

    .line 103
    if-nez v1, :cond_4e

    if-eqz p2, :cond_4e

    .line 104
    invoke-static {p2, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 113
    :goto_2d
    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v1, v2

    .line 114
    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_3c

    .line 115
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 118
    :cond_3c
    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_45

    .line 119
    const/high16 v1, -0x4180

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 122
    :cond_45
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 123
    return-void

    .line 88
    :cond_49
    invoke-virtual {p0, p2, p3}, LxJ;->a(Ljava/lang/String;Ljava/lang/Integer;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_12

    .line 105
    :cond_4e
    if-nez v1, :cond_55

    .line 106
    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_2d

    .line 108
    :cond_55
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_2d

    :cond_5a
    move-object v1, v0

    goto :goto_19
.end method
