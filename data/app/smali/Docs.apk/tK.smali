.class public final enum LtK;
.super Ljava/lang/Enum;
.source "EditCommentFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LtK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LtK;

.field private static final synthetic a:[LtK;

.field public static final enum b:LtK;

.field public static final enum c:LtK;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-instance v0, LtK;

    const-string v1, "REPLY"

    const-string v2, "reply"

    invoke-direct {v0, v1, v3, v2}, LtK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LtK;->a:LtK;

    .line 44
    new-instance v0, LtK;

    const-string v1, "EDIT"

    const-string v2, "edit"

    invoke-direct {v0, v1, v4, v2}, LtK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LtK;->b:LtK;

    .line 45
    new-instance v0, LtK;

    const-string v1, "NEW_DISCUSSION"

    const-string v2, "new"

    invoke-direct {v0, v1, v5, v2}, LtK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LtK;->c:LtK;

    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [LtK;

    sget-object v1, LtK;->a:LtK;

    aput-object v1, v0, v3

    sget-object v1, LtK;->b:LtK;

    aput-object v1, v0, v4

    sget-object v1, LtK;->c:LtK;

    aput-object v1, v0, v5

    sput-object v0, LtK;->a:[LtK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, LtK;->a:Ljava/lang/String;

    .line 51
    invoke-static {}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LtK;
    .registers 2
    .parameter

    .prologue
    .line 42
    const-class v0, LtK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LtK;

    return-object v0
.end method

.method public static values()[LtK;
    .registers 1

    .prologue
    .line 42
    sget-object v0, LtK;->a:[LtK;

    invoke-virtual {v0}, [LtK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LtK;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, LtK;->a:Ljava/lang/String;

    return-object v0
.end method
