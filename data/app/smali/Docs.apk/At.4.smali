.class public LAt;
.super LAw;
.source "Autogen.java"


# instance fields
.field private final a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LDb;LAb;Lzd;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "LAb;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, LAw;-><init>(LDb;LAb;Lzd;Lxu;)V

    .line 37
    new-instance v0, LDA;

    invoke-direct {v0}, LDA;-><init>()V

    iput-object v0, p0, LAt;->a:LDA;

    .line 38
    iget-object v0, p0, LAt;->a:LDA;

    invoke-virtual {v0, p0}, LDA;->a(Ljava/lang/Object;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected a(I)I
    .registers 3
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->f()I

    move-result v0

    if-ge p1, v0, :cond_f

    iget-object v0, p0, LAt;->a:LDA;

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, LAt;->a:LDA;

    invoke-virtual {v0}, LDA;->c()I

    move-result v0

    goto :goto_e
.end method

.method public a()LDA;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, LAt;->a:LDA;

    return-object v0
.end method

.method public bridge synthetic a()LDG;
    .registers 2

    .prologue
    .line 25
    invoke-virtual {p0}, LAt;->a()LDA;

    move-result-object v0

    return-object v0
.end method

.method public a(IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, LAt;->a:LDA;

    add-int/lit8 v1, p1, -0x1

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0, v1, v2}, LDA;->a(II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 87
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    invoke-interface {v0, p1, p2, p3}, LAx;->a(IILjava/util/List;)V

    goto :goto_e

    .line 89
    :cond_24
    return-void
.end method

.method public a(Lwj;Lza;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 50
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->a()I

    move-result v0

    if-nez v0, :cond_34

    .line 51
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0, v1}, LDb;->a(I)V

    .line 52
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    invoke-interface {p2, v0, v1}, Lza;->a(II)V

    .line 57
    :goto_18
    add-int/lit8 v0, p3, -0x1

    invoke-super {p0, p1, p2, v0}, LAw;->a(Lwj;Lza;I)V

    .line 60
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->b()I

    move-result v0

    if-nez v0, :cond_3e

    .line 61
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0, v1}, LDb;->b(I)V

    .line 62
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->g()I

    move-result v0

    invoke-interface {p2, v0, v1}, Lza;->a(II)V

    .line 66
    :goto_33
    return-void

    .line 54
    :cond_34
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    invoke-interface {p2, v0, v2}, Lza;->a(II)V

    goto :goto_18

    .line 64
    :cond_3e
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->g()I

    move-result v0

    invoke-interface {p2, v0, v2}, Lza;->a(II)V

    goto :goto_33
.end method

.method public a(IILCh;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, LAt;->a:LDA;

    invoke-virtual {v0, p1, p2}, LDA;->a(II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 71
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    .line 72
    invoke-interface {v0, p1, p2, p3}, LAx;->a(IILCh;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 73
    const/4 v0, 0x0

    .line 76
    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x1

    goto :goto_23
.end method

.method protected b(I)I
    .registers 3
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, LAt;->a:LDA;

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    if-ge p1, v0, :cond_f

    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, LAt;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    goto :goto_e
.end method
