.class public Lwa;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements LvZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "LvZ;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3119
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 3120
    return-void
.end method

.method static a(Lvw;J)Lwa;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3115
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lwa;

    invoke-direct {v0, p0, p1, p2}, Lwa;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a(D)V
    .registers 5
    .parameter

    .prologue
    .line 3343
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3344
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JD)V

    .line 3345
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 3189
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3190
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JI)V

    .line 3191
    return-void
.end method

.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3147
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3148
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JII)V

    .line 3149
    return-void
.end method

.method public a(IILjava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3203
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3204
    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JIILjava/lang/String;)V

    .line 3205
    return-void
.end method

.method public a(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3133
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3134
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JIZ)V

    .line 3135
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 3161
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3162
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JLjava/lang/String;)V

    .line 3163
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 3245
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3246
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JZ)V

    .line 3247
    return-void
.end method

.method public b(I)V
    .registers 4
    .parameter

    .prologue
    .line 3357
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3358
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)V

    .line 3359
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 3329
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3330
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JLjava/lang/String;)V

    .line 3331
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 3175
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3176
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(J)V

    .line 3177
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 3371
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3372
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JLjava/lang/String;)V

    .line 3373
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 3217
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3218
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->e(J)V

    .line 3219
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 3385
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3386
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(JLjava/lang/String;)V

    .line 3387
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 3231
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3232
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->f(J)V

    .line 3233
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 3413
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3414
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->e(JLjava/lang/String;)V

    .line 3415
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 3259
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3260
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->g(J)V

    .line 3261
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 3273
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3274
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->h(J)V

    .line 3275
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 3287
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3288
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->i(J)V

    .line 3289
    return-void
.end method

.method public i()V
    .registers 3

    .prologue
    .line 3301
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3302
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->j(J)V

    .line 3303
    return-void
.end method

.method public j()V
    .registers 3

    .prologue
    .line 3315
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3316
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->k(J)V

    .line 3317
    return-void
.end method

.method public k()V
    .registers 3

    .prologue
    .line 3399
    invoke-virtual {p0}, Lwa;->a()J

    move-result-wide v0

    .line 3400
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->l(J)V

    .line 3401
    return-void
.end method
