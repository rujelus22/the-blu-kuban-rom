.class LfN;
.super Ljava/lang/Object;
.source "DocsPreferencesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

.field final synthetic a:LfM;


# direct methods
.method constructor <init>(LfM;Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 479
    iput-object p1, p0, LfN;->a:LfM;

    iput-object p2, p0, LfN;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 482
    iget-object v0, p0, LfN;->a:LfM;

    iget-object v0, v0, LfM;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->g(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;

    move-result-object v0

    iget-object v1, p0, LfN;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 483
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 484
    iget-object v1, p0, LfN;->a:LfM;

    iget-object v1, v1, LfM;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Len;->terms_of_service_url_drivev2:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 486
    iget-object v1, p0, LfN;->a:LfM;

    iget-object v1, v1, LfM;->a:Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->startActivity(Landroid/content/Intent;)V

    .line 487
    return-void
.end method
