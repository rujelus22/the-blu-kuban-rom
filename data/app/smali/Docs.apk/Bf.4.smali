.class LBf;
.super Ljava/lang/Object;
.source "ImageCache.java"


# instance fields
.field private a:I

.field final synthetic a:LBd;

.field private final a:Landroid/graphics/drawable/BitmapDrawable;

.field private final a:Z

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(LBd;Landroid/graphics/drawable/BitmapDrawable;IIZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, LBf;->a:LBd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iput-object p2, p0, LBf;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 58
    invoke-static {p1}, LBd;->a(LBd;)I

    move-result v0

    iput v0, p0, LBf;->a:I

    .line 59
    iput p3, p0, LBf;->b:I

    .line 60
    iput p4, p0, LBf;->c:I

    .line 61
    iput-boolean p5, p0, LBf;->a:Z

    .line 62
    return-void
.end method

.method static synthetic a(LBf;)I
    .registers 2
    .parameter

    .prologue
    .line 53
    iget v0, p0, LBf;->b:I

    return v0
.end method

.method static synthetic a(LBf;)Landroid/graphics/drawable/BitmapDrawable;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, LBf;->a:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method static synthetic a(LBf;)Z
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-boolean v0, p0, LBf;->a:Z

    return v0
.end method

.method static synthetic b(LBf;)I
    .registers 2
    .parameter

    .prologue
    .line 53
    iget v0, p0, LBf;->c:I

    return v0
.end method

.method static synthetic c(LBf;)I
    .registers 2
    .parameter

    .prologue
    .line 53
    iget v0, p0, LBf;->a:I

    return v0
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 65
    iget-object v0, p0, LBf;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 66
    if-nez v0, :cond_11

    .line 67
    const-string v0, "ImageCache"

    const-string v1, "Null bitmap"

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/4 v0, 0x0

    .line 70
    :goto_10
    return v0

    :cond_11
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    mul-int/2addr v0, v1

    goto :goto_10
.end method

.method public a()Landroid/graphics/drawable/BitmapDrawable;
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, LBf;->a:LBd;

    invoke-static {v0}, LBd;->a(LBd;)I

    move-result v0

    iput v0, p0, LBf;->a:I

    .line 75
    iget-object v0, p0, LBf;->a:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method
