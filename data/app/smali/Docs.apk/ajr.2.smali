.class final Lajr;
.super Laji;
.source "ImmutableSortedAsList.java"

# interfaces
.implements LalU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Laji",
        "<TE;>;",
        "LalU",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final transient a:Laji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laji",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final transient a:Lajz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajz",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lajz;Laji;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lajz",
            "<TE;>;",
            "Laji",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Laji;-><init>()V

    .line 36
    iput-object p1, p0, Lajr;->a:Lajz;

    .line 37
    iput-object p2, p0, Lajr;->a:Laji;

    .line 38
    return-void
.end method


# virtual methods
.method public a(II)Laji;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lajr;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lagu;->a(III)V

    .line 63
    if-ne p1, p2, :cond_e

    invoke-static {}, Laji;->c()Laji;

    move-result-object v0

    :goto_d
    return-object v0

    :cond_e
    new-instance v0, LalK;

    iget-object v1, p0, Lajr;->a:Laji;

    invoke-virtual {v1, p1, p2}, Laji;->a(II)Laji;

    move-result-object v1

    iget-object v2, p0, Lajr;->a:Lajz;

    invoke-virtual {v2}, Lajz;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LalK;-><init>(Laji;Ljava/util/Comparator;)V

    invoke-virtual {v0}, LalK;->a()Laji;

    move-result-object v0

    goto :goto_d
.end method

.method public a()Laml;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laml",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0}, Laji;->a()Laml;

    move-result-object v0

    return-object v0
.end method

.method public a()Lamm;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lamm",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0}, Laji;->a()Lamm;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lamm;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lamm",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0, p1}, Laji;->a(I)Lamm;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0}, Laji;->a()Z

    move-result v0

    return v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lajr;->a:Lajz;

    invoke-virtual {v0}, Lajz;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lajr;->a:Lajz;

    invoke-virtual {v0, p1}, Lajz;->a(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0, p1}, Laji;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0, p1}, Laji;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0}, Laji;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lajr;->a:Lajz;

    invoke-virtual {v0, p1}, Lajz;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lajr;->a()Laml;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lajr;->a:Lajz;

    invoke-virtual {v0, p1}, Lajz;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lajr;->a()Lamm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .registers 3
    .parameter

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lajr;->a(I)Lamm;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lajr;->a:Laji;

    invoke-virtual {v0}, Laji;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lajr;->a(II)Laji;

    move-result-object v0

    return-object v0
.end method
