.class public Lyh;
.super LMA;
.source "KixEditText.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/kix/KixEditText;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V
    .registers 2
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-direct {p0}, LMA;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/editors/kix/KixEditText;Lyf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lyh;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_16

    .line 112
    iget-object v0, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)Landroid/widget/Scroller;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 114
    :cond_16
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)Landroid/widget/Scroller;

    move-result-object v0

    if-eqz v0, :cond_5e

    .line 120
    iget-object v0, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v1, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()LEj;

    move-result-object v1

    invoke-static {v0, v1}, LGt;->a(Lcom/google/android/apps/docs/editors/text/TextView;LEj;)Landroid/util/Pair;

    move-result-object v6

    .line 123
    iget-object v0, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v1, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()LEj;

    move-result-object v1

    invoke-static {v0, v1}, LGt;->b(Lcom/google/android/apps/docs/editors/text/TextView;LEj;)Landroid/util/Pair;

    move-result-object v8

    .line 126
    iget-object v0, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)Landroid/widget/Scroller;

    move-result-object v0

    iget-object v1, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getScrollX()I

    move-result v1

    iget-object v2, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getScrollY()I

    move-result v2

    float-to-int v3, p3

    neg-int v3, v3

    float-to-int v4, p4

    neg-int v4, v4

    iget-object v5, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 130
    iget-object v0, p0, Lyh;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->invalidate()V

    .line 132
    :cond_5e
    const/4 v0, 0x1

    return v0
.end method
