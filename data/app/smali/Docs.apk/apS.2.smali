.class LapS;
.super LaoY;
.source "InjectorImpl.java"

# interfaces
.implements LarD;
.implements Larn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LaoY",
        "<",
        "Laoz",
        "<TT;>;>;",
        "LarD",
        "<",
        "Laoz",
        "<TT;>;>;",
        "Larn;"
    }
.end annotation


# instance fields
.field final a:LaoY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaoY",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LapL;Laop;Laof;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "Laop",
            "<",
            "Laoz",
            "<TT;>;>;",
            "Laof",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 353
    invoke-interface {p3}, Laof;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-static {p3}, LapS;->a(Laof;)LapY;

    move-result-object v4

    sget-object v5, LaqC;->a:LaqC;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LaoY;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;)V

    .line 355
    check-cast p3, LaoY;

    iput-object p3, p0, LapS;->a:LaoY;

    .line 356
    return-void
.end method

.method static a(Laof;)LapY;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laof",
            "<TT;>;)",
            "LapY",
            "<",
            "Laoz",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 359
    invoke-interface {p0}, Laof;->a()Laoz;

    move-result-object v0

    .line 360
    new-instance v1, LapT;

    invoke-direct {v1, v0}, LapT;-><init>(Laoz;)V

    return-object v1
.end method


# virtual methods
.method public b()Laop;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laop",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, LapS;->a:LaoY;

    invoke-virtual {v0}, LaoY;->a()Laop;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p0}, LapS;->b()Laop;

    move-result-object v0

    invoke-static {v0}, Larg;->a(Laop;)Larg;

    move-result-object v0

    invoke-static {v0}, Lajm;->a(Ljava/lang/Object;)Lajm;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 392
    instance-of v1, p1, LapS;

    if-eqz v1, :cond_2e

    .line 393
    check-cast p1, LapS;

    .line 394
    invoke-virtual {p0}, LapS;->a()Laop;

    move-result-object v1

    invoke-virtual {p1}, LapS;->a()Laop;

    move-result-object v2

    invoke-virtual {v1, v2}, Laop;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    invoke-virtual {p0}, LapS;->a()LaqC;

    move-result-object v1

    invoke-virtual {p1}, LapS;->a()LaqC;

    move-result-object v2

    invoke-virtual {v1, v2}, LaqC;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, LapS;->a:LaoY;

    iget-object v2, p1, LapS;->a:LaoY;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/4 v0, 0x1

    .line 398
    :cond_2e
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 404
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, LapS;->a()Laop;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, LapS;->a()LaqC;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LapS;->a:LaoY;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 380
    const-class v0, LarD;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {p0}, LapS;->a()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "providedKey"

    invoke-virtual {p0}, LapS;->b()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
