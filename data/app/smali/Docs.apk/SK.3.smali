.class public LSK;
.super Ljava/lang/Object;
.source "ExtraTextDataSource.java"

# interfaces
.implements LSG;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/content/Context;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LSK;->b:Ljava/lang/String;

    .line 31
    if-nez p1, :cond_16

    .line 32
    iget-object v0, p0, LSK;->b:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-static {v0, p3, v1}, LSK;->a(Ljava/lang/String;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LSK;->a:Ljava/lang/String;

    .line 36
    :goto_15
    return-void

    .line 34
    :cond_16
    iput-object p1, p0, LSK;->a:Ljava/lang/String;

    goto :goto_15
.end method

.method static a(Ljava/lang/String;Landroid/content/Context;I)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    sget-object v0, LafP;->k:LafP;

    sget-object v1, LafP;->a:LafP;

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, LafP;->a()LafP;

    move-result-object v0

    invoke-virtual {v0, p0}, LafP;->a(Ljava/lang/CharSequence;)I

    move-result v0

    .line 73
    if-gez v0, :cond_2c

    .line 74
    sget v0, Len;->upload_untitled_file_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 87
    :goto_18
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 76
    :cond_2c
    add-int v2, p2, v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 77
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 78
    const/16 v3, 0x20

    invoke-virtual {v1, v0, v3}, LafP;->a(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_51

    .line 80
    invoke-virtual {v1, v0}, LafP;->b(Ljava/lang/CharSequence;)I

    move-result v2

    .line 81
    if-ltz v2, :cond_51

    .line 82
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 85
    :cond_51
    invoke-virtual {v1, v0}, LafP;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_18
.end method


# virtual methods
.method public a(I)Landroid/graphics/Bitmap;
    .registers 3
    .parameter

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, LSK;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(LrM;)LrK;
    .registers 3
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, LSK;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, LrM;->b(Ljava/lang/String;)LrM;

    .line 64
    invoke-virtual {p1}, LrM;->a()LrK;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 1

    .prologue
    .line 54
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    const-string v0, "text/plain"

    return-object v0
.end method
