.class public LyZ;
.super Ljava/lang/Object;
.source "KixWebViewOpener.java"


# instance fields
.field private final a:LKS;

.field private final a:Landroid/content/Context;

.field private a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LKS;)V
    .registers 4
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, LyZ;->a:Ljava/lang/Boolean;

    .line 45
    iput-object p1, p0, LyZ;->a:Landroid/content/Context;

    .line 46
    iput-object p2, p0, LyZ;->a:LKS;

    .line 47
    invoke-direct {p0}, LyZ;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LyZ;->a:Ljava/lang/Boolean;

    .line 48
    return-void
.end method

.method private a(Landroid/net/Uri;)Z
    .registers 6
    .parameter

    .prologue
    .line 99
    invoke-static {p1}, LoY;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 100
    iget-object v1, p0, LyZ;->a:LKS;

    const-string v2, "kixWebViewUrlPattern"

    const-string v3, "/document/d/[^/]*/edit.*"

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b()Z
    .registers 5

    .prologue
    .line 51
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iget-object v1, p0, LyZ;->a:LKS;

    const-string v2, "kixWebViewCutoffVersion"

    const v3, 0x7fffffff

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;I)I

    move-result v1

    if-ge v0, v1, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private c()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 56
    iget-object v1, p0, LyZ;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-object v1, p0, LyZ;->a:LKS;

    const-string v2, "enableKixWebView"

    invoke-interface {v1, v2, v0}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-direct {p0}, LyZ;->b()Z

    move-result v1

    if-eqz v1, :cond_1e

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    const/4 v0, 0x0

    .line 81
    invoke-virtual {p0}, LyZ;->a()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 82
    invoke-direct {p0, p1}, LyZ;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 83
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const-string v3, "/edit"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/native/webview"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, LyZ;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v1, v0, p2, p3}, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 91
    :cond_39
    :goto_39
    if-nez v0, :cond_41

    .line 92
    iget-object v0, p0, LyZ;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 94
    :cond_41
    const-string v1, "resourceId"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    iget-object v1, p0, LyZ;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 96
    return-void

    .line 88
    :cond_4c
    const-string v1, "KixWebViewOpener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to modify URI to use HiFi WebView "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_39
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, LyZ;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    .line 66
    invoke-direct {p0}, LyZ;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LyZ;->a:Ljava/lang/Boolean;

    .line 68
    :cond_e
    iget-object v0, p0, LyZ;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
