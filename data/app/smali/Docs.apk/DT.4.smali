.class public LDT;
.super Ljava/lang/Object;
.source "CanvasTextRenderFlags.java"

# interfaces
.implements LFe;


# static fields
.field private static a:I

.field private static final a:Z

.field private static final b:I


# instance fields
.field private final a:Landroid/graphics/Canvas;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0xb

    .line 18
    sput v1, LDT;->a:I

    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_1b

    .line 38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_16

    .line 39
    const/16 v0, 0x100

    sput v0, LDT;->b:I

    .line 43
    :goto_12
    const/4 v0, 0x1

    sput-boolean v0, LDT;->a:Z

    .line 48
    :goto_15
    return-void

    .line 41
    :cond_16
    const/16 v0, 0x80

    sput v0, LDT;->b:I

    goto :goto_12

    .line 45
    :cond_1b
    const/4 v0, 0x0

    sput-boolean v0, LDT;->a:Z

    .line 46
    const v0, 0x7fffffff

    sput v0, LDT;->b:I

    goto :goto_15
.end method

.method public constructor <init>(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, LDT;->a:Landroid/graphics/Canvas;

    .line 59
    return-void
.end method

.method public static a(I)V
    .registers 1
    .parameter

    .prologue
    .line 25
    sput p0, LDT;->a:I

    .line 26
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Paint;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    sget v3, LDT;->a:I

    if-ge v2, v3, :cond_9

    .line 78
    :cond_8
    :goto_8
    return v0

    .line 69
    :cond_9
    sget-boolean v2, LDT;->a:Z

    if-eqz v2, :cond_2a

    iget-object v2, p0, LDT;->a:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 70
    if-nez p1, :cond_1b

    .line 71
    invoke-virtual {p2}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object p1

    .line 74
    :cond_1b
    iget v2, p1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v3, p1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 75
    sget v3, LDT;->b:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_8

    move v0, v1

    goto :goto_8

    :cond_2a
    move v0, v1

    .line 78
    goto :goto_8
.end method
