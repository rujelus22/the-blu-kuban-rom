.class public LEm;
.super Ljava/lang/Object;
.source "LayoutChangeBatcher.java"

# interfaces
.implements LDS;
.implements Landroid/text/SpanWatcher;
.implements Landroid/text/TextWatcher;


# instance fields
.field private a:I

.field private final a:LEQ;

.field private a:Z

.field private b:I

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(LEQ;)V
    .registers 3
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, LEm;->a:Z

    .line 28
    iput-object p1, p0, LEm;->a:LEQ;

    .line 29
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 46
    iget-boolean v0, p0, LEm;->a:Z

    if-eqz v0, :cond_8

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, LEm;->b:Z

    .line 51
    :goto_7
    return-void

    .line 49
    :cond_8
    iget-object v0, p0, LEm;->a:LEQ;

    invoke-interface {v0}, LEQ;->a()V

    goto :goto_7
.end method

.method private a(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, LEm;->a:Z

    if-eqz v0, :cond_26

    .line 33
    iget v0, p0, LEm;->a:I

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LEm;->a:I

    .line 34
    iget v0, p0, LEm;->b:I

    add-int v1, p2, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LEm;->b:I

    .line 35
    sub-int v0, p4, p3

    .line 36
    iget v1, p0, LEm;->b:I

    add-int/2addr v1, v0

    iput v1, p0, LEm;->b:I

    .line 37
    iget v1, p0, LEm;->c:I

    add-int/2addr v0, v1

    iput v0, p0, LEm;->c:I

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, LEm;->b:Z

    .line 43
    :goto_25
    return-void

    .line 40
    :cond_26
    iget-object v0, p0, LEm;->a:LEQ;

    invoke-interface {v0, p1, p2, p3, p4}, LEQ;->a(Ljava/lang/CharSequence;III)V

    .line 41
    iget-object v0, p0, LEm;->a:LEQ;

    invoke-interface {v0}, LEQ;->a()V

    goto :goto_25
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, LEm;->a:Z

    .line 97
    const v0, 0x7fffffff

    iput v0, p0, LEm;->a:I

    .line 98
    iput v1, p0, LEm;->b:I

    .line 99
    iput v1, p0, LEm;->c:I

    .line 100
    iput-boolean v1, p0, LEm;->b:Z

    .line 101
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter

    .prologue
    .line 64
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .registers 6
    .parameter

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, LEm;->a:Z

    .line 106
    iget v0, p0, LEm;->b:I

    iget v1, p0, LEm;->a:I

    sub-int/2addr v0, v1

    .line 107
    iget v1, p0, LEm;->c:I

    sub-int v1, v0, v1

    .line 108
    iget v2, p0, LEm;->b:I

    iget v3, p0, LEm;->a:I

    if-lt v2, v3, :cond_19

    .line 109
    iget-object v2, p0, LEm;->a:LEQ;

    iget v3, p0, LEm;->a:I

    invoke-interface {v2, p1, v3, v1, v0}, LEQ;->a(Ljava/lang/CharSequence;III)V

    .line 111
    :cond_19
    iget-boolean v0, p0, LEm;->b:Z

    if-eqz v0, :cond_22

    .line 112
    iget-object v0, p0, LEm;->a:LEQ;

    invoke-interface {v0}, LEQ;->a()V

    .line 114
    :cond_22
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    return-void
.end method

.method public onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 68
    instance-of v0, p2, Landroid/text/style/UpdateLayout;

    if-eqz v0, :cond_c

    .line 69
    sub-int v0, p4, p3

    sub-int v1, p4, p3

    invoke-direct {p0, p1, p3, v0, v1}, LEm;->a(Ljava/lang/CharSequence;III)V

    .line 73
    :goto_b
    return-void

    .line 71
    :cond_c
    invoke-direct {p0}, LEm;->a()V

    goto :goto_b
.end method

.method public onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    instance-of v0, p2, Landroid/text/style/UpdateLayout;

    if-eqz v0, :cond_13

    .line 87
    sub-int v0, p4, p3

    sub-int v1, p4, p3

    invoke-direct {p0, p1, p3, v0, v1}, LEm;->a(Ljava/lang/CharSequence;III)V

    .line 88
    sub-int v0, p6, p5

    sub-int v1, p6, p5

    invoke-direct {p0, p1, p5, v0, v1}, LEm;->a(Ljava/lang/CharSequence;III)V

    .line 92
    :goto_12
    return-void

    .line 90
    :cond_13
    invoke-direct {p0}, LEm;->a()V

    goto :goto_12
.end method

.method public onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 77
    instance-of v0, p2, Landroid/text/style/UpdateLayout;

    if-eqz v0, :cond_c

    .line 78
    sub-int v0, p4, p3

    sub-int v1, p4, p3

    invoke-direct {p0, p1, p3, v0, v1}, LEm;->a(Ljava/lang/CharSequence;III)V

    .line 82
    :goto_b
    return-void

    .line 80
    :cond_c
    invoke-direct {p0}, LEm;->a()V

    goto :goto_b
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, LEm;->a(Ljava/lang/CharSequence;III)V

    .line 60
    return-void
.end method
