.class public Lwk;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lwj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lwj;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3644
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 3645
    return-void
.end method

.method static a(Lvw;J)Lwk;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3640
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lwk;

    invoke-direct {v0, p0, p1, p2}, Lwk;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 3658
    invoke-virtual {p0}, Lwk;->a()J

    move-result-wide v0

    .line 3659
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(J)I

    move-result v0

    .line 3662
    return v0
.end method

.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3726
    invoke-virtual {p0}, Lwk;->a()J

    move-result-wide v0

    .line 3727
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JII)V

    .line 3728
    return-void
.end method

.method public b()I
    .registers 3

    .prologue
    .line 3675
    invoke-virtual {p0}, Lwk;->a()J

    move-result-wide v0

    .line 3676
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->e(J)I

    move-result v0

    .line 3679
    return v0
.end method

.method public b(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3740
    invoke-virtual {p0}, Lwk;->a()J

    move-result-wide v0

    .line 3741
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(JII)V

    .line 3742
    return-void
.end method

.method public c()I
    .registers 3

    .prologue
    .line 3692
    invoke-virtual {p0}, Lwk;->a()J

    move-result-wide v0

    .line 3693
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->f(J)I

    move-result v0

    .line 3696
    return v0
.end method

.method public d()I
    .registers 3

    .prologue
    .line 3709
    invoke-virtual {p0}, Lwk;->a()J

    move-result-wide v0

    .line 3710
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->g(J)I

    move-result v0

    .line 3713
    return v0
.end method
