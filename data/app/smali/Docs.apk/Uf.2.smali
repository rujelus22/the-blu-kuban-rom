.class public LUf;
.super Ljava/lang/Object;
.source "ModifySharingActivity.java"

# interfaces
.implements LamE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LamE",
        "<",
        "LUq;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 279
    iput-object p1, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LUq;)V
    .registers 4
    .parameter

    .prologue
    .line 294
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    iget-object v1, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 295
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;LUq;)LUq;

    .line 296
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LZM;

    invoke-interface {v0}, LZM;->a()V

    .line 297
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget-object v1, LUi;->c:LUi;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;LUi;)LUi;

    .line 299
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 301
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 302
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 279
    check-cast p1, LUq;

    invoke-virtual {p0, p1}, LUf;->a(LUq;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 5
    .parameter

    .prologue
    .line 282
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    iget-object v1, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 283
    instance-of v0, p1, LKw;

    if-eqz v0, :cond_1a

    .line 284
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Len;->sharing_list_offline:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    .line 289
    :cond_14
    :goto_14
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->finish()V

    .line 290
    return-void

    .line 285
    :cond_1a
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_14

    .line 286
    const-string v0, "ModifySharingActivity"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 287
    iget-object v0, p0, LUf;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Len;->sharing_error:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    goto :goto_14
.end method
