.class public Lbt;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoCompat.java"


# static fields
.field private static final a:Lbv;


# instance fields
.field private final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 704
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_e

    .line 705
    new-instance v0, Lbw;

    invoke-direct {v0}, Lbw;-><init>()V

    sput-object v0, Lbt;->a:Lbv;

    .line 711
    :goto_d
    return-void

    .line 706
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1c

    .line 707
    new-instance v0, Lbu;

    invoke-direct {v0}, Lbu;-><init>()V

    sput-object v0, Lbt;->a:Lbv;

    goto :goto_d

    .line 709
    :cond_1c
    new-instance v0, Lbx;

    invoke-direct {v0}, Lbx;-><init>()V

    sput-object v0, Lbt;->a:Lbv;

    goto :goto_d
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 931
    iput-object p1, p0, Lbt;->a:Ljava/lang/Object;

    .line 932
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 938
    iget-object v0, p0, Lbt;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 1145
    sget-object v0, Lbt;->a:Lbv;

    iget-object v1, p0, Lbt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Lbv;->a(Ljava/lang/Object;I)V

    .line 1146
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter

    .prologue
    .line 1641
    sget-object v0, Lbt;->a:Lbv;

    iget-object v1, p0, Lbt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Lbv;->b(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 1642
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1448
    sget-object v0, Lbt;->a:Lbv;

    iget-object v1, p0, Lbt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Lbv;->b(Ljava/lang/Object;Z)V

    .line 1449
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter

    .prologue
    .line 1665
    sget-object v0, Lbt;->a:Lbv;

    iget-object v1, p0, Lbt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Lbv;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 1666
    return-void
.end method

.method public b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1617
    sget-object v0, Lbt;->a:Lbv;

    iget-object v1, p0, Lbt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Lbv;->a(Ljava/lang/Object;Z)V

    .line 1618
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1734
    if-ne p0, p1, :cond_5

    .line 1751
    :cond_4
    :goto_4
    return v0

    .line 1737
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 1738
    goto :goto_4

    .line 1740
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 1741
    goto :goto_4

    .line 1743
    :cond_15
    check-cast p1, Lbt;

    .line 1744
    iget-object v2, p0, Lbt;->a:Ljava/lang/Object;

    if-nez v2, :cond_21

    .line 1745
    iget-object v2, p1, Lbt;->a:Ljava/lang/Object;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1746
    goto :goto_4

    .line 1748
    :cond_21
    iget-object v2, p0, Lbt;->a:Ljava/lang/Object;

    iget-object v3, p1, Lbt;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1749
    goto :goto_4
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 1729
    iget-object v0, p0, Lbt;->a:Ljava/lang/Object;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lbt;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_5
.end method
