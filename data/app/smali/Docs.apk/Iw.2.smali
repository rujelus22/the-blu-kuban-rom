.class public final enum LIw;
.super Ljava/lang/Enum;
.source "TrixSheetEdgeView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LIw;

.field private static final synthetic a:[LIw;

.field public static final enum b:LIw;

.field public static final enum c:LIw;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, LIw;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v2}, LIw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIw;->a:LIw;

    new-instance v0, LIw;

    const-string v1, "ACTIVE_LEFT"

    invoke-direct {v0, v1, v3}, LIw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIw;->b:LIw;

    new-instance v0, LIw;

    const-string v1, "ACTIVE_RIGHT"

    invoke-direct {v0, v1, v4}, LIw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIw;->c:LIw;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [LIw;

    sget-object v1, LIw;->a:LIw;

    aput-object v1, v0, v2

    sget-object v1, LIw;->b:LIw;

    aput-object v1, v0, v3

    sget-object v1, LIw;->c:LIw;

    aput-object v1, v0, v4

    sput-object v0, LIw;->a:[LIw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LIw;
    .registers 2
    .parameter

    .prologue
    .line 31
    const-class v0, LIw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIw;

    return-object v0
.end method

.method public static values()[LIw;
    .registers 1

    .prologue
    .line 31
    sget-object v0, LIw;->a:[LIw;

    invoke-virtual {v0}, [LIw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIw;

    return-object v0
.end method
