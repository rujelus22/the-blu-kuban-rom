.class public Lxb;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lxa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lxa;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4793
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 4794
    return-void
.end method

.method static a(Lvw;J)Lxb;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4789
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lxb;

    invoke-direct {v0, p0, p1, p2}, Lxb;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()Lvm;
    .registers 4

    .prologue
    .line 4824
    invoke-virtual {p0}, Lxb;->a()J

    move-result-wide v0

    .line 4825
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->g(J)J

    move-result-wide v1

    .line 4828
    iget-object v0, p0, Lxb;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lvn;->a(Lvw;J)Lvn;

    move-result-object v0

    return-object v0
.end method

.method public a()Lvz;
    .registers 4

    .prologue
    .line 4886
    invoke-virtual {p0}, Lxb;->a()J

    move-result-wide v0

    .line 4887
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->h(J)J

    move-result-wide v1

    .line 4890
    iget-object v0, p0, Lxb;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, LvA;->a(Lvw;J)LvA;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lwj;
    .registers 5
    .parameter

    .prologue
    .line 4807
    invoke-virtual {p0}, Lxb;->a()J

    move-result-wide v0

    .line 4808
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JLjava/lang/String;)J

    move-result-wide v1

    .line 4811
    iget-object v0, p0, Lxb;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lwk;->a(Lvw;J)Lwk;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 5

    .prologue
    .line 4876
    iget-object v0, p0, Lxb;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->w(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4877
    iget-object v0, p0, Lxb;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, Lxb;->a()J

    move-result-wide v1

    const/16 v3, 0x78

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->w(Lvw;Z)Z

    .line 4880
    iget-object v0, p0, Lxb;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->x(Lvw;Z)Z

    .line 4882
    :cond_23
    iget-object v0, p0, Lxb;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->x(Lvw;)Z

    move-result v0

    return v0
.end method
