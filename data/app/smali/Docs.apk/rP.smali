.class LrP;
.super Ljava/lang/Object;
.source "ItemToUpload.java"

# interfaces
.implements LrN;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentResolver;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iput-object p1, p0, LrP;->a:Landroid/net/Uri;

    .line 136
    iput-object p2, p0, LrP;->a:Ljava/lang/String;

    .line 137
    iput-object p3, p0, LrP;->a:Landroid/content/ContentResolver;

    .line 138
    return-void
.end method


# virtual methods
.method public a()I
    .registers 6

    .prologue
    .line 169
    const/4 v0, -0x1

    .line 170
    const/4 v1, 0x0

    .line 172
    :try_start_2
    iget-object v2, p0, LrP;->a:Landroid/content/ContentResolver;

    iget-object v3, p0, LrP;->a:Landroid/net/Uri;

    const-string v4, "r"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_37
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_f} :catch_20

    move-result-wide v2

    long-to-int v0, v2

    .line 178
    if-eqz v1, :cond_16

    .line 179
    :try_start_13
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_16} :catch_17

    .line 185
    :cond_16
    :goto_16
    return v0

    .line 181
    :catch_17
    move-exception v1

    .line 182
    const-string v1, "ItemToUpload.UriDataSource"

    const-string v2, "Error closing file opened to obtain size."

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_16

    .line 174
    :catch_20
    move-exception v2

    .line 175
    :try_start_21
    const-string v2, "ItemToUpload.UriDataSource"

    const-string v3, "Error opening file to obtain size."

    invoke-static {v2, v3}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_28
    .catchall {:try_start_21 .. :try_end_28} :catchall_37

    .line 178
    if-eqz v1, :cond_16

    .line 179
    :try_start_2a
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2d} :catch_2e

    goto :goto_16

    .line 181
    :catch_2e
    move-exception v1

    .line 182
    const-string v1, "ItemToUpload.UriDataSource"

    const-string v2, "Error closing file opened to obtain size."

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_16

    .line 177
    :catchall_37
    move-exception v0

    .line 178
    if-eqz v1, :cond_3d

    .line 179
    :try_start_3a
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3d} :catch_3e

    .line 183
    :cond_3d
    :goto_3d
    throw v0

    .line 181
    :catch_3e
    move-exception v1

    .line 182
    const-string v1, "ItemToUpload.UriDataSource"

    const-string v2, "Error closing file opened to obtain size."

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3d
.end method

.method public a()Ljava/io/InputStream;
    .registers 3

    .prologue
    .line 142
    .line 144
    :try_start_0
    iget-object v0, p0, LrP;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, LrP;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    .line 148
    return-object v0

    .line 145
    :catch_9
    move-exception v0

    .line 146
    new-instance v0, LrT;

    const-string v1, "Shared item not found."

    invoke-direct {v0, v1}, LrT;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 158
    iget-object v0, p0, LrP;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, LrP;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lsl;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 194
    iget-object v1, p0, LrP;->a:Landroid/net/Uri;

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lsl;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lsl;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, LrP;->a:Landroid/net/Uri;

    iget-object v1, p0, LrP;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    const-string v0, "android.intent.extra.STREAM"

    iget-object v1, p0, LrP;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 165
    return-void
.end method
