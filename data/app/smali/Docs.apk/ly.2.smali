.class Lly;
.super Ljava/lang/Object;
.source "MenuItemsState.java"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llx;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const-class v0, Llx;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lly;->a:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Llw;)V
    .registers 2
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Lly;-><init>()V

    return-void
.end method


# virtual methods
.method a()Llv;
    .registers 7

    .prologue
    .line 102
    iget-object v0, p0, Lly;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lly;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lly;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2b

    const/4 v0, 0x1

    :goto_d
    invoke-static {v0}, Lagu;->b(Z)V

    .line 103
    new-instance v0, Llv;

    iget-object v1, p0, Lly;->a:Ljava/util/Set;

    iget-object v2, p0, Lly;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lly;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v4, p0, Lly;->c:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Llv;-><init>(Ljava/util/Set;ZZZLlw;)V

    return-object v0

    .line 102
    :cond_2b
    const/4 v0, 0x0

    goto :goto_d
.end method

.method a(Llx;Z)Lly;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    if-eqz p2, :cond_b

    .line 76
    iget-object v0, p0, Lly;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    :goto_a
    return-object p0

    .line 78
    :cond_b
    iget-object v0, p0, Lly;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_a
.end method

.method a(Z)Lly;
    .registers 3
    .parameter

    .prologue
    .line 84
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lly;->a:Ljava/lang/Boolean;

    .line 85
    return-object p0
.end method

.method b(Z)Lly;
    .registers 3
    .parameter

    .prologue
    .line 89
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lly;->b:Ljava/lang/Boolean;

    .line 90
    return-object p0
.end method

.method c(Z)Lly;
    .registers 3
    .parameter

    .prologue
    .line 94
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lly;->c:Ljava/lang/Boolean;

    .line 95
    return-object p0
.end method
