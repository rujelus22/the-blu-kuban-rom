.class public Lpg;
.super Ljava/lang/Object;
.source "DownloadFileDocumentOpenerImpl.java"

# interfaces
.implements Lnm;


# instance fields
.field private final a:LUK;

.field private a:LaaS;

.field private final a:Landroid/os/Bundle;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

.field private final a:Ljava/lang/String;

.field private final a:LkY;

.field private final a:Lpa;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;Lpa;LkM;Landroid/os/Bundle;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lpg;->a:LaaS;

    .line 83
    iput-object p4, p0, Lpg;->a:Landroid/os/Bundle;

    .line 84
    invoke-virtual {p3}, LkM;->a()LkY;

    move-result-object v0

    iput-object v0, p0, Lpg;->a:LkY;

    .line 85
    invoke-virtual {p3}, LkM;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpg;->a:Ljava/lang/String;

    .line 86
    invoke-virtual {p1, p3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(LkM;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpg;->b:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lpg;->a:Lpa;

    .line 88
    invoke-static {p4}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v0

    .line 89
    invoke-virtual {p3}, LkM;->a()LkP;

    move-result-object v1

    invoke-virtual {v0, v1}, LfS;->a(LkP;)LUK;

    move-result-object v1

    iput-object v1, p0, Lpg;->a:LUK;

    .line 90
    invoke-static {p1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Landroid/content/Context;

    move-result-object v1

    iget v0, v0, LfS;->a:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpg;->c:Ljava/lang/String;

    .line 91
    return-void
.end method

.method private a()I
    .registers 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    .line 125
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    iget-object v1, p0, Lpg;->a:LkY;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;LkY;)LkM;

    move-result-object v0

    .line 126
    if-nez v0, :cond_e

    move v0, v7

    .line 169
    :goto_d
    return v0

    .line 130
    :cond_e
    iget-object v1, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LUL;

    move-result-object v1

    iget-object v2, p0, Lpg;->a:LUK;

    invoke-interface {v1, v0, v2}, LUL;->c(LkM;LUK;)Z

    move-result v0

    if-eqz v0, :cond_1e

    move v0, v8

    .line 131
    goto :goto_d

    .line 134
    :cond_1e
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LeQ;

    move-result-object v0

    invoke-virtual {v0, p0}, LeQ;->a(Ljava/lang/Object;)V

    .line 136
    const/4 v3, 0x0

    .line 138
    :try_start_28
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    iget-object v1, p0, Lpg;->a:LkY;

    iget-object v2, p0, Lpg;->a:Lpa;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(LkY;Lpa;)Ljava/lang/String;
    :try_end_31
    .catch LasH; {:try_start_28 .. :try_end_31} :catch_36
    .catch Latc; {:try_start_28 .. :try_end_31} :catch_5b
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_31} :catch_80

    move-result-object v3

    .line 150
    :goto_32
    if-nez v3, :cond_a5

    move v0, v7

    .line 151
    goto :goto_d

    .line 139
    :catch_36
    move-exception v0

    .line 140
    const-string v1, "DownloadFileDocumentOpener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authentication error: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LasH;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v1, p0, Lpg;->a:Lpa;

    sget-object v2, Lpc;->f:Lpc;

    invoke-interface {v1, v2, v0}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_32

    .line 142
    :catch_5b
    move-exception v0

    .line 143
    const-string v1, "DownloadFileDocumentOpener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to parse document feed: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Latc;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v1, p0, Lpg;->a:Lpa;

    sget-object v2, Lpc;->h:Lpc;

    invoke-interface {v1, v2, v0}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_32

    .line 145
    :catch_80
    move-exception v0

    .line 146
    const-string v1, "DownloadFileDocumentOpener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Network error: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v1, p0, Lpg;->a:Lpa;

    sget-object v2, Lpc;->g:Lpc;

    invoke-interface {v1, v2, v0}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_32

    .line 154
    :cond_a5
    :try_start_a5
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LXP;

    move-result-object v0

    iget-object v1, p0, Lpg;->a:LkY;

    iget-object v1, v1, LkY;->b:Ljava/lang/String;

    iget-object v2, p0, Lpg;->a:LkY;

    iget-object v2, v2, LkY;->a:Ljava/lang/String;

    iget-object v4, p0, Lpg;->b:Ljava/lang/String;

    iget-object v5, p0, Lpg;->a:LUK;

    invoke-virtual {v5}, LUK;->name()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lph;

    iget-object v9, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v9}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lpg;->a:LaaS;

    invoke-direct {v6, p0, v9, v10}, Lph;-><init>(Lpg;Landroid/content/Context;LaaS;)V

    invoke-interface/range {v0 .. v6}, LXP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LXM;)V
    :try_end_cb
    .catchall {:try_start_a5 .. :try_end_cb} :catchall_104
    .catch Landroid/os/RemoteException; {:try_start_a5 .. :try_end_cb} :catch_d9

    .line 167
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LeQ;

    move-result-object v0

    const-string v1, "downloadTime"

    invoke-virtual {v0, p0, v1}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move v0, v8

    .line 169
    goto/16 :goto_d

    .line 163
    :catch_d9
    move-exception v0

    .line 164
    :try_start_da
    const-string v1, "DownloadFileDocumentOpener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f6
    .catchall {:try_start_da .. :try_end_f6} :catchall_104

    .line 167
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LeQ;

    move-result-object v0

    const-string v1, "downloadTime"

    invoke-virtual {v0, p0, v1}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move v0, v7

    goto/16 :goto_d

    :catchall_104
    move-exception v0

    iget-object v1, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LeQ;

    move-result-object v1

    const-string v2, "downloadTime"

    invoke-virtual {v1, p0, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lpg;)Lpa;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lpg;->a:Lpa;

    return-object v0
.end method

.method private b()I
    .registers 5

    .prologue
    .line 173
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 175
    const/4 v0, -0x1

    .line 178
    :goto_b
    return v0

    :cond_c
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    iget-object v1, p0, Lpg;->a:Lpa;

    iget-object v2, p0, Lpg;->a:LkY;

    iget-object v3, p0, Lpg;->a:Landroid/os/Bundle;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;Lpa;LkY;Landroid/os/Bundle;)I

    move-result v0

    goto :goto_b
.end method


# virtual methods
.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 95
    packed-switch p1, :pswitch_data_10

    .line 103
    const/4 v0, -0x1

    :goto_4
    return v0

    .line 99
    :pswitch_5
    invoke-direct {p0}, Lpg;->a()I

    move-result v0

    goto :goto_4

    .line 101
    :pswitch_a
    invoke-direct {p0}, Lpg;->b()I

    move-result v0

    goto :goto_4

    .line 95
    nop

    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_5
        :pswitch_a
    .end packed-switch
.end method

.method public a()Ljava/lang/String;
    .registers 5

    .prologue
    .line 109
    iget-object v0, p0, Lpg;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lpg;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaaS;)V
    .registers 8
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, Lpg;->a:LaaS;

    .line 120
    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x64

    iget-object v5, p0, Lpg;->c:Ljava/lang/String;

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, LaaS;->a(JJLjava/lang/String;)V

    .line 122
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method
