.class public final Lju;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LjY;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lka;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljy;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Liy;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Ljz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 35
    iput-object p1, p0, Lju;->a:LYD;

    .line 36
    const-class v0, LjY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lju;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lju;->a:LZb;

    .line 39
    const-class v0, Lka;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lju;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lju;->b:LZb;

    .line 42
    const-class v0, Ljy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lju;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lju;->c:LZb;

    .line 45
    const-class v0, LjZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lju;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lju;->d:LZb;

    .line 48
    const-class v0, Ljz;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lju;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lju;->e:LZb;

    .line 51
    return-void
.end method

.method static synthetic a(Lju;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lju;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 196
    const-class v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    new-instance v1, Ljv;

    invoke-direct {v1, p0}, Ljv;-><init>(Lju;)V

    invoke-virtual {p0, v0, v1}, Lju;->a(Ljava/lang/Class;Laou;)V

    .line 204
    const-class v0, Lcom/google/android/apps/docs/app/tablet/TabletHomeActivity;

    new-instance v1, Ljw;

    invoke-direct {v1, p0}, Ljw;-><init>(Lju;)V

    invoke-virtual {p0, v0, v1}, Lju;->a(Ljava/lang/Class;Laou;)V

    .line 212
    const-class v0, LjY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lju;->a:LZb;

    invoke-virtual {p0, v0, v1}, Lju;->a(Laop;LZb;)V

    .line 213
    const-class v0, Lka;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lju;->b:LZb;

    invoke-virtual {p0, v0, v1}, Lju;->a(Laop;LZb;)V

    .line 214
    const-class v0, Ljy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lju;->c:LZb;

    invoke-virtual {p0, v0, v1}, Lju;->a(Laop;LZb;)V

    .line 215
    const-class v0, LjZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lju;->d:LZb;

    invoke-virtual {p0, v0, v1}, Lju;->a(Laop;LZb;)V

    .line 216
    const-class v0, Ljz;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lju;->e:LZb;

    invoke-virtual {p0, v0, v1}, Lju;->a(Laop;LZb;)V

    .line 217
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/AccountListeningActivity;)V

    .line 59
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlE;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LlE;

    .line 65
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LPm;

    .line 71
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LKS;

    .line 77
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lez;

    iget-object v0, v0, Lez;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lev;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lev;

    .line 83
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LZj;

    .line 89
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LME;

    .line 95
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lgl;

    .line 101
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LLL;

    iget-object v0, v0, LLL;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LMf;

    .line 107
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LUL;

    .line 113
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    .line 119
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LZl;

    .line 125
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiG;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiG;

    .line 131
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LaaJ;

    .line 137
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    .line 143
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljo;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljo;

    .line 149
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Laoz;

    .line 155
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:LKS;

    .line 161
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->c:Laoz;

    .line 167
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXX;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LXX;

    .line 173
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llz;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llz;

    .line 179
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lju;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Llf;

    .line 185
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/tablet/TabletHomeActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 189
    iget-object v0, p0, Lju;->a:LYD;

    iget-object v0, v0, LYD;->a:Lju;

    invoke-virtual {v0, p1}, Lju;->a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    .line 191
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 221
    return-void
.end method
