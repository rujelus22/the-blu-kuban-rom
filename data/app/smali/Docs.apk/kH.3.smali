.class public LkH;
.super LkO;
.source "Collection.java"


# instance fields
.field private a:J


# direct methods
.method private constructor <init>(LPO;LkB;Landroid/database/Cursor;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, LkO;-><init>(LPO;LkB;Landroid/database/Cursor;)V

    .line 20
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LkH;->a:J

    .line 26
    invoke-virtual {p0}, LkH;->o()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 27
    invoke-static {}, LPE;->a()LPE;

    move-result-object v0

    invoke-virtual {v0}, LPE;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LkH;->b(J)V

    .line 29
    return-void
.end method

.method public constructor <init>(LPO;LkB;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    sget-object v0, LkP;->h:LkP;

    invoke-virtual {v0}, LkP;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, LkO;-><init>(LPO;LkB;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LkH;->a:J

    .line 33
    return-void
.end method

.method private a(J)Landroid/content/ContentValues;
    .registers 6
    .parameter

    .prologue
    .line 91
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 92
    sget-object v1, LPF;->c:LPF;

    invoke-virtual {v1}, LPF;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 93
    return-object v0
.end method

.method public static a(LPO;LkB;Landroid/database/Cursor;)LkH;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    new-instance v0, LkH;

    invoke-direct {v0, p0, p1, p2}, LkH;-><init>(LPO;LkB;Landroid/database/Cursor;)V

    .line 84
    return-object v0
.end method

.method private b(J)V
    .registers 7
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 46
    cmp-long v0, p1, v2

    if-ltz v0, :cond_12

    iget-wide v0, p0, LkH;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_12

    iget-wide v0, p0, LkH;->a:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_19

    :cond_12
    const/4 v0, 0x1

    :goto_13
    invoke-static {v0}, Lagu;->b(Z)V

    .line 48
    iput-wide p1, p0, LkH;->a:J

    .line 49
    return-void

    .line 46
    :cond_19
    const/4 v0, 0x0

    goto :goto_13
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 39
    iget-wide v0, p0, LkH;->a:J

    return-wide v0
.end method

.method protected a(LPO;)V
    .registers 6
    .parameter

    .prologue
    .line 98
    invoke-virtual {p0}, LkH;->a()J

    move-result-wide v0

    invoke-static {}, LPE;->a()LPE;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, LPO;->a(JLPN;Landroid/net/Uri;)V

    .line 99
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, LkH;->b(J)V

    .line 100
    return-void
.end method

.method protected a(LPO;J)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 105
    invoke-virtual {p0}, LkH;->a()J

    move-result-wide v1

    invoke-static {}, LPE;->a()LPE;

    move-result-object v3

    invoke-direct {p0, p2, p3}, LkH;->a(J)Landroid/content/ContentValues;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LPO;->a(JLPN;Landroid/content/ContentValues;Landroid/net/Uri;)J

    move-result-wide v0

    .line 108
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_20

    .line 109
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Error saving document"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_20
    invoke-direct {p0, v0, v1}, LkH;->b(J)V

    .line 112
    return-void
.end method

.method public a(LPm;)Z
    .registers 3
    .parameter

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method
