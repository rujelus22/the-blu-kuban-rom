.class public final enum LhB;
.super Ljava/lang/Enum;
.source "MainProxyLogic.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LhB;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LhB;

.field private static final synthetic a:[LhB;

.field public static final enum b:LhB;

.field public static final enum c:LhB;

.field public static final enum d:LhB;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, LhB;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LhB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhB;->a:LhB;

    new-instance v0, LhB;

    const-string v1, "CREATE_NEW"

    invoke-direct {v0, v1, v3}, LhB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhB;->b:LhB;

    new-instance v0, LhB;

    const-string v1, "WELCOME_TO_DRIVE"

    invoke-direct {v0, v1, v4}, LhB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhB;->c:LhB;

    new-instance v0, LhB;

    const-string v1, "DOCS_UPGRADED_TO_DRIVE"

    invoke-direct {v0, v1, v5}, LhB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhB;->d:LhB;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [LhB;

    sget-object v1, LhB;->a:LhB;

    aput-object v1, v0, v2

    sget-object v1, LhB;->b:LhB;

    aput-object v1, v0, v3

    sget-object v1, LhB;->c:LhB;

    aput-object v1, v0, v4

    sget-object v1, LhB;->d:LhB;

    aput-object v1, v0, v5

    sput-object v0, LhB;->a:[LhB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Landroid/content/Intent;)LhB;
    .registers 2
    .parameter

    .prologue
    .line 45
    const-string v0, "dialogToShow"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LhB;

    .line 47
    if-nez v0, :cond_c

    .line 48
    sget-object v0, LhB;->a:LhB;

    .line 50
    :cond_c
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LhB;
    .registers 2
    .parameter

    .prologue
    .line 41
    const-class v0, LhB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhB;

    return-object v0
.end method

.method public static values()[LhB;
    .registers 1

    .prologue
    .line 41
    sget-object v0, LhB;->a:[LhB;

    invoke-virtual {v0}, [LhB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LhB;

    return-object v0
.end method
