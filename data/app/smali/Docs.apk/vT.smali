.class public LvT;
.super Ljava/lang/Object;
.source "Kix.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/jsvm/JSCallback;


# instance fields
.field private a:LvS;

.field protected a:Lvw;


# direct methods
.method public constructor <init>(Lvw;LvS;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5837
    iput-object p1, p0, LvT;->a:Lvw;

    .line 5838
    iput-object p2, p0, LvT;->a:LvS;

    .line 5839
    return-void
.end method


# virtual methods
.method public addSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5842
    iget-object v0, p0, LvT;->a:LvS;

    invoke-interface {v0, p1, p2, p3}, LvS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5843
    return-void
.end method

.method public deleteSession(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 5846
    iget-object v0, p0, LvT;->a:LvS;

    invoke-interface {v0, p1}, LvS;->a(Ljava/lang/String;)V

    .line 5847
    return-void
.end method

.method public moveCursor(Ljava/lang/String;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5850
    iget-object v0, p0, LvT;->a:LvS;

    invoke-interface {v0, p1, p2, p3}, LvS;->a(Ljava/lang/String;II)V

    .line 5851
    return-void
.end method
