.class public interface abstract Llf;
.super Ljava/lang/Object;
.source "ModelLoader.java"


# virtual methods
.method public abstract a(J)I
.end method

.method public abstract a(LkN;)I
.end method

.method public abstract a()Landroid/database/Cursor;
.end method

.method public abstract a(LkB;Lnh;I)Landroid/database/Cursor;
.end method

.method public abstract a(Lnh;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract a(J)Ljava/lang/String;
.end method

.method public abstract a()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LkN;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LkB;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkB;",
            ")",
            "Ljava/util/List",
            "<",
            "Llj;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LkO;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkO;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LkX;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LkB;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkB;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LkD;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkD;",
            ")",
            "Ljava/util/Set",
            "<",
            "LkE;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LkB;)LkC;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)LkD;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)LkD;
.end method

.method public abstract a(LkD;LkN;)LkE;
.end method

.method public abstract a(LkB;Ljava/lang/String;J)LkF;
.end method

.method public abstract a(LkB;J)LkH;
.end method

.method public abstract a(LkB;Ljava/lang/String;)LkH;
.end method

.method public abstract a(LkB;Ljava/lang/String;)LkM;
.end method

.method public abstract a(LkB;Ljava/lang/String;Ljava/lang/String;)LkM;
.end method

.method public abstract a(J)LkN;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljavax/crypto/SecretKey;)LkN;
.end method

.method public abstract a(LkB;Ljava/lang/String;)LkO;
.end method

.method public abstract a(LkO;LkH;)LkX;
.end method

.method public abstract a(LkB;Ljava/lang/String;J)Lli;
.end method

.method public abstract a(LkB;Ljava/lang/String;J)Llj;
.end method

.method public abstract a()V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(LkN;)V
.end method

.method public abstract a()Z
.end method

.method public abstract a(JLjava/util/Set;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public abstract b(Lnh;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LkD;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(LkB;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkB;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)LkB;
.end method

.method public abstract b(LkB;Ljava/lang/String;)LkH;
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method
