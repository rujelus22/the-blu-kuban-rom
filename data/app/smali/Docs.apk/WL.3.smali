.class public LWL;
.super Ljava/lang/Object;
.source "SyncMoreFactoryImpl.java"

# interfaces
.implements LWK;


# instance fields
.field private final a:LWY;

.field private final a:LWs;

.field private final a:LXY;

.field private final a:LXm;

.field private final a:LkZ;

.field private final a:Llf;


# direct methods
.method public constructor <init>(LWs;LXY;Llf;LWY;LXm;LkZ;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, LWL;->a:LWs;

    .line 41
    iput-object p2, p0, LWL;->a:LXY;

    .line 42
    iput-object p3, p0, LWL;->a:Llf;

    .line 43
    iput-object p4, p0, LWL;->a:LWY;

    .line 44
    iput-object p5, p0, LWL;->a:LXm;

    .line 45
    iput-object p6, p0, LWL;->a:LkZ;

    .line 46
    return-void
.end method

.method private a(LkB;LWr;Ljava/util/Date;Ljava/util/Date;)Ljava/util/List;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkB;",
            "LWr;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List",
            "<",
            "Lli;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, LWL;->a:LWs;

    invoke-interface {v0, p2}, LWs;->a(LWr;)LWx;

    move-result-object v1

    .line 66
    iget-object v0, v1, LWx;->a:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, p1, v0, p3}, LWL;->a(LkB;Landroid/net/Uri;Ljava/util/Date;)Lli;

    move-result-object v2

    .line 68
    iget-object v0, v1, LWx;->b:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, p1, v0, p4}, LWL;->a(LkB;Landroid/net/Uri;Ljava/util/Date;)Lli;

    move-result-object v0

    .line 69
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 70
    if-eqz v2, :cond_20

    .line 71
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_20
    if-eqz v0, :cond_25

    .line 74
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_25
    return-object v1
.end method

.method private a(LkB;Landroid/net/Uri;Ljava/util/Date;)Lli;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    if-nez p2, :cond_4

    .line 54
    const/4 v0, 0x0

    .line 59
    :goto_3
    return-object v0

    .line 56
    :cond_4
    if-eqz p3, :cond_19

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Lagu;->a(Z)V

    .line 57
    iget-object v0, p0, LWL;->a:Llf;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {v0, p1, v1, v2, v3}, Llf;->a(LkB;Ljava/lang/String;J)Lli;

    move-result-object v0

    goto :goto_3

    .line 56
    :cond_19
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method public a(LkB;LSF;)LWy;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 121
    invoke-virtual {p2}, LSF;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LkB;LWr;)LWy;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-virtual {p1}, LkB;->c()Ljava/util/Date;

    move-result-object v0

    .line 84
    invoke-virtual {p1}, LkB;->b()Ljava/util/Date;

    move-result-object v1

    .line 95
    iget-object v2, p0, LWL;->a:LkZ;

    invoke-static {v0, v1, v2}, LWr;->a(Ljava/util/Date;Ljava/util/Date;LkZ;)LWr;

    move-result-object v2

    invoke-virtual {p2, v2}, LWr;->a(LWr;)LWr;

    move-result-object v2

    .line 97
    invoke-direct {p0, p1, v2, v0, v1}, LWL;->a(LkB;LWr;Ljava/util/Date;Ljava/util/Date;)Ljava/util/List;

    move-result-object v2

    .line 100
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 101
    const/4 v0, 0x0

    .line 116
    :goto_1d
    return-object v0

    .line 104
    :cond_1e
    new-instance v5, Landroid/content/SyncResult;

    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    .line 111
    iget-object v0, p0, LWL;->a:LXm;

    iget-object v1, p0, LWL;->a:LWY;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v1, v5, p1, v3}, LXm;->a(LWY;Landroid/content/SyncResult;LkB;Ljava/lang/Boolean;)LXg;

    move-result-object v3

    .line 113
    new-instance v0, LWA;

    iget-object v4, p0, LWL;->a:LXY;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LWA;-><init>(LkB;Ljava/util/List;LXg;LXY;Landroid/content/SyncResult;)V

    goto :goto_1d
.end method
