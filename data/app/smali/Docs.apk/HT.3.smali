.class public LHT;
.super Ljava/lang/Object;
.source "CollaboratorTracker.java"

# interfaces
.implements LHa;


# instance fields
.field private final a:I

.field private final a:Lalt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lalt",
            "<",
            "Landroid/graphics/Point;",
            "LHV;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LHV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LHT;->a:Ljava/util/Map;

    .line 38
    invoke-static {}, Laja;->a()Laja;

    move-result-object v0

    iput-object v0, p0, LHT;->a:Lalt;

    .line 42
    iput p1, p0, LHT;->a:I

    .line 43
    return-void
.end method


# virtual methods
.method public a(II)Ljava/lang/String;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 49
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    .line 50
    iget-object v1, p0, LHT;->a:Lalt;

    invoke-interface {v1, v0}, Lalt;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 51
    const/4 v0, 0x0

    .line 54
    :goto_e
    return-object v0

    .line 53
    :cond_f
    iget-object v0, p0, LHT;->a:Lalt;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v0, v1}, Lalt;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 54
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHV;

    iget-object v0, v0, LHV;->a:Ljava/lang/String;

    goto :goto_e
.end method

.method public a(LJD;)V
    .registers 8
    .parameter

    .prologue
    .line 60
    invoke-virtual {p1}, LJD;->a()I

    move-result v0

    iget v1, p0, LHT;->a:I

    if-eq v0, v1, :cond_9

    .line 71
    :goto_8
    return-void

    .line 64
    :cond_9
    invoke-virtual {p1}, LJD;->a()Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, LJD;->a()LJJ;

    move-result-object v1

    .line 66
    invoke-virtual {p1}, LJD;->c()Ljava/lang/String;

    move-result-object v2

    .line 67
    new-instance v3, LHV;

    new-instance v4, Landroid/graphics/Point;

    invoke-virtual {v1}, LJJ;->a()I

    move-result v5

    invoke-virtual {v1}, LJJ;->b()I

    move-result v1

    invoke-direct {v4, v5, v1}, Landroid/graphics/Point;-><init>(II)V

    const/4 v1, 0x0

    invoke-direct {v3, v2, v4, v1}, LHV;-><init>(Ljava/lang/String;Landroid/graphics/Point;LHU;)V

    .line 69
    iget-object v1, p0, LHT;->a:Ljava/util/Map;

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, LHT;->a:Lalt;

    iget-object v1, v3, LHV;->a:Landroid/graphics/Point;

    invoke-interface {v0, v1, v3}, Lalt;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public a([LJD;)V
    .registers 5
    .parameter

    .prologue
    .line 91
    if-eqz p1, :cond_e

    .line 92
    array-length v1, p1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_e

    aget-object v2, p1, v0

    .line 93
    invoke-virtual {p0, v2}, LHT;->a(LJD;)V

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 96
    :cond_e
    return-void
.end method

.method public b(LJD;)V
    .registers 6
    .parameter

    .prologue
    .line 75
    invoke-virtual {p1}, LJD;->a()Ljava/lang/String;

    move-result-object v1

    .line 76
    iget-object v0, p0, LHT;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHV;

    .line 77
    if-eqz v0, :cond_1a

    .line 78
    iget-object v2, p0, LHT;->a:Lalt;

    iget-object v3, v0, LHV;->a:Landroid/graphics/Point;

    invoke-interface {v2, v3, v0}, Lalt;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, LHT;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_1a
    invoke-virtual {p0, p1}, LHT;->a(LJD;)V

    .line 82
    return-void
.end method

.method public c(LJD;)V
    .registers 4
    .parameter

    .prologue
    .line 86
    invoke-virtual {p1}, LJD;->a()Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v1, p0, LHT;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    return-void
.end method
