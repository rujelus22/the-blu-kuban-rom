.class public final LIZ;
.super Ljava/lang/Object;
.source "GridViewModelChangedEvent.java"


# static fields
.field private static a:I

.field public static final a:LIZ;

.field private static a:[LIZ;

.field public static final b:LIZ;

.field public static final c:LIZ;

.field public static final d:LIZ;

.field public static final e:LIZ;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 63
    new-instance v0, LIZ;

    const-string v1, "kCellContentsChanged"

    invoke-direct {v0, v1}, LIZ;-><init>(Ljava/lang/String;)V

    sput-object v0, LIZ;->a:LIZ;

    .line 64
    new-instance v0, LIZ;

    const-string v1, "kRowSizesChanged"

    invoke-direct {v0, v1}, LIZ;-><init>(Ljava/lang/String;)V

    sput-object v0, LIZ;->b:LIZ;

    .line 65
    new-instance v0, LIZ;

    const-string v1, "kColumnSizesChanged"

    invoke-direct {v0, v1}, LIZ;-><init>(Ljava/lang/String;)V

    sput-object v0, LIZ;->c:LIZ;

    .line 66
    new-instance v0, LIZ;

    const-string v1, "kSelectionChanged"

    invoke-direct {v0, v1}, LIZ;-><init>(Ljava/lang/String;)V

    sput-object v0, LIZ;->d:LIZ;

    .line 67
    new-instance v0, LIZ;

    const-string v1, "kOptionsChanged"

    invoke-direct {v0, v1}, LIZ;-><init>(Ljava/lang/String;)V

    sput-object v0, LIZ;->e:LIZ;

    .line 103
    const/4 v0, 0x5

    new-array v0, v0, [LIZ;

    sget-object v1, LIZ;->a:LIZ;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, LIZ;->b:LIZ;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LIZ;->c:LIZ;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LIZ;->d:LIZ;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LIZ;->e:LIZ;

    aput-object v2, v0, v1

    sput-object v0, LIZ;->a:[LIZ;

    .line 104
    sput v3, LIZ;->a:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, LIZ;->a:Ljava/lang/String;

    .line 88
    sget v0, LIZ;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LIZ;->a:I

    iput v0, p0, LIZ;->b:I

    .line 89
    return-void
.end method

.method public static a(I)LIZ;
    .registers 4
    .parameter

    .prologue
    .line 78
    sget-object v0, LIZ;->a:[LIZ;

    array-length v0, v0

    if-ge p0, v0, :cond_14

    if-ltz p0, :cond_14

    sget-object v0, LIZ;->a:[LIZ;

    aget-object v0, v0, p0

    iget v0, v0, LIZ;->b:I

    if-ne v0, p0, :cond_14

    .line 79
    sget-object v0, LIZ;->a:[LIZ;

    aget-object v0, v0, p0

    .line 82
    :goto_13
    return-object v0

    .line 80
    :cond_14
    const/4 v0, 0x0

    :goto_15
    sget-object v1, LIZ;->a:[LIZ;

    array-length v1, v1

    if-ge v0, v1, :cond_2a

    .line 81
    sget-object v1, LIZ;->a:[LIZ;

    aget-object v1, v1, v0

    iget v1, v1, LIZ;->b:I

    if-ne v1, p0, :cond_27

    .line 82
    sget-object v1, LIZ;->a:[LIZ;

    aget-object v0, v1, v0

    goto :goto_13

    .line 80
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 83
    :cond_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LIZ;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, LIZ;->a:Ljava/lang/String;

    return-object v0
.end method
