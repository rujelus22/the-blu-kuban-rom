.class public LXT;
.super Ljava/lang/Object;
.source "SingleDocSynchronizerImpl.java"

# interfaces
.implements LXS;


# instance fields
.field private final a:LUL;

.field private final a:LVH;

.field private final a:LWY;

.field private final a:LaaZ;


# direct methods
.method public constructor <init>(LVH;LWY;LUL;LaaZ;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, LXT;->a:LVH;

    .line 42
    iput-object p2, p0, LXT;->a:LWY;

    .line 43
    iput-object p3, p0, LXT;->a:LUL;

    .line 44
    iput-object p4, p0, LXT;->a:LaaZ;

    .line 45
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;LkM;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 74
    new-instance v0, LXV;

    invoke-direct {v0, p0, p1, p2}, LXV;-><init>(LXT;Landroid/content/Context;LkM;)V

    invoke-virtual {v0}, LXV;->start()V

    .line 81
    return-void
.end method

.method public b(Landroid/content/Context;LkM;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 64
    new-instance v0, LXU;

    invoke-direct {v0, p0, p1, p2}, LXU;-><init>(LXT;Landroid/content/Context;LkM;)V

    invoke-virtual {v0}, LXU;->start()V

    .line 70
    return-void
.end method

.method public c(Landroid/content/Context;LkM;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, LXT;->d(Landroid/content/Context;LkM;)V

    .line 60
    return-void
.end method

.method d(Landroid/content/Context;LkM;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-virtual {p2}, LkM;->a()LkB;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, LkB;->b()Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {p2}, LkM;->i()Ljava/lang/String;

    move-result-object v2

    .line 88
    invoke-virtual {p2}, LkM;->h()Ljava/lang/String;

    move-result-object v3

    .line 91
    :try_start_10
    iget-object v4, p0, LXT;->a:LVH;

    invoke-static {v2}, LVI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v1, v3}, LVH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LVV;
    :try_end_19
    .catch Latc; {:try_start_10 .. :try_end_19} :catch_23
    .catch LasH; {:try_start_10 .. :try_end_19} :catch_3d
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_19} :catch_57

    move-result-object v1

    .line 104
    if-eqz v1, :cond_22

    .line 106
    :try_start_1c
    iget-object v3, p0, LXT;->a:LWY;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v1, v4}, LWY;->a(LkB;LVV;Ljava/lang/Boolean;)V
    :try_end_22
    .catch Ljava/text/ParseException; {:try_start_1c .. :try_end_22} :catch_71

    .line 112
    :cond_22
    :goto_22
    return-void

    .line 93
    :catch_23
    move-exception v0

    .line 94
    const-string v1, "SingleDocSynchronizerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to get entry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_22

    .line 96
    :catch_3d
    move-exception v0

    .line 97
    const-string v1, "SingleDocSynchronizerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to get entry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_22

    .line 99
    :catch_57
    move-exception v0

    .line 100
    const-string v1, "SingleDocSynchronizerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to get entry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_22

    .line 107
    :catch_71
    move-exception v0

    .line 108
    const-string v1, "SingleDocSynchronizerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to update entry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_22
.end method

.method e(Landroid/content/Context;LkM;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, LXT;->a:LUL;

    sget-object v1, LUK;->a:LUK;

    invoke-interface {v0, p2, v1}, LUL;->c(LkM;LUK;)Z

    move-result v0

    if-nez v0, :cond_19

    iget-object v0, p0, LXT;->a:LaaZ;

    invoke-interface {v0}, LaaZ;->b()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 119
    invoke-virtual {p2}, LkM;->a()LkY;

    move-result-object v0

    .line 120
    invoke-static {p1, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/content/Context;LkY;)V

    .line 122
    :cond_19
    return-void
.end method
