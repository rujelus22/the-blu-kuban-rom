.class public Ljn;
.super Ljava/lang/Object;
.source "NavigationPathImpl.java"

# interfaces
.implements Ljl;


# instance fields
.field private final a:Ljava/lang/Thread;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljm;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljo;


# direct methods
.method public constructor <init>(Ljo;)V
    .registers 3
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljn;->a:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Ljn;->a:Ljava/util/Set;

    .line 48
    iput-object p1, p0, Ljn;->a:Ljo;

    .line 49
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Ljn;->a:Ljava/lang/Thread;

    .line 50
    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    .line 92
    iget-object v0, p0, Ljn;->a:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 93
    return-void
.end method


# virtual methods
.method public a()LiQ;
    .registers 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljn;->c()V

    .line 63
    iget-object v0, p0, Ljn;->a:Ljava/util/List;

    invoke-static {v0}, LajB;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljn;->c()V

    .line 56
    iget-object v0, p0, Ljn;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 127
    invoke-direct {p0}, Ljn;->c()V

    .line 129
    iget-object v0, p0, Ljn;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljm;

    .line 130
    invoke-interface {v0}, Ljm;->b()V

    goto :goto_9

    .line 132
    :cond_19
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljn;->c()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Ljn;->a:Ljava/util/List;

    .line 71
    invoke-virtual {p0}, Ljn;->b()V

    .line 72
    return-void
.end method

.method public a(Ljm;)V
    .registers 3
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Ljn;->c()V

    .line 106
    iget-object v0, p0, Ljn;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 118
    invoke-direct {p0}, Ljn;->c()V

    .line 120
    iget-object v0, p0, Ljn;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljm;

    .line 121
    invoke-interface {v0}, Ljm;->a()V

    goto :goto_9

    .line 123
    :cond_19
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 97
    invoke-direct {p0}, Ljn;->c()V

    .line 99
    const-string v0, "Path %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ljn;->a:Ljava/util/List;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
