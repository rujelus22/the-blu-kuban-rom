.class public LBW;
.super Landroid/text/style/MetricAffectingSpan;
.source "FootnoteNumberSpan.java"

# interfaces
.implements LGF;


# instance fields
.field private final a:LGH;


# direct methods
.method public constructor <init>(LGH;)V
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 24
    iput-object p1, p0, LBW;->a:LGH;

    .line 25
    return-void
.end method

.method private a(Landroid/text/TextPaint;)V
    .registers 8
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, LBW;->a:LGH;

    invoke-virtual {v0}, LGH;->b()I

    move-result v0

    .line 33
    if-lez v0, :cond_17

    .line 34
    int-to-double v0, v0

    const-wide v2, 0x3fe5555555555555L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 36
    :cond_17
    iget v0, p1, Landroid/text/TextPaint;->baselineShift:I

    int-to-long v0, v0

    const-wide/high16 v2, 0x3fe0

    invoke-virtual {p1}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    add-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p1, Landroid/text/TextPaint;->baselineShift:I

    .line 37
    return-void
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, LBW;->a:LGH;

    invoke-virtual {v0, p1}, LGH;->updateDrawState(Landroid/text/TextPaint;)V

    .line 48
    invoke-direct {p0, p1}, LBW;->a(Landroid/text/TextPaint;)V

    .line 49
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, LBW;->a:LGH;

    invoke-virtual {v0, p1}, LGH;->updateMeasureState(Landroid/text/TextPaint;)V

    .line 42
    invoke-direct {p0, p1}, LBW;->a(Landroid/text/TextPaint;)V

    .line 43
    return-void
.end method
