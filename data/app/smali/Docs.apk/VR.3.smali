.class public LVR;
.super LasT;
.source "AccountMetadataEntry.java"


# instance fields
.field private a:I

.field private a:J

.field private final a:LalN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalN",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LkP;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lqu;

.field private b:I

.field private b:J

.field private final b:LalN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalN",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private final c:LalN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalN",
            "<",
            "Ljava/lang/String;",
            "LeG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const-wide/16 v0, 0x0

    .line 31
    invoke-direct {p0}, LasT;-><init>()V

    .line 32
    iput-wide v0, p0, LVR;->a:J

    .line 33
    iput-wide v0, p0, LVR;->b:J

    .line 34
    iput-wide v0, p0, LVR;->c:J

    .line 36
    const/4 v0, 0x0

    iput v0, p0, LVR;->a:I

    .line 40
    const/4 v0, -0x1

    iput v0, p0, LVR;->b:I

    .line 41
    invoke-static {}, Laja;->a()Laja;

    move-result-object v0

    iput-object v0, p0, LVR;->a:LalN;

    .line 42
    invoke-static {}, Laja;->a()Laja;

    move-result-object v0

    iput-object v0, p0, LVR;->b:LalN;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LVR;->a:Ljava/util/Set;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LVR;->a:Ljava/util/Map;

    .line 45
    new-instance v0, Lqw;

    invoke-direct {v0}, Lqw;-><init>()V

    iput-object v0, p0, LVR;->a:Lqu;

    .line 46
    invoke-static {}, Laja;->a()Laja;

    move-result-object v0

    iput-object v0, p0, LVR;->c:LalN;

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 97
    iget v0, p0, LVR;->a:I

    return v0
.end method

.method public a()J
    .registers 3

    .prologue
    .line 66
    iget-wide v0, p0, LVR;->a:J

    return-wide v0
.end method

.method public a(LkP;)J
    .registers 5
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, LVR;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 168
    if-nez v0, :cond_2f

    .line 169
    const-string v0, "AccountMetadataEntry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not find max import size for file type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LkP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returning 0 bytes as a result."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-wide/16 v0, 0x0

    .line 174
    :goto_2e
    return-wide v0

    :cond_2f
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_2e
.end method

.method public a()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, LVR;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, LVR;->a:LalN;

    invoke-interface {v0, p1}, LalN;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqu;
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, LVR;->a:Lqu;

    invoke-static {v0}, LqP;->a(Lqu;)LqP;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    const-wide/16 v0, 0x0

    .line 52
    invoke-super {p0}, LasT;->a()V

    .line 53
    iput-wide v0, p0, LVR;->a:J

    .line 54
    iput-wide v0, p0, LVR;->b:J

    .line 55
    iput-wide v0, p0, LVR;->c:J

    .line 56
    const/4 v0, 0x0

    iput v0, p0, LVR;->a:I

    .line 57
    const/4 v0, -0x1

    iput v0, p0, LVR;->b:I

    .line 58
    iget-object v0, p0, LVR;->a:LalN;

    invoke-interface {v0}, LalN;->a()V

    .line 59
    iget-object v0, p0, LVR;->b:LalN;

    invoke-interface {v0}, LalN;->a()V

    .line 60
    iget-object v0, p0, LVR;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 61
    iget-object v0, p0, LVR;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 62
    iget-object v0, p0, LVR;->a:Lqu;

    invoke-interface {v0}, Lqu;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 63
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 101
    iput p1, p0, LVR;->a:I

    .line 102
    return-void
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 70
    iput-wide p1, p0, LVR;->a:J

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, LVR;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 179
    return-void
.end method

.method public a(Ljava/lang/String;LeG;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 195
    iget-object v0, p0, LVR;->c:LalN;

    invoke-interface {v0, p1}, LalN;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 196
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 197
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, LVR;->a:LalN;

    invoke-interface {v0, p1, p2}, LalN;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 131
    return-void
.end method

.method public a(LkP;J)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 160
    iget-object v0, p0, LVR;->a:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 106
    iget v0, p0, LVR;->b:I

    if-ltz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(LWi;)Z
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, LVR;->a:Lqu;

    invoke-interface {v0}, Lqu;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 116
    invoke-virtual {p0}, LVR;->a()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 117
    iget v0, p0, LVR;->b:I

    return v0
.end method

.method public b()J
    .registers 3

    .prologue
    .line 74
    iget-wide v0, p0, LVR;->b:J

    return-wide v0
.end method

.method public b(Ljava/lang/String;)Ljava/util/Set;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "LeG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, LVR;->c:LalN;

    invoke-interface {v0, p1}, LalN;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 201
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 121
    if-ltz p1, :cond_9

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 122
    iput p1, p0, LVR;->b:I

    .line 123
    return-void

    .line 121
    :cond_9
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public b(J)V
    .registers 3
    .parameter

    .prologue
    .line 78
    iput-wide p1, p0, LVR;->b:J

    .line 79
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, LVR;->b:LalN;

    invoke-interface {v0, p1, p2}, LalN;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 146
    return-void
.end method

.method public c(J)V
    .registers 3
    .parameter

    .prologue
    .line 86
    iput-wide p1, p0, LVR;->c:J

    .line 87
    return-void
.end method
