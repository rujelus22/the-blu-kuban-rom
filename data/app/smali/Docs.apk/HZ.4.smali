.class public LHZ;
.super Ljava/lang/Object;
.source "NativeListView.java"

# interfaces
.implements LIK;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)V
    .registers 2
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, LHZ;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LJh;)V
    .registers 3
    .parameter

    .prologue
    .line 91
    invoke-virtual {p1}, LJh;->a()LJi;

    .line 93
    iget-object v0, p0, LHZ;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)V

    .line 94
    iget-object v0, p0, LHZ;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->invalidate()V

    .line 95
    return-void
.end method

.method public a(LJr;)V
    .registers 9
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, LHZ;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)LJk;

    move-result-object v0

    invoke-virtual {v0}, LJk;->b()LJI;

    move-result-object v0

    .line 100
    iget-object v1, p0, LHZ;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)LJk;

    move-result-object v1

    invoke-virtual {v1}, LJk;->a()D

    move-result-wide v1

    .line 101
    iget-object v3, p0, LHZ;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v0}, LJI;->a()D

    move-result-wide v4

    mul-double/2addr v4, v1

    double-to-int v4, v4

    invoke-virtual {v0}, LJI;->b()D

    move-result-wide v5

    mul-double v0, v5, v1

    double-to-int v0, v0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->scrollTo(II)V

    .line 102
    iget-object v0, p0, LHZ;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->invalidate()V

    .line 103
    return-void
.end method
