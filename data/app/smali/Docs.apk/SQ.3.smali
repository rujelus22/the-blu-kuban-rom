.class public LSQ;
.super Ljava/lang/Object;
.source "MimeTypeHelper.java"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LKS;

.field private final a:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 34
    const-string v0, ".3gp"

    const-string v1, "video/3gpp"

    invoke-static {v0, v1}, Lajk;->a(Ljava/lang/Object;Ljava/lang/Object;)Lajk;

    move-result-object v0

    sput-object v0, LSQ;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LKS;Landroid/content/ContentResolver;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, LSQ;->a:LKS;

    .line 43
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, LSQ;->a:Landroid/content/ContentResolver;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 56
    .line 59
    sget-object v0, LSQ;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 60
    invoke-virtual {p3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 61
    const-string v3, "MimeTypeHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Hardcoding mimetype to "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v1, LSQ;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    sget-object v1, LSQ;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object p2, v0

    goto :goto_a

    .line 68
    :cond_46
    if-eqz p2, :cond_50

    const-string v0, "/*"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 70
    :cond_50
    const-string v0, "MimeTypeHelper"

    const-string v1, "Could not get mime type -- will infer it"

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, LSQ;->a:Landroid/content/ContentResolver;

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_5d
    if-eqz v0, :cond_67

    iget-object v1, p0, LSQ;->a:LKS;

    invoke-static {v1, v0}, LaaA;->a(LKS;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6f

    .line 75
    :cond_67
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    :cond_6f
    if-eqz v0, :cond_79

    iget-object v1, p0, LSQ;->a:LKS;

    invoke-static {v1, v0}, LaaA;->a(LKS;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_83

    .line 79
    :cond_79
    iget-object v0, p0, LSQ;->a:LKS;

    const-string v1, "defaultUploadMimeType"

    const-string v2, "application/octet-stream"

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    :cond_83
    return-object v0

    :cond_84
    move-object v0, p2

    goto :goto_5d
.end method
