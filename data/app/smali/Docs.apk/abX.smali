.class public final enum LabX;
.super Ljava/lang/Enum;
.source "LinearLayoutListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LabX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LabX;

.field private static final synthetic a:[LabX;

.field public static final enum b:LabX;


# instance fields
.field private final a:I

.field private final a:Lacc;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 107
    new-instance v0, LabX;

    const-string v1, "HORIZONTAL"

    new-instance v5, LabY;

    invoke-direct {v5}, LabY;-><init>()V

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, LabX;-><init>(Ljava/lang/String;IIILacc;)V

    sput-object v0, LabX;->a:LabX;

    .line 124
    new-instance v3, LabX;

    const-string v4, "VERTICAL"

    new-instance v8, Laca;

    invoke-direct {v8}, Laca;-><init>()V

    move v5, v9

    move v6, v9

    move v7, v9

    invoke-direct/range {v3 .. v8}, LabX;-><init>(Ljava/lang/String;IIILacc;)V

    sput-object v3, LabX;->b:LabX;

    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [LabX;

    sget-object v1, LabX;->a:LabX;

    aput-object v1, v0, v2

    sget-object v1, LabX;->b:LabX;

    aput-object v1, v0, v9

    sput-object v0, LabX;->a:[LabX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILacc;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lacc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 151
    iput p3, p0, LabX;->a:I

    .line 152
    iput p4, p0, LabX;->b:I

    .line 153
    iput-object p5, p0, LabX;->a:Lacc;

    .line 154
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 161
    iget v0, p0, LabX;->a:I

    return v0
.end method

.method private a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 165
    iget v0, p0, LabX;->b:I

    if-nez v0, :cond_5

    :goto_4
    return p1

    :cond_5
    move p1, p2

    goto :goto_4
.end method

.method public static synthetic a(LabX;)I
    .registers 2
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, LabX;->b()I

    move-result v0

    return v0
.end method

.method public static synthetic a(LabX;II)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, LabX;->a(II)I

    move-result v0

    return v0
.end method

.method public static synthetic a(LabX;Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Lacd;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, LabX;->a(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Lacd;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Lacd;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, LabX;->a:Lacc;

    invoke-interface {v0, p1, p2}, Lacc;->a(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Lacd;

    move-result-object v0

    return-object v0
.end method

.method private b()I
    .registers 2

    .prologue
    .line 169
    iget v0, p0, LabX;->b:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public static synthetic b(LabX;)I
    .registers 2
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, LabX;->c()I

    move-result v0

    return v0
.end method

.method private c()I
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 173
    iget v1, p0, LabX;->b:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public static synthetic c(LabX;)I
    .registers 2
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, LabX;->a()I

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LabX;
    .registers 2
    .parameter

    .prologue
    .line 106
    const-class v0, LabX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LabX;

    return-object v0
.end method

.method public static values()[LabX;
    .registers 1

    .prologue
    .line 106
    sget-object v0, LabX;->a:[LabX;

    invoke-virtual {v0}, [LabX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LabX;

    return-object v0
.end method
