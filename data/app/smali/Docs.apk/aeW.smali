.class public LaeW;
.super LaeP;
.source "GsonFactory.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 44
    invoke-direct {p0}, LaeP;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)LaeQ;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 73
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1, p2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-virtual {p0, v0}, LaeW;->a(Ljava/io/Writer;)LaeQ;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/Writer;)LaeQ;
    .registers 4
    .parameter

    .prologue
    .line 78
    new-instance v0, LaeX;

    new-instance v1, Laoa;

    invoke-direct {v1, p1}, Laoa;-><init>(Ljava/io/Writer;)V

    invoke-direct {v0, p0, v1}, LaeX;-><init>(LaeW;Laoa;)V

    return-object v0
.end method

.method public a(Ljava/io/InputStream;)LaeS;
    .registers 4
    .parameter

    .prologue
    .line 50
    new-instance v0, Ljava/io/InputStreamReader;

    sget-object v1, Lagg;->c:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-virtual {p0, v0}, LaeW;->a(Ljava/io/Reader;)LaeS;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;Ljava/nio/charset/Charset;)LaeS;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 55
    if-nez p2, :cond_7

    .line 56
    invoke-virtual {p0, p1}, LaeW;->a(Ljava/io/InputStream;)LaeS;

    move-result-object v0

    .line 58
    :goto_6
    return-object v0

    :cond_7
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-virtual {p0, v0}, LaeW;->a(Ljava/io/Reader;)LaeS;

    move-result-object v0

    goto :goto_6
.end method

.method public a(Ljava/io/Reader;)LaeS;
    .registers 4
    .parameter

    .prologue
    .line 68
    new-instance v0, LaeY;

    new-instance v1, LanV;

    invoke-direct {v1, p1}, LanV;-><init>(Ljava/io/Reader;)V

    invoke-direct {v0, p0, v1}, LaeY;-><init>(LaeW;LanV;)V

    return-object v0
.end method
