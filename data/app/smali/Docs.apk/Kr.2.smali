.class final LKr;
.super Ljava/lang/Object;
.source "SwitchableQueue.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LKr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:LKp;

.field private final a:LKs;


# direct methods
.method private constructor <init>(LKp;LKs;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, LKr;->a:LKp;

    .line 153
    iput-object p2, p0, LKr;->a:LKs;

    .line 154
    iput p3, p0, LKr;->a:I

    .line 155
    return-void
.end method

.method synthetic constructor <init>(LKp;LKs;ILKm;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 143
    invoke-direct {p0, p1, p2, p3}, LKr;-><init>(LKp;LKs;I)V

    return-void
.end method

.method private a()LKp;
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, LKr;->a:LKp;

    return-object v0
.end method

.method static synthetic a(LKr;)LKp;
    .registers 2
    .parameter

    .prologue
    .line 143
    invoke-direct {p0}, LKr;->a()LKp;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LKr;)LKs;
    .registers 2
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, LKr;->a:LKs;

    return-object v0
.end method


# virtual methods
.method public a(LKr;)I
    .registers 4
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, LKr;->a:LKs;

    iget-object v1, p1, LKr;->a:LKs;

    if-eq v0, v1, :cond_14

    .line 161
    iget-object v0, p1, LKr;->a:LKs;

    invoke-virtual {v0}, LKs;->a()I

    move-result v0

    iget-object v1, p0, LKr;->a:LKs;

    invoke-virtual {v1}, LKs;->a()I

    move-result v1

    sub-int/2addr v0, v1

    .line 164
    :goto_13
    return v0

    :cond_14
    iget v0, p0, LKr;->a:I

    iget v1, p1, LKr;->a:I

    sub-int/2addr v0, v1

    goto :goto_13
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 143
    check-cast p1, LKr;

    invoke-virtual {p0, p1}, LKr;->a(LKr;)I

    move-result v0

    return v0
.end method
