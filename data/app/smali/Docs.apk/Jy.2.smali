.class public final LJy;
.super Ljava/lang/Object;
.source "SpreadsheetEvent.java"


# static fields
.field private static a:I

.field public static final a:LJy;

.field private static a:[LJy;

.field public static final b:LJy;

.field public static final c:LJy;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 47
    new-instance v0, LJy;

    const-string v1, "kSpreadsheetInitialized"

    invoke-direct {v0, v1}, LJy;-><init>(Ljava/lang/String;)V

    sput-object v0, LJy;->a:LJy;

    .line 48
    new-instance v0, LJy;

    const-string v1, "kSpreadsheetPartAvailable"

    invoke-direct {v0, v1}, LJy;-><init>(Ljava/lang/String;)V

    sput-object v0, LJy;->b:LJy;

    .line 49
    new-instance v0, LJy;

    const-string v1, "kSpreadsheetLoadComplete"

    invoke-direct {v0, v1}, LJy;-><init>(Ljava/lang/String;)V

    sput-object v0, LJy;->c:LJy;

    .line 85
    const/4 v0, 0x3

    new-array v0, v0, [LJy;

    sget-object v1, LJy;->a:LJy;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, LJy;->b:LJy;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LJy;->c:LJy;

    aput-object v2, v0, v1

    sput-object v0, LJy;->a:[LJy;

    .line 86
    sput v3, LJy;->a:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, LJy;->a:Ljava/lang/String;

    .line 70
    sget v0, LJy;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LJy;->a:I

    iput v0, p0, LJy;->b:I

    .line 71
    return-void
.end method

.method public static a(I)LJy;
    .registers 4
    .parameter

    .prologue
    .line 60
    sget-object v0, LJy;->a:[LJy;

    array-length v0, v0

    if-ge p0, v0, :cond_14

    if-ltz p0, :cond_14

    sget-object v0, LJy;->a:[LJy;

    aget-object v0, v0, p0

    iget v0, v0, LJy;->b:I

    if-ne v0, p0, :cond_14

    .line 61
    sget-object v0, LJy;->a:[LJy;

    aget-object v0, v0, p0

    .line 64
    :goto_13
    return-object v0

    .line 62
    :cond_14
    const/4 v0, 0x0

    :goto_15
    sget-object v1, LJy;->a:[LJy;

    array-length v1, v1

    if-ge v0, v1, :cond_2a

    .line 63
    sget-object v1, LJy;->a:[LJy;

    aget-object v1, v1, v0

    iget v1, v1, LJy;->b:I

    if-ne v1, p0, :cond_27

    .line 64
    sget-object v1, LJy;->a:[LJy;

    aget-object v0, v1, v0

    goto :goto_13

    .line 62
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 65
    :cond_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LJy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, LJy;->a:Ljava/lang/String;

    return-object v0
.end method
