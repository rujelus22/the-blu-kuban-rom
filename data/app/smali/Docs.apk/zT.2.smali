.class public LzT;
.super LEl;
.source "ZoomedLayout.java"


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(LEj;)V
    .registers 3
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, LEl;-><init>(LEj;)V

    .line 21
    const/high16 v0, 0x3f80

    iput v0, p0, LzT;->a:F

    .line 29
    return-void
.end method


# virtual methods
.method public a(I)F
    .registers 4
    .parameter

    .prologue
    .line 67
    invoke-super {p0, p1}, LEl;->a(I)F

    move-result v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    return v0
.end method

.method public a()I
    .registers 3

    .prologue
    .line 130
    invoke-super {p0}, LEl;->a()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public a(I)I
    .registers 4
    .parameter

    .prologue
    .line 97
    int-to-float v0, p1

    iget v1, p0, LzT;->a:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    invoke-super {p0, v0}, LEl;->a(I)I

    move-result v0

    return v0
.end method

.method public a(IF)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 102
    iget v0, p0, LzT;->a:F

    div-float v0, p2, v0

    invoke-super {p0, p1, v0}, LEl;->a(IF)I

    move-result v0

    return v0
.end method

.method public a(ILandroid/graphics/Rect;)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, LEl;->a(ILandroid/graphics/Rect;)I

    move-result v0

    .line 113
    if-eqz p2, :cond_2a

    .line 114
    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p0, LzT;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p2, Landroid/graphics/Rect;->left:I

    .line 115
    iget v1, p2, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v2, p0, LzT;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p2, Landroid/graphics/Rect;->right:I

    .line 116
    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget v2, p0, LzT;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p2, Landroid/graphics/Rect;->top:I

    .line 117
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iget v2, p0, LzT;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p2, Landroid/graphics/Rect;->bottom:I

    .line 120
    :cond_2a
    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public a(I)Landroid/util/Pair;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    invoke-super {p0, p1}, LEl;->a(I)Landroid/util/Pair;

    move-result-object v1

    .line 161
    new-instance v2, Landroid/util/Pair;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v3, p0, LzT;->a:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method public a(I)S
    .registers 4
    .parameter

    .prologue
    .line 107
    invoke-super {p0, p1}, LEl;->a(I)S

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 60
    iget v0, p0, LzT;->a:F

    iget v1, p0, LzT;->a:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 61
    invoke-super {p0, p1, p2, p3, p4}, LEl;->a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 62
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 63
    return-void
.end method

.method public a_(F)V
    .registers 2
    .parameter

    .prologue
    .line 46
    iput p1, p0, LzT;->a:F

    .line 47
    return-void
.end method

.method public b(I)F
    .registers 4
    .parameter

    .prologue
    .line 72
    invoke-super {p0, p1}, LEl;->b(I)F

    move-result v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    return v0
.end method

.method public b()I
    .registers 3

    .prologue
    .line 135
    invoke-super {p0}, LEl;->b()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public b(I)I
    .registers 4
    .parameter

    .prologue
    .line 125
    invoke-super {p0, p1}, LEl;->b(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public b(I)S
    .registers 4
    .parameter

    .prologue
    .line 172
    invoke-super {p0, p1}, LEl;->b(I)S

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method

.method public b(F)V
    .registers 2
    .parameter

    .prologue
    .line 54
    invoke-virtual {p0, p1}, LzT;->a(F)V

    .line 55
    return-void
.end method

.method public c(I)F
    .registers 4
    .parameter

    .prologue
    .line 77
    invoke-super {p0, p1}, LEl;->c(I)F

    move-result v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public c()I
    .registers 3

    .prologue
    .line 167
    invoke-super {p0}, LEl;->c()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public c(I)I
    .registers 4
    .parameter

    .prologue
    .line 140
    invoke-super {p0, p1}, LEl;->c(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public d(I)F
    .registers 4
    .parameter

    .prologue
    .line 82
    invoke-super {p0, p1}, LEl;->d(I)F

    move-result v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public d(I)I
    .registers 4
    .parameter

    .prologue
    .line 145
    invoke-super {p0, p1}, LEl;->d(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public e(I)F
    .registers 4
    .parameter

    .prologue
    .line 92
    invoke-super {p0, p1}, LEl;->e(I)F

    move-result v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public e(I)I
    .registers 4
    .parameter

    .prologue
    .line 150
    invoke-super {p0, p1}, LEl;->e(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public f(I)I
    .registers 4
    .parameter

    .prologue
    .line 197
    invoke-super {p0, p1}, LEl;->f(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LzT;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
