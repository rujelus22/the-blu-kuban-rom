.class public LNg;
.super Ljava/lang/Object;
.source "AuthTokenManagerImpl.java"

# interfaces
.implements LNe;


# annotations
.annotation runtime LaoJ;
.end annotation


# instance fields
.field private final a:LKS;

.field private final a:LNK;

.field private final a:LNb;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LNi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LNb;LNK;LKS;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LNg;->a:Ljava/util/Map;

    .line 155
    iput-object p1, p0, LNg;->a:LNb;

    .line 156
    iput-object p2, p0, LNg;->a:LNK;

    .line 157
    iput-object p3, p0, LNg;->a:LKS;

    .line 158
    return-void
.end method

.method static synthetic a(LNg;)LNb;
    .registers 2
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, LNg;->a:LNb;

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/String;)LNi;
    .registers 4
    .parameter

    .prologue
    .line 203
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LNg;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNi;

    .line 204
    if-nez v0, :cond_15

    .line 205
    new-instance v0, LNi;

    invoke-direct {v0, p0, p1}, LNi;-><init>(LNg;Ljava/lang/String;)V

    .line 206
    iget-object v1, p0, LNg;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 208
    :cond_15
    monitor-exit p0

    return-object v0

    .line 203
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 259
    const-string v0, "https://www.google.com/accounts/TokenAuth"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 261
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LNg;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 263
    const-string v2, "auth"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 264
    const-string v1, "continue"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 265
    if-eqz p3, :cond_20

    .line 266
    const-string v1, "service"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 268
    :cond_20
    const-string v1, "source"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 270
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;I)Ljava/lang/String;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 214
    if-lez p2, :cond_76

    :goto_4
    invoke-static {v0}, Lagu;->a(Z)V

    .line 216
    iget-object v0, p0, LNg;->a:LNb;

    invoke-interface {v0}, LNb;->a()V

    .line 218
    :goto_c
    add-int/lit8 v1, v1, 0x1

    if-gt v1, p2, :cond_c9

    .line 219
    :try_start_10
    const-string v0, "LSID"

    invoke-virtual {p0, p1, v0}, LNg;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    const-string v2, "SID"

    invoke-virtual {p0, p1, v2}, LNg;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 222
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    const-string v4, "https://www.google.com/accounts/IssueAuthToken"

    invoke-direct {v3, v4}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 223
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 224
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "SID"

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "LSID"

    invoke-direct {v2, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "service"

    const-string v5, "gaia"

    invoke-direct {v0, v2, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    new-instance v0, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v0, v4}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    .line 228
    invoke-virtual {v3, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_50
    .catchall {:try_start_10 .. :try_end_50} :catchall_b9

    .line 230
    :try_start_50
    iget-object v0, p0, LNg;->a:LNK;

    invoke-interface {v0, v3}, LNK;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 232
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_61
    .catchall {:try_start_50 .. :try_end_61} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_61} :catch_c0

    move-result-object v0

    .line 233
    if-nez v0, :cond_78

    const/4 v0, 0x0

    .line 234
    :goto_65
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_7d

    if-eqz v0, :cond_7d

    .line 247
    :try_start_6b
    iget-object v1, p0, LNg;->a:LNK;

    invoke-interface {v1}, LNK;->b()V
    :try_end_70
    .catchall {:try_start_6b .. :try_end_70} :catchall_b9

    .line 252
    iget-object v1, p0, LNg;->a:LNb;

    invoke-interface {v1}, LNb;->b()V

    return-object v0

    :cond_76
    move v0, v1

    .line 214
    goto :goto_4

    .line 233
    :cond_78
    :try_start_78
    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_65

    .line 237
    :cond_7d
    const-string v3, "SID"

    invoke-virtual {p0, p1, v3}, LNg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v3, "LSID"

    invoke-virtual {p0, p1, v3}, LNg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const-string v3, "AuthTokenManagerImpl"

    const-string v4, "getUberAuthToken attempt #%d/%d: statusCode %d, entityString=%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x3

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v0, p0, LNg;->a:LNK;

    invoke-interface {v0}, LNK;->a()V
    :try_end_b2
    .catchall {:try_start_78 .. :try_end_b2} :catchall_c2
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_b2} :catch_c0

    .line 247
    :try_start_b2
    iget-object v0, p0, LNg;->a:LNK;

    invoke-interface {v0}, LNK;->b()V
    :try_end_b7
    .catchall {:try_start_b2 .. :try_end_b7} :catchall_b9

    goto/16 :goto_c

    .line 252
    :catchall_b9
    move-exception v0

    iget-object v1, p0, LNg;->a:LNb;

    invoke-interface {v1}, LNb;->b()V

    throw v0

    .line 244
    :catch_c0
    move-exception v0

    .line 245
    :try_start_c1
    throw v0
    :try_end_c2
    .catchall {:try_start_c1 .. :try_end_c2} :catchall_c2

    .line 247
    :catchall_c2
    move-exception v0

    :try_start_c3
    iget-object v1, p0, LNg;->a:LNK;

    invoke-interface {v1}, LNK;->b()V

    throw v0

    .line 250
    :cond_c9
    new-instance v0, LNt;

    invoke-direct {v0}, LNt;-><init>()V

    throw v0
    :try_end_cf
    .catchall {:try_start_c3 .. :try_end_cf} :catchall_b9
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 169
    invoke-direct {p0, p1}, LNg;->a(Ljava/lang/String;)LNi;

    move-result-object v0

    .line 170
    invoke-virtual {v0, p2}, LNi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 275
    iget-object v0, p0, LNg;->a:LNb;

    invoke-interface {v0}, LNb;->a()V

    .line 276
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 285
    iget-object v0, p0, LNg;->a:LNb;

    invoke-interface {v0}, LNb;->a()V

    .line 288
    :try_start_5
    const-string v0, "LSID"

    invoke-virtual {p0, p1, v0}, LNg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v0, "SID"

    invoke-virtual {p0, p1, v0}, LNg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-string v0, "LSID"

    invoke-virtual {p0, p1, v0}, LNg;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v0, "SID"

    invoke-virtual {p0, p1, v0}, LNg;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_5 .. :try_end_19} :catchall_1f

    .line 294
    iget-object v0, p0, LNg;->a:LNb;

    invoke-interface {v0}, LNb;->b()V

    .line 296
    return-void

    .line 294
    :catchall_1f
    move-exception v0

    iget-object v1, p0, LNg;->a:LNb;

    invoke-interface {v1}, LNb;->b()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 184
    invoke-direct {p0, p1}, LNg;->a(Ljava/lang/String;)LNi;

    move-result-object v0

    .line 185
    invoke-virtual {v0, p2}, LNi;->a(Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LNf;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 301
    iget-object v0, p0, LNg;->a:LNb;

    invoke-interface {v0}, LNb;->a()V

    .line 302
    iget-object v0, p0, LNg;->a:LNb;

    new-instance v1, LNh;

    invoke-direct {v1, p0, p3}, LNh;-><init>(LNg;LNf;)V

    invoke-interface {v0, p1, p2, v1}, LNb;->a(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/AccountManagerCallback;)V

    .line 331
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 280
    iget-object v0, p0, LNg;->a:LNb;

    invoke-interface {v0}, LNb;->b()V

    .line 281
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 162
    invoke-direct {p0, p1}, LNg;->a(Ljava/lang/String;)LNi;

    move-result-object v0

    .line 163
    invoke-virtual {v0, p2}, LNi;->b(Ljava/lang/String;)V

    .line 164
    return-void
.end method
