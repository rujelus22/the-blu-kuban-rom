.class public LIr;
.super Ljava/lang/Object;
.source "TrixNativeGridView.java"

# interfaces
.implements LIu;


# instance fields
.field final synthetic a:F

.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;F)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 324
    iput-object p1, p0, LIr;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iput p2, p0, LIr;->a:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IIFFLHR;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 329
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, LIr;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 330
    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 331
    invoke-virtual {p5}, LHR;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 332
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 333
    invoke-static {v0, v2}, LaS;->a(Landroid/view/View;I)V

    .line 334
    iget v1, p0, LIr;->a:F

    mul-float/2addr v1, p3

    float-to-int v1, v1

    iget v2, p0, LIr;->a:F

    mul-float/2addr v2, p4

    float-to-int v2, v2

    float-to-double v3, p3

    invoke-virtual {p5}, LHR;->a()D

    move-result-wide v5

    add-double/2addr v3, v5

    iget v5, p0, LIr;->a:F

    float-to-double v5, v5

    mul-double/2addr v3, v5

    double-to-int v3, v3

    float-to-double v4, p4

    invoke-virtual {p5}, LHR;->b()D

    move-result-wide v6

    add-double/2addr v4, v6

    iget v6, p0, LIr;->a:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 337
    iget-object v1, p0, LIr;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->addView(Landroid/view/View;)V

    .line 338
    iget-object v1, p0, LIr;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)Ljava/util/Map;

    move-result-object v1

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, p2, p1}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    return-void
.end method
