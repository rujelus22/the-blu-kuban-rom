.class public LDp;
.super Ljava/lang/Object;
.source "PresentedEditable.java"

# interfaces
.implements LEg;


# instance fields
.field private final a:LDG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDG",
            "<*>;"
        }
    .end annotation
.end field

.field private a:LDu;

.field private final a:LDw;


# direct methods
.method public constructor <init>(LDG;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDG",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, LDp;->a:LDu;

    .line 90
    iput-object p1, p0, LDp;->a:LDG;

    .line 91
    new-instance v0, LDw;

    invoke-direct {v0, p0}, LDw;-><init>(Landroid/text/Spannable;)V

    iput-object v0, p0, LDp;->a:LDw;

    .line 92
    return-void
.end method

.method static synthetic a(LDp;)LDG;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LDp;->a:LDG;

    return-object v0
.end method


# virtual methods
.method public a(ILjava/lang/Object;)C
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 220
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0, p1, p2}, LDG;->a(ILjava/lang/Object;)C

    move-result v0

    return v0
.end method

.method public a(IILjava/lang/Class;)I
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Class",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, p1, p2, p3}, LDw;->a(IILjava/lang/Class;)I

    move-result v0

    return v0
.end method

.method public a(IILjava/lang/Class;Ljava/lang/Object;)I
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 309
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, p1, p2, p3}, LDw;->a(IILjava/lang/Class;)I

    move-result v0

    .line 310
    iget-object v1, p0, LDp;->a:LDG;

    invoke-virtual {v1}, LDG;->b()I

    move-result v1

    .line 311
    iget-object v2, p0, LDp;->a:LDG;

    invoke-virtual {v2}, LDG;->a()LDJ;

    move-result-object v2

    invoke-interface {v2}, LDJ;->a()LDG;

    move-result-object v2

    .line 312
    add-int v3, v1, p1

    add-int/2addr v0, v1

    invoke-virtual {v2, v3, v0, p3, p4}, LDG;->a(IILjava/lang/Class;Ljava/lang/Object;)I

    move-result v0

    sub-int/2addr v0, v1

    .line 314
    return v0
.end method

.method public a()V
    .registers 6

    .prologue
    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    iget-object v1, p0, LDp;->a:LDw;

    const/4 v2, 0x0

    invoke-virtual {p0}, LDp;->length()I

    move-result v3

    const-class v4, LDS;

    invoke-virtual {v1, v0, v2, v3, v4}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 110
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDS;

    .line 111
    invoke-interface {v0, p0}, LDS;->a(Ljava/lang/CharSequence;)V

    goto :goto_15

    .line 113
    :cond_25
    return-void
.end method

.method a(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 330
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, p1, p2, p3}, LDw;->a(III)V

    .line 331
    return-void
.end method

.method public a(II[CILjava/lang/Object;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 239
    iget-object v0, p0, LDp;->a:LDG;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LDG;->a(II[CILjava/lang/Object;)V

    .line 240
    return-void
.end method

.method public a(LDu;)V
    .registers 2
    .parameter

    .prologue
    .line 100
    iput-object p1, p0, LDp;->a:LDu;

    .line 101
    return-void
.end method

.method public a(Ljava/util/List;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDt;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 335
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 336
    iget-object v1, p0, LDp;->a:LDw;

    const-class v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v0, p2, p3, v2}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 337
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 338
    new-instance v2, LDq;

    invoke-direct {v2, p0, v0}, LDq;-><init>(LDp;Landroid/text/TextWatcher;)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 358
    :cond_25
    return-void
.end method

.method public a(IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    .line 400
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 401
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, v1, p1, p2, p3}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 404
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 405
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(IILjava/lang/Class;Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")[TT;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0}, LDG;->b()I

    move-result v3

    .line 289
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 290
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0}, LDG;->a()LDJ;

    move-result-object v0

    invoke-interface {v0}, LDJ;->a()LDG;

    move-result-object v0

    .line 291
    add-int v2, v3, p1

    add-int/2addr v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LDG;->a(Ljava/util/List;IILjava/lang/Class;Ljava/lang/Object;)V

    .line 294
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, v1, p1, p2, p3}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 298
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 299
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public append(C)Landroid/text/Editable;
    .registers 3
    .parameter

    .prologue
    .line 154
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LDp;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public append(Ljava/lang/CharSequence;)Landroid/text/Editable;
    .registers 8
    .parameter

    .prologue
    .line 148
    invoke-virtual {p0}, LDp;->length()I

    move-result v1

    .line 149
    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move-object v0, p0

    move v2, v1

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LDp;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public append(Ljava/lang/CharSequence;II)Landroid/text/Editable;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    invoke-virtual {p0}, LDp;->length()I

    move-result v1

    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move v4, p2

    move v5, p3

    .line 160
    invoke-virtual/range {v0 .. v5}, LDp;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(C)Ljava/lang/Appendable;
    .registers 3
    .parameter

    .prologue
    .line 23
    invoke-virtual {p0, p1}, LDp;->append(C)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .registers 3
    .parameter

    .prologue
    .line 23
    invoke-virtual {p0, p1}, LDp;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2, p3}, LDp;->append(Ljava/lang/CharSequence;II)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .registers 6

    .prologue
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 121
    iget-object v1, p0, LDp;->a:LDw;

    const/4 v2, 0x0

    invoke-virtual {p0}, LDp;->length()I

    move-result v3

    const-class v4, LDS;

    invoke-virtual {v1, v0, v2, v3, v4}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 122
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDS;

    .line 123
    invoke-interface {v0, p0}, LDS;->b(Ljava/lang/CharSequence;)V

    goto :goto_15

    .line 125
    :cond_25
    return-void
.end method

.method public b(Ljava/util/List;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 363
    iget-object v1, p0, LDp;->a:LDw;

    const-class v2, Landroid/text/SpanWatcher;

    invoke-virtual {v1, v0, p2, p3, v2}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 364
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpanWatcher;

    .line 365
    new-instance v2, LDr;

    invoke-direct {v2, p0, v0}, LDr;-><init>(LDp;Landroid/text/SpanWatcher;)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 396
    :cond_25
    return-void
.end method

.method public c()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 131
    invoke-virtual {p0}, LDp;->length()I

    move-result v1

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    iget-object v2, p0, LDp;->a:LDw;

    const-class v3, Landroid/text/TextWatcher;

    invoke-virtual {v2, v0, v4, v1, v3}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 134
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 137
    invoke-interface {v0, p0, v4, v1, v1}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    goto :goto_15

    .line 139
    :cond_25
    return-void
.end method

.method public charAt(I)C
    .registers 3
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0, p1}, LDG;->charAt(I)C

    move-result v0

    return v0
.end method

.method public clear()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-virtual {p0}, LDp;->length()I

    move-result v2

    const-string v3, ""

    move-object v0, p0

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, LDp;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 166
    return-void
.end method

.method public clearSpans()V
    .registers 3

    .prologue
    .line 170
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "PresentedEditable does not yet support clearing all spans"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public delete(II)Landroid/text/Editable;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 176
    const-string v3, ""

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, LDp;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public getChars(II[CI)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 235
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0, p1, p2, p3, p4}, LDG;->getChars(II[CI)V

    .line 236
    return-void
.end method

.method public getFilters()[Landroid/text/InputFilter;
    .registers 2

    .prologue
    .line 181
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/text/InputFilter;

    return-object v0
.end method

.method public getSpanEnd(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 258
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->c(Ljava/lang/Object;)I

    move-result v0

    .line 259
    if-gez v0, :cond_e

    .line 260
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0, p1}, LDG;->b(Ljava/lang/Object;)I

    move-result v0

    .line 262
    :cond_e
    return v0
.end method

.method public getSpanFlags(Ljava/lang/Object;)I
    .registers 4
    .parameter

    .prologue
    .line 267
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->a(Ljava/lang/Object;)I

    move-result v0

    .line 268
    if-nez v0, :cond_1c

    .line 269
    iget-object v1, p0, LDp;->a:LDG;

    invoke-virtual {v1}, LDG;->a()LDJ;

    move-result-object v1

    invoke-interface {v1}, LDJ;->a()LDy;

    move-result-object v1

    invoke-virtual {v1, p1}, LDy;->a(Ljava/lang/Object;)LDG;

    move-result-object v1

    .line 270
    if-eqz v1, :cond_1c

    .line 271
    invoke-virtual {v1, p1}, LDG;->getSpanFlags(Ljava/lang/Object;)I

    move-result v0

    .line 274
    :cond_1c
    return v0
.end method

.method public getSpanStart(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 279
    iget-object v0, p0, LDp;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->b(Ljava/lang/Object;)I

    move-result v0

    .line 280
    if-gez v0, :cond_e

    .line 281
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0, p1}, LDG;->a(Ljava/lang/Object;)I

    move-result v0

    .line 283
    :cond_e
    return v0
.end method

.method public getSpans(IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    .line 304
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LDp;->a(IILjava/lang/Class;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public insert(ILjava/lang/CharSequence;)Landroid/text/Editable;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 186
    const/4 v4, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move-object v0, p0

    move v1, p1

    move v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LDp;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public insert(ILjava/lang/CharSequence;II)Landroid/text/Editable;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    move-object v0, p0

    move v1, p1

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LDp;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public length()I
    .registers 2

    .prologue
    .line 225
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0}, LDG;->length()I

    move-result v0

    return v0
.end method

.method public nextSpanTransition(IILjava/lang/Class;)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 319
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LDp;->a(IILjava/lang/Class;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public removeSpan(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 244
    iget-object v0, p0, LDp;->a:LDw;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LDw;->a(Ljava/lang/Object;Z)V

    .line 245
    return-void
.end method

.method public replace(IILjava/lang/CharSequence;)Landroid/text/Editable;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 196
    const/4 v4, 0x0

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LDp;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 201
    iget-object v0, p0, LDp;->a:LDu;

    if-eqz v0, :cond_e

    .line 202
    iget-object v0, p0, LDp;->a:LDu;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, LDu;->a(IILjava/lang/CharSequence;II)V

    .line 204
    :cond_e
    return-object p0
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .registers 4
    .parameter

    .prologue
    .line 209
    array-length v0, p1

    if-lez v0, :cond_b

    .line 210
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "StringTree does not support input filters."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_b
    return-void
.end method

.method public setSpan(Ljava/lang/Object;III)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 253
    iget-object v0, p0, LDp;->a:LDw;

    const/4 v5, 0x1

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LDw;->a(Ljava/lang/Object;IIIZ)V

    .line 254
    return-void
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0, p1, p2}, LDG;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, LDp;->a:LDG;

    invoke-virtual {v0}, LDG;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
