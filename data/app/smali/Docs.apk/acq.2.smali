.class public Lacq;
.super Ljava/lang/Object;
.source "SwipeIntentDetector.java"


# instance fields
.field private a:F

.field private a:I

.field private a:J

.field private final a:Lacr;

.field private a:Z

.field private b:F

.field private b:J

.field private c:F

.field private d:F

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(Lacr;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v0, p0, Lacq;->a:Z

    .line 59
    iput v0, p0, Lacq;->a:I

    .line 80
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iput-object p1, p0, Lacq;->a:Lacr;

    .line 82
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Lacq;->a:Z

    .line 176
    return-void
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 168
    iget v0, p0, Lacq;->a:I

    if-eq p1, v0, :cond_b

    .line 169
    iput p1, p0, Lacq;->a:I

    .line 170
    iget-object v0, p0, Lacq;->a:Lacr;

    invoke-interface {v0, p1}, Lacr;->a(I)V

    .line 172
    :cond_b
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 85
    const-string v0, "SwipeIntentDetector"

    const-string v1, "onStartMove"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lacq;->a:Z

    .line 87
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lacq;->a:F

    .line 88
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lacq;->b:F

    .line 89
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lacq;->b:J

    .line 90
    iput v2, p0, Lacq;->e:F

    .line 91
    iput v2, p0, Lacq;->f:F

    .line 92
    iget v0, p0, Lacq;->a:F

    iput v0, p0, Lacq;->c:F

    .line 93
    iget v0, p0, Lacq;->b:F

    iput v0, p0, Lacq;->d:F

    .line 94
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lacq;->a:J

    .line 95
    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)V
    .registers 15
    .parameter

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 106
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 108
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_b0

    .line 165
    :cond_f
    :goto_f
    return-void

    .line 110
    :pswitch_10
    iget-boolean v0, p0, Lacq;->a:Z

    if-eqz v0, :cond_f

    .line 114
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 116
    iget v0, p0, Lacq;->c:F

    sub-float v3, v1, v0

    .line 117
    iget v0, p0, Lacq;->d:F

    sub-float v4, v2, v0

    .line 118
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    .line 119
    iget-wide v7, p0, Lacq;->a:J

    sub-long v7, v5, v7

    .line 121
    float-to-double v9, v3

    const-wide/16 v11, 0x0

    cmpl-double v0, v9, v11

    if-nez v0, :cond_3a

    float-to-double v9, v4

    const-wide/16 v11, 0x0

    cmpl-double v0, v9, v11

    if-eqz v0, :cond_78

    :cond_3a
    const/4 v0, 0x1

    .line 123
    :goto_3b
    if-nez v0, :cond_43

    const-wide/16 v9, 0x32

    cmp-long v9, v7, v9

    if-lez v9, :cond_4f

    .line 124
    :cond_43
    iput-wide v7, p0, Lacq;->b:J

    .line 125
    iput v3, p0, Lacq;->e:F

    .line 126
    iput v4, p0, Lacq;->f:F

    .line 127
    iput v1, p0, Lacq;->c:F

    .line 128
    iput v2, p0, Lacq;->d:F

    .line 129
    iput-wide v5, p0, Lacq;->a:J

    .line 132
    :cond_4f
    if-eqz v0, :cond_f

    .line 136
    iget v0, p0, Lacq;->e:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 137
    iget v1, p0, Lacq;->f:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 139
    cmpl-float v2, v0, v1

    if-lez v2, :cond_7c

    .line 140
    iget-wide v1, p0, Lacq;->b:J

    long-to-float v1, v1

    div-float/2addr v0, v1

    .line 141
    const v1, 0x3dcccccd

    cmpl-float v0, v0, v1

    if-lez v0, :cond_f

    .line 142
    iget v0, p0, Lacq;->e:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7a

    const/4 v0, 0x1

    .line 143
    :goto_74
    invoke-direct {p0, v0}, Lacq;->a(I)V

    goto :goto_f

    .line 121
    :cond_78
    const/4 v0, 0x0

    goto :goto_3b

    .line 142
    :cond_7a
    const/4 v0, -0x1

    goto :goto_74

    .line 146
    :cond_7c
    iget-wide v2, p0, Lacq;->b:J

    long-to-float v0, v2

    div-float v0, v1, v0

    .line 147
    const v1, 0x3dcccccd

    cmpl-float v0, v0, v1

    if-lez v0, :cond_f

    .line 148
    iget v0, p0, Lacq;->f:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_95

    const/4 v0, 0x1

    .line 149
    :goto_90
    invoke-direct {p0, v0}, Lacq;->a(I)V

    goto/16 :goto_f

    .line 148
    :cond_95
    const/4 v0, -0x1

    goto :goto_90

    .line 155
    :pswitch_97
    invoke-direct {p0, p1}, Lacq;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_f

    .line 158
    :pswitch_9c
    invoke-direct {p0}, Lacq;->a()V

    .line 159
    iget v0, p0, Lacq;->a:I

    if-eqz v0, :cond_f

    .line 160
    iget-object v0, p0, Lacq;->a:Lacr;

    iget v1, p0, Lacq;->a:I

    invoke-interface {v0, v1}, Lacr;->b(I)V

    .line 161
    const/4 v0, 0x0

    iput v0, p0, Lacq;->a:I

    goto/16 :goto_f

    .line 108
    nop

    :pswitch_data_b0
    .packed-switch 0x0
        :pswitch_97
        :pswitch_9c
        :pswitch_10
    .end packed-switch
.end method
