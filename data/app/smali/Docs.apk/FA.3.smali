.class public LFA;
.super Ljava/lang/Object;
.source "TextView.java"

# interfaces
.implements LFr;


# instance fields
.field private a:LFx;

.field final synthetic a:Lcom/google/android/apps/docs/editors/text/TextView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 2
    .parameter

    .prologue
    .line 8536
    iput-object p1, p0, LFA;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 8536
    invoke-direct {p0, p1}, LFA;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    return-void
.end method

.method private a()LFu;
    .registers 4

    .prologue
    .line 8570
    iget-object v0, p0, LFA;->a:LFx;

    if-nez v0, :cond_e

    .line 8571
    new-instance v0, LFx;

    iget-object v1, p0, LFA;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LFx;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V

    iput-object v0, p0, LFA;->a:LFx;

    .line 8573
    :cond_e
    iget-object v0, p0, LFA;->a:LFx;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 8543
    invoke-direct {p0}, LFA;->a()LFu;

    move-result-object v0

    check-cast v0, LFx;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, LFx;->c(I)V

    .line 8544
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 8559
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .registers 3

    .prologue
    .line 8547
    invoke-direct {p0}, LFA;->a()LFu;

    move-result-object v0

    check-cast v0, LFx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LFx;->c(I)V

    .line 8548
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 8552
    iget-object v0, p0, LFA;->a:LFx;

    if-eqz v0, :cond_9

    .line 8553
    iget-object v0, p0, LFA;->a:LFx;

    invoke-virtual {v0}, LFx;->d()V

    .line 8555
    :cond_9
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 8578
    iget-object v0, p0, LFA;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 8579
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 8581
    iget-object v0, p0, LFA;->a:LFx;

    if-eqz v0, :cond_12

    iget-object v0, p0, LFA;->a:LFx;

    invoke-virtual {v0}, LFx;->f()V

    .line 8582
    :cond_12
    return-void
.end method

.method public onTouchModeChanged(Z)V
    .registers 2
    .parameter

    .prologue
    .line 8564
    if-nez p1, :cond_5

    .line 8565
    invoke-virtual {p0}, LFA;->c()V

    .line 8567
    :cond_5
    return-void
.end method
