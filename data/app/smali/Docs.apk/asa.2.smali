.class public final Lasa;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final account_background_dark:I = 0x7f090043

.field public static final account_background_docs_dark:I = 0x7f090041

.field public static final account_background_docs_lite:I = 0x7f090042

.field public static final account_background_lite:I = 0x7f090044

.field public static final action_bar_background_color:I = 0x7f09006b

.field public static final action_bar_text:I = 0x7f09006f

.field public static final blue_toolbar_underline:I = 0x7f09000d

.field public static final color_cell_selected_border:I = 0x7f090004

.field public static final color_dropdown_toggle_unselected:I = 0x7f090007

.field public static final color_dropdown_toggle_unselected_border:I = 0x7f090008

.field public static final comments_divider:I = 0x7f09002f

.field public static final comments_stream_background:I = 0x7f09002d

.field public static final comments_text:I = 0x7f090030

.field public static final comments_thread_background:I = 0x7f09002e

.field public static final create_entry_dialog_list_background:I = 0x7f09002c

.field public static final dark_gradient_blue:I = 0x7f090037

.field public static final dark_grey_background:I = 0x7f09000b

.field public static final default_list_divider:I = 0x7f09004e

.field public static final default_list_vertical_divider:I = 0x7f09004f

.field public static final discussion_active_docos_background:I = 0x7f090017

.field public static final discussion_border:I = 0x7f090015

.field public static final discussion_edit_button_background:I = 0x7f09001d

.field public static final discussion_edit_text_background:I = 0x7f09001e

.field public static final discussion_holder_background:I = 0x7f090016

.field public static final discussion_list_color_hint:I = 0x7f09001b

.field public static final discussion_resolved_docos_background:I = 0x7f090018

.field public static final discussion_text_button_background:I = 0x7f09001a

.field public static final discussion_text_button_color:I = 0x7f090019

.field public static final discussion_text_color:I = 0x7f09001c

.field public static final divider_on_black:I = 0x7f09000c

.field public static final doc_entry_background:I = 0x7f090031

.field public static final doclist_group_header_text:I = 0x7f090060

.field public static final doclist_horizontal_rule:I = 0x7f090061

.field public static final drag_knob_band:I = 0x7f090032

.field public static final dragshadow_background:I = 0x7f090063

.field public static final dragshadow_hover:I = 0x7f090065

.field public static final dragshadow_text:I = 0x7f090064

.field public static final drive_action_bar_background_color:I = 0x7f090069

.field public static final drive_action_bar_icon_color:I = 0x7f090068

.field public static final dropdown_menu_highlight:I = 0x7f090005

.field public static final dropdown_menu_highlight_border:I = 0x7f090006

.field public static final font_black:I = 0x7f09003e

.field public static final font_properties_element_selected_color:I = 0x7f090009

.field public static final font_properties_element_unselected_color:I = 0x7f09000a

.field public static final gf_foreground_content:I = 0x7f090002

.field public static final gf_header_background:I = 0x7f090000

.field public static final gf_shadow:I = 0x7f090001

.field public static final grey:I = 0x7f09003f

.field public static final grey_disabled_quick_action_icons:I = 0x7f090040

.field public static final grouper_title_background:I = 0x7f090038

.field public static final grouper_title_background_docs:I = 0x7f090039

.field public static final home_icons:I = 0x7f09003c

.field public static final home_icons_label:I = 0x7f09003d

.field public static final horizontal_rule_color:I = 0x7f090011

.field public static final horizontal_scroll_indicator:I = 0x7f090012

.field public static final kix_webview_background:I = 0x7f090062

.field public static final list_entry_activated:I = 0x7f090056

.field public static final list_entry_disabled_focused:I = 0x7f090057

.field public static final list_entry_focused:I = 0x7f090058

.field public static final list_entry_pressed:I = 0x7f090059

.field public static final lite_gradient_blue:I = 0x7f090033

.field public static final mid_13_gradient_blue:I = 0x7f090036

.field public static final mid_50_gradient_blue:I = 0x7f090035

.field public static final mid_59_gradient_blue:I = 0x7f090034

.field public static final migration_font_color:I = 0x7f090066

.field public static final navigation_line:I = 0x7f090055

.field public static final navigation_text:I = 0x7f09005a

.field public static final operation_dialog_error:I = 0x7f09002a

.field public static final operation_dialog_syncing:I = 0x7f09002b

.field public static final pick_entry_dialog_header_divider:I = 0x7f09006e

.field public static final pick_entry_dialog_header_text:I = 0x7f09006c

.field public static final pick_entry_dialog_selected:I = 0x7f09006d

.field public static final pin_button_text_color:I = 0x7f090067

.field public static final preview_horizontal_rule:I = 0x7f090052

.field public static final preview_primary_text:I = 0x7f090053

.field public static final preview_secondary_text:I = 0x7f090054

.field public static final progress_title_background:I = 0x7f09004b

.field public static final progress_title_foreground:I = 0x7f09004c

.field public static final punch_grey_background:I = 0x7f09005b

.field public static final punch_status_text_color:I = 0x7f09005c

.field public static final sharing_grouper_background:I = 0x7f09004a

.field public static final sharing_list_background:I = 0x7f090045

.field public static final sharing_list_email:I = 0x7f090048

.field public static final sharing_list_empty_text:I = 0x7f090047

.field public static final sharing_list_name:I = 0x7f090046

.field public static final sharing_list_role:I = 0x7f090049

.field public static final slide_thumbnail_activated:I = 0x7f09005d

.field public static final tablet_doclist_primary_text:I = 0x7f09005e

.field public static final tablet_doclist_secondary_text:I = 0x7f09005f

.field public static final thumbnail_border:I = 0x7f090051

.field public static final thumbnail_open_button_background:I = 0x7f090050

.field public static final title_background:I = 0x7f09003a

.field public static final title_dividers:I = 0x7f09003b

.field public static final tool_bar_highlight:I = 0x7f09000e

.field public static final toolbar_regular_background:I = 0x7f090014

.field public static final toolbar_selected_background:I = 0x7f090013

.field public static final trix_keyboard_bar_background:I = 0x7f09001f

.field public static final trix_keyboard_bar_textfield_background:I = 0x7f090020

.field public static final trix_sheet_default_background:I = 0x7f090021

.field public static final trix_sheets_active_tab_background:I = 0x7f090024

.field public static final trix_sheets_tab_background:I = 0x7f090023

.field public static final trix_sheets_tab_bar_background:I = 0x7f090022

.field public static final trix_sheets_tab_menu_background:I = 0x7f090026

.field public static final trix_sheets_tab_menu_divider:I = 0x7f090027

.field public static final trix_sheets_tab_menu_item_text:I = 0x7f090029

.field public static final trix_sheets_tab_menu_title:I = 0x7f090028

.field public static final trix_sheets_tab_text_color:I = 0x7f090025

.field public static final uneditable_background:I = 0x7f09000f

.field public static final uneditable_text:I = 0x7f090010

.field public static final upload_queue_text_color:I = 0x7f09006a

.field public static final white_color_cell_border:I = 0x7f090003

.field public static final widget_text:I = 0x7f09004d


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
