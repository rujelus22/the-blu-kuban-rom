.class public abstract LCC;
.super Ljava/lang/Object;
.source "NativeBaseRenderer.java"

# interfaces
.implements LCJ;
.implements LvK;


# instance fields
.field private a:LCD;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, LCC;->a:LCD;

    .line 129
    iput-object p1, p0, LCC;->a:Ljava/lang/String;

    .line 130
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .registers 5
    .parameter

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    if-eqz p0, :cond_d

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_d

    .line 161
    :try_start_9
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_c} :catch_e

    move-result v0

    .line 166
    :cond_d
    :goto_d
    return v0

    .line 162
    :catch_e
    move-exception v1

    .line 163
    const-string v1, "NativeBaseRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid background color: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d
.end method

.method private a()V
    .registers 3

    .prologue
    .line 151
    iget-object v0, p0, LCC;->a:LCD;

    if-nez v0, :cond_14

    .line 152
    new-instance v0, LCD;

    invoke-direct {v0}, LCD;-><init>()V

    iput-object v0, p0, LCC;->a:LCD;

    .line 153
    invoke-virtual {p0}, LCC;->a()LzU;

    move-result-object v0

    iget-object v1, p0, LCC;->a:LCD;

    invoke-interface {v0, v1}, LzU;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    :cond_14
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, LCC;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 176
    invoke-virtual {p0}, LCC;->a()LzU;

    move-result-object v0

    invoke-interface {v0, p1}, LzU;->setWidth(I)V

    .line 177
    return-void
.end method

.method public a(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 186
    invoke-virtual {p0}, LCC;->a()LzU;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, LzU;->setMargin(IIII)V

    .line 187
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 134
    invoke-direct {p0}, LCC;->a()V

    .line 135
    iget-object v0, p0, LCC;->a:LCD;

    invoke-static {p1}, LCC;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LCD;->a(I)V

    .line 136
    return-void
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 181
    invoke-virtual {p0}, LCC;->a()LzU;

    move-result-object v0

    invoke-interface {v0, p1}, LzU;->setHeight(I)V

    .line 182
    return-void
.end method

.method public b(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 171
    invoke-virtual {p0}, LCC;->a()LzU;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, LzU;->setPadding(IIII)V

    .line 172
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 146
    invoke-direct {p0}, LCC;->a()V

    .line 147
    iget-object v0, p0, LCC;->a:LCD;

    invoke-static {p1}, LCC;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LCD;->b(I)V

    .line 148
    return-void
.end method

.method public c(IIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 140
    invoke-direct {p0}, LCC;->a()V

    .line 141
    iget-object v0, p0, LCC;->a:LCD;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, LCD;->a(Landroid/graphics/Rect;)V

    .line 142
    return-void
.end method
