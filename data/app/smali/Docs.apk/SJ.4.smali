.class public LSJ;
.super Ljava/lang/Object;
.source "ExtraStreamDataSource.java"


# instance fields
.field private a:LSQ;

.field private a:LSR;

.field private a:LSW;

.field private a:Landroid/net/Uri;

.field private a:Ljava/lang/String;

.field private a:Z

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, LSJ;->a:Z

    return-void
.end method

.method static synthetic a(LSJ;)LSQ;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, LSJ;->a:LSQ;

    return-object v0
.end method

.method static synthetic a(LSJ;)LSR;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, LSJ;->a:LSR;

    return-object v0
.end method

.method static synthetic a(LSJ;)LSW;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, LSJ;->a:LSW;

    return-object v0
.end method

.method static synthetic a(LSJ;)Landroid/net/Uri;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, LSJ;->a:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic a(LSJ;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, LSJ;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LSJ;)Z
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-boolean v0, p0, LSJ;->a:Z

    return v0
.end method

.method static synthetic b(LSJ;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, LSJ;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()LSH;
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, LSJ;->a:Landroid/net/Uri;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, LSJ;->a:Ljava/lang/String;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, LSJ;->a:LSW;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, LSJ;->a:LSR;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, LSJ;->a:LSQ;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    new-instance v0, LSH;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LSH;-><init>(LSJ;LSI;)V

    return-object v0
.end method

.method public a(LSQ;)LSJ;
    .registers 2
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, LSJ;->a:LSQ;

    .line 66
    return-object p0
.end method

.method public a(LSR;)LSJ;
    .registers 2
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, LSJ;->a:LSR;

    .line 56
    return-object p0
.end method

.method public a(LSW;)LSJ;
    .registers 2
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, LSJ;->a:LSW;

    .line 51
    return-object p0
.end method

.method public a(Landroid/net/Uri;)LSJ;
    .registers 2
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, LSJ;->a:Landroid/net/Uri;

    .line 36
    return-object p0
.end method

.method public a(Ljava/lang/String;)LSJ;
    .registers 2
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, LSJ;->a:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public a(Z)LSJ;
    .registers 2
    .parameter

    .prologue
    .line 45
    iput-boolean p1, p0, LSJ;->a:Z

    .line 46
    return-object p0
.end method

.method public b(Ljava/lang/String;)LSJ;
    .registers 2
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, LSJ;->b:Ljava/lang/String;

    .line 61
    return-object p0
.end method
