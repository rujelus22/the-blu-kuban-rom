.class public Loy;
.super Ljava/lang/Object;
.source "PrintDialogActivity.java"

# interfaces
.implements Lqd;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 364
    iput-object p1, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lqe;
    .registers 6

    .prologue
    .line 379
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Lqe;

    move-result-object v0

    if-nez v0, :cond_2d

    .line 380
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LPm;

    iget-object v1, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    iget-object v2, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LPm;->a(Landroid/content/Context;Landroid/net/Uri;)LPp;

    move-result-object v0

    .line 382
    iget-object v1, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    new-instance v2, Lqc;

    iget-object v3, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v3}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lqc;-><init>(Ljava/lang/String;Ljava/lang/String;LPp;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;Lqe;)Lqe;

    .line 385
    :cond_2d
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Lqe;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 373
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdL;

    iget-object v1, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 374
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->finish()V

    .line 375
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 367
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdL;

    iget-object v1, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 368
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 369
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 390
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdL;

    iget-object v1, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 391
    const-string v0, "PrintDialogActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UrlLoaderHelper.loadUrlInWebView Loading URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in webview."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 393
    return-void
.end method

.method public a(Lnm;)V
    .registers 4
    .parameter

    .prologue
    .line 403
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)LdG;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 404
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)LdG;

    move-result-object v0

    invoke-virtual {v0}, LdG;->a()V

    .line 407
    :cond_11
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    new-instance v1, Loz;

    invoke-direct {v1, p0, p1}, Loz;-><init>(Loy;Lnm;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;LdG;)LdG;

    .line 420
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)LdG;

    move-result-object v0

    invoke-virtual {v0}, LdG;->start()V

    .line 421
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 397
    const-string v0, "PrintDialogActivity"

    const-string v1, "Error occurred: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v0, p0, Loy;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)V

    .line 399
    return-void
.end method
