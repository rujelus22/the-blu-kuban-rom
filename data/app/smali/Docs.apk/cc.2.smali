.class Lcc;
.super Ljava/lang/Object;
.source "AnalyticsGmsCoreClient.java"

# interfaces
.implements Lcb;


# instance fields
.field private a:Ladv;

.field private a:Landroid/content/Context;

.field private a:Landroid/content/ServiceConnection;

.field private a:Lce;

.field private a:Lcf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lce;Lcf;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcc;->a:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lcc;->a:Lce;

    .line 59
    iput-object p3, p0, Lcc;->a:Lcf;

    .line 60
    return-void
.end method

.method private a()Ladv;
    .registers 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcc;->d()V

    .line 121
    iget-object v0, p0, Lcc;->a:Ladv;

    return-object v0
.end method

.method static synthetic a(Lcc;Ladv;)Ladv;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcc;->a:Ladv;

    return-object p1
.end method

.method static synthetic a(Lcc;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcc;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcc;Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcc;->a:Landroid/content/ServiceConnection;

    return-object p1
.end method

.method static synthetic a(Lcc;)Lce;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcc;->a:Lce;

    return-object v0
.end method

.method static synthetic a(Lcc;)Lcf;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcc;->a:Lcf;

    return-object v0
.end method

.method static synthetic a(Lcc;)V
    .registers 1
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Lcc;->e()V

    return-void
.end method

.method private e()V
    .registers 1

    .prologue
    .line 181
    invoke-direct {p0}, Lcc;->f()V

    .line 182
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, Lcc;->a:Lce;

    invoke-interface {v0}, Lce;->a()V

    .line 190
    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 114
    :try_start_0
    invoke-direct {p0}, Lcc;->a()Ladv;

    move-result-object v0

    invoke-interface {v0}, Ladv;->a()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    .line 118
    :goto_7
    return-void

    .line 115
    :catch_8
    move-exception v0

    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clear hits failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    goto :goto_7
.end method

.method public a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/analytics/internal/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    :try_start_0
    invoke-direct {p0}, Lcc;->a()Ladv;

    move-result-object v0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Ladv;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    .line 109
    :goto_b
    return-void

    .line 106
    :catch_c
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendHit failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    goto :goto_b
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, Lcc;->a:Ladv;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.analytics.service.START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    const-string v1, "app_package_name"

    iget-object v2, p0, Lcc;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    iget-object v1, p0, Lcc;->a:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_1d

    .line 71
    const-string v0, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    .line 82
    :cond_1c
    :goto_1c
    return-void

    .line 74
    :cond_1d
    new-instance v1, Lcd;

    invoke-direct {v1, p0}, Lcd;-><init>(Lcc;)V

    iput-object v1, p0, Lcc;->a:Landroid/content/ServiceConnection;

    .line 75
    iget-object v1, p0, Lcc;->a:Landroid/content/Context;

    iget-object v2, p0, Lcc;->a:Landroid/content/ServiceConnection;

    const/16 v3, 0x81

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connect: bindService returned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 78
    if-nez v1, :cond_1c

    .line 79
    iput-object v4, p0, Lcc;->a:Landroid/content/ServiceConnection;

    .line 80
    iget-object v0, p0, Lcc;->a:Lcf;

    const/4 v1, 0x1

    invoke-interface {v0, v1, v4}, Lcf;->a(ILandroid/content/Intent;)V

    goto :goto_1c
.end method

.method public c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 93
    iput-object v2, p0, Lcc;->a:Ladv;

    .line 94
    iget-object v0, p0, Lcc;->a:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_15

    .line 95
    iget-object v0, p0, Lcc;->a:Landroid/content/Context;

    iget-object v1, p0, Lcc;->a:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 96
    iput-object v2, p0, Lcc;->a:Landroid/content/ServiceConnection;

    .line 97
    iget-object v0, p0, Lcc;->a:Lce;

    invoke-interface {v0}, Lce;->b()V

    .line 99
    :cond_15
    return-void
.end method

.method protected d()V
    .registers 3

    .prologue
    .line 129
    invoke-virtual {p0}, Lcc;->a()Z

    move-result v0

    if-nez v0, :cond_e

    .line 130
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_e
    return-void
.end method
