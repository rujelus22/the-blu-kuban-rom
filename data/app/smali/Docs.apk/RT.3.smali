.class public final enum LRT;
.super Ljava/lang/Enum;
.source "PunchWebViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LRT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LRT;

.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LRT;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LRT;

.field public static final enum b:LRT;

.field public static final enum c:LRT;

.field public static final enum d:LRT;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 217
    new-instance v1, LRT;

    const-string v2, "EMPTY"

    invoke-direct {v1, v2, v0, v3}, LRT;-><init>(Ljava/lang/String;II)V

    sput-object v1, LRT;->a:LRT;

    .line 218
    new-instance v1, LRT;

    const-string v2, "LOADED"

    invoke-direct {v1, v2, v3, v4}, LRT;-><init>(Ljava/lang/String;II)V

    sput-object v1, LRT;->b:LRT;

    .line 219
    new-instance v1, LRT;

    const-string v2, "LOADING"

    invoke-direct {v1, v2, v4, v5}, LRT;-><init>(Ljava/lang/String;II)V

    sput-object v1, LRT;->c:LRT;

    .line 220
    new-instance v1, LRT;

    const-string v2, "ERROR"

    invoke-direct {v1, v2, v5, v6}, LRT;-><init>(Ljava/lang/String;II)V

    sput-object v1, LRT;->d:LRT;

    .line 216
    new-array v1, v6, [LRT;

    sget-object v2, LRT;->a:LRT;

    aput-object v2, v1, v0

    sget-object v2, LRT;->b:LRT;

    aput-object v2, v1, v3

    sget-object v2, LRT;->c:LRT;

    aput-object v2, v1, v4

    sget-object v2, LRT;->d:LRT;

    aput-object v2, v1, v5

    sput-object v1, LRT;->a:[LRT;

    .line 224
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, LRT;->a:Ljava/util/Map;

    .line 226
    invoke-static {}, LRT;->values()[LRT;

    move-result-object v1

    array-length v2, v1

    :goto_49
    if-ge v0, v2, :cond_5b

    aget-object v3, v1, v0

    .line 227
    sget-object v4, LRT;->a:Ljava/util/Map;

    iget v5, v3, LRT;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_49

    .line 229
    :cond_5b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 241
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 242
    iput p3, p0, LRT;->a:I

    .line 243
    return-void
.end method

.method public static a(I)LRT;
    .registers 3
    .parameter

    .prologue
    .line 238
    sget-object v0, LRT;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRT;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LRT;
    .registers 2
    .parameter

    .prologue
    .line 216
    const-class v0, LRT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LRT;

    return-object v0
.end method

.method public static values()[LRT;
    .registers 1

    .prologue
    .line 216
    sget-object v0, LRT;->a:[LRT;

    invoke-virtual {v0}, [LRT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LRT;

    return-object v0
.end method
