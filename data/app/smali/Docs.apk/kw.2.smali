.class public final enum Lkw;
.super Ljava/lang/Enum;
.source "CsiAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkw;

.field private static final synthetic a:[Lkw;


# instance fields
.field a:Ldy;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 17
    new-instance v0, Lkw;

    const-string v1, "HOME_SCREEN"

    const-string v2, "HomeScreen"

    invoke-direct {v0, v1, v3, v2}, Lkw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkw;->a:Lkw;

    .line 16
    const/4 v0, 0x1

    new-array v0, v0, [Lkw;

    sget-object v1, Lkw;->a:Lkw;

    aput-object v1, v0, v3

    sput-object v0, Lkw;->a:[Lkw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    new-instance v0, Ldy;

    invoke-direct {v0, p3}, Ldy;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lkw;->a:Ldy;

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkw;
    .registers 2
    .parameter

    .prologue
    .line 16
    const-class v0, Lkw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkw;

    return-object v0
.end method

.method public static values()[Lkw;
    .registers 1

    .prologue
    .line 16
    sget-object v0, Lkw;->a:[Lkw;

    invoke-virtual {v0}, [Lkw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkw;

    return-object v0
.end method


# virtual methods
.method public a()Ldx;
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, Lkw;->a:Ldy;

    invoke-virtual {v0}, Ldy;->a()Ldx;

    move-result-object v0

    .line 35
    return-object v0
.end method

.method public a()Ldy;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lkw;->a:Ldy;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ldx;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 42
    if-eqz p1, :cond_8

    invoke-static {p1}, LasV;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 43
    :cond_8
    const-string v0, "CsiActionName"

    const-string v1, "Event name cannot be null or whitespaces."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :goto_f
    return-void

    .line 46
    :cond_10
    iget-object v0, p0, Lkw;->a:Ldy;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p2, v1}, Ldy;->a(Ldx;[Ljava/lang/String;)Z

    goto :goto_f
.end method
