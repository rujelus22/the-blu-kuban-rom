.class public LIy;
.super Ljava/lang/Object;
.source "GridViewCache.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/trix/viewmodel/CellContentFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/docs/editors/trix/viewmodel/CellContentFactory;"
    }
.end annotation


# instance fields
.field private final a:LIA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LIA",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LIA;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LIA",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LIy;->a:Ljava/util/Stack;

    .line 41
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LIy;->a:Landroid/util/SparseArray;

    .line 46
    iput-object p1, p0, LIy;->a:LIA;

    .line 47
    return-void
.end method


# virtual methods
.method public a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 90
    const/high16 v0, 0x1

    mul-int/2addr v0, p1

    add-int/2addr v0, p2

    return v0
.end method

.method public a(LIS;)I
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-virtual {p1}, LIS;->a()LJJ;

    move-result-object v0

    invoke-virtual {v0}, LJJ;->a()I

    move-result v3

    .line 54
    invoke-virtual {p1}, LIS;->a()LJJ;

    move-result-object v0

    invoke-virtual {v0}, LJJ;->b()I

    move-result v4

    .line 55
    invoke-virtual {p0, v3, v4}, LIy;->a(II)I

    move-result v5

    .line 56
    iget-object v0, p0, LIy;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_78

    move v0, v1

    :goto_1f
    const-string v6, "Reallocating unreleased slot: %s, %s. Check for unreleased GridCell-s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v7, v1

    invoke-static {v0, v6, v7}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 62
    iget-object v0, p0, LIy;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7a

    .line 63
    iget-object v0, p0, LIy;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .line 67
    :goto_41
    iget-object v1, p0, LIy;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 68
    const-string v1, "Grid"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Allocate slot "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " to "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v1, p0, LIy;->a:LIA;

    invoke-interface {v1, v0, p1}, LIA;->a(Ljava/lang/Object;LIS;)V

    .line 70
    return v5

    :cond_78
    move v0, v2

    .line 56
    goto :goto_1f

    .line 65
    :cond_7a
    iget-object v0, p0, LIy;->a:LIA;

    invoke-interface {v0}, LIA;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_41
.end method

.method public a(I)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, LIy;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_f

    .line 103
    iget-object v0, p0, LIy;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 107
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public a(II)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)TT;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2}, LIy;->a(II)I

    move-result v0

    .line 95
    invoke-virtual {p0, v0}, LIy;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public allocate(J)I
    .registers 5
    .parameter

    .prologue
    .line 75
    new-instance v0, LIz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, LIz;-><init>(LIy;JZ)V

    invoke-virtual {p0, v0}, LIy;->a(LIS;)I

    move-result v0

    return v0
.end method

.method public free(I)V
    .registers 5
    .parameter

    .prologue
    .line 81
    const-string v0, "Grid"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Free slot "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, LIy;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 84
    iget-object v0, p0, LIy;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 85
    if-eqz v1, :cond_2f

    const/4 v0, 0x1

    :goto_26
    invoke-static {v0}, Lagu;->a(Z)V

    .line 86
    iget-object v0, p0, LIy;->a:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    return-void

    .line 85
    :cond_2f
    const/4 v0, 0x0

    goto :goto_26
.end method
