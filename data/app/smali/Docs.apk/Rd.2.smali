.class public LRd;
.super Ljava/lang/Object;
.source "PunchSvgViewer.java"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private a:I

.field a:LNS;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LQV;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Landroid/os/Handler;

.field private a:Landroid/webkit/WebView;

.field a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LRb;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 77
    const-string v0, "[\\s]*?<\\?xml(.|[\\n])*?\\?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LRd;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>()V
    .registers 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v0, -0x1

    iput v0, p0, LRd;->a:I

    .line 82
    const-string v0, ""

    iput-object v0, p0, LRd;->a:Ljava/lang/String;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, LRd;->a:Z

    .line 89
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LRd;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(LRd;)I
    .registers 2
    .parameter

    .prologue
    .line 30
    iget v0, p0, LRd;->a:I

    return v0
.end method

.method static synthetic a(LRd;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LRd;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(LRd;)Landroid/webkit/WebView;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic a(LRd;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LRd;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 130
    iget v0, p0, LRd;->a:I

    if-eq v0, p1, :cond_5

    .line 157
    :goto_4
    return-void

    .line 134
    :cond_5
    iget-object v0, p0, LRd;->a:LQV;

    iget v1, p0, LRd;->a:I

    new-instance v2, LRe;

    invoke-direct {v2, p0, p1}, LRe;-><init>(LRd;I)V

    invoke-virtual {v0, v1, v2}, LQV;->a(ILQT;)V

    goto :goto_4
.end method

.method static synthetic a(LRd;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, LRd;->a(I)V

    return-void
.end method

.method static synthetic a(LRd;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, LRd;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 216
    const-string v0, "PunchSvgViewer"

    const-string v1, "in updateEditorStateView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 219
    const-string v0, "PunchSvgViewer"

    const-string v1, "Slide content was not ready yet. Requesting again."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v0, -0x1

    iput v0, p0, LRd;->a:I

    .line 221
    iget-object v0, p0, LRd;->a:Landroid/os/Handler;

    new-instance v1, LRi;

    invoke-direct {v1, p0}, LRi;-><init>(LRd;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 236
    :goto_23
    return-void

    .line 230
    :cond_24
    invoke-direct {p0}, LRd;->c()V

    .line 231
    sget-object v0, LRd;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 232
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LRd;->a:Ljava/lang/String;

    .line 234
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    const-string v1, "javascript:document.getElementsByTagName(\'body\')[0].innerHTML=SvgLoader.getSvg()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_23
.end method

.method private c()V
    .registers 3

    .prologue
    .line 209
    iget-boolean v0, p0, LRd;->a:Z

    if-nez v0, :cond_e

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, LRd;->a:Z

    .line 211
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    const-string v1, "javascript:document.addEventListener(\"click\", function(event) {\n  // Flag telling whether we should ignore this click event or not.\n  var ignore = false;\n  for (var current = event.target;\n      current != null;\n      current = current.parentNode) {\n     if (current == document) {\n       break;\n     }\n\n     if (!!current.onclick) {\n      ignore = true;\n       break;\n     }\n\n    if (current instanceof HTMLAnchorElement) {\n      ignore = true;\n      break;\n    }\n\n    if (current instanceof SVGAElement) {\n      ignore = true;\n      break;\n    }\n  }\n\n  if (!ignore) {\n    event.stopPropagation();\n    window.SvgLoader.onTap();\n  }\n}, true);"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 213
    :cond_e
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/FrameLayout;)Landroid/webkit/WebView;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 160
    invoke-virtual {p1, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 161
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_13

    .line 162
    const-string v0, "PunchSvgViewer"

    const-string v1, "SKIPPING Creation of editorStateView not null"

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    .line 205
    :goto_12
    return-object v0

    .line 166
    :cond_13
    const-string v0, "PunchSvgViewer"

    const-string v1, "Creating editorStateView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 168
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, LRd;->a:LNS;

    invoke-interface {v2}, LNS;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/webkit/WebView;Landroid/content/res/Resources;Ljava/lang/String;)V

    .line 170
    new-instance v1, Landroid/webkit/WebChromeClient;

    invoke-direct {v1}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 171
    new-instance v1, LRg;

    invoke-direct {v1, p0, p1}, LRg;-><init>(LRd;Landroid/widget/FrameLayout;)V

    const-string v2, "SvgLoader"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    const-string v1, "<?xml version=\"1.0\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head/><body/></html>"

    const-string v2, "image/svg+xml"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iput-boolean v5, p0, LRd;->a:Z

    .line 200
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 202
    iput-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    .line 203
    iput v4, p0, LRd;->a:I

    .line 204
    invoke-virtual {p0}, LRd;->b()V

    .line 205
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    goto :goto_12
.end method

.method public a()V
    .registers 3

    .prologue
    .line 98
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_5

    .line 108
    :goto_4
    return-void

    .line 102
    :cond_5
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    .line 103
    const/4 v1, 0x0

    iput-object v1, p0, LRd;->a:Landroid/webkit/WebView;

    .line 104
    const/4 v1, -0x1

    iput v1, p0, LRd;->a:I

    .line 105
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 106
    const-string v1, "SvgLoader"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 107
    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_4
.end method

.method b()V
    .registers 3

    .prologue
    .line 111
    iget-object v0, p0, LRd;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_5

    .line 127
    :cond_4
    :goto_4
    return-void

    .line 115
    :cond_5
    iget-object v0, p0, LRd;->a:LQV;

    invoke-virtual {v0}, LQV;->c()I

    move-result v0

    .line 116
    if-eqz v0, :cond_4

    .line 120
    iget-object v0, p0, LRd;->a:LQV;

    invoke-virtual {v0}, LQV;->d()I

    move-result v0

    .line 121
    iget v1, p0, LRd;->a:I

    if-eq v0, v1, :cond_4

    .line 125
    iput v0, p0, LRd;->a:I

    .line 126
    invoke-direct {p0, v0}, LRd;->a(I)V

    goto :goto_4
.end method
