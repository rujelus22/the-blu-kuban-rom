.class public LAz;
.super LAM;
.source "HorizontalRule.java"


# instance fields
.field private a:Ljava/lang/Object;

.field private final a:Lxu;


# direct methods
.method public constructor <init>(LDb;Lxu;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1}, LAM;-><init>(LDb;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, LAz;->a:Ljava/lang/Object;

    .line 36
    iput-object p2, p0, LAz;->a:Lxu;

    .line 37
    return-void
.end method


# virtual methods
.method public a(IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, LAz;->a:LDI;

    invoke-virtual {v0}, LDI;->c()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, LAz;->a:LDI;

    invoke-virtual {v1}, LDI;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 72
    iget-object v1, p0, LAz;->a:LDI;

    invoke-virtual {v1}, LDI;->c()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, LAz;->a:LDI;

    invoke-virtual {v2}, LDI;->b()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 73
    if-ge v0, v1, :cond_3f

    .line 75
    new-instance v0, LKj;

    iget-object v1, p0, LAz;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    iget-object v2, p0, LAz;->a:LDb;

    invoke-virtual {v2}, LDb;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_3e
    :goto_3e
    return-void

    .line 76
    :cond_3f
    if-ne v0, v1, :cond_3e

    iget-object v1, p0, LAz;->a:LDI;

    invoke-virtual {v1}, LDI;->b()I

    move-result v1

    if-ne v0, v1, :cond_3e

    .line 78
    new-instance v0, LKj;

    iget-object v1, p0, LAz;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    iget-object v2, p0, LAz;->a:LDb;

    invoke-virtual {v2}, LDb;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3e
.end method

.method public a(Lza;)V
    .registers 5
    .parameter

    .prologue
    .line 41
    invoke-super {p0, p1}, LAM;->a(Lza;)V

    .line 43
    iget-object v0, p0, LAz;->a:LDI;

    iget-object v1, p0, LAz;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LDI;->removeSpan(Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, LAz;->a:LDI;

    const/4 v1, 0x0

    iget-object v2, p0, LAz;->a:LDI;

    invoke-virtual {v2}, LDI;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LDI;->a(II)LDI;

    .line 45
    return-void
.end method

.method protected b()V
    .registers 6

    .prologue
    .line 49
    iget-object v0, p0, LAz;->a:Ljava/lang/Object;

    if-nez v0, :cond_37

    .line 50
    iget-object v0, p0, LAz;->a:Lxu;

    invoke-interface {v0}, Lxu;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    sget v1, LsA;->horizontal_rule_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 52
    new-instance v1, LBX;

    const/high16 v2, 0x3f80

    invoke-direct {v1, v0, v2}, LBX;-><init>(IF)V

    iput-object v1, p0, LAz;->a:Ljava/lang/Object;

    .line 53
    iget-object v0, p0, LAz;->a:LDI;

    const-string v1, "\n \n"

    invoke-virtual {v0, v1}, LDI;->a(Ljava/lang/CharSequence;)LDI;

    .line 54
    iget-object v0, p0, LAz;->a:LDI;

    new-instance v1, LAA;

    invoke-direct {v1, p0}, LAA;-><init>(LAz;)V

    const/4 v2, 0x0

    iget-object v3, p0, LAz;->a:LDI;

    invoke-virtual {v3}, LDI;->length()I

    move-result v3

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v2, v3, v4}, LDI;->setSpan(Ljava/lang/Object;III)V

    .line 57
    :cond_37
    iget-object v0, p0, LAz;->a:LDI;

    iget-object v1, p0, LAz;->a:Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, LDI;->setSpan(Ljava/lang/Object;III)V

    .line 58
    return-void
.end method

.method public d(I)I
    .registers 3
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, LAz;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    if-gt p1, v0, :cond_f

    .line 63
    iget-object v0, p0, LAz;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    .line 65
    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, LAz;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    goto :goto_e
.end method
