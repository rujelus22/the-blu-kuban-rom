.class public LlF;
.super Ljava/lang/Object;
.source "OperationQueueImpl.java"

# interfaces
.implements LXe;
.implements Lih;
.implements LlE;


# annotations
.annotation runtime LaoJ;
.end annotation


# instance fields
.field private final a:I

.field private final a:LPa;

.field private final a:LaaX;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LlB;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Queue",
            "<",
            "Llj;",
            ">;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/concurrent/Executor;

.field private final a:LlR;

.field private final a:Llf;

.field private final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Llf;Ljava/util/concurrent/Executor;LKS;LlJ;Landroid/content/Context;LPa;LlR;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LlF;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 87
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LlF;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 93
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LlF;->a:Ljava/util/Queue;

    .line 72
    iput-object p1, p0, LlF;->a:Llf;

    .line 73
    iput-object p2, p0, LlF;->a:Ljava/util/concurrent/Executor;

    .line 74
    const-string v0, "operationQueueMaxAttempts"

    const/4 v1, 0x4

    invoke-interface {p3, v0, v1}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LlF;->a:I

    .line 75
    iput-object p4, p0, LlF;->a:LaaX;

    .line 76
    iput-object p5, p0, LlF;->a:Landroid/content/Context;

    .line 77
    iput-object p6, p0, LlF;->a:LPa;

    .line 78
    iput-object p7, p0, LlF;->a:LlR;

    .line 79
    return-void
.end method

.method static a(LkB;Llf;Llj;)Lll;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 445
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p2}, Llj;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 446
    invoke-static {p0, p1, v0}, Lll;->a(LkB;Llf;Lorg/json/JSONObject;)Lll;

    move-result-object v0

    .line 447
    return-object v0
.end method

.method private a(LkB;)V
    .registers 3
    .parameter

    .prologue
    .line 246
    new-instance v0, LlG;

    invoke-direct {v0, p0}, LlG;-><init>(LlF;)V

    .line 252
    invoke-virtual {p0, p1, v0}, LlF;->a(LkB;LlK;)V

    .line 253
    return-void
.end method

.method static synthetic a(LlF;LkB;LlK;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, LlF;->b(LkB;LlK;)V

    return-void
.end method

.method private static a(Llf;Llj;LkO;LlB;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 228
    const-string v0, "OperationQueueImpl"

    const-string v1, "The eTag we made the operation against is different to the current local etag."

    invoke-static {v0, v1}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-interface {p3, p0, p2}, LlB;->a(Llf;LkO;)LlB;

    move-result-object v0

    .line 230
    if-eqz p1, :cond_24

    .line 231
    new-instance v1, Lll;

    invoke-virtual {p2}, LkO;->h()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p3, v0, v2}, Lll;-><init>(LlB;LlB;Ljava/lang/String;)V

    .line 235
    :try_start_16
    invoke-virtual {v1}, Lll;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 236
    invoke-virtual {p1, v0}, Llj;->a(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p1}, Llj;->c()V
    :try_end_24
    .catch Lorg/json/JSONException; {:try_start_16 .. :try_end_24} :catch_25

    .line 242
    :cond_24
    :goto_24
    return-void

    .line 238
    :catch_25
    move-exception v0

    goto :goto_24
.end method

.method private a(Lll;)V
    .registers 9
    .parameter

    .prologue
    .line 428
    invoke-virtual {p1}, Lll;->a()LlB;

    move-result-object v0

    .line 429
    invoke-virtual {p1}, Lll;->a()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 430
    invoke-interface {v0}, LlB;->c()Ljava/lang/String;

    move-result-object v2

    .line 431
    iget-object v3, p0, LlF;->a:Llf;

    invoke-interface {v3, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v3

    .line 432
    iget-object v4, p0, LlF;->a:Llf;

    invoke-interface {v0}, LlB;->a()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-interface {v4, v3, v1, v5, v6}, Llf;->a(LkB;Ljava/lang/String;J)Llj;

    move-result-object v0

    .line 434
    const-string v1, "OperationQueueImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Save entry in local database:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    invoke-virtual {p0, v2}, LlF;->a(Ljava/lang/String;)Ljava/util/Queue;

    move-result-object v1

    .line 439
    invoke-virtual {v0}, Llj;->c()V

    .line 440
    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 441
    return-void
.end method

.method private static a(Lll;LkB;Llf;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-interface {p2}, Llf;->a()V

    .line 189
    :try_start_3
    invoke-virtual {p0}, Lll;->b()LlB;

    move-result-object v0

    .line 190
    invoke-interface {v0}, LlB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, p1, v1}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v1

    .line 191
    if-eqz v1, :cond_17

    .line 192
    invoke-interface {v0, p2, v1}, LlB;->a(Llf;LkO;)LlB;

    .line 193
    invoke-virtual {v1}, LkO;->c()V

    .line 196
    :cond_17
    invoke-interface {p2}, Llf;->c()V

    .line 197
    invoke-virtual {p1}, LkB;->a()V
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_2d
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_1d} :catch_21

    .line 201
    invoke-interface {p2}, Llf;->b()V

    .line 203
    :goto_20
    return-void

    .line 198
    :catch_21
    move-exception v0

    .line 199
    :try_start_22
    const-string v1, "OperationQueueImpl"

    const-string v2, "Failed to save reverted operation in database"

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catchall {:try_start_22 .. :try_end_29} :catchall_2d

    .line 201
    invoke-interface {p2}, Llf;->b()V

    goto :goto_20

    :catchall_2d
    move-exception v0

    invoke-interface {p2}, Llf;->b()V

    throw v0
.end method

.method private static a(LkB;LlK;Llf;Ljava/util/Queue;ILaaX;LlR;)Z
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkB;",
            "LlK;",
            "Llf;",
            "Ljava/util/Queue",
            "<",
            "Llj;",
            ">;I",
            "LaaX;",
            "LlR;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 133
    invoke-interface {p5}, LaaX;->a()LaaW;

    move-result-object v5

    .line 135
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 137
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 138
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    .line 140
    :cond_1c
    :goto_1c
    if-eqz v0, :cond_a3

    .line 141
    invoke-virtual {v0}, Llj;->b()I

    move-result v4

    .line 142
    if-le v4, p4, :cond_26

    move v0, v2

    .line 178
    :goto_25
    return v0

    .line 151
    :cond_26
    :try_start_26
    invoke-static {p0, p2, v0}, LlF;->a(LkB;Llf;Llj;)Lll;
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_9f
    .catch Lorg/json/JSONException; {:try_start_26 .. :try_end_29} :catch_4c

    move-result-object v4

    .line 159
    :try_start_2a
    invoke-static {v4, p1, p6}, LlF;->a(Lll;LlK;LlR;)Z
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_9f

    move-result v4

    if-nez v4, :cond_a1

    .line 162
    :try_start_30
    invoke-interface {v5}, LaaW;->b()V
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_80
    .catch Ljava/lang/InterruptedException; {:try_start_30 .. :try_end_33} :catch_7a

    .line 166
    :try_start_33
    invoke-interface {v5}, LaaW;->c()V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_85

    move v4, v2

    .line 170
    :goto_37
    if-nez v4, :cond_1c

    .line 171
    invoke-virtual {v0}, Llj;->b()V

    .line 172
    invoke-interface {p3, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 173
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    goto :goto_1c

    .line 152
    :catch_4c
    move-exception v4

    .line 156
    :try_start_4d
    const-string v4, "OperationQueueImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to get applied operation from pending operation:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_65
    .catchall {:try_start_4d .. :try_end_65} :catchall_9f

    .line 171
    invoke-virtual {v0}, Llj;->b()V

    .line 172
    invoke-interface {p3, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 173
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_78

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    goto :goto_1c

    :cond_78
    move-object v0, v1

    goto :goto_1c

    .line 163
    :catch_7a
    move-exception v4

    .line 166
    :try_start_7b
    invoke-interface {v5}, LaaW;->c()V

    move v4, v2

    .line 167
    goto :goto_37

    .line 166
    :catchall_80
    move-exception v1

    invoke-interface {v5}, LaaW;->c()V

    throw v1
    :try_end_85
    .catchall {:try_start_7b .. :try_end_85} :catchall_85

    .line 170
    :catchall_85
    move-exception v1

    move v3, v2

    :goto_87
    if-nez v3, :cond_9b

    .line 171
    invoke-virtual {v0}, Llj;->b()V

    .line 172
    invoke-interface {p3, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 173
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    :cond_9b
    throw v1

    :cond_9c
    move-object v0, v1

    goto/16 :goto_1c

    .line 170
    :catchall_9f
    move-exception v1

    goto :goto_87

    :cond_a1
    move v4, v3

    goto :goto_37

    :cond_a3
    move v0, v3

    goto :goto_25

    :cond_a5
    move-object v0, v1

    goto/16 :goto_1c
.end method

.method static a(Lll;LlK;LlR;)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 216
    invoke-virtual {p0}, Lll;->a()LlB;

    move-result-object v0

    .line 217
    invoke-interface {v0, p1, p2}, LlB;->a(LlK;LlR;)Z

    move-result v0

    return v0
.end method

.method private b(LkB;LlK;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 327
    .line 329
    invoke-virtual {p1}, LkB;->b()Ljava/lang/String;

    move-result-object v0

    .line 330
    invoke-virtual {p0, v0}, LlF;->a(Ljava/lang/String;)Ljava/util/Queue;

    move-result-object v3

    .line 331
    iget-object v1, p0, LlF;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    invoke-interface {v1, v0, v2}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    :try_start_13
    iget-object v1, p0, LlF;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_47

    .line 334
    :try_start_1a
    iget-object v2, p0, LlF;->a:Llf;

    iget v4, p0, LlF;->a:I

    iget-object v5, p0, LlF;->a:LaaX;

    iget-object v6, p0, LlF;->a:LlR;

    move-object v0, p1

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LlF;->a(LkB;LlK;Llf;Ljava/util/Queue;ILaaX;LlR;)Z
    :try_end_27
    .catchall {:try_start_1a .. :try_end_27} :catchall_37

    move-result v1

    .line 337
    :try_start_28
    monitor-exit v8
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_49

    .line 339
    if-eqz v1, :cond_32

    .line 340
    iget-object v0, p0, LlF;->a:Landroid/content/Context;

    iget-object v1, p0, LlF;->a:LPa;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a(Landroid/content/Context;LPa;)V

    .line 344
    :cond_32
    const/4 v0, 0x0

    invoke-interface {p2, v7, v0}, LlK;->a(ILjava/lang/Throwable;)V

    .line 345
    return-void

    .line 337
    :catchall_37
    move-exception v0

    move v1, v7

    :goto_39
    :try_start_39
    monitor-exit v8
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_49

    :try_start_3a
    throw v0
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_3b

    .line 339
    :catchall_3b
    move-exception v0

    move v7, v1

    :goto_3d
    if-eqz v7, :cond_46

    .line 340
    iget-object v1, p0, LlF;->a:Landroid/content/Context;

    iget-object v2, p0, LlF;->a:LPa;

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a(Landroid/content/Context;LPa;)V

    :cond_46
    throw v0

    .line 339
    :catchall_47
    move-exception v0

    goto :goto_3d

    .line 337
    :catchall_49
    move-exception v0

    goto :goto_39
.end method


# virtual methods
.method a(Ljava/lang/String;)Ljava/util/Queue;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Queue",
            "<",
            "Llj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, LlF;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 103
    if-nez v0, :cond_28

    .line 106
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0, p1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 107
    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v2, p0, LlF;->a:Llf;

    invoke-interface {v2, v0}, Llf;->a(LkB;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>(Ljava/util/Collection;)V

    .line 109
    iget-object v0, p0, LlF;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, LlF;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 112
    :cond_28
    return-object v0
.end method

.method public a()V
    .registers 9

    .prologue
    .line 280
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0}, Llf;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 281
    iget-object v2, p0, LlF;->a:Llf;

    invoke-interface {v2, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v2

    .line 282
    invoke-virtual {p0, v0}, LlF;->a(Ljava/lang/String;)Ljava/util/Queue;

    move-result-object v3

    .line 283
    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_24
    :goto_24
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    .line 284
    invoke-virtual {v0}, Llj;->a()I

    move-result v5

    iget v6, p0, LlF;->a:I

    if-le v5, v6, :cond_24

    .line 285
    invoke-virtual {v0}, Llj;->b()V

    .line 286
    invoke-interface {v3, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 289
    :try_start_3e
    new-instance v5, Lorg/json/JSONObject;

    invoke-virtual {v0}, Llj;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 290
    iget-object v6, p0, LlF;->a:Llf;

    invoke-static {v2, v6, v5}, Lll;->a(LkB;Llf;Lorg/json/JSONObject;)Lll;

    move-result-object v5

    .line 292
    iget-object v6, p0, LlF;->a:Llf;

    invoke-static {v5, v2, v6}, LlF;->a(Lll;LkB;Llf;)V
    :try_end_52
    .catch Lorg/json/JSONException; {:try_start_3e .. :try_end_52} :catch_53

    goto :goto_24

    .line 293
    :catch_53
    move-exception v5

    .line 295
    const-string v5, "OperationQueueImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "JSONException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Llj;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_24

    .line 300
    :cond_71
    return-void
.end method

.method public a(LkB;LlK;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 358
    iget-object v0, p0, LlF;->a:Landroid/content/Context;

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 359
    iget-object v0, p0, LlF;->a:Ljava/util/concurrent/Executor;

    new-instance v1, LlH;

    invoke-direct {v1, p0, p1, p2}, LlH;-><init>(LlF;LkB;LlK;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 365
    return-void
.end method

.method public a(LkO;)V
    .registers 9
    .parameter

    .prologue
    .line 306
    invoke-virtual {p1}, LkO;->a()LkB;

    move-result-object v1

    .line 307
    invoke-virtual {v1}, LkB;->b()Ljava/lang/String;

    move-result-object v0

    .line 308
    invoke-virtual {p1}, LkO;->i()Ljava/lang/String;

    move-result-object v2

    .line 309
    invoke-virtual {p0, v0}, LlF;->a(Ljava/lang/String;)Ljava/util/Queue;

    move-result-object v3

    .line 310
    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_14
    :goto_14
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    .line 312
    :try_start_20
    iget-object v5, p0, LlF;->a:Llf;

    invoke-static {v1, v5, v0}, LlF;->a(LkB;Llf;Llj;)Lll;

    move-result-object v5

    .line 314
    invoke-virtual {v5}, Lll;->a()LlB;

    move-result-object v6

    invoke-interface {v6}, LlB;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 315
    iget-object v6, p0, LlF;->a:Llf;

    invoke-virtual {v5}, Lll;->a()LlB;

    move-result-object v5

    invoke-static {v6, v0, p1, v5}, LlF;->a(Llf;Llj;LkO;LlB;)V
    :try_end_3d
    .catch Lorg/json/JSONException; {:try_start_20 .. :try_end_3d} :catch_3e

    goto :goto_14

    .line 318
    :catch_3e
    move-exception v5

    .line 319
    invoke-virtual {v0}, Llj;->b()V

    .line 320
    invoke-interface {v3, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 321
    const-string v0, "OperationQueueImpl"

    const-string v5, "JSONException when reapplying operations on sync"

    invoke-static {v0, v5}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_14

    .line 324
    :cond_4d
    return-void
.end method

.method public a(LlB;)V
    .registers 3
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, LlF;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public a()Z
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 379
    iget-object v0, p0, LlF;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 424
    :goto_b
    return v0

    .line 382
    :cond_c
    const/4 v3, 0x0

    .line 384
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 385
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0}, Llf;->a()V

    .line 388
    :cond_17
    :goto_17
    :try_start_17
    iget-object v0, p0, LlF;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlB;
    :try_end_1f
    .catchall {:try_start_17 .. :try_end_1f} :catchall_a8

    if-eqz v0, :cond_84

    .line 390
    if-eqz v3, :cond_31

    :try_start_23
    invoke-interface {v0}, LlB;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, LkB;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3b

    .line 392
    :cond_31
    iget-object v3, p0, LlF;->a:Llf;

    invoke-interface {v0}, LlB;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v3

    .line 394
    :cond_3b
    invoke-interface {v0}, LlB;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 395
    iget-object v5, p0, LlF;->a:Llf;

    invoke-interface {v0}, LlB;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v3, v6}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v5

    .line 396
    if-eqz v5, :cond_60

    .line 397
    iget-object v6, p0, LlF;->a:Llf;

    invoke-interface {v0, v6, v5}, LlB;->a(Llf;LkO;)LlB;

    move-result-object v6

    .line 398
    new-instance v7, Lll;

    invoke-virtual {v5}, LkO;->h()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v0, v6, v8}, Lll;-><init>(LlB;LlB;Ljava/lang/String;)V

    .line 400
    invoke-direct {p0, v7}, LlF;->a(Lll;)V
    :try_end_60
    .catchall {:try_start_23 .. :try_end_60} :catchall_a8
    .catch Lorg/json/JSONException; {:try_start_23 .. :try_end_60} :catch_75

    .line 407
    :cond_60
    if-eqz v5, :cond_17

    .line 408
    :try_start_62
    invoke-virtual {v5}, LkO;->c()V
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_a8
    .catch Ljava/lang/RuntimeException; {:try_start_62 .. :try_end_65} :catch_66

    goto :goto_17

    .line 410
    :catch_66
    move-exception v0

    .line 411
    :try_start_67
    const-string v1, "OperationQueueImpl"

    const-string v3, "Failed to save entry change in local database."

    invoke-static {v1, v3, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6e
    .catchall {:try_start_67 .. :try_end_6e} :catchall_a8

    .line 417
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0}, Llf;->b()V

    move v0, v2

    goto :goto_b

    .line 402
    :catch_75
    move-exception v0

    .line 403
    :try_start_76
    const-string v1, "OperationQueueImpl"

    const-string v3, "Failed to save operation in local database."

    invoke-static {v1, v3, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7d
    .catchall {:try_start_76 .. :try_end_7d} :catchall_a8

    .line 417
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0}, Llf;->b()V

    move v0, v2

    goto :goto_b

    .line 415
    :cond_84
    :try_start_84
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0}, Llf;->c()V
    :try_end_89
    .catchall {:try_start_84 .. :try_end_89} :catchall_a8

    .line 417
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0}, Llf;->b()V

    .line 421
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_92
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_af

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422
    iget-object v3, p0, LlF;->a:Llf;

    invoke-interface {v3, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    invoke-virtual {v0}, LkB;->a()V

    goto :goto_92

    .line 417
    :catchall_a8
    move-exception v0

    iget-object v1, p0, LlF;->a:Llf;

    invoke-interface {v1}, Llf;->b()V

    throw v0

    :cond_af
    move v0, v1

    .line 424
    goto/16 :goto_b
.end method

.method public b()V
    .registers 6

    .prologue
    .line 258
    iget-object v0, p0, LlF;->a:Llf;

    invoke-interface {v0}, Llf;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 259
    iget-object v2, p0, LlF;->a:Llf;

    invoke-interface {v2, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v2

    .line 260
    invoke-virtual {p0, v0}, LlF;->a(Ljava/lang/String;)Ljava/util/Queue;

    move-result-object v3

    .line 261
    const/4 v0, 0x0

    .line 263
    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_25
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_39

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    .line 264
    invoke-virtual {v0}, Llj;->a()V

    .line 265
    invoke-virtual {v0}, Llj;->c()V

    .line 266
    const/4 v0, 0x1

    goto :goto_25

    .line 268
    :cond_39
    if-eqz v0, :cond_a

    .line 271
    invoke-direct {p0, v2}, LlF;->a(LkB;)V

    goto :goto_a

    .line 274
    :cond_3f
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 369
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 374
    const/4 v0, 0x1

    return v0
.end method
