.class LapQ;
.super Ljava/lang/Object;
.source "InjectorImpl.java"


# instance fields
.field final a:LaoK;

.field final a:Z

.field final b:Z

.field final c:Z


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "stage"

    iget-object v2, p0, LapQ;->a:LaoK;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "jitDisabled"

    iget-boolean v2, p0, LapQ;->a:Z

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Z)Lagr;

    move-result-object v0

    const-string v1, "disableCircularProxies"

    iget-boolean v2, p0, LapQ;->b:Z

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Z)Lagr;

    move-result-object v0

    const-string v1, "atInjectRequired"

    iget-boolean v2, p0, LapQ;->c:Z

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Z)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
