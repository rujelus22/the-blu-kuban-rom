.class public final LSL;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LTc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 31
    iput-object p1, p0, LSL;->a:LYD;

    .line 32
    const-class v0, LTc;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LSL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LSL;->a:LZb;

    .line 35
    return-void
.end method

.method static synthetic a(LSL;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LSL;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 160
    const-class v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    new-instance v1, LSM;

    invoke-direct {v1, p0}, LSM;-><init>(LSL;)V

    invoke-virtual {p0, v0, v1}, LSL;->a(Ljava/lang/Class;Laou;)V

    .line 168
    const-class v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;

    new-instance v1, LSN;

    invoke-direct {v1, p0}, LSN;-><init>(LSL;)V

    invoke-virtual {p0, v0, v1}, LSL;->a(Ljava/lang/Class;Laou;)V

    .line 176
    const-class v0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;

    new-instance v1, LSO;

    invoke-direct {v1, p0}, LSO;-><init>(LSL;)V

    invoke-virtual {p0, v0, v1}, LSL;->a(Ljava/lang/Class;Laou;)V

    .line 184
    const-class v0, LTc;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LSL;->a:LZb;

    invoke-virtual {p0, v0, v1}, LSL;->a(Laop;LZb;)V

    .line 185
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->o:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfg;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Lfg;

    .line 137
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Llf;

    .line 143
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LSL;

    iget-object v0, v0, LSL;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTc;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTc;

    .line 149
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Laoz;

    .line 155
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 121
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->b:Laoz;

    .line 127
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogActivity;)V

    .line 43
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LrC;

    iget-object v0, v0, LrC;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LrR;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LrR;

    .line 49
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Laoz;

    .line 55
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->d:Laoz;

    .line 61
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Llf;

    .line 67
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->g:LZb;

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c:Laoz;

    .line 73
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZM;

    .line 79
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->t:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaD;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaD;

    .line 85
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LKS;

    .line 91
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZv;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZv;

    .line 97
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZj;

    .line 103
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lez;

    iget-object v0, v0, Lez;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lev;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lev;

    .line 109
    iget-object v0, p0, LSL;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LSL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LME;

    .line 115
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 189
    return-void
.end method
