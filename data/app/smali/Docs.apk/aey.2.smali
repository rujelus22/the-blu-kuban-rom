.class final Laey;
.super Laet;
.source "ApacheHttpRequest.java"


# instance fields
.field private final a:Lorg/apache/http/client/HttpClient;

.field private final a:Lorg/apache/http/client/methods/HttpRequestBase;


# direct methods
.method constructor <init>(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Laet;-><init>()V

    .line 40
    iput-object p1, p0, Laey;->a:Lorg/apache/http/client/HttpClient;

    .line 41
    iput-object p2, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 42
    return-void
.end method


# virtual methods
.method public a()Laeu;
    .registers 5

    .prologue
    .line 59
    new-instance v0, Laez;

    iget-object v1, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    iget-object v2, p0, Laey;->a:Lorg/apache/http/client/HttpClient;

    iget-object v3, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {v2, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Laez;-><init>(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;)V

    return-object v0
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 52
    int-to-long v1, p1

    invoke-static {v0, v1, v2}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 53
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 54
    invoke-static {v0, p2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 55
    return-void
.end method

.method public a(Laec;)V
    .registers 7
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    instance-of v0, v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    const-string v1, "Apache HTTP client does not support %s requests with content."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpRequestBase;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 67
    new-instance v1, LaeB;

    invoke-interface {p1}, Laec;->a()J

    move-result-wide v2

    invoke-direct {v1, v2, v3, p1}, LaeB;-><init>(JLaec;)V

    .line 68
    invoke-interface {p1}, Laec;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LaeB;->setContentEncoding(Ljava/lang/String;)V

    .line 69
    invoke-interface {p1}, Laec;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LaeB;->setContentType(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    check-cast v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/HttpEntityEnclosingRequest;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Laey;->a:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0, p1, p2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void
.end method
