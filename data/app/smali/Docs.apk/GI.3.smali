.class public final enum LGI;
.super Ljava/lang/Enum;
.source "TextFormatingSpan.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LGI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LGI;

.field private static final synthetic a:[LGI;

.field public static final enum b:LGI;

.field public static final enum c:LGI;


# instance fields
.field final a:D

.field final b:D


# direct methods
.method static constructor <clinit>()V
    .registers 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    const-wide v8, 0x3fe5555555555555L

    .line 34
    new-instance v0, LGI;

    const-string v1, "NORMAL"

    const-wide/16 v3, 0x0

    const-wide/high16 v5, 0x3ff0

    invoke-direct/range {v0 .. v6}, LGI;-><init>(Ljava/lang/String;IDD)V

    sput-object v0, LGI;->a:LGI;

    .line 35
    new-instance v3, LGI;

    const-string v4, "SUBSCRIPT"

    const-wide/high16 v6, -0x4020

    move v5, v10

    invoke-direct/range {v3 .. v9}, LGI;-><init>(Ljava/lang/String;IDD)V

    sput-object v3, LGI;->b:LGI;

    .line 36
    new-instance v3, LGI;

    const-string v4, "SUPERSCRIPT"

    const-wide/high16 v6, 0x3fe0

    move v5, v11

    invoke-direct/range {v3 .. v9}, LGI;-><init>(Ljava/lang/String;IDD)V

    sput-object v3, LGI;->c:LGI;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [LGI;

    sget-object v1, LGI;->a:LGI;

    aput-object v1, v0, v2

    sget-object v1, LGI;->b:LGI;

    aput-object v1, v0, v10

    sget-object v1, LGI;->c:LGI;

    aput-object v1, v0, v11

    sput-object v0, LGI;->a:[LGI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IDD)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DD)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput-wide p3, p0, LGI;->a:D

    .line 43
    iput-wide p5, p0, LGI;->b:D

    .line 44
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LGI;
    .registers 2
    .parameter

    .prologue
    .line 33
    const-class v0, LGI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LGI;

    return-object v0
.end method

.method public static values()[LGI;
    .registers 1

    .prologue
    .line 33
    sget-object v0, LGI;->a:[LGI;

    invoke-virtual {v0}, [LGI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGI;

    return-object v0
.end method
