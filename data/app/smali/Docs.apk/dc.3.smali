.class Ldc;
.super Ljava/lang/Object;
.source "PersistentAnalyticsStore.java"

# interfaces
.implements Lcg;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private a:J

.field private final a:Landroid/content/Context;

.field private final a:Lch;

.field private volatile a:Lcl;

.field private final a:Lde;

.field private a:Z

.field private b:J

.field private final b:Ljava/lang/String;

.field private c:J


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 62
    const-string v0, "CREATE TABLE IF NOT EXISTS %s ( \'%s\' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \'%s\' INTEGER NOT NULL, \'%s\' TEXT NOT NULL, \'%s\' TEXT NOT NULL);"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "hits2"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "hit_id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "hit_time"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "hit_url"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "hit_string"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldc;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lch;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 91
    const-string v0, "google_analytics_v2.db"

    invoke-direct {p0, p1, p2, v0}, Ldc;-><init>(Lch;Landroid/content/Context;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method constructor <init>(Lch;Landroid/content/Context;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Ldc;->a:J

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldc;->a:Z

    .line 109
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldc;->a:Landroid/content/Context;

    .line 110
    iput-object p3, p0, Ldc;->b:Ljava/lang/String;

    .line 111
    iput-object p1, p0, Ldc;->a:Lch;

    .line 112
    new-instance v0, Lde;

    iget-object v1, p0, Ldc;->a:Landroid/content/Context;

    iget-object v2, p0, Ldc;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lde;-><init>(Ldc;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Ldc;->a:Lde;

    .line 113
    new-instance v0, Ldh;

    invoke-direct {p0}, Ldc;->a()LcS;

    move-result-object v1

    iget-object v2, p0, Ldc;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1, v2}, Ldh;-><init>(Lcg;LcS;Landroid/content/Context;)V

    iput-object v0, p0, Ldc;->a:Lcl;

    .line 116
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldc;->c:J

    .line 117
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 3
    .parameter

    .prologue
    .line 562
    .line 564
    :try_start_0
    iget-object v0, p0, Ldc;->a:Lde;

    invoke-virtual {v0}, Lde;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 569
    :goto_6
    return-object v0

    .line 565
    :catch_7
    move-exception v0

    .line 566
    invoke-static {p1}, LcT;->h(Ljava/lang/String;)I

    .line 567
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private a()LcS;
    .registers 2

    .prologue
    .line 125
    new-instance v0, Ldd;

    invoke-direct {v0, p0}, Ldd;-><init>(Ldc;)V

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .registers 1

    .prologue
    .line 36
    sget-object v0, Ldc;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/util/Map;)Ljava/lang/String;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 267
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 268
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 269
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LcR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 271
    :cond_48
    const-string v0, "&"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Map;JLjava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 236
    const-string v0, "Error opening database for putHit"

    invoke-direct {p0, v0}, Ldc;->a(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 237
    if-nez v0, :cond_9

    .line 264
    :goto_8
    return-void

    .line 241
    :cond_9
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 243
    const-string v2, "hit_string"

    invoke-static {p1}, Ldc;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v2, "hit_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 245
    if-nez p4, :cond_24

    .line 246
    const-string p4, "http://www.google-analytics.com/collect"

    .line 249
    :cond_24
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_30

    .line 252
    const-string v0, "empty path: not sending hit"

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    goto :goto_8

    .line 255
    :cond_30
    const-string v2, "hit_url"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :try_start_35
    const-string v2, "hits2"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 260
    iget-object v0, p0, Ldc;->a:Lch;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lch;->a(Z)V
    :try_end_41
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_35 .. :try_end_41} :catch_42

    goto :goto_8

    .line 261
    :catch_42
    move-exception v0

    .line 262
    const-string v0, "Error storing hit"

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    goto :goto_8
.end method

.method private a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 207
    .line 208
    if-nez p3, :cond_4

    .line 209
    const-string p3, "-s1"

    .line 214
    :cond_4
    if-eqz p2, :cond_9

    .line 215
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    :cond_9
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/util/Collection;)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/analytics/internal/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/internal/Command;

    .line 194
    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/Command;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "appendVersion"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 195
    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/Command;->c()Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/Command;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, v1}, Ldc;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_27
    return-void
.end method

.method private c()V
    .registers 4

    .prologue
    .line 225
    invoke-virtual {p0}, Ldc;->b()I

    move-result v0

    add-int/lit16 v0, v0, -0x7d0

    add-int/lit8 v0, v0, 0x1

    .line 226
    if-lez v0, :cond_31

    .line 227
    invoke-virtual {p0, v0}, Ldc;->a(I)Ljava/util/List;

    move-result-object v0

    .line 228
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Store full, deleting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hits to make room"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LcT;->i(Ljava/lang/String;)I

    .line 229
    invoke-virtual {p0, v0}, Ldc;->a(Ljava/util/Collection;)V

    .line 231
    :cond_31
    return-void
.end method


# virtual methods
.method a()I
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 391
    iget-wide v4, p0, Ldc;->c:J

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    cmp-long v4, v2, v4

    if-gtz v4, :cond_11

    .line 402
    :cond_10
    :goto_10
    return v1

    .line 394
    :cond_11
    iput-wide v2, p0, Ldc;->c:J

    .line 395
    const-string v2, "Error opening database for deleteStaleHits"

    invoke-direct {p0, v2}, Ldc;->a(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 396
    if-eqz v2, :cond_10

    .line 399
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide v5, 0x9a7ec800L

    sub-long/2addr v3, v5

    .line 400
    const-string v5, "hits2"

    const-string v6, "HIT_TIME < ?"

    new-array v7, v0, [Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v1

    invoke-virtual {v2, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 401
    iget-object v3, p0, Ldc;->a:Lch;

    invoke-virtual {p0}, Ldc;->b()I

    move-result v4

    if-nez v4, :cond_42

    :goto_3d
    invoke-interface {v3, v0}, Lch;->a(Z)V

    move v1, v2

    .line 402
    goto :goto_10

    :cond_42
    move v0, v1

    .line 401
    goto :goto_3d
.end method

.method public a(I)Ljava/util/List;
    .registers 17
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "LcQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283
    const-string v1, "Error opening database for peekHits"

    invoke-direct {p0, v1}, Ldc;->a(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 284
    if-nez v1, :cond_e

    .line 285
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 373
    :cond_d
    :goto_d
    return-object v1

    .line 288
    :cond_e
    const/4 v10, 0x0

    .line 289
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 298
    :try_start_14
    const-string v2, "hits2"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "hit_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "hit_time"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "hit_url"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "%s ASC, %s ASC"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "hit_url"

    aput-object v12, v9, v11

    const/4 v11, 0x1

    const-string v12, "hit_id"

    aput-object v12, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_46
    .catchall {:try_start_14 .. :try_end_46} :catchall_101
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_14 .. :try_end_46} :catch_d9

    move-result-object v11

    .line 306
    :try_start_47
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 307
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_73

    .line 309
    :cond_52
    new-instance v2, LcQ;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x1

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, LcQ;-><init>(Ljava/lang/String;JJ)V

    .line 310
    const/4 v3, 0x2

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LcQ;->b(Ljava/lang/String;)V

    .line 311
    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_70
    .catchall {:try_start_47 .. :try_end_70} :catchall_196
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_47 .. :try_end_70} :catch_19e

    move-result v2

    if-nez v2, :cond_52

    .line 318
    :cond_73
    if-eqz v11, :cond_78

    .line 319
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 323
    :cond_78
    const/4 v12, 0x0

    .line 325
    :try_start_79
    const-string v2, "hits2"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "hit_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "hit_string"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "%s ASC"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "hit_id"

    aput-object v14, v9, v13

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_a1
    .catchall {:try_start_79 .. :try_end_a1} :catchall_192
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_79 .. :try_end_a1} :catch_194

    move-result-object v2

    .line 333
    :try_start_a2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_d1

    move v3, v12

    .line 335
    :goto_a9
    instance-of v1, v2, Landroid/database/sqlite/SQLiteCursor;

    if-eqz v1, :cond_175

    .line 336
    move-object v0, v2

    check-cast v0, Landroid/database/sqlite/SQLiteCursor;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteCursor;->getWindow()Landroid/database/CursorWindow;

    move-result-object v1

    .line 340
    invoke-virtual {v1}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v1

    if-lez v1, :cond_108

    .line 341
    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LcQ;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LcQ;->a(Ljava/lang/String;)V

    .line 349
    :goto_c9
    add-int/lit8 v1, v3, 0x1

    .line 350
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_ce
    .catchall {:try_start_a2 .. :try_end_ce} :catchall_185
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a2 .. :try_end_ce} :catch_12f

    move-result v3

    if-nez v3, :cond_1a2

    .line 372
    :cond_d1
    if-eqz v2, :cond_d6

    .line 373
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_d6
    move-object v1, v10

    goto/16 :goto_d

    .line 314
    :catch_d9
    move-exception v1

    move-object v2, v10

    .line 315
    :goto_db
    :try_start_db
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error in peekHits fetching hitIds: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LcT;->h(Ljava/lang/String;)I

    .line 316
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
    :try_end_fa
    .catchall {:try_start_db .. :try_end_fa} :catchall_19a

    .line 318
    if-eqz v2, :cond_d

    .line 319
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_d

    .line 318
    :catchall_101
    move-exception v1

    :goto_102
    if-eqz v10, :cond_107

    .line 319
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_107
    throw v1

    .line 343
    :cond_108
    :try_start_108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hitString for hitId "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LcQ;

    invoke-virtual {v1}, LcQ;->a()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " too large.  Hit will be deleted."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LcT;->h(Ljava/lang/String;)I
    :try_end_12e
    .catchall {:try_start_108 .. :try_end_12e} :catchall_185
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_108 .. :try_end_12e} :catch_12f

    goto :goto_c9

    .line 353
    :catch_12f
    move-exception v1

    move-object v11, v2

    .line 354
    :goto_131
    :try_start_131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error in peekHits fetching hitString: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LcT;->h(Ljava/lang/String;)I

    .line 358
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 359
    const/4 v3, 0x0

    .line 360
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_155
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LcQ;

    .line 361
    invoke-virtual {v1}, LcQ;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_168
    .catchall {:try_start_131 .. :try_end_168} :catchall_192

    move-result v5

    if-eqz v5, :cond_18e

    .line 362
    if-eqz v3, :cond_18d

    .line 372
    :cond_16d
    if-eqz v11, :cond_172

    .line 373
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_172
    move-object v1, v2

    goto/16 :goto_d

    .line 347
    :cond_175
    :try_start_175
    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LcQ;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LcQ;->a(Ljava/lang/String;)V
    :try_end_183
    .catchall {:try_start_175 .. :try_end_183} :catchall_185
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_175 .. :try_end_183} :catch_12f

    goto/16 :goto_c9

    .line 372
    :catchall_185
    move-exception v1

    move-object v11, v2

    :goto_187
    if-eqz v11, :cond_18c

    .line 373
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_18c
    throw v1

    .line 365
    :cond_18d
    const/4 v3, 0x1

    .line 368
    :cond_18e
    :try_start_18e
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_191
    .catchall {:try_start_18e .. :try_end_191} :catchall_192

    goto :goto_155

    .line 372
    :catchall_192
    move-exception v1

    goto :goto_187

    .line 353
    :catch_194
    move-exception v1

    goto :goto_131

    .line 318
    :catchall_196
    move-exception v1

    move-object v10, v11

    goto/16 :goto_102

    :catchall_19a
    move-exception v1

    move-object v10, v2

    goto/16 :goto_102

    .line 314
    :catch_19e
    move-exception v1

    move-object v2, v11

    goto/16 :goto_db

    :cond_1a2
    move v3, v1

    goto/16 :goto_a9
.end method

.method public a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 151
    const-string v0, "Error opening database for clearHits"

    invoke-direct {p0, v0}, Ldc;->a(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_14

    .line 156
    const-string v1, "hits2"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 157
    iget-object v0, p0, Ldc;->a:Lch;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lch;->a(Z)V

    .line 159
    :cond_14
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .registers 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LcQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 411
    if-nez p1, :cond_c

    .line 412
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "hits cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_c
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 436
    :cond_12
    :goto_12
    return-void

    .line 417
    :cond_13
    const-string v0, "Error opening database for deleteHit"

    invoke-direct {p0, v0}, Ldc;->a(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 418
    if-eqz v5, :cond_12

    .line 421
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v6, v0, [Ljava/lang/String;

    .line 422
    const-string v0, "HIT_ID in (%s)"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v3, ","

    array-length v7, v6

    const-string v8, "?"

    invoke-static {v7, v8}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 425
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_3d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LcQ;

    .line 426
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, LcQ;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    move v1, v3

    goto :goto_3d

    .line 429
    :cond_57
    :try_start_57
    const-string v0, "hits2"

    invoke-virtual {v5, v0, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 432
    iget-object v0, p0, Ldc;->a:Lch;

    invoke-virtual {p0}, Ldc;->b()I

    move-result v1

    if-nez v1, :cond_65

    move v2, v4

    :cond_65
    invoke-interface {v0, v2}, Lch;->a(Z)V
    :try_end_68
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_57 .. :try_end_68} :catch_69

    goto :goto_12

    .line 433
    :catch_69
    move-exception v0

    .line 434
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error deleting hit "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    goto :goto_12
.end method

.method public a(Ljava/util/Map;JLjava/lang/String;Ljava/util/Collection;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/analytics/internal/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 177
    invoke-virtual {p0}, Ldc;->a()I

    .line 178
    invoke-virtual {p0}, Ldc;->a()Z

    move-result v0

    if-nez v0, :cond_a

    .line 185
    :goto_9
    return-void

    .line 181
    :cond_a
    invoke-direct {p0, p1, p5}, Ldc;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 183
    invoke-direct {p0}, Ldc;->c()V

    .line 184
    invoke-direct {p0, p1, p2, p3, p4}, Ldc;->a(Ljava/util/Map;JLjava/lang/String;)V

    goto :goto_9
.end method

.method declared-synchronized a()Z
    .registers 12

    .prologue
    const-wide/32 v5, 0x1d4c0

    const-wide/16 v9, 0x7d0

    const/4 v0, 0x1

    .line 519
    monitor-enter p0

    :try_start_7
    iget-boolean v1, p0, Ldc;->a:Z
    :try_end_9
    .catchall {:try_start_7 .. :try_end_9} :catchall_3b

    if-nez v1, :cond_d

    .line 535
    :goto_b
    monitor-exit p0

    return v0

    .line 522
    :cond_d
    :try_start_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 523
    iget-wide v3, p0, Ldc;->a:J

    cmp-long v3, v3, v5

    if-gez v3, :cond_2d

    .line 524
    iget-wide v3, p0, Ldc;->b:J

    sub-long v3, v1, v3

    .line 525
    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_2d

    .line 526
    const-wide/32 v5, 0x1d4c0

    iget-wide v7, p0, Ldc;->a:J

    add-long/2addr v3, v7

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    iput-wide v3, p0, Ldc;->a:J

    .line 529
    :cond_2d
    iput-wide v1, p0, Ldc;->b:J

    .line 530
    iget-wide v1, p0, Ldc;->a:J

    cmp-long v1, v1, v9

    if-ltz v1, :cond_3e

    .line 531
    iget-wide v1, p0, Ldc;->a:J

    sub-long/2addr v1, v9

    iput-wide v1, p0, Ldc;->a:J
    :try_end_3a
    .catchall {:try_start_d .. :try_end_3a} :catchall_3b

    goto :goto_b

    .line 519
    :catchall_3b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 534
    :cond_3e
    :try_start_3e
    const-string v0, "Excessive tracking detected.  Tracking call ignored."

    invoke-static {v0}, LcT;->i(Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_3e .. :try_end_43} :catchall_3b

    .line 535
    const/4 v0, 0x0

    goto :goto_b
.end method

.method b()I
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 439
    .line 440
    const-string v2, "Error opening database for requestNumHitsPending"

    invoke-direct {p0, v2}, Ldc;->a(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 441
    if-nez v2, :cond_b

    .line 457
    :cond_a
    :goto_a
    return v0

    .line 446
    :cond_b
    :try_start_b
    const-string v3, "SELECT COUNT(*) from hits2"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 447
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 448
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1c
    .catchall {:try_start_b .. :try_end_1c} :catchall_30
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_1c} :catch_24

    move-result-wide v2

    long-to-int v0, v2

    .line 453
    :cond_1e
    if-eqz v1, :cond_a

    .line 454
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_a

    .line 450
    :catch_24
    move-exception v2

    .line 451
    :try_start_25
    const-string v2, "Error getting numStoredHits"

    invoke-static {v2}, LcT;->h(Ljava/lang/String;)I
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_30

    .line 453
    if-eqz v1, :cond_a

    .line 454
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_a

    .line 453
    :catchall_30
    move-exception v0

    if-eqz v1, :cond_36

    .line 454
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_36
    throw v0
.end method

.method public b()V
    .registers 5

    .prologue
    .line 462
    const-string v0, "dispatch running..."

    invoke-static {v0}, LcT;->g(Ljava/lang/String;)I

    .line 464
    iget-object v0, p0, Ldc;->a:Lcl;

    invoke-interface {v0}, Lcl;->a()Z

    move-result v0

    if-nez v0, :cond_e

    .line 486
    :cond_d
    :goto_d
    return-void

    .line 468
    :cond_e
    const/16 v0, 0x28

    invoke-virtual {p0, v0}, Ldc;->a(I)Ljava/util/List;

    move-result-object v0

    .line 469
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 470
    const-string v0, "...nothing to dispatch"

    invoke-static {v0}, LcT;->g(Ljava/lang/String;)I

    goto :goto_d

    .line 473
    :cond_20
    iget-object v1, p0, Ldc;->a:Lcl;

    invoke-interface {v1, v0}, Lcl;->a(Ljava/util/List;)I

    move-result v1

    .line 474
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " hits"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LcT;->g(Ljava/lang/String;)I

    .line 479
    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-interface {v0, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v2}, Ldc;->a(Ljava/util/Collection;)V

    .line 483
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_d

    invoke-virtual {p0}, Ldc;->b()I

    move-result v0

    if-lez v0, :cond_d

    .line 484
    invoke-static {}, Lct;->a()Lct;

    move-result-object v0

    invoke-virtual {v0}, Lct;->a()V

    goto :goto_d
.end method
