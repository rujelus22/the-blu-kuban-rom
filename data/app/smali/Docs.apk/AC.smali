.class public abstract LAC;
.super Ljava/lang/Object;
.source "LeafElement.java"

# interfaces
.implements LAx;


# instance fields
.field protected final a:LDI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDI",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field protected final a:LDb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDb",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LDb;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LAC;->a:LDb;

    .line 37
    iget-object v0, p0, LAC;->a:LDb;

    invoke-virtual {v0, p0}, LDb;->a(Ljava/lang/Object;)V

    .line 38
    new-instance v0, LDI;

    invoke-direct {v0}, LDI;-><init>()V

    iput-object v0, p0, LAC;->a:LDI;

    .line 39
    iget-object v0, p0, LAC;->a:LDI;

    invoke-virtual {v0, p0}, LDI;->a(Ljava/lang/Object;)V

    .line 40
    return-void
.end method

.method private a(IILza;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, LAC;->a:LDb;

    neg-int v1, p2

    invoke-virtual {v0, v1}, LDb;->a(I)V

    .line 140
    invoke-virtual {p0, p1, p2}, LAC;->a(II)V

    .line 141
    neg-int v0, p2

    invoke-interface {p3, p1, v0}, Lza;->b(II)V

    .line 142
    return-void
.end method

.method private b(IILza;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 149
    iget-object v0, p0, LAC;->a:LDb;

    invoke-virtual {v0, p2}, LDb;->a(I)V

    .line 150
    invoke-virtual {p0, p1, p2}, LAC;->b(II)V

    .line 151
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p3, v0, p2}, Lza;->a(II)V

    .line 152
    return-void
.end method


# virtual methods
.method public a()LDG;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDG",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, LAC;->a:LDI;

    return-object v0
.end method

.method protected abstract a(II)V
.end method

.method public a(Lwj;Lza;I)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, LAC;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v2

    .line 53
    :goto_7
    invoke-static {p1}, Lzp;->a(Lwj;)Lzp;

    move-result-object v0

    sget-object v3, Lzp;->a:Lzp;

    if-eq v0, v3, :cond_81

    invoke-interface {p1}, Lwj;->b()I

    move-result v0

    iget-object v3, p0, LAC;->a:LDb;

    invoke-virtual {v3}, LDb;->d()I

    move-result v3

    if-ge v0, v3, :cond_81

    invoke-interface {p1}, Lwj;->b()I

    move-result v0

    if-ge v0, p3, :cond_81

    .line 54
    invoke-static {p1}, Lzp;->a(Lwj;)Lzp;

    move-result-object v3

    .line 55
    invoke-interface {p1}, Lwj;->b()I

    move-result v4

    .line 56
    if-lt v4, v2, :cond_51

    const/4 v0, 0x1

    :goto_2c
    invoke-static {v0}, Lagu;->a(Z)V

    .line 57
    sget-object v0, LAD;->a:[I

    invoke-virtual {v3}, Lzp;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_9e

    goto :goto_7

    .line 59
    :pswitch_3b
    sub-int v0, v4, v2

    .line 60
    invoke-interface {p1}, Lwj;->d()I

    move-result v3

    iget-object v5, p0, LAC;->a:LDb;

    invoke-virtual {v5}, LDb;->e()I

    move-result v5

    sub-int v0, v5, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 62
    invoke-direct {p0, v4, v0, p2}, LAC;->a(IILza;)V

    goto :goto_7

    :cond_51
    move v0, v1

    .line 56
    goto :goto_2c

    .line 66
    :pswitch_53
    invoke-interface {p1}, Lwj;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 67
    sub-int/2addr v0, v4

    .line 68
    invoke-direct {p0, v4, v0, p2}, LAC;->b(IILza;)V

    goto :goto_7

    .line 72
    :pswitch_62
    iget-object v0, p0, LAC;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 73
    invoke-interface {p1}, Lwj;->c()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 74
    sub-int v3, v0, v4

    invoke-virtual {p0, v4, v3}, LAC;->c(II)V

    .line 75
    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0, v1}, Lza;->a(II)V

    goto :goto_7

    .line 83
    :cond_81
    sub-int v0, p3, v2

    .line 84
    iget-object v1, p0, LAC;->a:LDb;

    invoke-virtual {v1}, LDb;->e()I

    move-result v1

    sub-int/2addr v0, v1

    .line 85
    if-lez v0, :cond_96

    .line 86
    iget-object v1, p0, LAC;->a:LDb;

    invoke-virtual {v1}, LDb;->d()I

    move-result v1

    .line 87
    invoke-direct {p0, v1, v0, p2}, LAC;->b(IILza;)V

    .line 91
    :cond_95
    :goto_95
    return-void

    .line 88
    :cond_96
    if-gez v0, :cond_95

    .line 89
    neg-int v0, v0

    invoke-direct {p0, p3, v0, p2}, LAC;->a(IILza;)V

    goto :goto_95

    .line 57
    nop

    :pswitch_data_9e
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_53
        :pswitch_62
    .end packed-switch
.end method

.method public a(Lza;)V
    .registers 4
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, LAC;->a:LDb;

    invoke-virtual {v0}, LDb;->e()I

    move-result v0

    .line 96
    iget-object v1, p0, LAC;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    .line 97
    invoke-direct {p0, v1, v0, p1}, LAC;->a(IILza;)V

    .line 98
    return-void
.end method

.method protected abstract b(II)V
.end method

.method protected abstract c(II)V
.end method
