.class public LUj;
.super Ljava/lang/Object;
.source "SelectionDialogHelper.java"


# instance fields
.field private a:I

.field private a:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, LUj;->a:I

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, LUj;->a:Landroid/widget/ListView;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, LUj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    return v0
.end method

.method public a(LUl;LdL;Landroid/app/Activity;)Landroid/content/DialogInterface$OnClickListener;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    new-instance v0, LUk;

    invoke-direct {v0, p0, p2, p3, p1}, LUk;-><init>(LUj;LdL;Landroid/app/Activity;LUl;)V

    return-object v0
.end method

.method public a()V
    .registers 4

    .prologue
    .line 92
    iget-object v0, p0, LUj;->a:Landroid/widget/ListView;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, LUj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    .line 94
    iget v1, p0, LUj;->a:I

    invoke-static {v1, v0}, Lagu;->b(II)I

    .line 96
    iget-object v0, p0, LUj;->a:Landroid/widget/ListView;

    iget v1, p0, LUj;->a:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 97
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 47
    if-ltz p1, :cond_9

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 48
    iput p1, p0, LUj;->a:I

    .line 49
    return-void

    .line 47
    :cond_9
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 81
    if-eqz p1, :cond_a

    .line 82
    const-string v0, "selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LUj;->a:I

    .line 84
    :cond_a
    return-void
.end method

.method public a(Landroid/widget/ListView;)V
    .registers 4
    .parameter

    .prologue
    .line 74
    iget v0, p0, LUj;->a:I

    if-ltz v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    const-string v1, "The initial selection state has not been set yet."

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 77
    iput-object p1, p0, LUj;->a:Landroid/widget/ListView;

    .line 78
    return-void

    .line 74
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0}, LUj;->a()I

    move-result v0

    iput v0, p0, LUj;->a:I

    .line 88
    const-string v0, "selection"

    iget v1, p0, LUj;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 89
    return-void
.end method
