.class public LEd;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "EditableInputConnection.java"


# instance fields
.field private a:Landroid/text/Editable;

.field private final a:Lcom/google/android/apps/docs/editors/text/TextView;

.field private b:Landroid/text/Editable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    .line 42
    iput-object p1, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    .line 43
    return-void
.end method


# virtual methods
.method public beginBatchEdit()Z
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()V

    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public clearMetaKeyStates(I)Z
    .registers 5
    .parameter

    .prologue
    .line 78
    invoke-virtual {p0}, LEd;->getEditable()Landroid/text/Editable;

    move-result-object v0

    .line 79
    if-nez v0, :cond_8

    const/4 v0, 0x0

    .line 89
    :goto_7
    return v0

    .line 80
    :cond_8
    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/text/method/KeyListener;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_15

    .line 83
    :try_start_10
    iget-object v2, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-interface {v1, v2, v0, p1}, Landroid/text/method/KeyListener;->clearMetaKeyState(Landroid/view/View;Landroid/text/Editable;I)V
    :try_end_15
    .catch Ljava/lang/AbstractMethodError; {:try_start_10 .. :try_end_15} :catch_17

    .line 89
    :cond_15
    :goto_15
    const/4 v0, 0x1

    goto :goto_7

    .line 84
    :catch_17
    move-exception v0

    goto :goto_15
.end method

.method public commitCompletion(Landroid/view/inputmethod/CompletionInfo;)Z
    .registers 3
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()V

    .line 96
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/view/inputmethod/CompletionInfo;)V

    .line 97
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)Z
    .registers 3
    .parameter
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()V

    .line 109
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/view/inputmethod/CorrectionInfo;)V

    .line 110
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    .line 111
    const/4 v0, 0x1

    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 152
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    if-nez v0, :cond_9

    .line 153
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    .line 158
    :goto_8
    return v0

    .line 156
    :cond_9
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    goto :goto_8
.end method

.method public endBatchEdit()Z
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public getEditable()Landroid/text/Editable;
    .registers 3

    .prologue
    .line 47
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    .line 48
    if-eqz v0, :cond_21

    .line 49
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Landroid/text/Editable;

    move-result-object v0

    .line 50
    iget-object v1, p0, LEd;->b:Landroid/text/Editable;

    if-eq v0, v1, :cond_1b

    .line 51
    iput-object v0, p0, LEd;->b:Landroid/text/Editable;

    .line 52
    instance-of v1, v0, LEg;

    if-eqz v1, :cond_1e

    .line 54
    new-instance v1, LEf;

    check-cast v0, LEg;

    invoke-direct {v1, v0}, LEf;-><init>(LEg;)V

    iput-object v1, p0, LEd;->a:Landroid/text/Editable;

    .line 59
    :cond_1b
    :goto_1b
    iget-object v0, p0, LEd;->a:Landroid/text/Editable;

    .line 61
    :goto_1d
    return-object v0

    .line 56
    :cond_1e
    iput-object v0, p0, LEd;->a:Landroid/text/Editable;

    goto :goto_1b

    .line 61
    :cond_21
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    if-eqz v0, :cond_1b

    .line 133
    new-instance v0, Landroid/view/inputmethod/ExtractedText;

    invoke-direct {v0}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    .line 134
    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/view/inputmethod/ExtractedTextRequest;Landroid/view/inputmethod/ExtractedText;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 135
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1a

    .line 136
    iget-object v1, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setExtracting(Landroid/view/inputmethod/ExtractedTextRequest;)V

    .line 141
    :cond_1a
    :goto_1a
    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public performContextMenuAction(I)Z
    .registers 3
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()V

    .line 125
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)Z

    .line 126
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public performEditorAction(I)Z
    .registers 3
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)V

    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method public performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, LEd;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 147
    const/4 v0, 0x1

    return v0
.end method
