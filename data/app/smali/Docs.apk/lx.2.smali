.class public final enum Llx;
.super Ljava/lang/Enum;
.source "MenuItemsState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Llx;

.field private static final synthetic a:[Llx;

.field public static final enum b:Llx;

.field public static final enum c:Llx;

.field public static final enum d:Llx;

.field public static final enum e:Llx;

.field public static final enum f:Llx;

.field public static final enum g:Llx;

.field public static final enum h:Llx;

.field public static final enum i:Llx;

.field public static final enum j:Llx;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 45
    new-instance v0, Llx;

    const-string v1, "COMMENT"

    sget v2, Leh;->menu_comments:I

    invoke-direct {v0, v1, v4, v2}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->a:Llx;

    .line 46
    new-instance v0, Llx;

    const-string v1, "DELETE"

    sget v2, Leh;->menu_delete:I

    invoke-direct {v0, v1, v5, v2}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->b:Llx;

    .line 47
    new-instance v0, Llx;

    const-string v1, "SHARING"

    sget v2, Leh;->menu_sharing:I

    invoke-direct {v0, v1, v6, v2}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->c:Llx;

    .line 48
    new-instance v0, Llx;

    const-string v1, "OPEN_WITH"

    sget v2, Leh;->menu_open_with:I

    invoke-direct {v0, v1, v7, v2}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->d:Llx;

    .line 49
    new-instance v0, Llx;

    const-string v1, "SEND"

    sget v2, Leh;->menu_send:I

    invoke-direct {v0, v1, v8, v2}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->e:Llx;

    .line 50
    new-instance v0, Llx;

    const-string v1, "PRINT"

    const/4 v2, 0x5

    sget v3, Leh;->menu_print:I

    invoke-direct {v0, v1, v2, v3}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->f:Llx;

    .line 51
    new-instance v0, Llx;

    const-string v1, "SEND_LINK"

    const/4 v2, 0x6

    sget v3, Leh;->menu_send_link:I

    invoke-direct {v0, v1, v2, v3}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->g:Llx;

    .line 52
    new-instance v0, Llx;

    const-string v1, "RENAME"

    const/4 v2, 0x7

    sget v3, Leh;->menu_rename:I

    invoke-direct {v0, v1, v2, v3}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->h:Llx;

    .line 53
    new-instance v0, Llx;

    const-string v1, "PIN"

    const/16 v2, 0x8

    sget v3, Leh;->menu_pin:I

    invoke-direct {v0, v1, v2, v3}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->i:Llx;

    .line 54
    new-instance v0, Llx;

    const-string v1, "MOVE_TO_FOLDER"

    const/16 v2, 0x9

    sget v3, Leh;->menu_move_to_folder:I

    invoke-direct {v0, v1, v2, v3}, Llx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llx;->j:Llx;

    .line 44
    const/16 v0, 0xa

    new-array v0, v0, [Llx;

    sget-object v1, Llx;->a:Llx;

    aput-object v1, v0, v4

    sget-object v1, Llx;->b:Llx;

    aput-object v1, v0, v5

    sget-object v1, Llx;->c:Llx;

    aput-object v1, v0, v6

    sget-object v1, Llx;->d:Llx;

    aput-object v1, v0, v7

    sget-object v1, Llx;->e:Llx;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Llx;->f:Llx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Llx;->g:Llx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Llx;->h:Llx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Llx;->i:Llx;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Llx;->j:Llx;

    aput-object v2, v0, v1

    sput-object v0, Llx;->a:[Llx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput p3, p0, Llx;->a:I

    .line 60
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llx;
    .registers 2
    .parameter

    .prologue
    .line 44
    const-class v0, Llx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llx;

    return-object v0
.end method

.method public static values()[Llx;
    .registers 1

    .prologue
    .line 44
    sget-object v0, Llx;->a:[Llx;

    invoke-virtual {v0}, [Llx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llx;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 63
    iget v0, p0, Llx;->a:I

    return v0
.end method
