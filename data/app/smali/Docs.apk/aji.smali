.class public abstract Laji;
.super Lajd;
.source "ImmutableList.java"

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lajd",
        "<TE;>;",
        "Ljava/util/List",
        "<TE;>;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 310
    invoke-direct {p0}, Lajd;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Iterable;)Laji;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 216
    invoke-static {p0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_10

    invoke-static {p0}, LaiH;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Laji;->a(Ljava/util/Collection;)Laji;

    move-result-object v0

    :goto_f
    return-object v0

    :cond_10
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Laji;->a(Ljava/util/Iterator;)Laji;

    move-result-object v0

    goto :goto_f
.end method

.method public static a(Ljava/lang/Object;)Laji;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, LalP;

    invoke-direct {v0, p0}, LalP;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Laji;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Laji;->b([Ljava/lang/Object;)Laji;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Laji;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v0}, Laji;->b([Ljava/lang/Object;)Laji;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Laji;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 135
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    invoke-static {v0}, Laji;->b([Ljava/lang/Object;)Laji;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Laji;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 242
    instance-of v0, p0, Lajd;

    if-eqz v0, :cond_15

    .line 244
    check-cast p0, Lajd;

    invoke-virtual {p0}, Lajd;->a()Laji;

    move-result-object v0

    .line 245
    invoke-virtual {v0}, Laji;->a()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-static {v0}, Laji;->b(Ljava/util/Collection;)Laji;

    move-result-object v0

    .line 247
    :cond_14
    :goto_14
    return-object v0

    :cond_15
    invoke-static {p0}, Laji;->b(Ljava/util/Collection;)Laji;

    move-result-object v0

    goto :goto_14
.end method

.method public static a(Ljava/util/Iterator;)Laji;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 256
    invoke-static {p0}, LajX;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Laji;->b(Ljava/util/Collection;)Laji;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/lang/Object;)Laji;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 266
    array-length v0, p0

    packed-switch v0, :pswitch_data_1e

    .line 272
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Laji;->b([Ljava/lang/Object;)Laji;

    move-result-object v0

    :goto_e
    return-object v0

    .line 268
    :pswitch_f
    invoke-static {}, Laji;->c()Laji;

    move-result-object v0

    goto :goto_e

    .line 270
    :pswitch_14
    new-instance v0, LalP;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, LalP;-><init>(Ljava/lang/Object;)V

    goto :goto_e

    .line 266
    nop

    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_f
        :pswitch_14
    .end packed-switch
.end method

.method public static a()Lajj;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lajj",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 535
    new-instance v0, Lajj;

    invoke-direct {v0}, Lajj;-><init>()V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 304
    if-nez p0, :cond_1b

    .line 305
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_1b
    return-object p0
.end method

.method private static b(Ljava/util/Collection;)Laji;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 278
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 279
    array-length v0, v1

    packed-switch v0, :pswitch_data_1c

    .line 289
    invoke-static {v1}, Laji;->b([Ljava/lang/Object;)Laji;

    move-result-object v0

    :goto_c
    return-object v0

    .line 281
    :pswitch_d
    invoke-static {}, Laji;->c()Laji;

    move-result-object v0

    goto :goto_c

    .line 284
    :pswitch_12
    new-instance v0, LalP;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, LalP;-><init>(Ljava/lang/Object;)V

    goto :goto_c

    .line 279
    nop

    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_d
        :pswitch_12
    .end packed-switch
.end method

.method private static varargs b([Ljava/lang/Object;)Laji;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            ")",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 295
    const/4 v0, 0x0

    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_c

    .line 296
    aget-object v1, p0, v0

    invoke-static {v1, v0}, Laji;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 295
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 298
    :cond_c
    new-instance v0, Lalz;

    invoke-direct {v0, p0}, Lalz;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method public static c()Laji;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 68
    sget-object v0, LaiQ;->a:LaiQ;

    return-object v0
.end method


# virtual methods
.method public a()Laji;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laji",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 389
    return-object p0
.end method

.method public abstract a(II)Laji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Laji",
            "<TE;>;"
        }
    .end annotation
.end method

.method public a()Laml;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laml",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 315
    invoke-virtual {p0}, Laji;->a()Lamm;

    move-result-object v0

    return-object v0
.end method

.method public a()Lamm;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lamm",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 319
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Laji;->a(I)Lamm;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(I)Lamm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lamm",
            "<TE;>;"
        }
    .end annotation
.end method

.method public final add(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 370
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 499
    invoke-static {p0, p1}, LajX;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 503
    invoke-static {p0}, LajX;->a(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method public abstract indexOf(Ljava/lang/Object;)I
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Laji;->a()Laml;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Laji;->a()Lamm;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .registers 3
    .parameter

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Laji;->a(I)Lamm;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 380
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 360
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2}, Laji;->a(II)Laji;

    move-result-object v0

    return-object v0
.end method
