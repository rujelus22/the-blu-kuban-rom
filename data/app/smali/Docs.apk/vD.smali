.class public LvD;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements LvC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "LvC;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2652
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 2653
    return-void
.end method

.method static a(Lvw;J)LvD;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2648
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, LvD;

    invoke-direct {v0, p0, p1, p2}, LvD;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()LvB;
    .registers 4

    .prologue
    .line 2666
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2667
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->f(J)J

    move-result-wide v1

    .line 2670
    iget-object v0, p0, LvD;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, LvE;->a(Lvw;J)LvE;

    move-result-object v0

    return-object v0
.end method

.method public a(LvI;)V
    .registers 6
    .parameter

    .prologue
    .line 2725
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2726
    invoke-static {p1}, LuZ;->a(LuY;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JJ)V

    .line 2727
    return-void
.end method

.method public a(LvU;)V
    .registers 6
    .parameter

    .prologue
    .line 2767
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2768
    invoke-static {p1}, LuZ;->a(LuY;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(JJ)V

    .line 2769
    return-void
.end method

.method public a(LwA;LwP;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 2781
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2782
    invoke-static {p1}, LuZ;->a(LuY;)J

    move-result-wide v2

    invoke-static {p2}, LuZ;->a(LuY;)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JJJ)V

    .line 2783
    return-void
.end method

.method public a(Lwe;)V
    .registers 6
    .parameter

    .prologue
    .line 2753
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2754
    invoke-static {p1}, LuZ;->a(LuY;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JJ)V

    .line 2755
    return-void
.end method

.method public a(Lwi;)V
    .registers 6
    .parameter

    .prologue
    .line 2739
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2740
    invoke-static {p1}, LuZ;->a(LuY;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JJ)V

    .line 2741
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 2683
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2684
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JZ)V

    .line 2685
    return-void
.end method

.method public a()Z
    .registers 5

    .prologue
    .line 2743
    iget-object v0, p0, LvD;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->c(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 2744
    iget-object v0, p0, LvD;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v1

    const/16 v3, 0x14

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->c(Lvw;Z)Z

    .line 2747
    iget-object v0, p0, LvD;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->d(Lvw;Z)Z

    .line 2749
    :cond_23
    iget-object v0, p0, LvD;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->d(Lvw;)Z

    move-result v0

    return v0
.end method

.method public b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 2711
    invoke-virtual {p0}, LvD;->a()J

    move-result-wide v0

    .line 2712
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JZ)V

    .line 2713
    return-void
.end method
