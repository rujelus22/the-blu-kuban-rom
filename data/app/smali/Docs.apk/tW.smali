.class LtW;
.super Landroid/widget/ArrayAdapter;
.source "OneDiscussionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lmz;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LAQ;

.field private final a:Landroid/view/View$OnLongClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 37
    sget v0, LsF;->discussion_list_element_one_discussion:I

    sget v1, LsD;->comment_container_collapsed_text:I

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, LtW;->a:LAQ;

    .line 39
    invoke-static {p2}, LsY;->a(Landroid/widget/AdapterView$OnItemClickListener;)Landroid/view/View$OnLongClickListener;

    move-result-object v0

    iput-object v0, p0, LtW;->a:Landroid/view/View$OnLongClickListener;

    .line 40
    return-void
.end method


# virtual methods
.method a(LAQ;)V
    .registers 2
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, LtW;->a:LAQ;

    .line 45
    return-void
.end method

.method a()Z
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, LtW;->a:LAQ;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 54
    if-nez p2, :cond_2a

    .line 55
    const-string v0, "OneDiscussionAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating Entry View "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LsF;->discussion_list_element_one_discussion:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 60
    :cond_2a
    sget v0, LsD;->comment_separator:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 61
    sget v1, LsD;->comment_container:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 63
    sget v1, LsD;->adapter_position_tag:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v1, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 64
    iget-object v1, p0, LtW;->a:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 66
    invoke-virtual {p0}, LtW;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_65

    .line 67
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 72
    :goto_4f
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LtW;->a:LAQ;

    iget-object v4, p0, LtW;->a:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0, p1}, LtW;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmz;

    move v7, p1

    invoke-static/range {v0 .. v7}, LsY;->a(Landroid/content/Context;LAQ;Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lmz;ZI)V

    .line 74
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 76
    return-object p2

    .line 69
    :cond_65
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4f
.end method
