.class LCx;
.super Lxq;
.source "TextStyle.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lxq",
        "<",
        "LCw;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LCw;


# direct methods
.method constructor <init>(LCw;Lth;[Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    iput-object p1, p0, LCx;->a:LCw;

    invoke-direct {p0, p2, p3, p4}, Lxq;-><init>(Lth;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a()LCt;
    .registers 2

    .prologue
    .line 172
    invoke-virtual {p0}, LCx;->a()LCw;

    move-result-object v0

    return-object v0
.end method

.method public a()LCw;
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, LCx;->a:LCw;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/util/Collection;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    invoke-super {p0, p1}, Lxq;->a(Landroid/content/Context;)Ljava/util/Collection;

    move-result-object v0

    .line 177
    iget-object v1, p0, LCx;->a:LCw;

    invoke-static {v1}, LCw;->a(LCw;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 178
    new-instance v1, Landroid/text/style/UnderlineSpan;

    invoke-direct {v1}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_14
    iget-object v1, p0, LCx;->a:LCw;

    invoke-static {v1}, LCw;->b(LCw;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 181
    new-instance v1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_24
    iget-object v1, p0, LCx;->a:LCw;

    invoke-virtual {v1}, LCw;->a()LGH;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 184
    return-object v0
.end method
