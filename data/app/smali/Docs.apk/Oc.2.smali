.class public final LOc;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;


# direct methods
.method public constructor <init>(LYD;)V
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 30
    iput-object p1, p0, LOc;->a:LYD;

    .line 31
    return-void
.end method

.method static synthetic a(LOc;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LOc;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 92
    const-class v0, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;

    new-instance v1, LOd;

    invoke-direct {v1, p0}, LOd;-><init>(LOc;)V

    invoke-virtual {p0, v0, v1}, LOc;->a(Ljava/lang/Class;Laou;)V

    .line 100
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 39
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->a:LPm;

    .line 45
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->a:LZM;

    .line 51
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:Ld;

    iget-object v0, v0, Ld;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->b:Landroid/os/Handler;

    .line 57
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->a:Ljava/lang/Class;

    .line 63
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->a:LeQ;

    .line 69
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->a:LNe;

    .line 75
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->a:LKS;

    .line 81
    iget-object v0, p0, LOc;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, LOc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/kixwebview/KixWebViewActivity;->b:Laoz;

    .line 87
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 104
    return-void
.end method
