.class final enum Lain;
.super Ljava/lang/Enum;
.source "AbstractIterator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lain;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lain;

.field private static final synthetic a:[Lain;

.field public static final enum b:Lain;

.field public static final enum c:Lain;

.field public static final enum d:Lain;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v0, Lain;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lain;->a:Lain;

    .line 75
    new-instance v0, Lain;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Lain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lain;->b:Lain;

    .line 78
    new-instance v0, Lain;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lain;->c:Lain;

    .line 81
    new-instance v0, Lain;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lain;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lain;->d:Lain;

    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [Lain;

    sget-object v1, Lain;->a:Lain;

    aput-object v1, v0, v2

    sget-object v1, Lain;->b:Lain;

    aput-object v1, v0, v3

    sget-object v1, Lain;->c:Lain;

    aput-object v1, v0, v4

    sget-object v1, Lain;->d:Lain;

    aput-object v1, v0, v5

    sput-object v0, Lain;->a:[Lain;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lain;
    .registers 2
    .parameter

    .prologue
    .line 70
    const-class v0, Lain;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lain;

    return-object v0
.end method

.method public static values()[Lain;
    .registers 1

    .prologue
    .line 70
    sget-object v0, Lain;->a:[Lain;

    invoke-virtual {v0}, [Lain;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lain;

    return-object v0
.end method
