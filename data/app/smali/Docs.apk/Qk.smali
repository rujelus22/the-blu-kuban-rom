.class public abstract LQk;
.super Ljava/lang/Object;
.source "AbstractPunchModelListener.java"

# interfaces
.implements LQS;


# static fields
.field protected static a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field protected static b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LdL;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Landroid/content/Context;

.field final a:LdL;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-object v0, LQk;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LdL;

    iput-object v0, p0, LQk;->a:LdL;

    .line 22
    sget-object v0, LQk;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LQk;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 26
    iget-object v0, p0, LQk;->a:LdL;

    iget-object v1, p0, LQk;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 28
    invoke-virtual {p0}, LQk;->b()V

    .line 29
    return-void
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, LQk;->a:LdL;

    iget-object v1, p0, LQk;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 68
    invoke-virtual {p0, p1}, LQk;->b(I)V

    .line 69
    return-void
.end method

.method protected b()V
    .registers 1

    .prologue
    .line 32
    return-void
.end method

.method protected b(I)V
    .registers 2
    .parameter

    .prologue
    .line 72
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 36
    iget-object v0, p0, LQk;->a:LdL;

    iget-object v1, p0, LQk;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 38
    invoke-virtual {p0}, LQk;->d()V

    .line 39
    return-void
.end method

.method protected d()V
    .registers 1

    .prologue
    .line 42
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 46
    iget-object v0, p0, LQk;->a:LdL;

    iget-object v1, p0, LQk;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 48
    invoke-virtual {p0}, LQk;->f()V

    .line 49
    return-void
.end method

.method protected f()V
    .registers 1

    .prologue
    .line 52
    return-void
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 56
    iget-object v0, p0, LQk;->a:LdL;

    iget-object v1, p0, LQk;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 58
    invoke-virtual {p0}, LQk;->h()V

    .line 59
    return-void
.end method

.method protected h()V
    .registers 1

    .prologue
    .line 62
    return-void
.end method

.method public final i()V
    .registers 3

    .prologue
    .line 76
    iget-object v0, p0, LQk;->a:LdL;

    iget-object v1, p0, LQk;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 78
    invoke-virtual {p0}, LQk;->j()V

    .line 79
    return-void
.end method

.method protected j()V
    .registers 1

    .prologue
    .line 82
    return-void
.end method
