.class Lmi;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LlW;

.field final synthetic a:Lmb;

.field final synthetic a:Lmq;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lmb;Ljava/lang/String;Ljava/lang/String;Lmq;LlW;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 643
    iput-object p1, p0, Lmi;->a:Lmb;

    iput-object p2, p0, Lmi;->a:Ljava/lang/String;

    iput-object p3, p0, Lmi;->b:Ljava/lang/String;

    iput-object p4, p0, Lmi;->a:Lmq;

    iput-object p5, p0, Lmi;->a:LlW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 646
    new-instance v0, Lcom/google/api/services/discussions/model/Post;

    invoke-direct {v0}, Lcom/google/api/services/discussions/model/Post;-><init>()V

    .line 647
    new-instance v1, Lcom/google/api/services/discussions/model/MimedcontentJson;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/MimedcontentJson;-><init>()V

    iget-object v2, p0, Lmi;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v1

    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/MimedcontentJson;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v1

    .line 649
    new-instance v2, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-direct {v2}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->b(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    move-result-object v1

    const-string v2, "post"

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    move-result-object v1

    .line 651
    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Lcom/google/api/services/discussions/model/Post$DiscussionsObject;)Lcom/google/api/services/discussions/model/Post;

    .line 652
    const-string v1, "discussions#post"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 653
    const-string v1, "post"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 654
    new-instance v1, Lcom/google/api/services/discussions/model/Post$Target;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/Post$Target;-><init>()V

    iget-object v2, p0, Lmi;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Post$Target;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post$Target;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Lcom/google/api/services/discussions/model/Post$Target;)Lcom/google/api/services/discussions/model/Post;

    .line 655
    iget-object v1, p0, Lmi;->a:Lmq;

    invoke-virtual {v1}, Lmq;->d()Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 657
    const-string v1, "reopen"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->c(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 661
    :cond_4d
    :try_start_4d
    iget-object v1, p0, Lmi;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)Lcom/google/api/services/discussions/Discussions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/services/discussions/Discussions;->a()Lcom/google/api/services/discussions/Discussions$Posts;

    move-result-object v1

    iget-object v2, p0, Lmi;->a:Lmb;

    invoke-static {v2}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lmi;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/api/services/discussions/Discussions$Posts;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/discussions/model/Post;)Lcom/google/api/services/discussions/Discussions$Posts$Insert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->a()Lcom/google/api/services/discussions/model/Post;
    :try_end_66
    .catch LadQ; {:try_start_4d .. :try_end_66} :catch_c7
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_66} :catch_e6

    move-result-object v0

    .line 675
    iget-object v1, p0, Lmi;->a:Lmq;

    invoke-static {v1, v0}, Lmq;->a(Lmq;Lcom/google/api/services/discussions/model/Post;)Landroid/util/Pair;

    move-result-object v0

    .line 676
    iget-object v1, p0, Lmi;->a:Lmb;

    invoke-static {v1}, Lmb;->c(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 677
    :try_start_74
    iget-object v2, p0, Lmi;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v2

    iget-object v3, p0, Lmi;->a:Lmq;

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8d

    .line 678
    iget-object v2, p0, Lmi;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v2

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 680
    :cond_8d
    iget-object v2, p0, Lmi;->a:Lmb;

    invoke-static {v2}, Lmb;->c(Lmb;)V

    .line 681
    monitor-exit v1
    :try_end_93
    .catchall {:try_start_74 .. :try_end_93} :catchall_100

    .line 682
    iget-object v1, p0, Lmi;->a:Lmb;

    invoke-static {v1}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 683
    :try_start_9a
    iget-object v2, p0, Lmi;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Lmo;

    move-result-object v2

    sget-object v3, Lmo;->b:Lmo;

    if-ne v2, v3, :cond_ab

    .line 685
    iget-object v2, p0, Lmi;->a:Lmb;

    sget-object v3, Lmo;->c:Lmo;

    invoke-static {v2, v3}, Lmb;->a(Lmb;Lmo;)Lmo;

    .line 687
    :cond_ab
    monitor-exit v1
    :try_end_ac
    .catchall {:try_start_9a .. :try_end_ac} :catchall_103

    .line 688
    iget-object v1, p0, Lmi;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionReplyOk"

    iget-object v4, p0, Lmi;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    iget-object v1, p0, Lmi;->a:LlW;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Object;)V

    .line 691
    :goto_c6
    return-void

    .line 662
    :catch_c7
    move-exception v0

    .line 663
    iget-object v1, p0, Lmi;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionReplyError"

    iget-object v4, p0, Lmi;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    iget-object v1, p0, Lmi;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    .line 666
    iget-object v1, p0, Lmi;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_c6

    .line 668
    :catch_e6
    move-exception v0

    .line 669
    iget-object v1, p0, Lmi;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionReplyError"

    iget-object v4, p0, Lmi;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-object v1, p0, Lmi;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_c6

    .line 681
    :catchall_100
    move-exception v0

    :try_start_101
    monitor-exit v1
    :try_end_102
    .catchall {:try_start_101 .. :try_end_102} :catchall_100

    throw v0

    .line 687
    :catchall_103
    move-exception v0

    :try_start_104
    monitor-exit v1
    :try_end_105
    .catchall {:try_start_104 .. :try_end_105} :catchall_103

    throw v0
.end method
