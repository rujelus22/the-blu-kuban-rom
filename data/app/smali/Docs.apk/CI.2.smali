.class public LCI;
.super LCC;
.source "NativeLinearRenderer.java"

# interfaces
.implements LwC;


# instance fields
.field private final a:Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

.field private final a:Lzs;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lzs;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, LCC;-><init>(Ljava/lang/String;)V

    .line 38
    iput-object p2, p0, LCI;->a:Lzs;

    .line 39
    new-instance v0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

    invoke-direct {v0, p3}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LCI;->a:Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

    .line 40
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, LCI;->a:Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

    return-object v0
.end method

.method public bridge synthetic a()LzU;
    .registers 2

    .prologue
    .line 17
    invoke-virtual {p0}, LCI;->a()Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, LCI;->a:Lzs;

    invoke-interface {v0, p1}, Lzs;->a(Ljava/lang/String;)LCJ;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_38

    .line 46
    iget-object v1, p0, LCI;->a:Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

    invoke-interface {v0}, LCJ;->a()LzU;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a(LzU;I)V

    .line 47
    const-string v0, "NativeLinearRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Child "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " added to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LCI;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :goto_37
    return-void

    .line 49
    :cond_38
    const-string v0, "NativeLinearRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to find view with ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_37
.end method

.method public c(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, LCI;->a:Lzs;

    invoke-interface {v0, p1}, Lzs;->a(Ljava/lang/String;)LCJ;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_38

    .line 57
    iget-object v1, p0, LCI;->a:Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

    invoke-interface {v0}, LCJ;->a()LzU;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a(LzU;)V

    .line 58
    const-string v0, "NativeLinearRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Child "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " removed from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LCI;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_37
    return-void

    .line 60
    :cond_38
    const-string v0, "NativeLinearRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to find view with ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_37
.end method
