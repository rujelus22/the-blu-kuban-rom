.class public LpX;
.super LoX;
.source "LocalOpenerSelector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LoX",
        "<",
        "LkP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Laoz;Laoz;Laoz;Laoz;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/ZippedKixDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/ZippedTrixDocumentOpener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {p1, p2, p3, p4}, LpX;->a(Laoz;Laoz;Laoz;Laoz;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, LoX;-><init>(Ljava/util/Map;)V

    .line 28
    return-void
.end method

.method static a(Laoz;Laoz;Laoz;Laoz;)Ljava/util/Map;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/ZippedKixDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/ZippedTrixDocumentOpener;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "LkP;",
            "Laoz",
            "<+",
            "LoZ;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 40
    sget-object v1, LkP;->j:LkP;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v1, LkP;->k:LkP;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v1, LkP;->a:LkP;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v1, LkP;->d:LkP;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-object v0
.end method
