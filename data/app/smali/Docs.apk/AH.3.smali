.class public LAH;
.super LAL;
.source "Section.java"


# instance fields
.field private final a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LDb;LAb;Lzd;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "LAb;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, LAL;-><init>(LDb;LAb;Lzd;Lxu;)V

    .line 30
    new-instance v0, LDA;

    invoke-direct {v0}, LDA;-><init>()V

    iput-object v0, p0, LAH;->a:LDA;

    .line 31
    iget-object v0, p0, LAH;->a:LDA;

    invoke-virtual {v0, p0}, LDA;->a(Ljava/lang/Object;)V

    .line 32
    return-void
.end method


# virtual methods
.method public a()LDA;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, LAH;->a:LDA;

    return-object v0
.end method

.method public bridge synthetic a()LDG;
    .registers 2

    .prologue
    .line 18
    invoke-virtual {p0}, LAH;->a()LDA;

    move-result-object v0

    return-object v0
.end method

.method public a(Lwj;Lza;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 42
    iget-object v0, p0, LAH;->a:LDb;

    invoke-virtual {v0}, LDb;->e()I

    move-result v0

    if-nez v0, :cond_1b

    .line 44
    iget-object v0, p0, LAH;->a:LDb;

    invoke-virtual {v0, v1}, LDb;->a(I)V

    .line 45
    iget-object v0, p0, LAH;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    invoke-interface {p2, v0, v1}, Lza;->a(II)V

    .line 50
    :goto_17
    invoke-super {p0, p1, p2, p3}, LAL;->a(Lwj;Lza;I)V

    .line 51
    return-void

    .line 47
    :cond_1b
    iget-object v0, p0, LAH;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lza;->a(II)V

    goto :goto_17
.end method

.method public a(Lza;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 55
    invoke-super {p0, p1}, LAL;->a(Lza;)V

    .line 56
    iget-object v0, p0, LAH;->a:LDb;

    invoke-virtual {v0}, LDb;->e()I

    move-result v0

    if-lez v0, :cond_1a

    .line 57
    iget-object v0, p0, LAH;->a:LDb;

    invoke-virtual {v0, v1}, LDb;->a(I)V

    .line 59
    iget-object v0, p0, LAH;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    invoke-interface {p1, v0, v1}, Lza;->b(II)V

    .line 61
    :cond_1a
    return-void
.end method
