.class public Ltz;
.super Ljava/lang/Object;
.source "AllDiscussionsViewManager.java"

# interfaces
.implements Lty;


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private final a:Landroid/widget/AdapterView$OnItemClickListener;

.field private a:Landroid/widget/ListView;

.field private final a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

.field private final a:Ltv;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;)V
    .registers 5
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, LtA;

    invoke-direct {v0, p0}, LtA;-><init>(Ltz;)V

    iput-object v0, p0, Ltz;->a:Landroid/view/View$OnClickListener;

    .line 46
    new-instance v0, LtB;

    invoke-direct {v0, p0}, LtB;-><init>(Ltz;)V

    iput-object v0, p0, Ltz;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 59
    iput-object p1, p0, Ltz;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    .line 60
    new-instance v0, Ltv;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltz;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-direct {v0, v1, v2}, Ltv;-><init>(Landroid/content/Context;Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v0, p0, Ltz;->a:Ltv;

    .line 62
    return-void
.end method

.method static synthetic a(Ltz;)Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;
    .registers 2
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Ltz;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    return-object v0
.end method

.method static synthetic a(Ltz;)Ltv;
    .registers 2
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Ltz;->a:Ltv;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 5
    .parameter

    .prologue
    .line 66
    sget v0, LsF;->discussion_fragment_all_discussions:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 68
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    .line 69
    iget-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    iget-object v2, p0, Ltz;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 71
    sget v0, LsD;->discussion_loading_spinner:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltz;->a:Landroid/view/View;

    .line 72
    sget v0, LsD;->discussion_no_comments:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltz;->b:Landroid/view/View;

    .line 74
    sget v0, LsD;->action_new_comment:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 75
    iget-object v2, p0, Ltz;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    sget v0, LsD;->action_all_close:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 78
    iget-object v2, p0, Ltz;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-object v1
.end method

.method public a(Ljava/util/SortedSet;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    if-eqz v0, :cond_e

    if-eqz p1, :cond_e

    iget-object v0, p0, Ltz;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->e()Z

    move-result v0

    if-nez v0, :cond_f

    .line 107
    :cond_e
    :goto_e
    return-void

    .line 90
    :cond_f
    iget-object v0, p0, Ltz;->a:Ltv;

    invoke-virtual {v0}, Ltv;->clear()V

    .line 91
    sget-object v0, Lmz;->a:Lagv;

    invoke-static {p1, v0}, LajB;->a(Ljava/lang/Iterable;Lagv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 93
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 94
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-interface {v0}, Lmz;->a()Ljava/util/Collection;

    move-result-object v0

    sget-object v3, Lmz;->a:Lagv;

    invoke-static {v0, v3}, LajB;->a(Ljava/lang/Iterable;Lagv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v2, v0}, LajB;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 98
    iget-object v0, p0, Ltz;->a:Ltv;

    invoke-virtual {v0, v2}, Ltv;->add(Ljava/lang/Object;)V

    goto :goto_1e

    .line 100
    :cond_45
    iget-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_54

    .line 101
    iget-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    iget-object v1, p0, Ltz;->a:Ltv;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 103
    :cond_54
    iget-object v0, p0, Ltz;->a:Ltv;

    invoke-virtual {v0}, Ltv;->a()Z

    move-result v0

    if-nez v0, :cond_67

    .line 104
    iget-object v0, p0, Ltz;->a:Ltv;

    iget-object v1, p0, Ltz;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a()LAQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv;->a(LAQ;)V

    .line 106
    :cond_67
    iget-object v0, p0, Ltz;->a:Ltv;

    invoke-virtual {v0}, Ltv;->notifyDataSetChanged()V

    goto :goto_e
.end method

.method public a(Ltx;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 111
    sget-object v0, LtC;->a:[I

    invoke-virtual {p1}, Ltx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3e

    .line 124
    iget-object v0, p0, Ltz;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Ltz;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 128
    :goto_1d
    return-void

    .line 113
    :pswitch_1e
    iget-object v0, p0, Ltz;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Ltz;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1d

    .line 118
    :pswitch_2e
    iget-object v0, p0, Ltz;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Ltz;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Ltz;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1d

    .line 111
    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_2e
    .end packed-switch
.end method
