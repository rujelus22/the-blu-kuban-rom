.class public Ltd;
.super Ljava/lang/Object;
.source "DiscussionSpec.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 51
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    if-nez p1, :cond_7

    if-eqz p2, :cond_12

    :cond_7
    const/4 v0, 0x1

    :goto_8
    invoke-static {v0}, Lagu;->a(Z)V

    .line 39
    iput-object p1, p0, Ltd;->a:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Ltd;->b:Ljava/lang/String;

    .line 41
    iput-boolean p3, p0, Ltd;->a:Z

    .line 42
    return-void

    .line 38
    :cond_12
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public constructor <init>(Lmz;)V
    .registers 5
    .parameter

    .prologue
    .line 61
    invoke-interface {p1}, Lmz;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lmz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lmz;->d()Z

    move-result v0

    if-nez v0, :cond_13

    const/4 v0, 0x1

    :goto_f
    invoke-direct {p0, v1, v2, v0}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 62
    return-void

    .line 61
    :cond_13
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public static a(Landroid/os/Bundle;)Ltd;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 130
    if-nez p0, :cond_4

    .line 151
    :cond_3
    :goto_3
    return-object v0

    .line 136
    :cond_4
    if-eqz p0, :cond_5f

    const-string v1, "bundleCommentId"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5f

    .line 137
    const-string v1, "bundleCommentId"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 140
    :goto_15
    if-eqz p0, :cond_5d

    const-string v1, "bundleAnchorId"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5d

    .line 141
    const-string v1, "bundleAnchorId"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 143
    :goto_26
    const/4 v1, 0x0

    .line 144
    if-eqz p0, :cond_37

    const-string v4, "bundleIsOpened"

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_37

    .line 145
    const-string v1, "bundleIsOpened"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 147
    :cond_37
    if-nez v3, :cond_3b

    if-eqz v2, :cond_3

    .line 148
    :cond_3b
    new-instance v0, Ltd;

    invoke-direct {v0, v3, v2, v1}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 149
    const-string v1, "DiscussionSpec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFromBundle / discussion = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ltd;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5d
    move-object v2, v0

    goto :goto_26

    :cond_5f
    move-object v3, v0

    goto :goto_15
.end method

.method public static a(Landroid/os/Bundle;Ltd;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 113
    if-eqz p1, :cond_4

    if-nez p0, :cond_5

    .line 120
    :cond_4
    :goto_4
    return-void

    .line 116
    :cond_5
    const-string v0, "DiscussionSpec"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addToBundle / discussion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v0, "bundleCommentId"

    iget-object v1, p1, Ltd;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v0, "bundleAnchorId"

    iget-object v1, p1, Ltd;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v0, "bundleIsOpened"

    iget-boolean v1, p1, Ltd;->a:Z

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Ltd;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 73
    iget-boolean v0, p0, Ltd;->a:Z

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Ltd;->a:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 85
    iget-object v0, p0, Ltd;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 87
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, Ltd;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Ltd;->b:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 99
    iget-object v0, p0, Ltd;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 101
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 162
    instance-of v1, p1, Ltd;

    if-eqz v1, :cond_22

    .line 163
    check-cast p1, Ltd;

    .line 164
    iget-object v1, p0, Ltd;->a:Ljava/lang/String;

    iget-object v2, p1, Ltd;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    iget-object v1, p0, Ltd;->b:Ljava/lang/String;

    iget-object v2, p1, Ltd;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    iget-boolean v1, p1, Ltd;->a:Z

    iget-boolean v2, p0, Ltd;->a:Z

    if-ne v1, v2, :cond_22

    const/4 v0, 0x1

    .line 169
    :cond_22
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 174
    iget-object v0, p0, Ltd;->a:Ljava/lang/String;

    if-eqz v0, :cond_2d

    iget-object v0, p0, Ltd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 175
    :goto_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    mul-int/lit8 v0, v0, 0x25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ltd;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_28

    iget-object v0, p0, Ltd;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 176
    :cond_28
    iget-boolean v0, p0, Ltd;->a:Z

    if-eqz v0, :cond_2f

    :goto_2c
    return v1

    :cond_2d
    move v0, v1

    .line 174
    goto :goto_b

    .line 176
    :cond_2f
    mul-int/lit8 v1, v1, -0x1

    goto :goto_2c
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiscussionSpec: anchorId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / commentId = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / isOpened = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ltd;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
