.class public abstract LYY;
.super Ljava/lang/Object;
.source "GellyInjectorStoreBase.java"


# instance fields
.field private final a:LYL;


# direct methods
.method public constructor <init>(LYL;)V
    .registers 2
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, LYY;->a:LYL;

    .line 83
    return-void
.end method

.method protected static a(Laop;Ljava/lang/Class;)LZb;
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LZb",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, LZb;

    invoke-direct {v0, p0, p1}, LZb;-><init>(Laop;Ljava/lang/Class;)V

    return-object v0
.end method

.method protected static a(Laoz;)Laoz;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laoz",
            "<+",
            "Laoz",
            "<+TT;>;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, LZa;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LZa;-><init>(Laoz;LYZ;)V

    return-object v0
.end method

.method protected static final aN(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(Laop;LZb;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "LZb",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, LYY;->a:LYL;

    invoke-virtual {v0, p1, p2}, LYL;->a(Laop;LZb;)V

    .line 96
    return-void
.end method

.method protected final a(Ljava/lang/Class;Laou;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Laou",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, LYY;->a:LYL;

    invoke-virtual {v0, p1, p2}, LYL;->a(Ljava/lang/Class;Laou;)V

    .line 92
    return-void
.end method

.method protected abstract b()V
.end method
