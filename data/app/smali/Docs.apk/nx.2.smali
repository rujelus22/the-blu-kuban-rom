.class public final Lnx;
.super Ljava/lang/Object;
.source "CreateEntryDialogFragment.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;
    .registers 9
    .parameter

    .prologue
    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LkP;

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 159
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;-><init>(LkP;IIIII)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;
    .registers 3
    .parameter

    .prologue
    .line 169
    new-array v0, p1, [Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lnx;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lnx;->a(I)[Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    move-result-object v0

    return-object v0
.end method
