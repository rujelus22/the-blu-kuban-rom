.class public LsK;
.super Ljava/lang/Object;
.source "CollaboratorController.java"

# interfaces
.implements LHa;
.implements LyF;
.implements LyQ;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field private final a:Landroid/view/LayoutInflater;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/ImageView;

.field private final a:Landroid/widget/LinearLayout;

.field private final a:Landroid/widget/TextView;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LsN;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/LinearLayout;Landroid/widget/ImageView;LsN;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LsK;->a:Ljava/util/Map;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, LsK;->a:Z

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LsK;->a:Landroid/os/Handler;

    .line 58
    iput-object p1, p0, LsK;->a:Landroid/content/Context;

    .line 59
    iput-object p2, p0, LsK;->a:Landroid/view/View;

    .line 60
    iput-object p4, p0, LsK;->a:Landroid/widget/LinearLayout;

    .line 61
    iput-object p3, p0, LsK;->a:Landroid/widget/TextView;

    .line 62
    iput-object p5, p0, LsK;->a:Landroid/widget/ImageView;

    .line 63
    iput-object p6, p0, LsK;->a:LsN;

    .line 65
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LsK;->a:Landroid/view/LayoutInflater;

    .line 66
    new-instance v0, LsL;

    invoke-direct {v0, p0, p4}, LsL;-><init>(LsK;Landroid/widget/LinearLayout;)V

    .line 74
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    invoke-virtual {p5, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method static synthetic a(LsK;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LsK;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(LsK;)Landroid/widget/LinearLayout;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LsK;->a:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic a(LsK;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LsK;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 129
    iget-object v0, p0, LsK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v6, :cond_29

    .line 130
    iget-object v1, p0, LsK;->a:Landroid/content/Context;

    sget v2, LsH;->one_collaborator_viewing:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v0, p0, LsK;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v4, LsD;->collaborator_display_name:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 135
    :goto_28
    return-object v0

    :cond_29
    iget-object v1, p0, LsK;->a:Landroid/content/Context;

    sget v2, LsH;->multiple_collaborator_viewing:I

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p0, LsK;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v4, LsD;->collaborator_display_name:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v3, v5

    iget-object v0, p0, LsK;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_28
.end method

.method static synthetic a(LsK;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, LsK;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LsK;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LsK;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(LsK;)LsN;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, LsK;->a:LsN;

    return-object v0
.end method

.method static synthetic a(LsK;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, LsK;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 104
    iget-object v1, p0, LsK;->a:Landroid/widget/ImageView;

    if-eqz p1, :cond_a

    sget v0, LsC;->ic_arrow_small_up:I

    :goto_6
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 106
    return-void

    .line 104
    :cond_a
    sget v0, LsC;->ic_arrow_small_down:I

    goto :goto_6
.end method

.method static synthetic a(LsK;)Z
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, LsK;->a:Z

    return v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 109
    iget-object v0, p0, LsK;->a:Landroid/os/Handler;

    new-instance v1, LsM;

    invoke-direct {v1, p0}, LsM;-><init>(LsK;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 126
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 146
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, LsK;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 147
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148
    invoke-virtual {p0, v0}, LsK;->a(Ljava/lang/String;)V

    goto :goto_f

    .line 150
    :cond_1f
    return-void
.end method

.method public a(LJD;)V
    .registers 5
    .parameter

    .prologue
    .line 170
    invoke-virtual {p1}, LJD;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LJD;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LJD;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LsK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, LsK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 96
    if-eqz v0, :cond_17

    .line 97
    iget-object v1, p0, LsK;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 98
    iget-object v0, p0, LsK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-direct {p0}, LsK;->b()V

    .line 101
    :cond_17
    return-void
.end method

.method public a(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 160
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 155
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, LsK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3e

    .line 81
    iget-object v0, p0, LsK;->a:Landroid/view/LayoutInflater;

    sget v1, LsF;->collaborator_list_item:I

    iget-object v2, p0, LsK;->a:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 83
    sget v0, LsD;->collaborator_display_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    sget v0, LsD;->collaborator_color_indicator:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 87
    iget-object v0, p0, LsK;->a:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v0, p0, LsK;->a:Landroid/widget/LinearLayout;

    iget-object v2, p0, LsK;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 89
    invoke-direct {p0}, LsK;->b()V

    .line 91
    :cond_3e
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 164
    iput-boolean p1, p0, LsK;->a:Z

    .line 165
    invoke-direct {p0}, LsK;->b()V

    .line 166
    return-void
.end method

.method public b(LJD;)V
    .registers 2
    .parameter

    .prologue
    .line 176
    return-void
.end method

.method public c(LJD;)V
    .registers 3
    .parameter

    .prologue
    .line 180
    invoke-virtual {p1}, LJD;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LsK;->a(Ljava/lang/String;)V

    .line 181
    return-void
.end method
