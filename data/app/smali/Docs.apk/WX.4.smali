.class public LWX;
.super Ljava/lang/Object;
.source "ContentSyncTasks.java"


# instance fields
.field private final a:I

.field private final a:J

.field private final a:LWW;

.field private a:Z

.field private final b:J

.field private b:LWW;


# direct methods
.method constructor <init>(LWW;JJI)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget-object v0, LWW;->f:LWW;

    iput-object v0, p0, LWX;->b:LWW;

    .line 49
    iput-object p1, p0, LWX;->a:LWW;

    .line 50
    iput-wide p2, p0, LWX;->a:J

    .line 51
    iput-wide p4, p0, LWX;->b:J

    .line 52
    iput p6, p0, LWX;->a:I

    .line 53
    return-void
.end method

.method public static a()LWX;
    .registers 7

    .prologue
    const-wide/16 v2, 0x0

    .line 56
    new-instance v0, LWX;

    sget-object v1, LWW;->a:LWW;

    const/4 v6, 0x0

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, LWX;-><init>(LWW;JJI)V

    return-object v0
.end method

.method public static a(I)LWX;
    .registers 8
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 68
    new-instance v0, LWX;

    sget-object v1, LWW;->d:LWW;

    move-wide v4, v2

    move v6, p0

    invoke-direct/range {v0 .. v6}, LWX;-><init>(LWW;JJI)V

    return-object v0
.end method

.method public static a(JJ)LWX;
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 60
    new-instance v0, LWX;

    sget-object v1, LWW;->b:LWW;

    const/4 v6, 0x0

    move-wide v2, p0

    move-wide v4, p2

    invoke-direct/range {v0 .. v6}, LWX;-><init>(LWW;JJI)V

    return-object v0
.end method

.method static synthetic a(LWX;LWX;)LWX;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-static {p0, p1}, LWX;->b(LWX;LWX;)LWX;

    move-result-object v0

    return-object v0
.end method

.method public static b()LWX;
    .registers 7

    .prologue
    const-wide/16 v2, 0x0

    .line 64
    new-instance v0, LWX;

    sget-object v1, LWW;->c:LWW;

    const/4 v6, 0x0

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, LWX;-><init>(LWW;JJI)V

    return-object v0
.end method

.method private static b(LWX;LWX;)LWX;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 81
    invoke-virtual {p1}, LWX;->a()LWW;

    move-result-object v1

    .line 83
    sget-object v0, LWW;->c:LWW;

    if-eq v1, v0, :cond_10

    sget-object v0, LWW;->e:LWW;

    if-eq v1, v0, :cond_10

    sget-object v0, LWW;->d:LWW;

    if-ne v1, v0, :cond_22

    .line 85
    :cond_10
    new-instance v0, LWX;

    invoke-virtual {p0}, LWX;->a()J

    move-result-wide v2

    invoke-virtual {p0}, LWX;->b()J

    move-result-wide v4

    invoke-virtual {p1}, LWX;->a()I

    move-result v6

    invoke-direct/range {v0 .. v6}, LWX;-><init>(LWW;JJI)V

    move-object p1, v0

    .line 88
    :cond_22
    return-object p1
.end method

.method public static c()LWX;
    .registers 7

    .prologue
    const-wide/16 v2, 0x0

    .line 72
    new-instance v0, LWX;

    sget-object v1, LWW;->e:LWW;

    const/4 v6, 0x0

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, LWX;-><init>(LWW;JJI)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 104
    iget v0, p0, LWX;->a:I

    return v0
.end method

.method public a()J
    .registers 3

    .prologue
    .line 96
    iget-wide v0, p0, LWX;->a:J

    return-wide v0
.end method

.method public a()LWW;
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, LWX;->a:LWW;

    return-object v0
.end method

.method public a(LWW;)V
    .registers 2
    .parameter

    .prologue
    .line 116
    iput-object p1, p0, LWX;->b:LWW;

    .line 117
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 120
    iput-boolean p1, p0, LWX;->a:Z

    .line 121
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 112
    iget-boolean v0, p0, LWX;->a:Z

    return v0
.end method

.method public b()J
    .registers 3

    .prologue
    .line 100
    iget-wide v0, p0, LWX;->b:J

    return-wide v0
.end method

.method public b()LWW;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, LWX;->b:LWW;

    return-object v0
.end method
