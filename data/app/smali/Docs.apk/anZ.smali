.class public final enum LanZ;
.super Ljava/lang/Enum;
.source "JsonToken.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LanZ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LanZ;

.field private static final synthetic a:[LanZ;

.field public static final enum b:LanZ;

.field public static final enum c:LanZ;

.field public static final enum d:LanZ;

.field public static final enum e:LanZ;

.field public static final enum f:LanZ;

.field public static final enum g:LanZ;

.field public static final enum h:LanZ;

.field public static final enum i:LanZ;

.field public static final enum j:LanZ;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, LanZ;

    const-string v1, "BEGIN_ARRAY"

    invoke-direct {v0, v1, v3}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->a:LanZ;

    .line 37
    new-instance v0, LanZ;

    const-string v1, "END_ARRAY"

    invoke-direct {v0, v1, v4}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->b:LanZ;

    .line 43
    new-instance v0, LanZ;

    const-string v1, "BEGIN_OBJECT"

    invoke-direct {v0, v1, v5}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->c:LanZ;

    .line 49
    new-instance v0, LanZ;

    const-string v1, "END_OBJECT"

    invoke-direct {v0, v1, v6}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->d:LanZ;

    .line 56
    new-instance v0, LanZ;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v7}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->e:LanZ;

    .line 61
    new-instance v0, LanZ;

    const-string v1, "STRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->f:LanZ;

    .line 67
    new-instance v0, LanZ;

    const-string v1, "NUMBER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->g:LanZ;

    .line 72
    new-instance v0, LanZ;

    const-string v1, "BOOLEAN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->h:LanZ;

    .line 77
    new-instance v0, LanZ;

    const-string v1, "NULL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->i:LanZ;

    .line 84
    new-instance v0, LanZ;

    const-string v1, "END_DOCUMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LanZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LanZ;->j:LanZ;

    .line 25
    const/16 v0, 0xa

    new-array v0, v0, [LanZ;

    sget-object v1, LanZ;->a:LanZ;

    aput-object v1, v0, v3

    sget-object v1, LanZ;->b:LanZ;

    aput-object v1, v0, v4

    sget-object v1, LanZ;->c:LanZ;

    aput-object v1, v0, v5

    sget-object v1, LanZ;->d:LanZ;

    aput-object v1, v0, v6

    sget-object v1, LanZ;->e:LanZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LanZ;->f:LanZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LanZ;->g:LanZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LanZ;->h:LanZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LanZ;->i:LanZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LanZ;->j:LanZ;

    aput-object v2, v0, v1

    sput-object v0, LanZ;->a:[LanZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LanZ;
    .registers 2
    .parameter

    .prologue
    .line 25
    const-class v0, LanZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LanZ;

    return-object v0
.end method

.method public static values()[LanZ;
    .registers 1

    .prologue
    .line 25
    sget-object v0, LanZ;->a:[LanZ;

    invoke-virtual {v0}, [LanZ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LanZ;

    return-object v0
.end method
