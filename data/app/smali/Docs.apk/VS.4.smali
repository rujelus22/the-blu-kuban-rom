.class public LVS;
.super LasT;
.source "AclEntry.java"


# instance fields
.field private a:LeI;

.field private a:LeK;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0}, LasT;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, LVS;->a:Ljava/lang/String;

    .line 30
    sget-object v0, LeI;->f:LeI;

    iput-object v0, p0, LVS;->a:LeI;

    .line 31
    sget-object v0, LeK;->e:LeK;

    iput-object v0, p0, LVS;->a:LeK;

    .line 32
    const-class v0, LeD;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LVS;->a:Ljava/util/Set;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, LVS;->a:Z

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, LasT;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, LVS;->a:Ljava/lang/String;

    .line 30
    sget-object v0, LeI;->f:LeI;

    iput-object v0, p0, LVS;->a:LeI;

    .line 31
    sget-object v0, LeK;->e:LeK;

    iput-object v0, p0, LVS;->a:LeK;

    .line 32
    const-class v0, LeD;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LVS;->a:Ljava/util/Set;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, LVS;->a:Z

    .line 40
    invoke-static {p2, p1}, LeM;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LVS;->q(Ljava/lang/String;)V

    .line 41
    const-string v0, "http://schemas.google.com/acl/2007#accessRule"

    invoke-virtual {p0, v0}, LVS;->l(Ljava/lang/String;)V

    .line 42
    const-string v0, "http://schemas.google.com/g/2005#kind"

    invoke-virtual {p0, v0}, LVS;->m(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public static a(Ljava/lang/String;LeB;)LVS;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 140
    new-instance v1, LVS;

    invoke-virtual {p1}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, LVS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LVS;->a(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p1}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LeM;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LVS;->s(Ljava/lang/String;)V

    .line 143
    invoke-virtual {p1}, LeB;->a()LeI;

    move-result-object v0

    sget-object v2, LeI;->e:LeI;

    if-eq v0, v2, :cond_3d

    .line 144
    invoke-virtual {p1}, LeB;->a()LeI;

    move-result-object v0

    invoke-virtual {v0}, LeI;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LVS;->b(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p1}, LeB;->a()LeK;

    move-result-object v0

    invoke-virtual {v0}, LeK;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LVS;->c(Ljava/lang/String;)V

    .line 147
    :cond_3d
    invoke-virtual {p1}, LeB;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_45
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeD;

    .line 148
    invoke-virtual {v1, v0}, LVS;->a(LeD;)V

    goto :goto_45

    .line 150
    :cond_55
    return-object v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)LeB;
    .registers 4
    .parameter

    .prologue
    .line 128
    new-instance v0, LeF;

    invoke-direct {v0}, LeF;-><init>()V

    invoke-virtual {v0, p1}, LeF;->a(Ljava/lang/String;)LeF;

    move-result-object v0

    iget-object v1, p0, LVS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LeF;->b(Ljava/lang/String;)LeF;

    move-result-object v0

    iget-object v1, p0, LVS;->a:LeK;

    invoke-virtual {v0, v1}, LeF;->a(LeK;)LeF;

    move-result-object v0

    iget-object v1, p0, LVS;->a:LeI;

    invoke-virtual {v0, v1}, LeF;->a(LeI;)LeF;

    move-result-object v0

    iget-object v1, p0, LVS;->a:Ljava/util/Set;

    invoke-virtual {v0, v1}, LeF;->a(Ljava/util/Set;)LeF;

    move-result-object v0

    iget-boolean v1, p0, LVS;->a:Z

    invoke-virtual {v0, v1}, LeF;->a(Z)LeF;

    move-result-object v0

    invoke-virtual {v0}, LeF;->a()LeB;

    move-result-object v0

    return-object v0
.end method

.method public a()LeI;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, LVS;->a:LeI;

    return-object v0
.end method

.method public a()LeK;
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, LVS;->a:LeK;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, LVS;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, LVS;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(LeD;)V
    .registers 3
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, LVS;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 94
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, LVS;->a:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 110
    iget-boolean v0, p0, LVS;->a:Z

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 80
    :try_start_0
    invoke-static {p1}, LeI;->a(Ljava/lang/String;)LeI;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_3} :catch_c

    move-result-object v0

    .line 85
    :goto_4
    sget-object v1, LeI;->d:LeI;

    if-ne v0, v1, :cond_28

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, LVS;->a:Z

    .line 90
    :cond_b
    :goto_b
    return-void

    .line 81
    :catch_c
    move-exception v0

    .line 82
    const-string v0, "AclEntry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown role: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    sget-object v0, LeI;->f:LeI;

    goto :goto_4

    .line 87
    :cond_28
    sget-object v1, LeI;->f:LeI;

    if-eq v0, v1, :cond_b

    .line 88
    iput-object v0, p0, LVS;->a:LeI;

    goto :goto_b
.end method

.method public c(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 102
    :try_start_0
    invoke-static {p1}, LeK;->a(Ljava/lang/String;)LeK;

    move-result-object v0

    iput-object v0, p0, LVS;->a:LeK;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_6} :catch_7

    .line 107
    :goto_6
    return-void

    .line 103
    :catch_7
    move-exception v0

    .line 104
    const-string v0, "AclEntry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown scope: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget-object v0, LeK;->e:LeK;

    iput-object v0, p0, LVS;->a:LeK;

    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\rACL Entry: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LVS;->a:LeI;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LVS;->a:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LVS;->a:LeK;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LVS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LVS;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
