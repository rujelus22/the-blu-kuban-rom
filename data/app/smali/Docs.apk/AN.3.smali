.class public LAN;
.super LAM;
.source "UnsupportedElement.java"


# static fields
.field private static final a:LCy;


# instance fields
.field protected final a:Lxu;

.field private a:LzI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LzI",
            "<",
            "LCy;",
            ">;"
        }
    .end annotation
.end field

.field protected final a:Lzd;

.field private b:LCy;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 30
    new-instance v0, LCy;

    sget v1, LsC;->uneditable_item:I

    sget v2, LsH;->uneditable_item:I

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LCy;-><init>(IIZ)V

    sput-object v0, LAN;->a:LCy;

    return-void
.end method

.method public constructor <init>(LDb;Lzd;Lxu;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1}, LAM;-><init>(LDb;)V

    .line 44
    iput-object p2, p0, LAN;->a:Lzd;

    .line 45
    iput-object p3, p0, LAN;->a:Lxu;

    .line 46
    return-void
.end method


# virtual methods
.method protected a()LCy;
    .registers 3

    .prologue
    .line 93
    iget-object v0, p0, LAN;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    .line 94
    iget-object v1, p0, LAN;->a:Lzd;

    invoke-interface {v1, v0, v0}, Lzd;->a(II)Lvs;

    move-result-object v0

    .line 95
    invoke-interface {v0}, Lvs;->a()I

    move-result v0

    invoke-static {v0}, LxH;->a(I)LxH;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_1b

    invoke-virtual {v0}, LxH;->a()LCy;

    move-result-object v0

    :goto_1a
    return-object v0

    :cond_1b
    sget-object v0, LAN;->a:LCy;

    goto :goto_1a
.end method

.method public a(IILjava/util/List;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, LAN;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    .line 86
    iget-object v1, p0, LAN;->b:LCy;

    if-eqz v1, :cond_24

    iget-object v1, p0, LAN;->b:LCy;

    invoke-virtual {v1}, LCy;->a()Z

    move-result v1

    if-eqz v1, :cond_24

    if-gt p1, v0, :cond_24

    if-lt p2, v0, :cond_24

    .line 87
    iget-object v0, p0, LAN;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    .line 88
    new-instance v1, LKj;

    invoke-direct {v1, v0, v0}, LKj;-><init>(II)V

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_24
    return-void
.end method

.method public a(Lza;)V
    .registers 5
    .parameter

    .prologue
    .line 50
    invoke-super {p0, p1}, LAM;->a(Lza;)V

    .line 52
    iget-object v0, p0, LAN;->a:LzI;

    iget-object v1, p0, LAN;->a:LDI;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 53
    iget-object v0, p0, LAN;->a:LDI;

    const/4 v1, 0x0

    iget-object v2, p0, LAN;->a:LDI;

    invoke-virtual {v2}, LDI;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LDI;->a(II)LDI;

    .line 54
    return-void
.end method

.method protected b()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 58
    invoke-virtual {p0}, LAN;->a()LCy;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_56

    iget-object v1, p0, LAN;->b:LCy;

    invoke-virtual {v0, v1}, LCy;->a(LCt;)Z

    move-result v1

    if-nez v1, :cond_56

    .line 60
    iput-object v0, p0, LAN;->b:LCy;

    .line 61
    iget-object v0, p0, LAN;->a:LzI;

    if-eqz v0, :cond_1c

    .line 63
    iget-object v0, p0, LAN;->a:LzI;

    iget-object v1, p0, LAN;->a:LDI;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 65
    :cond_1c
    iget-object v0, p0, LAN;->b:LCy;

    invoke-virtual {v0}, LCy;->a()LzI;

    move-result-object v0

    iput-object v0, p0, LAN;->a:LzI;

    .line 66
    iget-object v0, p0, LAN;->a:LDI;

    iget-object v1, p0, LAN;->a:LDI;

    invoke-virtual {v1}, LDI;->length()I

    move-result v1

    iget-object v2, p0, LAN;->a:Lxu;

    invoke-interface {v2}, Lxu;->a()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LAN;->b:LCy;

    invoke-virtual {v3}, LCy;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    const/16 v4, 0x20

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v5, v1, v2}, LDI;->a(IILjava/lang/CharSequence;)LDI;

    .line 69
    iget-object v0, p0, LAN;->b:LCy;

    invoke-virtual {v0}, LCy;->a()Z

    move-result v0

    if-nez v0, :cond_56

    .line 70
    iget-object v0, p0, LAN;->a:LDI;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, LDI;->a(Ljava/lang/CharSequence;)LDI;

    .line 74
    :cond_56
    iget-object v0, p0, LAN;->a:Lxu;

    invoke-interface {v0}, Lxu;->a()Landroid/content/Context;

    move-result-object v1

    .line 75
    iget-object v0, p0, LAN;->b:LCy;

    invoke-virtual {v0}, LCy;->a()Z

    move-result v0

    if-eqz v0, :cond_72

    iget-object v0, p0, LAN;->a:LDI;

    invoke-virtual {v0}, LDI;->length()I

    move-result v0

    .line 77
    :goto_6a
    iget-object v2, p0, LAN;->a:LzI;

    iget-object v3, p0, LAN;->a:LDI;

    invoke-interface {v2, v3, v5, v0, v1}, LzI;->a(Landroid/text/Spannable;IILandroid/content/Context;)V

    .line 78
    return-void

    .line 75
    :cond_72
    iget-object v0, p0, LAN;->a:LDI;

    invoke-virtual {v0}, LDI;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_6a
.end method
