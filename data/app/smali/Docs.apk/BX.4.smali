.class public LBX;
.super Ljava/lang/Object;
.source "HorizontalRuleSpan.java"

# interfaces
.implements LGB;


# instance fields
.field private final a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(IF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LBX;->a:Landroid/graphics/Paint;

    .line 21
    iget-object v0, p0, LBX;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 22
    iget-object v0, p0, LBX;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 23
    return-void
.end method


# virtual methods
.method public a(LFf;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;III)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    add-int/lit8 v0, p3, 0xa

    int-to-float v1, v0

    int-to-float v2, p6

    add-int/lit8 v0, p4, -0xa

    int-to-float v3, v0

    int-to-float v4, p6

    iget-object v5, p0, LBX;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, LFf;->b(FFFFLandroid/graphics/Paint;)V

    .line 29
    return-void
.end method
