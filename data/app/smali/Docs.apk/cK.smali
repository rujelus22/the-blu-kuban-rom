.class public LcK;
.super Ljava/lang/Object;
.source "GoogleAnalytics.java"

# interfaces
.implements LbZ;
.implements Ldk;


# static fields
.field private static a:LcK;


# instance fields
.field private a:Landroid/content/Context;

.field private a:LbX;

.field private a:Lci;

.field private a:Ldj;

.field private volatile a:Ljava/lang/Boolean;

.field private volatile a:Ljava/lang/String;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ldj;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LcK;->a:Ljava/util/Map;

    .line 53
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 56
    invoke-static {p1}, LcF;->a(Landroid/content/Context;)LcF;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LcK;-><init>(Landroid/content/Context;Lci;)V

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lci;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LcK;->a:Ljava/util/Map;

    .line 60
    if-nez p1, :cond_14

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_14
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LcK;->a:Landroid/content/Context;

    .line 64
    iput-object p2, p0, LcK;->a:Lci;

    .line 65
    new-instance v0, LbX;

    invoke-direct {v0}, LbX;-><init>()V

    iput-object v0, p0, LcK;->a:LbX;

    .line 66
    iget-object v0, p0, LcK;->a:Lci;

    new-instance v1, LcL;

    invoke-direct {v1, p0}, LcL;-><init>(LcK;)V

    invoke-interface {v0, v1}, Lci;->a(Lca;)V

    .line 74
    iget-object v0, p0, LcK;->a:Lci;

    new-instance v1, LcM;

    invoke-direct {v1, p0}, LcM;-><init>(LcK;)V

    invoke-interface {v0, v1}, Lci;->a(Lcj;)V

    .line 82
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LcK;
    .registers 3
    .parameter

    .prologue
    .line 91
    const-class v1, LcK;

    monitor-enter v1

    :try_start_3
    sget-object v0, LcK;->a:LcK;

    if-nez v0, :cond_e

    .line 92
    new-instance v0, LcK;

    invoke-direct {v0, p0}, LcK;-><init>(Landroid/content/Context;)V

    sput-object v0, LcK;->a:LcK;

    .line 94
    :cond_e
    sget-object v0, LcK;->a:LcK;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 91
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(LcK;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, LcK;->a:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(LcK;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, LcK;->a:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Ldj;
    .registers 4
    .parameter

    .prologue
    .line 142
    monitor-enter p0

    if-nez p1, :cond_e

    .line 143
    :try_start_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackingId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_b

    .line 142
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 145
    :cond_e
    :try_start_e
    iget-object v0, p0, LcK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldj;

    .line 148
    if-nez v0, :cond_28

    .line 149
    new-instance v0, LcN;

    invoke-direct {v0, p1, p0}, LcN;-><init>(Ljava/lang/String;Ldk;)V

    .line 150
    iget-object v1, p0, LcK;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v1, p0, LcK;->a:Ldj;

    if-nez v1, :cond_28

    .line 152
    iput-object v0, p0, LcK;->a:Ldj;
    :try_end_28
    .catchall {:try_start_e .. :try_end_28} :catchall_b

    .line 155
    :cond_28
    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized a(Ljava/util/Map;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    monitor-enter p0

    if-nez p1, :cond_e

    .line 179
    :try_start_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "hit cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_b

    .line 178
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 181
    :cond_e
    :try_start_e
    const-string v0, "language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ldl;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    const-string v0, "adSenseAdMobHitId"

    iget-object v1, p0, LcK;->a:LbX;

    invoke-virtual {v1}, LbX;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const-string v0, "screenResolution"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LcK;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LcK;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, LcK;->a:Lci;

    invoke-interface {v0, p1}, Lci;->a(Ljava/util/Map;)V

    .line 188
    const-string v0, "trackingId"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LcK;->b:Ljava/lang/String;
    :try_end_6d
    .catchall {:try_start_e .. :try_end_6d} :catchall_b

    .line 189
    monitor-exit p0

    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 131
    iput-boolean p1, p0, LcK;->a:Z

    .line 132
    invoke-static {p1}, LcT;->a(Z)V

    .line 133
    return-void
.end method
