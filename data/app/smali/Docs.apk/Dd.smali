.class public abstract LDd;
.super Ljava/lang/Object;
.source "OffsetTreeNode.java"

# interfaces
.implements LDc;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ContainerType:",
        "Ljava/lang/Object;",
        "NodeType:",
        "LDd",
        "<TContainerType;TNodeType;>;>",
        "Ljava/lang/Object;",
        "LDc;",
        "Ljava/lang/Iterable",
        "<TNodeType;>;"
    }
.end annotation


# instance fields
.field protected final a:LKh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LKh",
            "<TContainerType;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TNodeType;>;"
        }
    .end annotation
.end field

.field protected final b:LKh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LKh",
            "<TContainerType;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LKh;LKh;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKh",
            "<TContainerType;>;",
            "LKh",
            "<TContainerType;>;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LDd;->a:Ljava/util/List;

    .line 34
    iput-object p1, p0, LDd;->a:LKh;

    .line 35
    iput-object p2, p0, LDd;->b:LKh;

    .line 36
    return-void
.end method

.method private a(I)I
    .registers 5
    .parameter

    .prologue
    .line 213
    new-instance v0, LDe;

    invoke-direct {v0, p0, p1}, LDe;-><init>(LDd;I)V

    .line 223
    iget-object v1, p0, LDd;->a:Ljava/util/List;

    new-instance v2, LDf;

    invoke-direct {v2, p0}, LDf;-><init>(LDd;)V

    invoke-static {v1, v0, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 239
    if-ltz v0, :cond_13

    .line 242
    :goto_12
    return v0

    :cond_13
    const/4 v0, -0x1

    goto :goto_12
.end method

.method private a(LKh;)LDd;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKh",
            "<TContainerType;>;)TNodeType;"
        }
    .end annotation

    .prologue
    .line 100
    .line 101
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LKh;->a(Ljava/lang/Object;I)LKh;

    move-result-object v0

    .line 102
    invoke-virtual {p0, v0, p1}, LDd;->a(LKh;LKh;)LDd;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LDd;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TNodeType;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, LDd;->b:LKh;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LKh;->a(Ljava/lang/Object;I)LKh;

    move-result-object v0

    .line 118
    invoke-direct {p0, v0}, LDd;->a(LKh;)LDd;

    move-result-object v0

    .line 119
    iget-object v1, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    return-object v0
.end method

.method public final a(I)LDd;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TNodeType;"
        }
    .end annotation

    .prologue
    .line 251
    invoke-direct {p0, p1}, LDd;->a(I)I

    move-result v0

    .line 252
    if-ltz v0, :cond_f

    .line 253
    iget-object v1, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDd;

    .line 255
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final a(LDd;)LDd;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeType;)TNodeType;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 129
    if-ltz v2, :cond_1e

    const/4 v0, 0x1

    :goto_a
    invoke-static {v0}, Lagu;->a(Z)V

    .line 131
    iget-object v0, p1, LDd;->a:LKh;

    .line 132
    const/4 v3, 0x0

    invoke-interface {v0, v3, v1}, LKh;->a(Ljava/lang/Object;I)LKh;

    move-result-object v0

    .line 133
    invoke-direct {p0, v0}, LDd;->a(LKh;)LDd;

    move-result-object v0

    .line 134
    iget-object v1, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 135
    return-object v0

    :cond_1e
    move v0, v1

    .line 129
    goto :goto_a
.end method

.method protected abstract a(LKh;LKh;)LDd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKh",
            "<TContainerType;>;",
            "LKh",
            "<TContainerType;>;)TNodeType;"
        }
    .end annotation
.end method

.method protected a()V
    .registers 3

    .prologue
    .line 42
    iget-object v0, p0, LDd;->a:LKh;

    invoke-interface {v0}, LKh;->a()V

    .line 43
    iget-object v0, p0, LDd;->b:LKh;

    invoke-interface {v0}, LKh;->a()V

    .line 44
    iget-object v0, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDd;

    .line 47
    invoke-virtual {v0}, LDd;->a()V

    goto :goto_10

    .line 49
    :cond_20
    return-void
.end method

.method public final a(LDd;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeType;)Z"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 146
    if-eqz v0, :cond_b

    .line 147
    invoke-virtual {p1}, LDd;->a()V

    .line 149
    :cond_b
    return v0
.end method

.method public b()LDd;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TNodeType;"
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_12

    iget-object v0, p0, LDd;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDd;

    :goto_11
    return-object v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, LDd;->a:LKh;

    invoke-interface {v0}, LKh;->a()I

    move-result v0

    return v0
.end method

.method public final d()I
    .registers 3

    .prologue
    .line 67
    iget-object v0, p0, LDd;->b:LKh;

    invoke-interface {v0}, LKh;->a()I

    move-result v0

    iget-object v1, p0, LDd;->b:LKh;

    invoke-interface {v1}, LKh;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final e()I
    .registers 3

    .prologue
    .line 75
    invoke-virtual {p0}, LDd;->d()I

    move-result v0

    invoke-virtual {p0}, LDd;->c()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final f()I
    .registers 3

    .prologue
    .line 83
    iget-object v0, p0, LDd;->a:LKh;

    invoke-interface {v0}, LKh;->a()I

    move-result v0

    iget-object v1, p0, LDd;->a:LKh;

    invoke-interface {v1}, LKh;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final g()I
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, LDd;->b:LKh;

    invoke-interface {v0}, LKh;->a()I

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TNodeType;>;"
        }
    .end annotation

    .prologue
    .line 267
    iget-object v0, p0, LDd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
