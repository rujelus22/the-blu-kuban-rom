.class final LG;
.super Ljava/lang/Object;
.source "LoaderManager.java"

# interfaces
.implements LM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LM",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final a:I

.field a:LE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LE",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:LF;

.field a:LG;

.field a:LL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LL",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final a:Landroid/os/Bundle;

.field a:Ljava/lang/Object;

.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z


# direct methods
.method public constructor <init>(LF;ILandroid/os/Bundle;LE;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            "LE",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    iput-object p1, p0, LG;->a:LF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    iput p2, p0, LG;->a:I

    .line 228
    iput-object p3, p0, LG;->a:Landroid/os/Bundle;

    .line 229
    iput-object p4, p0, LG;->a:LE;

    .line 230
    return-void
.end method


# virtual methods
.method a()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 233
    iget-boolean v0, p0, LG;->d:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, LG;->e:Z

    if-eqz v0, :cond_c

    .line 237
    iput-boolean v3, p0, LG;->c:Z

    .line 265
    :cond_b
    :goto_b
    return-void

    .line 241
    :cond_c
    iget-boolean v0, p0, LG;->c:Z

    if-nez v0, :cond_b

    .line 246
    iput-boolean v3, p0, LG;->c:Z

    .line 248
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_2e

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Starting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :cond_2e
    iget-object v0, p0, LG;->a:LL;

    if-nez v0, :cond_42

    iget-object v0, p0, LG;->a:LE;

    if-eqz v0, :cond_42

    .line 250
    iget-object v0, p0, LG;->a:LE;

    iget v1, p0, LG;->a:I

    iget-object v2, p0, LG;->a:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, LE;->a(ILandroid/os/Bundle;)LL;

    move-result-object v0

    iput-object v0, p0, LG;->a:LL;

    .line 252
    :cond_42
    iget-object v0, p0, LG;->a:LL;

    if-eqz v0, :cond_b

    .line 253
    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v0

    if-eqz v0, :cond_7d

    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_7d

    .line 255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object returned from onCreateLoader must not be a non-static inner member class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LG;->a:LL;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_7d
    iget-boolean v0, p0, LG;->h:Z

    if-nez v0, :cond_8a

    .line 260
    iget-object v0, p0, LG;->a:LL;

    iget v1, p0, LG;->a:I

    invoke-virtual {v0, v1, p0}, LL;->a(ILM;)V

    .line 261
    iput-boolean v3, p0, LG;->h:Z

    .line 263
    :cond_8a
    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {v0}, LL;->c()V

    goto/16 :goto_b
.end method

.method public a(LL;Ljava/lang/Object;)V
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LL",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 360
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_1d

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadComplete: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_1d
    iget-boolean v0, p0, LG;->g:Z

    if-eqz v0, :cond_2d

    .line 363
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_2c

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load complete -- destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_2c
    :goto_2c
    return-void

    .line 367
    :cond_2d
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Lae;

    iget v1, p0, LG;->a:I

    invoke-virtual {v0, v1}, Lae;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_45

    .line 370
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_2c

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load complete -- not active"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2c

    .line 374
    :cond_45
    iget-object v0, p0, LG;->a:LG;

    .line 375
    if-eqz v0, :cond_79

    .line 379
    sget-boolean v1, LF;->a:Z

    if-eqz v1, :cond_65

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Switching to pending loader: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_65
    iput-object v4, p0, LG;->a:LG;

    .line 381
    iget-object v1, p0, LG;->a:LF;

    iget-object v1, v1, LF;->a:Lae;

    iget v2, p0, LG;->a:I

    invoke-virtual {v1, v2, v4}, Lae;->a(ILjava/lang/Object;)V

    .line 382
    invoke-virtual {p0}, LG;->f()V

    .line 383
    iget-object v1, p0, LG;->a:LF;

    invoke-virtual {v1, v0}, LF;->a(LG;)V

    goto :goto_2c

    .line 389
    :cond_79
    iget-object v0, p0, LG;->a:Ljava/lang/Object;

    if-ne v0, p2, :cond_81

    iget-boolean v0, p0, LG;->a:Z

    if-nez v0, :cond_8d

    .line 390
    :cond_81
    iput-object p2, p0, LG;->a:Ljava/lang/Object;

    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, LG;->a:Z

    .line 392
    iget-boolean v0, p0, LG;->c:Z

    if-eqz v0, :cond_8d

    .line 393
    invoke-virtual {p0, p1, p2}, LG;->b(LL;Ljava/lang/Object;)V

    .line 403
    :cond_8d
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->b:Lae;

    iget v1, p0, LG;->a:I

    invoke-virtual {v0, v1}, Lae;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LG;

    .line 404
    if-eqz v0, :cond_ac

    if-eq v0, p0, :cond_ac

    .line 405
    const/4 v1, 0x0

    iput-boolean v1, v0, LG;->b:Z

    .line 406
    invoke-virtual {v0}, LG;->f()V

    .line 407
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->b:Lae;

    iget v1, p0, LG;->a:I

    invoke-virtual {v0, v1}, Lae;->b(I)V

    .line 410
    :cond_ac
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_2c

    iget-object v0, p0, LG;->a:LF;

    invoke-virtual {v0}, LF;->a()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 411
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    invoke-virtual {v0}, Lr;->b()V

    goto/16 :goto_2c
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 449
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, LG;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 450
    const-string v0, " mArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LG;->a:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 451
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCallbacks="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LG;->a:LE;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 452
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoader="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 453
    iget-object v0, p0, LG;->a:LL;

    if-eqz v0, :cond_4d

    .line 454
    iget-object v0, p0, LG;->a:LL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LL;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 456
    :cond_4d
    iget-boolean v0, p0, LG;->a:Z

    if-nez v0, :cond_55

    iget-boolean v0, p0, LG;->b:Z

    if-eqz v0, :cond_79

    .line 457
    :cond_55
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->a:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 458
    const-string v0, "  mDeliveredData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->b:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 459
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LG;->a:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 461
    :cond_79
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->c:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 462
    const-string v0, " mReportNextStart="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->f:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 463
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->g:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 464
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->d:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 465
    const-string v0, " mRetainingStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->e:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 466
    const-string v0, " mListenerRegistered="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LG;->h:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 467
    iget-object v0, p0, LG;->a:LG;

    if-eqz v0, :cond_e9

    .line 468
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Loader "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 469
    iget-object v0, p0, LG;->a:LG;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, LG;->a:LG;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LG;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 472
    :cond_e9
    return-void
.end method

.method b()V
    .registers 4

    .prologue
    .line 268
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_1c

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Retaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_1c
    const/4 v0, 0x1

    iput-boolean v0, p0, LG;->d:Z

    .line 270
    iget-boolean v0, p0, LG;->c:Z

    iput-boolean v0, p0, LG;->e:Z

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, LG;->c:Z

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, LG;->a:LE;

    .line 273
    return-void
.end method

.method b(LL;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LL",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 416
    iget-object v0, p0, LG;->a:LE;

    if-eqz v0, :cond_5e

    .line 417
    const/4 v0, 0x0

    .line 418
    iget-object v1, p0, LG;->a:LF;

    iget-object v1, v1, LF;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_6f

    .line 419
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    iget-object v0, v0, Lr;->a:Ljava/lang/String;

    .line 420
    iget-object v1, p0, LG;->a:LF;

    iget-object v1, v1, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    const-string v2, "onLoadFinished"

    iput-object v2, v1, Lr;->a:Ljava/lang/String;

    move-object v1, v0

    .line 423
    :goto_1e
    :try_start_1e
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_48

    const-string v0, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  onLoadFinished in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p2}, LL;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :cond_48
    iget-object v0, p0, LG;->a:LE;

    invoke-interface {v0, p1, p2}, LE;->a(LL;Ljava/lang/Object;)V
    :try_end_4d
    .catchall {:try_start_1e .. :try_end_4d} :catchall_5f

    .line 427
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_5b

    .line 428
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    iput-object v1, v0, Lr;->a:Ljava/lang/String;

    .line 431
    :cond_5b
    const/4 v0, 0x1

    iput-boolean v0, p0, LG;->b:Z

    .line 433
    :cond_5e
    return-void

    .line 427
    :catchall_5f
    move-exception v0

    iget-object v2, p0, LG;->a:LF;

    iget-object v2, v2, LF;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_6e

    .line 428
    iget-object v2, p0, LG;->a:LF;

    iget-object v2, v2, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, v2, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    iput-object v1, v2, Lr;->a:Ljava/lang/String;

    :cond_6e
    throw v0

    :cond_6f
    move-object v1, v0

    goto :goto_1e
.end method

.method c()V
    .registers 4

    .prologue
    .line 276
    iget-boolean v0, p0, LG;->d:Z

    if-eqz v0, :cond_30

    .line 277
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_20

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Finished Retaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :cond_20
    const/4 v0, 0x0

    iput-boolean v0, p0, LG;->d:Z

    .line 279
    iget-boolean v0, p0, LG;->c:Z

    iget-boolean v1, p0, LG;->e:Z

    if-eq v0, v1, :cond_30

    .line 280
    iget-boolean v0, p0, LG;->c:Z

    if-nez v0, :cond_30

    .line 284
    invoke-virtual {p0}, LG;->e()V

    .line 289
    :cond_30
    iget-boolean v0, p0, LG;->c:Z

    if-eqz v0, :cond_43

    iget-boolean v0, p0, LG;->a:Z

    if-eqz v0, :cond_43

    iget-boolean v0, p0, LG;->f:Z

    if-nez v0, :cond_43

    .line 296
    iget-object v0, p0, LG;->a:LL;

    iget-object v1, p0, LG;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, LG;->b(LL;Ljava/lang/Object;)V

    .line 298
    :cond_43
    return-void
.end method

.method d()V
    .registers 3

    .prologue
    .line 301
    iget-boolean v0, p0, LG;->c:Z

    if-eqz v0, :cond_16

    .line 302
    iget-boolean v0, p0, LG;->f:Z

    if-eqz v0, :cond_16

    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, LG;->f:Z

    .line 304
    iget-boolean v0, p0, LG;->a:Z

    if-eqz v0, :cond_16

    .line 305
    iget-object v0, p0, LG;->a:LL;

    iget-object v1, p0, LG;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, LG;->b(LL;Ljava/lang/Object;)V

    .line 309
    :cond_16
    return-void
.end method

.method e()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 312
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_1d

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Stopping: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_1d
    iput-boolean v3, p0, LG;->c:Z

    .line 314
    iget-boolean v0, p0, LG;->d:Z

    if-nez v0, :cond_37

    .line 315
    iget-object v0, p0, LG;->a:LL;

    if-eqz v0, :cond_37

    iget-boolean v0, p0, LG;->h:Z

    if-eqz v0, :cond_37

    .line 317
    iput-boolean v3, p0, LG;->h:Z

    .line 318
    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {v0, p0}, LL;->a(LM;)V

    .line 319
    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {v0}, LL;->f()V

    .line 322
    :cond_37
    return-void
.end method

.method f()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 325
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_1e

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Destroying: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_1e
    const/4 v0, 0x1

    iput-boolean v0, p0, LG;->g:Z

    .line 327
    iget-boolean v0, p0, LG;->b:Z

    .line 328
    iput-boolean v4, p0, LG;->b:Z

    .line 329
    iget-object v1, p0, LG;->a:LE;

    if-eqz v1, :cond_7d

    iget-object v1, p0, LG;->a:LL;

    if-eqz v1, :cond_7d

    iget-boolean v1, p0, LG;->a:Z

    if-eqz v1, :cond_7d

    if-eqz v0, :cond_7d

    .line 330
    sget-boolean v0, LF;->a:Z

    if-eqz v0, :cond_4f

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Reseting: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_4f
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_b1

    .line 333
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    iget-object v0, v0, Lr;->a:Ljava/lang/String;

    .line 334
    iget-object v1, p0, LG;->a:LF;

    iget-object v1, v1, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, v1, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    const-string v3, "onLoaderReset"

    iput-object v3, v1, Lr;->a:Ljava/lang/String;

    move-object v1, v0

    .line 337
    :goto_68
    :try_start_68
    iget-object v0, p0, LG;->a:LE;

    iget-object v3, p0, LG;->a:LL;

    invoke-interface {v0, v3}, LE;->a(LL;)V
    :try_end_6f
    .catchall {:try_start_68 .. :try_end_6f} :catchall_a1

    .line 339
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_7d

    .line 340
    iget-object v0, p0, LG;->a:LF;

    iget-object v0, v0, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    iput-object v1, v0, Lr;->a:Ljava/lang/String;

    .line 344
    :cond_7d
    iput-object v2, p0, LG;->a:LE;

    .line 345
    iput-object v2, p0, LG;->a:Ljava/lang/Object;

    .line 346
    iput-boolean v4, p0, LG;->a:Z

    .line 347
    iget-object v0, p0, LG;->a:LL;

    if-eqz v0, :cond_97

    .line 348
    iget-boolean v0, p0, LG;->h:Z

    if-eqz v0, :cond_92

    .line 349
    iput-boolean v4, p0, LG;->h:Z

    .line 350
    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {v0, p0}, LL;->a(LM;)V

    .line 352
    :cond_92
    iget-object v0, p0, LG;->a:LL;

    invoke-virtual {v0}, LL;->j()V

    .line 354
    :cond_97
    iget-object v0, p0, LG;->a:LG;

    if-eqz v0, :cond_a0

    .line 355
    iget-object v0, p0, LG;->a:LG;

    invoke-virtual {v0}, LG;->f()V

    .line 357
    :cond_a0
    return-void

    .line 339
    :catchall_a1
    move-exception v0

    iget-object v2, p0, LG;->a:LF;

    iget-object v2, v2, LF;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_b0

    .line 340
    iget-object v2, p0, LG;->a:LF;

    iget-object v2, v2, LF;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, v2, Landroid/support/v4/app/FragmentActivity;->a:Lr;

    iput-object v1, v2, Lr;->a:Ljava/lang/String;

    :cond_b0
    throw v0

    :cond_b1
    move-object v1, v2

    goto :goto_68
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 437
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 438
    const-string v1, "LoaderInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    iget v1, p0, LG;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 442
    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    iget-object v1, p0, LG;->a:LL;

    invoke-static {v1, v0}, Lab;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 444
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
