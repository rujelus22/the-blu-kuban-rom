.class public abstract LaqC;
.super Ljava/lang/Object;
.source "Scoping.java"


# static fields
.field public static final a:LaqC;

.field public static final b:LaqC;

.field public static final c:LaqC;

.field public static final d:LaqC;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 43
    new-instance v0, LaqD;

    invoke-direct {v0}, LaqD;-><init>()V

    sput-object v0, LaqC;->a:LaqC;

    .line 61
    new-instance v0, LaqE;

    invoke-direct {v0}, LaqE;-><init>()V

    sput-object v0, LaqC;->b:LaqC;

    .line 79
    new-instance v0, LaqF;

    invoke-direct {v0}, LaqF;-><init>()V

    sput-object v0, LaqC;->c:LaqC;

    .line 97
    new-instance v0, LaqG;

    invoke-direct {v0}, LaqG;-><init>()V

    sput-object v0, LaqC;->d:LaqC;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LaqD;)V
    .registers 2
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, LaqC;-><init>()V

    return-void
.end method

.method static a(Laop;LapL;LapY;Ljava/lang/Object;LaqC;)LapY;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "LapL;",
            "LapY",
            "<+TT;>;",
            "Ljava/lang/Object;",
            "LaqC;",
            ")",
            "LapY",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p4}, LaqC;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 243
    :goto_6
    return-object p2

    .line 239
    :cond_7
    invoke-virtual {p4}, LaqC;->a()LaoC;

    move-result-object v0

    .line 241
    new-instance v1, Laqv;

    invoke-direct {v1, p1, p2}, Laqv;-><init>(LapL;LapY;)V

    invoke-interface {v0, p0, v1}, LaoC;->a(Laop;Laoz;)Laoz;

    move-result-object v0

    .line 243
    new-instance p2, LapZ;

    invoke-direct {p2, v0, p3}, LapZ;-><init>(Laoz;Ljava/lang/Object;)V

    goto :goto_6
.end method

.method public static a(LaoC;)LaqC;
    .registers 2
    .parameter

    .prologue
    .line 141
    sget-object v0, LaoE;->a:LaoC;

    if-ne p0, v0, :cond_7

    .line 142
    sget-object v0, LaqC;->c:LaqC;

    .line 145
    :goto_6
    return-object v0

    :cond_7
    new-instance v0, LaqI;

    invoke-direct {v0, p0}, LaqI;-><init>(LaoC;)V

    goto :goto_6
.end method

.method static a(LaqC;LapL;Lapu;)LaqC;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 252
    invoke-virtual {p0}, LaqC;->a()Ljava/lang/Class;

    move-result-object v0

    .line 253
    if-nez v0, :cond_7

    .line 263
    :goto_6
    return-object p0

    .line 257
    :cond_7
    iget-object v1, p1, LapL;->a:LaqL;

    invoke-interface {v1, v0}, LaqL;->a(Ljava/lang/Class;)LaoC;

    move-result-object v1

    .line 258
    if-eqz v1, :cond_14

    .line 259
    invoke-static {v1}, LaqC;->a(LaoC;)LaqC;

    move-result-object p0

    goto :goto_6

    .line 262
    :cond_14
    invoke-virtual {p2, v0}, Lapu;->b(Ljava/lang/Class;)Lapu;

    .line 263
    sget-object p0, LaqC;->a:LaqC;

    goto :goto_6
.end method

.method public static a(Ljava/lang/Class;)LaqC;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LaqC;"
        }
    .end annotation

    .prologue
    .line 116
    const-class v0, LaoJ;

    if-eq p0, v0, :cond_8

    const-class v0, LatJ;

    if-ne p0, v0, :cond_b

    .line 118
    :cond_8
    sget-object v0, LaqC;->b:LaqC;

    .line 121
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, LaqH;

    invoke-direct {v0, p0}, LaqH;-><init>(Ljava/lang/Class;)V

    goto :goto_a
.end method


# virtual methods
.method public a()LaoC;
    .registers 2

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Ljava/lang/Class;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(LaqZ;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LaqZ",
            "<TV;>;)TV;"
        }
    .end annotation
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 169
    sget-object v0, LaqC;->a:LaqC;

    if-eq p0, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 177
    invoke-virtual {p0}, LaqC;->a()LaoC;

    move-result-object v0

    sget-object v1, LaoE;->b:LaoC;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 211
    instance-of v1, p1, LaqC;

    if-eqz v1, :cond_24

    .line 212
    check-cast p1, LaqC;

    .line 213
    invoke-virtual {p0}, LaqC;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, LaqC;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    invoke-virtual {p0}, LaqC;->a()LaoC;

    move-result-object v1

    invoke-virtual {p1}, LaqC;->a()LaoC;

    move-result-object v2

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    const/4 v0, 0x1

    .line 216
    :cond_24
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 222
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, LaqC;->a()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, LaqC;->a()LaoC;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
