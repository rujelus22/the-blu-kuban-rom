.class LyP;
.super Lvg;
.source "KixJSVM.java"


# instance fields
.field final synthetic a:Lyz;


# direct methods
.method private constructor <init>(Lyz;)V
    .registers 2
    .parameter

    .prologue
    .line 1097
    iput-object p1, p0, LyP;->a:Lyz;

    invoke-direct {p0}, Lvg;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lyz;LyA;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1097
    invoke-direct {p0, p1}, LyP;-><init>(Lyz;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 1107
    iget-object v0, p0, LyP;->a:Lyz;

    #getter for: Lyz;->a:LyK;
    invoke-static {v0}, Lyz;->access$1700(Lyz;)LyK;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1108
    iget-object v0, p0, LyP;->a:Lyz;

    #getter for: Lyz;->a:LyK;
    invoke-static {v0}, Lyz;->access$1700(Lyz;)LyK;

    move-result-object v0

    invoke-interface {v0}, LyK;->b()V

    .line 1110
    :cond_11
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 1129
    iget-object v0, p0, LyP;->a:Lyz;

    invoke-virtual {v0, p1}, Lyz;->onDocumentSaveStateChanged(I)V

    .line 1130
    return-void
.end method

.method public a(IIZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1102
    iget-object v0, p0, LyP;->a:Lyz;

    invoke-virtual {v0, p1, p2}, Lyz;->onSelectionChanged(II)V

    .line 1103
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1124
    iget-object v0, p0, LyP;->a:Lyz;

    invoke-virtual {v0, p1}, Lyz;->onModelLoadFailed(Ljava/lang/String;)V

    .line 1125
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1114
    iget-object v0, p0, LyP;->a:Lyz;

    invoke-virtual {v0, p1, p2}, Lyz;->onImageUrlFetched(Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1139
    iget-object v0, p0, LyP;->a:Lyz;

    invoke-virtual {v0}, Lyz;->onDocumentUpdated()V

    .line 1140
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 1119
    iget-object v0, p0, LyP;->a:Lyz;

    invoke-virtual {v0}, Lyz;->onModelLoadComplete()V

    .line 1120
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 1134
    iget-object v0, p0, LyP;->a:Lyz;

    invoke-virtual {v0}, Lyz;->onDocumentUpdated()V

    .line 1135
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 1144
    iget-object v0, p0, LyP;->a:Lyz;

    #calls: Lyz;->onReset()V
    invoke-static {v0}, Lyz;->access$2000(Lyz;)V

    .line 1145
    return-void
.end method
