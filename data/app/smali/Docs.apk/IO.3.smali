.class public final LIO;
.super Ljava/lang/Object;
.source "ApplicationEvent.java"


# static fields
.field private static a:I

.field public static final a:LIO;

.field private static a:[LIO;

.field public static final b:LIO;

.field public static final c:LIO;

.field public static final d:LIO;

.field public static final e:LIO;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 51
    new-instance v0, LIO;

    const-string v1, "kApplicationInitialized"

    invoke-direct {v0, v1}, LIO;-><init>(Ljava/lang/String;)V

    sput-object v0, LIO;->a:LIO;

    .line 52
    new-instance v0, LIO;

    const-string v1, "kSheetInserted"

    invoke-direct {v0, v1}, LIO;-><init>(Ljava/lang/String;)V

    sput-object v0, LIO;->b:LIO;

    .line 53
    new-instance v0, LIO;

    const-string v1, "kSheetMoved"

    invoke-direct {v0, v1}, LIO;-><init>(Ljava/lang/String;)V

    sput-object v0, LIO;->c:LIO;

    .line 54
    new-instance v0, LIO;

    const-string v1, "kSheetRemoved"

    invoke-direct {v0, v1}, LIO;-><init>(Ljava/lang/String;)V

    sput-object v0, LIO;->d:LIO;

    .line 55
    new-instance v0, LIO;

    const-string v1, "kSheetRenamed"

    invoke-direct {v0, v1}, LIO;-><init>(Ljava/lang/String;)V

    sput-object v0, LIO;->e:LIO;

    .line 91
    const/4 v0, 0x5

    new-array v0, v0, [LIO;

    sget-object v1, LIO;->a:LIO;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, LIO;->b:LIO;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LIO;->c:LIO;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LIO;->d:LIO;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LIO;->e:LIO;

    aput-object v2, v0, v1

    sput-object v0, LIO;->a:[LIO;

    .line 92
    sput v3, LIO;->a:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, LIO;->a:Ljava/lang/String;

    .line 76
    sget v0, LIO;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LIO;->a:I

    iput v0, p0, LIO;->b:I

    .line 77
    return-void
.end method

.method public static a(I)LIO;
    .registers 4
    .parameter

    .prologue
    .line 66
    sget-object v0, LIO;->a:[LIO;

    array-length v0, v0

    if-ge p0, v0, :cond_14

    if-ltz p0, :cond_14

    sget-object v0, LIO;->a:[LIO;

    aget-object v0, v0, p0

    iget v0, v0, LIO;->b:I

    if-ne v0, p0, :cond_14

    .line 67
    sget-object v0, LIO;->a:[LIO;

    aget-object v0, v0, p0

    .line 70
    :goto_13
    return-object v0

    .line 68
    :cond_14
    const/4 v0, 0x0

    :goto_15
    sget-object v1, LIO;->a:[LIO;

    array-length v1, v1

    if-ge v0, v1, :cond_2a

    .line 69
    sget-object v1, LIO;->a:[LIO;

    aget-object v1, v1, v0

    iget v1, v1, LIO;->b:I

    if-ne v1, p0, :cond_27

    .line 70
    sget-object v1, LIO;->a:[LIO;

    aget-object v0, v1, v0

    goto :goto_13

    .line 68
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 71
    :cond_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LIO;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, LIO;->a:Ljava/lang/String;

    return-object v0
.end method
