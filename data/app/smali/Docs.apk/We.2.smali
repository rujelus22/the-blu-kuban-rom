.class public LWe;
.super Latd;
.source "DocsListGDataParser.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Latd;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 61
    return-void
.end method


# virtual methods
.method protected a()LVU;
    .registers 2

    .prologue
    .line 70
    new-instance v0, LVU;

    invoke-direct {v0}, LVU;-><init>()V

    return-object v0
.end method

.method protected a()LVV;
    .registers 2

    .prologue
    .line 65
    new-instance v0, LVV;

    invoke-direct {v0}, LVV;-><init>()V

    return-object v0
.end method

.method public a(LasT;)LVV;
    .registers 3
    .parameter

    .prologue
    .line 223
    invoke-super {p0, p1}, Latd;->a(LasT;)LasT;

    move-result-object v0

    check-cast v0, LVV;

    return-object v0
.end method

.method protected bridge synthetic a()LasT;
    .registers 2

    .prologue
    .line 29
    invoke-virtual {p0}, LWe;->a()LVV;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LasT;)LasT;
    .registers 3
    .parameter

    .prologue
    .line 29
    invoke-virtual {p0, p1}, LWe;->a(LasT;)LVV;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a()LasU;
    .registers 2

    .prologue
    .line 29
    invoke-virtual {p0}, LWe;->a()LVU;

    move-result-object v0

    return-object v0
.end method

.method protected a(LasT;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x3a

    .line 165
    invoke-virtual {p0}, LWe;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 167
    instance-of v0, p1, LVV;

    if-nez v0, :cond_13

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected DocEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_13
    check-cast p1, LVV;

    .line 172
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v3

    .line 174
    const-string v4, "http://schemas.google.com/g/2005"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_99

    .line 175
    const-string v3, "resourceId"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_53

    .line 176
    invoke-static {v2}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 180
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 181
    const/4 v1, 0x0

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 185
    :cond_4c
    invoke-virtual {p1, v0}, LVV;->b(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p1, v1}, LVV;->c(Ljava/lang/String;)V

    .line 219
    :cond_52
    :goto_52
    return-void

    .line 187
    :cond_53
    const-string v1, "lastViewed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_63

    .line 188
    invoke-static {v2}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LVV;->e(Ljava/lang/String;)V

    goto :goto_52

    .line 189
    :cond_63
    const-string v1, "lastModifiedBy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 190
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    .line 191
    invoke-static {v2, v1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;I)Ljava/lang/String;

    move-result-object v0

    :goto_73
    if-eqz v0, :cond_52

    .line 193
    const-string v3, "name"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_89

    .line 194
    invoke-static {v2}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-virtual {p1, v0}, LVV;->h(Ljava/lang/String;)V

    .line 192
    :cond_84
    :goto_84
    invoke-static {v2, v1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_73

    .line 196
    :cond_89
    const-string v3, "email"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 197
    invoke-static {v2}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-virtual {p1, v0}, LVV;->i(Ljava/lang/String;)V

    goto :goto_84

    .line 202
    :cond_99
    const-string v4, "http://schemas.google.com/docs/2007"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_52

    .line 203
    const-string v3, "removed"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_ae

    .line 204
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LVV;->d(Z)V

    goto :goto_52

    .line 205
    :cond_ae
    const-string v3, "changestamp"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c4

    .line 206
    const-string v0, "value"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, LVV;->a(I)V

    goto :goto_52

    .line 208
    :cond_c4
    const-string v1, "md5Checksum"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d5

    .line 209
    invoke-static {v2}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 210
    invoke-virtual {p1, v0}, LVV;->d(Ljava/lang/String;)V

    goto/16 :goto_52

    .line 211
    :cond_d5
    const-string v1, "sharedWithMeDate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e6

    .line 212
    invoke-static {v2}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 213
    invoke-virtual {p1, v0}, LVV;->f(Ljava/lang/String;)V

    goto/16 :goto_52

    .line 214
    :cond_e6
    const-string v1, "modifiedByMeDate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 215
    invoke-static {v2}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-virtual {p1, v0}, LVV;->g(Ljava/lang/String;)V

    goto/16 :goto_52
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LasT;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x3a

    const/4 v4, 0x0

    .line 130
    const-string v0, "http://schemas.google.com/docs/2007#parent"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 131
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 135
    const-string v0, ""

    .line 137
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 138
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {v1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 142
    :cond_2f
    sget-object v2, LkP;->h:LkP;

    invoke-virtual {v2}, LkP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 143
    invoke-virtual {p0}, LWe;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Latf;->q:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_4e

    .line 145
    check-cast p4, LVV;

    .line 146
    invoke-virtual {p4, v1, v0}, LVV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_4d
    :goto_4d
    return-void

    .line 149
    :cond_4e
    const-string v0, "DocsListGDataParser"

    const-string v2, "Parent folder with resourceId %s has no title: "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4d

    .line 152
    :cond_5f
    const-string v0, "DocsListGDataParser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error parsing parent entry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4d

    .line 154
    :cond_78
    const-string v0, "http://schemas.google.com/docs/2007/thumbnail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_86

    .line 155
    check-cast p4, LVV;

    .line 156
    invoke-virtual {p4, p3}, LVV;->j(Ljava/lang/String;)V

    goto :goto_4d

    .line 157
    :cond_86
    instance-of v0, p4, LVV;

    if-eqz v0, :cond_4d

    .line 158
    check-cast p4, LVV;

    .line 159
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p4, p1, v0}, LVV;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_4d
.end method

.method protected a(LasT;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 98
    invoke-virtual {p0}, LWe;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 100
    instance-of v0, p1, LVV;

    if-nez v0, :cond_11

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected DocEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    move-object v0, p1

    .line 104
    check-cast v0, LVV;

    .line 105
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 107
    sget-object v4, Latf;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4e

    .line 108
    const/4 v3, 0x0

    sget-object v4, Latf;->x:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 109
    const-string v3, "http://schemas.google.com/g/2005/labels#starred"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 110
    invoke-virtual {v0, v1}, LVV;->a(Z)V

    move v0, v1

    .line 125
    :goto_33
    return v0

    .line 114
    :cond_34
    const-string v3, "http://schemas.google.com/g/2005/labels#shared"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_41

    .line 115
    invoke-virtual {v0, v1}, LVV;->b(Z)V

    move v0, v1

    .line 116
    goto :goto_33

    .line 119
    :cond_41
    const-string v3, "http://schemas.google.com/g/2005/labels#hidden"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 120
    invoke-virtual {v0, v1}, LVV;->c(Z)V

    move v0, v1

    .line 121
    goto :goto_33

    .line 125
    :cond_4e
    invoke-super {p0, p1}, Latd;->a(LasT;)Z

    move-result v0

    goto :goto_33
.end method

.method public a(LasU;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 75
    invoke-virtual {p0}, LWe;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 77
    instance-of v0, p1, LVU;

    if-nez v0, :cond_11

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected DocEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    move-object v0, p1

    .line 81
    check-cast v0, LVU;

    .line 82
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 84
    sget-object v3, Latf;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_39

    .line 85
    sget-object v2, Latf;->h:Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 86
    const-string v3, "next"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_39

    .line 87
    sget-object v2, Latf;->j:Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-virtual {v0, v1}, LVU;->a(Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x1

    .line 92
    :goto_38
    return v0

    :cond_39
    invoke-super {p0, p1}, Latd;->a(LasU;)Z

    move-result v0

    goto :goto_38
.end method
