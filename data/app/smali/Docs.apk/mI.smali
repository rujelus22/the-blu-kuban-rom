.class public LmI;
.super LJ;
.source "DocListCursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LJ",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/database/Cursor;

.field private final a:Ljava/lang/String;

.field private final a:Llf;

.field private final a:LmJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Llf;LmJ;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, LJ;-><init>(Landroid/content/Context;)V

    .line 34
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in DocListCursorLoader constructor docListQuery="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p0, LmI;->a:Llf;

    .line 36
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LmJ;

    iput-object v0, p0, LmI;->a:LmJ;

    .line 37
    iput-object p4, p0, LmI;->a:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public a()Landroid/database/Cursor;
    .registers 4

    .prologue
    .line 42
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in loadInBackground "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v0, p0, LmI;->a:Llf;

    iget-object v1, p0, LmI;->a:LmJ;

    invoke-virtual {v1}, LmJ;->a()Lnh;

    move-result-object v1

    iget-object v2, p0, LmI;->a:LmJ;

    invoke-virtual {v2}, LmJ;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Llf;->a(Lnh;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 18
    invoke-virtual {p0}, LmI;->a()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .registers 4

    .prologue
    .line 97
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onForceLoad "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-super {p0}, LJ;->a()V

    .line 99
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .registers 5
    .parameter

    .prologue
    .line 48
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in deliverResult "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-virtual {p0}, LmI;->d()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 51
    if-eqz p1, :cond_25

    .line 52
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 67
    :cond_25
    :goto_25
    return-void

    .line 57
    :cond_26
    iget-object v0, p0, LmI;->a:Landroid/database/Cursor;

    .line 58
    iput-object p1, p0, LmI;->a:Landroid/database/Cursor;

    .line 60
    invoke-virtual {p0}, LmI;->b()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 61
    invoke-super {p0, p1}, LJ;->b(Ljava/lang/Object;)V

    .line 64
    :cond_33
    if-eqz v0, :cond_25

    if-eq v0, p1, :cond_25

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_25

    .line 65
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_25
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 18
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, LmI;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .registers 5
    .parameter

    .prologue
    .line 89
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onCanceled "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    if-eqz p1, :cond_25

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_25

    .line 91
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 93
    :cond_25
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 18
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, LmI;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method protected d()V
    .registers 4

    .prologue
    .line 71
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onStartLoading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, LmI;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_23

    .line 73
    iget-object v0, p0, LmI;->a:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, LmI;->a(Landroid/database/Cursor;)V

    .line 76
    :cond_23
    invoke-virtual {p0}, LmI;->e()Z

    move-result v0

    if-nez v0, :cond_2d

    iget-object v0, p0, LmI;->a:Landroid/database/Cursor;

    if-nez v0, :cond_30

    .line 77
    :cond_2d
    invoke-virtual {p0}, LmI;->e()V

    .line 79
    :cond_30
    return-void
.end method

.method protected g()V
    .registers 4

    .prologue
    .line 83
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onStopLoading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-virtual {p0}, LmI;->a()Z

    .line 85
    return-void
.end method

.method protected k()V
    .registers 4

    .prologue
    .line 103
    const-string v0, "DocListCursorLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onReset "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-super {p0}, LJ;->k()V

    .line 106
    invoke-virtual {p0}, LmI;->g()V

    .line 108
    iget-object v0, p0, LmI;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_31

    iget-object v0, p0, LmI;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_31

    .line 109
    iget-object v0, p0, LmI;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 112
    :cond_31
    const/4 v0, 0x0

    iput-object v0, p0, LmI;->a:Landroid/database/Cursor;

    .line 113
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DocListCursorLoader [tagName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LmI;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", docListQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LmI;->a:LmJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
