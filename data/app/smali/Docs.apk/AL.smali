.class public abstract LAL;
.super LAw;
.source "TextBoxElement.java"


# direct methods
.method public constructor <init>(LDb;LAb;Lzd;Lxu;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "LAb;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, LAw;-><init>(LDb;LAb;Lzd;Lxu;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected a(I)I
    .registers 3
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, LAL;->a:LDb;

    invoke-virtual {v0}, LDb;->f()I

    move-result v0

    if-ge p1, v0, :cond_11

    invoke-virtual {p0}, LAL;->a()LDA;

    move-result-object v0

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    :goto_10
    return v0

    :cond_11
    invoke-virtual {p0}, LAL;->a()LDA;

    move-result-object v0

    invoke-virtual {v0}, LDA;->c()I

    move-result v0

    goto :goto_10
.end method

.method public a(IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0}, LAL;->a()LDA;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0, v1, v2}, LDA;->a(II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 59
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    invoke-interface {v0, p1, p2, p3}, LAx;->a(IILjava/util/List;)V

    goto :goto_10

    .line 61
    :cond_26
    return-void
.end method

.method public a(IILCh;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-virtual {p0}, LAL;->a()LDA;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LDA;->a(II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 46
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    .line 47
    invoke-interface {v0, p1, p2, p3}, LAx;->a(IILCh;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 48
    const/4 v0, 0x0

    .line 51
    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x1

    goto :goto_25
.end method

.method protected b(I)I
    .registers 3
    .parameter

    .prologue
    .line 73
    invoke-virtual {p0}, LAL;->a()LDA;

    move-result-object v0

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    if-ge p1, v0, :cond_11

    iget-object v0, p0, LAL;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    :goto_10
    return v0

    :cond_11
    iget-object v0, p0, LAL;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    goto :goto_10
.end method

.method public d(I)I
    .registers 3
    .parameter

    .prologue
    .line 35
    invoke-virtual {p0}, LAL;->a()LDA;

    move-result-object v0

    invoke-virtual {v0}, LDA;->c()I

    move-result v0

    if-ne p1, v0, :cond_13

    .line 38
    iget-object v0, p0, LAL;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 40
    :goto_12
    return v0

    :cond_13
    invoke-super {p0, p1}, LAw;->d(I)I

    move-result v0

    goto :goto_12
.end method
