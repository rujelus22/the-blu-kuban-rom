.class public Lss;
.super Ljava/lang/Object;
.source "ErrorNotificationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lss;->a:Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

    iput-object p2, p0, Lss;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lss;->a:Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:LdL;

    iget-object v1, p0, Lss;->a:Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 75
    new-instance v0, LZP;

    iget-object v1, p0, Lss;->a:Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

    const-string v2, "android_docs_editors"

    invoke-direct {v0, v1, v2}, LZP;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lss;->a:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 78
    const-string v1, "stack_trace"

    iget-object v2, p0, Lss;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LZP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_1d
    iget-object v1, p0, Lss;->a:Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:LZR;

    invoke-interface {v1, v0}, LZR;->a(LZP;)V

    .line 81
    iget-object v0, p0, Lss;->a:Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a(Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;Z)Z

    .line 82
    return-void
.end method
