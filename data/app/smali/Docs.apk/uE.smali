.class public LuE;
.super Ljava/lang/Object;
.source "FontPickerEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropertyType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/View$OnClickListener;

.field private a:Landroid/widget/RadioButton;

.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPropertyType;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPropertyType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, LuE;->a:Landroid/view/View$OnClickListener;

    .line 27
    iput-object v0, p0, LuE;->a:Landroid/widget/RadioButton;

    .line 36
    iput-object p1, p0, LuE;->a:Ljava/lang/Object;

    .line 37
    iput-object p2, p0, LuE;->a:Ljava/lang/String;

    .line 38
    return-void
.end method

.method static synthetic a(LuE;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, LuE;->a:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/widget/RadioButton;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, LuE;->a:Landroid/widget/RadioButton;

    return-object v0
.end method

.method public a(Landroid/app/Activity;Landroid/view/ViewGroup;)Landroid/widget/RadioButton;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LsF;->font_properties_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, LuE;->a:Landroid/widget/RadioButton;

    .line 77
    iget-object v0, p0, LuE;->a:Landroid/widget/RadioButton;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, LuE;->a:Landroid/widget/RadioButton;

    new-instance v1, LuF;

    invoke-direct {v1, p0}, LuF;-><init>(LuE;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, LuE;->a:Landroid/widget/RadioButton;

    iget-object v1, p0, LuE;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, LuE;->a:Landroid/widget/RadioButton;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TPropertyType;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, LuE;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, LuE;->a:Landroid/view/View$OnClickListener;

    .line 47
    return-void
.end method
