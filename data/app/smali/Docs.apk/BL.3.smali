.class abstract enum LBL;
.super Ljava/lang/Enum;
.source "HorizontalPositioning.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LBL;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LBL;

.field private static final synthetic a:[LBL;

.field public static final enum b:LBL;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, LBM;

    const-string v1, "TEXT_CENTER"

    invoke-direct {v0, v1, v2}, LBM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBL;->a:LBL;

    .line 23
    new-instance v0, LBN;

    const-string v1, "EDITOR_CENTER"

    invoke-direct {v0, v1, v3}, LBN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBL;->b:LBL;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [LBL;

    sget-object v1, LBL;->a:LBL;

    aput-object v1, v0, v2

    sget-object v1, LBL;->b:LBL;

    aput-object v1, v0, v3

    sput-object v0, LBL;->a:[LBL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILBM;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, LBL;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LBL;
    .registers 2
    .parameter

    .prologue
    .line 12
    const-class v0, LBL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LBL;

    return-object v0
.end method

.method public static values()[LBL;
    .registers 1

    .prologue
    .line 12
    sget-object v0, LBL;->a:[LBL;

    invoke-virtual {v0}, [LBL;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LBL;

    return-object v0
.end method


# virtual methods
.method abstract a(LBH;LKj;II)I
.end method
