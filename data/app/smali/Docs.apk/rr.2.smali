.class public final enum Lrr;
.super Ljava/lang/Enum;
.source "TimeRange.java"

# interfaces
.implements Lri;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lrr;",
        ">;",
        "Lri;"
    }
.end annotation


# static fields
.field public static final enum a:Lrr;

.field private static final synthetic a:[Lrr;

.field public static final enum b:Lrr;

.field public static final enum c:Lrr;

.field public static final enum d:Lrr;

.field public static final enum e:Lrr;

.field public static final enum f:Lrr;

.field public static final enum g:Lrr;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 15
    new-instance v0, Lrr;

    const-string v1, "SAME_AS_PREVIOUS"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lrr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrr;->a:Lrr;

    .line 16
    new-instance v0, Lrr;

    const-string v1, "TODAY"

    sget v2, Len;->today:I

    invoke-direct {v0, v1, v5, v2}, Lrr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrr;->b:Lrr;

    .line 17
    new-instance v0, Lrr;

    const-string v1, "YESTERDAY"

    sget v2, Len;->yesterday:I

    invoke-direct {v0, v1, v6, v2}, Lrr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrr;->c:Lrr;

    .line 18
    new-instance v0, Lrr;

    const-string v1, "THIS_WEEK"

    sget v2, Len;->this_week:I

    invoke-direct {v0, v1, v7, v2}, Lrr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrr;->d:Lrr;

    .line 19
    new-instance v0, Lrr;

    const-string v1, "THIS_MONTH"

    sget v2, Len;->this_month:I

    invoke-direct {v0, v1, v8, v2}, Lrr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrr;->e:Lrr;

    .line 20
    new-instance v0, Lrr;

    const-string v1, "THIS_YEAR"

    const/4 v2, 0x5

    sget v3, Len;->this_year:I

    invoke-direct {v0, v1, v2, v3}, Lrr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrr;->f:Lrr;

    .line 21
    new-instance v0, Lrr;

    const-string v1, "OLDER"

    const/4 v2, 0x6

    sget v3, Len;->older:I

    invoke-direct {v0, v1, v2, v3}, Lrr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrr;->g:Lrr;

    .line 14
    const/4 v0, 0x7

    new-array v0, v0, [Lrr;

    sget-object v1, Lrr;->a:Lrr;

    aput-object v1, v0, v4

    sget-object v1, Lrr;->b:Lrr;

    aput-object v1, v0, v5

    sget-object v1, Lrr;->c:Lrr;

    aput-object v1, v0, v6

    sget-object v1, Lrr;->d:Lrr;

    aput-object v1, v0, v7

    sget-object v1, Lrr;->e:Lrr;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lrr;->f:Lrr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lrr;->g:Lrr;

    aput-object v2, v0, v1

    sput-object v0, Lrr;->a:[Lrr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 106
    iput p3, p0, Lrr;->a:I

    .line 107
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lrr;
    .registers 2
    .parameter

    .prologue
    .line 14
    const-class v0, Lrr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lrr;

    return-object v0
.end method

.method public static values()[Lrr;
    .registers 1

    .prologue
    .line 14
    sget-object v0, Lrr;->a:[Lrr;

    invoke-virtual {v0}, [Lrr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lrr;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 111
    iget v0, p0, Lrr;->a:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 116
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 121
    iget v0, p0, Lrr;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method
