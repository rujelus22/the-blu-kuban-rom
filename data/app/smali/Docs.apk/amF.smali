.class public final LamF;
.super Ljava/lang/Object;
.source "Futures.java"


# static fields
.field private static final a:Lalw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lalw",
            "<",
            "Ljava/lang/reflect/Constructor",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1236
    invoke-static {}, Lalw;->b()Lalw;

    move-result-object v0

    new-instance v1, LamK;

    invoke-direct {v1}, LamK;-><init>()V

    invoke-virtual {v0, v1}, Lalw;->a(Lagl;)Lalw;

    move-result-object v0

    invoke-virtual {v0}, Lalw;->a()Lalw;

    move-result-object v0

    sput-object v0, LamF;->a:Lalw;

    return-void
.end method

.method public static a(LamQ;Lagl;)LamQ;
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LamQ",
            "<TI;>;",
            "Lagl",
            "<-TI;+TO;>;)",
            "LamQ",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 446
    invoke-static {}, LamU;->a()LamS;

    move-result-object v0

    invoke-static {p0, p1, v0}, LamF;->b(LamQ;Lagl;Ljava/util/concurrent/Executor;)LamQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LamQ;Lagl;Ljava/util/concurrent/Executor;)LamQ;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LamQ",
            "<TI;>;",
            "Lagl",
            "<-TI;+",
            "LamQ",
            "<+TO;>;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LamQ",
            "<TO;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 269
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    new-instance v0, LamL;

    new-instance v1, LamH;

    invoke-direct {v1, p1}, LamH;-><init>(Lagl;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, LamL;-><init>(LamA;LamQ;LamG;)V

    .line 282
    invoke-interface {p0, v0, p2}, LamQ;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 283
    return-object v0
.end method

.method public static a(LamQ;LamA;)LamQ;
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LamQ",
            "<TI;>;",
            "LamA",
            "<-TI;+TO;>;)",
            "LamQ",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 336
    invoke-static {}, LamU;->a()LamS;

    move-result-object v0

    invoke-static {p0, p1, v0}, LamF;->a(LamQ;LamA;Ljava/util/concurrent/Executor;)LamQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LamQ;LamA;Ljava/util/concurrent/Executor;)LamQ;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LamQ",
            "<TI;>;",
            "LamA",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LamQ",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 391
    new-instance v0, LamL;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, LamL;-><init>(LamA;LamQ;LamG;)V

    .line 393
    invoke-interface {p0, v0, p2}, LamQ;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 394
    return-object v0
.end method

.method public static a(Ljava/lang/Object;)LamQ;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(TV;)",
            "LamQ",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {}, LamZ;->a()LamZ;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p0}, LamZ;->a(Ljava/lang/Object;)Z

    .line 93
    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;)LamQ;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "LamQ",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {p0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    invoke-static {}, LamZ;->a()LamZ;

    move-result-object v0

    .line 131
    invoke-virtual {v0, p0}, LamZ;->a(Ljava/lang/Throwable;)Z

    .line 132
    return-object v0
.end method

.method public static varargs a([LamQ;)LamQ;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">([",
            "LamQ",
            "<+TV;>;)",
            "LamQ",
            "<",
            "Ljava/util/List",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 804
    new-instance v0, LamN;

    invoke-static {p0}, Laji;->a([Ljava/lang/Object;)Laji;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {}, LamU;->a()LamS;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LamN;-><init>(Laji;ZLjava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static a(LamQ;LamE;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LamQ",
            "<TV;>;",
            "LamE",
            "<-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 915
    invoke-static {}, LamU;->a()LamS;

    move-result-object v0

    invoke-static {p0, p1, v0}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    .line 916
    return-void
.end method

.method public static a(LamQ;LamE;Ljava/util/concurrent/Executor;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "LamQ",
            "<TV;>;",
            "LamE",
            "<-TV;>;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 967
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    new-instance v0, LamJ;

    invoke-direct {v0, p0, p1}, LamJ;-><init>(LamQ;LamE;)V

    .line 985
    invoke-interface {p0, v0, p2}, LamQ;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 986
    return-void
.end method

.method public static b(LamQ;Lagl;Ljava/util/concurrent/Executor;)LamQ;
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "LamQ",
            "<TI;>;",
            "Lagl",
            "<-TI;+TO;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LamQ",
            "<TO;>;"
        }
    .end annotation

    .prologue
    .line 499
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    new-instance v0, LamI;

    invoke-direct {v0, p1}, LamI;-><init>(Lagl;)V

    .line 507
    invoke-static {p0, v0, p2}, LamF;->a(LamQ;Lagl;Ljava/util/concurrent/Executor;)LamQ;

    move-result-object v0

    return-object v0
.end method
