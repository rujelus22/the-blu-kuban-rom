.class public LWf;
.super Ljava/lang/Object;
.source "DocsListGDataParserFactory.java"

# interfaces
.implements LasL;


# instance fields
.field private final a:Latg;


# direct methods
.method public constructor <init>(Latg;)V
    .registers 2
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, LWf;->a:Latg;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Latb;
    .registers 5
    .parameter

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, LWf;->a:Latg;

    invoke-interface {v0}, Latg;->a()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_5} :catch_c

    move-result-object v0

    .line 44
    new-instance v1, LWe;

    invoke-direct {v1, p1, v0}, LWe;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    return-object v1

    .line 41
    :catch_c
    move-exception v0

    .line 42
    new-instance v1, Latc;

    const-string v2, "Could not create XmlPullParser"

    invoke-direct {v1, v2, v0}, Latc;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/Class;Ljava/io/InputStream;)Latb;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 50
    const-class v0, LVV;

    if-eq p1, v0, :cond_8

    const-class v0, LVT;

    if-ne p1, v0, :cond_d

    .line 51
    :cond_8
    invoke-virtual {p0, p2}, LWf;->a(Ljava/io/InputStream;)Latb;

    move-result-object v0

    return-object v0

    .line 54
    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown entry class \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' specified."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LasT;)Lath;
    .registers 5
    .parameter

    .prologue
    .line 60
    instance-of v0, p1, LVT;

    if-eqz v0, :cond_e

    .line 61
    check-cast p1, LVT;

    .line 62
    new-instance v0, LWc;

    iget-object v1, p0, LWf;->a:Latg;

    invoke-direct {v0, v1, p1}, LWc;-><init>(Latg;LVT;)V

    .line 65
    :goto_d
    return-object v0

    .line 63
    :cond_e
    instance-of v0, p1, LVV;

    if-eqz v0, :cond_1c

    .line 64
    check-cast p1, LVV;

    .line 65
    new-instance v0, LWo;

    iget-object v1, p0, LWf;->a:Latg;

    invoke-direct {v0, v1, p1}, LWo;-><init>(Latg;LVV;)V

    goto :goto_d

    .line 68
    :cond_1c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected Entry class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/util/Enumeration;)Lath;
    .registers 4
    .parameter

    .prologue
    .line 74
    new-instance v0, Lati;

    iget-object v1, p0, LWf;->a:Latg;

    invoke-direct {v0, p0, v1, p1}, Lati;-><init>(LasL;Latg;Ljava/util/Enumeration;)V

    return-object v0
.end method
