.class public abstract LYL;
.super Ljava/lang/Object;
.source "GellyInjectorBuilderBase.java"


# instance fields
.field private final a:LYX;

.field private a:Laoo;

.field private a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LYY;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LYO;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Laov;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Laop",
            "<*>;",
            "LZb",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Laou",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LaoC;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Laop",
            "<*>;",
            "LaoC;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lari;",
            ">;",
            "Ljava/util/Collection",
            "<+",
            "Lari;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Laov;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LYL;->a:Ljava/util/Map;

    .line 251
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LYL;->b:Ljava/util/Map;

    .line 257
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LYL;->c:Ljava/util/Map;

    .line 260
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LYL;->d:Ljava/util/Map;

    .line 263
    new-instance v0, LYX;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LYX;-><init>(LYM;)V

    iput-object v0, p0, LYL;->a:LYX;

    .line 265
    const-class v0, LYO;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LYL;->a:Ljava/util/EnumSet;

    .line 267
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LYL;->e:Ljava/util/Map;

    .line 274
    invoke-static {p1}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LYL;->a:Ljava/util/List;

    .line 275
    return-void
.end method

.method private a(Laop;)LZb;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<*>;)",
            "LZb",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 421
    iget-object v0, p0, LYL;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZb;

    .line 422
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find static provider for key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    return-object v0
.end method

.method private a(Ljava/lang/Class;)LaoC;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LaoC;"
        }
    .end annotation

    .prologue
    .line 535
    iget-object v0, p0, LYL;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaoC;

    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find scope instance for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scope annotation."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    return-object v0
.end method

.method private a(Ljava/lang/Class;)Ljava/util/Collection;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lari;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 517
    iget-object v0, p0, LYL;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic a(LYL;)Ljava/util/EnumSet;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, LYL;->a:Ljava/util/EnumSet;

    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    .line 298
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 299
    new-instance v1, LYN;

    iget-object v2, p0, LYL;->a:LYX;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LYN;-><init>(Laoz;LYM;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 300
    iget-object v1, p0, LYL;->a:Ljava/util/List;

    invoke-static {v1}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 302
    iget-object v1, p0, LYL;->e:Ljava/util/Map;

    const-class v2, Laof;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    iget-object v1, p0, LYL;->e:Ljava/util/Map;

    const-class v2, Lary;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    iget-object v1, p0, LYL;->e:Ljava/util/Map;

    const-class v2, LarE;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v1, p0, LYL;->e:Ljava/util/Map;

    const-class v2, LarL;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v1, p0, LYL;->e:Ljava/util/Map;

    const-class v2, Larx;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    iget-object v1, p0, LYL;->e:Ljava/util/Map;

    const-class v2, LarG;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v1, p0, LYL;->e:Ljava/util/Map;

    const-class v2, LarA;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    invoke-static {v0}, Larj;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_75
    :goto_75
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lari;

    .line 311
    instance-of v2, v0, Laof;

    if-eqz v2, :cond_9e

    .line 312
    const-class v2, Laof;

    invoke-direct {p0, v2, v0}, LYL;->a(Ljava/lang/Class;Lari;)V

    .line 314
    instance-of v2, v0, Lary;

    if-eqz v2, :cond_94

    .line 315
    const-class v2, Lary;

    invoke-direct {p0, v2, v0}, LYL;->a(Ljava/lang/Class;Lari;)V

    goto :goto_75

    .line 316
    :cond_94
    instance-of v2, v0, LarE;

    if-eqz v2, :cond_75

    .line 317
    const-class v2, LarE;

    invoke-direct {p0, v2, v0}, LYL;->a(Ljava/lang/Class;Lari;)V

    goto :goto_75

    .line 319
    :cond_9e
    instance-of v2, v0, LarL;

    if-eqz v2, :cond_a8

    .line 320
    const-class v2, LarL;

    invoke-direct {p0, v2, v0}, LYL;->a(Ljava/lang/Class;Lari;)V

    goto :goto_75

    .line 321
    :cond_a8
    instance-of v2, v0, Larx;

    if-eqz v2, :cond_b2

    .line 322
    const-class v2, Larx;

    invoke-direct {p0, v2, v0}, LYL;->a(Ljava/lang/Class;Lari;)V

    goto :goto_75

    .line 323
    :cond_b2
    instance-of v2, v0, LarG;

    if-eqz v2, :cond_bc

    .line 324
    const-class v2, LarG;

    invoke-direct {p0, v2, v0}, LYL;->a(Ljava/lang/Class;Lari;)V

    goto :goto_75

    .line 325
    :cond_bc
    instance-of v2, v0, LarA;

    if-eqz v2, :cond_75

    .line 326
    const-class v2, LarA;

    invoke-direct {p0, v2, v0}, LYL;->a(Ljava/lang/Class;Lari;)V

    goto :goto_75

    .line 329
    :cond_c6
    return-void
.end method

.method static synthetic a(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->a()V

    return-void
.end method

.method private a(LarA;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LarA",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 505
    iget-object v0, p0, LYL;->a:Laoo;

    invoke-virtual {p1}, LarA;->a()LaoL;

    move-result-object v1

    invoke-interface {v0, v1}, Laoo;->a(LaoL;)Laou;

    move-result-object v0

    .line 507
    invoke-virtual {p1, v0}, LarA;->a(Laou;)V

    .line 508
    return-void
.end method

.method private a(LarE;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LarE",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 384
    invoke-interface {p1}, LarE;->a()Laop;

    move-result-object v0

    .line 386
    invoke-direct {p0, v0}, LYL;->a(Laop;)LZb;

    move-result-object v0

    .line 387
    invoke-interface {p1}, LarE;->b()Laoz;

    move-result-object v1

    .line 388
    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 389
    return-void
.end method

.method private a(LarG;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LarG",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 499
    iget-object v0, p0, LYL;->a:Laoo;

    invoke-virtual {p1}, LarG;->a()Laop;

    move-result-object v1

    invoke-interface {v0, v1}, Laoo;->a(Laop;)Laoz;

    move-result-object v0

    .line 500
    invoke-virtual {p1, v0}, LarG;->a(Laoz;)V

    .line 501
    return-void
.end method

.method private a(Larx;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Larx",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 511
    invoke-virtual {p1}, Larx;->b()Ljava/lang/Object;

    move-result-object v0

    .line 512
    iget-object v1, p0, LYL;->a:Laoo;

    invoke-interface {v1, v0}, Laoo;->a(Ljava/lang/Object;)V

    .line 513
    return-void
.end method

.method private a(Lary;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lary",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 375
    invoke-interface {p1}, Lary;->a()Laop;

    move-result-object v0

    .line 377
    invoke-direct {p0, v0}, LYL;->a(Laop;)LZb;

    move-result-object v0

    .line 378
    invoke-interface {p1}, Lary;->b()Ljava/lang/Object;

    move-result-object v1

    .line 379
    invoke-static {v1}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 380
    return-void
.end method

.method private a(Ljava/lang/Class;Lari;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lari;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lari;",
            ")V"
        }
    .end annotation

    .prologue
    .line 332
    invoke-direct {p0, p1}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    .line 335
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 336
    return-void
.end method

.method private b()V
    .registers 5

    .prologue
    .line 342
    const-class v0, LarL;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarL;

    .line 343
    iget-object v2, p0, LYL;->c:Ljava/util/Map;

    invoke-virtual {v0}, LarL;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0}, LarL;->a()LaoC;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 345
    :cond_24
    return-void
.end method

.method static synthetic b(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->b()V

    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    .line 348
    invoke-virtual {p0}, LYL;->a()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, LYL;->a:Ljava/util/Collection;

    .line 349
    iget-object v0, p0, LYL;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYY;

    .line 350
    invoke-virtual {v0}, LYY;->a()V

    goto :goto_c

    .line 352
    :cond_1c
    return-void
.end method

.method static synthetic c(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->c()V

    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 364
    const-class v0, Lary;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lary;

    .line 365
    invoke-direct {p0, v0}, LYL;->a(Lary;)V

    goto :goto_a

    .line 369
    :cond_1a
    const-class v0, LarE;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarE;

    .line 370
    invoke-direct {p0, v0}, LYL;->a(LarE;)V

    goto :goto_24

    .line 372
    :cond_34
    return-void
.end method

.method static synthetic d(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->d()V

    return-void
.end method

.method private e()V
    .registers 6

    .prologue
    .line 395
    const-class v0, Laof;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laof;

    .line 396
    invoke-static {v0}, LZc;->a(Laof;)LaoC;

    move-result-object v1

    .line 397
    if-nez v1, :cond_4a

    .line 398
    invoke-static {v0}, LZc;->a(Laof;)Ljava/lang/Class;

    move-result-object v1

    .line 399
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Binding "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Laof;->a()Laop;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has no scope instance nor scope binding. An unscoped binding should have "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Scopes.NO_SCOPE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    invoke-direct {p0, v1}, LYL;->a(Ljava/lang/Class;)LaoC;

    move-result-object v1

    .line 405
    :cond_4a
    sget-object v3, LaoE;->b:LaoC;

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 407
    iget-object v3, p0, LYL;->d:Ljava/util/Map;

    invoke-interface {v0}, Laof;->a()Laop;

    move-result-object v0

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 411
    :cond_5c
    iget-object v0, p0, LYL;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_66
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_86

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 412
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laop;

    .line 413
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaoC;

    .line 415
    invoke-direct {p0, v1}, LYL;->a(Laop;)LZb;

    move-result-object v1

    .line 416
    invoke-virtual {v1, v0}, LZb;->a(LaoC;)V

    goto :goto_66

    .line 418
    :cond_86
    return-void
.end method

.method static synthetic e(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->e()V

    return-void
.end method

.method private f()V
    .registers 6

    .prologue
    .line 431
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 432
    const-class v0, Laof;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laof;

    .line 433
    invoke-interface {v0}, Laof;->a()Laop;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .line 436
    :cond_23
    new-instance v0, LYJ;

    iget-object v2, p0, LYL;->a:Ljava/util/Map;

    iget-object v3, p0, LYL;->b:Ljava/util/Map;

    iget-object v4, p0, LYL;->c:Ljava/util/Map;

    invoke-direct {v0, v2, v3, v4, v1}, LYJ;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    iput-object v0, p0, LYL;->a:Laoo;

    .line 438
    iget-object v0, p0, LYL;->a:LYX;

    iget-object v1, p0, LYL;->a:Laoo;

    invoke-virtual {v0, v1}, LYX;->a(Laoo;)V

    .line 439
    return-void
.end method

.method static synthetic f(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->f()V

    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 454
    const-class v0, LarE;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarE;

    .line 455
    iget-object v2, p0, LYL;->a:Laoo;

    invoke-interface {v0}, LarE;->b()Laoz;

    move-result-object v0

    invoke-interface {v2, v0}, Laoo;->a(Ljava/lang/Object;)V

    goto :goto_a

    .line 458
    :cond_20
    iget-object v0, p0, LYL;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_26
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYY;

    .line 459
    invoke-virtual {v0}, LYY;->b()V

    goto :goto_26

    .line 470
    :cond_36
    const-class v0, Lary;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_40
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lary;

    .line 471
    iget-object v2, p0, LYL;->a:Laoo;

    invoke-interface {v0}, Lary;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Laoo;->a(Ljava/lang/Object;)V

    goto :goto_40

    .line 474
    :cond_56
    const-class v0, Larx;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_60
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larx;

    .line 475
    iget-object v2, p0, LYL;->a:Laoo;

    invoke-interface {v2, v0}, Laoo;->a(Ljava/lang/Object;)V

    goto :goto_60

    .line 477
    :cond_72
    return-void
.end method

.method static synthetic g(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->g()V

    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 484
    const-class v0, LarG;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarG;

    .line 485
    invoke-direct {p0, v0}, LYL;->a(LarG;)V

    goto :goto_a

    .line 489
    :cond_1a
    const-class v0, LarA;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarA;

    .line 490
    invoke-direct {p0, v0}, LYL;->a(LarA;)V

    goto :goto_24

    .line 493
    :cond_34
    const-class v0, Larx;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larx;

    .line 494
    invoke-direct {p0, v0}, LYL;->a(Larx;)V

    goto :goto_3e

    .line 496
    :cond_4e
    return-void
.end method

.method static synthetic h(LYL;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, LYL;->h()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()Laoo;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 281
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, LYL;->a:Laoo;

    if-eqz v1, :cond_a

    .line 282
    iget-object v0, p0, LYL;->a:Laoo;
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_2c

    .line 291
    :goto_8
    monitor-exit p0

    return-object v0

    .line 284
    :cond_a
    :try_start_a
    iget-object v1, p0, LYL;->a:Ljava/util/EnumSet;

    invoke-virtual {v1}, Ljava/util/EnumSet;->size()I

    move-result v1

    if-nez v1, :cond_27

    const/4 v1, 0x1

    :goto_13
    const-string v2, "This builder is no longer usable. Please create a new builder."

    invoke-static {v1, v2}, Lagu;->b(ZLjava/lang/Object;)V

    .line 287
    invoke-static {}, LYO;->values()[LYO;

    move-result-object v1

    array-length v2, v1

    :goto_1d
    if-ge v0, v2, :cond_29

    aget-object v3, v1, v0

    .line 288
    invoke-virtual {v3, p0}, LYO;->a(LYL;)V

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    :cond_27
    move v1, v0

    .line 284
    goto :goto_13

    .line 291
    :cond_29
    iget-object v0, p0, LYL;->a:Laoo;
    :try_end_2b
    .catchall {:try_start_a .. :try_end_2b} :catchall_2c

    goto :goto_8

    .line 281
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LYY;",
            ">;"
        }
    .end annotation
.end method

.method protected a(Laop;LZb;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "LZb",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 524
    iget-object v0, p0, LYL;->a:Laoo;

    if-nez v0, :cond_1f

    const/4 v0, 0x1

    :goto_5
    const-string v1, "No one should be registering additional providers after the injector has been initialized."

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 527
    iget-object v0, p0, LYL;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    invoke-virtual {p2}, LZb;->a()Ljava/lang/Class;

    move-result-object v0

    .line 529
    if-eqz v0, :cond_1e

    .line 530
    iget-object v1, p0, LYL;->d:Ljava/util/Map;

    invoke-direct {p0, v0}, LYL;->a(Ljava/lang/Class;)LaoC;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    :cond_1e
    return-void

    .line 524
    :cond_1f
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected a(Ljava/lang/Class;Laou;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Laou",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 545
    iget-object v0, p0, LYL;->a:Laoo;

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_5
    const-string v1, "No one should be registering additional member injectors after the injector has been initialized."

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 548
    iget-object v0, p0, LYL;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    return-void

    .line 545
    :cond_10
    const/4 v0, 0x0

    goto :goto_5
.end method
