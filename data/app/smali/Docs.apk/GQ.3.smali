.class public LGQ;
.super Ljava/lang/Object;
.source "LayoutedString.java"


# static fields
.field static a:I


# instance fields
.field private final a:Landroid/text/TextPaint;

.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final a:[I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput v0, LGQ;->a:I

    return-void
.end method

.method public constructor <init>(LJf;D)V
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LGQ;->a:Landroid/util/SparseArray;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LGQ;->a:Ljava/util/HashMap;

    .line 29
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, LGQ;->a:Landroid/text/TextPaint;

    .line 42
    invoke-virtual {p1}, LJf;->a()Ljava/lang/String;

    move-result-object v0

    .line 43
    iget-object v1, p0, LGQ;->a:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v8

    .line 44
    invoke-virtual {p1}, LJf;->a()LIU;

    move-result-object v1

    invoke-virtual {p1}, LJf;->a()LJc;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-wide v5, p2

    invoke-static/range {v0 .. v6}, LHQ;->a(Ljava/lang/String;LIU;LJc;ZLIT;D)LEW;

    move-result-object v9

    .line 50
    invoke-virtual {v9}, LEW;->c()I

    move-result v1

    iput v1, p0, LGQ;->b:I

    .line 51
    invoke-virtual {v9}, LEW;->d()I

    move-result v1

    mul-int/lit8 v1, v1, 0x5

    add-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, LGQ;->a:[I

    .line 52
    iget-object v1, p0, LGQ;->a:[I

    const/4 v2, 0x0

    invoke-virtual {v9}, LEW;->d()I

    move-result v3

    aput v3, v1, v2

    .line 53
    const/4 v2, 0x1

    .line 54
    const/4 v1, 0x0

    move v7, v1

    move v1, v2

    :goto_50
    invoke-virtual {v9}, LEW;->d()I

    move-result v2

    if-ge v7, v2, :cond_8b

    .line 55
    invoke-virtual {v9, v7}, LEW;->n(I)I

    move-result v6

    .line 56
    invoke-virtual {v9, v7}, LEW;->h(I)I

    move-result v10

    .line 58
    const/4 v5, 0x0

    .line 59
    iget-object v2, p0, LGQ;->a:[I

    add-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    aput v4, v2, v1

    .line 60
    iget-object v1, p0, LGQ;->a:[I

    add-int/lit8 v2, v3, 0x1

    const/4 v4, 0x0

    aput v4, v1, v3

    .line 61
    iget-object v1, p0, LGQ;->a:[I

    add-int/lit8 v4, v2, 0x1

    iget v3, v8, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v11, v8, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v3, v11

    mul-int/2addr v3, v7

    aput v3, v1, v2

    .line 62
    iget-object v2, p0, LGQ;->a:Landroid/text/TextPaint;

    iget-object v3, p0, LGQ;->a:[I

    invoke-virtual {v0, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, LGQ;->a(Landroid/text/TextPaint;[IIILjava/lang/String;)I

    move-result v2

    .line 54
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v1, v2

    goto :goto_50

    .line 64
    :cond_8b
    return-void
.end method

.method private a(Landroid/text/TextPaint;[IIILjava/lang/String;)I
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, LGQ;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 122
    if-nez v0, :cond_2c

    .line 123
    iget-object v0, p0, LGQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 124
    iget-object v2, p0, LGQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1, p5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 125
    iget-object v1, p0, LGQ;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    :cond_2c
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, p2, p3

    .line 129
    add-int/lit8 v0, p3, 0x1

    aput p4, p2, v0

    .line 131
    add-int/lit8 v0, p3, 0x2

    .line 132
    return v0
.end method


# virtual methods
.method public a()[I
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, LGQ;->a:[I

    return-object v0
.end method
