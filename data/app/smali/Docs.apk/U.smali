.class public final enum LU;
.super Ljava/lang/Enum;
.source "ModernAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LU;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LU;

.field private static final synthetic a:[LU;

.field public static final enum b:LU;

.field public static final enum c:LU;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, LU;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, LU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU;->a:LU;

    .line 97
    new-instance v0, LU;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, LU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU;->b:LU;

    .line 101
    new-instance v0, LU;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, LU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LU;->c:LU;

    .line 89
    const/4 v0, 0x3

    new-array v0, v0, [LU;

    sget-object v1, LU;->a:LU;

    aput-object v1, v0, v2

    sget-object v1, LU;->b:LU;

    aput-object v1, v0, v3

    sget-object v1, LU;->c:LU;

    aput-object v1, v0, v4

    sput-object v0, LU;->a:[LU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LU;
    .registers 2
    .parameter

    .prologue
    .line 89
    const-class v0, LU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LU;

    return-object v0
.end method

.method public static values()[LU;
    .registers 1

    .prologue
    .line 89
    sget-object v0, LU;->a:[LU;

    invoke-virtual {v0}, [LU;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LU;

    return-object v0
.end method
