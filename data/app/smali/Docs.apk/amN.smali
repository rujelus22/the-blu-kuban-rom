.class LamN;
.super Lamx;
.source "Futures.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lamx",
        "<",
        "Ljava/util/List",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field a:Laji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laji",
            "<+",
            "LamQ",
            "<+TV;>;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation
.end field

.field final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field final a:Z


# direct methods
.method constructor <init>(Laji;ZLjava/util/concurrent/Executor;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laji",
            "<+",
            "LamQ",
            "<+TV;>;>;Z",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1293
    invoke-direct {p0}, Lamx;-><init>()V

    .line 1294
    iput-object p1, p0, LamN;->a:Laji;

    .line 1295
    invoke-virtual {p1}, Laji;->size()I

    move-result v0

    invoke-static {v0}, LajX;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LamN;->a:Ljava/util/List;

    .line 1296
    iput-boolean p2, p0, LamN;->a:Z

    .line 1297
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Laji;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LamN;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1299
    invoke-direct {p0, p3}, LamN;->a(Ljava/util/concurrent/Executor;)V

    .line 1300
    return-void
.end method

.method private a(ILjava/util/concurrent/Future;)V
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/Future",
            "<+TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1353
    iget-object v2, p0, LamN;->a:Ljava/util/List;

    .line 1354
    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v3

    if-nez v3, :cond_c

    if-nez v2, :cond_14

    .line 1358
    :cond_c
    iget-boolean v0, p0, LamN;->a:Z

    const-string v1, "Future was done before all dependencies completed"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1400
    :cond_13
    :goto_13
    return-void

    .line 1364
    :cond_14
    :try_start_14
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v3

    const-string v4, "Tried to set value from future which is not done"

    invoke-static {v3, v4}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1366
    invoke-static {p2}, Lanb;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_24
    .catchall {:try_start_14 .. :try_end_24} :catchall_104
    .catch Ljava/util/concurrent/CancellationException; {:try_start_14 .. :try_end_24} :catch_49
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_14 .. :try_end_24} :catch_77
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_24} :catch_aa
    .catch Ljava/lang/Error; {:try_start_14 .. :try_end_24} :catch_d9

    .line 1389
    iget-object v2, p0, LamN;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    .line 1390
    if-ltz v2, :cond_3f

    :goto_2c
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1391
    if-nez v2, :cond_13

    .line 1392
    iget-object v0, p0, LamN;->a:Ljava/util/List;

    .line 1393
    if-eqz v0, :cond_41

    .line 1394
    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, LamN;->a(Ljava/lang/Object;)Z

    goto :goto_13

    :cond_3f
    move v0, v1

    .line 1390
    goto :goto_2c

    .line 1396
    :cond_41
    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto :goto_13

    .line 1367
    :catch_49
    move-exception v2

    .line 1368
    :try_start_4a
    iget-boolean v2, p0, LamN;->a:Z

    if-eqz v2, :cond_52

    .line 1373
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, LamN;->cancel(Z)Z
    :try_end_52
    .catchall {:try_start_4a .. :try_end_52} :catchall_104

    .line 1389
    :cond_52
    iget-object v2, p0, LamN;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    .line 1390
    if-ltz v2, :cond_6d

    :goto_5a
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1391
    if-nez v2, :cond_13

    .line 1392
    iget-object v0, p0, LamN;->a:Ljava/util/List;

    .line 1393
    if-eqz v0, :cond_6f

    .line 1394
    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, LamN;->a(Ljava/lang/Object;)Z

    goto :goto_13

    :cond_6d
    move v0, v1

    .line 1390
    goto :goto_5a

    .line 1396
    :cond_6f
    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto :goto_13

    .line 1375
    :catch_77
    move-exception v2

    .line 1376
    :try_start_78
    iget-boolean v3, p0, LamN;->a:Z

    if-eqz v3, :cond_83

    .line 1379
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {p0, v2}, LamN;->a(Ljava/lang/Throwable;)Z
    :try_end_83
    .catchall {:try_start_78 .. :try_end_83} :catchall_104

    .line 1389
    :cond_83
    iget-object v2, p0, LamN;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    .line 1390
    if-ltz v2, :cond_9f

    :goto_8b
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1391
    if-nez v2, :cond_13

    .line 1392
    iget-object v0, p0, LamN;->a:Ljava/util/List;

    .line 1393
    if-eqz v0, :cond_a1

    .line 1394
    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, LamN;->a(Ljava/lang/Object;)Z

    goto/16 :goto_13

    :cond_9f
    move v0, v1

    .line 1390
    goto :goto_8b

    .line 1396
    :cond_a1
    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto/16 :goto_13

    .line 1381
    :catch_aa
    move-exception v2

    .line 1382
    :try_start_ab
    iget-boolean v3, p0, LamN;->a:Z

    if-eqz v3, :cond_b2

    .line 1383
    invoke-virtual {p0, v2}, LamN;->a(Ljava/lang/Throwable;)Z
    :try_end_b2
    .catchall {:try_start_ab .. :try_end_b2} :catchall_104

    .line 1389
    :cond_b2
    iget-object v2, p0, LamN;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    .line 1390
    if-ltz v2, :cond_ce

    :goto_ba
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1391
    if-nez v2, :cond_13

    .line 1392
    iget-object v0, p0, LamN;->a:Ljava/util/List;

    .line 1393
    if-eqz v0, :cond_d0

    .line 1394
    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, LamN;->a(Ljava/lang/Object;)Z

    goto/16 :goto_13

    :cond_ce
    move v0, v1

    .line 1390
    goto :goto_ba

    .line 1396
    :cond_d0
    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto/16 :goto_13

    .line 1385
    :catch_d9
    move-exception v2

    .line 1387
    :try_start_da
    invoke-virtual {p0, v2}, LamN;->a(Ljava/lang/Throwable;)Z
    :try_end_dd
    .catchall {:try_start_da .. :try_end_dd} :catchall_104

    .line 1389
    iget-object v2, p0, LamN;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    .line 1390
    if-ltz v2, :cond_f9

    :goto_e5
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1391
    if-nez v2, :cond_13

    .line 1392
    iget-object v0, p0, LamN;->a:Ljava/util/List;

    .line 1393
    if-eqz v0, :cond_fb

    .line 1394
    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, LamN;->a(Ljava/lang/Object;)Z

    goto/16 :goto_13

    :cond_f9
    move v0, v1

    .line 1390
    goto :goto_e5

    .line 1396
    :cond_fb
    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto/16 :goto_13

    .line 1389
    :catchall_104
    move-exception v2

    iget-object v3, p0, LamN;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v3

    .line 1390
    if-ltz v3, :cond_120

    :goto_10d
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 1391
    if-nez v3, :cond_11f

    .line 1392
    iget-object v0, p0, LamN;->a:Ljava/util/List;

    .line 1393
    if-eqz v0, :cond_122

    .line 1394
    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, LamN;->a(Ljava/lang/Object;)Z

    .line 1399
    :cond_11f
    :goto_11f
    throw v2

    :cond_120
    move v0, v1

    .line 1390
    goto :goto_10d

    .line 1396
    :cond_122
    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto :goto_11f
.end method

.method static synthetic a(LamN;ILjava/util/concurrent/Future;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1276
    invoke-direct {p0, p1, p2}, LamN;->a(ILjava/util/concurrent/Future;)V

    return-void
.end method

.method private a(Ljava/util/concurrent/Executor;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1304
    new-instance v0, LamO;

    invoke-direct {v0, p0}, LamO;-><init>(LamN;)V

    invoke-static {}, LamU;->a()LamS;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, LamN;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1319
    iget-object v0, p0, LamN;->a:Laji;

    invoke-virtual {v0}, Laji;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1320
    iget-object v0, p0, LamN;->a:Ljava/util/List;

    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, LamN;->a(Ljava/lang/Object;)Z

    .line 1347
    :cond_1e
    return-void

    :cond_1f
    move v0, v1

    .line 1325
    :goto_20
    iget-object v2, p0, LamN;->a:Laji;

    invoke-virtual {v2}, Laji;->size()I

    move-result v2

    if-ge v0, v2, :cond_31

    .line 1326
    iget-object v2, p0, LamN;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1325
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 1336
    :cond_31
    iget-object v2, p0, LamN;->a:Laji;

    .line 1337
    :goto_33
    invoke-virtual {v2}, Laji;->size()I

    move-result v0

    if-ge v1, v0, :cond_1e

    .line 1338
    invoke-virtual {v2, v1}, Laji;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamQ;

    .line 1340
    new-instance v3, LamP;

    invoke-direct {v3, p0, v1, v0}, LamP;-><init>(LamN;ILamQ;)V

    invoke-interface {v0, v3, p1}, LamQ;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1337
    add-int/lit8 v1, v1, 0x1

    goto :goto_33
.end method

.method private b()V
    .registers 4

    .prologue
    .line 1417
    iget-object v0, p0, LamN;->a:Laji;

    .line 1418
    if-eqz v0, :cond_2d

    invoke-virtual {p0}, LamN;->isDone()Z

    move-result v1

    if-nez v1, :cond_2d

    .line 1419
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamQ;

    .line 1423
    :cond_1a
    :goto_1a
    invoke-interface {v0}, LamQ;->isDone()Z

    move-result v2

    if-nez v2, :cond_e

    .line 1425
    :try_start_20
    invoke-interface {v0}, LamQ;->get()Ljava/lang/Object;
    :try_end_23
    .catch Ljava/lang/Error; {:try_start_20 .. :try_end_23} :catch_24
    .catch Ljava/lang/InterruptedException; {:try_start_20 .. :try_end_23} :catch_26
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_23} :catch_28

    goto :goto_1a

    .line 1426
    :catch_24
    move-exception v0

    .line 1427
    throw v0

    .line 1428
    :catch_26
    move-exception v0

    .line 1429
    throw v0

    .line 1430
    :catch_28
    move-exception v2

    .line 1432
    iget-boolean v2, p0, LamN;->a:Z

    if-eqz v2, :cond_1a

    .line 1441
    :cond_2d
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1404
    invoke-direct {p0}, LamN;->b()V

    .line 1408
    invoke-super {p0}, Lamx;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1276
    invoke-virtual {p0}, LamN;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
