.class public Labr;
.super Ljava/lang/Object;
.source "Views.java"


# direct methods
.method public static a(FII)Landroid/util/Pair;
    .registers 10
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FII)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    const/high16 v6, 0x4000

    const-wide/high16 v4, 0x3fe0

    .line 86
    .line 89
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-eqz v1, :cond_56

    .line 90
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 91
    int-to-float v2, v1

    div-float/2addr v2, p0

    float-to-double v2, v2

    add-double/2addr v2, v4

    double-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 94
    :goto_1e
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-eqz v2, :cond_35

    .line 95
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 96
    int-to-float v2, v0

    mul-float/2addr v2, p0

    float-to-double v2, v2

    add-double/2addr v2, v4

    double-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 99
    :cond_35
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-ne v2, v6, :cond_3f

    .line 100
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 103
    :cond_3f
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-ne v2, v6, :cond_49

    .line 104
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 107
    :cond_49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 108
    return-object v0

    :cond_56
    move v1, v0

    goto :goto_1e
.end method

.method public static a(Landroid/view/View;)Landroid/view/ViewGroup;
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 53
    if-nez v0, :cond_35

    .line 55
    invoke-virtual {p0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, p0

    .line 59
    :goto_b
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    move-object v4, v3

    .line 60
    :goto_10
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_31

    .line 61
    check-cast v0, Landroid/view/ViewGroup;

    .line 64
    const/4 v2, 0x0

    move v5, v2

    :goto_18
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v5, v2, :cond_32

    .line 65
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 66
    invoke-virtual {v2, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_2d

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    :goto_2b
    move-object v4, v2

    .line 73
    goto :goto_10

    .line 64
    :cond_2d
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_18

    .line 75
    :cond_31
    return-object v4

    :cond_32
    move-object v0, v3

    move-object v2, v4

    goto :goto_2b

    :cond_35
    move-object v1, v0

    goto :goto_b
.end method

.method public static a(Landroid/view/View;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 39
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 40
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v1, v2, v3, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 41
    return-void
.end method

.method public static a(Landroid/widget/EditText;Landroid/app/Dialog;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 121
    new-instance v0, Labs;

    invoke-direct {v0, p1}, Labs;-><init>(Landroid/app/Dialog;)V

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 143
    return-void
.end method
