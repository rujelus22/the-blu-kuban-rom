.class public LMw;
.super Ljava/lang/Object;
.source "GestureDetector.java"


# static fields
.field private static final f:I

.field private static final g:I

.field private static final h:I


# instance fields
.field private a:F

.field private a:I

.field private a:LMy;

.field private final a:LMz;

.field private final a:Landroid/os/Handler;

.field private a:Landroid/view/MotionEvent;

.field private a:Landroid/view/VelocityTracker;

.field private a:Z

.field private b:F

.field private b:I

.field private b:Landroid/view/MotionEvent;

.field private b:Z

.field private c:F

.field private c:I

.field private c:Z

.field private d:F

.field private d:I

.field private d:Z

.field private e:I

.field private e:Z

.field private f:Z

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 215
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, LMw;->f:I

    .line 216
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, LMw;->g:I

    .line 217
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, LMw;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LMz;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 338
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LMw;-><init>(Landroid/content/Context;LMz;Landroid/os/Handler;)V

    .line 339
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LMz;Landroid/os/Handler;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_24

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, LMw;->g:Z

    .line 356
    if-eqz p3, :cond_26

    .line 357
    new-instance v0, LMx;

    invoke-direct {v0, p0, p3}, LMx;-><init>(LMw;Landroid/os/Handler;)V

    iput-object v0, p0, LMw;->a:Landroid/os/Handler;

    .line 361
    :goto_15
    iput-object p2, p0, LMw;->a:LMz;

    .line 362
    instance-of v0, p2, LMy;

    if-eqz v0, :cond_20

    .line 363
    check-cast p2, LMy;

    invoke-virtual {p0, p2}, LMw;->a(LMy;)V

    .line 365
    :cond_20
    invoke-direct {p0, p1}, LMw;->a(Landroid/content/Context;)V

    .line 366
    return-void

    .line 354
    :cond_24
    const/4 v0, 0x0

    goto :goto_a

    .line 359
    :cond_26
    new-instance v0, LMx;

    invoke-direct {v0, p0}, LMx;-><init>(LMw;)V

    iput-object v0, p0, LMw;->a:Landroid/os/Handler;

    goto :goto_15
.end method

.method static synthetic a(LMw;)LMy;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LMw;->a:LMy;

    return-object v0
.end method

.method static synthetic a(LMw;)LMz;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LMw;->a:LMz;

    return-object v0
.end method

.method static synthetic a(LMw;)Landroid/view/MotionEvent;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LMw;->a:Landroid/view/MotionEvent;

    return-object v0
.end method

.method private a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 617
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 618
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 619
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 620
    iget-object v0, p0, LMw;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 621
    const/4 v0, 0x0

    iput-object v0, p0, LMw;->a:Landroid/view/VelocityTracker;

    .line 622
    iput-boolean v2, p0, LMw;->e:Z

    .line 623
    iput-boolean v2, p0, LMw;->a:Z

    .line 624
    iput-boolean v2, p0, LMw;->c:Z

    .line 625
    iput-boolean v2, p0, LMw;->d:Z

    .line 626
    iget-boolean v0, p0, LMw;->b:Z

    if-eqz v0, :cond_29

    .line 627
    iput-boolean v2, p0, LMw;->b:Z

    .line 629
    :cond_29
    return-void
.end method

.method static synthetic a(LMw;)V
    .registers 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, LMw;->c()V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 7
    .parameter

    .prologue
    .line 386
    iget-object v0, p0, LMw;->a:LMz;

    if-nez v0, :cond_c

    .line 387
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, LMw;->f:Z

    .line 393
    if-nez p1, :cond_31

    .line 395
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v0

    .line 397
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v1

    .line 399
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, LMw;->d:I

    .line 400
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, LMw;->e:I

    move v2, v0

    .line 409
    :goto_26
    mul-int/2addr v2, v2

    iput v2, p0, LMw;->a:I

    .line 410
    mul-int/2addr v0, v0

    iput v0, p0, LMw;->b:I

    .line 411
    mul-int v0, v1, v1

    iput v0, p0, LMw;->c:I

    .line 412
    return-void

    .line 402
    :cond_31
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    .line 403
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 404
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v0

    .line 405
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 406
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, LMw;->d:I

    .line 407
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, LMw;->e:I

    goto :goto_26
.end method

.method static synthetic a(LMw;)Z
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-boolean v0, p0, LMw;->a:Z

    return v0
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 645
    iget-boolean v1, p0, LMw;->d:Z

    if-nez v1, :cond_6

    .line 655
    :cond_5
    :goto_5
    return v0

    .line 649
    :cond_6
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    sget v3, LMw;->h:I

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_5

    .line 653
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 654
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 655
    mul-int/2addr v1, v1

    mul-int/2addr v2, v2

    add-int/2addr v1, v2

    iget v2, p0, LMw;->c:I

    if-ge v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 632
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 633
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 634
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 635
    iput-boolean v2, p0, LMw;->e:Z

    .line 636
    iput-boolean v2, p0, LMw;->c:Z

    .line 637
    iput-boolean v2, p0, LMw;->d:Z

    .line 638
    iget-boolean v0, p0, LMw;->b:Z

    if-eqz v0, :cond_1f

    .line 639
    iput-boolean v2, p0, LMw;->b:Z

    .line 641
    :cond_1f
    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    .line 659
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 660
    const/4 v0, 0x1

    iput-boolean v0, p0, LMw;->b:Z

    .line 661
    iget-object v0, p0, LMw;->a:LMz;

    iget-object v1, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, LMz;->b(Landroid/view/MotionEvent;)V

    .line 662
    return-void
.end method


# virtual methods
.method public a(LMy;)V
    .registers 2
    .parameter

    .prologue
    .line 422
    iput-object p1, p0, LMw;->a:LMy;

    .line 423
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 14
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x3

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 454
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    .line 456
    iget-object v0, p0, LMw;->a:Landroid/view/VelocityTracker;

    if-nez v0, :cond_13

    .line 457
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LMw;->a:Landroid/view/VelocityTracker;

    .line 459
    :cond_13
    iget-object v0, p0, LMw;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 461
    and-int/lit16 v0, v8, 0xff

    const/4 v2, 0x6

    if-ne v0, v2, :cond_35

    move v6, v7

    .line 463
    :goto_1e
    if-eqz v6, :cond_37

    iget-boolean v0, p0, LMw;->g:Z

    if-nez v0, :cond_37

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 467
    :goto_28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    move v5, v3

    move v2, v1

    .line 468
    :goto_2e
    if-ge v5, v4, :cond_44

    .line 469
    if-ne v0, v5, :cond_39

    .line 468
    :goto_32
    add-int/lit8 v5, v5, 0x1

    goto :goto_2e

    :cond_35
    move v6, v3

    .line 461
    goto :goto_1e

    .line 463
    :cond_37
    const/4 v0, -0x1

    goto :goto_28

    .line 470
    :cond_39
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    add-float/2addr v2, v9

    .line 471
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    add-float/2addr v1, v9

    goto :goto_32

    .line 473
    :cond_44
    if-eqz v6, :cond_52

    add-int/lit8 v0, v4, -0x1

    .line 474
    :goto_48
    int-to-float v4, v0

    div-float/2addr v2, v4

    .line 475
    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 479
    and-int/lit16 v0, v8, 0xff

    packed-switch v0, :pswitch_data_214

    .line 613
    :cond_51
    :goto_51
    :pswitch_51
    return v3

    :cond_52
    move v0, v4

    .line 473
    goto :goto_48

    .line 481
    :pswitch_54
    iput v2, p0, LMw;->a:F

    iput v2, p0, LMw;->c:F

    .line 482
    iput v1, p0, LMw;->b:F

    iput v1, p0, LMw;->d:F

    .line 484
    invoke-direct {p0}, LMw;->b()V

    goto :goto_51

    .line 488
    :pswitch_60
    iput v2, p0, LMw;->a:F

    iput v2, p0, LMw;->c:F

    .line 489
    iput v1, p0, LMw;->b:F

    iput v1, p0, LMw;->d:F

    goto :goto_51

    .line 493
    :pswitch_69
    iget-object v0, p0, LMw;->a:LMy;

    if-eqz v0, :cond_fc

    .line 494
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    .line 495
    if-eqz v0, :cond_7a

    iget-object v4, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v4, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 496
    :cond_7a
    iget-object v4, p0, LMw;->a:Landroid/view/MotionEvent;

    if-eqz v4, :cond_f4

    iget-object v4, p0, LMw;->b:Landroid/view/MotionEvent;

    if-eqz v4, :cond_f4

    if-eqz v0, :cond_f4

    iget-object v0, p0, LMw;->a:Landroid/view/MotionEvent;

    iget-object v4, p0, LMw;->b:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v4, p1}, LMw;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_f4

    .line 499
    iput-boolean v7, p0, LMw;->e:Z

    .line 501
    iget-object v0, p0, LMw;->a:LMy;

    iget-object v4, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-interface {v0, v4}, LMy;->c(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v3

    .line 503
    iget-object v4, p0, LMw;->a:LMy;

    invoke-interface {v4, p1}, LMy;->e(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v0, v4

    .line 510
    :goto_a0
    iput v2, p0, LMw;->a:F

    iput v2, p0, LMw;->c:F

    .line 511
    iput v1, p0, LMw;->b:F

    iput v1, p0, LMw;->d:F

    .line 512
    iget-object v1, p0, LMw;->a:Landroid/view/MotionEvent;

    if-eqz v1, :cond_b1

    .line 513
    iget-object v1, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 515
    :cond_b1
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, LMw;->a:Landroid/view/MotionEvent;

    .line 516
    iput-boolean v7, p0, LMw;->c:Z

    .line 517
    iput-boolean v7, p0, LMw;->d:Z

    .line 518
    iput-boolean v7, p0, LMw;->a:Z

    .line 519
    iput-boolean v3, p0, LMw;->b:Z

    .line 521
    iget-boolean v1, p0, LMw;->f:Z

    if-eqz v1, :cond_db

    .line 522
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 523
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    iget-object v2, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, LMw;->g:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    sget v4, LMw;->f:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v11, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 526
    :cond_db
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    iget-object v2, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, LMw;->g:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v7, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 528
    iget-object v1, p0, LMw;->a:LMz;

    invoke-interface {v1, p1}, LMz;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int v3, v0, v1

    .line 529
    goto/16 :goto_51

    .line 506
    :cond_f4
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    sget v4, LMw;->h:I

    int-to-long v4, v4

    invoke-virtual {v0, v10, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_fc
    move v0, v3

    goto :goto_a0

    .line 532
    :pswitch_fe
    iget-boolean v0, p0, LMw;->b:Z

    if-nez v0, :cond_51

    .line 535
    iget v0, p0, LMw;->a:F

    sub-float/2addr v0, v2

    .line 536
    iget v4, p0, LMw;->b:F

    sub-float/2addr v4, v1

    .line 537
    iget-boolean v5, p0, LMw;->e:Z

    if-eqz v5, :cond_115

    .line 539
    iget-object v0, p0, LMw;->a:LMy;

    invoke-interface {v0, p1}, LMy;->e(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v3, v0

    goto/16 :goto_51

    .line 540
    :cond_115
    iget-boolean v5, p0, LMw;->c:Z

    if-eqz v5, :cond_150

    .line 541
    iget v5, p0, LMw;->c:F

    sub-float v5, v2, v5

    float-to-int v5, v5

    .line 542
    iget v6, p0, LMw;->d:F

    sub-float v6, v1, v6

    float-to-int v6, v6

    .line 543
    mul-int/2addr v5, v5

    mul-int/2addr v6, v6

    add-int/2addr v5, v6

    .line 544
    iget v6, p0, LMw;->a:I

    if-le v5, v6, :cond_210

    .line 545
    iget-object v6, p0, LMw;->a:LMz;

    iget-object v8, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-interface {v6, v8, p1, v0, v4}, LMz;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 546
    iput v2, p0, LMw;->a:F

    .line 547
    iput v1, p0, LMw;->b:F

    .line 548
    iput-boolean v3, p0, LMw;->c:Z

    .line 549
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v1, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 550
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 551
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 553
    :goto_147
    iget v1, p0, LMw;->b:I

    if-le v5, v1, :cond_14d

    .line 554
    iput-boolean v3, p0, LMw;->d:Z

    :cond_14d
    move v3, v0

    .line 556
    goto/16 :goto_51

    :cond_150
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f80

    cmpl-float v5, v5, v6

    if-gez v5, :cond_164

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f80

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_51

    .line 557
    :cond_164
    iget-object v3, p0, LMw;->a:LMz;

    iget-object v5, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-interface {v3, v5, p1, v0, v4}, LMz;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    .line 558
    iput v2, p0, LMw;->a:F

    .line 559
    iput v1, p0, LMw;->b:F

    goto/16 :goto_51

    .line 564
    :pswitch_172
    iput-boolean v3, p0, LMw;->a:Z

    .line 565
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 566
    iget-boolean v0, p0, LMw;->e:Z

    if-eqz v0, :cond_1a9

    .line 568
    iget-object v0, p0, LMw;->a:LMy;

    invoke-interface {v0, p1}, LMy;->e(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v3

    .line 592
    :goto_183
    iget-object v1, p0, LMw;->b:Landroid/view/MotionEvent;

    if-eqz v1, :cond_18c

    .line 593
    iget-object v1, p0, LMw;->b:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 596
    :cond_18c
    iput-object v2, p0, LMw;->b:Landroid/view/MotionEvent;

    .line 597
    iget-object v1, p0, LMw;->a:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_19a

    .line 600
    iget-object v1, p0, LMw;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 601
    const/4 v1, 0x0

    iput-object v1, p0, LMw;->a:Landroid/view/VelocityTracker;

    .line 603
    :cond_19a
    iput-boolean v3, p0, LMw;->e:Z

    .line 604
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 605
    iget-object v1, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    move v3, v0

    .line 606
    goto/16 :goto_51

    .line 569
    :cond_1a9
    iget-boolean v0, p0, LMw;->b:Z

    if-eqz v0, :cond_1b6

    .line 570
    iget-object v0, p0, LMw;->a:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 571
    iput-boolean v3, p0, LMw;->b:Z

    move v0, v3

    goto :goto_183

    .line 572
    :cond_1b6
    iget-boolean v0, p0, LMw;->c:Z

    if-eqz v0, :cond_1c1

    .line 573
    iget-object v0, p0, LMw;->a:LMz;

    invoke-interface {v0, p1}, LMz;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_183

    .line 577
    :cond_1c1
    iget-object v1, p0, LMw;->a:Landroid/view/VelocityTracker;

    .line 578
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    .line 579
    const/16 v0, 0x3e8

    iget v5, p0, LMw;->e:I

    int-to-float v5, v5

    invoke-virtual {v1, v0, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 581
    iget-boolean v0, p0, LMw;->g:Z

    if-eqz v0, :cond_1fe

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    .line 584
    :goto_1d7
    iget-boolean v5, p0, LMw;->g:Z

    if-eqz v5, :cond_203

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    .line 587
    :goto_1df
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, LMw;->d:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_1f5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, LMw;->d:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_20d

    .line 589
    :cond_1f5
    iget-object v4, p0, LMw;->a:LMz;

    iget-object v5, p0, LMw;->a:Landroid/view/MotionEvent;

    invoke-interface {v4, v5, p1, v1, v0}, LMz;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_183

    .line 581
    :cond_1fe
    invoke-virtual {v1, v4}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    goto :goto_1d7

    .line 584
    :cond_203
    invoke-virtual {v1, v4}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v1

    goto :goto_1df

    .line 609
    :pswitch_208
    invoke-direct {p0}, LMw;->a()V

    goto/16 :goto_51

    :cond_20d
    move v0, v3

    goto/16 :goto_183

    :cond_210
    move v0, v3

    goto/16 :goto_147

    .line 479
    nop

    :pswitch_data_214
    .packed-switch 0x0
        :pswitch_69
        :pswitch_172
        :pswitch_fe
        :pswitch_208
        :pswitch_51
        :pswitch_54
        :pswitch_60
    .end packed-switch
.end method
