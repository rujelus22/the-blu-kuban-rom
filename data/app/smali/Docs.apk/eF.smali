.class public LeF;
.super Ljava/lang/Object;
.source "AclType.java"


# instance fields
.field private a:LeI;

.field private a:LeK;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    iput-object v0, p0, LeF;->a:Ljava/lang/String;

    .line 340
    iput-object v0, p0, LeF;->b:Ljava/lang/String;

    .line 341
    sget-object v0, LeK;->e:LeK;

    iput-object v0, p0, LeF;->a:LeK;

    .line 342
    sget-object v0, LeI;->f:LeI;

    iput-object v0, p0, LeF;->a:LeI;

    .line 343
    const-class v0, LeD;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LeF;->a:Ljava/util/Set;

    .line 344
    const/4 v0, 0x0

    iput-boolean v0, p0, LeF;->a:Z

    .line 355
    return-void
.end method

.method private a(LeG;LeK;Z)LeF;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 433
    invoke-virtual {p1}, LeG;->a()LeI;

    move-result-object v0

    iput-object v0, p0, LeF;->a:LeI;

    .line 434
    iput-object p2, p0, LeF;->a:LeK;

    .line 435
    iput-boolean p3, p0, LeF;->a:Z

    .line 436
    iget-object v0, p0, LeF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 437
    iget-object v0, p0, LeF;->a:Ljava/util/Set;

    invoke-virtual {p1}, LeG;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 438
    return-object p0
.end method


# virtual methods
.method public a()LeB;
    .registers 8

    .prologue
    .line 426
    new-instance v0, LeB;

    iget-object v1, p0, LeF;->a:Ljava/lang/String;

    iget-object v2, p0, LeF;->b:Ljava/lang/String;

    iget-object v3, p0, LeF;->a:LeK;

    iget-object v4, p0, LeF;->a:LeI;

    iget-object v5, p0, LeF;->a:Ljava/util/Set;

    invoke-static {v4, v5}, LeG;->a(LeI;Ljava/util/Set;)LeG;

    move-result-object v4

    iget-boolean v5, p0, LeF;->a:Z

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LeB;-><init>(Ljava/lang/String;Ljava/lang/String;LeK;LeG;ZLeC;)V

    return-object v0
.end method

.method public a(LeB;)LeF;
    .registers 4
    .parameter

    .prologue
    .line 409
    invoke-static {p1}, LeB;->a(LeB;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LeF;->a:Ljava/lang/String;

    .line 410
    invoke-static {p1}, LeB;->b(LeB;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LeF;->b:Ljava/lang/String;

    .line 411
    invoke-static {p1}, LeB;->a(LeB;)LeK;

    move-result-object v0

    iput-object v0, p0, LeF;->a:LeK;

    .line 412
    invoke-virtual {p1}, LeB;->a()LeI;

    move-result-object v0

    iput-object v0, p0, LeF;->a:LeI;

    .line 413
    invoke-static {p1}, LeB;->a(LeB;)Z

    move-result v0

    iput-boolean v0, p0, LeF;->a:Z

    .line 414
    iget-object v0, p0, LeF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 415
    iget-object v0, p0, LeF;->a:Ljava/util/Set;

    invoke-virtual {p1}, LeB;->a()LeG;

    move-result-object v1

    invoke-virtual {v1}, LeG;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 416
    return-object p0
.end method

.method public a(LeG;)LeF;
    .registers 3
    .parameter

    .prologue
    .line 397
    invoke-virtual {p1}, LeG;->a()LeI;

    move-result-object v0

    iput-object v0, p0, LeF;->a:LeI;

    .line 398
    invoke-virtual {p1}, LeG;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, LeF;->a(Ljava/util/Set;)LeF;

    .line 399
    return-object p0
.end method

.method public a(LeH;)LeF;
    .registers 5
    .parameter

    .prologue
    .line 420
    invoke-virtual {p1}, LeH;->a()LeG;

    move-result-object v0

    invoke-virtual {p1}, LeH;->a()LeK;

    move-result-object v1

    invoke-virtual {p1}, LeH;->a()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LeF;->a(LeG;LeK;Z)LeF;

    move-result-object v0

    return-object v0
.end method

.method public a(LeI;)LeF;
    .registers 2
    .parameter

    .prologue
    .line 373
    iput-object p1, p0, LeF;->a:LeI;

    .line 374
    return-object p0
.end method

.method public a(LeK;)LeF;
    .registers 2
    .parameter

    .prologue
    .line 368
    iput-object p1, p0, LeF;->a:LeK;

    .line 369
    return-object p0
.end method

.method public a(Ljava/lang/String;)LeF;
    .registers 2
    .parameter

    .prologue
    .line 358
    iput-object p1, p0, LeF;->a:Ljava/lang/String;

    .line 359
    return-object p0
.end method

.method public a(Ljava/util/Set;)LeF;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;)",
            "LeF;"
        }
    .end annotation

    .prologue
    .line 378
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 379
    const-class v0, LeD;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LeF;->a:Ljava/util/Set;

    .line 383
    :goto_e
    return-object p0

    .line 381
    :cond_f
    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LeF;->a:Ljava/util/Set;

    goto :goto_e
.end method

.method public a(Z)LeF;
    .registers 2
    .parameter

    .prologue
    .line 387
    iput-boolean p1, p0, LeF;->a:Z

    .line 388
    return-object p0
.end method

.method public b(Ljava/lang/String;)LeF;
    .registers 2
    .parameter

    .prologue
    .line 363
    iput-object p1, p0, LeF;->b:Ljava/lang/String;

    .line 364
    return-object p0
.end method
