.class public LAK;
.super LAC;
.source "Text.java"


# instance fields
.field private final a:Lxz;


# direct methods
.method public constructor <init>(LDb;Lzd;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1}, LAC;-><init>(LDb;)V

    .line 32
    new-instance v0, Lxz;

    iget-object v1, p0, LAK;->a:LDI;

    invoke-direct {v0, p1, v1, p2, p3}, Lxz;-><init>(LDb;LDI;Lzd;Lxu;)V

    iput-object v0, p0, LAK;->a:Lxz;

    .line 33
    return-void
.end method


# virtual methods
.method protected a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, LAK;->a:Lxz;

    invoke-virtual {v0, p1, p2}, Lxz;->c(II)V

    .line 49
    return-void
.end method

.method public a(IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, LAK;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 75
    iget-object v1, p0, LAK;->a:LDI;

    invoke-virtual {v1}, LDI;->c()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 76
    if-lt v1, v0, :cond_2e

    iget-object v2, p0, LAK;->a:LDI;

    invoke-virtual {v2}, LDI;->c()I

    move-result v2

    if-ge v0, v2, :cond_2e

    .line 77
    new-instance v2, LKj;

    invoke-virtual {p0, v0}, LAK;->d(I)I

    move-result v0

    invoke-virtual {p0, v1}, LAK;->d(I)I

    move-result v1

    invoke-direct {v2, v0, v1}, LKj;-><init>(II)V

    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_2e
    return-void
.end method

.method public a(IILCh;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, LAK;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 64
    iget-object v1, p0, LAK;->a:LDI;

    invoke-virtual {v1}, LDI;->c()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 65
    if-le v1, v0, :cond_1e

    .line 66
    iget-object v2, p0, LAK;->a:Lxz;

    sub-int/2addr v1, v0

    invoke-virtual {v2, v0, v1, p3}, Lxz;->a(IILCh;)Z

    move-result v0

    .line 68
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x1

    goto :goto_1d
.end method

.method protected b(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, LAK;->a:Lxz;

    invoke-virtual {v0, p1, p2}, Lxz;->a(II)V

    .line 54
    return-void
.end method

.method public c(I)I
    .registers 4
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, LAK;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    sub-int v0, p1, v0

    iget-object v1, p0, LAK;->a:LDI;

    invoke-virtual {v1}, LDI;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected c(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, LAK;->a:Lxz;

    invoke-virtual {v0, p1, p2}, Lxz;->b(II)V

    .line 59
    return-void
.end method

.method public d(I)I
    .registers 4
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LAK;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    sub-int v0, p1, v0

    iget-object v1, p0, LAK;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
