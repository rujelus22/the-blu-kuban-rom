.class public Lzw;
.super Ljava/lang/Object;
.source "SimpleFastScroller.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ":",
        "LxG",
        "<TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:[I

.field private static final b:[I


# instance fields
.field private a:I

.field private a:Landroid/graphics/drawable/Drawable;

.field private final a:Landroid/os/Handler;

.field private final a:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private a:Lzx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lzw",
            "<TT;>.zx;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private b:Landroid/graphics/drawable/Drawable;

.field private final b:Landroid/view/View;

.field private b:Z

.field private c:I

.field private c:Z

.field private d:I

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 63
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lzw;->a:[I

    .line 67
    new-array v0, v2, [I

    sput-object v0, Lzw;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lzw;->a:Landroid/os/Handler;

    .line 98
    iput v1, p0, Lzw;->l:I

    .line 106
    iput-boolean v1, p0, Lzw;->c:Z

    .line 107
    iput-boolean v1, p0, Lzw;->d:Z

    .line 113
    iput-object p2, p0, Lzw;->a:Landroid/view/View;

    .line 114
    iput-object p3, p0, Lzw;->b:Landroid/view/View;

    .line 115
    invoke-direct {p0, p1}, Lzw;->a(Landroid/content/Context;)V

    .line 116
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/util/Pair;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496
    new-instance v0, Landroid/util/Pair;

    iget v1, p0, Lzw;->l:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    iget v3, p0, Lzw;->l:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic a(Lzw;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lzw;->b:Landroid/view/View;

    return-object v0
.end method

.method private a(D)V
    .registers 6
    .parameter

    .prologue
    .line 375
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    check-cast v0, LxG;

    invoke-interface {v0}, LxG;->b()I

    move-result v0

    iget-object v1, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    iput v0, p0, Lzw;->j:I

    .line 376
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    iget-object v1, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    iget v2, p0, Lzw;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->scrollTo(II)V

    .line 377
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 189
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 190
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_6e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 192
    sget v2, LsC;->scrollbar_handle_accelerated_anim2:I

    invoke-virtual {v1, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lzw;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 194
    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    .line 195
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 197
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LsB;->kix_edit_scrollbar_edge_offset:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget v2, p0, Lzw;->a:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lzw;->l:I

    .line 200
    sget v1, LsB;->kix_edit_horizontal_scrollbar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lzw;->d:I

    .line 201
    iput v3, p0, Lzw;->e:I

    .line 202
    iget-object v1, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lzw;->f:I

    .line 203
    sget v1, LsB;->kix_edit_horizontal_scrollbar_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lzw;->g:I

    .line 206
    sget v1, LsA;->horizontal_scroll_indicator:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lzw;->h:I

    .line 208
    new-instance v0, Lzx;

    invoke-direct {v0, p0}, Lzx;-><init>(Lzw;)V

    iput-object v0, p0, Lzw;->a:Lzx;

    .line 209
    iput-boolean v4, p0, Lzw;->a:Z

    .line 210
    iput v3, p0, Lzw;->k:I

    .line 211
    invoke-direct {p0}, Lzw;->c()V

    .line 212
    return-void

    .line 190
    :array_6e
    .array-data 0x4
        0x36t 0x3t 0x1t 0x1t
        0x39t 0x3t 0x1t 0x1t
    .end array-data
.end method

.method private a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 169
    iput-object p2, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    .line 170
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsB;->fastscroll_thumb_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lzw;->b:I

    .line 171
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsB;->fastscroll_thumb_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lzw;->a:I

    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lzw;->b:Z

    .line 173
    return-void
.end method

.method private b()V
    .registers 6

    .prologue
    .line 162
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 164
    iget-object v1, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lzw;->b:I

    sub-int v2, v0, v2

    const/4 v3, 0x0

    iget v4, p0, Lzw;->a:I

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 165
    iget-object v0, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xd0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 166
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x0

    .line 257
    iget v0, p0, Lzw;->c:I

    iget v1, p0, Lzw;->a:I

    div-int/lit8 v1, v1, 0x2

    sub-int v2, v0, v1

    .line 258
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 259
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lzw;->a(Landroid/view/View;)Landroid/util/Pair;

    move-result-object v4

    .line 260
    iget-object v1, p0, Lzw;->a:Lzx;

    .line 262
    const/4 v0, -0x1

    .line 263
    iget v5, p0, Lzw;->k:I

    if-ne v5, v10, :cond_3e

    .line 264
    invoke-virtual {v1}, Lzx;->a()I

    move-result v0

    .line 265
    const/16 v1, 0x68

    if-ge v0, v1, :cond_2c

    .line 266
    iget-object v1, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    mul-int/lit8 v5, v0, 0x2

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 268
    :cond_2c
    iget v1, p0, Lzw;->b:I

    mul-int/2addr v1, v0

    div-int/lit16 v1, v1, 0xd0

    sub-int v1, v3, v1

    .line 269
    iget-object v5, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    iget v7, p0, Lzw;->a:I

    invoke-virtual {v5, v1, v6, v3, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 270
    const/4 v1, 0x1

    iput-boolean v1, p0, Lzw;->b:Z

    :cond_3e
    move v1, v0

    .line 273
    iget-object v0, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_75

    .line 274
    iget-object v0, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 275
    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 276
    iget-object v5, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 277
    iget v6, p0, Lzw;->b:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v0, v6

    div-int/lit8 v6, v5, 0x2

    sub-int v6, v0, v6

    .line 278
    iget-object v7, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/2addr v5, v6

    iget-object v0, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v7, v6, v8, v5, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 280
    iget-object v0, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 283
    :cond_75
    int-to-float v0, v2

    invoke-virtual {p1, v9, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 284
    iget-object v0, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 285
    neg-int v0, v2

    int-to-float v0, v0

    invoke-virtual {p1, v9, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 287
    iget v0, p0, Lzw;->k:I

    if-ne v0, v10, :cond_95

    if-eqz v1, :cond_95

    .line 288
    iget-object v0, p0, Lzw;->b:Landroid/view/View;

    iget v1, p0, Lzw;->b:I

    sub-int v1, v3, v1

    iget v4, p0, Lzw;->a:I

    add-int/2addr v4, v2

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->invalidate(IIII)V

    .line 290
    :cond_95
    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    .line 215
    iget v0, p0, Lzw;->k:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2a

    sget-object v0, Lzw;->a:[I

    .line 216
    :goto_7
    iget-object v1, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 217
    iget-object v1, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 219
    :cond_18
    iget-object v1, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_29

    iget-object v1, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_29

    .line 220
    iget-object v1, p0, Lzw;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 222
    :cond_29
    return-void

    .line 215
    :cond_2a
    sget-object v0, Lzw;->b:[I

    goto :goto_7
.end method

.method private c(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x4

    .line 293
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 294
    iget-object v2, p0, Lzw;->a:Lzx;

    .line 296
    const/16 v0, 0xd0

    .line 297
    iget v3, p0, Lzw;->k:I

    if-ne v3, v7, :cond_13

    .line 298
    invoke-virtual {v2}, Lzx;->a()I

    move-result v0

    .line 301
    :cond_13
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 302
    iget v3, p0, Lzw;->h:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 303
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 305
    iget v3, p0, Lzw;->f:I

    iget v4, p0, Lzw;->e:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    .line 306
    iget v4, p0, Lzw;->g:I

    sub-int v4, v1, v4

    iget v5, p0, Lzw;->d:I

    sub-int/2addr v4, v5

    .line 307
    iget v5, p0, Lzw;->f:I

    iget v6, p0, Lzw;->e:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    .line 308
    iget v6, p0, Lzw;->g:I

    sub-int/2addr v1, v6

    .line 310
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 311
    invoke-virtual {p1, v6, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 313
    iget v2, p0, Lzw;->k:I

    if-ne v2, v7, :cond_4b

    if-eqz v0, :cond_4b

    .line 314
    iget-object v0, p0, Lzw;->b:Landroid/view/View;

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/view/View;->invalidate(IIII)V

    .line 316
    :cond_4b
    return-void
.end method

.method private d()V
    .registers 9

    .prologue
    const-wide/16 v0, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 381
    move-wide v2, v0

    move v6, v5

    move v7, v4

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 382
    iget-object v1, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 383
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 384
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 158
    iget v0, p0, Lzw;->k:I

    return v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lzw;->a(I)V

    .line 226
    return-void
.end method

.method public a(I)V
    .registers 9
    .parameter

    .prologue
    .line 124
    packed-switch p1, :pswitch_data_68

    .line 153
    :goto_3
    :pswitch_3
    iput p1, p0, Lzw;->k:I

    .line 154
    invoke-direct {p0}, Lzw;->c()V

    .line 155
    return-void

    .line 126
    :pswitch_9
    iget-object v0, p0, Lzw;->a:Landroid/os/Handler;

    iget-object v1, p0, Lzw;->a:Lzx;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 127
    iget-object v0, p0, Lzw;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_3

    .line 130
    :pswitch_16
    iget v0, p0, Lzw;->k:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1e

    .line 131
    invoke-direct {p0}, Lzw;->b()V

    .line 137
    :cond_1e
    :pswitch_1e
    iget-object v0, p0, Lzw;->a:Landroid/os/Handler;

    iget-object v1, p0, Lzw;->a:Lzx;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 140
    :pswitch_26
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 141
    iget-object v1, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 143
    iget-object v2, p0, Lzw;->b:Landroid/view/View;

    iget v3, p0, Lzw;->b:I

    sub-int v3, v0, v3

    iget v4, p0, Lzw;->c:I

    iget v5, p0, Lzw;->a:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Lzw;->c:I

    iget v6, p0, Lzw;->a:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/view/View;->invalidate(IIII)V

    .line 146
    iget-object v0, p0, Lzw;->b:Landroid/view/View;

    iget v2, p0, Lzw;->f:I

    iget v3, p0, Lzw;->e:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lzw;->g:I

    sub-int v3, v1, v3

    iget v4, p0, Lzw;->d:I

    sub-int/2addr v3, v4

    iget v4, p0, Lzw;->f:I

    iget v5, p0, Lzw;->e:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    iget v5, p0, Lzw;->g:I

    sub-int/2addr v1, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_3

    .line 124
    nop

    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_9
        :pswitch_3
        :pswitch_16
        :pswitch_1e
        :pswitch_26
    .end packed-switch
.end method

.method public a(II)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    iget v0, p0, Lzw;->i:I

    if-ne p1, v0, :cond_c

    iget v0, p0, Lzw;->j:I

    if-ne p2, v0, :cond_c

    .line 372
    :cond_b
    :goto_b
    return-void

    .line 329
    :cond_c
    iput p1, p0, Lzw;->i:I

    .line 330
    iput p2, p0, Lzw;->j:I

    .line 332
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    check-cast v0, LxG;

    invoke-interface {v0}, LxG;->a()I

    move-result v3

    .line 333
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v4, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v0, v4

    iget-object v4, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    sub-int v4, v0, v4

    .line 334
    if-ge v4, v3, :cond_65

    move v0, v1

    :goto_30
    iput-boolean v0, p0, Lzw;->c:Z

    .line 336
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    check-cast v0, LxG;

    invoke-interface {v0}, LxG;->b()I

    move-result v0

    iget-object v5, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    sub-int v5, v0, v5

    .line 337
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    check-cast v0, LxG;

    invoke-interface {v0}, LxG;->b()I

    move-result v0

    if-ge v6, v0, :cond_67

    move v0, v1

    :goto_53
    iput-boolean v0, p0, Lzw;->d:Z

    .line 339
    iget-boolean v0, p0, Lzw;->c:Z

    if-nez v0, :cond_69

    iget-boolean v0, p0, Lzw;->d:Z

    if-nez v0, :cond_69

    .line 340
    iget v0, p0, Lzw;->k:I

    if-eqz v0, :cond_b

    .line 341
    invoke-virtual {p0, v2}, Lzw;->a(I)V

    goto :goto_b

    :cond_65
    move v0, v2

    .line 334
    goto :goto_30

    :cond_67
    move v0, v2

    .line 337
    goto :goto_53

    .line 346
    :cond_69
    iget-boolean v0, p0, Lzw;->c:Z

    if-eqz v0, :cond_86

    .line 347
    iget v0, p0, Lzw;->i:I

    div-int/lit8 v6, v4, 0x2

    add-int/2addr v0, v6

    .line 349
    iget-object v6, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingLeft()I

    move-result v6

    mul-int/2addr v0, v4

    div-int/2addr v0, v3

    add-int/2addr v0, v6

    iput v0, p0, Lzw;->f:I

    .line 352
    mul-int v0, v4, v4

    div-int/2addr v0, v3

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lzw;->e:I

    .line 356
    :cond_86
    iget-boolean v0, p0, Lzw;->d:Z

    if-eqz v0, :cond_bc

    iget v0, p0, Lzw;->k:I

    if-eq v0, v7, :cond_bc

    .line 357
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lzw;->a(Landroid/view/View;)Landroid/util/Pair;

    move-result-object v3

    .line 358
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr v4, v0

    .line 360
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int v3, v4, p2

    div-int/2addr v3, v5

    add-int/2addr v0, v3

    iput v0, p0, Lzw;->c:I

    .line 361
    iget-boolean v0, p0, Lzw;->b:Z

    if-eqz v0, :cond_bc

    .line 362
    invoke-direct {p0}, Lzw;->b()V

    .line 363
    iput-boolean v2, p0, Lzw;->b:Z

    .line 367
    :cond_bc
    iput-boolean v1, p0, Lzw;->a:Z

    .line 368
    iget v0, p0, Lzw;->k:I

    if-eq v0, v7, :cond_b

    .line 369
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lzw;->a(I)V

    .line 370
    iget-object v0, p0, Lzw;->a:Landroid/os/Handler;

    iget-object v1, p0, Lzw;->a:Lzx;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_b
.end method

.method public a(IIII)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 176
    iget-object v0, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_10

    .line 177
    iget-object v0, p0, Lzw;->a:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lzw;->b:I

    sub-int v1, p1, v1

    iget v2, p0, Lzw;->a:I

    invoke-virtual {v0, v1, v3, p1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 184
    :cond_10
    invoke-virtual {p0, v3}, Lzw;->a(I)V

    .line 185
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter

    .prologue
    .line 238
    iget v0, p0, Lzw;->k:I

    if-nez v0, :cond_5

    .line 254
    :cond_4
    :goto_4
    return-void

    .line 243
    :cond_5
    iget-boolean v0, p0, Lzw;->c:Z

    if-eqz v0, :cond_c

    .line 244
    invoke-direct {p0, p1}, Lzw;->c(Landroid/graphics/Canvas;)V

    .line 247
    :cond_c
    iget-boolean v0, p0, Lzw;->d:Z

    if-eqz v0, :cond_13

    .line 248
    invoke-direct {p0, p1}, Lzw;->b(Landroid/graphics/Canvas;)V

    .line 251
    :cond_13
    iget v0, p0, Lzw;->k:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lzw;->a:Lzx;

    invoke-virtual {v0}, Lzx;->a()I

    move-result v0

    if-nez v0, :cond_4

    .line 252
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lzw;->a(I)V

    goto :goto_4
.end method

.method a(FF)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 444
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p0, Lzw;->b:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_28

    iget v0, p0, Lzw;->c:I

    iget v1, p0, Lzw;->a:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_28

    iget v0, p0, Lzw;->c:I

    iget v1, p0, Lzw;->a:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_28

    const/4 v0, 0x1

    :goto_27
    return v0

    :cond_28
    const/4 v0, 0x0

    goto :goto_27
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    .line 387
    iget v0, p0, Lzw;->k:I

    if-lez v0, :cond_1e

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1e

    .line 388
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lzw;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 389
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lzw;->a(I)V

    .line 390
    const/4 v0, 0x1

    .line 393
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 397
    iget v1, p0, Lzw;->k:I

    if-nez v1, :cond_9

    .line 440
    :cond_8
    :goto_8
    return v0

    .line 401
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 403
    if-nez v1, :cond_25

    .line 404
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v1, v3}, Lzw;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 405
    invoke-virtual {p0, v5}, Lzw;->a(I)V

    .line 406
    invoke-direct {p0}, Lzw;->d()V

    move v0, v2

    .line 407
    goto :goto_8

    .line 409
    :cond_25
    if-ne v1, v2, :cond_3e

    .line 410
    iget v1, p0, Lzw;->k:I

    if-ne v1, v5, :cond_8

    .line 411
    invoke-virtual {p0, v4}, Lzw;->a(I)V

    .line 412
    iget-object v0, p0, Lzw;->a:Landroid/os/Handler;

    .line 413
    iget-object v1, p0, Lzw;->a:Lzx;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 414
    iget-object v1, p0, Lzw;->a:Lzx;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v2

    .line 415
    goto :goto_8

    .line 417
    :cond_3e
    if-ne v1, v4, :cond_8

    .line 418
    iget v1, p0, Lzw;->k:I

    if-ne v1, v5, :cond_8

    .line 419
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lzw;->a(Landroid/view/View;)Landroid/util/Pair;

    move-result-object v3

    .line 420
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v1, v0

    .line 422
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v1, v0, :cond_6c

    .line 423
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 427
    :goto_61
    iget v1, p0, Lzw;->c:I

    sub-int/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v4, :cond_7f

    move v0, v2

    .line 428
    goto :goto_8

    .line 424
    :cond_6c
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v1, v0, :cond_b1

    .line 425
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_61

    .line 430
    :cond_7f
    iput v0, p0, Lzw;->c:I

    .line 432
    iget-boolean v0, p0, Lzw;->a:Z

    if-eqz v0, :cond_ae

    .line 433
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr v1, v0

    .line 434
    iget v4, p0, Lzw;->c:I

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v4, v0

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-direct {p0, v0, v1}, Lzw;->a(D)V

    .line 435
    iget-object v0, p0, Lzw;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    :cond_ae
    move v0, v2

    .line 437
    goto/16 :goto_8

    :cond_b1
    move v0, v1

    goto :goto_61
.end method
