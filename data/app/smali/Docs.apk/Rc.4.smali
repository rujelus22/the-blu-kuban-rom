.class public LRc;
.super Landroid/widget/BaseAdapter;
.source "PunchSlideAdapter.java"


# instance fields
.field private final a:LQR;

.field private final a:LRj;

.field private final a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(LQR;LRj;Landroid/view/LayoutInflater;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 48
    iput-object p1, p0, LRc;->a:LQR;

    .line 49
    iput-object p2, p0, LRc;->a:LRj;

    .line 50
    iput-object p3, p0, LRc;->a:Landroid/view/LayoutInflater;

    .line 51
    return-void
.end method

.method private a()F
    .registers 4

    .prologue
    .line 130
    iget-object v0, p0, LRc;->a:LQR;

    invoke-interface {v0}, LQR;->a()I

    move-result v1

    .line 131
    iget-object v0, p0, LRc;->a:LQR;

    invoke-interface {v0}, LQR;->b()I

    move-result v2

    .line 132
    const/high16 v0, 0x3f80

    .line 133
    if-ltz v1, :cond_15

    if-ltz v2, :cond_15

    .line 134
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    .line 136
    :cond_15
    return v0
.end method

.method private a(ILandroid/widget/ImageView;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, LRc;->a:LRj;

    invoke-interface {v0, p1}, LRj;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 126
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 127
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 99
    sget v0, Leh;->slide_index:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 100
    add-int/lit8 v1, p2, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    sget v0, Leh;->speaker_notes_indicator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 104
    iget-object v1, p0, LRc;->a:LQR;

    invoke-interface {v1, p2}, LQR;->a(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_55

    move v1, v3

    .line 105
    :goto_2b
    if-eqz v1, :cond_57

    move v1, v2

    :goto_2e
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    sget v0, Leh;->thumbnail:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;

    .line 109
    invoke-direct {p0}, LRc;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->setAspect(F)V

    .line 117
    if-ne p2, p3, :cond_5a

    .line 118
    :goto_42
    sget v1, Leh;->thumbnail_border:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 119
    if-eqz v3, :cond_4e

    sget v2, Lee;->slide_thumbnail_activated:I

    :cond_4e
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 121
    invoke-direct {p0, p2, v0}, LRc;->a(ILandroid/widget/ImageView;)V

    .line 122
    return-void

    :cond_55
    move v1, v2

    .line 104
    goto :goto_2b

    .line 105
    :cond_57
    const/16 v1, 0x8

    goto :goto_2e

    :cond_5a
    move v3, v2

    .line 117
    goto :goto_42
.end method


# virtual methods
.method public a(Landroid/view/View;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget v0, Leh;->thumbnail:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 90
    invoke-direct {p0, p2, v0}, LRc;->a(ILandroid/widget/ImageView;)V

    .line 91
    return-void
.end method

.method public getCount()I
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, LRc;->a:LQR;

    invoke-interface {v0}, LQR;->c()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 60
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 65
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    .line 71
    if-nez p2, :cond_37

    .line 72
    iget-object v0, p0, LRc;->a:Landroid/view/LayoutInflater;

    sget v1, Lej;->punch_slide_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 73
    sget v0, Leh;->slide_index:I

    sget v1, Leh;->slide_index:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 74
    sget v0, Leh;->speaker_notes_indicator:I

    sget v1, Leh;->speaker_notes_indicator:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 75
    sget v0, Leh;->thumbnail:I

    sget v1, Leh;->thumbnail:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 76
    sget v0, Leh;->thumbnail_border:I

    sget v1, Leh;->thumbnail_border:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 79
    :cond_37
    iget-object v0, p0, LRc;->a:LQR;

    invoke-interface {v0}, LQR;->d()I

    move-result v0

    invoke-direct {p0, p2, p1, v0}, LRc;->a(Landroid/view/View;II)V

    .line 81
    return-object p2
.end method
