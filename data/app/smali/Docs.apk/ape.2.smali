.class public abstract enum Lape;
.super Ljava/lang/Enum;
.source "BytecodeGen.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lape;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lape;

.field private static final synthetic a:[Lape;

.field public static final enum b:Lape;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 235
    new-instance v0, Lapf;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v2}, Lapf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lape;->a:Lape;

    .line 248
    new-instance v0, Lapg;

    const-string v1, "SAME_PACKAGE"

    invoke-direct {v0, v1, v3}, Lapg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lape;->b:Lape;

    .line 229
    const/4 v0, 0x2

    new-array v0, v0, [Lape;

    sget-object v1, Lape;->a:Lape;

    aput-object v1, v0, v2

    sget-object v1, Lape;->b:Lape;

    aput-object v1, v0, v3

    sput-object v0, Lape;->a:[Lape;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 229
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILapa;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 229
    invoke-direct {p0, p1, p2}, Lape;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/Class;)Lape;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lape;"
        }
    .end annotation

    .prologue
    .line 281
    invoke-virtual {p0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    and-int/lit8 v0, v0, 0x5

    if-eqz v0, :cond_b

    sget-object v0, Lape;->a:Lape;

    :goto_a
    return-object v0

    :cond_b
    sget-object v0, Lape;->b:Lape;

    goto :goto_a
.end method

.method public static valueOf(Ljava/lang/String;)Lape;
    .registers 2
    .parameter

    .prologue
    .line 229
    const-class v0, Lape;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lape;

    return-object v0
.end method

.method public static values()[Lape;
    .registers 1

    .prologue
    .line 229
    sget-object v0, Lape;->a:[Lape;

    invoke-virtual {v0}, [Lape;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lape;

    return-object v0
.end method
