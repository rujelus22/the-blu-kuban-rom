.class public Luw;
.super Ljava/lang/Object;
.source "FontPicker.java"


# instance fields
.field private final a:Landroid/app/Activity;

.field private a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/RadioGroup;

.field private a:Ljava/lang/Integer;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LuE",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private a:LuC;

.field private a:LuD;

.field a:LxI;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Z

.field private final a:[I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LuE",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Luw;->a:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Luw;->b:Ljava/util/List;

    .line 74
    iput-object v1, p0, Luw;->a:Landroid/view/View$OnClickListener;

    .line 75
    iput-object v1, p0, Luw;->a:LuD;

    .line 80
    iput-object v1, p0, Luw;->a:Landroid/view/View;

    .line 106
    iput-object p1, p0, Luw;->a:Landroid/app/Activity;

    .line 107
    iput-boolean p2, p0, Luw;->a:Z

    .line 108
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lsz;->kix_font_picker_sizes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Luw;->a:[I

    .line 109
    invoke-static {p1}, LdY;->a(Landroid/content/Context;)Laoo;

    move-result-object v0

    invoke-interface {v0, p0}, Laoo;->a(Ljava/lang/Object;)V

    .line 110
    return-void
.end method

.method static synthetic a(Luw;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Luw;->a:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic a(Luw;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Luw;->a:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Luw;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Luw;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Luw;LuC;)LuC;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Luw;->a:LuC;

    return-object p1
.end method

.method static synthetic a(Luw;)LuD;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Luw;->a:LuD;

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/widget/RadioButton;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 368
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 370
    sget v0, LsD;->properties_scroller:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 371
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 372
    new-instance v2, LuB;

    invoke-direct {v2, p0, p2, v0, p1}, LuB;-><init>(Luw;Landroid/widget/RadioButton;Landroid/widget/ScrollView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 380
    return-void
.end method

.method static synthetic a(Luw;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1}, Luw;->d(Landroid/view/View;)V

    return-void
.end method

.method private b()V
    .registers 4

    .prologue
    .line 247
    new-instance v0, Ljava/util/TreeSet;

    iget-object v1, p0, Luw;->a:LxI;

    invoke-interface {v1}, LxI;->a()Lajm;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    .line 250
    iget-object v1, p0, Luw;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 251
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 252
    new-instance v2, LuE;

    invoke-direct {v2, v0, v0}, LuE;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    new-instance v0, Luz;

    invoke-direct {v0, p0, v2}, Luz;-><init>(Luw;LuE;)V

    invoke-virtual {v2, v0}, LuE;->a(Landroid/view/View$OnClickListener;)V

    .line 262
    iget-object v0, p0, Luw;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 264
    :cond_33
    return-void
.end method

.method private b(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 193
    iget-boolean v0, p0, Luw;->a:Z

    if-eqz v0, :cond_16

    .line 194
    sget v0, LsD;->back_to_keyboard:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 195
    new-instance v1, Lux;

    invoke-direct {v1, p0}, Lux;-><init>(Luw;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    :cond_16
    return-void
.end method

.method static synthetic b(Luw;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1}, Luw;->e(Landroid/view/View;)V

    return-void
.end method

.method private c()V
    .registers 8

    .prologue
    .line 299
    iget-object v0, p0, Luw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 300
    iget-object v1, p0, Luw;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_9
    if-ge v0, v2, :cond_39

    aget v3, v1, v0

    .line 301
    new-instance v4, LuE;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "pt"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v3}, LuE;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    new-instance v3, LuA;

    invoke-direct {v3, p0, v4}, LuA;-><init>(Luw;LuE;)V

    invoke-virtual {v4, v3}, LuE;->a(Landroid/view/View$OnClickListener;)V

    .line 311
    iget-object v3, p0, Luw;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 313
    :cond_39
    return-void
.end method

.method private c(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 215
    invoke-direct {p0}, Luw;->b()V

    .line 216
    invoke-direct {p0}, Luw;->c()V

    .line 218
    sget v0, LsD;->font_family:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 220
    sget-object v1, LuC;->a:LuC;

    iput-object v1, p0, Luw;->a:LuC;

    .line 221
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 223
    sget v0, LsD;->font_mode:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 224
    new-instance v1, Luy;

    invoke-direct {v1, p0, p1}, Luy;-><init>(Luw;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 239
    sget v0, LsD;->font_properties_list:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Luw;->a:Landroid/widget/RadioGroup;

    .line 240
    invoke-direct {p0, p1}, Luw;->d(Landroid/view/View;)V

    .line 241
    return-void
.end method

.method private d(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    .line 273
    iget-object v0, p0, Luw;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 275
    const/4 v2, 0x0

    .line 276
    iget-object v0, p0, Luw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuE;

    .line 277
    iget-object v1, p0, Luw;->a:Landroid/app/Activity;

    iget-object v4, p0, Luw;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1, v4}, LuE;->a(Landroid/app/Activity;Landroid/view/ViewGroup;)Landroid/widget/RadioButton;

    move-result-object v1

    .line 279
    invoke-virtual {v0}, LuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 280
    iget-object v4, p0, Luw;->a:LxI;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v0, v5}, LxI;->a(Ljava/lang/String;Ljava/lang/Integer;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 282
    iget-object v4, p0, Luw;->a:Ljava/lang/String;

    if-eqz v4, :cond_4e

    iget-object v4, p0, Luw;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4e

    move-object v0, v1

    :goto_41
    move-object v2, v0

    .line 285
    goto :goto_c

    .line 286
    :cond_43
    iget-object v0, p0, Luw;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 288
    if-eqz v2, :cond_4d

    .line 289
    invoke-direct {p0, p1, v2}, Luw;->a(Landroid/view/View;Landroid/widget/RadioButton;)V

    .line 291
    :cond_4d
    return-void

    :cond_4e
    move-object v0, v2

    goto :goto_41
.end method

.method private e(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    .line 322
    iget-object v0, p0, Luw;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 324
    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Luw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuE;

    .line 326
    iget-object v1, p0, Luw;->a:Landroid/app/Activity;

    iget-object v4, p0, Luw;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1, v4}, LuE;->a(Landroid/app/Activity;Landroid/view/ViewGroup;)Landroid/widget/RadioButton;

    move-result-object v1

    .line 327
    iget-object v4, p0, Luw;->a:Ljava/lang/Integer;

    if-eqz v4, :cond_3a

    iget-object v4, p0, Luw;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, LuE;->a()Ljava/lang/Object;

    move-result-object v0

    if-ne v4, v0, :cond_3a

    move-object v0, v1

    :goto_2d
    move-object v2, v0

    .line 330
    goto :goto_c

    .line 331
    :cond_2f
    iget-object v0, p0, Luw;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 333
    if-eqz v2, :cond_39

    .line 334
    invoke-direct {p0, p1, v2}, Luw;->a(Landroid/view/View;Landroid/widget/RadioButton;)V

    .line 336
    :cond_39
    return-void

    :cond_3a
    move-object v0, v2

    goto :goto_2d
.end method


# virtual methods
.method public a()Ljava/lang/Integer;
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Luw;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Luw;->a:Ljava/lang/String;

    return-object v0
.end method

.method a()V
    .registers 5

    .prologue
    .line 342
    iget-object v0, p0, Luw;->a:LuC;

    sget-object v1, LuC;->a:LuC;

    if-ne v0, v1, :cond_32

    .line 343
    iget-object v0, p0, Luw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuE;

    .line 344
    iget-object v2, p0, Luw;->a:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Luw;->a:Ljava/lang/String;

    invoke-virtual {v0}, LuE;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 345
    iget-object v1, p0, Luw;->a:Landroid/view/View;

    invoke-virtual {v0}, LuE;->a()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Luw;->a(Landroid/view/View;Landroid/widget/RadioButton;)V

    .line 358
    :cond_31
    :goto_31
    return-void

    .line 349
    :cond_32
    iget-object v0, p0, Luw;->a:LuC;

    sget-object v1, LuC;->b:LuC;

    if-ne v0, v1, :cond_31

    .line 350
    iget-object v0, p0, Luw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuE;

    .line 351
    iget-object v2, p0, Luw;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_3e

    iget-object v2, p0, Luw;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, LuE;->a()Ljava/lang/Object;

    move-result-object v3

    if-ne v2, v3, :cond_3e

    .line 353
    iget-object v1, p0, Luw;->a:Landroid/view/View;

    invoke-virtual {v0}, LuE;->a()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Luw;->a(Landroid/view/View;Landroid/widget/RadioButton;)V

    goto :goto_31
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 184
    iput-object p1, p0, Luw;->a:Landroid/view/View$OnClickListener;

    .line 185
    return-void
.end method

.method public a(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 162
    iput-object p1, p0, Luw;->a:Landroid/view/View;

    .line 163
    invoke-direct {p0, p1}, Luw;->b(Landroid/view/View;)V

    .line 164
    invoke-direct {p0, p1}, Luw;->c(Landroid/view/View;)V

    .line 165
    return-void
.end method

.method public a(Ljava/lang/Integer;)V
    .registers 3
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Luw;->a:Ljava/lang/Integer;

    if-eq v0, p1, :cond_9

    .line 142
    iput-object p1, p0, Luw;->a:Ljava/lang/Integer;

    .line 143
    invoke-virtual {p0}, Luw;->a()V

    .line 145
    :cond_9
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Luw;->a:Ljava/lang/String;

    if-eq v0, p1, :cond_9

    .line 120
    iput-object p1, p0, Luw;->a:Ljava/lang/String;

    .line 121
    invoke-virtual {p0}, Luw;->a()V

    .line 123
    :cond_9
    return-void
.end method

.method public a(LuD;)V
    .registers 2
    .parameter

    .prologue
    .line 174
    iput-object p1, p0, Luw;->a:LuD;

    .line 175
    return-void
.end method
