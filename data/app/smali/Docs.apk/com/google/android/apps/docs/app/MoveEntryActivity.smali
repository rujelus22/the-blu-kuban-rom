.class public Lcom/google/android/apps/docs/app/MoveEntryActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "MoveEntryActivity.java"


# instance fields
.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LhS;

.field private a:Ljava/lang/String;

.field private a:LkY;

.field public a:LlE;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 121
    return-void
.end method

.method private a(LkH;LkH;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 301
    if-eqz p1, :cond_1e

    .line 302
    invoke-virtual {p1}, LkH;->d()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 303
    invoke-virtual {p2}, LkH;->d()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 304
    sget v0, Len;->move_shared_to_shared:I

    .line 319
    :cond_11
    :goto_11
    return v0

    .line 306
    :cond_12
    sget v0, Len;->move_shared_to_unshared:I

    goto :goto_11

    .line 309
    :cond_15
    invoke-virtual {p2}, LkH;->d()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 310
    sget v0, Len;->move_unshared_to_shared:I

    goto :goto_11

    .line 316
    :cond_1e
    invoke-virtual {p2}, LkH;->d()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 317
    sget v0, Len;->move_unshared_to_shared:I

    goto :goto_11
.end method

.method public static a(Landroid/content/Context;LkY;)Landroid/content/Intent;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 425
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/MoveEntryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 426
    invoke-virtual {p1, v0}, LkY;->a(Landroid/content/Intent;)V

    .line 427
    return-object v0
.end method

.method private a()LhK;
    .registers 4

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()LkO;

    move-result-object v0

    .line 196
    if-nez v0, :cond_9

    .line 197
    sget-object v0, LhK;->f:LhK;

    .line 207
    :goto_8
    return-object v0

    .line 200
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    invoke-interface {v1, v0}, Llf;->a(LkO;)Ljava/util/Map;

    move-result-object v1

    .line 203
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_1c

    .line 204
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(LkO;)V

    .line 205
    sget-object v0, LhK;->a:LhK;

    goto :goto_8

    .line 207
    :cond_1c
    sget-object v0, LhK;->b:LhK;

    goto :goto_8
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LhK;
    .registers 2
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()LhK;

    move-result-object v0

    return-object v0
.end method

.method private a()LhR;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 362
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()LkO;

    move-result-object v3

    .line 363
    if-nez v3, :cond_8

    .line 386
    :cond_7
    :goto_7
    return-object v1

    .line 367
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    invoke-interface {v0, v3}, Llf;->a(LkO;)Ljava/util/Map;

    move-result-object v0

    .line 370
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v4, 0x2

    if-ge v2, v4, :cond_7

    .line 375
    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    iget-object v4, v4, LkY;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v2

    .line 376
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4a

    .line 377
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LajB;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkX;

    .line 378
    iget-object v4, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    invoke-virtual {v0}, LkX;->b()J

    move-result-wide v5

    invoke-interface {v4, v2, v5, v6}, Llf;->a(LkB;J)LkH;

    move-result-object v0

    .line 381
    :goto_39
    iget-object v4, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    iget-object v5, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Ljava/lang/String;

    invoke-interface {v4, v2, v5}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v4

    .line 382
    if-eqz v4, :cond_7

    .line 386
    new-instance v2, LhR;

    invoke-direct {v2, v3, v0, v4, v1}, LhR;-><init>(LkO;LkH;LkH;LhF;)V

    move-object v1, v2

    goto :goto_7

    :cond_4a
    move-object v0, v1

    goto :goto_39
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LhS;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    return-object v0
.end method

.method private a()LkO;
    .registers 4

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    iget-object v1, v1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 396
    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    iget-object v2, v2, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 397
    return-object v0
.end method

.method private a(LkO;)V
    .registers 5
    .parameter

    .prologue
    .line 401
    invoke-static {p0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 402
    sget v0, Len;->move:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 404
    invoke-virtual {p1}, LkO;->o()Z

    move-result v0

    if-eqz v0, :cond_31

    sget v0, Len;->move_multi_parent_folder:I

    .line 406
    :goto_11
    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lfg;

    invoke-virtual {v2, v0}, Lfg;->a(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 407
    const v0, 0x104000a

    new-instance v2, LhI;

    invoke-direct {v2, p0}, LhI;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 414
    new-instance v0, LhJ;

    invoke-direct {v0, p0}, LhJ;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 421
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 422
    return-void

    .line 404
    :cond_31
    sget v0, Len;->move_multi_parent_file:I

    goto :goto_11
.end method

.method private b()LhK;
    .registers 7

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()LkO;

    move-result-object v1

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    invoke-interface {v0, v1}, Llf;->a(LkO;)Ljava/util/Map;

    move-result-object v2

    .line 218
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 219
    const-string v0, "root"

    .line 229
    :goto_12
    iget-object v3, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    iget-object v3, v3, LkY;->a:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a(Landroid/content/Context;Ljava/lang/String;)LhX;

    move-result-object v3

    invoke-virtual {v3, v0}, LhX;->a(Ljava/lang/String;)LhX;

    move-result-object v0

    sget-object v3, LkP;->h:LkP;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v0, v3}, LhX;->a(Ljava/util/EnumSet;)LhX;

    move-result-object v0

    sget v3, Len;->move:I

    invoke-virtual {v0, v3}, LhX;->a(I)LhX;

    move-result-object v0

    .line 234
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_42

    .line 235
    invoke-virtual {v0}, LhX;->a()LhX;

    .line 236
    instance-of v1, v1, LkH;

    if-eqz v1, :cond_42

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    iget-object v1, v1, LkY;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LhX;->b(Ljava/lang/String;)LhX;

    .line 241
    :cond_42
    invoke-virtual {v0}, LhX;->a()Landroid/content/Intent;

    move-result-object v0

    .line 242
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 243
    sget-object v0, LhK;->c:LhK;

    return-object v0

    .line 222
    :cond_4d
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v3}, LajB;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    iget-object v5, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    iget-object v5, v5, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v5}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 224
    iget-object v5, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    invoke-interface {v5, v0, v3, v4}, Llf;->a(LkB;J)LkH;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, LkH;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_12
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LhK;
    .registers 2
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->b()LhK;

    move-result-object v0

    return-object v0
.end method

.method private c()LhK;
    .registers 8

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()LhR;

    move-result-object v0

    .line 248
    if-nez v0, :cond_9

    .line 249
    sget-object v0, LhK;->a:LhK;

    .line 292
    :goto_8
    return-object v0

    .line 252
    :cond_9
    iget-object v1, v0, LhR;->a:LkO;

    .line 253
    iget-object v2, v0, LhR;->a:LkH;

    .line 254
    iget-object v3, v0, LhR;->b:LkH;

    .line 256
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(LkH;LkH;)I

    move-result v4

    .line 257
    if-gtz v4, :cond_18

    .line 258
    sget-object v0, LhK;->e:LhK;

    goto :goto_8

    .line 261
    :cond_18
    invoke-static {p0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 262
    sget v0, Len;->move_confirm_dialog_title:I

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 264
    if-eqz v2, :cond_64

    invoke-virtual {v2}, LkH;->c()Ljava/lang/String;

    move-result-object v0

    .line 265
    :goto_27
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, LkO;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    const/4 v1, 0x1

    aput-object v0, v2, v1

    const/4 v0, 0x2

    invoke-virtual {v3}, LkH;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-virtual {p0, v4, v2}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 267
    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 269
    sget v0, Len;->move:I

    new-instance v1, LhF;

    invoke-direct {v1, p0}, LhF;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 276
    const/high16 v0, 0x104

    new-instance v1, LhG;

    invoke-direct {v1, p0}, LhG;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 283
    new-instance v0, LhH;

    invoke-direct {v0, p0}, LhH;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 290
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 292
    sget-object v0, LhK;->d:LhK;

    goto :goto_8

    .line 264
    :cond_64
    const-string v0, ""

    goto :goto_27
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LhK;
    .registers 2
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->c()LhK;

    move-result-object v0

    return-object v0
.end method

.method private d()LhK;
    .registers 12

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 325
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()LhR;

    move-result-object v7

    .line 326
    if-nez v7, :cond_c

    .line 327
    sget-object v0, LhK;->a:LhK;

    .line 353
    :goto_b
    return-object v0

    .line 330
    :cond_c
    iget-object v0, v7, LhR;->a:LkH;

    .line 331
    if-eqz v0, :cond_5e

    invoke-virtual {v0}, LkH;->i()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 333
    :goto_15
    new-instance v1, LlQ;

    iget-object v0, v7, LhR;->a:LkO;

    iget-object v2, v7, LhR;->b:LkH;

    invoke-virtual {v2}, LkH;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v6, v2}, LlQ;-><init>(LkO;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    iget-object v2, v2, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v4

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LZM;

    invoke-static {p0, v0}, LlN;->a(Landroid/content/Context;LZM;)LlK;

    move-result-object v5

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LlE;

    iget-object v2, v7, LhR;->a:LkO;

    move-object v3, p0

    invoke-static/range {v0 .. v5}, LlN;->a(LlE;LlB;LkO;Landroid/content/Context;LkB;LlK;)I

    .line 342
    if-nez v6, :cond_61

    .line 343
    sget v0, Len;->move_toast_no_source_folder:I

    new-array v1, v10, [Ljava/lang/Object;

    iget-object v2, v7, LhR;->a:LkO;

    invoke-virtual {v2}, LkO;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    iget-object v2, v7, LhR;->b:LkH;

    invoke-virtual {v2}, LkH;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 351
    :goto_54
    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 353
    sget-object v0, LhK;->f:LhK;

    goto :goto_b

    .line 331
    :cond_5e
    const/4 v0, 0x0

    move-object v6, v0

    goto :goto_15

    .line 347
    :cond_61
    sget v0, Len;->move_toast_with_source_folder:I

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v7, LhR;->a:LkO;

    invoke-virtual {v2}, LkO;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    iget-object v2, v7, LhR;->a:LkH;

    invoke-virtual {v2}, LkH;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    iget-object v2, v7, LhR;->b:LkH;

    invoke-virtual {v2}, LkH;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_54
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LhK;
    .registers 2
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->d()LhK;

    move-result-object v0

    return-object v0
.end method

.method private e()LhK;
    .registers 2

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->finish()V

    .line 391
    sget-object v0, LhK;->f:LhK;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LhK;
    .registers 2
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->e()LhK;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 179
    if-nez p1, :cond_36

    .line 180
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2e

    .line 181
    sget-object v0, LhK;->c:LhK;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    invoke-virtual {v1}, LhS;->a()LhK;

    move-result-object v1

    invoke-virtual {v0, v1}, LhK;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 183
    const-string v0, "resourceId"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Ljava/lang/String;

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    sget-object v1, LhK;->d:LhK;

    invoke-virtual {v0, v1}, LhS;->a(LhK;)V

    .line 192
    :goto_2d
    return-void

    .line 187
    :cond_2e
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    sget-object v1, LhK;->f:LhK;

    invoke-virtual {v0, v1}, LhS;->a(LhK;)V

    goto :goto_2d

    .line 190
    :cond_36
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    invoke-virtual {v0}, LhS;->a()V

    goto :goto_2d
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 150
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LkY;->a(Landroid/content/Intent;)LkY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LkY;

    .line 154
    const/4 v0, 0x0

    .line 155
    if-eqz p1, :cond_20

    .line 156
    const-string v0, "movingState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LhK;

    .line 157
    const-string v1, "collectionResourceId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Ljava/lang/String;

    .line 160
    :cond_20
    if-nez v0, :cond_24

    .line 161
    sget-object v0, LhK;->a:LhK;

    .line 164
    :cond_24
    new-instance v1, LhS;

    invoke-direct {v1, p0, v0}, LhS;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;LhK;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    invoke-virtual {v0}, LhS;->a()V

    .line 166
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 170
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 171
    const-string v0, "movingState"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LhS;

    invoke-virtual {v1}, LhS;->a()LhK;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 172
    const-string v0, "collectionResourceId"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method
