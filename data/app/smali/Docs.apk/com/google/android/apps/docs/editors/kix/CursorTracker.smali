.class public Lcom/google/android/apps/docs/editors/kix/CursorTracker;
.super Ljava/lang/Object;
.source "CursorTracker.java"


# instance fields
.field private final a:Landroid/text/Spannable;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/docs/editors/kix/CursorTracker$Session;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/text/Spannable;)V
    .registers 3
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Ljava/util/Map;

    .line 87
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Landroid/text/Spannable;

    .line 88
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/docs/editors/kix/CursorTracker;)Landroid/text/Spannable;
    .registers 2
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Landroid/text/Spannable;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/CursorTracker$Session;

    .line 125
    if-eqz v0, :cond_f

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Landroid/text/Spannable;

    invoke-interface {v1, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 128
    :cond_f
    return-void
.end method

.method public a(Ljava/lang/String;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/CursorTracker$Session;

    .line 112
    if-nez v0, :cond_b

    .line 117
    :goto_a
    return-void

    .line 116
    :cond_b
    invoke-virtual {v0, p3, p2}, Lcom/google/android/apps/docs/editors/kix/CursorTracker$Session;->a(II)V

    goto :goto_a
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/CursorTracker$Session;

    .line 98
    if-eqz v0, :cond_b

    .line 104
    :goto_a
    return-void

    .line 102
    :cond_b
    new-instance v0, Lcom/google/android/apps/docs/editors/kix/CursorTracker$Session;

    invoke-direct {v0, p0, p3, p2}, Lcom/google/android/apps/docs/editors/kix/CursorTracker$Session;-><init>(Lcom/google/android/apps/docs/editors/kix/CursorTracker;ILjava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a
.end method
