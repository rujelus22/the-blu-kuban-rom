.class public Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;
.super Landroid/view/ViewGroup;
.source "SpreadsheetView.java"


# instance fields
.field private a:I

.field private a:LIb;

.field private a:LJv;

.field private a:Landroid/view/View;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

.field private a:Z

.field private b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

.field private b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

.field private c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

.field private c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

.field private d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

.field private d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:I

    .line 61
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->setFocusable(Z)V

    .line 62
    return-void
.end method

.method private b(I)V
    .registers 7
    .parameter

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getBottom()I

    move-result v1

    sub-int/2addr v1, p1

    .line 170
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 172
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;
    .registers 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;
    .registers 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;
    .registers 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    return-object v0
.end method

.method public a(II)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    invoke-virtual {v0}, LJv;->b()I

    move-result v0

    .line 254
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    invoke-virtual {v1}, LJv;->a()I

    move-result v1

    .line 256
    if-lt p1, v0, :cond_16

    if-lt p2, v1, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    :goto_12
    return-object v0

    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    goto :goto_12

    :cond_16
    if-lt p2, v1, :cond_1b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    goto :goto_12

    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    goto :goto_12
.end method

.method public a(Landroid/graphics/Point;)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;
    .registers 4
    .parameter

    .prologue
    .line 229
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v1

    if-lt v0, v1, :cond_42

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getRight()I

    move-result v1

    if-gt v0, v1, :cond_42

    .line 230
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v1

    if-lt v0, v1, :cond_2b

    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getBottom()I

    move-result v1

    if-gt v0, v1, :cond_2b

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    .line 249
    :goto_2a
    return-object v0

    .line 234
    :cond_2b
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v1

    if-lt v0, v1, :cond_42

    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getBottom()I

    move-result v1

    if-gt v0, v1, :cond_42

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    goto :goto_2a

    .line 239
    :cond_42
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v1

    if-lt v0, v1, :cond_84

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getRight()I

    move-result v1

    if-gt v0, v1, :cond_84

    .line 240
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v1

    if-lt v0, v1, :cond_6d

    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getBottom()I

    move-result v1

    if-gt v0, v1, :cond_6d

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    goto :goto_2a

    .line 244
    :cond_6d
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v1

    if-lt v0, v1, :cond_84

    iget v0, p1, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getBottom()I

    move-result v1

    if-gt v0, v1, :cond_84

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    goto :goto_2a

    .line 249
    :cond_84
    const/4 v0, 0x0

    goto :goto_2a
.end method

.method public a()V
    .registers 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_e

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 178
    :cond_e
    return-void
.end method

.method public a(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, -0x2

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    if-nez v0, :cond_16

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LsF;->trix_loading_indicator:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    .line 157
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2d

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Z

    .line 164
    :goto_2c
    return-void

    .line 162
    :cond_2d
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b(I)V

    goto :goto_2c
.end method

.method public b()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;
    .registers 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    return-object v0
.end method

.method public b()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;
    .registers 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    return-object v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_e

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 269
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_1c

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()V

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 273
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_2a

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()V

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 277
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_38

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()V

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 282
    :cond_38
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    if-eqz v0, :cond_46

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 286
    :cond_46
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    if-eqz v0, :cond_54

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 290
    :cond_54
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    if-eqz v0, :cond_62

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 294
    :cond_62
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    if-eqz v0, :cond_70

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 298
    :cond_70
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    if-eqz v0, :cond_79

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->removeView(Landroid/view/View;)V

    .line 301
    :cond_79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    .line 302
    return-void
.end method

.method public c()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;
    .registers 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    return-object v0
.end method

.method public c()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;
    .registers 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    return-object v0
.end method

.method public computeScroll()V
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LIb;

    if-eqz v0, :cond_9

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LIb;

    invoke-virtual {v0}, LIb;->f()V

    .line 148
    :cond_9
    return-void
.end method

.method public d()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;
    .registers 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    return-object v0
.end method

.method public d()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;
    .registers 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 188
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    if-nez v4, :cond_5

    .line 226
    :cond_4
    :goto_4
    return-void

    .line 192
    :cond_5
    sub-int v8, p4, p2

    .line 193
    sub-int v9, p5, p3

    .line 195
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v4}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()I

    move-result v10

    .line 196
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v4}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b()I

    move-result v11

    .line 198
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v4}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->b()I

    move-result v7

    .line 199
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {v4}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()I

    move-result v5

    .line 204
    if-lez v11, :cond_87

    add-int v4, v7, v11

    add-int/lit8 v4, v4, 0x2

    move v6, v4

    .line 205
    :goto_28
    if-lez v10, :cond_89

    add-int v4, v5, v10

    add-int/lit8 v4, v4, 0x2

    .line 207
    :goto_2e
    iget-object v12, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    const/4 v13, 0x0

    add-int v14, v5, v10

    invoke-virtual {v12, v5, v13, v14, v7}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->layout(IIII)V

    .line 209
    iget-object v12, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    const/4 v13, 0x0

    invoke-virtual {v12, v4, v13, v8, v7}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->layout(IIII)V

    .line 211
    iget-object v12, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    const/4 v13, 0x0

    add-int v14, v7, v11

    invoke-virtual {v12, v13, v7, v5, v14}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->layout(IIII)V

    .line 213
    iget-object v12, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v6, v5, v9}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->layout(IIII)V

    .line 216
    iget-object v12, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    add-int v13, v5, v10

    add-int v14, v7, v11

    invoke-virtual {v12, v5, v7, v13, v14}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->layout(IIII)V

    .line 217
    iget-object v12, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    add-int/2addr v11, v7

    invoke-virtual {v12, v4, v7, v8, v11}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->layout(IIII)V

    .line 218
    iget-object v7, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    add-int/2addr v10, v5

    invoke-virtual {v7, v5, v6, v10, v9}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->layout(IIII)V

    .line 219
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v5, v4, v6, v8, v9}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->layout(IIII)V

    .line 220
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->layout(IIII)V

    .line 222
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->indexOfChild(Landroid/view/View;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    iget-boolean v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Z

    if-eqz v4, :cond_4

    .line 223
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b(I)V

    .line 224
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Z

    goto/16 :goto_4

    :cond_87
    move v6, v7

    .line 204
    goto :goto_28

    :cond_89
    move v4, v5

    .line 205
    goto :goto_2e
.end method

.method protected onMeasure(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->measureChildren(II)V

    .line 183
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 184
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 306
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LIb;

    if-eqz v0, :cond_c

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LIb;

    invoke-virtual {v0, p1, p2, p3, p4}, LIb;->a(IIII)V

    .line 310
    :cond_c
    return-void
.end method

.method public setActiveSheet(ILJA;LHT;LGY;LIb;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()V

    .line 71
    const v0, -0xbbbbbc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->setBackgroundColor(I)V

    .line 73
    iput p1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:I

    .line 74
    invoke-virtual {p2}, LJA;->a()LJv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    .line 76
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 79
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 82
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 85
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 88
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 91
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 94
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 97
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 100
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->addView(Landroid/view/View;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    invoke-virtual {v0}, LJv;->b()I

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    invoke-virtual {v0}, LJv;->a()I

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->g:LIL;

    invoke-static {v1, v2, p3}, LIB;->a(LJv;LIL;LHT;)LIB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(LIB;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->e:LIL;

    invoke-static {v1, v2, p3}, LIB;->a(LJv;LIL;LHT;)LIB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(LIB;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->d:LIL;

    invoke-static {v1, v2, p3}, LIB;->a(LJv;LIL;LHT;)LIB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(LIB;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->h:LIL;

    invoke-static {v1, v2, p3}, LIB;->a(LJv;LIL;LHT;)LIB;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(LIB;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->a:LIL;

    invoke-static {v1, v2}, LIG;->a(LJv;LIL;)LIG;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(LIG;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->b:LIL;

    invoke-static {v1, v2}, LIG;->a(LJv;LIL;)LIG;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(LIG;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->c:LIL;

    invoke-static {v1, v2}, LIG;->a(LJv;LIL;)LIG;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(LIG;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LJv;

    sget-object v2, LIL;->f:LIL;

    invoke-static {v1, v2}, LIG;->a(LJv;LIL;)LIG;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(LIG;)V

    .line 130
    iput-object p5, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:LIb;

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LJb;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a(LJb;Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;)V

    .line 134
    invoke-interface {p4}, LGY;->i()Z

    move-result v0

    if-eqz v0, :cond_13b

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->setShowDebugInfo(Z)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->setShowDebugInfo(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->setShowDebugInfo(Z)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->setShowDebugInfo(Z)V

    .line 140
    :cond_13b
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->requestLayout()V

    .line 141
    return-void
.end method
