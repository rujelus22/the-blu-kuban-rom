.class public Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;
.super Lcom/google/android/apps/docs/fragment/WebViewFragment;
.source "PunchWebViewFragment.java"

# interfaces
.implements LRb;
.implements LSg;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field a:I

.field private a:J

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNS;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LNU;

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LQS;

.field public a:LQV;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LQW;

.field public a:LRC;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LRD;

.field private a:LRR;

.field public a:LRU;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LRd;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LSd;

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZS;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Landroid/webkit/WebChromeClient;

.field private a:Landroid/widget/ProgressBar;

.field private a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

.field private a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ljava/lang/Class;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DocListActivity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/Object;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LQT;",
            ">;"
        }
    .end annotation
.end field

.field private a:LkY;

.field private a:LmB;

.field private final a:Lqd;

.field private a:Lqe;

.field private a:Z

.field private b:J

.field public b:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:Landroid/view/ViewGroup;

.field private b:Landroid/webkit/WebView;

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 137
    const-string v0, "[\\s]*?<\\?xml(.|[\\n])*?\\?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;-><init>()V

    .line 480
    new-instance v0, LRM;

    invoke-direct {v0, p0}, LRM;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lqd;

    .line 545
    new-instance v0, LRN;

    invoke-direct {v0, p0}, LRN;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebChromeClient;

    .line 634
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lqe;

    .line 643
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    .line 646
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Ljava/lang/Object;

    .line 649
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/Object;

    .line 655
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:I

    .line 663
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    return-void
.end method

.method private A()V
    .registers 4

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    if-eqz v0, :cond_10

    .line 1117
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    const-string v2, "webViewPunchApiLoadedDuration"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    .line 1120
    :cond_10
    return-void
.end method

.method private B()V
    .registers 3

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    if-eqz v0, :cond_e

    .line 1125
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LeQ;->b(Ljava/lang/Object;)V

    .line 1126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    .line 1128
    :cond_e
    return-void
.end method

.method private C()V
    .registers 4

    .prologue
    .line 1131
    invoke-static {}, LZL;->b()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1132
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRC;

    invoke-interface {v0}, LRC;->b()Z

    move-result v0

    .line 1133
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_16

    const/4 v0, 0x0

    :goto_11
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1136
    :cond_15
    return-void

    .line 1133
    :cond_16
    const/4 v0, 0x1

    goto :goto_11
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)J
    .registers 3
    .parameter

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:J

    return-wide v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/webkit/WebView;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/widget/ProgressBar;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method public static a(Landroid/net/Uri;LkY;Ljava/lang/String;)Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 145
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in createFragment"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 147
    const-string v1, "url"

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v1, "resId"

    iget-object v2, p1, LkY;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v1, "accountName"

    iget-object v2, p1, LkY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;-><init>()V

    .line 152
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->d(Landroid/os/Bundle;)V

    .line 153
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LmB;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LmB;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LmB;)LmB;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LmB;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Lqe;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lqe;

    return-object v0
.end method

.method private a(ILQT;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 980
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:I

    .line 982
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 983
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNU;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.punchWebViewEventListener.requestSlideContent("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LNU;->a(Ljava/lang/String;)V

    .line 985
    return-void
.end method

.method private a(LRR;)V
    .registers 5
    .parameter

    .prologue
    .line 1083
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRR;

    if-eqz v0, :cond_14

    .line 1084
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRR;

    invoke-static {v2}, LRR;->a(LRR;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1085
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Ljava/lang/Object;

    .line 1088
    :cond_14
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRR;

    .line 1090
    if-eqz p1, :cond_26

    .line 1091
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Ljava/lang/Object;

    .line 1092
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LeQ;->a(Ljava/lang/Object;)V

    .line 1094
    :cond_26
    return-void
.end method

.method private a(LSd;)V
    .registers 5
    .parameter

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LSd;

    if-eqz v0, :cond_14

    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LSd;

    invoke-static {v2}, LSd;->a(LSd;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1099
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/Object;

    .line 1102
    :cond_14
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LSd;

    .line 1104
    if-eqz p1, :cond_26

    .line 1105
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/Object;

    .line 1106
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LeQ;->a(Ljava/lang/Object;)V

    .line 1108
    :cond_26
    return-void
.end method

.method static a(Landroid/view/View;LRb;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 188
    new-instance v0, LRK;

    invoke-direct {v0, p0, p1}, LRK;-><init>(Landroid/view/View;LRb;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 203
    new-instance v0, LRL;

    invoke-direct {v0, p0, p1}, LRL;-><init>(Landroid/view/View;LRb;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    return-void
.end method

.method public static a(Landroid/webkit/WebView;Landroid/content/res/Resources;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 158
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in commonWebViewConfiguration"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {p0, v2}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 160
    invoke-virtual {p0, v2}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 161
    sget v0, Lee;->punch_grey_background:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 162
    invoke-virtual {p0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 163
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 164
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setPluginsEnabled(Z)V

    .line 165
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 166
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 167
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 168
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 169
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 170
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 171
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 172
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 173
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 175
    invoke-static {p1}, LZL;->c(Landroid/content/res/Resources;)Z

    move-result v0

    .line 176
    if-eqz v0, :cond_55

    sget-object v0, Landroid/webkit/WebSettings$ZoomDensity;->FAR:Landroid/webkit/WebSettings$ZoomDensity;

    :goto_45
    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setDefaultZoom(Landroid/webkit/WebSettings$ZoomDensity;)V

    .line 179
    invoke-static {}, LZL;->b()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 180
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 183
    :cond_51
    invoke-virtual {v1, p2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 184
    return-void

    .line 176
    :cond_55
    sget-object v0, Landroid/webkit/WebSettings$ZoomDensity;->MEDIUM:Landroid/webkit/WebSettings$ZoomDensity;

    goto :goto_45
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->A()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;ILQT;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(ILQT;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V
    .registers 4
    .parameter

    .prologue
    .line 788
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQS;

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 789
    new-instance v0, LRO;

    invoke-direct {v0, p0, p1}, LRO;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQS;

    .line 800
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQS;

    invoke-virtual {v0, v1}, LQV;->a(LQS;)V

    .line 801
    return-void

    .line 788
    :cond_17
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)J
    .registers 3
    .parameter

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    return-wide v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->q()V

    return-void
.end method

.method private b(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V
    .registers 4
    .parameter

    .prologue
    .line 839
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->a()I

    move-result v0

    .line 840
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v1}, LQV;->b()I

    move-result v1

    .line 841
    if-lez v1, :cond_14

    .line 842
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 843
    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;->setAspect(F)V

    .line 845
    :cond_14
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->u()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->C()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic m(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic n(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic o(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic p(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic q(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    return-object v0
.end method

.method private q()V
    .registers 7

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v2

    .line 674
    sget v0, Len;->punch_open_failed:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 675
    sget v0, Len;->punch_open_failed_expanded:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 676
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Lo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LkY;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(Lo;LkY;LfS;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 678
    return-void
.end method

.method private r()V
    .registers 3

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQS;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 805
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQS;

    invoke-virtual {v0, v1}, LQV;->b(LQS;)V

    .line 806
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQS;

    .line 807
    return-void
.end method

.method private s()V
    .registers 3

    .prologue
    .line 810
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRD;

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 811
    new-instance v0, LRP;

    invoke-direct {v0, p0}, LRP;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRD;

    .line 822
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRC;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRD;

    invoke-interface {v0, v1}, LRC;->a(LRD;)V

    .line 823
    return-void

    .line 810
    :cond_17
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private t()V
    .registers 3

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRD;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRC;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRD;

    invoke-interface {v0, v1}, LRC;->b(LRD;)V

    .line 828
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRD;

    .line 829
    return-void

    .line 826
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private u()V
    .registers 5

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRC;

    invoke-interface {v0}, LRC;->a()LRy;

    move-result-object v0

    invoke-virtual {v0}, LRy;->a()Z

    move-result v0

    .line 833
    const-string v1, "PunchWebViewFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "passTouchEventsToWebView = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNU;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.punchWebViewEventListener.setInterceptClicks("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v0, :cond_44

    const/4 v0, 0x1

    :goto_32
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LNU;->a(Ljava/lang/String;)V

    .line 836
    return-void

    .line 834
    :cond_44
    const/4 v0, 0x0

    goto :goto_32
.end method

.method private v()V
    .registers 5

    .prologue
    .line 911
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in loadJavascriptBridge"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    :try_start_7
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "javascript/punch/webview/PunchNativeMessagingBridge.js"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 916
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZS;

    invoke-interface {v1, v0}, LZS;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 917
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_20} :catch_26

    .line 922
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNU;

    invoke-interface {v0, v1}, LNU;->a(Ljava/lang/String;)V

    .line 923
    return-void

    .line 918
    :catch_26
    move-exception v0

    .line 919
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load Punch javascript bridge: ."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private w()V
    .registers 4

    .prologue
    .line 1041
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1042
    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1043
    const-string v1, "url"

    invoke-static {v0, v1}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    .line 1044
    const-string v1, "resId"

    invoke-static {v0, v1}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1045
    const-string v2, "accountName"

    invoke-static {v0, v2}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1046
    invoke-static {v2, v1}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LkY;

    .line 1047
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    const-string v2, "title"

    invoke-static {v0, v2}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LQV;->a(Ljava/lang/String;)V

    .line 1048
    return-void
.end method

.method private x()V
    .registers 3

    .prologue
    .line 1051
    new-instance v0, LRQ;

    invoke-direct {v0, p0}, LRQ;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQW;

    .line 1072
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQW;

    invoke-virtual {v0, v1}, LQV;->a(LQW;)V

    .line 1073
    return-void
.end method

.method private y()V
    .registers 2

    .prologue
    .line 1076
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1077
    invoke-static {v0}, LRR;->a(I)LRR;

    move-result-object v0

    .line 1079
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LRR;)V

    .line 1080
    return-void
.end method

.method private z()V
    .registers 3

    .prologue
    .line 1111
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    .line 1112
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LeQ;->a(Ljava/lang/Object;)V

    .line 1113
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 701
    const-string v0, "PunchWebViewFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onCreateView savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    .line 703
    sget v0, Lej;->punch_web_view_fragment:I

    invoke-virtual {p1, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 705
    sget v0, Leh;->loading_spinner:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/ProgressBar;

    .line 706
    sget v0, Leh;->punch_detachable_slide_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    .line 709
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->z()V

    .line 714
    sget v0, Leh;->punch_web_view_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/view/ViewGroup;

    .line 715
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 716
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_159

    .line 717
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:J

    .line 718
    iput-boolean v9, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    .line 720
    new-instance v0, Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2, v10}, Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    .line 721
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    .line 726
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNS;

    invoke-interface {v3}, LNS;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/webkit/WebView;Landroid/content/res/Resources;Ljava/lang/String;)V

    .line 728
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->C()V

    .line 729
    new-instance v0, LNV;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-direct {v0, v2}, LNV;-><init>(Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNU;

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 732
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->x()V

    .line 733
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->w()V

    .line 734
    new-instance v0, LRS;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lqd;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LkY;

    iget-object v3, v3, LkY;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v3}, LRS;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Lqd;Ljava/lang/String;)V

    .line 736
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 737
    new-instance v2, Lqc;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v4}, LQV;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LPm;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LPm;->a(Landroid/content/Context;Landroid/net/Uri;)LPp;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lqc;-><init>(Ljava/lang/String;Ljava/lang/String;LPp;)V

    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lqe;

    .line 739
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    new-instance v3, LRV;

    invoke-direct {v3, p0, v10}, LRV;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LRK;)V

    const-string v4, "WebViewApi"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 740
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->v()V

    .line 741
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LRS;->a(Ljava/lang/String;)V

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    .line 744
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->s()V

    .line 745
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->a()I

    move-result v0

    if-eq v0, v8, :cond_fe

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->b()I

    move-result v0

    if-eq v0, v8, :cond_fe

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    .line 749
    :cond_fe
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->u()V

    .line 755
    :goto_101
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 756
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRC;

    invoke-interface {v2}, LRC;->a()Z

    move-result v2

    if-nez v2, :cond_164

    invoke-static {}, LZL;->b()Z

    move-result v2

    if-eqz v2, :cond_164

    .line 757
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 758
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    sget v2, Leh;->editor_state_view:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 760
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRd;

    invoke-virtual {v2, v0}, LRd;->a(Landroid/widget/FrameLayout;)Landroid/webkit/WebView;

    move-result-object v0

    .line 768
    :goto_127
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setWebView(Landroid/webkit/WebView;)V

    .line 770
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/view/ViewGroup;

    invoke-static {v0, p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/view/View;LRb;)V

    .line 772
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    if-nez v0, :cond_141

    .line 773
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v9}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 777
    :cond_141
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->a()Z

    move-result v0

    if-eqz v0, :cond_150

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 781
    :cond_150
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->y()V

    .line 782
    sget-object v0, LSd;->b:LSd;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LSd;)V

    .line 784
    return-object v1

    .line 751
    :cond_159
    const-string v0, "PunchWebViewFragment"

    const-string v2, "Reusing saved webview"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->x()V

    goto :goto_101

    .line 762
    :cond_164
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v9}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_127
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 682
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a(Landroid/os/Bundle;)V

    .line 683
    const-string v0, "PunchWebViewFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onCreate savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 686
    return-void
.end method

.method public a(Landroid/webkit/WebView;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1016
    const-string v0, "PunchWebViewFragment"

    const-string v2, "in restoreWebView"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_16

    .line 1023
    const-string v0, "PunchWebViewFragment"

    const-string v1, "early exit in restoreWebView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    :goto_15
    return-void

    .line 1027
    :cond_16
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    if-eqz v0, :cond_39

    .line 1028
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    if-ne v0, p1, :cond_3c

    const/4 v0, 0x1

    :goto_1f
    invoke-static {v0}, Lagu;->b(Z)V

    .line 1029
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v2, Leh;->punch_web_view_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1031
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1032
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1035
    sget-object v0, LSd;->b:LSd;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LSd;)V

    .line 1037
    :cond_39
    iput-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    goto :goto_15

    :cond_3c
    move v0, v1

    .line 1028
    goto :goto_1f
.end method

.method public b()V
    .registers 3

    .prologue
    .line 951
    const-string v0, "PunchWebViewFragment"

    const-string v1, "requestSkipToPreviousSlide"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 953
    if-ltz v0, :cond_14

    .line 954
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(I)V

    .line 956
    :cond_14
    return-void
.end method

.method public b(I)V
    .registers 5
    .parameter

    .prologue
    .line 930
    const-string v0, "PunchWebViewFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestGoToSlide "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->c()I

    move-result v0

    if-nez v0, :cond_21

    .line 938
    :goto_20
    return-void

    .line 935
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Lagu;->b(II)I

    .line 936
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    .line 937
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNU;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.PUNCH_WEBVIEW_CONTROL_API.gotoSlide("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LNU;->a(Ljava/lang/String;)V

    goto :goto_20
.end method

.method public c()Landroid/webkit/WebView;
    .registers 3

    .prologue
    .line 989
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    if-nez v0, :cond_21

    .line 990
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, Leh;->punch_web_view_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 992
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 993
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 994
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    .line 1009
    sget-object v0, LSd;->a:LSd;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LSd;)V

    .line 1011
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    return-object v0
.end method

.method public c()V
    .registers 3

    .prologue
    .line 960
    const-string v0, "PunchWebViewFragment"

    const-string v1, "animateToNextState"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->c()I

    move-result v0

    if-nez v0, :cond_10

    .line 967
    :goto_f
    return-void

    .line 965
    :cond_10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    .line 966
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNU;

    const-string v1, "window.PUNCH_WEBVIEW_CONTROL_API.goNext();"

    invoke-interface {v0, v1}, LNU;->a(Ljava/lang/String;)V

    goto :goto_f
.end method

.method public d()V
    .registers 3

    .prologue
    .line 971
    const-string v0, "PunchWebViewFragment"

    const-string v1, "animateToPreviousState"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->c()I

    move-result v0

    if-nez v0, :cond_10

    .line 977
    :goto_f
    return-void

    .line 975
    :cond_10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    .line 976
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNU;

    const-string v1, "window.PUNCH_WEBVIEW_CONTROL_API.goPrev();"

    invoke-interface {v0, v1}, LNU;->a(Ljava/lang/String;)V

    goto :goto_f
.end method

.method public i()V
    .registers 3

    .prologue
    .line 690
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 691
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 695
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->i()V

    .line 696
    return-void
.end method

.method public j_()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 849
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onDestroyView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->B()V

    .line 851
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LRR;)V

    .line 852
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LSd;)V

    .line 856
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQT;

    .line 857
    invoke-interface {v0}, LQT;->a()V

    goto :goto_1b

    .line 859
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 861
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 862
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onDestroyView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQW;

    invoke-virtual {v0, v1}, LQV;->b(LQW;)V

    .line 867
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, Leh;->punch_web_view_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 868
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 869
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 870
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRd;

    invoke-virtual {v0}, LRd;->a()V

    .line 873
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->j_()V

    .line 874
    return-void
.end method

.method public m_()V
    .registers 3

    .prologue
    .line 942
    const-string v0, "PunchWebViewFragment"

    const-string v1, "requestSkipToNextSlide"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v0}, LQV;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 944
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    invoke-virtual {v1}, LQV;->c()I

    move-result v1

    if-ge v0, v1, :cond_1a

    .line 945
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(I)V

    .line 947
    :cond_1a
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 878
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 880
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->y()V

    .line 881
    return-void
.end method

.method public p()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 884
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in cleanup"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 886
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 887
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LmB;

    if-eqz v0, :cond_1c

    .line 888
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LmB;

    invoke-virtual {v0}, LmB;->a()V

    .line 889
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LmB;

    .line 891
    :cond_1c
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lqe;

    .line 893
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->t()V

    .line 894
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->r()V

    .line 898
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_31

    .line 899
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    const-string v1, "WebViewApi"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 902
    :cond_31
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()V

    .line 903
    return-void
.end method
