.class public Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "PunchWebViewActivity.java"

# interfaces
.implements LLK;
.implements LRU;
.implements LSi;


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LQN;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LQS;

.field public a:LQU;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LQV;

.field private a:LRD;

.field private final a:LRF;

.field private a:Landroid/support/v4/app/Fragment;

.field private a:Landroid/view/View;

.field private a:Landroid/view/ViewGroup;

.field private a:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

.field private a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

.field private a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

.field private a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

.field private a:Lcom/google/android/apps/docs/view/TitleBar;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 70
    new-instance v0, LQV;

    invoke-direct {v0}, LQV;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQV;

    .line 73
    new-instance v0, LRF;

    invoke-direct {v0}, LRF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)LRF;
    .registers 2
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 102
    sget-object v0, LkP;->c:LkP;

    invoke-static {p1, p2, p3, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LkP;)Landroid/content/Intent;

    move-result-object v0

    .line 104
    const-class v1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 105
    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;
    .registers 3

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    const-string v1, "FRAGMENT_TAG_FULLSCREEN"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 234
    instance-of v1, v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    if-eqz v1, :cond_11

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;
    .registers 2
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 148
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_9

    .line 149
    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    .line 154
    :goto_8
    return-void

    .line 151
    :cond_9
    const-string v0, "PunchWebViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to set rootView as received:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    goto :goto_8
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->h()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b(Z)V

    return-void
.end method

.method private a(LkY;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    .line 427
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Lz;LkY;Landroid/net/Uri;)V

    .line 432
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->d(Lz;)V

    .line 433
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->e(Lz;)V

    .line 434
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b(Lz;)V

    .line 435
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->c(Lz;)V

    .line 437
    invoke-virtual {v0}, Lz;->b()I

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Z

    .line 439
    return-void
.end method

.method private a(Lz;LkY;Landroid/net/Uri;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 502
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in addModelFragment"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_25

    .line 505
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    const-string v1, "webView"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_25

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQU;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    invoke-interface {v0, p3, p2, v1}, LQU;->a(Landroid/net/Uri;LkY;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    .line 511
    :cond_25
    sget v0, Leh;->web_view_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    const-string v2, "webView"

    invoke-virtual {p1, v0, v1, v2}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    .line 512
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->l()V

    return-void
.end method

.method private b(Lz;)V
    .registers 4
    .parameter

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    if-nez v0, :cond_e

    .line 478
    sget v0, Leh;->speaker_notes_presence_panel:I

    sget v1, Leh;->speaker_notes_content_panel:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a(II)Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    .line 482
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    const-string v1, "SpeakerNotesFragmentTag"

    invoke-virtual {p1, v0, v1}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    .line 483
    return-void
.end method

.method private b(Z)V
    .registers 6
    .parameter

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->f()Z

    move-result v0

    if-ne p1, v0, :cond_7

    .line 256
    :goto_6
    return-void

    .line 247
    :cond_7
    if-eqz p1, :cond_25

    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    .line 249
    invoke-static {}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    move-result-object v1

    .line 250
    sget v2, Leh;->main_container:I

    const-string v3, "FRAGMENT_TAG_FULLSCREEN"

    invoke-virtual {v0, v2, v1, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    .line 251
    const-string v1, "punchFullscreen"

    invoke-virtual {v0, v1}, Lz;->a(Ljava/lang/String;)Lz;

    .line 252
    invoke-virtual {v0}, Lz;->a()I

    goto :goto_6

    .line 254
    :cond_25
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->i()V

    goto :goto_6
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->s()V

    return-void
.end method

.method private c(Lz;)V
    .registers 5
    .parameter

    .prologue
    .line 486
    sget v0, Leh;->drag_knob_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_9

    .line 498
    :goto_8
    return-void

    .line 490
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    if-nez v0, :cond_28

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQN;

    invoke-virtual {v0}, LQN;->b()Z

    move-result v0

    .line 492
    if-nez v0, :cond_32

    const/4 v0, 0x1

    :goto_16
    sget v1, Len;->punch_drag_knob_button_content_description_close_slide_picker:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Len;->punch_drag_knob_button_content_description_open_slide_picker:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    .line 497
    :cond_28
    sget v0, Leh;->drag_knob_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    const-string v2, "dragKnob"

    invoke-virtual {p1, v0, v1, v2}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    goto :goto_8

    .line 492
    :cond_32
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private d(Lz;)V
    .registers 5
    .parameter

    .prologue
    .line 515
    sget v0, Leh;->grid_slide_picker_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    if-nez v0, :cond_12

    .line 518
    invoke-static {}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    .line 521
    :cond_12
    sget v0, Leh;->grid_slide_picker_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    const-string v2, "slidePicker"

    invoke-virtual {p1, v0, v1, v2}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    .line 524
    :cond_1b
    return-void
.end method

.method private e(Lz;)V
    .registers 5
    .parameter

    .prologue
    .line 527
    sget v0, Leh;->list_slide_picker_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 529
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    if-nez v0, :cond_12

    .line 530
    invoke-static {}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    .line 533
    :cond_12
    sget v0, Leh;->list_slide_picker_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    const-string v2, "slidePicker"

    invoke-virtual {p1, v0, v1, v2}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    .line 536
    :cond_1b
    return-void
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 239
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private g()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    if-eqz v0, :cond_e

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 214
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    .line 217
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_19

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 219
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 222
    :cond_19
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_22

    .line 224
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->p()V

    .line 226
    :cond_22
    return-void
.end method

.method private g()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 325
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->e()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 326
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->g()V

    .line 343
    :goto_b
    return v0

    .line 332
    :cond_c
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->f()Z

    move-result v2

    if-nez v2, :cond_2e

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    invoke-virtual {v2}, LRF;->c()Z

    move-result v2

    if-eqz v2, :cond_2e

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v2

    if-nez v2, :cond_2e

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    if-eqz v2, :cond_2e

    .line 334
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Z)V

    goto :goto_b

    .line 338
    :cond_2e
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->f()Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 339
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    invoke-virtual {v2, v1}, LRF;->a(Z)V

    goto :goto_b

    :cond_3a
    move v0, v1

    .line 343
    goto :goto_b
.end method

.method private h()V
    .registers 3

    .prologue
    .line 229
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->f()Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_9
    invoke-virtual {v1, v0}, LRF;->a(Z)V

    .line 230
    return-void

    .line 229
    :cond_d
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private i()V
    .registers 4

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    const-string v1, "punchFullscreen"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lo;->a(Ljava/lang/String;I)V

    .line 261
    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    .line 265
    sget v0, Leh;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-interface {v0, v1}, LMM;->a(LMZ;)V

    .line 267
    invoke-static {}, LZL;->a()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/view/TitleBar;->setVisibility(I)V

    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 271
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 273
    const/16 v1, 0x18

    invoke-virtual {v0, v2, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 277
    :cond_2d
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->l()V

    .line 278
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->k()V

    .line 279
    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    .line 385
    invoke-static {}, LZL;->a()Z

    move-result v0

    if-nez v0, :cond_f

    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LMM;

    move-result-object v0

    sget v1, Leg;->activity_icon_punch:I

    invoke-interface {v0, v1}, LMM;->a(I)V

    .line 388
    :cond_f
    return-void
.end method

.method private l()V
    .registers 6

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQV;

    invoke-virtual {v0}, LQV;->c()I

    move-result v0

    .line 393
    if-lez v0, :cond_31

    .line 394
    sget v1, Len;->punch_which_slide_is_displayed:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQV;

    invoke-virtual {v4}, LQV;->d()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 400
    :goto_27
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LMM;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LMM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    return-void

    .line 397
    :cond_31
    sget v0, Len;->loading:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_27
.end method

.method private m()V
    .registers 3

    .prologue
    .line 404
    sget v0, Lej;->punch_web_view_whole:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->setContentView(I)V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LkY;->a(Landroid/content/Intent;)LkY;

    move-result-object v0

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 409
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(LkY;Landroid/net/Uri;)V

    .line 411
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->j()V

    .line 413
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()V

    .line 414
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->s()V

    .line 415
    return-void
.end method

.method private n()V
    .registers 3

    .prologue
    .line 446
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    .line 448
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-eqz v1, :cond_11

    .line 449
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Lz;->a(Landroid/support/v4/app/Fragment;)Lz;

    .line 452
    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    if-eqz v1, :cond_1a

    .line 453
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    invoke-virtual {v0, v1}, Lz;->a(Landroid/support/v4/app/Fragment;)Lz;

    .line 456
    :cond_1a
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    if-eqz v1, :cond_23

    .line 457
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    invoke-virtual {v0, v1}, Lz;->a(Landroid/support/v4/app/Fragment;)Lz;

    .line 460
    :cond_23
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    if-eqz v1, :cond_2c

    .line 461
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    invoke-virtual {v0, v1}, Lz;->a(Landroid/support/v4/app/Fragment;)Lz;

    .line 464
    :cond_2c
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    if-eqz v1, :cond_35

    .line 465
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    invoke-virtual {v0, v1}, Lz;->a(Landroid/support/v4/app/Fragment;)Lz;

    .line 472
    :cond_35
    invoke-virtual {v0}, Lz;->b()I

    .line 473
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Z

    .line 474
    return-void
.end method

.method private o()V
    .registers 3

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQS;

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 558
    new-instance v0, LRH;

    invoke-direct {v0, p0}, LRH;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQS;

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQV;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQS;

    invoke-virtual {v0, v1}, LQV;->a(LQS;)V

    .line 579
    return-void

    .line 557
    :cond_17
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private p()V
    .registers 3

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQS;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQV;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQS;

    invoke-virtual {v0, v1}, LQV;->b(LQS;)V

    .line 584
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQS;

    .line 585
    return-void

    .line 582
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private q()V
    .registers 3

    .prologue
    .line 588
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in registerUiListener"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRD;

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0}, Lagu;->b(Z)V

    .line 590
    new-instance v0, LRI;

    invoke-direct {v0, p0}, LRI;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRD;

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRD;

    invoke-virtual {v0, v1}, LRF;->a(LRD;)V

    .line 603
    return-void

    .line 589
    :cond_1e
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private r()V
    .registers 3

    .prologue
    .line 606
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in unregisterUiListener"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRD;

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0}, Lagu;->b(Z)V

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRD;

    invoke-virtual {v0, v1}, LRF;->b(LRD;)V

    .line 609
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRD;

    .line 610
    return-void

    .line 607
    :cond_1a
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private s()V
    .registers 3

    .prologue
    .line 628
    sget v0, Leh;->slide_picker_open_spacer:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 629
    if-eqz v0, :cond_14

    .line 630
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    invoke-virtual {v1}, LRF;->c()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 631
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 636
    :cond_14
    :goto_14
    return-void

    .line 633
    :cond_15
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_14
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 356
    const-class v0, LQV;

    if-ne p1, v0, :cond_7

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQV;

    .line 373
    :goto_6
    return-object v0

    .line 360
    :cond_7
    const-class v0, LRC;

    if-ne p1, v0, :cond_e

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    goto :goto_6

    .line 364
    :cond_e
    const-class v0, LSg;

    if-ne p1, v0, :cond_15

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    goto :goto_6

    .line 368
    :cond_15
    const-class v0, LRb;

    if-ne p1, v0, :cond_1c

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    goto :goto_6

    .line 373
    :cond_1c
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/app/BaseActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_6
.end method

.method public a()V
    .registers 3

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->h()Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    .line 624
    :goto_9
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    invoke-virtual {v1, v0}, LRF;->c(Z)V

    .line 625
    return-void

    .line 623
    :cond_f
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public a(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->e()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    if-nez v0, :cond_10

    .line 189
    :cond_a
    if-eqz p2, :cond_f

    .line 190
    invoke-interface {p2}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 204
    :cond_f
    :goto_f
    return-void

    .line 196
    :cond_10
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_19

    .line 198
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()V

    .line 201
    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 202
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    .line 203
    iput-object p2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    goto :goto_f
.end method

.method public b_(I)V
    .registers 4
    .parameter

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQV;

    invoke-virtual {v0, p1}, LQV;->a(I)V

    .line 616
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQN;

    invoke-virtual {v0}, LQN;->a()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    if-eqz v0, :cond_17

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Z)V

    .line 619
    :cond_17
    return-void
.end method

.method public f()V
    .registers 1

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->g()V

    .line 209
    return-void
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 349
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->g()Z

    move-result v0

    if-nez v0, :cond_12

    .line 350
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onBackPressed()V

    .line 352
    :cond_12
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 315
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->n()V

    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->m()V

    .line 321
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 322
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    const-string v0, "PunchWebViewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onCreate savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    if-eqz p1, :cond_20

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->i()V

    .line 116
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LKS;

    invoke-static {v0}, LSh;->a(LKS;)LSh;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    invoke-virtual {v0}, LSh;->a()LRy;

    move-result-object v2

    invoke-virtual {v1, v2}, LRF;->a(LRy;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LRF;

    invoke-virtual {v0}, LSh;->a()Z

    move-result v0

    invoke-virtual {v1, v0}, LRF;->b(Z)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "docListTitle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    .line 126
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->m()V

    .line 127
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lek;->menu_punch_webview:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 162
    sget v0, Leh;->menu_full_screen:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 163
    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    new-instance v1, LRG;

    invoke-direct {v1, p0}, LRG;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 178
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 291
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 294
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->g()V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_1f

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQU;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1}, LQU;->a(Landroid/support/v4/app/Fragment;)V

    .line 300
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    if-eqz v0, :cond_28

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()V

    .line 304
    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    if-eqz v0, :cond_31

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()V

    .line 308
    :cond_31
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 309
    return-void
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 284
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->g()V

    .line 285
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onPause()V

    .line 286
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 379
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 381
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->k()V

    .line 382
    return-void
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 540
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onStart()V

    .line 541
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onStart"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->o()V

    .line 544
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->q()V

    .line 545
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 550
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->p()V

    .line 551
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->r()V

    .line 553
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onStop()V

    .line 554
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .parameter

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->setContentView(I)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Landroid/view/View;)V

    .line 133
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->setContentView(Landroid/view/View;)V

    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Landroid/view/View;)V

    .line 139
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 143
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/app/BaseActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Landroid/view/View;)V

    .line 145
    return-void
.end method
