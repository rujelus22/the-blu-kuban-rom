.class public abstract Lcom/google/android/apps/docs/editors/popup/SelectionPopup;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "SelectionPopup.java"


# instance fields
.field private final a:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field private a:Landroid/widget/PopupWindow;

.field protected a:Z

.field private b:Z

.field private c:Z

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 39
    new-instance v0, LDg;

    invoke-direct {v0, p0}, LDg;-><init>(Lcom/google/android/apps/docs/editors/popup/SelectionPopup;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/popup/SelectionPopup;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->b:Z

    return p1
.end method


# virtual methods
.method protected abstract a(II)Landroid/graphics/Point;
.end method

.method protected abstract a(Landroid/view/LayoutInflater;)Landroid/view/View;
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x2

    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 80
    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-direct {v1, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 85
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LDh;

    invoke-direct {v2, p0}, LDh;-><init>(Lcom/google/android/apps/docs/editors/popup/SelectionPopup;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 94
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->b(Landroid/view/View;)V

    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public a()V
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x2

    const/4 v2, 0x0

    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->b()Landroid/view/View;

    move-result-object v3

    .line 232
    if-eqz v3, :cond_3c

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_3c

    move v0, v1

    :goto_e
    invoke-static {v0}, Lagu;->b(Z)V

    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewGroup;->measure(II)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a(II)Landroid/graphics/Point;

    move-result-object v0

    .line 238
    if-nez v0, :cond_3e

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 254
    :goto_3b
    return-void

    :cond_3c
    move v0, v2

    .line 232
    goto :goto_e

    .line 243
    :cond_3e
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 244
    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 245
    iget v5, v0, Landroid/graphics/Point;->x:I

    aget v6, v4, v2

    add-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Point;->x:I

    .line 246
    iget v5, v0, Landroid/graphics/Point;->y:I

    aget v1, v4, v1

    add-int/2addr v1, v5

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 248
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_64

    .line 249
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v0, v7, v7}, Landroid/widget/PopupWindow;->update(IIII)V

    goto :goto_3b

    .line 252
    :cond_64
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v3, v2, v4, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_3b
.end method

.method protected a(IZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 104
    if-eqz p2, :cond_12

    const/4 v0, 0x0

    .line 105
    :goto_b
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-ne v2, v0, :cond_15

    .line 110
    :goto_11
    return-void

    .line 104
    :cond_12
    const/16 v0, 0x8

    goto :goto_b

    .line 108
    :cond_15
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Z

    goto :goto_11
.end method

.method protected abstract b()Landroid/view/View;
.end method

.method protected b(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 66
    return-void
.end method

.method protected c(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 166
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->d:Landroid/view/View;

    .line 172
    sget v0, LsD;->back:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LDi;

    invoke-direct {v1, p0}, LDi;-><init>(Lcom/google/android/apps/docs/editors/popup/SelectionPopup;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()V

    .line 183
    return-void
.end method

.method protected i()Z
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->d:Landroid/view/View;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected s()V
    .registers 11

    .prologue
    const/4 v4, 0x0

    const/16 v9, 0x8

    const/4 v1, 0x0

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()Landroid/view/ViewGroup;

    move-result-object v6

    move v5, v1

    move v0, v1

    move-object v3, v4

    .line 119
    :goto_b
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v5, v2, :cond_48

    .line 120
    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 121
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_27

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    const-string v8, "selection_popup_separator"

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3c

    .line 122
    :cond_27
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_46

    .line 123
    const/4 v0, 0x1

    .line 124
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v5, v2, :cond_46

    move-object v2, v4

    .line 119
    :goto_37
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v2

    goto :goto_b

    .line 129
    :cond_3c
    if-eqz v0, :cond_43

    .line 131
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    move v0, v1

    .line 132
    goto :goto_37

    .line 134
    :cond_43
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_46
    move-object v2, v3

    goto :goto_37

    .line 139
    :cond_48
    if-eqz v3, :cond_4d

    .line 140
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 142
    :cond_4d
    return-void
.end method

.method public t()V
    .registers 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->d:Landroid/view/View;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->d:Landroid/view/View;

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()V

    .line 153
    return-void
.end method

.method public u()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->b()Landroid/view/View;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_29

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_29

    move v0, v1

    :goto_d
    invoke-static {v0}, Lagu;->b(Z)V

    .line 201
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->c:Z

    if-nez v0, :cond_1f

    .line 202
    invoke-virtual {v3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v4}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 203
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->c:Z

    .line 206
    :cond_1f
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->b:Z

    if-nez v0, :cond_2b

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 211
    :goto_28
    return-void

    :cond_29
    move v0, v2

    .line 199
    goto :goto_d

    .line 209
    :cond_2b
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()V

    goto :goto_28
.end method

.method public v()V
    .registers 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1d

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->c:Z

    if-eqz v0, :cond_1d

    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->c:Z

    .line 225
    :cond_1d
    return-void
.end method
