.class public Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;
.super Ljava/lang/Object;
.source "mobilenativeJNI.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native ApplicationEventListener_getEvent(JLIP;)J
.end method

.method public static final native ApplicationEvent_getSheetId(JLIN;)I
.end method

.method public static final native ApplicationEvent_getType(JLIN;)I
.end method

.method public static final native ApplicationPtr_get(JLIQ;)J
.end method

.method public static final native Application_CreateApplication(Ljava/lang/String;Ljava/lang/String;Z)J
.end method

.method public static final native Application_CreateIdleQueue(JLIM;JLIR;)V
.end method

.method public static final native Application_Pause(JLIM;)V
.end method

.method public static final native Application_Resume(JLIM;)V
.end method

.method public static final native Application_SetConnectedState(JLIM;Z)V
.end method

.method public static final native Application_activateSheet(JLIM;I)J
.end method

.method public static final native Application_getSheetIds(JLIM;)J
.end method

.method public static final native Application_getSheetNameById(JLIM;I)Ljava/lang/String;
.end method

.method public static final native Application_getUserSessions(JLIM;)J
.end method

.method public static final native CellInfo_getFormat(JLIS;)J
.end method

.method public static final native CellInfo_getText(JLIS;)Ljava/lang/String;
.end method

.method public static final native CellInfo_gridCoord_get(JLIS;)J
.end method

.method public static final native CellInfo_size_get(JLIS;)J
.end method

.method public static final native Color4b_a_get(JLIT;)S
.end method

.method public static final native Color4b_b_get(JLIT;)S
.end method

.method public static final native Color4b_g_get(JLIT;)S
.end method

.method public static final native Color4b_r_get(JLIT;)S
.end method

.method public static final native FontDescriptor_bold_get(JLIU;)I
.end method

.method public static final native FontDescriptor_family_get(JLIU;)Ljava/lang/String;
.end method

.method public static final native FontDescriptor_italic_get(JLIU;)I
.end method

.method public static final native FontDescriptor_size_get(JLIU;)F
.end method

.method public static final native GridCellBorderState_has_border_bottom(JLIV;)Z
.end method

.method public static final native GridCellBorderState_has_border_right(JLIV;)Z
.end method

.method public static final native GridCellFormat_backgroundColor_get(JLIW;)J
.end method

.method public static final native GridCellFormat_borderState_get(JLIW;)J
.end method

.method public static final native GridCellFormat_font_get(JLIW;)J
.end method

.method public static final native GridCellFormat_horizontalAlignment_get(JLIW;)I
.end method

.method public static final native GridCellFormat_textColor_get(JLIW;)J
.end method

.method public static final native GridCellFormat_textDecoration_get(JLIW;)S
.end method

.method public static final native GridCellFormat_verticalAlignment_get(JLIW;)I
.end method

.method public static final native GridSelection_rect(JLIX;)J
.end method

.method public static final native GridViewModelChangedEventListener_getEvent(JLJa;)J
.end method

.method public static final native GridViewModelChangedEvent_range(JLIY;)J
.end method

.method public static final native GridViewModelChangedEvent_type(JLIY;)I
.end method

.method public static final native GridViewModelInterface_CanvasCoordToNearestGridCoord(JLJb;JLJI;)J
.end method

.method public static final native GridViewModelInterface_ColumnRangeToCanvasRange(JLJb;JLJo;)J
.end method

.method public static final native GridViewModelInterface_GetGridSize(JLJb;)J
.end method

.method public static final native GridViewModelInterface_GetSelection(JLJb;)J
.end method

.method public static final native GridViewModelInterface_GridRectToViewRect(JLJb;JLJq;)J
.end method

.method public static final native GridViewModelInterface_RowRangeToCanvasRange(JLJb;JLJo;)J
.end method

.method public static final native GridViewModelInterface_SWIGUpcast(J)J
.end method

.method public static final native GridViewModelInterface_ViewCoordToGridCoord(JLJb;JLJJ;JLJJ;)Z
.end method

.method public static final native IdleHandlerPtr_get(JLJe;)J
.end method

.method public static final native IdleHandler_Invoke(JLJd;)Z
.end method

.method public static final native IdleHandler_SWIGUpcast(J)J
.end method

.method public static final native LayoutInfo_getFont(JLJf;)J
.end method

.method public static final native LayoutInfo_getHorizontalAlignment(JLJf;)I
.end method

.method public static final native LayoutInfo_getText(JLJf;)Ljava/lang/String;
.end method

.method public static final native ListSelection_range(JLJg;)J
.end method

.method public static final native ListViewModelChangedEventListener_getEvent(JLJj;)J
.end method

.method public static final native ListViewModelChangedEvent_type(JLJh;)I
.end method

.method public static final native ListViewModelInterface_CanvasRectToListRange(JLJk;JLJp;)J
.end method

.method public static final native ListViewModelInterface_GetListSize(JLJk;)I
.end method

.method public static final native ListViewModelInterface_GetSelection(JLJk;)J
.end method

.method public static final native ListViewModelInterface_ListRangeToCanvasRange(JLJk;JLJo;)J
.end method

.method public static final native ListViewModelInterface_SWIGUpcast(J)J
.end method

.method public static final native NullableGridSelection_get__SWIG_0(JLJl;)J
.end method

.method public static final native NullableGridSelection_hasValue(JLJl;)Z
.end method

.method public static final native NullableListSelection_get__SWIG_0(JLJm;)J
.end method

.method public static final native NullableListSelection_hasValue(JLJm;)Z
.end method

.method public static final native RangeD_min_pos(JLJn;)D
.end method

.method public static final native RangeD_size(JLJn;)D
.end method

.method public static final native RangeI_max_pos(JLJo;)I
.end method

.method public static final native RangeI_min_pos(JLJo;)I
.end method

.method public static final native RectI_max_x(JLJq;)I
.end method

.method public static final native RectI_max_y(JLJq;)I
.end method

.method public static final native RectI_min_x(JLJq;)I
.end method

.method public static final native RectI_min_y(JLJq;)I
.end method

.method public static final native ScrollViewModelChangedEventListener_getEvent(JLJs;)J
.end method

.method public static final native ScrollViewModelInterface_GetCanvasSize(JLJt;)J
.end method

.method public static final native ScrollViewModelInterface_GetScrollOffset(JLJt;)J
.end method

.method public static final native ScrollViewModelInterface_GetZoomScale(JLJt;)D
.end method

.method public static final native ScrollViewModelInterface_SWIGUpcast(J)J
.end method

.method public static final native ScrollViewModelInterface_SetScrollOffset(JLJt;JLJI;)V
.end method

.method public static final native ScrollViewModelInterface_SetZoomScale(JLJt;D)V
.end method

.method public static final native SpreadsheetEventListener_getEvent(JLJz;)J
.end method

.method public static final native SpreadsheetEvent_getType(JLJx;)I
.end method

.method public static final native SpreadsheetPtr_duplicateReference(JLJA;)J
.end method

.method public static final native SpreadsheetPtr_get(JLJA;)J
.end method

.method public static final native SpreadsheetUtil_EnumerateGridCellsForRect(JLJb;JLJq;JLJK;)V
.end method

.method public static final native SpreadsheetUtil_EnumerateListItemsForRange(JLJk;JLJo;JLJK;)V
.end method

.method public static final native SpreadsheetUtil_GetCellSlotId__SWIG_0(JLJb;JLJJ;)I
.end method

.method public static final native SpreadsheetUtil_GetCellSlotId__SWIG_1(JLJk;I)I
.end method

.method public static final native Spreadsheet_GetCellEditableValue(JLJv;JLJJ;)Ljava/lang/String;
.end method

.method public static final native Spreadsheet_GetCellFormat(JLJv;JLJJ;)J
.end method

.method public static final native Spreadsheet_GetFrozenColumnCount(JLJv;)I
.end method

.method public static final native Spreadsheet_GetFrozenRowCount(JLJv;)I
.end method

.method public static final native Spreadsheet_InitializeGridViewModel(JLJv;IJLIR;)J
.end method

.method public static final native Spreadsheet_InitializeListViewModel(JLJv;IJLIR;)J
.end method

.method public static final native Spreadsheet_IsDataAvailable(JLJv;)Z
.end method

.method public static final native Spreadsheet_IsDataLoadComplete(JLJv;)Z
.end method

.method public static final native Spreadsheet_SetSelection(JLJv;JLJl;)V
.end method

.method public static final native Spreadsheet_kFrozenHeaderColumn_get()I
.end method

.method public static final native Spreadsheet_kFrozenHeaderRow_get()I
.end method

.method public static final native Spreadsheet_kFrozenRowColumnGrid_get()I
.end method

.method public static final native Spreadsheet_kFrozenRowScrollableColumnGrid_get()I
.end method

.method public static final native Spreadsheet_kScrollableHeaderColumn_get()I
.end method

.method public static final native Spreadsheet_kScrollableHeaderRow_get()I
.end method

.method public static final native Spreadsheet_kScrollableRowColumnGrid_get()I
.end method

.method public static final native Spreadsheet_kScrollableRowFrozenColumnGrid_get()I
.end method

.method public static final native UserSessionEventListener_getEvent(JLJG;)J
.end method

.method public static final native UserSessionEvent_getType(JLJE;)I
.end method

.method public static final native UserSessionEvent_getUserSession(JLJE;)J
.end method

.method public static final native UserSessionVector_at(JLJH;I)J
.end method

.method public static final native UserSessionVector_size(JLJH;)I
.end method

.method public static final native UserSession_getActiveGridId(JLJD;)I
.end method

.method public static final native UserSession_getBorderColor(JLJD;)Ljava/lang/String;
.end method

.method public static final native UserSession_getCursorLocation(JLJD;)J
.end method

.method public static final native UserSession_getSessionNum(JLJD;)Ljava/lang/String;
.end method

.method public static final native UserSession_getUsername(JLJD;)Ljava/lang/String;
.end method

.method public static final native Vector2D_x_get(JLJI;)D
.end method

.method public static final native Vector2D_y_get(JLJI;)D
.end method

.method public static final native Vector2I_x_get(JLJJ;)I
.end method

.method public static final native Vector2I_y_get(JLJJ;)I
.end method

.method public static final native VectorI_at(JLJK;I)I
.end method

.method public static final native VectorI_size(JLJK;)I
.end method

.method public static final native ViewModelInterface_SetViewport(JLJM;JLJq;)V
.end method

.method public static final native delete_Application(J)V
.end method

.method public static final native delete_ApplicationEvent(J)V
.end method

.method public static final native delete_ApplicationEventListener(J)V
.end method

.method public static final native delete_ApplicationPtr(J)V
.end method

.method public static final native delete_BaseObjectReference(J)V
.end method

.method public static final native delete_CellInfo(J)V
.end method

.method public static final native delete_Color4b(J)V
.end method

.method public static final native delete_FontDescriptor(J)V
.end method

.method public static final native delete_GridCellBorderState(J)V
.end method

.method public static final native delete_GridCellFormat(J)V
.end method

.method public static final native delete_GridSelection(J)V
.end method

.method public static final native delete_GridViewModelChangedEvent(J)V
.end method

.method public static final native delete_GridViewModelChangedEventListener(J)V
.end method

.method public static final native delete_GridViewModelInterface(J)V
.end method

.method public static final native delete_IdleHandler(J)V
.end method

.method public static final native delete_IdleHandlerPtr(J)V
.end method

.method public static final native delete_LayoutInfo(J)V
.end method

.method public static final native delete_ListSelection(J)V
.end method

.method public static final native delete_ListViewModelChangedEvent(J)V
.end method

.method public static final native delete_ListViewModelChangedEventListener(J)V
.end method

.method public static final native delete_ListViewModelInterface(J)V
.end method

.method public static final native delete_NullableGridSelection(J)V
.end method

.method public static final native delete_NullableListSelection(J)V
.end method

.method public static final native delete_RangeD(J)V
.end method

.method public static final native delete_RangeI(J)V
.end method

.method public static final native delete_RectD(J)V
.end method

.method public static final native delete_RectI(J)V
.end method

.method public static final native delete_ScrollViewModelChangedEvent(J)V
.end method

.method public static final native delete_ScrollViewModelChangedEventListener(J)V
.end method

.method public static final native delete_ScrollViewModelInterface(J)V
.end method

.method public static final native delete_SharedObject(J)V
.end method

.method public static final native delete_Spreadsheet(J)V
.end method

.method public static final native delete_SpreadsheetEvent(J)V
.end method

.method public static final native delete_SpreadsheetEventListener(J)V
.end method

.method public static final native delete_SpreadsheetPtr(J)V
.end method

.method public static final native delete_SpreadsheetUtil(J)V
.end method

.method public static final native delete_UserSession(J)V
.end method

.method public static final native delete_UserSessionEvent(J)V
.end method

.method public static final native delete_UserSessionEventListener(J)V
.end method

.method public static final native delete_UserSessionVector(J)V
.end method

.method public static final native delete_Vector2D(J)V
.end method

.method public static final native delete_Vector2I(J)V
.end method

.method public static final native delete_VectorI(J)V
.end method

.method public static final native delete_ViewModelInterface(J)V
.end method

.method public static final native kTriStateFalse_get()I
.end method

.method public static final native kTriStateTrue_get()I
.end method

.method public static final native kTriStateUndefined_get()I
.end method

.method public static final native new_ApplicationEventListener(JLIM;JLIR;)J
.end method

.method public static final native new_ApplicationPtr()J
.end method

.method public static final native new_FontDescriptor__SWIG_0()J
.end method

.method public static final native new_GridCellBorderState__SWIG_0()J
.end method

.method public static final native new_GridCellFormat__SWIG_0()J
.end method

.method public static final native new_GridSelection__SWIG_0()J
.end method

.method public static final native new_GridSelection__SWIG_2(JLJq;Z)J
.end method

.method public static final native new_GridViewModelChangedEventListener(JLJb;JLIR;)J
.end method

.method public static final native new_IdleHandlerPtr()J
.end method

.method public static final native new_ListSelection__SWIG_0()J
.end method

.method public static final native new_ListViewModelChangedEventListener(JLJk;JLIR;)J
.end method

.method public static final native new_NullableGridSelection__SWIG_0()J
.end method

.method public static final native new_NullableGridSelection__SWIG_1(JLIX;)J
.end method

.method public static final native new_NullableListSelection__SWIG_0()J
.end method

.method public static final native new_RangeD__SWIG_0()J
.end method

.method public static final native new_RangeI__SWIG_0()J
.end method

.method public static final native new_RangeI__SWIG_1(II)J
.end method

.method public static final native new_RectD__SWIG_0()J
.end method

.method public static final native new_RectD__SWIG_3(DDDD)J
.end method

.method public static final native new_RectI__SWIG_0()J
.end method

.method public static final native new_RectI__SWIG_3(IIII)J
.end method

.method public static final native new_ScrollViewModelChangedEventListener(JLJt;JLIR;)J
.end method

.method public static final native new_SharedObject()J
.end method

.method public static final native new_SpreadsheetEventListener(JLJv;JLIR;)J
.end method

.method public static final native new_SpreadsheetPtr()J
.end method

.method public static final native new_SpreadsheetUtil()J
.end method

.method public static final native new_UserSessionEventListener(JLIM;JLIR;)J
.end method

.method public static final native new_UserSessionVector()J
.end method

.method public static final native new_Vector2D(DD)J
.end method

.method public static final native new_Vector2I(II)J
.end method

.method public static final native new_VectorI()J
.end method
