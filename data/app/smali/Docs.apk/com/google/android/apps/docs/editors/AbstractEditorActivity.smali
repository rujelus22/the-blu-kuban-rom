.class public abstract Lcom/google/android/apps/docs/editors/AbstractEditorActivity;
.super Lcom/google/android/apps/docs/RoboFragmentActivity;
.source "AbstractEditorActivity.java"


# instance fields
.field private a:Landroid/content/Intent;

.field private a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field protected final i:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;-><init>()V

    .line 35
    invoke-static {}, LZL;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->i:Z

    return-void
.end method

.method private d()V
    .registers 4

    .prologue
    .line 92
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 96
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    const-string v2, "editor_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    const-string v1, "editor_title"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 99
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    const-class v3, LUq;

    if-ne p1, v3, :cond_23

    .line 56
    if-nez p2, :cond_1f

    :goto_9
    invoke-static {v0}, Lagu;->a(Z)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a()Lo;

    move-result-object v0

    const-string v1, "SharingFragment"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_21

    .line 60
    check-cast v0, Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()LUq;

    move-result-object v0

    .line 78
    :cond_1e
    :goto_1e
    return-object v0

    :cond_1f
    move v0, v1

    .line 56
    goto :goto_9

    :cond_21
    move-object v0, v2

    .line 63
    goto :goto_1e

    .line 65
    :cond_23
    const-class v3, LTr;

    if-ne p1, v3, :cond_3c

    .line 68
    if-nez p2, :cond_3a

    :goto_29
    invoke-static {v0}, Lagu;->a(Z)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a()Lo;

    move-result-object v0

    const-string v1, "SharingFragment"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 70
    if-nez v0, :cond_1e

    move-object v0, v2

    .line 75
    goto :goto_1e

    :cond_3a
    move v0, v1

    .line 68
    goto :goto_29

    .line 78
    :cond_3c
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/RoboFragmentActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1e
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 6
    .parameter

    .prologue
    .line 102
    invoke-static {p1}, Laaz;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 103
    const-string v1, "CAKEMIX_EDITOR_CRASH"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uncaughtException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    const-string v2, "stack_trace"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->finish()V

    .line 107
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 108
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 109
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->d()V

    .line 86
    return-void
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    if-eqz v0, :cond_9

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 130
    :cond_9
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onPause()V

    .line 131
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onResume()V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Landroid/content/Intent;

    if-eqz v0, :cond_15

    .line 115
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 116
    new-instance v0, Lso;

    invoke-direct {v0, p0}, Lso;-><init>(Lcom/google/android/apps/docs/editors/AbstractEditorActivity;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 123
    :cond_15
    return-void
.end method

.method public setContentView(I)V
    .registers 4
    .parameter

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->setContentView(I)V

    .line 40
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->i:Z

    if-eqz v0, :cond_11

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 43
    if-eqz v0, :cond_12

    .line 44
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 49
    :cond_11
    :goto_11
    return-void

    .line 46
    :cond_12
    const-string v0, "AbstractEditorActivity"

    const-string v1, "The action bar is not available."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11
.end method
