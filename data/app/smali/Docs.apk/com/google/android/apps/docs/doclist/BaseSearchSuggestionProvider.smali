.class public Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;
.super Lcom/google/android/apps/docs/GuiceContentProvider;
.source "BaseSearchSuggestionProvider.java"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:I

.field public a:LPO;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Landroid/content/UriMatcher;

.field private a:Lnh;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "suggest_intent_action"

    aput-object v1, v0, v4

    const-string v1, "suggest_icon_1"

    aput-object v1, v0, v5

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v6

    const-string v1, "suggest_text_2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "suggest_intent_data_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:[Ljava/lang/String;

    .line 51
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LPX;->a:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, LPX;->p:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sget-object v1, LPX;->o:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, LPX;->n:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, LPt;->a:LPt;

    invoke-virtual {v1}, LPt;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LPX;->j:LPX;

    invoke-virtual {v2}, LPX;->a()LPI;

    move-result-object v2

    invoke-virtual {v2}, LPI;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/GuiceContentProvider;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:I

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:Lnh;

    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a(Ljava/lang/String;)Landroid/content/UriMatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:Landroid/content/UriMatcher;

    .line 70
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/content/UriMatcher;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 76
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 78
    const-string v1, "search_suggest_query"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    const-string v1, "search_suggest_query/*"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 80
    return-object v0
.end method

.method private a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 20
    .parameter

    .prologue
    .line 160
    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:[Ljava/lang/String;

    invoke-direct {v3, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 161
    sget-object v1, LPX;->a:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 163
    sget-object v1, LPX;->p:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 165
    sget-object v1, LPX;->o:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 167
    sget-object v1, LPX;->n:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 169
    sget-object v1, LPt;->a:LPt;

    invoke-virtual {v1}, LPt;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 172
    sget-object v1, LPX;->j:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 174
    const/4 v1, 0x0

    .line 175
    :goto_68
    const/16 v2, 0xa

    if-ge v1, v2, :cond_d1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_d1

    .line 176
    add-int/lit8 v2, v1, 0x1

    .line 177
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 178
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 179
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 180
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 181
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 182
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_cf

    const/4 v1, 0x1

    .line 184
    :goto_9b
    const/16 v15, 0x8

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "android.intent.action.VIEW"

    aput-object v17, v15, v16

    const/16 v16, 0x2

    invoke-static {v10, v11, v1}, LkO;->b(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v16

    const/4 v1, 0x3

    aput-object v12, v15, v1

    const/4 v1, 0x4

    aput-object v13, v15, v1

    const/4 v1, 0x5

    aput-object v14, v15, v1

    const/4 v1, 0x6

    aput-object v13, v15, v1

    const/4 v1, 0x7

    const-string v10, "_-1"

    aput-object v10, v15, v1

    invoke-virtual {v3, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v1, v2

    .line 202
    goto :goto_68

    .line 182
    :cond_cf
    const/4 v1, 0x0

    goto :goto_9b

    .line 203
    :cond_d1
    return-object v3
.end method

.method private static a(Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 2
    .parameter

    .prologue
    .line 245
    const/4 v0, 0x0

    .line 246
    if-eqz p0, :cond_9

    .line 247
    const-string v0, "app_data"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 249
    :cond_9
    if-nez v0, :cond_10

    .line 250
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 252
    :cond_10
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Landroid/os/Bundle;
    .registers 3
    .parameter

    .prologue
    .line 222
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 223
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 231
    if-nez p0, :cond_4

    .line 232
    const/4 v0, 0x0

    .line 241
    :cond_3
    :goto_3
    return-object v0

    .line 237
    :cond_4
    const-string v0, "intent_extra_data_key"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    if-nez v0, :cond_3

    .line 239
    invoke-static {p0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method a(Lnh;)V
    .registers 2
    .parameter

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:Lnh;

    .line 157
    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 216
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_2e

    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :pswitch_2b
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0

    .line 96
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_2b
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 107
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .registers 2

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceContentProvider;->onCreate()Z

    move-result v0

    if-nez v0, :cond_8

    .line 86
    const/4 v0, 0x0

    .line 90
    :goto_7
    return v0

    .line 89
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:LPO;

    invoke-virtual {v0}, LPO;->g()V

    .line 90
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_e4

    .line 147
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :pswitch_2d
    aget-object v0, p4, v2

    .line 117
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c6

    .line 120
    const-wide/16 v1, -0x1

    invoke-static {v1, v2, v0}, Lnl;->a(JLjava/lang/String;)Lnh;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:Lnh;

    if-eqz v1, :cond_e1

    .line 123
    sget-object v1, Lnk;->a:Lnk;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:Lnh;

    invoke-virtual {v0, v1, v2}, Lnh;->a(Lnk;Lnh;)Lnh;

    move-result-object v0

    move-object v4, v0

    .line 126
    :goto_48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EntryView inner join "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, LPs;->a()LPs;

    move-result-object v1

    invoke-virtual {v1}, LPs;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, LPs;->a()LPs;

    move-result-object v1

    invoke-virtual {v1}, LPs;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LPX;->x:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 133
    :try_start_8f
    sget-object v0, Lrl;->b:Lrl;

    invoke-virtual {v0}, Lrl;->a()LqT;

    move-result-object v7

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:LPO;

    sget-object v2, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->b:[Ljava/lang/String;

    invoke-virtual {v4}, Lnh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lnh;->a()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v7}, LqT;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LPO;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_b3
    .catchall {:try_start_8f .. :try_end_b3} :catchall_be

    move-result-object v1

    .line 138
    :try_start_b4
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a(Landroid/database/Cursor;)Landroid/database/Cursor;
    :try_end_b7
    .catchall {:try_start_b4 .. :try_end_b7} :catchall_df

    move-result-object v0

    .line 140
    if-eqz v1, :cond_bd

    .line 141
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_bd
    return-object v0

    .line 140
    :catchall_be
    move-exception v0

    move-object v1, v8

    :goto_c0
    if-eqz v1, :cond_c5

    .line 141
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_c5
    throw v0

    .line 145
    :cond_c6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :catchall_df
    move-exception v0

    goto :goto_c0

    :cond_e1
    move-object v4, v0

    goto/16 :goto_48

    .line 114
    :pswitch_data_e4
    .packed-switch 0x0
        :pswitch_2d
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 209
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
