.class public Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;
.super Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;
.source "DownloadFileDocumentOpenerImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;


# instance fields
.field private final a:LUL;

.field private final a:LVH;

.field private final a:LXP;

.field private final a:Landroid/content/Context;

.field private final a:LeQ;

.field private final a:Llf;

.field private final a:LoZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LXP;Llf;LeQ;LVH;LoZ;LUL;)V
    .registers 8
    .parameter
    .end parameter
    .parameter
    .end parameter
    .parameter
    .end parameter
    .parameter
    .end parameter
    .parameter
    .end parameter
    .parameter
        .annotation runtime LaqW;
            value = "DefaultLocal"
        .end annotation
    .end parameter
    .parameter
    .end parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LXP;

    .line 62
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Llf;

    .line 63
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LeQ;

    .line 64
    iput-object p5, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LVH;

    .line 65
    iput-object p6, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LoZ;

    .line 66
    iput-object p7, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LUL;

    .line 67
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;Lpa;LkY;Landroid/os/Bundle;)I
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(Lpa;LkY;Landroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method private a(Lpa;LkY;Landroid/os/Bundle;)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 197
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(LkY;)LkM;

    move-result-object v2

    .line 198
    if-nez v2, :cond_a

    move v0, v1

    .line 234
    :cond_9
    :goto_9
    return v0

    .line 202
    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LoZ;

    invoke-static {v3, p1, v2, p3}, Lpe;->a(LoZ;Lpa;LkM;Landroid/os/Bundle;)Lnm;

    move-result-object v3

    .line 207
    if-eqz v3, :cond_9

    .line 211
    invoke-interface {v3}, Lnm;->a()Z

    move-result v4

    if-eqz v4, :cond_26

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 214
    new-instance v4, Lpf;

    invoke-direct {v4, p0, v3, v2, v0}, Lpf;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;Lnm;LkM;Landroid/support/v4/app/FragmentActivity;)V

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_24
    move v0, v1

    .line 234
    goto :goto_9

    .line 227
    :cond_26
    :goto_26
    if-ltz v0, :cond_24

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_24

    .line 228
    invoke-interface {v3, v0}, Lnm;->a(I)I

    move-result v0

    goto :goto_26
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LUL;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LUL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LXP;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LXP;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LeQ;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LeQ;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;LkY;)LkM;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(LkY;)LkM;

    move-result-object v0

    return-object v0
.end method

.method private a(LkY;)LkM;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Llf;

    iget-object v2, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 240
    if-nez v1, :cond_13

    .line 241
    const-string v1, "DownloadFileDocumentOpener"

    const-string v2, "OpenLocalFile failed: account no longer exists"

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :goto_12
    return-object v0

    .line 245
    :cond_13
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v2, v1, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v1

    .line 246
    if-nez v1, :cond_25

    .line 247
    const-string v1, "DownloadFileDocumentOpener"

    const-string v2, "OpenLocalFile failed: document no longer exists"

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12

    :cond_25
    move-object v0, v1

    .line 250
    goto :goto_12
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Len;->opening_document:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LkM;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 274
    invoke-virtual {p1}, LkM;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LkY;Lpa;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LVH;

    invoke-interface {v0, p1}, LVH;->a(LkY;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lpa;LkM;Landroid/os/Bundle;)Lnm;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    new-instance v0, Lpg;

    invoke-direct {v0, p0, p1, p2, p3}, Lpg;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;Lpa;LkM;Landroid/os/Bundle;)V

    return-object v0
.end method
