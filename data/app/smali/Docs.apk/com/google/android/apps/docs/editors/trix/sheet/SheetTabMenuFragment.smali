.class public Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "SheetTabMenuFragment.java"

# interfaces
.implements LHG;


# instance fields
.field private a:I

.field private a:LHz;

.field private a:Landroid/app/AlertDialog;

.field private a:Landroid/widget/EditText;

.field private a:Landroid/widget/PopupWindow;

.field private a:Landroid/widget/TextView;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)LHz;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:LHz;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)Landroid/widget/EditText;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->r()V

    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    sget v1, LsD;->sheet_tab_menu_delete:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 131
    new-instance v1, LHH;

    invoke-direct {v1, p0}, LHH;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    sget v1, LsD;->sheet_tab_menu_duplicate:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 141
    new-instance v1, LHI;

    invoke-direct {v1, p0}, LHI;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    sget v1, LsD;->sheet_tab_menu_rename:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 151
    new-instance v1, LHJ;

    invoke-direct {v1, p0}, LHJ;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    sget v1, LsD;->sheet_tab_menu_move_right:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 161
    new-instance v1, LHK;

    invoke-direct {v1, p0}, LHK;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    sget v1, LsD;->sheet_tab_menu_move_left:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 171
    new-instance v1, LHL;

    invoke-direct {v1, p0}, LHL;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    sget v1, LsD;->sheet_tab_menu_close:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 181
    new-instance v1, LHM;

    invoke-direct {v1, p0}, LHM;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    return-void
.end method

.method private q()V
    .registers 8

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 193
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 195
    sget v2, LsH;->trix_sheets_tab_menu_change_name:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 197
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/EditText;

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->setSingleLine()V

    .line 199
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    .line 200
    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, LsE;->trix_max_sheet_name_chars:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    .line 202
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 203
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 205
    sget v2, LsH;->trix_sheets_tab_menu_ok:I

    new-instance v3, LHN;

    invoke-direct {v3, p0, v0}, LHN;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 215
    sget v2, LsH;->trix_sheets_tab_menu_cancel:I

    new-instance v3, LHO;

    invoke-direct {v3, p0, v0}, LHO;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 224
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/app/AlertDialog;

    .line 225
    return-void
.end method

.method private r()V
    .registers 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/app/AlertDialog;

    if-nez v0, :cond_7

    .line 230
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->q()V

    .line 233
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 235
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 80
    return-void
.end method

.method public a(IILjava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 92
    iput-object p3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->c:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v1, v1}, Landroid/view/View;->measure(II)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 100
    iget v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->b:I

    iget v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_4d

    .line 103
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->b:I

    iget v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 104
    iget p1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    .line 115
    :cond_25
    :goto_25
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->d:Landroid/view/View;

    const/16 v2, 0x53

    iget v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    add-int/2addr v3, p2

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 125
    return-void

    .line 105
    :cond_4d
    iget v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    if-ge p1, v1, :cond_54

    .line 107
    iget p1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    goto :goto_25

    .line 109
    :cond_54
    add-int v1, p1, v0

    iget v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->b:I

    iget v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_25

    .line 111
    add-int v1, p1, v0

    iget v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->b:I

    sub-int/2addr v1, v2

    sub-int v1, p1, v1

    iget v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    sub-int p1, v1, v2

    goto :goto_25
.end method

.method public a(LHz;)V
    .registers 2
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:LHz;

    .line 70
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/view/View;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 48
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->d:Landroid/view/View;

    .line 49
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LsF;->trix_sheet_tab_menu:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    sget v1, LsD;->sheet_tab_menu_title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/TextView;

    .line 51
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->e:Landroid/view/View;

    invoke-direct {v0, v1, v3, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsB;->trix_sheets_tab_menu_window_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:I

    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->p()V

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->b:I

    .line 65
    return-void
.end method

.method public a_()Z
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public h()V
    .registers 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a()V

    .line 87
    return-void
.end method
