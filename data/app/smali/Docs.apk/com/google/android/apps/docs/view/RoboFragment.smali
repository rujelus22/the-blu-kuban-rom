.class public Lcom/google/android/apps/docs/view/RoboFragment;
.super Landroid/support/v4/app/Fragment;
.source "RoboFragment.java"

# interfaces
.implements LdZ;


# instance fields
.field private a:Laoo;

.field protected a:LdL;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Laoo;
    .registers 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:Laoo;

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 81
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 82
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .registers 4
    .parameter

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->a()Laoo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:Laoo;

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:Laoo;

    const-class v1, LdL;

    invoke-interface {v0, v1}, Laoo;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LdL;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-interface {v0, p1}, LdL;->a(Landroid/content/Context;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:Laoo;

    invoke-interface {v0, p0}, Laoo;->a(Ljava/lang/Object;)V

    .line 46
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 58
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .registers 4
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 93
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;)V

    .line 94
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 87
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 88
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->b(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 99
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 63
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 64
    return-void
.end method

.method public h_()V
    .registers 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 69
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->h_()V

    .line 70
    return-void
.end method

.method public i_()V
    .registers 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 75
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->i_()V

    .line 76
    return-void
.end method

.method protected k()Z
    .registers 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->d(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/view/RoboFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 105
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 106
    return-void
.end method
