.class public Lcom/google/android/apps/docs/editors/text/TextView;
.super Landroid/view/View;
.source "TextView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.implements Landroid/view/accessibility/AccessibilityEventSource;


# static fields
.field private static D:I

.field private static final a:Landroid/graphics/RectF;

.field private static final a:Landroid/text/BoringLayout$Metrics;

.field private static final a:Landroid/text/Spanned;

.field private static final a:[Landroid/text/InputFilter;

.field private static b:Landroid/graphics/Rect;

.field private static final b:[I

.field private static c:J


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private a:F

.field private a:I

.field private a:J

.field private a:LDX;

.field private a:LER;

.field private final a:LEX;

.field public a:LEj;

.field private a:LFA;

.field private a:LFB;

.field private a:LFH;

.field private a:LFZ;

.field private a:LFn;

.field private a:LFo;

.field private a:LFp;

.field private a:LFq;

.field private a:LFt;

.field a:LFv;

.field a:LFw;

.field private a:LGE;

.field private a:LGv;

.field private a:LGw;

.field private final a:Landroid/content/Context;

.field private a:Landroid/content/res/ColorStateList;

.field private final a:Landroid/graphics/Paint;

.field private a:Landroid/graphics/Path;

.field public a:Landroid/graphics/Rect;

.field private a:Landroid/graphics/drawable/Drawable;

.field private a:Landroid/text/Editable$Factory;

.field private a:Landroid/text/Spannable$Factory;

.field private final a:Landroid/text/TextPaint;

.field private a:Landroid/text/TextUtils$TruncateAt;

.field private a:Landroid/text/method/KeyListener;

.field private a:Landroid/view/ActionMode$Callback;

.field private a:Landroid/view/View$OnClickListener;

.field private a:Landroid/widget/Scroller;

.field private a:Ljava/lang/CharSequence;

.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/TextWatcher;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field public final a:[I

.field private final a:[Landroid/graphics/drawable/Drawable;

.field private b:F

.field private b:I

.field private b:J

.field private b:Landroid/content/res/ColorStateList;

.field private b:Landroid/graphics/drawable/Drawable;

.field private b:Landroid/text/BoringLayout$Metrics;

.field private b:Ljava/lang/CharSequence;

.field private b:Z

.field private b:[Landroid/text/InputFilter;

.field private c:F

.field private c:I

.field private c:Landroid/content/res/ColorStateList;

.field private c:Landroid/graphics/drawable/Drawable;

.field private c:Z

.field private d:F

.field private d:I

.field private d:Landroid/graphics/drawable/Drawable;

.field private d:Z

.field private e:F

.field private e:I

.field private e:Landroid/graphics/drawable/Drawable;

.field private e:Z

.field private final f:I

.field private f:Landroid/graphics/drawable/Drawable;

.field private f:Z

.field private final g:I

.field private g:Landroid/graphics/drawable/Drawable;

.field private g:Z

.field private final h:I

.field private h:Z

.field private final i:I

.field private i:Z

.field private final j:I

.field private j:Z

.field private final k:I

.field private k:Z

.field private l:I

.field private l:Z

.field private m:I

.field private m:Z

.field private n:I

.field private n:Z

.field private final o:I

.field private o:Z

.field private final p:I

.field private p:Z

.field private q:I

.field private q:Z

.field private r:I

.field private r:Z

.field private s:I

.field private s:Z

.field private t:I

.field private t:Z

.field private u:I

.field private u:Z

.field private v:I

.field private v:Z

.field private w:I

.field private w:Z

.field private x:I

.field private x:Z

.field private y:I

.field private y:Z

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 275
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 276
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 278
    const-string v1, "H"

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 5229
    new-instance v0, Landroid/text/BoringLayout$Metrics;

    invoke-direct {v0}, Landroid/text/BoringLayout$Metrics;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    .line 9009
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/graphics/Rect;

    .line 9046
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    .line 9062
    new-array v0, v2, [Landroid/text/InputFilter;

    sput-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/text/InputFilter;

    .line 9064
    new-instance v0, Landroid/text/SpannedString;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Spanned;

    .line 9065
    const/16 v0, 0x14

    sput v0, Lcom/google/android/apps/docs/editors/text/TextView;->D:I

    .line 9072
    new-array v0, v3, [I

    const v1, 0x101034d

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 303
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 306
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/editors/text/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 307
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x2

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 311
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 153
    new-instance v0, LEX;

    invoke-direct {v0}, LEX;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEX;

    .line 154
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/view/View$OnClickListener;

    .line 157
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    .line 159
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    .line 169
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Z

    .line 171
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Z

    .line 172
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Z

    .line 174
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Editable$Factory;

    .line 175
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Spannable$Factory;

    .line 182
    iput v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    .line 184
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    .line 207
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LDX;

    .line 209
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Z

    .line 210
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:Z

    .line 215
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:I

    .line 244
    sget v0, LEw;->text_select_handle_left:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:I

    .line 245
    sget v0, LEw;->text_select_handle_right:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:I

    .line 246
    sget v0, LEw;->text_select_handle_middle:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->h:I

    .line 247
    sget v0, LEy;->text_edit_paste_window:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->i:I

    .line 248
    sget v0, LEy;->text_edit_no_paste_window:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->j:I

    .line 251
    new-array v0, v1, [Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    .line 266
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->i:Z

    .line 8952
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Landroid/graphics/drawable/Drawable;

    .line 8953
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Landroid/graphics/drawable/Drawable;

    .line 8954
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Landroid/graphics/drawable/Drawable;

    .line 8955
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:Landroid/graphics/drawable/Drawable;

    .line 8961
    sget-object v0, LFo;->a:LFo;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFo;

    .line 8963
    iput v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    .line 8971
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    .line 8977
    const v0, -0x7fa83f18

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:I

    .line 8989
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->k:Z

    .line 9003
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->o:Z

    .line 9004
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->p:Z

    .line 9005
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:Z

    .line 9011
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:Z

    .line 9013
    const/16 v0, 0x33

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    .line 9017
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:Z

    .line 9019
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:F

    .line 9020
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:F

    .line 9021
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    .line 9023
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:Z

    .line 9029
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    .line 9030
    iput v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    .line 9031
    iput v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    .line 9032
    iput v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:I

    .line 9034
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:I

    .line 9035
    iput v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->z:I

    .line 9036
    iput v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->A:I

    .line 9037
    iput v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->B:I

    .line 9040
    iput v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->C:I

    .line 9041
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:Z

    .line 9045
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 9055
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    .line 9063
    sget-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/text/InputFilter;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:[Landroid/text/InputFilter;

    .line 312
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    .line 314
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    .line 316
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v2}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, v0, Landroid/text/TextPaint;->density:F

    .line 323
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    .line 328
    iput-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    .line 334
    const/16 v7, 0xf

    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()Z

    .line 398
    const/4 v0, 0x0

    .line 411
    const-string v8, ""

    .line 427
    const v1, 0x20001

    .line 428
    sget v6, LEw;->text_cursor:I

    iput v6, p0, Lcom/google/android/apps/docs/editors/text/TextView;->k:I

    .line 759
    sget-object v6, LFo;->c:LFo;

    .line 767
    if-eqz v4, :cond_190

    .line 771
    :try_start_e1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_e8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_e1 .. :try_end_e8} :catch_176

    move-result-object v0

    .line 777
    :try_start_e9
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/method/KeyListener;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;
    :try_end_f1
    .catch Ljava/lang/InstantiationException; {:try_start_e9 .. :try_end_f1} :catch_17d
    .catch Ljava/lang/IllegalAccessException; {:try_start_e9 .. :try_end_f1} :catch_184

    .line 784
    :try_start_f1
    iput v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I
    :try_end_f3
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_f1 .. :try_end_f3} :catch_18b

    :goto_f3
    move-object v0, v6

    move v1, v3

    .line 874
    :goto_f5
    invoke-virtual {p0, v4, v4, v4, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 876
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->setCompoundDrawablePadding(I)V

    .line 881
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)V

    .line 882
    invoke-direct {p0, v1, v1, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(ZZZ)V

    .line 884
    if-eqz v1, :cond_1dd

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-nez v1, :cond_1dd

    .line 885
    const/4 v1, 0x3

    .line 888
    :goto_108
    packed-switch v1, :pswitch_data_1e0

    .line 904
    :goto_10b
    if-eqz v4, :cond_1d2

    move-object v1, v4

    :goto_10e
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 905
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 906
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 910
    int-to-float v1, v7

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(F)V

    .line 915
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    and-int/lit16 v1, v1, 0xfff

    const/16 v4, 0x81

    if-ne v1, v4, :cond_1da

    .line 916
    const/4 v1, 0x3

    .line 919
    :goto_124
    invoke-direct {p0, v1, v5}, Lcom/google/android/apps/docs/editors/text/TextView;->b(II)V

    .line 928
    sget-object v1, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/text/InputFilter;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 931
    invoke-virtual {p0, v8, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;LFo;)V

    .line 941
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-nez v0, :cond_137

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_138

    :cond_137
    move v3, v2

    .line 968
    :cond_138
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->setFocusable(Z)V

    .line 969
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->setClickable(Z)V

    .line 970
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->setLongClickable(Z)V

    .line 972
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    .line 974
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 975
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    .line 976
    mul-int/2addr v0, v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->o:I

    .line 978
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LEv;->doubletap_slop:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->p:I

    .line 982
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_175

    .line 983
    new-instance v0, LFj;

    invoke-direct {v0, p0}, LFj;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 997
    new-instance v0, LFk;

    invoke-direct {v0, p0}, LFk;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1007
    :cond_175
    return-void

    .line 772
    :catch_176
    move-exception v0

    .line 773
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 778
    :catch_17d
    move-exception v0

    .line 779
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 780
    :catch_184
    move-exception v0

    .line 781
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 785
    :catch_18b
    move-exception v0

    .line 786
    iput v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    goto/16 :goto_f3

    .line 788
    :cond_190
    if-eqz v4, :cond_1a3

    .line 789
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LFV;->a(Ljava/lang/String;)LFV;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    move v0, v1

    .line 793
    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    move-object v0, v6

    move v1, v3

    goto/16 :goto_f5

    .line 795
    :cond_1a3
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IZ)V

    .line 798
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->c(I)Z

    move-result v0

    if-nez v0, :cond_1b1

    move v0, v2

    :goto_1ad
    move v1, v0

    move-object v0, v6

    goto/16 :goto_f5

    :cond_1b1
    move v0, v3

    goto :goto_1ad

    .line 890
    :pswitch_1b3
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto/16 :goto_10b

    .line 893
    :pswitch_1ba
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto/16 :goto_10b

    .line 896
    :pswitch_1c1
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto/16 :goto_10b

    .line 899
    :pswitch_1c8
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 900
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto/16 :goto_10b

    .line 904
    :cond_1d2
    const/high16 v1, -0x100

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_10e

    :cond_1da
    move v1, v5

    goto/16 :goto_124

    :cond_1dd
    move v1, v5

    goto/16 :goto_108

    .line 888
    :pswitch_data_1e0
    .packed-switch 0x1
        :pswitch_1b3
        :pswitch_1ba
        :pswitch_1c1
        :pswitch_1c8
    .end packed-switch
.end method

.method private A()V
    .registers 5

    .prologue
    const/4 v3, -0x2

    const/4 v1, 0x1

    .line 5397
    const/4 v0, 0x0

    .line 5399
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v2, :cond_26

    .line 5401
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v2, v3, :cond_13

    .line 5403
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    move v0, v1

    .line 5407
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v2, v3, :cond_2c

    .line 5408
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()I

    move-result v2

    .line 5410
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v3

    if-eq v2, v3, :cond_43

    :goto_25
    move v0, v1

    .line 5424
    :cond_26
    :goto_26
    if-eqz v0, :cond_2b

    .line 5425
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 5428
    :cond_2b
    return-void

    .line 5413
    :cond_2c
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_26

    .line 5414
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->C:I

    if-ltz v2, :cond_26

    .line 5415
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()I

    move-result v2

    .line 5417
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->C:I

    if-eq v2, v3, :cond_26

    move v0, v1

    .line 5418
    goto :goto_26

    :cond_43
    move v1, v0

    goto :goto_25
.end method

.method private A()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 7617
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    if-eqz v1, :cond_6

    .line 7642
    :cond_5
    :goto_5
    return v0

    .line 7622
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->w()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestFocus()Z

    move-result v1

    if-nez v1, :cond_1a

    .line 7623
    :cond_12
    const-string v1, "TextView"

    const-string v2, "TextView does not support text selection. Action mode cancelled."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 7627
    :cond_1a
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->z()Z

    move-result v1

    .line 7628
    if-eqz v1, :cond_5

    .line 7633
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/content/Context;Lcom/google/android/apps/docs/editors/text/TextView;)LER;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    .line 7634
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    if-eqz v1, :cond_2d

    const/4 v0, 0x1

    .line 7636
    :cond_2d
    if-eqz v0, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-nez v1, :cond_5

    .line 7639
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()V

    goto :goto_5
.end method

.method private B()V
    .registers 1

    .prologue
    .line 5437
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 5438
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 5439
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 5440
    return-void
.end method

.method private C()V
    .registers 3

    .prologue
    .line 5985
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_5

    .line 5997
    :cond_4
    :goto_4
    return-void

    .line 5987
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(F)Z

    move-result v0

    if-nez v0, :cond_4

    .line 5991
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v0}, LFB;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_26
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_32

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_32
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5994
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    if-nez v0, :cond_4a

    new-instance v0, LFB;

    invoke-direct {v0, p0}, LFB;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    .line 5995
    :cond_4a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:I

    invoke-virtual {v0, v1}, LFB;->a(I)V

    goto :goto_4
.end method

.method private D()V
    .registers 2

    .prologue
    .line 6000
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v0}, LFB;->d()Z

    move-result v0

    if-nez v0, :cond_11

    .line 6001
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v0}, LFB;->b()V

    .line 6003
    :cond_11
    return-void
.end method

.method private E()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 6778
    .line 6780
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 6781
    instance-of v3, v0, Landroid/view/WindowManager$LayoutParams;

    if-eqz v3, :cond_67

    .line 6782
    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 6783
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v4, 0x3e8

    if-lt v3, v4, :cond_1d

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v3, 0x7cf

    if-le v0, v3, :cond_61

    :cond_1d
    move v0, v2

    :goto_1e
    move v3, v0

    .line 6788
    :goto_1f
    if-eqz v3, :cond_63

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->s()Z

    move-result v0

    if-eqz v0, :cond_63

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_63

    move v0, v2

    :goto_2c
    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:Z

    .line 6789
    if-eqz v3, :cond_65

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->x()Z

    move-result v0

    if-eqz v0, :cond_65

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_65

    :goto_3a
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->m:Z

    .line 6791
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:Z

    if-nez v0, :cond_4e

    .line 6792
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->G()V

    .line 6793
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    if-eqz v0, :cond_4e

    .line 6794
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    invoke-virtual {v0}, LFA;->d()V

    .line 6795
    iput-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    .line 6799
    :cond_4e
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->m:Z

    if-nez v0, :cond_60

    .line 6800
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    .line 6801
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    if-eqz v0, :cond_60

    .line 6802
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    invoke-virtual {v0}, LFH;->d()V

    .line 6803
    iput-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    .line 6806
    :cond_60
    return-void

    :cond_61
    move v0, v1

    .line 6783
    goto :goto_1e

    :cond_63
    move v0, v1

    .line 6788
    goto :goto_2c

    :cond_65
    move v2, v1

    .line 6789
    goto :goto_3a

    :cond_67
    move v3, v1

    goto :goto_1f
.end method

.method private F()V
    .registers 7

    .prologue
    .line 6911
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->s()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 6912
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 6913
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:J

    .line 6914
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    if-nez v0, :cond_1d

    new-instance v0, LFn;

    invoke-direct {v0, p0}, LFn;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    .line 6915
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    invoke-virtual {v0, v1}, LFn;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6916
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    iget-wide v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:J

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LFn;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 6921
    :cond_30
    :goto_30
    return-void

    .line 6919
    :cond_31
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    invoke-virtual {v0, v1}, LFn;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_30
.end method

.method private G()V
    .registers 2

    .prologue
    .line 8727
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    if-eqz v0, :cond_9

    .line 8728
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    invoke-virtual {v0}, LFA;->c()V

    .line 8730
    :cond_9
    return-void
.end method

.method private H()V
    .registers 2

    .prologue
    .line 8737
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->G()V

    .line 8738
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    .line 8739
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEX;

    invoke-virtual {v0}, LEX;->b()V

    .line 8740
    return-void
.end method

.method private a()I
    .registers 3

    .prologue
    .line 5358
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(LEj;Z)I

    move-result v0

    return v0
.end method

.method private a(ILandroid/view/KeyEvent;Landroid/view/KeyEvent;)I
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 4376
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    .line 4496
    :goto_b
    return v0

    .line 4380
    :cond_c
    sparse-switch p1, :sswitch_data_f0

    .line 4445
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_ba

    .line 4447
    if-eqz p3, :cond_ed

    .line 4449
    :try_start_15
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()V

    .line 4450
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v5, p0, v0, p3}, Landroid/text/method/KeyListener;->onKeyOther(Landroid/view/View;Landroid/text/Editable;Landroid/view/KeyEvent;)Z
    :try_end_21
    .catchall {:try_start_15 .. :try_end_21} :catchall_b5
    .catch Ljava/lang/AbstractMethodError; {:try_start_15 .. :try_end_21} :catch_af

    move-result v0

    .line 4452
    if-eqz v0, :cond_94

    .line 4459
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    move v0, v2

    goto :goto_b

    .line 4382
    :sswitch_29
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->p:Z

    .line 4383
    invoke-static {p2}, LFW;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 4388
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-eqz v0, :cond_4b

    .line 4391
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v0, v0, LFv;->a:LFD;

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v0, v0, LFv;->a:LFD;

    invoke-interface {v0, p0, v1, p2}, LFD;->a(Lcom/google/android/apps/docs/editors/text/TextView;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 4394
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iput-boolean v3, v0, LFv;->a:Z

    move v0, v2

    .line 4396
    goto :goto_b

    .line 4403
    :cond_4b
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_59

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 4404
    :cond_59
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_5f

    move v0, v1

    .line 4405
    goto :goto_b

    :cond_5f
    move v0, v2

    .line 4407
    goto :goto_b

    .line 4413
    :sswitch_61
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->o:Z

    .line 4414
    invoke-static {p2}, LFW;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 4415
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    .line 4416
    goto :goto_b

    .line 4422
    :sswitch_71
    invoke-static {p2}, LFW;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 4423
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v1

    .line 4424
    goto :goto_b

    .line 4427
    :cond_7f
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 4428
    if-eqz v0, :cond_88

    .line 4429
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_88
    move v0, v2

    .line 4431
    goto :goto_b

    .line 4438
    :sswitch_8a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    if-eqz v0, :cond_f

    .line 4439
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    move v0, v2

    .line 4440
    goto/16 :goto_b

    .line 4459
    :cond_94
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    move v0, v1

    .line 4463
    :goto_98
    if-eqz v0, :cond_ba

    .line 4464
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()V

    .line 4465
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v5, p0, v0, p1, p2}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 4466
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    .line 4467
    if-eqz v0, :cond_ba

    move v0, v3

    goto/16 :goto_b

    .line 4455
    :catch_af
    move-exception v0

    .line 4459
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    move v0, v3

    .line 4460
    goto :goto_98

    .line 4459
    :catchall_b5
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()V

    throw v0

    .line 4474
    :cond_ba
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_e8

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_e8

    .line 4476
    if-eqz p3, :cond_d4

    .line 4478
    :try_start_c4
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v5, p0, v0, p3}, LFZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/KeyEvent;)Z
    :try_end_cd
    .catch Ljava/lang/AbstractMethodError; {:try_start_c4 .. :try_end_cd} :catch_eb

    move-result v0

    .line 4480
    if-eqz v0, :cond_d3

    move v0, v2

    .line 4481
    goto/16 :goto_b

    :cond_d3
    move v3, v1

    .line 4488
    :cond_d4
    :goto_d4
    if-eqz v3, :cond_e8

    .line 4489
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v2, p0, v0, p1, p2}, LFZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_e8

    .line 4490
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()V

    move v0, v4

    .line 4491
    goto/16 :goto_b

    :cond_e8
    move v0, v1

    .line 4496
    goto/16 :goto_b

    .line 4483
    :catch_eb
    move-exception v0

    goto :goto_d4

    :cond_ed
    move v0, v3

    goto :goto_98

    .line 4380
    nop

    :sswitch_data_f0
    .sparse-switch
        0x4 -> :sswitch_8a
        0x17 -> :sswitch_61
        0x3d -> :sswitch_71
        0x42 -> :sswitch_29
    .end sparse-switch
.end method

.method private static a(J)I
    .registers 4
    .parameter

    .prologue
    .line 7132
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    long-to-int v0, v0

    return v0
.end method

.method private static a(LEj;)I
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 5194
    invoke-interface {p0}, LEj;->d()I

    move-result v3

    .line 5195
    invoke-interface {p0}, LEj;->a()Ljava/lang/CharSequence;

    move-result-object v4

    .line 5196
    const/4 v1, 0x0

    move v2, v0

    .line 5201
    :goto_b
    add-int/lit8 v5, v3, -0x1

    if-ge v2, v5, :cond_22

    .line 5202
    invoke-interface {p0, v2}, LEj;->h(I)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    if-eq v5, v6, :cond_1f

    const/4 v0, -0x1

    .line 5209
    :goto_1e
    return v0

    .line 5201
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 5205
    :cond_22
    :goto_22
    if-ge v0, v3, :cond_2f

    .line 5206
    invoke-interface {p0, v0}, LEj;->e(I)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 5205
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 5209
    :cond_2f
    invoke-static {v1}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_1e
.end method

.method private a(LEj;Z)I
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 5362
    if-nez p1, :cond_4

    .line 5363
    const/4 v0, 0x0

    .line 5389
    :goto_3
    return v0

    .line 5366
    :cond_4
    invoke-interface {p1}, LEj;->d()I

    move-result v1

    .line 5367
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v2

    add-int/2addr v2, v0

    .line 5368
    invoke-interface {p1, v1}, LEj;->b(I)I

    move-result v0

    .line 5370
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 5371
    if-eqz v3, :cond_25

    .line 5372
    iget v4, v3, LFt;->g:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 5373
    iget v3, v3, LFt;->h:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 5376
    :cond_25
    add-int/2addr v0, v2

    .line 5378
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_42

    .line 5379
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    if-ge v1, v2, :cond_39

    .line 5380
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    sub-int v1, v3, v1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 5387
    :cond_39
    :goto_39
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_3

    .line 5383
    :cond_42
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_39
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;II)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->b(II)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;Z)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v0

    return v0
.end method

.method private a(Z)I
    .registers 7
    .parameter

    .prologue
    .line 3345
    const/4 v0, 0x0

    .line 3346
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v1, v1, 0x70

    .line 3348
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    .line 3350
    const/16 v3, 0x30

    if-eq v1, v3, :cond_25

    .line 3353
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    move-result v4

    sub-int/2addr v3, v4

    .line 3354
    invoke-interface {v2}, LEj;->c()I

    move-result v2

    .line 3356
    if-ge v2, v3, :cond_25

    .line 3357
    const/16 v0, 0x50

    if-ne v1, v0, :cond_26

    .line 3358
    sub-int v0, v3, v2

    .line 3364
    :cond_25
    :goto_25
    return v0

    .line 3361
    :cond_26
    sub-int v0, v3, v2

    shr-int/lit8 v0, v0, 0x1

    goto :goto_25
.end method

.method public static synthetic a()J
    .registers 2

    .prologue
    .line 144
    sget-wide v0, Lcom/google/android/apps/docs/editors/text/TextView;->c:J

    return-wide v0
.end method

.method private static a(II)J
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 7128
    int-to-long v0, p0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private a(IILjava/lang/CharSequence;)J
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v3, 0xa

    .line 7411
    if-eqz p3, :cond_6a

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_6a

    .line 7412
    if-lez p1, :cond_3d

    .line 7413
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 7414
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 7416
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-eqz v2, :cond_6f

    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-eqz v2, :cond_6f

    .line 7418
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 7419
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    add-int/lit8 v2, p1, -0x1

    invoke-interface {v0, v2, p1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 7423
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    sub-int/2addr v0, v1

    .line 7424
    add-int/2addr p1, v0

    .line 7425
    add-int/2addr p2, v0

    .line 7438
    :cond_3d
    :goto_3d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge p2, v0, :cond_6a

    .line 7439
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p3, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 7440
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    invoke-interface {v1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 7442
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-eqz v2, :cond_98

    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-eqz v2, :cond_98

    .line 7444
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    add-int/lit8 v1, p2, 0x1

    invoke-interface {v0, p2, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 7453
    :cond_6a
    :goto_6a
    invoke-static {p1, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)J

    move-result-wide v0

    return-wide v0

    .line 7426
    :cond_6f
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-nez v2, :cond_3d

    if-eq v0, v3, :cond_3d

    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-nez v0, :cond_3d

    if-eq v1, v3, :cond_3d

    .line 7429
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 7430
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    const-string v2, " "

    invoke-interface {v0, p1, p1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 7432
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    sub-int/2addr v0, v1

    .line 7433
    add-int/2addr p1, v0

    .line 7434
    add-int/2addr p2, v0

    goto :goto_3d

    .line 7445
    :cond_98
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v2

    if-nez v2, :cond_6a

    if-eq v0, v3, :cond_6a

    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-nez v0, :cond_6a

    if-eq v1, v3, :cond_6a

    .line 7448
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    const-string v1, " "

    invoke-interface {v0, p2, p2, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_6a
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)LEX;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEX;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)LFZ;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;LFq;)LFq;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFq;

    return-object p1
.end method

.method private a(FF)LGE;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 6650
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v1

    .line 6652
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    const-class v2, LGE;

    invoke-interface {v0, v1, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGE;

    .line 6653
    array-length v1, v0

    if-lez v1, :cond_21

    aget-object v1, v0, v3

    invoke-virtual {v1}, LGE;->a()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 6654
    aget-object v0, v0, v3

    .line 6656
    :goto_20
    return-object v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static synthetic a()Landroid/graphics/Rect;
    .registers 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static synthetic a()Landroid/graphics/RectF;
    .registers 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 7795
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_2d

    .line 7796
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    move-object v0, p1

    .line 7797
    check-cast v0, Landroid/text/Spanned;

    move v3, p2

    .line 7800
    :goto_d
    if-ge p2, p3, :cond_2b

    .line 7801
    const-class v1, LGC;

    invoke-interface {v0, v3, p3, v1}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result p2

    .line 7802
    add-int/lit8 v1, v3, 0x1

    const-class v4, LGC;

    invoke-interface {v0, v3, v1, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LGC;

    .line 7803
    array-length v1, v1

    if-nez v1, :cond_29

    .line 7804
    invoke-interface {p1, v3, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_29
    move v3, p2

    .line 7807
    goto :goto_d

    :cond_2b
    move-object v0, v2

    .line 7813
    :goto_2c
    return-object v0

    .line 7810
    :cond_2d
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2c
.end method

.method private a(F)V
    .registers 3
    .parameter

    .prologue
    .line 1777
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1c

    .line 1778
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1780
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_1c

    .line 1781
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 1782
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 1783
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1786
    :cond_1c
    return-void
.end method

.method private a(IIIF)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x3f00

    .line 4188
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    if-nez v0, :cond_18

    .line 4189
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->k:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, p1

    .line 4191
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    if-nez v0, :cond_23

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    .line 4193
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 4194
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 4195
    sub-float v1, p4, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 4196
    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 4197
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, p1

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int v3, p2, v3

    add-int/2addr v0, v1

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, p3

    invoke-virtual {v2, v1, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 4199
    return-void
.end method

.method private a(IZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2991
    and-int/lit8 v2, p1, 0xf

    .line 2993
    if-ne v2, v0, :cond_30

    .line 2994
    const v2, 0x8000

    and-int/2addr v2, p1

    if-eqz v2, :cond_d

    move v1, v0

    .line 2996
    :cond_d
    and-int/lit16 v0, p1, 0x1000

    if-eqz v0, :cond_1f

    .line 2997
    sget-object v0, LGp;->d:LGp;

    .line 3005
    :goto_13
    invoke-static {v1, v0}, LGo;->a(ZLGp;)LGo;

    move-result-object v0

    .line 3027
    :goto_17
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setRawInputType(I)V

    .line 3028
    if-eqz p2, :cond_69

    .line 3029
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    .line 3033
    :goto_1e
    return-void

    .line 2998
    :cond_1f
    and-int/lit16 v0, p1, 0x2000

    if-eqz v0, :cond_26

    .line 2999
    sget-object v0, LGp;->c:LGp;

    goto :goto_13

    .line 3000
    :cond_26
    and-int/lit16 v0, p1, 0x4000

    if-eqz v0, :cond_2d

    .line 3001
    sget-object v0, LGp;->b:LGp;

    goto :goto_13

    .line 3003
    :cond_2d
    sget-object v0, LGp;->a:LGp;

    goto :goto_13

    .line 3006
    :cond_30
    const/4 v3, 0x2

    if-ne v2, v3, :cond_45

    .line 3007
    and-int/lit16 v2, p1, 0x1000

    if-eqz v2, :cond_41

    move v2, v0

    :goto_38
    and-int/lit16 v3, p1, 0x2000

    if-eqz v3, :cond_43

    :goto_3c
    invoke-static {v2, v0}, LFV;->a(ZZ)LFV;

    move-result-object v0

    goto :goto_17

    :cond_41
    move v2, v1

    goto :goto_38

    :cond_43
    move v0, v1

    goto :goto_3c

    .line 3010
    :cond_45
    const/4 v0, 0x4

    if-ne v2, v0, :cond_5c

    .line 3011
    and-int/lit16 v0, p1, 0xff0

    sparse-switch v0, :sswitch_data_6e

    .line 3019
    invoke-static {}, LFT;->a()LFT;

    move-result-object v0

    goto :goto_17

    .line 3013
    :sswitch_52
    invoke-static {}, LFS;->a()LFS;

    move-result-object v0

    goto :goto_17

    .line 3016
    :sswitch_57
    invoke-static {}, LGs;->a()LGs;

    move-result-object v0

    goto :goto_17

    .line 3022
    :cond_5c
    const/4 v0, 0x3

    if-ne v2, v0, :cond_64

    .line 3023
    invoke-static {}, LFU;->a()LFU;

    move-result-object v0

    goto :goto_17

    .line 3025
    :cond_64
    invoke-static {}, LGo;->a()LGo;

    move-result-object v0

    goto :goto_17

    .line 3031
    :cond_69
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/method/KeyListener;)V

    goto :goto_1e

    .line 3011
    nop

    :sswitch_data_6e
    .sparse-switch
        0x10 -> :sswitch_52
        0x20 -> :sswitch_57
    .end sparse-switch
.end method

.method private a(Landroid/content/ClipData;)V
    .registers 4
    .parameter

    .prologue
    .line 7769
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 7771
    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 7772
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/docs/editors/text/TextView;->c:J

    .line 7773
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter

    .prologue
    .line 4112
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;II)V

    .line 4115
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;II)V

    .line 4118
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;II)V

    .line 4121
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;II)V

    .line 4123
    return-void
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 4202
    if-eqz p2, :cond_19

    const/4 v1, 0x1

    .line 4203
    :goto_5
    if-eqz v1, :cond_b

    int-to-float v2, p2

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 4204
    :cond_b
    :goto_b
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    if-ge v0, v2, :cond_1b

    .line 4205
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 4204
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_19
    move v1, v0

    .line 4202
    goto :goto_5

    .line 4207
    :cond_1b
    if-eqz v1, :cond_22

    neg-int v0, p2

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 4208
    :cond_22
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;II)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4126
    if-eqz p2, :cond_15

    .line 4127
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v0

    .line 4128
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    .line 4130
    invoke-virtual {p2, p3, v0, p4, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 4131
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 4133
    :cond_15
    return-void
.end method

.method private a(Landroid/graphics/Rect;)V
    .registers 4
    .parameter

    .prologue
    .line 5781
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()I

    move-result v0

    .line 5782
    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 5783
    iget v1, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 5785
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->q()I

    move-result v0

    .line 5786
    iget v1, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 5787
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 5788
    return-void
.end method

.method private a(Landroid/graphics/Rect;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 5771
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/graphics/Rect;)V

    .line 5776
    if-nez p2, :cond_e

    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 5777
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_21

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 5778
    :cond_21
    return-void
.end method

.method private a(Landroid/text/Editable;[Landroid/text/InputFilter;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 3323
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    instance-of v0, v0, Landroid/text/InputFilter;

    if-eqz v0, :cond_1b

    .line 3324
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v1, v0, [Landroid/text/InputFilter;

    .line 3326
    array-length v0, p2

    invoke-static {p2, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3327
    array-length v2, p2

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    check-cast v0, Landroid/text/InputFilter;

    aput-object v0, v1, v2

    .line 3329
    invoke-interface {p1, v1}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    .line 3333
    :goto_1a
    return-void

    .line 3331
    :cond_1b
    invoke-interface {p1, p2}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_1a
.end method

.method static a(Landroid/text/Spannable;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4785
    const-class v0, Landroid/text/ParcelableSpan;

    invoke-interface {p0, p1, p2, v0}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    .line 4786
    array-length v0, v1

    .line 4787
    :goto_7
    if-lez v0, :cond_11

    .line 4788
    add-int/lit8 v0, v0, -0x1

    .line 4789
    aget-object v2, v1, v0

    invoke-interface {p0, v2}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    goto :goto_7

    .line 4791
    :cond_11
    return-void
.end method

.method private a(Landroid/text/method/KeyListener;)V
    .registers 4
    .parameter

    .prologue
    .line 1188
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    .line 1189
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Editable;

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1191
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:[Landroid/text/InputFilter;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/Editable;[Landroid/text/InputFilter;)V

    .line 1192
    return-void
.end method

.method private a(Landroid/view/DragEvent;)V
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 8825
    invoke-static {}, LEt;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 8882
    :cond_7
    :goto_7
    return-void

    .line 8829
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8830
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    .line 8831
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    move v0, v1

    .line 8832
    :goto_18
    if-ge v0, v4, :cond_2a

    .line 8833
    invoke-virtual {v2, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v5

    .line 8834
    iget-object v6, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 8832
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 8837
    :cond_2a
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v4

    .line 8839
    invoke-virtual {p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v0

    .line 8840
    const/4 v2, 0x0

    .line 8841
    instance-of v5, v0, LFs;

    if-eqz v5, :cond_44

    .line 8842
    check-cast v0, LFs;

    move-object v2, v0

    .line 8844
    :cond_44
    if-eqz v2, :cond_4c

    iget-object v0, v2, LFs;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    if-ne v0, p0, :cond_4c

    const/4 v0, 0x1

    move v1, v0

    .line 8846
    :cond_4c
    if-eqz v1, :cond_56

    .line 8847
    iget v0, v2, LFs;->a:I

    if-lt v4, v0, :cond_56

    iget v0, v2, LFs;->b:I

    if-lt v4, v0, :cond_7

    .line 8853
    :cond_56
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v5

    .line 8854
    invoke-direct {p0, v4, v4, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IILjava/lang/CharSequence;)J

    move-result-wide v6

    .line 8855
    invoke-static {v6, v7}, Lcom/google/android/apps/docs/editors/text/TextView;->a(J)I

    move-result v4

    .line 8856
    invoke-static {v6, v7}, Lcom/google/android/apps/docs/editors/text/TextView;->b(J)I

    move-result v6

    .line 8858
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 8859
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v0, v4, v6, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 8861
    if-eqz v1, :cond_7

    .line 8862
    iget v1, v2, LFs;->a:I

    .line 8863
    iget v0, v2, LFs;->b:I

    .line 8864
    if-gt v6, v1, :cond_c9

    .line 8866
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sub-int/2addr v2, v5

    .line 8867
    add-int/2addr v1, v2

    .line 8868
    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    .line 8872
    :goto_89
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v0, v2, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 8875
    if-eqz v2, :cond_a0

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_a0
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eq v2, v0, :cond_b4

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 8878
    :cond_b4
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ne v2, v0, :cond_be

    add-int/lit8 v2, v2, -0x1

    .line 8879
    :cond_be
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    add-int/lit8 v1, v2, 0x1

    invoke-interface {v0, v2, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto/16 :goto_7

    :cond_c9
    move v2, v1

    move v1, v0

    goto :goto_89
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 1
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;II)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->c(II)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/graphics/Rect;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/graphics/Rect;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/editors/text/TextView;->d(Ljava/lang/CharSequence;III)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter

    .prologue
    .line 7776
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 7779
    invoke-virtual {v0, p1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 7781
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/docs/editors/text/TextView;->c:J

    .line 7782
    return-void
.end method

.method private a(Ljava/lang/CharSequence;LFo;ZI)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x12

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 2716
    if-nez p1, :cond_174

    .line 2717
    const-string v1, ""

    .line 2720
    :goto_8
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->j:Z

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    const/high16 v3, 0x3f80

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setTextScaleX(F)V

    .line 2722
    :cond_13
    instance-of v0, v1, Landroid/text/Spanned;

    if-eqz v0, :cond_2a

    move-object v0, v1

    check-cast v0, Landroid/text/Spanned;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-interface {v0, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_2a

    .line 2723
    invoke-virtual {p0, v7}, Lcom/google/android/apps/docs/editors/text/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 2724
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2727
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:[Landroid/text/InputFilter;

    array-length v9, v0

    move v8, v2

    .line 2728
    :goto_2e
    if-ge v8, v9, :cond_47

    .line 2729
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:[Landroid/text/InputFilter;

    aget-object v0, v0, v8

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Spanned;

    move v5, v2

    move v6, v2

    invoke-interface/range {v0 .. v6}, Landroid/text/InputFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2730
    if-eqz v0, :cond_43

    move-object v1, v0

    .line 2728
    :cond_43
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2e

    .line 2735
    :cond_47
    if-eqz p3, :cond_5c

    .line 2736
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_ec

    .line 2737
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result p4

    .line 2738
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-direct {p0, v0, v2, p4, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->d(Ljava/lang/CharSequence;III)V

    .line 2746
    :cond_5c
    :goto_5c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_171

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_171

    move v4, v7

    .line 2750
    :goto_69
    sget-object v0, LFo;->c:LFo;

    if-eq p2, v0, :cond_73

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-nez v0, :cond_73

    if-eqz v4, :cond_f7

    .line 2751
    :cond_73
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Editable$Factory;

    invoke-virtual {v0, v1}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v1

    .line 2753
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:[Landroid/text/InputFilter;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/Editable;[Landroid/text/InputFilter;)V

    .line 2754
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 2755
    if-eqz v0, :cond_89

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 2762
    :cond_89
    :goto_89
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:I

    if-eqz v0, :cond_bf

    .line 2765
    sget-object v0, LFo;->c:LFo;

    if-eq p2, v0, :cond_95

    instance-of v0, v1, Landroid/text/Spannable;

    if-eqz v0, :cond_110

    :cond_95
    move-object v0, v1

    .line 2766
    check-cast v0, Landroid/text/Spannable;

    .line 2771
    :goto_98
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:I

    invoke-static {v0, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    move-result v3

    if-eqz v3, :cond_bf

    .line 2773
    sget-object v1, LFo;->c:LFo;

    if-ne p2, v1, :cond_117

    sget-object v1, LFo;->c:LFo;

    .line 2780
    :goto_a6
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    .line 2785
    instance-of v3, p0, Lcom/google/android/apps/docs/editors/text/EditText;

    if-nez v3, :cond_b0

    iget-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-eqz v3, :cond_11a

    .line 2786
    :cond_b0
    :goto_b0
    iget-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:Z

    if-eqz v3, :cond_bd

    if-nez v7, :cond_bd

    .line 2787
    invoke-static {}, LFX;->a()LFZ;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->setMovementMethod(LFZ;)V

    :cond_bd
    move-object p2, v1

    move-object v1, v0

    .line 2792
    :cond_bf
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFo;

    .line 2793
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    .line 2795
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    if-nez v0, :cond_11c

    .line 2796
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    .line 2800
    :goto_c9
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    .line 2802
    instance-of v0, v1, Landroid/text/Spannable;

    if-eqz v0, :cond_159

    move-object v0, v1

    .line 2803
    check-cast v0, Landroid/text/Spannable;

    .line 2807
    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v3

    const-class v5, LFp;

    invoke-interface {v0, v2, v3, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [LFp;

    .line 2808
    array-length v7, v3

    move v5, v2

    .line 2809
    :goto_e2
    if-ge v5, v7, :cond_125

    .line 2810
    aget-object v8, v3, v5

    invoke-interface {v0, v8}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2809
    add-int/lit8 v5, v5, 0x1

    goto :goto_e2

    .line 2740
    :cond_ec
    const-string v0, ""

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-direct {p0, v0, v2, v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->d(Ljava/lang/CharSequence;III)V

    goto/16 :goto_5c

    .line 2756
    :cond_f7
    sget-object v0, LFo;->b:LFo;

    if-eq p2, v0, :cond_ff

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_106

    .line 2757
    :cond_ff
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Spannable$Factory;

    invoke-virtual {v0, v1}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v1

    goto :goto_89

    .line 2758
    :cond_106
    instance-of v0, v1, LDX;

    if-nez v0, :cond_89

    .line 2759
    invoke-static {v1}, Landroid/text/TextUtils;->stringOrSpannedString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    goto/16 :goto_89

    .line 2768
    :cond_110
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Spannable$Factory;

    invoke-virtual {v0, v1}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_98

    .line 2773
    :cond_117
    sget-object v1, LFo;->b:LFo;

    goto :goto_a6

    :cond_11a
    move v7, v2

    .line 2785
    goto :goto_b0

    .line 2798
    :cond_11c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    invoke-interface {v0, v1, p0}, LGv;->a(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    goto :goto_c9

    .line 2812
    :cond_125
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFp;

    if-nez v3, :cond_131

    new-instance v3, LFp;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, LFp;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V

    iput-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFp;

    .line 2814
    :cond_131
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFp;

    const v5, 0x640012

    invoke-interface {v0, v3, v2, v6, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2817
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v3, :cond_142

    .line 2818
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    invoke-interface {v0, v3, v2, v6, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2821
    :cond_142
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    if-eqz v3, :cond_14b

    .line 2822
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    invoke-interface {v0, v3, v2, v6, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2826
    :cond_14b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_159

    .line 2827
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    move-object v0, v1

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v3, p0, v0}, LFZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;)V

    .line 2834
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Z

    .line 2838
    :cond_159
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_160

    .line 2839
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->B()V

    .line 2842
    :cond_160
    invoke-virtual {p0, v1, v2, p4, v6}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Ljava/lang/CharSequence;III)V

    .line 2843
    invoke-virtual {p0, v1, v2, p4, v6}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;III)V

    .line 2845
    if-eqz v4, :cond_16d

    .line 2846
    check-cast v1, Landroid/text/Editable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/Editable;)V

    .line 2851
    :cond_16d
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    .line 2852
    return-void

    :cond_171
    move v4, v2

    goto/16 :goto_69

    :cond_174
    move-object v1, p1

    goto/16 :goto_8
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 5861
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x1

    if-ne v0, v1, :cond_11

    .line 5862
    if-eqz p1, :cond_12

    .line 5863
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    .line 5868
    :cond_11
    :goto_11
    return-void

    .line 5865
    :cond_12
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    goto :goto_11
.end method

.method private a(ZZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 5872
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    .line 5873
    if-eqz p1, :cond_15

    .line 5874
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setLines(I)V

    .line 5875
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setHorizontallyScrolling(Z)V

    .line 5876
    if-eqz p2, :cond_14

    .line 5877
    invoke-static {}, LGn;->a()LGn;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setTransformationMethod(LGv;)V

    .line 5888
    :cond_14
    :goto_14
    return-void

    .line 5880
    :cond_15
    if-eqz p3, :cond_1d

    .line 5881
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setMaxLines(I)V

    .line 5883
    :cond_1d
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setHorizontallyScrolling(Z)V

    .line 5884
    if-eqz p2, :cond_14

    .line 5885
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setTransformationMethod(LGv;)V

    goto :goto_14
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 2975
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    instance-of v0, v0, LGd;

    return v0
.end method

.method private a(F)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/high16 v4, 0x3f80

    .line 5171
    invoke-static {}, LEt;->a()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isHardwareAccelerated()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 5190
    :cond_11
    :goto_11
    return v0

    .line 5174
    :cond_12
    cmpl-float v2, p1, v3

    if-lez v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v2, :cond_11

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()I

    move-result v2

    if-ne v2, v1, :cond_11

    iget-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->j:Z

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextScaleX()F

    move-result v2

    cmpl-float v2, v2, v4

    if-nez v2, :cond_11

    .line 5176
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->e(I)F

    move-result v2

    .line 5177
    add-float/2addr v2, v4

    sub-float/2addr v2, p1

    div-float/2addr v2, p1

    .line 5178
    cmpl-float v3, v2, v3

    if-lez v3, :cond_11

    const v3, 0x3d8f5c29

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_11

    .line 5179
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    sub-float v2, v4, v2

    const v3, 0x3ba3d70a

    sub-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextScaleX(F)V

    .line 5180
    new-instance v0, LFl;

    invoke-direct {v0, p0}, LFl;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 5186
    goto :goto_11
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/text/TextView;)Z
    .registers 2
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Z

    move-result v0

    return v0
.end method

.method private b()I
    .registers 3

    .prologue
    .line 5791
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private b(I)I
    .registers 5
    .parameter

    .prologue
    .line 8760
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->j()I

    move-result v0

    sub-int v0, p1, v0

    .line 8762
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 8763
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 8764
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v1

    add-int/2addr v0, v1

    .line 8765
    return v0
.end method

.method private b(II)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 8778
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)I

    move-result v0

    .line 8779
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v1

    int-to-float v0, v0

    invoke-interface {v1, p1, v0}, LEj;->a(IF)I

    move-result v0

    return v0
.end method

.method private static b(J)I
    .registers 4
    .parameter

    .prologue
    .line 7136
    const-wide v0, 0xffffffffL

    and-long/2addr v0, p0

    long-to-int v0, v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->i:I

    return v0
.end method

.method private b(Z)I
    .registers 7
    .parameter

    .prologue
    .line 3368
    const/4 v0, 0x0

    .line 3369
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v1, v1, 0x70

    .line 3371
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    .line 3373
    const/16 v3, 0x50

    if-eq v1, v3, :cond_25

    .line 3376
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    move-result v4

    sub-int/2addr v3, v4

    .line 3377
    invoke-interface {v2}, LEj;->c()I

    move-result v2

    .line 3379
    if-ge v2, v3, :cond_25

    .line 3380
    const/16 v0, 0x30

    if-ne v1, v0, :cond_26

    .line 3381
    sub-int v0, v3, v2

    .line 3387
    :cond_25
    :goto_25
    return v0

    .line 3384
    :cond_26
    sub-int v0, v3, v2

    shr-int/lit8 v0, v0, 0x1

    goto :goto_25
.end method

.method private b()J
    .registers 3

    .prologue
    .line 7211
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:Z

    if-eqz v0, :cond_11

    .line 7212
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v1

    .line 7213
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v0

    .line 7220
    :goto_c
    invoke-static {v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)J

    move-result-wide v0

    return-wide v0

    .line 7215
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFH;

    move-result-object v0

    .line 7216
    invoke-virtual {v0}, LFH;->a()I

    move-result v1

    .line 7217
    invoke-virtual {v0}, LFH;->b()I

    move-result v0

    goto :goto_c
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method private b()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 7245
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/text/TextView;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1226
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_14

    .line 1227
    :cond_a
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->setFocusable(Z)V

    .line 1228
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->setClickable(Z)V

    .line 1229
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->setLongClickable(Z)V

    .line 1235
    :goto_13
    return-void

    .line 1231
    :cond_14
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setFocusable(Z)V

    .line 1232
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setClickable(Z)V

    .line 1233
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setLongClickable(Z)V

    goto :goto_13
.end method

.method private b(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1010
    const/4 v0, 0x0

    .line 1011
    packed-switch p1, :pswitch_data_12

    .line 1025
    :goto_4
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1026
    return-void

    .line 1013
    :pswitch_8
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_4

    .line 1017
    :pswitch_b
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_4

    .line 1021
    :pswitch_e
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_4

    .line 1011
    nop

    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_8
        :pswitch_b
        :pswitch_e
    .end packed-switch
.end method

.method private b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;II)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4137
    if-eqz p2, :cond_1a

    .line 4138
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v0

    .line 4139
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    .line 4141
    invoke-virtual {p2, v0, p3, v1, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 4142
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 4144
    :cond_1a
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 1
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->G()V

    return-void
.end method

.method private b()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 4348
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-nez v2, :cond_7

    .line 4364
    :cond_6
    :goto_6
    return v0

    .line 4352
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    if-eqz v2, :cond_d

    move v0, v1

    .line 4353
    goto :goto_6

    .line 4356
    :cond_d
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    and-int/lit8 v2, v2, 0xf

    if-ne v2, v1, :cond_6

    .line 4357
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    and-int/lit16 v2, v2, 0xff0

    .line 4358
    const/16 v3, 0x20

    if-eq v2, v3, :cond_1f

    const/16 v3, 0x30

    if-ne v2, v3, :cond_6

    :cond_1f
    move v0, v1

    .line 4360
    goto :goto_6
.end method

.method private b(II)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 8788
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v1

    if-nez v1, :cond_8

    .line 8795
    :cond_7
    :goto_7
    return v0

    .line 8790
    :cond_8
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(I)I

    move-result v1

    .line 8791
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)I

    move-result v2

    .line 8793
    int-to-float v3, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v4

    invoke-interface {v4, v1}, LEj;->c(I)F

    move-result v4

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_7

    .line 8794
    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v3

    invoke-interface {v3, v1}, LEj;->d(I)F

    move-result v1

    cmpl-float v1, v2, v1

    if-gtz v1, :cond_7

    .line 8795
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/text/TextView;)Z
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->j:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method private c()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2407
    .line 2408
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getDrawableState()[I

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 2409
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:I

    if-eq v0, v3, :cond_4f

    .line 2410
    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:I

    move v0, v1

    .line 2413
    :goto_13
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/content/res/ColorStateList;

    if-eqz v3, :cond_2c

    .line 2414
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getDrawableState()[I

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    .line 2415
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    iget v4, v4, Landroid/text/TextPaint;->linkColor:I

    if-eq v3, v4, :cond_2c

    .line 2416
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    iput v3, v0, Landroid/text/TextPaint;->linkColor:I

    move v0, v1

    .line 2420
    :cond_2c
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/content/res/ColorStateList;

    if-eqz v3, :cond_49

    .line 2421
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getDrawableState()[I

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v2

    .line 2422
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:I

    if-eq v2, v3, :cond_49

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_49

    .line 2423
    iput v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:I

    move v0, v1

    .line 2427
    :cond_49
    if-eqz v0, :cond_4e

    .line 2428
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2430
    :cond_4e
    return-void

    :cond_4f
    move v0, v2

    goto :goto_13
.end method

.method private c(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 7724
    invoke-static {}, LEt;->a()Z

    move-result v0

    if-eqz v0, :cond_70

    .line 7725
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 7729
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v3

    .line 7730
    if-eqz v3, :cond_70

    move v2, v1

    .line 7732
    :goto_1a
    invoke-virtual {v3}, Landroid/content/ClipData;->getItemCount()I

    move-result v0

    if-ge v1, v0, :cond_68

    .line 7733
    invoke-virtual {v3, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 7734
    if-eqz v4, :cond_4b

    .line 7735
    if-nez v2, :cond_4f

    .line 7736
    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IILjava/lang/CharSequence;)J

    move-result-wide v5

    .line 7737
    invoke-static {v5, v6}, Lcom/google/android/apps/docs/editors/text/TextView;->a(J)I

    move-result p1

    .line 7738
    invoke-static {v5, v6}, Lcom/google/android/apps/docs/editors/text/TextView;->b(J)I

    move-result p2

    .line 7739
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 7740
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v0, p1, p2, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 7741
    const/4 v2, 0x1

    .line 7732
    :cond_4b
    :goto_4b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1a

    .line 7743
    :cond_4f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v5

    const-string v6, "\n"

    invoke-interface {v0, v5, v6}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 7744
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v5

    invoke-interface {v0, v5, v4}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_4b

    .line 7748
    :cond_68
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    .line 7749
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/apps/docs/editors/text/TextView;->c:J

    .line 7766
    :cond_6f
    :goto_6f
    return-void

    .line 7754
    :cond_70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 7758
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 7759
    if-eqz v1, :cond_6f

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_6f

    .line 7760
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IILjava/lang/CharSequence;)J

    move-result-wide v2

    .line 7761
    invoke-static {v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->a(J)I

    move-result v4

    .line 7762
    invoke-static {v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->b(J)I

    move-result v2

    .line 7763
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 7764
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v0, v4, v2, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_6f
.end method

.method private c(Z)V
    .registers 4
    .parameter

    .prologue
    .line 6006
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v0, v1, :cond_b

    .line 6007
    if-eqz p1, :cond_c

    .line 6008
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->C()V

    .line 6013
    :cond_b
    :goto_b
    return-void

    .line 6010
    :cond_c
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->D()V

    goto :goto_b
.end method

.method private c(I)Z
    .registers 4
    .parameter

    .prologue
    .line 2920
    const v0, 0x2000f

    and-int/2addr v0, p1

    const v1, 0x20001

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/text/TextView;)Z
    .registers 2
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()Z

    move-result v0

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->h:I

    return v0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 3294
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->h:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v0, v1, :cond_10

    .line 3295
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->h:Z

    .line 3296
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->C()V

    .line 3298
    :cond_10
    return-void
.end method

.method private d(Ljava/lang/CharSequence;III)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6231
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1b

    .line 6232
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    .line 6233
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 6234
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v3, :cond_1b

    .line 6235
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 6234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 6238
    :cond_1b
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/editors/text/TextView;)Z
    .registers 2
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->u()Z

    move-result v0

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()I

    move-result v0

    return v0
.end method

.method private e()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 3488
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 3490
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    if-nez v1, :cond_f

    .line 3491
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 3492
    iput v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    .line 3498
    :cond_e
    :goto_e
    return-void

    .line 3493
    :cond_f
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_e

    .line 3494
    iput v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    goto :goto_e
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/editors/text/TextView;)Z
    .registers 2
    .parameter

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->q()I

    move-result v0

    return v0
.end method

.method private f()V
    .registers 2

    .prologue
    .line 5079
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    .line 5082
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    .line 5083
    return-void
.end method

.method public static synthetic g(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->o:I

    return v0
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 4372
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    if-nez v0, :cond_10

    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->c(I)Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public static synthetic h(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:I

    return v0
.end method

.method private h()Z
    .registers 12

    .prologue
    const/16 v10, 0x50

    const/4 v9, -0x1

    const/4 v1, 0x0

    .line 5446
    .line 5447
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v0, v0, 0x70

    if-ne v0, v10, :cond_de

    .line 5448
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 5451
    :goto_12
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v2

    .line 5452
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v3, v0}, LEj;->o(I)I

    move-result v3

    .line 5453
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    sub-int/2addr v4, v5

    .line 5454
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getBottom()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    move-result v6

    sub-int/2addr v5, v6

    .line 5455
    iget-object v6, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v6}, LEj;->c()I

    move-result v6

    .line 5459
    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    if-ne v2, v7, :cond_95

    .line 5465
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->c(I)F

    move-result v2

    invoke-static {v2}, Landroid/util/FloatMath;->floor(F)F

    move-result v2

    float-to-int v2, v2

    .line 5466
    iget-object v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v7, v0}, LEj;->d(I)F

    move-result v7

    invoke-static {v7}, Landroid/util/FloatMath;->ceil(F)F

    move-result v7

    float-to-int v7, v7

    .line 5467
    iget-object v8, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v8, v0}, LEj;->f(I)I

    move-result v0

    .line 5469
    sub-int v8, v7, v2

    add-int/2addr v8, v0

    if-ge v8, v4, :cond_8e

    .line 5470
    add-int/2addr v2, v7

    div-int/lit8 v2, v2, 0x2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    div-int/lit8 v2, v4, 0x2

    sub-int/2addr v0, v2

    :goto_79
    move v2, v0

    .line 5502
    :goto_7a
    if-ge v6, v5, :cond_d3

    move v0, v1

    .line 5512
    :goto_7d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v3

    if-ne v2, v3, :cond_89

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v3

    if-eq v0, v3, :cond_8d

    .line 5513
    :cond_89
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->scrollTo(II)V

    .line 5514
    const/4 v1, 0x1

    .line 5516
    :cond_8d
    return v1

    .line 5472
    :cond_8e
    if-ne v3, v9, :cond_93

    .line 5473
    sub-int v0, v7, v4

    goto :goto_79

    :cond_93
    move v0, v2

    .line 5475
    goto :goto_79

    .line 5478
    :cond_95
    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    if-ne v2, v7, :cond_b6

    .line 5483
    if-ne v3, v9, :cond_a9

    .line 5484
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->d(I)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    .line 5485
    sub-int/2addr v0, v4

    move v2, v0

    .line 5486
    goto :goto_7a

    .line 5487
    :cond_a9
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->c(I)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->floor(F)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    goto :goto_7a

    .line 5494
    :cond_b6
    if-ne v3, v9, :cond_c5

    .line 5495
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->c(I)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->floor(F)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    goto :goto_7a

    .line 5497
    :cond_c5
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->d(I)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    .line 5498
    sub-int/2addr v0, v4

    move v2, v0

    goto :goto_7a

    .line 5505
    :cond_d3
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v0, v0, 0x70

    if-ne v0, v10, :cond_dc

    .line 5506
    sub-int v0, v6, v5

    goto :goto_7d

    :cond_dc
    move v0, v1

    .line 5508
    goto :goto_7d

    :cond_de
    move v0, v1

    goto/16 :goto_12
.end method

.method public static synthetic i(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:I

    return v0
.end method

.method public static synthetic j(Lcom/google/android/apps/docs/editors/text/TextView;)I
    .registers 2
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->p:I

    return v0
.end method

.method private q()I
    .registers 4

    .prologue
    .line 5795
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v1

    sub-int/2addr v0, v1

    .line 5796
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v1, v1, 0x70

    const/16 v2, 0x30

    if-eq v1, v2, :cond_17

    .line 5797
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 5799
    :cond_17
    return v0
.end method

.method private r()I
    .registers 5

    .prologue
    .line 6574
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    if-eqz v0, :cond_49

    .line 6575
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    invoke-virtual {v0}, LFH;->a()I

    move-result v0

    .line 6576
    if-ltz v0, :cond_49

    .line 6578
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-le v0, v1, :cond_48

    .line 6579
    const-string v1, "TextView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid tap focus position ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " vs "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6581
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 6587
    :cond_48
    :goto_48
    return v0

    :cond_49
    const/4 v0, -0x1

    goto :goto_48
.end method

.method private s()Z
    .registers 2

    .prologue
    .line 5975
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->k:Z

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->u()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private t()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 5979
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v2

    sub-int/2addr v1, v2

    .line 5980
    if-lez v1, :cond_22

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->e(I)F

    move-result v2

    int-to-float v1, v1

    cmpl-float v1, v2, v1

    if-lez v1, :cond_22

    const/4 v0, 0x1

    :cond_22
    return v0
.end method

.method private u()Z
    .registers 2

    .prologue
    .line 6813
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Editable;

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->onCheckIsTextEditor()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private v()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 6899
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v1

    if-nez v1, :cond_8

    .line 6907
    :cond_7
    :goto_7
    return v0

    .line 6901
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v1

    .line 6902
    if-ltz v1, :cond_7

    .line 6904
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v2

    .line 6905
    if-ltz v2, :cond_7

    .line 6907
    if-ne v1, v2, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method private w()Z
    .registers 2

    .prologue
    .line 7069
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->q()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private x()Z
    .registers 2

    .prologue
    .line 7084
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    invoke-interface {v0}, LFZ;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private y()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 7140
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 7141
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 7142
    if-lez v2, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    move v0, v1

    goto :goto_11
.end method

.method private z()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 5090
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v1

    sub-int v2, v0, v1

    .line 5092
    const/4 v0, 0x1

    if-ge v2, v0, :cond_19

    move v2, v6

    .line 5098
    :cond_19
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:Z

    if-eqz v0, :cond_29

    .line 5099
    const/16 v1, 0x4000

    .line 5102
    :goto_1f
    sget-object v3, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    sget-object v4, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    move-object v0, p0

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    .line 5103
    return-void

    :cond_29
    move v1, v2

    goto :goto_1f
.end method

.method private z()Z
    .registers 8

    .prologue
    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 7150
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->w()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v2

    .line 7205
    :goto_a
    return v0

    .line 7154
    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 7159
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->y()Z

    move-result v0

    goto :goto_a

    .line 7162
    :cond_16
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    and-int/lit8 v0, v0, 0xf

    .line 7163
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    and-int/lit16 v1, v1, 0xff0

    .line 7166
    const/4 v4, 0x2

    if-eq v0, v4, :cond_33

    const/4 v4, 0x3

    if-eq v0, v4, :cond_33

    const/4 v4, 0x4

    if-eq v0, v4, :cond_33

    const/16 v0, 0x10

    if-eq v1, v0, :cond_33

    const/16 v0, 0x20

    if-eq v1, v0, :cond_33

    const/16 v0, 0xb0

    if-ne v1, v0, :cond_38

    .line 7170
    :cond_33
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->y()Z

    move-result v0

    goto :goto_a

    .line 7173
    :cond_38
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()J

    move-result-wide v0

    .line 7174
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(J)I

    move-result v4

    .line 7175
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->b(J)I

    move-result v5

    .line 7177
    if-ltz v4, :cond_48

    if-gez v5, :cond_4a

    :cond_48
    move v0, v2

    .line 7178
    goto :goto_a

    .line 7185
    :cond_4a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {v0, v4, v5, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 7186
    array-length v1, v0

    if-ne v1, v3, :cond_76

    .line 7187
    aget-object v2, v0, v2

    .line 7188
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    invoke-interface {v0, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 7189
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    invoke-interface {v0, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    move v2, v1

    move v1, v0

    .line 7204
    :goto_6d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    move v0, v3

    .line 7205
    goto :goto_a

    .line 7191
    :cond_76
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGw;

    if-nez v0, :cond_81

    .line 7192
    new-instance v0, LGw;

    invoke-direct {v0}, LGw;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGw;

    .line 7195
    :cond_81
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGw;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, LGw;->a(Ljava/lang/CharSequence;)V

    .line 7197
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGw;

    invoke-virtual {v0, v4}, LGw;->c(I)I

    move-result v1

    .line 7198
    if-ne v1, v6, :cond_93

    move v0, v2

    goto/16 :goto_a

    .line 7200
    :cond_93
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGw;

    invoke-virtual {v0, v5}, LGw;->d(I)I

    move-result v0

    .line 7201
    if-ne v0, v6, :cond_9e

    move v0, v2

    goto/16 :goto_a

    :cond_9e
    move v2, v1

    move v1, v0

    goto :goto_6d
.end method


# virtual methods
.method protected a(I)I
    .registers 5
    .parameter

    .prologue
    .line 8770
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 8771
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 8772
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->l()I

    move-result v1

    sub-int/2addr v0, v1

    .line 8773
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v1

    add-int/2addr v0, v1

    .line 8774
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v1

    invoke-interface {v1, v0}, LEj;->a(I)I

    move-result v0

    return v0
.end method

.method public a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 8753
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v0

    if-nez v0, :cond_8

    const/4 v0, -0x1

    .line 8756
    :goto_7
    return v0

    .line 8754
    :cond_8
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(I)I

    move-result v0

    .line 8755
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->b(II)I

    move-result v0

    goto :goto_7
.end method

.method public a(ILandroid/graphics/Rect;)I
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 4258
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v1, :cond_b

    .line 4259
    if-eqz p2, :cond_a

    .line 4260
    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 4273
    :cond_a
    :goto_a
    return v0

    .line 4264
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, p1, p2}, LEj;->a(ILandroid/graphics/Rect;)I

    move-result v1

    .line 4266
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v0

    .line 4267
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v2, v2, 0x70

    const/16 v3, 0x30

    if-eq v2, v3, :cond_23

    .line 4268
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v2

    add-int/2addr v0, v2

    .line 4270
    :cond_23
    if-eqz p2, :cond_2c

    .line 4271
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v2

    invoke-virtual {p2, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 4273
    :cond_2c
    add-int/2addr v0, v1

    goto :goto_a
.end method

.method public a()LDY;
    .registers 2

    .prologue
    .line 7664
    new-instance v0, LFE;

    invoke-direct {v0, p0}, LFE;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/apps/docs/editors/text/TextView;)LER;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 7650
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LEY;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 7651
    new-instance v0, LES;

    invoke-direct {v0, p1, p2}, LES;-><init>(Landroid/content/Context;Lcom/google/android/apps/docs/editors/text/TextView;)V

    .line 7656
    :goto_f
    invoke-interface {v0}, LER;->p()V

    .line 7657
    invoke-interface {v0}, LER;->h()Z

    move-result v1

    if-eqz v1, :cond_1f

    :goto_18
    return-object v0

    .line 7653
    :cond_19
    new-instance v0, LEV;

    invoke-direct {v0, p2}, LEV;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    goto :goto_f

    .line 7657
    :cond_1f
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final a()LEj;
    .registers 2

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    return-object v0
.end method

.method protected a(ILandroid/text/Layout$Alignment;I)LEj;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5960
    new-instance v0, LDV;

    invoke-direct {v0}, LDV;-><init>()V

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/text/TextView;->a(LFg;ILandroid/text/Layout$Alignment;I)LEj;

    move-result-object v0

    return-object v0
.end method

.method public final a(LFg;ILandroid/text/Layout$Alignment;I)LEj;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5966
    new-instance v0, LEa;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    iget v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:F

    iget v8, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:F

    iget-boolean v9, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:Z

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-nez v1, :cond_1d

    iget-object v10, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    :goto_14
    move-object v1, p1

    move v5, p2

    move-object v6, p3

    move/from16 v11, p4

    invoke-direct/range {v0 .. v11}, LEa;-><init>(LFg;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    .line 5971
    return-object v0

    .line 5966
    :cond_1d
    const/4 v10, 0x0

    goto :goto_14
.end method

.method a()LFA;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 8899
    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:Z

    if-nez v1, :cond_6

    .line 8910
    :goto_5
    return-object v0

    .line 8903
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    if-nez v1, :cond_1a

    .line 8904
    new-instance v1, LFA;

    invoke-direct {v1, p0, v0}, LFA;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    .line 8906
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 8907
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 8910
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    goto :goto_5
.end method

.method public a()LFH;
    .registers 3

    .prologue
    .line 8914
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->m:Z

    if-nez v0, :cond_6

    .line 8915
    const/4 v0, 0x0

    .line 8925
    :goto_5
    return-object v0

    .line 8918
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    if-nez v0, :cond_1a

    .line 8919
    new-instance v0, LFH;

    invoke-direct {v0, p0}, LFH;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    .line 8921
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 8922
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 8925
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    goto :goto_5
.end method

.method protected a()LFZ;
    .registers 2

    .prologue
    .line 1085
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()LGE;
    .registers 2

    .prologue
    .line 8949
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGE;

    return-object v0
.end method

.method protected a(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .registers 14
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 2519
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Z

    .line 2523
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_81

    .line 2524
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v6

    .line 2525
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v3

    .line 2526
    if-gez v6, :cond_14

    if-ltz v3, :cond_15

    :cond_14
    move v0, v7

    .line 2532
    :cond_15
    :goto_15
    if-eqz v0, :cond_77

    .line 2533
    new-instance v4, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;

    invoke-direct {v4, p1}, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2535
    iput v6, v4, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:I

    .line 2536
    iput v3, v4, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->b:I

    .line 2538
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_78

    .line 2546
    new-instance v8, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2548
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 2549
    const-class v2, Ljava/lang/Object;

    invoke-interface {v8, v6, v3, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v9

    .line 2550
    :goto_3b
    array-length v2, v9

    if-ge v1, v2, :cond_68

    .line 2551
    instance-of v2, v0, Landroid/text/TextWatcher;

    if-nez v2, :cond_65

    instance-of v2, v0, Landroid/text/SpanWatcher;

    if-nez v2, :cond_65

    .line 2552
    aget-object v2, v9, v1

    invoke-interface {v0, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 2553
    aget-object v2, v9, v1

    invoke-interface {v0, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    .line 2554
    aget-object v10, v9, v1

    invoke-interface {v0, v10}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v10

    .line 2555
    if-ge v5, v6, :cond_5b

    move v5, v6

    .line 2556
    :cond_5b
    if-le v2, v3, :cond_5e

    move v2, v3

    .line 2557
    :cond_5e
    aget-object v11, v9, v1

    sub-int/2addr v5, v6

    sub-int/2addr v2, v6

    invoke-interface {v8, v11, v5, v2, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2550
    :cond_65
    add-int/lit8 v1, v1, 0x1

    goto :goto_3b

    .line 2561
    :cond_68
    iput-object v8, v4, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:Ljava/lang/CharSequence;

    .line 2566
    :goto_6a
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_76

    if-ltz v6, :cond_76

    if-ltz v3, :cond_76

    .line 2567
    iput-boolean v7, v4, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:Z

    :cond_76
    move-object p1, v4

    .line 2573
    :cond_77
    return-object p1

    .line 2563
    :cond_78
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:Ljava/lang/CharSequence;

    goto :goto_6a

    :cond_81
    move v3, v1

    move v6, v1

    goto :goto_15
.end method

.method public a()Landroid/text/TextPaint;
    .registers 2

    .prologue
    .line 1936
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    return-object v0
.end method

.method public final a()Landroid/text/method/KeyListener;
    .registers 2

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    return-object v0
.end method

.method protected a(I)Landroid/util/Pair;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4153
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/view/ActionMode$Callback;
    .registers 2

    .prologue
    .line 7609
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/view/ActionMode$Callback;

    return-object v0
.end method

.method public a()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected a()V
    .registers 1

    .prologue
    .line 6758
    return-void
.end method

.method public a(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 6180
    if-ltz p1, :cond_4

    if-gez p2, :cond_5

    .line 6197
    :cond_4
    :goto_4
    return-void

    .line 6184
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v4

    .line 6185
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6186
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Ljava/lang/CharSequence;

    move-result-object v1

    .line 6187
    if-ne p1, p2, :cond_39

    instance-of v0, v1, Landroid/text/Spannable;

    if-eqz v0, :cond_39

    move-object v0, v1

    .line 6188
    check-cast v0, Landroid/text/Spannable;

    .line 6189
    const-class v2, Landroid/text/style/ReplacementSpan;

    invoke-interface {v0, p1, p2, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/ReplacementSpan;

    .line 6190
    array-length v5, v2

    const/4 v3, 0x0

    :goto_2a
    if-ge v3, v5, :cond_39

    aget-object v6, v2, v3

    .line 6191
    invoke-interface {v0, v6}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 6190
    add-int/lit8 v3, v3, 0x1

    goto :goto_2a

    .line 6194
    :cond_39
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v4, v1, v0, v2}, LEZ;->a(Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;II)V

    goto :goto_4
.end method

.method public a(III)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3438
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v0, :cond_8

    .line 3439
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 3485
    :cond_7
    :goto_7
    return-void

    .line 3441
    :cond_8
    if-gez p1, :cond_e

    if-gez p2, :cond_e

    if-ltz p3, :cond_7

    .line 3442
    :cond_e
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 3443
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 3445
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, v1}, LEj;->g(I)I

    move-result v0

    .line 3446
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v0}, LEj;->b(I)I

    move-result v2

    .line 3454
    if-lez v0, :cond_35

    .line 3455
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, LEj;->c(I)I

    move-result v4

    sub-int/2addr v2, v4

    .line 3460
    :cond_35
    if-ne v1, v3, :cond_69

    .line 3465
    :goto_37
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v0}, LEj;->b(I)I

    move-result v1

    .line 3467
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v3

    .line 3468
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v0

    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v4

    add-int/2addr v4, v0

    .line 3473
    const/4 v0, 0x0

    :goto_4e
    iget v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    if-ge v0, v5, :cond_70

    .line 3474
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    .line 3475
    iget v6, v5, Landroid/graphics/Rect;->top:I

    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 3476
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 3473
    add-int/lit8 v0, v0, 0x1

    goto :goto_4e

    .line 3463
    :cond_69
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, v3}, LEj;->g(I)I

    move-result v0

    goto :goto_37

    .line 3480
    :cond_70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v5

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v5

    add-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v5

    sub-int/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    sub-int/2addr v3, v5

    add-int/2addr v1, v4

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate(IIII)V

    goto/16 :goto_7
.end method

.method protected a(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 5111
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->D()V

    .line 5113
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 5115
    if-gez p1, :cond_9

    .line 5116
    const/4 p1, 0x0

    .line 5118
    :cond_9
    if-gez p2, :cond_b

    .line 5123
    :cond_b
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v0, v0, 0x7

    sparse-switch v0, :sswitch_data_5a

    .line 5135
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 5138
    :goto_14
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-nez v1, :cond_1c

    .line 5140
    :cond_1c
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v1, v1, Landroid/text/Spannable;

    if-eqz v1, :cond_28

    .line 5141
    invoke-virtual {p0, p1, v0, p5}, Lcom/google/android/apps/docs/editors/text/TextView;->a(ILandroid/text/Layout$Alignment;I)LEj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    .line 5144
    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    if-eqz v0, :cond_2c

    .line 5146
    :cond_2c
    if-eqz p6, :cond_31

    .line 5147
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()V

    .line 5150
    :cond_31
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v0, v1, :cond_4d

    .line 5151
    int-to-float v0, p5

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(F)Z

    move-result v0

    if-nez v0, :cond_4d

    .line 5152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 5156
    const/4 v1, -0x2

    if-eq v0, v1, :cond_57

    const/4 v1, -0x1

    if-eq v0, v1, :cond_57

    .line 5157
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->C()V

    .line 5167
    :cond_4d
    :goto_4d
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    .line 5168
    return-void

    .line 5125
    :sswitch_51
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_14

    .line 5131
    :sswitch_54
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_14

    .line 5161
    :cond_57
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->h:Z

    goto :goto_4d

    .line 5123
    :sswitch_data_5a
    .sparse-switch
        0x1 -> :sswitch_51
        0x5 -> :sswitch_54
    .end sparse-switch
.end method

.method a(LFw;)V
    .registers 3
    .parameter

    .prologue
    .line 5021
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->t()V

    .line 5023
    iget-boolean v0, p1, LFw;->c:Z

    if-nez v0, :cond_b

    iget-boolean v0, p1, LFw;->b:Z

    if-eqz v0, :cond_12

    .line 5024
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->r()V

    .line 5025
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->l()Z

    .line 5030
    :cond_11
    :goto_11
    return-void

    .line 5026
    :cond_12
    iget-boolean v0, p1, LFw;->a:Z

    if-eqz v0, :cond_11

    .line 5028
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()V

    goto :goto_11
.end method

.method public a(Landroid/text/Editable;)V
    .registers 6
    .parameter

    .prologue
    .line 6259
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1b

    .line 6260
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    .line 6261
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 6262
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v3, :cond_1b

    .line 6263
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 6262
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 6266
    :cond_1b
    return-void
.end method

.method public a(Landroid/text/Spanned;Ljava/lang/Object;IIII)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x1

    .line 6308
    const/4 v0, 0x0

    .line 6311
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 6313
    sget-object v2, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    if-ne p2, v2, :cond_c5

    .line 6314
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 6318
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_13

    .line 6319
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Z

    .line 6322
    :cond_13
    if-gez p3, :cond_17

    if-ltz p4, :cond_24

    .line 6323
    :cond_17
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-virtual {p0, v0, p3, p4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(III)V

    .line 6324
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()V

    .line 6325
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->F()V

    :cond_24
    move v0, p4

    move v2, v3

    .line 6329
    :goto_26
    sget-object v5, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    if-ne p2, v5, :cond_41

    .line 6330
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 6334
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v1

    if-nez v1, :cond_34

    .line 6335
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Z

    .line 6338
    :cond_34
    if-gez p3, :cond_38

    if-ltz p4, :cond_3f

    .line 6339
    :cond_38
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 6340
    invoke-virtual {p0, v1, p3, p4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(III)V

    :cond_3f
    move v1, p4

    move v2, v3

    .line 6344
    :cond_41
    if-eqz v2, :cond_5a

    .line 6345
    invoke-interface {p1, p2}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v2

    and-int/lit16 v2, v2, 0x200

    if-nez v2, :cond_5a

    .line 6346
    if-gez v1, :cond_51

    .line 6347
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 6349
    :cond_51
    if-gez v0, :cond_57

    .line 6350
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    .line 6352
    :cond_57
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)V

    .line 6356
    :cond_5a
    instance-of v0, p2, Landroid/text/style/UpdateAppearance;

    if-nez v0, :cond_62

    instance-of v0, p2, Landroid/text/style/ParagraphStyle;

    if-eqz v0, :cond_70

    .line 6357
    :cond_62
    if-eqz v4, :cond_68

    iget v0, v4, LFw;->a:I

    if-nez v0, :cond_bc

    .line 6358
    :cond_68
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 6359
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 6360
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->A()V

    .line 6366
    :cond_70
    :goto_70
    invoke-static {p1, p2}, LFY;->a(Ljava/lang/CharSequence;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 6367
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 6368
    if-eqz v4, :cond_82

    invoke-static {p1, p2}, LFY;->b(Ljava/lang/CharSequence;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 6369
    iput-boolean v3, v4, LFw;->b:Z

    .line 6372
    :cond_82
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    if-ltz v0, :cond_91

    .line 6373
    if-eqz v4, :cond_8e

    iget v0, v4, LFw;->a:I

    if-nez v0, :cond_bf

    .line 6374
    :cond_8e
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()V

    .line 6381
    :cond_91
    :goto_91
    instance-of v0, p2, Landroid/text/ParcelableSpan;

    if-eqz v0, :cond_bb

    .line 6384
    if-eqz v4, :cond_bb

    iget-object v0, v4, LFw;->a:Landroid/view/inputmethod/ExtractedTextRequest;

    if-eqz v0, :cond_bb

    .line 6385
    iget v0, v4, LFw;->a:I

    if-eqz v0, :cond_c2

    .line 6386
    if-ltz p3, :cond_ad

    .line 6387
    iget v0, v4, LFw;->b:I

    if-le v0, p3, :cond_a7

    .line 6388
    iput p3, v4, LFw;->b:I

    .line 6390
    :cond_a7
    iget v0, v4, LFw;->b:I

    if-le v0, p5, :cond_ad

    .line 6391
    iput p5, v4, LFw;->b:I

    .line 6394
    :cond_ad
    if-ltz p4, :cond_bb

    .line 6395
    iget v0, v4, LFw;->b:I

    if-le v0, p4, :cond_b5

    .line 6396
    iput p4, v4, LFw;->b:I

    .line 6398
    :cond_b5
    iget v0, v4, LFw;->b:I

    if-le v0, p6, :cond_bb

    .line 6399
    iput p6, v4, LFw;->b:I

    .line 6410
    :cond_bb
    :goto_bb
    return-void

    .line 6362
    :cond_bc
    iput-boolean v3, v4, LFw;->c:Z

    goto :goto_70

    .line 6376
    :cond_bf
    iput-boolean v3, v4, LFw;->a:Z

    goto :goto_91

    .line 6406
    :cond_c2
    iput-boolean v3, v4, LFw;->c:Z

    goto :goto_bb

    :cond_c5
    move v2, v0

    move v0, v1

    goto/16 :goto_26
.end method

.method public a(Landroid/text/TextWatcher;)V
    .registers 3
    .parameter

    .prologue
    .line 6209
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 6210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    .line 6213
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6214
    return-void
.end method

.method public a(Landroid/view/inputmethod/CompletionInfo;)V
    .registers 2
    .parameter

    .prologue
    .line 4862
    return-void
.end method

.method public a(Landroid/view/inputmethod/CorrectionInfo;)V
    .registers 4
    .parameter

    .prologue
    .line 4874
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFq;

    if-nez v0, :cond_11

    .line 4875
    new-instance v0, LFq;

    invoke-direct {v0, p0}, LFq;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFq;

    .line 4880
    :goto_b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFq;

    invoke-virtual {v0, p1}, LFq;->a(Landroid/view/inputmethod/CorrectionInfo;)V

    .line 4881
    return-void

    .line 4877
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFq;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LFq;->a(LFq;Z)V

    goto :goto_b
.end method

.method protected a(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6170
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, -0x1

    .line 1273
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v0, p1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 1274
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v0, p1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    .line 1277
    if-eq v1, v3, :cond_15

    if-ne v2, v3, :cond_16

    .line 1296
    :cond_15
    return-void

    .line 1281
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, v1}, LEj;->g(I)I

    move-result v0

    .line 1282
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, v2}, LEj;->g(I)I

    move-result v1

    .line 1284
    :goto_22
    if-gt v0, v1, :cond_15

    .line 1285
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1286
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v3, v0, v2}, LEj;->a(ILandroid/graphics/Rect;)I

    .line 1288
    iget v3, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 1289
    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 1291
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 1292
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1294
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate(Landroid/graphics/Rect;)V

    .line 1284
    add-int/lit8 v0, v0, 0x1

    goto :goto_22
.end method

.method public a(I)Z
    .registers 4
    .parameter

    .prologue
    .line 5525
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, p1}, LEj;->g(I)I

    move-result v0

    .line 5528
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, p1}, LEj;->a(I)F

    move-result v1

    float-to-int v1, v1

    .line 5529
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)Z

    move-result v0

    return v0
.end method

.method public a(II)Z
    .registers 20
    .parameter
    .parameter

    .prologue
    .line 5538
    const/4 v7, 0x0

    .line 5540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move/from16 v0, p1

    invoke-interface {v2, v0}, LEj;->b(I)I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v3

    add-int v9, v2, v3

    .line 5541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    add-int/lit8 v3, p1, 0x1

    invoke-interface {v2, v3}, LEj;->b(I)I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v3

    add-int v10, v2, v3

    .line 5543
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move/from16 v0, p1

    invoke-interface {v2, v0}, LEj;->c(I)F

    move-result v2

    invoke-static {v2}, Landroid/util/FloatMath;->floor(F)F

    move-result v2

    float-to-int v6, v2

    .line 5544
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move/from16 v0, p1

    invoke-interface {v2, v0}, LEj;->d(I)F

    move-result v2

    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    move-result v2

    float-to-int v11, v2

    .line 5545
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2}, LEj;->c()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v3

    add-int v12, v2, v3

    .line 5549
    sget-object v2, LFm;->a:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move/from16 v0, p1

    invoke-interface {v3, v0}, LEj;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/text/Layout$Alignment;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_232

    .line 5559
    const/4 v2, 0x0

    .line 5562
    :goto_68
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move/from16 v0, p1

    invoke-interface {v3, v0}, LEj;->o(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_77

    .line 5563
    mul-int/lit8 v2, v2, -0x1

    .line 5566
    :cond_77
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v4

    sub-int v13, v3, v4

    .line 5567
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getBottom()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getTop()I

    move-result v4

    sub-int v14, v3, v4

    .line 5569
    sub-int v3, v10, v9

    div-int/lit8 v4, v3, 0x2

    .line 5572
    div-int/lit8 v3, v14, 0x4

    if-le v4, v3, :cond_22e

    div-int/lit8 v3, v14, 0x4

    .line 5573
    :goto_9f
    div-int/lit8 v5, v13, 0x4

    if-le v4, v5, :cond_a5

    div-int/lit8 v4, v13, 0x4

    .line 5575
    :cond_a5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v5

    .line 5576
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v8

    .line 5578
    sub-int v15, v9, v3

    sub-int v15, v8, v15

    .line 5579
    add-int/2addr v3, v10

    add-int v16, v8, v14

    sub-int v16, v3, v16

    .line 5581
    if-lez v16, :cond_22b

    if-gez v15, :cond_22b

    .line 5583
    neg-int v3, v15

    move/from16 v0, v16

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    add-int/2addr v3, v8

    .line 5585
    :goto_c2
    if-lez v15, :cond_ce

    if-gez v16, :cond_ce

    .line 5586
    move/from16 v0, v16

    neg-int v8, v0

    invoke-static {v15, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    sub-int/2addr v3, v8

    .line 5588
    :cond_ce
    sub-int v8, v12, v3

    if-ge v8, v14, :cond_d4

    sub-int v3, v12, v14

    .line 5589
    :cond_d4
    rsub-int/lit8 v8, v3, 0x0

    if-lez v8, :cond_d9

    const/4 v3, 0x0

    .line 5591
    :cond_d9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move/from16 v0, p1

    invoke-interface {v8, v0}, LEj;->f(I)I

    move-result v8

    .line 5592
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v12}, LEj;->a()I

    move-result v12

    .line 5593
    if-eqz v2, :cond_ff

    if-le v12, v13, :cond_ff

    .line 5594
    sub-int v14, p2, v5

    if-ge v14, v4, :cond_f5

    .line 5595
    sub-int v5, p2, v4

    .line 5597
    :cond_f5
    sub-int v14, p2, v5

    sub-int v15, v13, v4

    if-le v14, v15, :cond_ff

    .line 5598
    sub-int v5, v13, v4

    sub-int v5, p2, v5

    .line 5602
    :cond_ff
    if-gez v2, :cond_174

    .line 5603
    if-le v12, v13, :cond_225

    .line 5604
    sub-int v2, v6, v5

    if-lez v2, :cond_228

    move v2, v6

    .line 5605
    :goto_108
    sub-int v4, v11, v2

    if-ge v4, v13, :cond_10e

    sub-int v2, v11, v13

    .line 5653
    :cond_10e
    :goto_10e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v4

    if-ne v2, v4, :cond_11a

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v4

    if-eq v3, v4, :cond_21d

    .line 5654
    :cond_11a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    if-nez v4, :cond_1c4

    .line 5655
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->scrollTo(II)V

    .line 5676
    :goto_125
    const/4 v2, 0x1

    .line 5679
    :goto_126
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_16d

    .line 5684
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    if-nez v3, :cond_13b

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    .line 5685
    :cond_13b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    add-int/lit8 v4, p2, 0x1

    move/from16 v0, p2

    invoke-virtual {v3, v0, v9, v4, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 5686
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v3, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/graphics/Rect;I)V

    .line 5687
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 5689
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->requestRectangleOnScreen(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_16d

    .line 5690
    const/4 v2, 0x1

    .line 5694
    :cond_16d
    return v2

    .line 5551
    :pswitch_16e
    const/4 v2, 0x1

    .line 5552
    goto/16 :goto_68

    .line 5555
    :pswitch_171
    const/4 v2, -0x1

    .line 5556
    goto/16 :goto_68

    .line 5607
    :cond_174
    if-lez v2, :cond_184

    .line 5608
    if-le v12, v13, :cond_225

    .line 5609
    sub-int v2, v11, v5

    if-ge v2, v13, :cond_222

    sub-int v2, v11, v13

    .line 5610
    :goto_17e
    sub-int v4, v6, v2

    if-lez v4, :cond_10e

    move v2, v6

    goto :goto_10e

    .line 5613
    :cond_184
    if-gt v12, v13, :cond_192

    .line 5617
    add-int v2, v11, v6

    div-int/lit8 v2, v2, 0x2

    div-int/lit8 v4, v8, 0x2

    sub-int/2addr v2, v4

    div-int/lit8 v4, v13, 0x2

    sub-int/2addr v2, v4

    goto/16 :goto_10e

    .line 5618
    :cond_192
    sub-int v2, v11, v4

    move/from16 v0, p2

    if-le v0, v2, :cond_19c

    .line 5623
    sub-int v2, v11, v13

    goto/16 :goto_10e

    .line 5624
    :cond_19c
    add-int v2, v6, v4

    move/from16 v0, p2

    if-ge v0, v2, :cond_1a5

    move v2, v6

    .line 5629
    goto/16 :goto_10e

    .line 5630
    :cond_1a5
    if-le v6, v5, :cond_1aa

    move v2, v6

    .line 5634
    goto/16 :goto_10e

    .line 5635
    :cond_1aa
    add-int v2, v5, v13

    if-ge v11, v2, :cond_1b2

    .line 5639
    sub-int v2, v11, v13

    goto/16 :goto_10e

    .line 5644
    :cond_1b2
    sub-int v2, p2, v5

    if-ge v2, v4, :cond_220

    .line 5645
    sub-int v2, p2, v4

    .line 5647
    :goto_1b8
    sub-int v5, p2, v2

    sub-int v6, v13, v4

    if-le v5, v6, :cond_10e

    .line 5648
    sub-int v2, v13, v4

    sub-int v2, p2, v2

    goto/16 :goto_10e

    .line 5657
    :cond_1c4
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:J

    sub-long/2addr v4, v6

    .line 5658
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v6

    sub-int/2addr v2, v6

    .line 5659
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v6

    sub-int/2addr v3, v6

    .line 5661
    const-wide/16 v6, 0xfa

    cmp-long v4, v4, v6

    if-lez v4, :cond_206

    .line 5662
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v6

    invoke-virtual {v4, v5, v6, v2, v3}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 5663
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getDuration()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->awakenScrollBars(I)Z

    .line 5664
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 5673
    :goto_1fc
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:J

    goto/16 :goto_125

    .line 5666
    :cond_206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->isFinished()Z

    move-result v4

    if-nez v4, :cond_217

    .line 5667
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->abortAnimation()V

    .line 5670
    :cond_217
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->scrollBy(II)V

    goto :goto_1fc

    :cond_21d
    move v2, v7

    goto/16 :goto_126

    :cond_220
    move v2, v5

    goto :goto_1b8

    :cond_222
    move v2, v5

    goto/16 :goto_17e

    :cond_225
    move v2, v5

    goto/16 :goto_10e

    :cond_228
    move v2, v5

    goto/16 :goto_108

    :cond_22b
    move v3, v8

    goto/16 :goto_c2

    :cond_22e
    move v3, v4

    goto/16 :goto_9f

    .line 5549
    nop

    :pswitch_data_232
    .packed-switch 0x1
        :pswitch_16e
        :pswitch_171
    .end packed-switch
.end method

.method a(Landroid/view/inputmethod/ExtractedTextRequest;IIILandroid/view/inputmethod/ExtractedText;)Z
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 4678
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    .line 4679
    if-eqz v1, :cond_4c

    .line 4680
    const/4 v0, -0x2

    if-eq p2, v0, :cond_93

    .line 4681
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    .line 4682
    if-gez p2, :cond_4d

    .line 4683
    const/4 v0, -0x1

    iput v0, p5, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    iput v0, p5, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    move v4, v5

    move v0, v6

    .line 4718
    :cond_15
    :goto_15
    iget v2, p1, Landroid/view/inputmethod/ExtractedTextRequest;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8c

    .line 4719
    invoke-interface {v1, v0, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p5, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    .line 4728
    :goto_21
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    .line 4729
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    const/16 v1, 0x800

    invoke-static {v0, v1}, LFY;->a(Ljava/lang/CharSequence;I)I

    move-result v0

    if-eqz v0, :cond_33

    .line 4730
    iget v0, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    .line 4732
    :cond_33
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    if-eqz v0, :cond_3d

    .line 4733
    iget v0, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p5, Landroid/view/inputmethod/ExtractedText;->flags:I

    .line 4735
    :cond_3d
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    .line 4736
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    iput v0, p5, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 4737
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v0

    iput v0, p5, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    .line 4738
    const/4 v6, 0x1

    .line 4740
    :cond_4c
    return v6

    .line 4689
    :cond_4d
    add-int v4, p3, p4

    .line 4691
    instance-of v0, v1, Landroid/text/Spanned;

    if-eqz v0, :cond_76

    move-object v0, v1

    .line 4692
    check-cast v0, Landroid/text/Spanned;

    .line 4693
    const-class v2, Landroid/text/ParcelableSpan;

    invoke-interface {v0, p2, v4, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    .line 4695
    array-length v2, v8

    move v3, p2

    .line 4696
    :goto_5e
    if-lez v2, :cond_77

    .line 4697
    add-int/lit8 v7, v2, -0x1

    .line 4698
    aget-object v2, v8, v7

    invoke-interface {v0, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    .line 4699
    if-ge v2, v3, :cond_a0

    .line 4700
    :goto_6a
    aget-object v3, v8, v7

    invoke-interface {v0, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    .line 4701
    if-le v3, v4, :cond_9e

    :goto_72
    move v4, v3

    move v3, v2

    move v2, v7

    .line 4702
    goto :goto_5e

    :cond_76
    move v3, p2

    .line 4704
    :cond_77
    iput v3, p5, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    .line 4705
    sub-int v0, v4, p4

    iput v0, p5, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    .line 4707
    if-le v3, v5, :cond_84

    move v0, v5

    .line 4712
    :goto_80
    if-le v4, v5, :cond_88

    move v4, v5

    .line 4713
    goto :goto_15

    .line 4709
    :cond_84
    if-gez v3, :cond_9c

    move v0, v6

    .line 4710
    goto :goto_80

    .line 4714
    :cond_88
    if-gez v4, :cond_15

    move v4, v6

    .line 4715
    goto :goto_15

    .line 4721
    :cond_8c
    invoke-static {v1, v0, v4}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p5, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    goto :goto_21

    .line 4724
    :cond_93
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    .line 4725
    iput v6, p5, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    .line 4726
    const-string v0, ""

    iput-object v0, p5, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    goto :goto_21

    :cond_9c
    move v0, v3

    goto :goto_80

    :cond_9e
    move v3, v4

    goto :goto_72

    :cond_a0
    move v2, v3

    goto :goto_6a
.end method

.method public a(Landroid/view/inputmethod/ExtractedTextRequest;Landroid/view/inputmethod/ExtractedText;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 4670
    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v4, v2

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/view/inputmethod/ExtractedTextRequest;IIILandroid/view/inputmethod/ExtractedText;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 5075
    const/4 v0, 0x0

    return v0
.end method

.method protected a(ZI)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3564
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(I)Z

    move-result v0

    return v0
.end method

.method public b()Landroid/text/Editable;
    .registers 2

    .prologue
    .line 1115
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Editable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public b(I)V
    .registers 20
    .parameter

    .prologue
    .line 3145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    .line 3146
    if-eqz v2, :cond_7c

    .line 3147
    iget-object v3, v2, LFv;->a:LFD;

    if-eqz v3, :cond_18

    .line 3148
    iget-object v2, v2, LFv;->a:LFD;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v2, v0, v1, v3}, LFD;->a(Lcom/google/android/apps/docs/editors/text/TextView;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 3200
    :cond_17
    :goto_17
    return-void

    .line 3158
    :cond_18
    const/4 v2, 0x5

    move/from16 v0, p1

    if-ne v0, v2, :cond_37

    .line 3159
    const/16 v2, 0x82

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->focusSearch(I)Landroid/view/View;

    move-result-object v2

    .line 3160
    if-eqz v2, :cond_17

    .line 3161
    const/16 v3, 0x82

    invoke-virtual {v2, v3}, Landroid/view/View;->requestFocus(I)Z

    move-result v2

    if-nez v2, :cond_17

    .line 3162
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "focus search returned a view that wasn\'t able to take focus!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3167
    :cond_37
    invoke-static {}, LEt;->a()Z

    move-result v2

    if-eqz v2, :cond_5c

    const/4 v2, 0x7

    move/from16 v0, p1

    if-ne v0, v2, :cond_5c

    .line 3169
    const/16 v2, 0x21

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->focusSearch(I)Landroid/view/View;

    move-result-object v2

    .line 3170
    if-eqz v2, :cond_17

    .line 3171
    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Landroid/view/View;->requestFocus(I)Z

    move-result v2

    if-nez v2, :cond_17

    .line 3172
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "focus search returned a view that wasn\'t able to take focus!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3178
    :cond_5c
    const/4 v2, 0x6

    move/from16 v0, p1

    if-ne v0, v2, :cond_7c

    .line 3179
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v2}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    .line 3180
    if-eqz v2, :cond_17

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 3181
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_17

    .line 3187
    :cond_7c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHandler()Landroid/os/Handler;

    move-result-object v17

    .line 3188
    if-eqz v17, :cond_17

    .line 3189
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 3190
    const/16 v14, 0x3f3

    new-instance v2, Landroid/view/KeyEvent;

    const/4 v7, 0x0

    const/16 v8, 0x42

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget v11, LEt;->a:I

    const/4 v12, 0x0

    const/16 v13, 0x16

    move-wide v5, v3

    invoke-direct/range {v2 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3194
    const/16 v2, 0x3f3

    new-instance v5, Landroid/view/KeyEvent;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    const/4 v10, 0x1

    const/16 v11, 0x42

    const/4 v12, 0x0

    const/4 v13, 0x0

    sget v14, LEt;->a:I

    const/4 v15, 0x0

    const/16 v16, 0x16

    move-wide v8, v3

    invoke-direct/range {v5 .. v16}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_17
.end method

.method b(Ljava/lang/CharSequence;III)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6245
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1b

    .line 6246
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/util/ArrayList;

    .line 6247
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 6248
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v3, :cond_1b

    .line 6249
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 6248
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 6252
    :cond_1b
    return-void
.end method

.method protected b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 3773
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:Z

    .line 3774
    return-void
.end method

.method public b(I)Z
    .registers 10
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 7336
    .line 7337
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 7339
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_b2

    .line 7340
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    .line 7341
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v2

    .line 7343
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 7344
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v1

    move v1, v0

    .line 7347
    :goto_29
    sparse-switch p1, :sswitch_data_b6

    .line 7401
    :goto_2c
    return v3

    .line 7349
    :sswitch_2d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    const-class v6, Landroid/text/style/URLSpan;

    invoke-interface {v0, v2, v1, v6}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 7350
    array-length v1, v0

    if-lt v1, v5, :cond_6d

    invoke-static {}, LEt;->a()Z

    move-result v1

    if-eqz v1, :cond_6d

    move v2, v3

    move-object v1, v4

    .line 7352
    :goto_44
    array-length v6, v0

    if-ge v2, v6, :cond_63

    .line 7353
    aget-object v6, v0, v3

    invoke-virtual {v6}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 7354
    if-nez v1, :cond_5a

    .line 7355
    invoke-static {v4, v6}, Landroid/content/ClipData;->newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v1

    .line 7352
    :goto_57
    add-int/lit8 v2, v2, 0x1

    goto :goto_44

    .line 7357
    :cond_5a
    new-instance v7, Landroid/content/ClipData$Item;

    invoke-direct {v7, v6}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1, v7}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_57

    .line 7360
    :cond_63
    if-eqz v1, :cond_68

    .line 7361
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/content/ClipData;)V

    .line 7366
    :cond_68
    :goto_68
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    move v3, v5

    .line 7367
    goto :goto_2c

    .line 7364
    :cond_6d
    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;)V

    goto :goto_68

    .line 7370
    :sswitch_77
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    if-eqz v0, :cond_80

    .line 7372
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->z()Z

    :goto_7e
    move v3, v5

    .line 7376
    goto :goto_2c

    .line 7374
    :cond_80
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->A()Z

    goto :goto_7e

    .line 7383
    :sswitch_84
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->y()Z

    move v3, v5

    .line 7384
    goto :goto_2c

    .line 7387
    :sswitch_89
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->c(II)V

    move v3, v5

    .line 7388
    goto :goto_2c

    .line 7391
    :sswitch_8e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;)V

    .line 7392
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v0, v2, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 7393
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    move v3, v5

    .line 7394
    goto :goto_2c

    .line 7397
    :sswitch_a3
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;)V

    .line 7398
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    move v3, v5

    .line 7399
    goto/16 :goto_2c

    :cond_b2
    move v1, v0

    move v2, v3

    goto/16 :goto_29

    .line 7347
    :sswitch_data_b6
    .sparse-switch
        0x102001f -> :sswitch_84
        0x1020020 -> :sswitch_8e
        0x1020021 -> :sswitch_a3
        0x1020022 -> :sswitch_89
        0x1020023 -> :sswitch_2d
        0x102002d -> :sswitch_77
    .end sparse-switch
.end method

.method public c()I
    .registers 3

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:F

    add-float/2addr v0, v1

    invoke-static {v0}, LEh;->a(F)I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/CharSequence;III)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6273
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 6274
    if-eqz v0, :cond_8

    iget v1, v0, LFw;->a:I

    if-nez v1, :cond_b

    .line 6275
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->r()V

    .line 6277
    :cond_b
    if-eqz v0, :cond_21

    .line 6278
    const/4 v1, 0x1

    iput-boolean v1, v0, LFw;->c:Z

    .line 6279
    iget v1, v0, LFw;->b:I

    if-gez v1, :cond_31

    .line 6280
    iput p2, v0, LFw;->b:I

    .line 6281
    add-int v1, p2, p3

    iput v1, v0, LFw;->c:I

    .line 6286
    :goto_1a
    iget v1, v0, LFw;->d:I

    sub-int v2, p4, p3

    add-int/2addr v1, v2

    iput v1, v0, LFw;->d:I

    .line 6289
    :cond_21
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Ljava/lang/CharSequence;III)V

    .line 6290
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;III)V

    .line 6295
    if-eq p3, p4, :cond_30

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Z

    if-eqz v0, :cond_30

    .line 6296
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->H()V

    .line 6298
    :cond_30
    return-void

    .line 6283
    :cond_31
    iget v1, v0, LFw;->b:I

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, LFw;->b:I

    .line 6284
    iget v1, v0, LFw;->c:I

    add-int v2, p2, p3

    iget v3, v0, LFw;->d:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, LFw;->c:I

    goto :goto_1a
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 7548
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->performLongClick()Z

    move-result v0

    return v0
.end method

.method public cancelLongPress()V
    .registers 2

    .prologue
    .line 6836
    invoke-super {p0}, Landroid/view/View;->cancelLongPress()V

    .line 6837
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Z

    .line 6838
    return-void
.end method

.method protected computeHorizontalScrollRange()I
    .registers 3

    .prologue
    .line 6978
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_1f

    .line 6979
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    if-eqz v0, :cond_18

    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v0, v0, 0x7

    const/4 v1, 0x3

    if-ne v0, v1, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LEj;->e(I)F

    move-result v0

    float-to-int v0, v0

    .line 6983
    :goto_17
    return v0

    .line 6979
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->a()I

    move-result v0

    goto :goto_17

    .line 6983
    :cond_1f
    invoke-super {p0}, Landroid/view/View;->computeHorizontalScrollRange()I

    move-result v0

    goto :goto_17
.end method

.method public computeScroll()V
    .registers 3

    .prologue
    .line 5762
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    if-eqz v0, :cond_1e

    .line 5763
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 5764
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->scrollTo(II)V

    .line 5765
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->postInvalidate()V

    .line 5768
    :cond_1e
    return-void
.end method

.method protected computeVerticalScrollExtent()I
    .registers 3

    .prologue
    .line 6997
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method protected computeVerticalScrollRange()I
    .registers 3

    .prologue
    .line 6988
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_15

    .line 6989
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    move-result v1

    add-int/2addr v0, v1

    .line 6991
    :goto_14
    return v0

    :cond_15
    invoke-super {p0}, Landroid/view/View;->computeVerticalScrollRange()I

    move-result v0

    goto :goto_14
.end method

.method public d()I
    .registers 4

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1304
    if-eqz v0, :cond_8

    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_d

    .line 1305
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingTop()I

    move-result v0

    .line 1307
    :goto_c
    return v0

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingTop()I

    move-result v1

    iget v2, v0, LFt;->i:I

    add-int/2addr v1, v2

    iget v0, v0, LFt;->a:I

    add-int/2addr v0, v1

    goto :goto_c
.end method

.method public d()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 7088
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 7096
    :cond_7
    :goto_7
    return v0

    .line 7092
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v1, v1, Landroid/text/Editable;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v1, :cond_7

    .line 7093
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 7702
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isShown()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 7716
    :goto_8
    return v0

    .line 7705
    :cond_9
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_32

    .line 7706
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Ljava/lang/CharSequence;

    move-result-object v0

    .line 7707
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    .line 7708
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/16 v3, 0x1f4

    if-le v2, v3, :cond_29

    .line 7709
    const/16 v2, 0x1f5

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 7711
    :cond_29
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_30
    move v0, v1

    .line 7714
    goto :goto_8

    .line 7716
    :cond_32
    invoke-super {p0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_8
.end method

.method protected drawableStateChanged()V
    .registers 4

    .prologue
    .line 2434
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 2435
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-nez v0, :cond_27

    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-nez v0, :cond_27

    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 2438
    :cond_27
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()V

    .line 2441
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 2442
    if-eqz v0, :cond_76

    .line 2443
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getDrawableState()[I

    move-result-object v1

    .line 2444
    iget-object v2, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_43

    iget-object v2, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_43

    .line 2445
    iget-object v2, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2447
    :cond_43
    iget-object v2, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_54

    iget-object v2, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_54

    .line 2448
    iget-object v2, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2450
    :cond_54
    iget-object v2, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_65

    iget-object v2, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_65

    .line 2451
    iget-object v2, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2453
    :cond_65
    iget-object v2, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_76

    iget-object v2, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_76

    .line 2454
    iget-object v0, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2457
    :cond_76
    return-void
.end method

.method public e()I
    .registers 4

    .prologue
    .line 1316
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1317
    if-eqz v0, :cond_8

    iget-object v1, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_d

    .line 1318
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingBottom()I

    move-result v0

    .line 1320
    :goto_c
    return v0

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingBottom()I

    move-result v1

    iget v2, v0, LFt;->i:I

    add-int/2addr v1, v2

    iget v0, v0, LFt;->b:I

    add-int/2addr v0, v1

    goto :goto_c
.end method

.method public e()Z
    .registers 3

    .prologue
    .line 7112
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Editable;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_2a

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    if-ltz v0, :cond_2a

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v0

    if-ltz v0, :cond_2a

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    invoke-virtual {v0}, Landroid/text/ClipboardManager;->hasText()Z

    move-result v0

    if-eqz v0, :cond_2a

    const/4 v0, 0x1

    :goto_29
    return v0

    :cond_2a
    const/4 v0, 0x0

    goto :goto_29
.end method

.method public f()I
    .registers 4

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1330
    if-eqz v0, :cond_8

    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_d

    .line 1331
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v0

    .line 1333
    :goto_c
    return v0

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v1

    iget v2, v0, LFt;->i:I

    add-int/2addr v1, v2

    iget v0, v0, LFt;->c:I

    add-int/2addr v0, v1

    goto :goto_c
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 7668
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()Z

    move-result v0

    return v0
.end method

.method public g()I
    .registers 4

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1343
    if-eqz v0, :cond_8

    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_d

    .line 1344
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingRight()I

    move-result v0

    .line 1346
    :goto_c
    return v0

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingRight()I

    move-result v1

    iget v2, v0, LFt;->i:I

    add-int/2addr v1, v2

    iget v0, v0, LFt;->d:I

    add-int/2addr v0, v1

    goto :goto_c
.end method

.method public g()V
    .registers 2

    .prologue
    .line 1819
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_9

    .line 1820
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->a()V

    .line 1822
    :cond_9
    return-void
.end method

.method public getBaseline()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 4279
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v0, :cond_a

    .line 4280
    invoke-super {p0}, Landroid/view/View;->getBaseline()I

    move-result v0

    .line 4288
    :goto_9
    return v0

    .line 4284
    :cond_a
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v0, v0, 0x70

    const/16 v2, 0x30

    if-eq v0, v2, :cond_24

    .line 4285
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v0

    .line 4288
    :goto_17
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v1}, LEj;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_9

    :cond_24
    move v0, v1

    goto :goto_17
.end method

.method protected getBottomPaddingOffset()I
    .registers 4

    .prologue
    .line 3625
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:F

    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    add-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .registers 6
    .parameter

    .prologue
    .line 4212
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v0, :cond_8

    .line 4213
    invoke-super {p0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 4237
    :goto_7
    return-void

    .line 4217
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v0

    .line 4218
    if-gez v0, :cond_12

    .line 4219
    invoke-super {p0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    goto :goto_7

    .line 4223
    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, v0}, LEj;->g(I)I

    move-result v1

    .line 4224
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v1}, LEj;->b(I)I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->top:I

    .line 4225
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2, v1}, LEj;->d(I)I

    move-result v1

    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 4227
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, v0}, LEj;->a(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 4228
    iget v0, p1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 4231
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v1

    .line 4232
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v0

    .line 4233
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v2, v2, 0x70

    const/16 v3, 0x30

    if-eq v2, v3, :cond_4d

    .line 4234
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v2

    add-int/2addr v0, v2

    .line 4236
    :cond_4d
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_7
.end method

.method protected getLeftFadingEdgeStrength()F
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 6925
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    int-to-float v1, v1

    const v2, 0x40a95555

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_d

    .line 6947
    :cond_c
    :goto_c
    :pswitch_c
    return v0

    .line 6926
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v1, v2, :cond_3e

    .line 6927
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    if-eqz v1, :cond_30

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v1}, LFB;->d()Z

    move-result v1

    if-nez v1, :cond_30

    .line 6928
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    .line 6929
    invoke-virtual {v1}, LFB;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 6930
    iget v0, v1, LFB;->b:F

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHorizontalFadingEdgeLength()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_c

    .line 6934
    :cond_30
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3e

    .line 6935
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v1, v1, 0x7

    packed-switch v1, :pswitch_data_6e

    .line 6947
    :cond_3e
    :pswitch_3e
    invoke-super {p0}, Landroid/view/View;->getLeftFadingEdgeStrength()F

    move-result v0

    goto :goto_c

    .line 6939
    :pswitch_43
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, v3}, LEj;->d(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, v3}, LEj;->c(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHorizontalFadingEdgeLength()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_c

    .line 6935
    :pswitch_data_6e
    .packed-switch 0x1
        :pswitch_c
        :pswitch_3e
        :pswitch_c
        :pswitch_3e
        :pswitch_43
    .end packed-switch
.end method

.method protected getLeftPaddingOffset()I
    .registers 5

    .prologue
    .line 3614
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:F

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    sub-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected getRightFadingEdgeStrength()F
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 6952
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    int-to-float v1, v1

    const v2, 0x40a95555

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_d

    .line 6973
    :goto_c
    :pswitch_c
    return v0

    .line 6953
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v1, v2, :cond_3c

    .line 6954
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    if-eqz v1, :cond_2e

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v1}, LFB;->d()Z

    move-result v1

    if-nez v1, :cond_2e

    .line 6955
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    .line 6956
    iget v1, v0, LFB;->a:F

    iget v0, v0, LFB;->b:F

    sub-float v0, v1, v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHorizontalFadingEdgeLength()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_c

    .line 6957
    :cond_2e
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3c

    .line 6958
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v1, v1, 0x7

    packed-switch v1, :pswitch_data_86

    .line 6973
    :cond_3c
    :pswitch_3c
    invoke-super {p0}, Landroid/view/View;->getRightFadingEdgeStrength()F

    move-result v0

    goto :goto_c

    .line 6960
    :pswitch_41
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v1

    sub-int/2addr v0, v1

    .line 6962
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, v3}, LEj;->e(I)F

    move-result v1

    .line 6963
    int-to-float v0, v0

    sub-float v0, v1, v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHorizontalFadingEdgeLength()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_c

    .line 6968
    :pswitch_64
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, v3}, LEj;->e(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHorizontalFadingEdgeLength()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_c

    .line 6958
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_64
        :pswitch_3c
        :pswitch_41
        :pswitch_3c
        :pswitch_c
        :pswitch_3c
        :pswitch_64
    .end packed-switch
.end method

.method protected getRightPaddingOffset()I
    .registers 5

    .prologue
    .line 3630
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    neg-int v0, v0

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:F

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected getTopPaddingOffset()I
    .registers 4

    .prologue
    .line 3620
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:F

    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public h()I
    .registers 6

    .prologue
    .line 1356
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    .line 1357
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v0

    .line 1379
    :cond_9
    :goto_9
    return v0

    .line 1360
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->d()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    if-gt v0, v1, :cond_19

    .line 1361
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v0

    goto :goto_9

    .line 1364
    :cond_19
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v0

    .line 1365
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v1

    .line 1366
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v2

    sub-int/2addr v2, v0

    sub-int v1, v2, v1

    .line 1367
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    invoke-interface {v2, v3}, LEj;->b(I)I

    move-result v2

    .line 1369
    if-ge v2, v1, :cond_9

    .line 1373
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v3, v3, 0x70

    .line 1374
    const/16 v4, 0x30

    if-eq v3, v4, :cond_9

    .line 1376
    const/16 v4, 0x50

    if-ne v3, v4, :cond_41

    .line 1377
    add-int/2addr v0, v1

    sub-int/2addr v0, v2

    goto :goto_9

    .line 1379
    :cond_41
    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_9
.end method

.method public h()V
    .registers 2

    .prologue
    .line 2114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Z

    .line 2115
    return-void
.end method

.method public i()I
    .registers 6

    .prologue
    .line 1389
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    .line 1390
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v0

    .line 1412
    :cond_9
    :goto_9
    return v0

    .line 1393
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->d()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    if-gt v0, v1, :cond_19

    .line 1394
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v0

    goto :goto_9

    .line 1397
    :cond_19
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v1

    .line 1398
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v0

    .line 1399
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v2

    sub-int v1, v2, v1

    sub-int/2addr v1, v0

    .line 1400
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    invoke-interface {v2, v3}, LEj;->b(I)I

    move-result v2

    .line 1402
    if-ge v2, v1, :cond_9

    .line 1406
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v3, v3, 0x70

    .line 1407
    const/16 v4, 0x30

    if-ne v3, v4, :cond_3d

    .line 1408
    add-int/2addr v0, v1

    sub-int/2addr v0, v2

    goto :goto_9

    .line 1409
    :cond_3d
    const/16 v4, 0x50

    if-eq v3, v4, :cond_9

    .line 1412
    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_9
.end method

.method public i()V
    .registers 1

    .prologue
    .line 2121
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()V

    .line 2122
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()V

    .line 2123
    return-void
.end method

.method protected i()Z
    .registers 2

    .prologue
    .line 1078
    const/4 v0, 0x0

    return v0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 10
    .parameter

    .prologue
    .line 3646
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 3647
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 3648
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v1

    .line 3649
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v0

    .line 3655
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 3656
    if-eqz v3, :cond_3b

    .line 3657
    iget-object v4, v3, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-ne p1, v4, :cond_4b

    .line 3658
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v4

    .line 3659
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v5

    .line 3660
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getBottom()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getTop()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int v5, v6, v5

    sub-int/2addr v5, v4

    .line 3662
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v6

    add-int/2addr v1, v6

    .line 3663
    iget v3, v3, LFt;->g:I

    sub-int v3, v5, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 3688
    :cond_3b
    :goto_3b
    iget v3, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iget v4, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v0

    iget v5, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v5

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {p0, v3, v4, v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate(IIII)V

    .line 3691
    :cond_4a
    return-void

    .line 3664
    :cond_4b
    iget-object v4, v3, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-ne p1, v4, :cond_7e

    .line 3665
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v4

    .line 3666
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v5

    .line 3667
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getBottom()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getTop()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int v5, v6, v5

    sub-int/2addr v5, v4

    .line 3669
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, v3, LFt;->d:I

    sub-int/2addr v6, v7

    add-int/2addr v1, v6

    .line 3670
    iget v3, v3, LFt;->h:I

    sub-int v3, v5, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 3671
    goto :goto_3b

    :cond_7e
    iget-object v4, v3, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-ne p1, v4, :cond_a4

    .line 3672
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v4

    .line 3673
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    .line 3674
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int v5, v6, v5

    sub-int/2addr v5, v4

    .line 3676
    iget v3, v3, LFt;->e:I

    sub-int v3, v5, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 3677
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    .line 3678
    goto :goto_3b

    :cond_a4
    iget-object v4, v3, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-ne p1, v4, :cond_3b

    .line 3679
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v4

    .line 3680
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    .line 3681
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int v5, v6, v5

    sub-int/2addr v5, v4

    .line 3683
    iget v6, v3, LFt;->f:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 3684
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    iget v3, v3, LFt;->b:I

    sub-int v3, v4, v3

    add-int/2addr v0, v3

    goto/16 :goto_3b
.end method

.method protected isPaddingOffsetRequired()Z
    .registers 3

    .prologue
    .line 3609
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    if-eqz v0, :cond_d

    :cond_b
    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public j()I
    .registers 2

    .prologue
    .line 1421
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v0

    return v0
.end method

.method public j()V
    .registers 8

    .prologue
    const/4 v0, 0x0

    const/high16 v1, 0x3f80

    .line 3391
    iget-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    if-eqz v2, :cond_b

    .line 3392
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()V

    .line 3429
    :cond_a
    :goto_a
    return-void

    .line 3394
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v2

    .line 3395
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v3

    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v4

    add-int/2addr v3, v4

    .line 3397
    iget v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    if-nez v4, :cond_72

    .line 3398
    sget-object v4, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    monitor-enter v4

    .line 3407
    :try_start_20
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getStrokeWidth()F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    .line 3408
    cmpg-float v5, v0, v1

    if-gez v5, :cond_2f

    move v0, v1

    .line 3412
    :cond_2f
    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    .line 3414
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    sget-object v5, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 3416
    int-to-float v1, v2

    sget-object v5, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v5

    sub-float/2addr v1, v0

    invoke-static {v1}, Landroid/util/FloatMath;->floor(F)F

    move-result v1

    float-to-int v1, v1

    int-to-float v5, v3

    sget-object v6, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v6

    sub-float/2addr v5, v0

    invoke-static {v5}, Landroid/util/FloatMath;->floor(F)F

    move-result v5

    float-to-int v5, v5

    int-to-float v2, v2

    sget-object v6, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v6

    add-float/2addr v2, v0

    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    move-result v2

    float-to-int v2, v2

    int-to-float v3, v3

    sget-object v6, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v6

    add-float/2addr v0, v3

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v1, v5, v2, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate(IIII)V

    .line 3420
    monitor-exit v4

    goto :goto_a

    :catchall_6f
    move-exception v0

    monitor-exit v4
    :try_end_71
    .catchall {:try_start_20 .. :try_end_71} :catchall_6f

    throw v0

    .line 3422
    :cond_72
    :goto_72
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    if-ge v0, v1, :cond_a

    .line 3423
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 3424
    iget v4, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v2

    iget v5, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v3

    iget v6, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v2

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v3

    invoke-virtual {p0, v4, v5, v6, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate(IIII)V

    .line 3422
    add-int/lit8 v0, v0, 0x1

    goto :goto_72
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 2176
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:Z

    return v0
.end method

.method public k()I
    .registers 2

    .prologue
    .line 1429
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v0

    return v0
.end method

.method protected k()V
    .registers 2

    .prologue
    .line 3432
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v0

    .line 3434
    invoke-virtual {p0, v0, v0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(III)V

    .line 3435
    return-void
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 4590
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public l()I
    .registers 3

    .prologue
    .line 1438
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public l()V
    .registers 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4157
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->k:I

    if-nez v0, :cond_a

    .line 4158
    iput v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    .line 4185
    :cond_9
    :goto_9
    return-void

    .line 4161
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v2

    .line 4162
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, v2}, LEj;->g(I)I

    .line 4166
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(I)Landroid/util/Pair;

    move-result-object v3

    .line 4167
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v1

    .line 4168
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v3

    .line 4171
    iput v6, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    .line 4174
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    if-ne v0, v7, :cond_48

    .line 4177
    add-int v0, v3, v1

    shr-int/lit8 v0, v0, 0x1

    .line 4180
    :goto_31
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v4, v2}, LEj;->a(I)F

    move-result v4

    invoke-direct {p0, v5, v3, v0, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IIIF)V

    .line 4182
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    if-ne v3, v7, :cond_9

    .line 4183
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v3, v2}, LEj;->b(I)F

    move-result v2

    invoke-direct {p0, v6, v0, v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IIIF)V

    goto :goto_9

    :cond_48
    move v0, v1

    goto :goto_31
.end method

.method l()Z
    .registers 11

    .prologue
    const/4 v9, -0x1

    const/4 v6, 0x0

    .line 4744
    iget-object v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 4745
    if-eqz v7, :cond_4b

    .line 4746
    iget-boolean v0, v7, LFw;->c:Z

    .line 4747
    if-nez v0, :cond_e

    iget-boolean v1, v7, LFw;->b:Z

    if-eqz v1, :cond_4b

    .line 4748
    :cond_e
    iput-boolean v6, v7, LFw;->c:Z

    .line 4749
    iput-boolean v6, v7, LFw;->b:Z

    .line 4750
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    iget-object v1, v1, LFw;->a:Landroid/view/inputmethod/ExtractedTextRequest;

    .line 4751
    if-eqz v1, :cond_4b

    .line 4752
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v2}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    .line 4753
    if-eqz v8, :cond_4b

    .line 4757
    iget v2, v7, LFw;->b:I

    if-gez v2, :cond_29

    if-nez v0, :cond_29

    .line 4758
    const/4 v0, -0x2

    iput v0, v7, LFw;->b:I

    .line 4760
    :cond_29
    iget v2, v7, LFw;->b:I

    iget v3, v7, LFw;->c:I

    iget v4, v7, LFw;->d:I

    iget-object v5, v7, LFw;->a:Landroid/view/inputmethod/ExtractedText;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/view/inputmethod/ExtractedTextRequest;IIILandroid/view/inputmethod/ExtractedText;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 4765
    iget v0, v1, Landroid/view/inputmethod/ExtractedTextRequest;->token:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    iget-object v1, v1, LFw;->a:Landroid/view/inputmethod/ExtractedText;

    invoke-virtual {v8, p0, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->updateExtractedText(Landroid/view/View;ILandroid/view/inputmethod/ExtractedText;)V

    .line 4766
    iput v9, v7, LFw;->b:I

    .line 4767
    iput v9, v7, LFw;->c:I

    .line 4768
    iput v6, v7, LFw;->d:I

    .line 4769
    iput-boolean v6, v7, LFw;->c:Z

    .line 4770
    const/4 v0, 0x1

    .line 4776
    :goto_4a
    return v0

    :cond_4b
    move v0, v6

    goto :goto_4a
.end method

.method public m()I
    .registers 3

    .prologue
    .line 1447
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Z)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public m()V
    .registers 3

    .prologue
    .line 4597
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->k()Z

    move-result v0

    if-nez v0, :cond_7

    .line 4605
    :cond_6
    :goto_6
    return-void

    .line 4600
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 4601
    if-eqz v0, :cond_6

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:Z

    if-eqz v1, :cond_6

    .line 4602
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 4603
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()V

    goto :goto_6
.end method

.method public m()Z
    .registers 3

    .prologue
    .line 5820
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    .line 5821
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v1

    .line 5823
    if-ltz v0, :cond_e

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public n()I
    .registers 2

    .prologue
    .line 4244
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->d()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public n()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 4974
    iput-boolean v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->n:Z

    .line 4975
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 4976
    if-eqz v0, :cond_29

    .line 4977
    iget v1, v0, LFw;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LFw;->a:I

    .line 4978
    if-ne v1, v4, :cond_29

    .line 4979
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->q()V

    .line 4980
    iput-boolean v2, v0, LFw;->a:Z

    .line 4981
    iput v2, v0, LFw;->d:I

    .line 4982
    iget-boolean v1, v0, LFw;->c:Z

    if-eqz v1, :cond_2a

    .line 4985
    iput v2, v0, LFw;->b:I

    .line 4986
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    iput v1, v0, LFw;->c:I

    .line 4992
    :goto_26
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->s()V

    .line 4995
    :cond_29
    return-void

    .line 4988
    :cond_2a
    iput v3, v0, LFw;->b:I

    .line 4989
    iput v3, v0, LFw;->c:I

    .line 4990
    iput-boolean v2, v0, LFw;->c:Z

    goto :goto_26
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 6823
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:Z

    return v0
.end method

.method public o()I
    .registers 2

    .prologue
    .line 5806
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method public o()V
    .registers 3

    .prologue
    .line 4998
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->n:Z

    .line 4999
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 5000
    if-eqz v0, :cond_12

    .line 5001
    iget v1, v0, LFw;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LFw;->a:I

    .line 5002
    if-nez v1, :cond_12

    .line 5003
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(LFw;)V

    .line 5006
    :cond_12
    return-void
.end method

.method public o()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 7100
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 7108
    :cond_7
    :goto_7
    return v0

    .line 7104
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 7105
    const/4 v0, 0x1

    goto :goto_7
.end method

.method protected onAttachedToWindow()V
    .registers 3

    .prologue
    .line 3569
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 3571
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 3574
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    if-eqz v1, :cond_10

    .line 3575
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 3577
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    if-eqz v1, :cond_19

    .line 3578
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 3580
    :cond_19
    return-void
.end method

.method public onCheckIsTextEditor()Z
    .registers 2

    .prologue
    .line 4609
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected onCreateContextMenu(Landroid/view/ContextMenu;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 7250
    invoke-super {p0, p1}, Landroid/view/View;->onCreateContextMenu(Landroid/view/ContextMenu;)V

    .line 7252
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->o:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->p:Z

    if-eqz v0, :cond_1a

    :cond_c
    const/4 v0, 0x1

    :goto_d
    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:Z

    .line 7264
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->p:Z

    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->o:Z

    .line 7266
    new-instance v0, LFC;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LFC;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V

    .line 7299
    return-void

    :cond_1a
    move v0, v1

    .line 7252
    goto :goto_d
.end method

.method protected onCreateDrawableState(I)[I
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 3780
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    if-eqz v0, :cond_29

    .line 3781
    invoke-super {p0, p1}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 3787
    :goto_9
    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-eqz v1, :cond_28

    .line 3794
    array-length v4, v0

    move v2, v3

    .line 3795
    :goto_f
    if-ge v2, v4, :cond_28

    .line 3796
    aget v1, v0, v2

    const v5, 0x10100a7

    if-ne v1, v5, :cond_35

    .line 3797
    add-int/lit8 v1, v4, -0x1

    new-array v1, v1, [I

    .line 3798
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3799
    add-int/lit8 v3, v2, 0x1

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v3, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 3805
    :cond_28
    return-object v0

    .line 3783
    :cond_29
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 3784
    sget-object v1, Lcom/google/android/apps/docs/editors/text/TextView;->b:[I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->mergeDrawableStates([I[I)[I

    goto :goto_9

    .line 3795
    :cond_35
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_f
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .registers 6
    .parameter

    .prologue
    const/high16 v3, 0x4000

    const/high16 v2, 0x800

    .line 4614
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->onCheckIsTextEditor()Z

    move-result v0

    if-eqz v0, :cond_b1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 4615
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    if-nez v0, :cond_1b

    .line 4616
    new-instance v0, LFw;

    invoke-direct {v0, p0}, LFw;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 4618
    :cond_1b
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 4619
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-eqz v0, :cond_a6

    .line 4620
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget v0, v0, LFv;->a:I

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 4621
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v0, v0, LFv;->a:Ljava/lang/String;

    iput-object v0, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    .line 4622
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v0, v0, LFv;->a:Ljava/lang/CharSequence;

    iput-object v0, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    .line 4623
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget v0, v0, LFv;->b:I

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->actionId:I

    .line 4624
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v0, v0, LFv;->a:Landroid/os/Bundle;

    iput-object v0, p1, Landroid/view/inputmethod/EditorInfo;->extras:Landroid/os/Bundle;

    .line 4628
    :goto_41
    const/16 v0, 0x82

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4e

    .line 4629
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/2addr v0, v2

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 4631
    :cond_4e
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5d

    .line 4632
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v1, 0x400

    or-int/2addr v0, v1

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 4634
    :cond_5d
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    and-int/lit16 v0, v0, 0xff

    if-nez v0, :cond_79

    .line 4635
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    and-int/2addr v0, v2

    if-eqz v0, :cond_aa

    .line 4638
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x5

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 4644
    :goto_6e
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Z

    move-result v0

    if-nez v0, :cond_79

    .line 4645
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/2addr v0, v3

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 4648
    :cond_79
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->c(I)Z

    move-result v0

    if-eqz v0, :cond_86

    .line 4650
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/2addr v0, v3

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 4652
    :cond_86
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Editable;

    if-eqz v0, :cond_b1

    .line 4653
    new-instance v0, LEd;

    invoke-direct {v0, p0}, LEd;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    .line 4654
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v1

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    .line 4655
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v1

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    .line 4656
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    invoke-interface {v0, v1}, Landroid/view/inputmethod/InputConnection;->getCursorCapsMode(I)I

    move-result v1

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->initialCapsMode:I

    .line 4660
    :goto_a5
    return-object v0

    .line 4626
    :cond_a6
    const/4 v0, 0x0

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_41

    .line 4642
    :cond_aa
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v0, v0, 0x6

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    goto :goto_6e

    .line 4660
    :cond_b1
    const/4 v0, 0x0

    goto :goto_a5
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    .prologue
    .line 3584
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 3586
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 3587
    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    if-eqz v1, :cond_11

    .line 3588
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 3589
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    .line 3592
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    if-eqz v0, :cond_1c

    .line 3593
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    invoke-virtual {v0, v1}, LFn;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3596
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    if-eqz v0, :cond_25

    .line 3597
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFA;

    invoke-virtual {v0}, LFA;->d()V

    .line 3600
    :cond_25
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    if-eqz v0, :cond_2e

    .line 3601
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    invoke-virtual {v0}, LFH;->d()V

    .line 3604
    :cond_2e
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->H()V

    .line 3605
    return-void
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 8800
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_30

    :pswitch_8
    move v0, v1

    .line 8820
    :goto_9
    return v0

    .line 8802
    :pswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()Z

    move-result v0

    goto :goto_9

    .line 8805
    :pswitch_f
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestFocus()Z

    move v0, v1

    .line 8806
    goto :goto_9

    .line 8809
    :pswitch_14
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v2

    .line 8810
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    move v0, v1

    .line 8811
    goto :goto_9

    .line 8814
    :pswitch_2b
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/view/DragEvent;)V

    move v0, v1

    .line 8815
    goto :goto_9

    .line 8800
    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_a
        :pswitch_14
        :pswitch_2b
        :pswitch_8
        :pswitch_f
    .end packed-switch
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 19
    .parameter

    .prologue
    .line 3810
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_15

    .line 3811
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 3812
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 3813
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    .line 3816
    :cond_15
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    int-to-float v2, v2

    const v3, 0x40a95555

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_22

    .line 4092
    :goto_21
    return-void

    .line 3818
    :cond_22
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()V

    .line 3821
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 3823
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v6

    .line 3824
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v2

    .line 3825
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v3

    .line 3826
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v4

    .line 3827
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollX()I

    move-result v7

    .line 3828
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getScrollY()I

    move-result v8

    .line 3829
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v9

    .line 3830
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v10

    .line 3831
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getBottom()I

    move-result v11

    .line 3832
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getTop()I

    move-result v12

    .line 3836
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 3837
    if-eqz v5, :cond_101

    .line 3843
    sub-int v13, v11, v12

    sub-int v4, v13, v4

    sub-int/2addr v4, v2

    .line 3844
    sub-int v13, v9, v10

    sub-int v3, v13, v3

    sub-int/2addr v3, v6

    .line 3849
    iget-object v13, v5, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v13, :cond_86

    .line 3850
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 3851
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v13

    add-int/2addr v13, v7

    int-to-float v13, v13

    add-int v14, v8, v2

    iget v15, v5, LFt;->g:I

    sub-int v15, v4, v15

    div-int/lit8 v15, v15, 0x2

    add-int/2addr v14, v15

    int-to-float v14, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3853
    iget-object v13, v5, LFt;->c:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3854
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 3860
    :cond_86
    iget-object v13, v5, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v13, :cond_b0

    .line 3861
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 3862
    add-int v13, v7, v9

    sub-int/2addr v13, v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingRight()I

    move-result v14

    sub-int/2addr v13, v14

    iget v14, v5, LFt;->d:I

    sub-int/2addr v13, v14

    int-to-float v13, v13

    add-int/2addr v2, v8

    iget v14, v5, LFt;->h:I

    sub-int/2addr v4, v14

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    int-to-float v2, v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3864
    iget-object v2, v5, LFt;->d:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3865
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 3871
    :cond_b0
    iget-object v2, v5, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_d6

    .line 3872
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 3873
    add-int v2, v7, v6

    iget v4, v5, LFt;->e:I

    sub-int v4, v3, v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingTop()I

    move-result v4

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3875
    iget-object v2, v5, LFt;->a:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3876
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 3882
    :cond_d6
    iget-object v2, v5, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_101

    .line 3883
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 3884
    add-int v2, v7, v6

    iget v4, v5, LFt;->f:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    add-int v3, v8, v11

    sub-int/2addr v3, v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, v5, LFt;->b:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3886
    iget-object v2, v5, LFt;->b:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3887
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 3891
    :cond_101
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:I

    .line 3893
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v2, :cond_10e

    .line 3894
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->z()V

    .line 3897
    :cond_10e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    .line 3900
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v2, v13}, Landroid/text/TextPaint;->setColor(I)V

    .line 3901
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    const/16 v3, 0xff

    if-eq v2, v3, :cond_133

    .line 3903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    invoke-static {v13}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    mul-int/2addr v3, v4

    div-int/lit16 v3, v3, 0xff

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 3905
    :cond_133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getDrawableState()[I

    move-result-object v3

    iput-object v3, v2, Landroid/text/TextPaint;->drawableState:[I

    .line 3907
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 3913
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v15

    .line 3914
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    .line 3916
    int-to-float v5, v7

    .line 3917
    int-to-float v4, v8

    .line 3918
    sub-int v2, v9, v10

    add-int/2addr v2, v7

    int-to-float v3, v2

    .line 3919
    sub-int v2, v11, v12

    add-int/2addr v2, v8

    int-to-float v2, v2

    .line 3921
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_196

    .line 3922
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    sub-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    add-float/2addr v5, v7

    .line 3923
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->b:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    add-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    add-float/2addr v3, v7

    .line 3925
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->c:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    sub-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    add-float/2addr v4, v7

    .line 3926
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->c:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    add-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    add-float/2addr v2, v7

    .line 3929
    :cond_196
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4, v3, v2}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 3931
    const/4 v3, 0x0

    .line 3932
    const/4 v2, 0x0

    .line 3937
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v4, v4, 0x70

    const/16 v5, 0x30

    if-eq v4, v5, :cond_1b5

    .line 3938
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v3

    .line 3939
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)I

    move-result v2

    .line 3941
    :cond_1b5
    int-to-float v4, v6

    add-int v5, v15, v3

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3944
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v4, v5, :cond_223

    .line 3945
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    if-nez v4, :cond_206

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_206

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->t()Z

    move-result v4

    if-eqz v4, :cond_206

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v4, v4, 0x7

    const/4 v5, 0x3

    if-eq v4, v5, :cond_206

    .line 3947
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LEj;->d(I)F

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3953
    :cond_206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    if-eqz v4, :cond_223

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v4}, LFB;->c()Z

    move-result v4

    if-eqz v4, :cond_223

    .line 3954
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    iget v4, v4, LFB;->b:F

    neg-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3958
    :cond_223
    const/4 v7, 0x0

    .line 3959
    const/4 v6, -0x1

    const/4 v5, -0x1

    .line 3960
    const/4 v4, 0x0

    .line 3967
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v8, :cond_442

    .line 3968
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v6

    .line 3969
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v5

    .line 3971
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->s()Z

    move-result v8

    if-nez v8, :cond_241

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-eqz v8, :cond_442

    :cond_241
    if-ltz v6, :cond_442

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_442

    .line 3972
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    if-nez v8, :cond_258

    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    .line 3974
    :cond_258
    if-ne v6, v5, :cond_3ea

    .line 3975
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-nez v8, :cond_442

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    rem-long/2addr v8, v10

    const-wide/16 v10, 0x1f4

    cmp-long v8, v8, v10

    if-gez v8, :cond_442

    .line 3977
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    if-eqz v4, :cond_296

    .line 3978
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 3979
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v4, v6, v7, v8}, LEj;->a(ILandroid/graphics/Path;Ljava/lang/CharSequence;)V

    .line 3980
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->l()V

    .line 3981
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 3985
    :cond_296
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 3986
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    const/16 v7, 0xff

    if-eq v4, v7, :cond_2b7

    .line 3987
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    invoke-static {v13}, Landroid/graphics/Color;->alpha(I)I

    move-result v8

    mul-int/2addr v7, v8

    div-int/lit16 v7, v7, 0xff

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 3989
    :cond_2b7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3990
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    .line 3991
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/docs/editors/text/TextView;->l:I

    if-lez v4, :cond_3e7

    const/4 v4, 0x1

    :goto_2cb
    move v8, v4

    move-object v9, v7

    move v4, v6

    .line 4020
    :goto_2ce
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 4021
    sub-int v11, v2, v3

    .line 4022
    if-eqz v10, :cond_392

    iget v2, v10, LFw;->a:I

    if-nez v2, :cond_392

    .line 4023
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v2}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    .line 4024
    if-eqz v2, :cond_392

    .line 4025
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_31a

    .line 4026
    const/4 v3, 0x0

    .line 4027
    iget-boolean v6, v10, LFw;->c:Z

    if-nez v6, :cond_2f5

    iget-boolean v6, v10, LFw;->b:Z

    if-eqz v6, :cond_2f9

    .line 4031
    :cond_2f5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/text/TextView;->l()Z

    move-result v3

    .line 4033
    :cond_2f9
    if-nez v3, :cond_31a

    if-eqz v9, :cond_31a

    .line 4034
    const/4 v6, -0x1

    .line 4035
    const/4 v7, -0x1

    .line 4036
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v3, v3, Landroid/text/Spannable;

    if-eqz v3, :cond_315

    .line 4037
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v3, Landroid/text/Spannable;

    .line 4038
    invoke-static {v3}, LEd;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v6

    .line 4039
    invoke-static {v3}, LEd;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v7

    :cond_315
    move-object/from16 v3, p0

    .line 4041
    invoke-virtual/range {v2 .. v7}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    .line 4045
    :cond_31a
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/inputmethod/InputMethodManager;->isWatchingCursor(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_392

    if-eqz v9, :cond_392

    .line 4046
    iget-object v3, v10, LFw;->a:Landroid/graphics/RectF;

    const/4 v4, 0x1

    invoke-virtual {v9, v3, v4}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 4047
    iget-object v3, v10, LFw;->a:[F

    const/4 v4, 0x0

    iget-object v5, v10, LFw;->a:[F

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    aput v7, v3, v4

    .line 4049
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    iget-object v4, v10, LFw;->a:[F

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 4050
    iget-object v3, v10, LFw;->a:Landroid/graphics/RectF;

    iget-object v4, v10, LFw;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    iget-object v5, v10, LFw;->a:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 4052
    iget-object v3, v10, LFw;->a:Landroid/graphics/RectF;

    const/4 v4, 0x0

    int-to-float v5, v11

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 4054
    iget-object v3, v10, LFw;->a:Landroid/graphics/Rect;

    iget-object v4, v10, LFw;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    float-to-double v4, v4

    const-wide/high16 v6, 0x3fe0

    add-double/2addr v4, v6

    double-to-int v4, v4

    iget-object v5, v10, LFw;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    float-to-double v5, v5

    const-wide/high16 v12, 0x3fe0

    add-double/2addr v5, v12

    double-to-int v5, v5

    iget-object v6, v10, LFw;->a:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    float-to-double v6, v6

    const-wide/high16 v12, 0x3fe0

    add-double/2addr v6, v12

    double-to-int v6, v6

    iget-object v7, v10, LFw;->a:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    float-to-double v12, v7

    const-wide/high16 v15, 0x3fe0

    add-double/2addr v12, v15

    double-to-int v7, v12

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 4058
    iget-object v3, v10, LFw;->a:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    iget-object v3, v10, LFw;->a:Landroid/graphics/Rect;

    iget v5, v3, Landroid/graphics/Rect;->top:I

    iget-object v3, v10, LFw;->a:Landroid/graphics/Rect;

    iget v6, v3, Landroid/graphics/Rect;->right:I

    iget-object v3, v10, LFw;->a:Landroid/graphics/Rect;

    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v7}, Landroid/view/inputmethod/InputMethodManager;->updateCursor(Landroid/view/View;IIII)V

    .line 4064
    :cond_392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFq;

    if-eqz v2, :cond_3a1

    .line 4065
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFq;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v11}, LFq;->a(Landroid/graphics/Canvas;I)V

    .line 4068
    :cond_3a1
    if-eqz v8, :cond_3a4

    .line 4069
    const/4 v9, 0x0

    .line 4072
    :cond_3a4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-interface {v14, v0, v9, v2, v11}, LEj;->a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 4074
    if-eqz v8, :cond_3b6

    .line 4075
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/graphics/Canvas;I)V

    .line 4078
    :cond_3b6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    if-eqz v2, :cond_3df

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v2}, LFB;->b()Z

    move-result v2

    if-eqz v2, :cond_3df

    .line 4079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFB;

    invoke-virtual {v2}, LFB;->a()F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 4080
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-interface {v14, v0, v9, v2, v11}, LEj;->a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 4089
    :cond_3df
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 4091
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/graphics/Canvas;)V

    goto/16 :goto_21

    .line 3991
    :cond_3e7
    const/4 v4, 0x0

    goto/16 :goto_2cb

    .line 3994
    :cond_3ea
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    if-eqz v7, :cond_407

    .line 3995
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->reset()V

    .line 3996
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    invoke-interface {v7, v6, v5, v8}, LEj;->a(IILandroid/graphics/Path;)V

    .line 3997
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 4001
    :cond_407
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->r:I

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 4002
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    const/16 v8, 0xff

    if-eq v7, v8, :cond_430

    .line 4003
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/docs/editors/text/TextView;->r:I

    invoke-static {v9}, Landroid/graphics/Color;->alpha(I)I

    move-result v9

    mul-int/2addr v8, v9

    div-int/lit16 v8, v8, 0xff

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 4005
    :cond_430
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Paint;

    sget-object v8, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 4007
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Path;

    move v8, v4

    move-object v9, v7

    move v4, v6

    goto/16 :goto_2ce

    :cond_442
    move v8, v4

    move-object v9, v7

    move v4, v6

    goto/16 :goto_2ce
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6485
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:J

    .line 6487
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()V

    .line 6489
    if-eqz p1, :cond_9b

    .line 6490
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v3

    .line 6491
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v4

    .line 6496
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:Z

    if-eqz v0, :cond_97

    if-nez v3, :cond_97

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ne v4, v0, :cond_97

    move v0, v1

    .line 6498
    :goto_24
    iget-boolean v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Z

    if-eqz v5, :cond_99

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()Z

    move-result v5

    if-eqz v5, :cond_99

    if-nez v0, :cond_99

    move v0, v1

    :goto_31
    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->i:Z

    .line 6500
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Z

    if-eqz v0, :cond_3b

    if-ltz v3, :cond_3b

    if-gez v4, :cond_6d

    .line 6504
    :cond_3b
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->r()I

    move-result v5

    .line 6505
    if-ltz v5, :cond_48

    .line 6506
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 6509
    :cond_48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_55

    .line 6510
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v5, p0, v0, p2}, LFZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;I)V

    .line 6522
    :cond_55
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Z

    if-eqz v0, :cond_64

    if-ltz v3, :cond_64

    if-ltz v4, :cond_64

    .line 6531
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v3, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 6534
    :cond_64
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:Z

    if-eqz v0, :cond_6b

    .line 6535
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->y()Z

    .line 6538
    :cond_6b
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:Z

    .line 6541
    :cond_6d
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Z

    .line 6542
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Z

    .line 6544
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_7e

    .line 6545
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    .line 6546
    invoke-static {v0}, LFY;->a(Landroid/text/Spannable;)V

    .line 6549
    :cond_7e
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->F()V

    .line 6562
    :cond_81
    :goto_81
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->c(Z)V

    .line 6564
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    if-eqz v0, :cond_93

    .line 6565
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    move-object v1, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, LGv;->a(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V

    .line 6568
    :cond_93
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 6569
    return-void

    :cond_97
    move v0, v2

    .line 6496
    goto :goto_24

    :cond_99
    move v0, v2

    .line 6498
    goto :goto_31

    .line 6552
    :cond_9b
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->t()V

    .line 6554
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->H()V

    .line 6557
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    if-eqz v0, :cond_81

    .line 6558
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    invoke-virtual {v0}, LFH;->c()V

    goto :goto_81
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    .line 6762
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_1d

    .line 6764
    :try_start_e
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v1, p0, v0, p1}, LFZ;->d(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    :try_end_17
    .catch Ljava/lang/AbstractMethodError; {:try_start_e .. :try_end_17} :catch_1c

    move-result v0

    if-eqz v0, :cond_1d

    .line 6765
    const/4 v0, 0x1

    .line 6774
    :goto_1b
    return v0

    .line 6767
    :catch_1c
    move-exception v0

    .line 6774
    :cond_1d
    invoke-super {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1b
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4293
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(ILandroid/view/KeyEvent;Landroid/view/KeyEvent;)I

    move-result v0

    .line 4294
    if-nez v0, :cond_c

    .line 4296
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 4299
    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x1

    goto :goto_b
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 4304
    const/4 v0, 0x0

    invoke-static {p3, v0}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v3

    .line 4306
    invoke-direct {p0, p1, v3, p3}, Lcom/google/android/apps/docs/editors/text/TextView;->a(ILandroid/view/KeyEvent;Landroid/view/KeyEvent;)I

    move-result v0

    .line 4307
    if-nez v0, :cond_11

    .line 4309
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    .line 4338
    :goto_10
    return v0

    .line 4311
    :cond_11
    const/4 v2, -0x1

    if-ne v0, v2, :cond_16

    move v0, v1

    .line 4313
    goto :goto_10

    .line 4316
    :cond_16
    add-int/lit8 v2, p2, -0x1

    .line 4323
    invoke-static {p3, v1}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v4

    .line 4324
    if-ne v0, v1, :cond_3e

    .line 4325
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v5, p0, v0, p1, v4}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    .line 4326
    :goto_27
    add-int/lit8 v2, v2, -0x1

    if-lez v2, :cond_63

    .line 4327
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v5, p0, v0, p1, v3}, Landroid/text/method/KeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    .line 4328
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v5, p0, v0, p1, v4}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    goto :goto_27

    .line 4330
    :cond_3e
    const/4 v5, 0x2

    if-ne v0, v5, :cond_63

    .line 4331
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v5, p0, v0, p1, v4}, LFZ;->b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    move v0, v2

    .line 4332
    :goto_4b
    add-int/lit8 v2, v0, -0x1

    if-lez v2, :cond_63

    .line 4333
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v5, p0, v0, p1, v3}, LFZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    .line 4334
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v5, p0, v0, p1, v4}, LFZ;->b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    move v0, v2

    goto :goto_4b

    :cond_63
    move v0, v1

    .line 4338
    goto :goto_10
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 7034
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v0

    and-int/lit16 v0, v0, -0x7001

    .line 7035
    invoke-static {v0}, LFW;->a(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 7036
    sparse-switch p1, :sswitch_data_4c

    .line 7059
    :cond_f
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_13
    return v0

    .line 7038
    :sswitch_14
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->w()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 7039
    const v0, 0x102001f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)Z

    move-result v0

    goto :goto_13

    .line 7043
    :sswitch_22
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 7044
    const v0, 0x1020020

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)Z

    move-result v0

    goto :goto_13

    .line 7048
    :sswitch_30
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 7049
    const v0, 0x1020021

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)Z

    move-result v0

    goto :goto_13

    .line 7053
    :sswitch_3e
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 7054
    const v0, 0x1020022

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(I)Z

    move-result v0

    goto :goto_13

    .line 7036
    :sswitch_data_4c
    .sparse-switch
        0x1d -> :sswitch_14
        0x1f -> :sswitch_30
        0x32 -> :sswitch_3e
        0x34 -> :sswitch_22
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x82

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4501
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_f

    .line 4502
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 4583
    :goto_e
    return v0

    .line 4505
    :cond_f
    sparse-switch p1, :sswitch_data_e0

    .line 4578
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_c3

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-interface {v2, p0, v0, p1, p2}, Landroid/text/method/KeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_c3

    move v0, v1

    goto :goto_e

    .line 4507
    :sswitch_24
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->o:Z

    .line 4508
    invoke-static {p2}, LFW;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 4517
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_47

    .line 4518
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Editable;

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_47

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->onCheckIsTextEditor()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 4520
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()V

    .line 4524
    :cond_47
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_e

    .line 4527
    :sswitch_4c
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->p:Z

    .line 4528
    invoke-static {p2}, LFW;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 4529
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-eqz v0, :cond_74

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v0, v0, LFv;->a:LFD;

    if-eqz v0, :cond_74

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-boolean v0, v0, LFv;->a:Z

    if-eqz v0, :cond_74

    .line 4531
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iput-boolean v2, v0, LFv;->a:Z

    .line 4532
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v0, v0, LFv;->a:LFD;

    invoke-interface {v0, p0, v2, p2}, LFD;->a(Lcom/google/android/apps/docs/editors/text/TextView;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_74

    move v0, v1

    .line 4534
    goto :goto_e

    .line 4538
    :cond_74
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_82

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Z

    move-result v0

    if-eqz v0, :cond_bd

    .line 4548
    :cond_82
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_bd

    .line 4549
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 4551
    if-eqz v0, :cond_a0

    .line 4552
    invoke-virtual {v0, v3}, Landroid/view/View;->requestFocus(I)Z

    move-result v0

    if-nez v0, :cond_9a

    .line 4553
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "focus search returned a view that wasn\'t able to take focus!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4561
    :cond_9a
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move v0, v1

    .line 4562
    goto/16 :goto_e

    .line 4563
    :cond_a0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_bd

    .line 4566
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 4567
    if-eqz v0, :cond_bd

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_bd

    .line 4568
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 4573
    :cond_bd
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_e

    .line 4580
    :cond_c3
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_da

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_da

    .line 4581
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v2, p0, v0, p1, p2}, LFZ;->b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_da

    move v0, v1

    goto/16 :goto_e

    .line 4583
    :cond_da
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_e

    .line 4505
    :sswitch_data_e0
    .sparse-switch
        0x17 -> :sswitch_24
        0x42 -> :sswitch_4c
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 5233
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 5234
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 5235
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 5236
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 5241
    sget-object v3, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    .line 5242
    sget-object v4, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    .line 5244
    const/4 v0, -0x1

    .line 5245
    const/4 v1, 0x0

    .line 5247
    const/high16 v2, 0x4000

    if-ne v6, v2, :cond_8e

    move v2, v0

    move v7, v5

    move v0, v1

    .line 5302
    :goto_1d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v1

    sub-int v1, v7, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    sub-int v8, v1, v5

    .line 5305
    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:Z

    if-eqz v1, :cond_17a

    const/16 v1, 0x4000

    .line 5307
    :goto_2f
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v5, :cond_127

    .line 5308
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v0

    sub-int v0, v7, v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    sub-int v5, v0, v5

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    .line 5323
    :cond_45
    :goto_45
    const/high16 v0, 0x4000

    if-ne v10, v0, :cond_163

    .line 5326
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->C:I

    move v0, v9

    .line 5338
    :cond_4d
    :goto_4d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->d()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()I

    move-result v2

    sub-int/2addr v1, v2

    .line 5339
    iget v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_73

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2}, LEj;->d()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    if-le v2, v3, :cond_73

    .line 5340
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    invoke-interface {v2, v3}, LEj;->b(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 5347
    :cond_73
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-nez v2, :cond_87

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2}, LEj;->a()I

    move-result v2

    if-gt v2, v8, :cond_87

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v2}, LEj;->c()I

    move-result v2

    if-le v2, v1, :cond_173

    .line 5349
    :cond_87
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()V

    .line 5354
    :goto_8a
    invoke-virtual {p0, v7, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setMeasuredDimension(II)V

    .line 5355
    return-void

    .line 5251
    :cond_8e
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v2, :cond_9c

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    if-nez v2, :cond_9c

    .line 5252
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(LEj;)I

    move-result v0

    .line 5255
    :cond_9c
    if-gez v0, :cond_113

    .line 5256
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    iget-object v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/text/BoringLayout$Metrics;

    invoke-static {v2, v3, v7}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    move-result-object v3

    .line 5257
    if-eqz v3, :cond_183

    .line 5258
    iput-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/text/BoringLayout$Metrics;

    move v2, v1

    .line 5264
    :goto_ad
    if-eqz v3, :cond_b3

    sget-object v1, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    if-ne v3, v1, :cond_116

    .line 5265
    :cond_b3
    if-gez v0, :cond_c2

    .line 5266
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-static {v0, v1}, LDO;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    :cond_c2
    move v1, v0

    .line 5274
    :goto_c3
    iget-object v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 5275
    if-eqz v7, :cond_d3

    .line 5276
    iget v8, v7, LFt;->e:I

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 5277
    iget v7, v7, LFt;->f:I

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 5280
    :cond_d3
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v1, v7

    .line 5282
    iget v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->z:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_119

    .line 5283
    iget v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()I

    move-result v8

    mul-int/2addr v7, v8

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 5288
    :goto_ed
    iget v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->B:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_120

    .line 5289
    iget v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->A:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()I

    move-result v8

    mul-int/2addr v7, v8

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 5295
    :goto_fd
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getSuggestedMinimumWidth()I

    move-result v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 5297
    const/high16 v7, -0x8000

    if-ne v6, v7, :cond_17d

    .line 5298
    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v7, v1

    move v12, v0

    move v0, v2

    move v2, v12

    goto/16 :goto_1d

    .line 5261
    :cond_113
    const/4 v1, 0x1

    move v2, v1

    goto :goto_ad

    .line 5271
    :cond_116
    iget v1, v3, Landroid/text/BoringLayout$Metrics;->width:I

    goto :goto_c3

    .line 5285
    :cond_119
    iget v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:I

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_ed

    .line 5291
    :cond_120
    iget v7, p0, Lcom/google/android/apps/docs/editors/text/TextView;->A:I

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_fd

    .line 5310
    :cond_127
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v5}, LEj;->a()I

    move-result v5

    if-ne v5, v1, :cond_142

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v5}, LEj;->b()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v6

    sub-int v6, v7, v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v11

    sub-int/2addr v6, v11

    if-eq v5, v6, :cond_45

    .line 5313
    :cond_142
    if-eqz v0, :cond_14f

    if-ltz v2, :cond_14f

    if-gt v2, v1, :cond_14f

    .line 5314
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, v1}, LEj;->a(I)V

    goto/16 :goto_45

    .line 5316
    :cond_14f
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v0

    sub-int v0, v7, v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    sub-int v5, v0, v5

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    goto/16 :goto_45

    .line 5328
    :cond_163
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()I

    move-result v0

    .line 5331
    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->C:I

    .line 5333
    const/high16 v1, -0x8000

    if-ne v10, v1, :cond_4d

    .line 5334
    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/16 :goto_4d

    .line 5351
    :cond_173
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->scrollTo(II)V

    goto/16 :goto_8a

    :cond_17a
    move v1, v8

    goto/16 :goto_2f

    :cond_17d
    move v7, v1

    move v12, v0

    move v0, v2

    move v2, v12

    goto/16 :goto_1d

    :cond_183
    move v2, v1

    goto/16 :goto_ad
.end method

.method public onPreDraw()Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3502
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    if-eq v0, v2, :cond_7

    .line 3552
    :goto_6
    return v2

    .line 3506
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v0, :cond_e

    .line 3507
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->z()V

    .line 3512
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_51

    .line 3518
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v0

    .line 3520
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    if-eqz v3, :cond_26

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFH;

    invoke-virtual {v3}, LFH;->a()Z

    move-result v3

    if-eqz v3, :cond_26

    .line 3522
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    .line 3530
    :cond_26
    if-gez v0, :cond_36

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v3, v3, 0x70

    const/16 v4, 0x50

    if-ne v3, v4, :cond_36

    .line 3531
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 3534
    :cond_36
    if-ltz v0, :cond_56

    .line 3535
    iget-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Z

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(ZI)Z

    move-result v0

    .line 3536
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Z

    .line 3546
    :goto_40
    iget-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->i:Z

    if-eqz v3, :cond_49

    .line 3547
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->A()Z

    .line 3548
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->i:Z

    .line 3551
    :cond_49
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:I

    .line 3552
    if-nez v0, :cond_4f

    move v1, v2

    :cond_4f
    move v2, v1

    goto :goto_6

    .line 3539
    :cond_51
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()Z

    move-result v0

    goto :goto_40

    :cond_56
    move v0, v1

    goto :goto_40
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 6
    .parameter

    .prologue
    .line 2584
    instance-of v0, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;

    if-nez v0, :cond_8

    .line 2585
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2619
    :cond_7
    :goto_7
    return-void

    .line 2589
    :cond_8
    check-cast p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;

    .line 2590
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2593
    iget-object v0, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1a

    .line 2594
    iget-object v0, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2597
    :cond_1a
    iget v0, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:I

    if-ltz v0, :cond_7

    iget v0, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->b:I

    if-ltz v0, :cond_7

    .line 2598
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_7

    .line 2599
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 2601
    iget v1, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:I

    if-gt v1, v0, :cond_36

    iget v1, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->b:I

    if-le v1, v0, :cond_7b

    .line 2602
    :cond_36
    const-string v0, ""

    .line 2604
    iget-object v1, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:Ljava/lang/CharSequence;

    if-eqz v1, :cond_3e

    .line 2605
    const-string v0, "(restored) "

    .line 2608
    :cond_3e
    const-string v1, "TextView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saved cursor position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " out of range for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "text "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 2611
    :cond_7b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    iget v1, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:I

    iget v2, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->b:I

    invoke-static {v0, v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 2613
    iget-boolean v0, p1, Lcom/google/android/apps/docs/editors/text/TextView$SavedState;->a:Z

    if-eqz v0, :cond_7

    .line 2614
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Z

    goto/16 :goto_7
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 2

    .prologue
    .line 2578
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2579
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/os/Parcelable;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method protected onSetAlpha(I)Z
    .registers 4
    .parameter

    .prologue
    .line 3698
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_42

    .line 3699
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    .line 3700
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 3701
    if-eqz v0, :cond_40

    .line 3702
    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_19

    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 3703
    :cond_19
    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_26

    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 3704
    :cond_26
    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_33

    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 3705
    :cond_33
    iget-object v1, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_40

    iget-object v0, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 3707
    :cond_40
    const/4 v0, 0x1

    .line 3711
    :goto_41
    return v0

    .line 3710
    :cond_42
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:I

    .line 3711
    const/4 v0, 0x0

    goto :goto_41
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 6662
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 6664
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 6665
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFA;

    move-result-object v1

    invoke-virtual {v1, p1}, LFA;->a(Landroid/view/MotionEvent;)Z

    .line 6667
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->q()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 6668
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFH;

    move-result-object v1

    invoke-virtual {v1, p1}, LFH;->a(Landroid/view/MotionEvent;)Z

    .line 6671
    :cond_20
    if-nez v0, :cond_50

    .line 6672
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->m:I

    .line 6673
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->n:I

    .line 6677
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:Z

    .line 6678
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Z

    .line 6680
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->a(FF)LGE;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGE;

    .line 6685
    :cond_42
    :goto_42
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 6691
    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Z

    if-eqz v1, :cond_59

    if-ne v0, v2, :cond_59

    .line 6692
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Z

    move v2, v5

    .line 6752
    :cond_4f
    :goto_4f
    return v2

    .line 6681
    :cond_50
    if-eq v0, v2, :cond_55

    const/4 v1, 0x3

    if-ne v0, v1, :cond_42

    .line 6682
    :cond_55
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGE;

    goto :goto_42

    .line 6696
    :cond_59
    if-ne v0, v2, :cond_101

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Z

    if-nez v0, :cond_101

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_101

    move v1, v2

    .line 6699
    :goto_66
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-nez v0, :cond_70

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->onCheckIsTextEditor()Z

    move-result v0

    if-eqz v0, :cond_fe

    :cond_70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_fe

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_fe

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_fe

    .line 6703
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_128

    .line 6704
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v4, p0, v0, p1}, LFZ;->b(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    or-int v4, v3, v0

    .line 6707
    :goto_90
    if-eqz v1, :cond_126

    .line 6712
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v7

    const-class v8, LGx;

    invoke-interface {v0, v6, v7, v8}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGx;

    .line 6715
    array-length v6, v0

    if-eqz v6, :cond_126

    .line 6716
    aget-object v0, v0, v3

    .line 6719
    instance-of v6, v0, Landroid/text/style/ClickableSpan;

    if-eqz v6, :cond_bb

    iget-boolean v6, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:Z

    if-eqz v6, :cond_126

    iget v6, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:I

    if-eqz v6, :cond_126

    iget-boolean v6, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-eqz v6, :cond_126

    .line 6721
    :cond_bb
    invoke-interface {v0, p0}, LGx;->a(Landroid/view/View;)V

    move v0, v2

    .line 6727
    :goto_bf
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->u()Z

    move-result v4

    if-nez v4, :cond_c9

    iget-boolean v4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-eqz v4, :cond_fc

    :cond_c9
    if-eqz v1, :cond_fc

    .line 6729
    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-nez v1, :cond_e6

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:Z

    if-eqz v1, :cond_e6

    .line 6730
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v1}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    .line 6731
    if-eqz v1, :cond_104

    invoke-virtual {v1, p0, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_104

    move v1, v2

    :goto_e2
    or-int/2addr v0, v1

    .line 6732
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()V

    .line 6735
    :cond_e6
    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:Z

    if-eqz v1, :cond_f1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->n()Z

    move-result v1

    if-eqz v1, :cond_f1

    move v3, v2

    .line 6736
    :cond_f1
    if-nez v3, :cond_106

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->m()Z

    move-result v1

    if-eqz v1, :cond_106

    .line 6737
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->A()Z

    .line 6747
    :cond_fc
    :goto_fc
    if-nez v0, :cond_4f

    :cond_fe
    move v2, v5

    .line 6752
    goto/16 :goto_4f

    :cond_101
    move v1, v3

    .line 6696
    goto/16 :goto_66

    :cond_104
    move v1, v3

    .line 6731
    goto :goto_e2

    .line 6739
    :cond_106
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    .line 6740
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEX;

    invoke-virtual {v1}, LEX;->b()V

    .line 6741
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()Z

    move-result v1

    if-eqz v1, :cond_fc

    if-nez v3, :cond_fc

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_fc

    .line 6742
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFA;

    move-result-object v1

    invoke-virtual {v1}, LFA;->a()V

    goto :goto_fc

    :cond_126
    move v0, v4

    goto :goto_bf

    :cond_128
    move v4, v3

    goto/16 :goto_90
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    .line 6842
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_1c

    .line 6843
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v1, p0, v0, p1}, LFZ;->c(Lcom/google/android/apps/docs/editors/text/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 6844
    const/4 v0, 0x1

    .line 6848
    :goto_1b
    return v0

    :cond_1c
    invoke-super {p0, p1}, Landroid/view/View;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1b
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 6617
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 6618
    if-eqz p2, :cond_8

    .line 6619
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->H()V

    .line 6621
    :cond_8
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 4
    .parameter

    .prologue
    .line 6592
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 6594
    if-eqz p1, :cond_15

    .line 6595
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    if-eqz v0, :cond_11

    .line 6596
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    invoke-virtual {v0}, LFn;->b()V

    .line 6597
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->F()V

    .line 6612
    :cond_11
    :goto_11
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->c(Z)V

    .line 6613
    return-void

    .line 6600
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    if-eqz v0, :cond_1e

    .line 6601
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFn;

    invoke-virtual {v0}, LFn;->a()V

    .line 6604
    :cond_1e
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->t()V

    .line 6605
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-eqz v0, :cond_2a

    .line 6606
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    const/4 v1, 0x0

    iput-boolean v1, v0, LFv;->a:Z

    .line 6608
    :cond_2a
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->H()V

    .line 6609
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEX;

    invoke-virtual {v0}, LEX;->a()V

    goto :goto_11
.end method

.method public p()I
    .registers 2

    .prologue
    .line 5813
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method p()V
    .registers 3

    .prologue
    .line 5009
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 5010
    if-eqz v0, :cond_e

    iget v1, v0, LFw;->a:I

    if-eqz v1, :cond_e

    .line 5011
    const/4 v1, 0x0

    iput v1, v0, LFw;->a:I

    .line 5012
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(LFw;)V

    .line 5014
    :cond_e
    return-void
.end method

.method p()Z
    .registers 2

    .prologue
    .line 8888
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:Z

    return v0
.end method

.method public performLongClick()Z
    .registers 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 7497
    invoke-super {p0}, Landroid/view/View;->performLongClick()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 7498
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Z

    move v0, v1

    .line 7543
    :cond_b
    :goto_b
    return v0

    .line 7506
    :cond_c
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->m:I

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->n:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->b(II)Z

    move-result v0

    if-nez v0, :cond_5a

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->l:Z

    if-eqz v0, :cond_5a

    .line 7507
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->m:I

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->n:I

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v3

    .line 7508
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->v()V

    .line 7509
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 7510
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 7511
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFA;

    move-result-object v0

    invoke-virtual {v0}, LFA;->b()V

    :cond_39
    move v0, v1

    .line 7516
    :goto_3a
    if-nez v0, :cond_58

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    if-eqz v3, :cond_58

    .line 7531
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->z()Z

    move v3, v1

    .line 7536
    :goto_44
    if-nez v3, :cond_56

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->A()Z

    move-result v0

    if-eqz v0, :cond_56

    move v0, v1

    :goto_4d
    or-int/2addr v0, v3

    .line 7538
    if-eqz v0, :cond_b

    .line 7539
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->performHapticFeedback(I)Z

    .line 7540
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Z

    goto :goto_b

    :cond_56
    move v0, v2

    .line 7536
    goto :goto_4d

    :cond_58
    move v3, v0

    goto :goto_44

    :cond_5a
    move v0, v2

    goto :goto_3a
.end method

.method q()V
    .registers 1

    .prologue
    .line 5018
    return-void
.end method

.method q()Z
    .registers 2

    .prologue
    .line 8895
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->m:Z

    return v0
.end method

.method r()V
    .registers 4

    .prologue
    .line 5033
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 5034
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v0

    .line 5036
    if-gez v0, :cond_11

    iget v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v1, v1, 0x70

    const/16 v2, 0x50

    if-ne v1, v2, :cond_14

    .line 5037
    :cond_11
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->e()V

    .line 5040
    :cond_14
    if-ltz v0, :cond_1c

    .line 5041
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:Z

    .line 5042
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->F()V

    .line 5045
    :cond_1c
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->A()V

    .line 5046
    return-void
.end method

.method public r()Z
    .registers 2

    .prologue
    .line 8929
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    .line 8930
    if-eqz v0, :cond_c

    .line 8931
    iget v0, v0, LFw;->a:I

    if-lez v0, :cond_a

    const/4 v0, 0x1

    .line 8933
    :goto_9
    return v0

    .line 8931
    :cond_a
    const/4 v0, 0x0

    goto :goto_9

    .line 8933
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->n:Z

    goto :goto_9
.end method

.method public s()V
    .registers 1

    .prologue
    .line 5054
    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .registers 3
    .parameter

    .prologue
    .line 7228
    const/16 v0, 0x1000

    if-ne p1, v0, :cond_5

    .line 7236
    :cond_4
    :goto_4
    return-void

    .line 7232
    :cond_5
    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    .line 7235
    invoke-super {p0, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_4
.end method

.method public final setAutoLinkMask(I)V
    .registers 2
    .parameter

    .prologue
    .line 1948
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:I

    .line 1949
    return-void
.end method

.method public setCompoundDrawablePadding(I)V
    .registers 3
    .parameter

    .prologue
    .line 1631
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1632
    if-nez p1, :cond_f

    .line 1633
    if-eqz v0, :cond_8

    .line 1634
    iput p1, v0, LFt;->i:I

    .line 1643
    :cond_8
    :goto_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1644
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 1645
    return-void

    .line 1637
    :cond_f
    if-nez v0, :cond_18

    .line 1638
    new-instance v0, LFt;

    invoke-direct {v0, p0}, LFt;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1640
    :cond_18
    iput p1, v0, LFt;->i:I

    goto :goto_8
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1461
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1463
    if-nez p1, :cond_c

    if-nez p2, :cond_c

    if-nez p3, :cond_c

    if-eqz p4, :cond_1e

    :cond_c
    const/4 v1, 0x1

    .line 1465
    :goto_d
    if-nez v1, :cond_5d

    .line 1467
    if-eqz v0, :cond_17

    .line 1468
    iget v1, v0, LFt;->i:I

    if-nez v1, :cond_20

    .line 1469
    iput-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1558
    :cond_17
    :goto_17
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1559
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 1560
    return-void

    :cond_1e
    move v1, v2

    .line 1463
    goto :goto_d

    .line 1473
    :cond_20
    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_29

    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1474
    :cond_29
    iput-object v3, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    .line 1475
    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_34

    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1476
    :cond_34
    iput-object v3, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    .line 1477
    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3f

    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1478
    :cond_3f
    iput-object v3, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    .line 1479
    iget-object v1, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4a

    iget-object v1, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1480
    :cond_4a
    iput-object v3, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    .line 1481
    iput v2, v0, LFt;->g:I

    iput v2, v0, LFt;->c:I

    .line 1482
    iput v2, v0, LFt;->h:I

    iput v2, v0, LFt;->d:I

    .line 1483
    iput v2, v0, LFt;->e:I

    iput v2, v0, LFt;->a:I

    .line 1484
    iput v2, v0, LFt;->f:I

    iput v2, v0, LFt;->b:I

    goto :goto_17

    .line 1488
    :cond_5d
    if-nez v0, :cond_66

    .line 1489
    new-instance v0, LFt;

    invoke-direct {v0, p0}, LFt;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    .line 1492
    :cond_66
    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-eq v1, p1, :cond_73

    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_73

    .line 1493
    iget-object v1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1495
    :cond_73
    iput-object p1, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    .line 1497
    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-eq v1, p2, :cond_82

    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_82

    .line 1498
    iget-object v1, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1500
    :cond_82
    iput-object p2, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    .line 1502
    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-eq v1, p3, :cond_91

    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_91

    .line 1503
    iget-object v1, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1505
    :cond_91
    iput-object p3, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    .line 1507
    iget-object v1, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-eq v1, p4, :cond_a0

    iget-object v1, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_a0

    .line 1508
    iget-object v1, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1510
    :cond_a0
    iput-object p4, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    .line 1512
    iget-object v1, v0, LFt;->a:Landroid/graphics/Rect;

    .line 1515
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getDrawableState()[I

    move-result-object v3

    .line 1517
    if-eqz p1, :cond_106

    .line 1518
    invoke-virtual {p1, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1519
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 1520
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1521
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, v0, LFt;->c:I

    .line 1522
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, v0, LFt;->g:I

    .line 1527
    :goto_bf
    if-eqz p3, :cond_10b

    .line 1528
    invoke-virtual {p3, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1529
    invoke-virtual {p3, v1}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 1530
    invoke-virtual {p3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1531
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, v0, LFt;->d:I

    .line 1532
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, v0, LFt;->h:I

    .line 1537
    :goto_d6
    if-eqz p2, :cond_110

    .line 1538
    invoke-virtual {p2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1539
    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 1540
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1541
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, v0, LFt;->a:I

    .line 1542
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, v0, LFt;->e:I

    .line 1547
    :goto_ed
    if-eqz p4, :cond_115

    .line 1548
    invoke-virtual {p4, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1549
    invoke-virtual {p4, v1}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 1550
    invoke-virtual {p4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1551
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v0, LFt;->b:I

    .line 1552
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, LFt;->f:I

    goto/16 :goto_17

    .line 1524
    :cond_106
    iput v2, v0, LFt;->g:I

    iput v2, v0, LFt;->c:I

    goto :goto_bf

    .line 1534
    :cond_10b
    iput v2, v0, LFt;->h:I

    iput v2, v0, LFt;->d:I

    goto :goto_d6

    .line 1544
    :cond_110
    iput v2, v0, LFt;->e:I

    iput v2, v0, LFt;->a:I

    goto :goto_ed

    .line 1554
    :cond_115
    iput v2, v0, LFt;->f:I

    iput v2, v0, LFt;->b:I

    goto/16 :goto_17
.end method

.method public setCompoundDrawablesWithIntrinsicBounds(IIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1578
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1579
    if-eqz p1, :cond_27

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v3, v1

    :goto_10
    if-eqz p2, :cond_29

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v2, v1

    :goto_17
    if-eqz p3, :cond_2b

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_1d
    if-eqz p4, :cond_23

    invoke-virtual {v4, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_23
    invoke-virtual {p0, v3, v2, v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1582
    return-void

    :cond_27
    move-object v3, v0

    .line 1579
    goto :goto_10

    :cond_29
    move-object v2, v0

    goto :goto_17

    :cond_2b
    move-object v1, v0

    goto :goto_1d
.end method

.method public setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1597
    if-eqz p1, :cond_e

    .line 1598
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1600
    :cond_e
    if-eqz p3, :cond_1b

    .line 1601
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p3, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1603
    :cond_1b
    if-eqz p2, :cond_28

    .line 1604
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1606
    :cond_28
    if-eqz p4, :cond_35

    .line 1607
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p4, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1609
    :cond_35
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/editors/text/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1610
    return-void
.end method

.method public setCursorVisible(Z)V
    .registers 3
    .parameter

    .prologue
    .line 5948
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->k:Z

    if-eq v0, p1, :cond_f

    .line 5949
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->k:Z

    .line 5950
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 5952
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->F()V

    .line 5955
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    .line 5957
    :cond_f
    return-void
.end method

.method public setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V
    .registers 2
    .parameter

    .prologue
    .line 7599
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/view/ActionMode$Callback;

    .line 7600
    return-void
.end method

.method public final setEditableFactory(Landroid/text/Editable$Factory;)V
    .registers 3
    .parameter

    .prologue
    .line 2657
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Editable$Factory;

    .line 2658
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2659
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .registers 3
    .parameter

    .prologue
    .line 5899
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    .line 5901
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_f

    .line 5902
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 5903
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 5904
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 5906
    :cond_f
    return-void
.end method

.method public setEms(I)V
    .registers 3
    .parameter

    .prologue
    .line 2344
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->A:I

    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:I

    .line 2345
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->B:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->z:I

    .line 2347
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2348
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2349
    return-void
.end method

.method public setEnabled(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1030
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isEnabled()Z

    move-result v0

    if-ne p1, v0, :cond_7

    .line 1042
    :goto_6
    return-void

    .line 1034
    :cond_7
    if-nez p1, :cond_1f

    .line 1036
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 1037
    if-eqz v0, :cond_1f

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 1038
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1041
    :cond_1f
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_6
.end method

.method public setExtractedText(Landroid/view/inputmethod/ExtractedText;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 4798
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()Landroid/text/Editable;

    move-result-object v4

    .line 4799
    iget-object v0, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    if-eqz v0, :cond_12

    .line 4800
    if-nez v4, :cond_32

    .line 4801
    iget-object v0, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    sget-object v1, LFo;->c:LFo;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;LFo;)V

    .line 4820
    :cond_12
    :goto_12
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    .line 4821
    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    .line 4822
    iget v1, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 4823
    if-gez v1, :cond_5d

    move v1, v3

    .line 4826
    :cond_21
    :goto_21
    iget v4, p1, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    .line 4827
    if-gez v4, :cond_61

    .line 4830
    :goto_25
    invoke-static {v0, v1, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 4833
    iget v1, p1, Landroid/view/inputmethod/ExtractedText;->flags:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_65

    .line 4834
    invoke-static {p0, v0}, LFY;->a(Landroid/view/View;Landroid/text/Spannable;)V

    .line 4838
    :goto_31
    return-void

    .line 4802
    :cond_32
    iget v0, p1, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    if-gez v0, :cond_47

    .line 4803
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-static {v4, v3, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/Spannable;II)V

    .line 4804
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v4, v3, v0, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_12

    .line 4806
    :cond_47
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v1

    .line 4807
    iget v0, p1, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    .line 4808
    if-le v0, v1, :cond_50

    move v0, v1

    .line 4809
    :cond_50
    iget v2, p1, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    .line 4810
    if-le v2, v1, :cond_6b

    .line 4811
    :goto_54
    invoke-static {v4, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/Spannable;II)V

    .line 4812
    iget-object v2, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v4, v0, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_12

    .line 4825
    :cond_5d
    if-le v1, v2, :cond_21

    move v1, v2

    goto :goto_21

    .line 4829
    :cond_61
    if-le v4, v2, :cond_69

    move v3, v2

    goto :goto_25

    .line 4836
    :cond_65
    invoke-static {p0, v0}, LFY;->b(Landroid/view/View;Landroid/text/Spannable;)V

    goto :goto_31

    :cond_69
    move v3, v4

    goto :goto_25

    :cond_6b
    move v1, v2

    goto :goto_54
.end method

.method public setExtracting(Landroid/view/inputmethod/ExtractedTextRequest;)V
    .registers 3
    .parameter

    .prologue
    .line 4844
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    if-eqz v0, :cond_8

    .line 4845
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFw;

    iput-object p1, v0, LFw;->a:Landroid/view/inputmethod/ExtractedTextRequest;

    .line 4848
    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->H()V

    .line 4849
    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .registers 3
    .parameter

    .prologue
    .line 3307
    if-nez p1, :cond_8

    .line 3308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 3311
    :cond_8
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:[Landroid/text/InputFilter;

    .line 3313
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Editable;

    if-eqz v0, :cond_17

    .line 3314
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Editable;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/Editable;[Landroid/text/InputFilter;)V

    .line 3316
    :cond_17
    return-void
.end method

.method public setFreezesText(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2636
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Z

    .line 2637
    return-void
.end method

.method public setGravity(I)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2073
    and-int/lit8 v0, p1, 0x7

    if-nez v0, :cond_4b

    .line 2074
    or-int/lit8 v0, p1, 0x3

    .line 2076
    :goto_8
    and-int/lit8 v1, v0, 0x70

    if-nez v1, :cond_e

    .line 2077
    or-int/lit8 v0, v0, 0x30

    .line 2082
    :cond_e
    and-int/lit8 v1, v0, 0x7

    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    and-int/lit8 v3, v3, 0x7

    if-eq v1, v3, :cond_49

    move v1, v6

    .line 2086
    :goto_17
    iget v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    if-eq v0, v3, :cond_1e

    .line 2087
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2090
    :cond_1e
    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:I

    .line 2092
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_48

    if-eqz v1, :cond_48

    .line 2094
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0}, LEj;->a()I

    move-result v1

    .line 2095
    sget-object v3, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    sget-object v4, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/BoringLayout$Metrics;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLeft()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    sub-int v5, v0, v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IILandroid/text/BoringLayout$Metrics;Landroid/text/BoringLayout$Metrics;IZ)V

    .line 2098
    :cond_48
    return-void

    :cond_49
    move v1, v2

    goto :goto_17

    :cond_4b
    move v0, p1

    goto :goto_8
.end method

.method public setHeight(I)V
    .registers 3
    .parameter

    .prologue
    .line 2274
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    .line 2275
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    .line 2277
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2278
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2279
    return-void
.end method

.method public setHighlightColor(I)V
    .registers 3
    .parameter

    .prologue
    .line 1906
    iget v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:I

    if-eq v0, p1, :cond_9

    .line 1907
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:I

    .line 1908
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1910
    :cond_9
    return-void
.end method

.method public final setHintTextColor(I)V
    .registers 3
    .parameter

    .prologue
    .line 1996
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/content/res/ColorStateList;

    .line 1997
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()V

    .line 1998
    return-void
.end method

.method public final setHintTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .parameter

    .prologue
    .line 2006
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:Landroid/content/res/ColorStateList;

    .line 2007
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()V

    .line 2008
    return-void
.end method

.method public setHorizontallyScrolling(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2159
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->s:Z

    .line 2161
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_f

    .line 2162
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 2163
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2164
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2166
    :cond_f
    return-void
.end method

.method public setImeActionLabel(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3082
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-nez v0, :cond_b

    .line 3083
    new-instance v0, LFv;

    invoke-direct {v0, p0}, LFv;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    .line 3085
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iput-object p1, v0, LFv;->a:Ljava/lang/CharSequence;

    .line 3086
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iput p2, v0, LFv;->b:I

    .line 3087
    return-void
.end method

.method public setImeOptions(I)V
    .registers 3
    .parameter

    .prologue
    .line 3054
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-nez v0, :cond_b

    .line 3055
    new-instance v0, LFv;

    invoke-direct {v0, p0}, LFv;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    .line 3057
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iput p1, v0, LFv;->a:I

    .line 3058
    return-void
.end method

.method public setIncludeFontPadding(Z)V
    .registers 3
    .parameter

    .prologue
    .line 5220
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:Z

    .line 5222
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_f

    .line 5223
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 5224
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 5225
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 5227
    :cond_f
    return-void
.end method

.method public setInputExtras(I)V
    .registers 5
    .parameter

    .prologue
    .line 3238
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    .line 3239
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-nez v1, :cond_13

    new-instance v1, LFv;

    invoke-direct {v1, p0}, LFv;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    .line 3240
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, v1, LFv;->a:Landroid/os/Bundle;

    .line 3241
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iget-object v2, v2, LFv;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->parseBundleExtras(Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;)V

    .line 3242
    return-void
.end method

.method public setInputType(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2941
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(IZ)V

    .line 2944
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->c(I)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 2948
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    if-ne v2, v0, :cond_1c

    .line 2954
    :goto_10
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 2955
    if-eqz v0, :cond_1b

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 2956
    :cond_1b
    return-void

    .line 2951
    :cond_1c
    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(ZZZ)V

    goto :goto_10
.end method

.method public setKeyListener(Landroid/text/method/KeyListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1166
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Landroid/text/method/KeyListener;)V

    .line 1167
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()V

    .line 1169
    if-eqz p1, :cond_29

    .line 1171
    :try_start_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/method/KeyListener;

    invoke-interface {v0}, Landroid/text/method/KeyListener;->getInputType()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I
    :try_end_10
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_8 .. :try_end_10} :catch_24

    .line 1177
    :goto_10
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)V

    .line 1182
    :goto_15
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    .line 1183
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/Context;

    invoke-static {v0}, LEt;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 1184
    if-eqz v0, :cond_23

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 1185
    :cond_23
    return-void

    .line 1172
    :catch_24
    move-exception v0

    .line 1173
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    goto :goto_10

    .line 1179
    :cond_29
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    goto :goto_15
.end method

.method public setLineSpacing(FF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2375
    iput p2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:F

    .line 2376
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:F

    .line 2378
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_11

    .line 2379
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 2380
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2381
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2383
    :cond_11
    return-void
.end method

.method public setLines(I)V
    .registers 3
    .parameter

    .prologue
    .line 2256
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    .line 2257
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    .line 2259
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2260
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2261
    return-void
.end method

.method public final setLinkTextColor(I)V
    .registers 3
    .parameter

    .prologue
    .line 2039
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/content/res/ColorStateList;

    .line 2040
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()V

    .line 2041
    return-void
.end method

.method public final setLinkTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .parameter

    .prologue
    .line 2049
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:Landroid/content/res/ColorStateList;

    .line 2050
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()V

    .line 2051
    return-void
.end method

.method public final setLinksClickable(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1960
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->t:Z

    .line 1961
    return-void
.end method

.method public setMarqueeRepeatLimit(I)V
    .registers 2
    .parameter

    .prologue
    .line 5915
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:I

    .line 5916
    return-void
.end method

.method public setMaxEms(I)V
    .registers 3
    .parameter

    .prologue
    .line 2316
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:I

    .line 2317
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->z:I

    .line 2319
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2320
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2321
    return-void
.end method

.method public setMaxHeight(I)V
    .registers 3
    .parameter

    .prologue
    .line 2238
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    .line 2239
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    .line 2241
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2242
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2243
    return-void
.end method

.method public setMaxLines(I)V
    .registers 3
    .parameter

    .prologue
    .line 2221
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:I

    .line 2222
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->v:I

    .line 2224
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2225
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2226
    return-void
.end method

.method public setMaxWidth(I)V
    .registers 3
    .parameter

    .prologue
    .line 2330
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:I

    .line 2331
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->z:I

    .line 2333
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2334
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2335
    return-void
.end method

.method public setMinEms(I)V
    .registers 3
    .parameter

    .prologue
    .line 2288
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->A:I

    .line 2289
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->B:I

    .line 2291
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2292
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2293
    return-void
.end method

.method public setMinHeight(I)V
    .registers 3
    .parameter

    .prologue
    .line 2205
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    .line 2206
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:I

    .line 2208
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2209
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2210
    return-void
.end method

.method public setMinLines(I)V
    .registers 3
    .parameter

    .prologue
    .line 2189
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->w:I

    .line 2190
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->x:I

    .line 2192
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2193
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2194
    return-void
.end method

.method public setMinWidth(I)V
    .registers 3
    .parameter

    .prologue
    .line 2302
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->A:I

    .line 2303
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->B:I

    .line 2305
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2306
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2307
    return-void
.end method

.method public final setMovementMethod(LFZ;)V
    .registers 3
    .parameter

    .prologue
    .line 1214
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    .line 1216
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFZ;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1218
    :cond_11
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->b()V

    .line 1222
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    .line 1223
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 8938
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/view/View$OnClickListener;

    .line 8939
    invoke-super {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 8940
    return-void
.end method

.method public setOnEditorActionListener(LFD;)V
    .registers 3
    .parameter

    .prologue
    .line 3118
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-nez v0, :cond_b

    .line 3119
    new-instance v0, LFv;

    invoke-direct {v0, p0}, LFv;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    .line 3121
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iput-object p1, v0, LFv;->a:LFD;

    .line 3122
    return-void
.end method

.method public setPadding(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1657
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingLeft()I

    move-result v0

    if-ne p1, v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingRight()I

    move-result v0

    if-ne p3, v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingTop()I

    move-result v0

    if-ne p2, v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getPaddingBottom()I

    move-result v0

    if-eq p4, v0, :cond_1b

    .line 1659
    :cond_18
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 1663
    :cond_1b
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 1664
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1665
    return-void
.end method

.method public setPaddingDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4105
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->d:Landroid/graphics/drawable/Drawable;

    .line 4106
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Landroid/graphics/drawable/Drawable;

    .line 4107
    iput-object p3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->f:Landroid/graphics/drawable/Drawable;

    .line 4108
    iput-object p4, p0, Lcom/google/android/apps/docs/editors/text/TextView;->g:Landroid/graphics/drawable/Drawable;

    .line 4109
    return-void
.end method

.method public setPaintFlags(I)V
    .registers 3
    .parameter

    .prologue
    .line 2141
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFlags()I

    move-result v0

    if-eq v0, p1, :cond_1a

    .line 2142
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setFlags(I)V

    .line 2144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_1a

    .line 2145
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 2146
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2147
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2150
    :cond_1a
    return-void
.end method

.method public setPrivateImeOptions(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 3212
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    if-nez v0, :cond_b

    new-instance v0, LFv;

    invoke-direct {v0, p0}, LFv;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    .line 3213
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFv;

    iput-object p1, v0, LFv;->a:Ljava/lang/String;

    .line 3214
    return-void
.end method

.method public setRawInputType(I)V
    .registers 2
    .parameter

    .prologue
    .line 2987
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->q:I

    .line 2988
    return-void
.end method

.method public setScroller(Landroid/widget/Scroller;)V
    .registers 2
    .parameter

    .prologue
    .line 6852
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/widget/Scroller;

    .line 6853
    return-void
.end method

.method public setSelectAllOnFocus(Z)V
    .registers 4
    .parameter

    .prologue
    .line 5934
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->r:Z

    .line 5936
    if-eqz p1, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-nez v0, :cond_11

    .line 5937
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    sget-object v1, LFo;->b:LFo;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;LFo;)V

    .line 5939
    :cond_11
    return-void
.end method

.method public setSelected(Z)V
    .registers 4
    .parameter

    .prologue
    .line 6636
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->isSelected()Z

    move-result v0

    .line 6638
    invoke-super {p0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 6640
    if-eq p1, v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextUtils$TruncateAt;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v0, v1, :cond_14

    .line 6641
    if-eqz p1, :cond_15

    .line 6642
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->C()V

    .line 6647
    :cond_14
    :goto_14
    return-void

    .line 6644
    :cond_15
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->D()V

    goto :goto_14
.end method

.method public setShadowLayer(FFFI)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1922
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 1924
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:F

    .line 1925
    iput p2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->b:F

    .line 1926
    iput p3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->c:F

    .line 1928
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1929
    return-void
.end method

.method public setSingleLine()V
    .registers 2

    .prologue
    .line 5833
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setSingleLine(Z)V

    .line 5834
    return-void
.end method

.method public setSingleLine(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 5851
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Z)V

    .line 5852
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(ZZZ)V

    .line 5853
    return-void
.end method

.method public final setSpannableFactory(Landroid/text/Spannable$Factory;)V
    .registers 3
    .parameter

    .prologue
    .line 2665
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/Spannable$Factory;

    .line 2666
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2667
    return-void
.end method

.method public final setText(I)V
    .registers 3
    .parameter

    .prologue
    .line 2912
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2913
    return-void
.end method

.method public final setText(ILFo;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2916
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;LFo;)V

    .line 2917
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 2683
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFo;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;LFo;)V

    .line 2684
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;LFo;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2708
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;LFo;ZI)V

    .line 2710
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LDX;

    if-eqz v0, :cond_f

    .line 2711
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LDX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LDX;->a([C)V

    .line 2713
    :cond_f
    return-void
.end method

.method public final setText([CII)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2862
    .line 2864
    if-ltz p2, :cond_a

    if-ltz p3, :cond_a

    add-int v0, p2, p3

    array-length v2, p1

    if-le v0, v2, :cond_27

    .line 2865
    :cond_a
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2872
    :cond_27
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_49

    .line 2873
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 2874
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-direct {p0, v2, v1, v0, p3}, Lcom/google/android/apps/docs/editors/text/TextView;->d(Ljava/lang/CharSequence;III)V

    .line 2879
    :goto_36
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LDX;

    if-nez v2, :cond_50

    .line 2880
    new-instance v2, LDX;

    invoke-direct {v2, p1, p2, p3}, LDX;-><init>([CII)V

    iput-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LDX;

    .line 2885
    :goto_41
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LDX;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFo;

    invoke-direct {p0, v2, v3, v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Ljava/lang/CharSequence;LFo;ZI)V

    .line 2886
    return-void

    .line 2876
    :cond_49
    const-string v0, ""

    invoke-direct {p0, v0, v1, v1, p3}, Lcom/google/android/apps/docs/editors/text/TextView;->d(Ljava/lang/CharSequence;III)V

    move v0, v1

    goto :goto_36

    .line 2882
    :cond_50
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LDX;

    invoke-virtual {v2, p1, p2, p3}, LDX;->a([CII)V

    goto :goto_41
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1732
    return-void
.end method

.method public setTextColor(I)V
    .registers 3
    .parameter

    .prologue
    .line 1861
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/res/ColorStateList;

    .line 1862
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()V

    .line 1863
    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .registers 3
    .parameter

    .prologue
    .line 1871
    if-nez p1, :cond_8

    .line 1872
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1875
    :cond_8
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/content/res/ColorStateList;

    .line 1876
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->c()V

    .line 1877
    return-void
.end method

.method public setTextIsSelectable(Z)V
    .registers 4
    .parameter

    .prologue
    .line 3749
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    if-ne v0, p1, :cond_5

    .line 3765
    :goto_4
    return-void

    .line 3751
    :cond_5
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->u:Z

    .line 3753
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setFocusableInTouchMode(Z)V

    .line 3754
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setFocusable(Z)V

    .line 3755
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setClickable(Z)V

    .line 3756
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setLongClickable(Z)V

    .line 3760
    if-eqz p1, :cond_2b

    invoke-static {}, LFM;->a()LFZ;

    move-result-object v0

    :goto_19
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setMovementMethod(LFZ;)V

    .line 3761
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz p1, :cond_2d

    sget-object v0, LFo;->b:LFo;

    :goto_24
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;LFo;)V

    .line 3764
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->E()V

    goto :goto_4

    .line 3760
    :cond_2b
    const/4 v0, 0x0

    goto :goto_19

    .line 3761
    :cond_2d
    sget-object v0, LFo;->a:LFo;

    goto :goto_24
.end method

.method public final setTextKeepState(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 2696
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFo;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setTextKeepState(Ljava/lang/CharSequence;LFo;)V

    .line 2697
    return-void
.end method

.method public final setTextKeepState(Ljava/lang/CharSequence;LFo;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 2896
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->o()I

    move-result v1

    .line 2897
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->p()I

    move-result v2

    .line 2898
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 2900
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;LFo;)V

    .line 2902
    if-gez v1, :cond_14

    if-ltz v2, :cond_31

    .line 2903
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_31

    .line 2904
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 2908
    :cond_31
    return-void
.end method

.method public setTextScaleX(F)V
    .registers 3
    .parameter

    .prologue
    .line 1803
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextScaleX()F

    move-result v0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1f

    .line 1804
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->j:Z

    .line 1805
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextScaleX(F)V

    .line 1807
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_1f

    .line 1808
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 1809
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 1810
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1813
    :cond_1f
    return-void
.end method

.method public setTextSize(F)V
    .registers 3
    .parameter

    .prologue
    .line 1752
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setTextSize(IF)V

    .line 1753
    return-void
.end method

.method public setTextSize(IF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1765
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1768
    if-nez v0, :cond_16

    .line 1769
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    .line 1773
    :goto_a
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {p1, p2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(F)V

    .line 1774
    return-void

    .line 1771
    :cond_16
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_a
.end method

.method public final setTransformationMethod(LGv;)V
    .registers 4
    .parameter

    .prologue
    .line 1253
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    if-ne p1, v0, :cond_5

    .line 1267
    :goto_4
    return-void

    .line 1258
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    if-eqz v0, :cond_18

    .line 1259
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_18

    .line 1260
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    invoke-interface {v0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 1264
    :cond_18
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LGv;

    .line 1266
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .parameter

    .prologue
    .line 1834
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eq v0, p1, :cond_1a

    .line 1835
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1837
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v0, :cond_1a

    .line 1838
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()V

    .line 1839
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 1840
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 1843
    :cond_1a
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1053
    if-lez p2, :cond_35

    .line 1054
    if-nez p1, :cond_2c

    .line 1055
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1060
    :goto_a
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1062
    if-eqz v0, :cond_31

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 1063
    :goto_13
    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p2

    .line 1064
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_1d

    const/4 v1, 0x1

    :cond_1d
    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1065
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_33

    const/high16 v0, -0x4180

    :goto_28
    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 1071
    :goto_2b
    return-void

    .line 1057
    :cond_2c
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_a

    :cond_31
    move v0, v1

    .line 1062
    goto :goto_13

    :cond_33
    move v0, v2

    .line 1065
    goto :goto_28

    .line 1067
    :cond_35
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1068
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 1069
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/text/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_2b
.end method

.method public setWidth(I)V
    .registers 3
    .parameter

    .prologue
    .line 2359
    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->A:I

    iput p1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->y:I

    .line 2360
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->B:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->z:I

    .line 2362
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->requestLayout()V

    .line 2363
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 2364
    return-void
.end method

.method public t()V
    .registers 1

    .prologue
    .line 5062
    return-void
.end method

.method public u()V
    .registers 2

    .prologue
    .line 6831
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->e:Z

    .line 6832
    return-void
.end method

.method protected v()V
    .registers 2

    .prologue
    .line 7672
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    if-eqz v0, :cond_9

    .line 7674
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    invoke-interface {v0}, LER;->q()V

    .line 7676
    :cond_9
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 4
    .parameter

    .prologue
    .line 3636
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 3637
    if-nez v0, :cond_23

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    if-eqz v1, :cond_23

    .line 3638
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    iget-object v0, v0, LFt;->c:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    iget-object v0, v0, LFt;->a:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    iget-object v0, v0, LFt;->d:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LFt;

    iget-object v0, v0, LFt;->b:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_24

    :cond_22
    const/4 v0, 0x1

    .line 3641
    :cond_23
    :goto_23
    return v0

    .line 3638
    :cond_24
    const/4 v0, 0x0

    goto :goto_23
.end method

.method public w()V
    .registers 2

    .prologue
    .line 7682
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFH;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 7683
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFH;

    move-result-object v0

    invoke-virtual {v0}, LFH;->a()V

    .line 7685
    :cond_d
    return-void
.end method

.method public x()V
    .registers 2

    .prologue
    .line 7691
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFH;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 7692
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LFH;

    move-result-object v0

    invoke-virtual {v0}, LFH;->b()V

    .line 7694
    :cond_d
    return-void
.end method

.method public y()V
    .registers 2

    .prologue
    .line 7697
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LER;

    .line 7698
    return-void
.end method
