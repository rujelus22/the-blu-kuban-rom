.class public Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;
.super Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;
.source "EditCommentStateMachineFragment.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;-><init>()V

    return-void
.end method


# virtual methods
.method a(Landroid/view/ViewGroup;)Landroid/view/animation/Animation;
    .registers 4
    .parameter

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lsy;->discussion_vertical_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method public a()Ltm;
    .registers 2

    .prologue
    .line 24
    sget-object v0, Ltm;->d:Ltm;

    return-object v0
.end method

.method public g()V
    .registers 4

    .prologue
    .line 29
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->g()V

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;->k()Z

    move-result v1

    if-nez v1, :cond_21

    .line 33
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 36
    :cond_21
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;->a(Landroid/view/ViewGroup;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 38
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 39
    const-string v0, "EditCommentStateMachineFragment"

    const-string v1, "onResume - starting animation"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    return-void
.end method
