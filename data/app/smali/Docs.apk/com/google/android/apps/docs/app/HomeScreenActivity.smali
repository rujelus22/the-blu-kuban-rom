.class public Lcom/google/android/apps/docs/app/HomeScreenActivity;
.super Lcom/google/android/apps/docs/app/BaseDialogActivity;
.source "HomeScreenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Lhm;

.field private static final b:Lhm;


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/widget/Button;

.field private a:Lcom/google/android/apps/docs/view/TitleBar;

.field private a:Ldx;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lev;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LiG;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ljo;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LkB;

.field public a:Lky;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field protected a:LoL;

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<[",
            "Lii;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    .line 99
    new-instance v0, Lhm;

    new-array v1, v4, [I

    sget v2, Leh;->home_screen_row1:I

    aput v2, v1, v3

    sget v2, Leh;->home_screen_row2:I

    aput v2, v1, v5

    invoke-direct {v0, v6, v1, v7}, Lhm;-><init>(I[ILhf;)V

    sput-object v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lhm;

    .line 101
    new-instance v0, Lhm;

    new-array v1, v6, [I

    sget v2, Leh;->home_screen_row1:I

    aput v2, v1, v3

    sget v2, Leh;->home_screen_row2:I

    aput v2, v1, v5

    sget v2, Leh;->home_screen_row3:I

    aput v2, v1, v4

    const/4 v2, 0x3

    sget v3, Leh;->home_screen_row4:I

    aput v3, v1, v2

    invoke-direct {v0, v4, v1, v7}, Lhm;-><init>(I[ILhf;)V

    sput-object v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b:Lhm;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 234
    invoke-static {}, Lhl;->values()[Lhl;

    move-result-object v0

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;-><init>(I)V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->i:Z

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Ldx;

    .line 235
    return-void
.end method

.method public static a(LmK;)I
    .registers 2
    .parameter

    .prologue
    .line 807
    if-eqz p0, :cond_9

    invoke-virtual {p0}, LmK;->ordinal()I

    move-result v0

    .line 808
    :goto_6
    add-int/lit16 v0, v0, 0x3e8

    return v0

    .line 807
    :cond_9
    const/4 v0, -0x1

    goto :goto_6
.end method

.method private a()Lhm;
    .registers 3

    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_10

    .line 432
    sget-object v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lhm;

    .line 434
    :goto_f
    return-object v0

    :cond_10
    sget-object v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b:Lhm;

    goto :goto_f
.end method

.method private a(Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 581
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/util/List;
    .registers 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 376
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 377
    sget-object v0, Lik;->a:Lik;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Lik;)[Lhk;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_10
    if-ge v0, v4, :cond_4c

    aget-object v5, v3, v0

    .line 378
    new-instance v6, Landroid/widget/Button;

    sget v7, Leo;->HomeScreen_IconEntry:I

    invoke-direct {v6, p0, v10, v7}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 379
    invoke-virtual {v6, v5}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 380
    iget-object v7, v5, Lhk;->a:LmK;

    invoke-static {v7}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LmK;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setId(I)V

    .line 381
    iget v7, v5, Lhk;->a:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 382
    sget v8, Leg;->home_page_button:I

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 383
    invoke-virtual {v6, v7}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 384
    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 385
    iget v5, v5, Lhk;->b:I

    invoke-virtual {v6, v1, v5, v1, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 386
    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    invoke-virtual {v6, v9}, Landroid/widget/Button;->setGravity(I)V

    .line 388
    invoke-virtual {v6, v9}, Landroid/widget/Button;->setFocusable(Z)V

    .line 389
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 392
    :cond_4c
    new-instance v0, Landroid/widget/Button;

    sget v3, Leo;->HomeScreen_IconEntry:I

    invoke-direct {v0, p0, v10, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 393
    const-string v3, "buttonMore"

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 394
    sget v3, Len;->home_more_choices:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 395
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 396
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 397
    sget v3, Leg;->home_more:I

    invoke-virtual {v0, v1, v3, v1, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 398
    sget v1, Leg;->home_more_background:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 399
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 400
    invoke-virtual {v0, v9}, Landroid/widget/Button;->setGravity(I)V

    .line 401
    invoke-virtual {v0, v9}, Landroid/widget/Button;->setFocusable(Z)V

    .line 402
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    return-object v2
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/HomeScreenActivity;)LkB;
    .registers 2
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    return-object v0
.end method

.method private a(Landroid/accounts/Account;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 681
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b(Ljava/lang/String;Z)V

    .line 683
    return-void
.end method

.method private a(Landroid/widget/ListView;)V
    .registers 8
    .parameter

    .prologue
    .line 327
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 329
    sget-object v0, Lik;->a:Lik;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Lik;)[Lhk;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->d()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 332
    sget-object v0, Lik;->c:Lik;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Lik;)[Lhk;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 335
    :cond_27
    new-instance v0, Lhg;

    sget v3, Lej;->navigation_menu_item:I

    sget v4, Leh;->navigation_name:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lhg;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;Landroid/content/Context;IILjava/util/ArrayList;)V

    .line 348
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_36
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhk;

    .line 349
    iget v1, v1, Lhk;->a:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_36

    .line 352
    :cond_4c
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 353
    new-instance v0, Lhh;

    invoke-direct {v0, p0, v5}, Lhh;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 362
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/HomeScreenActivity;Ljava/lang/String;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 639
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->i:Z

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Ljava/lang/String;Z)V

    .line 640
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 643
    if-eqz p1, :cond_2a

    move v0, v1

    :goto_5
    invoke-static {v0}, Lagu;->a(Z)V

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    invoke-static {v0, p1}, LZn;->a(LME;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 647
    if-eqz v0, :cond_2c

    :goto_10
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Google account not found for user="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lagu;->a(ZLjava/lang/Object;)V

    .line 650
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/accounts/Account;Z)V

    .line 651
    return-void

    :cond_2a
    move v0, v2

    .line 643
    goto :goto_5

    :cond_2c
    move v1, v2

    .line 647
    goto :goto_10
.end method

.method private a(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 408
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Lhm;

    move-result-object v5

    .line 413
    iget-object v3, v5, Lhm;->a:[I

    array-length v4, v3

    move v1, v2

    :goto_9
    if-ge v1, v4, :cond_1a

    aget v0, v3, v1

    .line 414
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 415
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 413
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 419
    :cond_1a
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_1f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 420
    add-int/lit8 v4, v3, 0x1

    .line 422
    iget v0, v5, Lhm;->a:I

    div-int/2addr v3, v0

    .line 423
    iget-object v0, v5, Lhm;->a:[I

    array-length v0, v0

    if-ge v3, v0, :cond_49

    const/4 v0, 0x1

    :goto_37
    invoke-static {v0}, Lagu;->b(Z)V

    .line 424
    iget-object v0, v5, Lhm;->a:[I

    aget v0, v0, v3

    .line 425
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 426
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v3, v4

    .line 427
    goto :goto_1f

    :cond_49
    move v0, v2

    .line 423
    goto :goto_37

    .line 428
    :cond_4b
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/HomeScreenActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->e()Z

    move-result v0

    return v0
.end method

.method private a(Lik;)[Lhk;
    .registers 12
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 444
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lii;

    array-length v4, v0

    move v1, v2

    :goto_18
    if-ge v1, v4, :cond_3e

    aget-object v5, v0, v1

    .line 450
    invoke-virtual {v5, p1}, Lii;->a(Lik;)Z

    move-result v6

    if-nez v6, :cond_25

    .line 447
    :goto_22
    add-int/lit8 v1, v1, 0x1

    goto :goto_18

    .line 454
    :cond_25
    new-instance v6, Lhk;

    invoke-virtual {v5}, Lii;->a()LmK;

    move-result-object v7

    invoke-virtual {v5}, Lii;->b()I

    move-result v8

    invoke-virtual {v5}, Lii;->a()I

    move-result v9

    invoke-virtual {v5}, Lii;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v7, v8, v9, v5}, Lhk;-><init>(LmK;IILjava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_22

    .line 459
    :cond_3e
    new-array v0, v2, [Lhk;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhk;

    return-object v0
.end method

.method private a()[Lii;
    .registers 4

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    invoke-virtual {v0}, Lfb;->b()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lii;->a(IILfo;)[Lii;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;Z)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 686
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 687
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4c

    move v0, v1

    :goto_c
    invoke-static {v0}, Lagu;->a(Z)V

    .line 688
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Llf;

    invoke-interface {v0, p1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 689
    invoke-virtual {v0}, LkB;->c()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_22

    .line 690
    invoke-virtual {v0}, LkB;->c()V

    .line 693
    :cond_22
    iget-object v3, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    if-eqz v3, :cond_4e

    invoke-virtual {v0}, LkB;->c()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v5}, LkB;->c()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_4e

    .line 694
    :goto_34
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LMM;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    invoke-interface {v2, v3, p1}, LMM;->a(Landroid/widget/Button;Ljava/lang/String;)V

    .line 695
    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    .line 697
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->n()V

    .line 699
    if-nez v1, :cond_4b

    if-nez p2, :cond_4b

    .line 700
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    invoke-interface {v1, v0}, LME;->a(LkB;)V

    .line 702
    :cond_4b
    return-void

    :cond_4c
    move v0, v2

    .line 687
    goto :goto_c

    :cond_4e
    move v1, v2

    .line 693
    goto :goto_34
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 744
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private h()V
    .registers 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 310
    sget-object v1, Lfb;->b:Lfb;

    if-ne v0, v1, :cond_1f

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 312
    sget v1, Leh;->home_screen_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 314
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/widget/ListView;)V

    .line 316
    :cond_1f
    return-void
.end method

.method private i()V
    .registers 2

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Ljava/util/List;

    move-result-object v0

    .line 372
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Ljava/util/List;)V

    .line 373
    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    .line 463
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/IntroDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/IntroDialogFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Lo;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/IntroDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 464
    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    if-eqz v0, :cond_5

    .line 634
    :goto_4
    return-void

    .line 628
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    invoke-static {v0}, LMG;->b(LME;)Landroid/accounts/Account;

    move-result-object v0

    .line 629
    if-nez v0, :cond_11

    .line 630
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->l()V

    goto :goto_4

    .line 632
    :cond_11
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/accounts/Account;Z)V

    goto :goto_4
.end method

.method private l()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    const-string v1, "/addAccount"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 655
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LZM;

    sget v1, Len;->google_account_needed_all_apps:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 656
    new-instance v0, Lhi;

    invoke-direct {v0, p0}, Lhi;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;)V

    .line 677
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    const-string v2, "com.google"

    invoke-interface {v1, v2, p0, v0}, LME;->a(Ljava/lang/String;Landroid/app/Activity;LMF;)V

    .line 678
    return-void
.end method

.method private m()V
    .registers 3

    .prologue
    .line 738
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b()Ljava/lang/String;

    move-result-object v0

    .line 739
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    invoke-interface {v1, v0}, LME;->a(Ljava/lang/String;)V

    .line 740
    return-void
.end method

.method private n()V
    .registers 5

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v0

    .line 763
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LMM;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    new-instance v3, Lhj;

    invoke-direct {v3, p0}, Lhj;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;)V

    invoke-interface {v1, v2, v0, v3}, LMM;->a(Landroid/widget/Button;[Landroid/accounts/Account;LMN;)V

    .line 773
    return-void
.end method


# virtual methods
.method protected a()LNa;
    .registers 2

    .prologue
    .line 782
    new-instance v0, LfA;

    invoke-direct {v0, p0}, LfA;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 813
    const-class v0, [Lii;

    if-ne p1, v0, :cond_9

    .line 815
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()[Lii;

    move-result-object v0

    .line 818
    :goto_8
    return-object v0

    :cond_9
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_8
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 797
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    if-eqz v0, :cond_b

    .line 798
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v0}, LkB;->b()Ljava/lang/String;

    move-result-object v0

    .line 802
    :goto_a
    return-object v0

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method

.method protected a(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 585
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 586
    if-eqz v0, :cond_c

    .line 587
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Ljava/lang/String;)V

    .line 588
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->n()V

    .line 590
    :cond_c
    return-void
.end method

.method public a(LmK;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 705
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    const-string v1, "homeScreen"

    invoke-virtual {v0, v1, p2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 708
    new-instance v1, LiR;

    invoke-direct {v1}, LiR;-><init>()V

    .line 709
    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v2}, LkB;->b()Ljava/lang/String;

    move-result-object v2

    .line 710
    iget-object v3, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LiG;

    invoke-interface {v3, v2}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v3

    invoke-virtual {v1, v3}, LiR;->a(LiE;)LiR;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LiG;

    invoke-interface {v4, p1, v2}, LiG;->b(LmK;Ljava/lang/String;)LiE;

    move-result-object v4

    invoke-virtual {v3, v4}, LiR;->a(LiE;)LiR;

    .line 712
    invoke-virtual {v1}, LiR;->a()LiQ;

    move-result-object v1

    .line 713
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 715
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Ljo;

    invoke-static {p0, v0, v2, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljo;)V

    .line 717
    return-void
.end method

.method protected a()Z
    .registers 2

    .prologue
    .line 829
    const/4 v0, 0x1

    return v0
.end method

.method b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 733
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    if-nez v0, :cond_7

    const-string v0, ""

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v0}, LkB;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 792
    const/4 v0, 0x0

    return v0
.end method

.method protected d()V
    .registers 2

    .prologue
    .line 366
    sget v0, Leh;->account_switcher:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    .line 367
    sget v0, Leh;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    .line 368
    return-void
.end method

.method public e()V
    .registers 4

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->k()V

    .line 321
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Lcom/google/android/apps/docs/app/BaseActivity;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lev;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Lfb;Ljava/lang/String;Lev;)V

    .line 324
    return-void
.end method

.method protected f()V
    .registers 7

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    const-string v1, "homeScreen"

    const-string v2, "switchAccount"

    const/4 v3, 0x0

    invoke-static {p0}, LZn;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v4, v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 597
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 598
    const-class v1, Lcom/google/android/apps/docs/app/AccountsActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 599
    const-string v1, "accountName"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v2}, LkB;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 601
    return-void
.end method

.method protected g()V
    .registers 6

    .prologue
    .line 748
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    if-nez v0, :cond_13

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    invoke-interface {v0}, LME;->a()Ljava/lang/String;

    move-result-object v0

    .line 750
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 752
    :try_start_10
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_13} :catch_14

    .line 758
    :cond_13
    :goto_13
    return-void

    .line 753
    :catch_14
    move-exception v1

    .line 754
    const-string v2, "HomeScreenActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not set account for ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]. Ignoring this user."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_13
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 606
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 607
    if-nez p1, :cond_27

    .line 608
    const/4 v0, -0x1

    if-ne p2, v0, :cond_27

    .line 609
    const-string v0, "accountName"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 610
    if-eqz v0, :cond_27

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    .line 611
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->finish()V

    .line 612
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LZl;

    invoke-interface {v1, p0, v0}, LZl;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 616
    :cond_27
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 535
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Leh;->account_switcher:I

    if-ne v0, v1, :cond_11

    .line 536
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->f()V

    .line 548
    :cond_10
    :goto_10
    return-void

    .line 539
    :cond_11
    const-string v0, "buttonMore"

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LoL;

    invoke-interface {v0}, LoL;->a()V

    goto :goto_10

    .line 544
    :cond_23
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lhk;

    if-eqz v0, :cond_10

    .line 545
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhk;

    .line 546
    iget-object v1, v0, Lhk;->a:LmK;

    iget-object v0, v0, Lhk;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LmK;Ljava/lang/String;)V

    goto :goto_10
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 10
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 246
    sget-object v0, Lkw;->a:Lkw;

    invoke-virtual {v0}, Lkw;->a()Ldx;

    .line 247
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 249
    sget v0, Lej;->home_screen_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->setContentView(I)V

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 253
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->e()Z

    move-result v1

    if-nez v1, :cond_27

    .line 254
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    const-string v2, "/homeScreen"

    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 257
    :cond_27
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Llf;

    invoke-interface {v0}, Llf;->d()V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-interface {v0, v1}, LMM;->a(LMZ;)V

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LMM;

    move-result-object v0

    invoke-interface {v0, v7, v7}, LMM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->g()V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Lo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a(Lo;)V

    .line 267
    if-nez p1, :cond_54

    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)V

    .line 274
    :cond_54
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    .line 276
    sget v1, Leh;->home_screen_list:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 277
    sget v2, Leh;->home_screen_table:I

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 278
    sget-object v2, Lfb;->b:Lfb;

    if-ne v0, v2, :cond_b1

    move v2, v3

    :goto_77
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 279
    sget-object v1, Lfb;->a:Lfb;

    if-ne v0, v1, :cond_b3

    :goto_7e
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 281
    sget-object v1, Lfb;->a:Lfb;

    if-ne v0, v1, :cond_b5

    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->i()V

    .line 283
    const-string v0, "buttonMore"

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 284
    sget v1, Lec;->quick_action_slide:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 285
    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 286
    new-instance v2, Lhn;

    invoke-direct {v2, p0, v7}, Lhn;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;Lhf;)V

    .line 287
    new-instance v3, LoE;

    invoke-direct {v3, v2, v5, v0, v1}, LoE;-><init>(LoH;Landroid/view/View;Landroid/view/View;Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LoL;

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    sget v1, Leg;->account_background_docs:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 302
    :goto_b0
    return-void

    :cond_b1
    move v2, v4

    .line 278
    goto :goto_77

    :cond_b3
    move v3, v4

    .line 279
    goto :goto_7e

    .line 295
    :cond_b5
    new-instance v0, Lhf;

    invoke-direct {v0, p0}, Lhf;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LoL;

    goto :goto_b0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 518
    sget v1, Lek;->menu_action_bar:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 519
    sget v1, Lek;->menu_home_page_a:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 520
    sget v1, Lek;->menu_home_page_b:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 521
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Laoz;

    invoke-interface {v1}, Laoz;->a()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lfb;->a:Lfb;

    if-ne v1, v2, :cond_27

    .line 522
    sget v1, Lek;->menu_home_page_c:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 527
    :cond_27
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LMM;

    move-result-object v0

    sget v1, Leh;->menu_search:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LMM;->a(Landroid/view/MenuItem;LMP;)V

    .line 529
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 470
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onDestroy()V

    .line 471
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 475
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 478
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 479
    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 480
    :cond_1a
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->setIntent(Landroid/content/Intent;)V

    .line 481
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)V

    .line 487
    :goto_20
    return-void

    .line 484
    :cond_21
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 485
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->finish()V

    goto :goto_20
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 552
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v1, p0}, LdL;->a(Landroid/content/Context;)V

    .line 553
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_switch_account:I

    if-ne v1, v2, :cond_12

    .line 554
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->f()V

    .line 576
    :goto_11
    return v0

    .line 556
    :cond_12
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_send_feedback:I

    if-ne v1, v2, :cond_25

    .line 557
    new-instance v1, LZP;

    const-string v2, "android_docs"

    invoke-direct {v1, p0, v2}, LZP;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 559
    invoke-virtual {v1}, LZP;->a()V

    goto :goto_11

    .line 561
    :cond_25
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_create_new_from_upload:I

    if-ne v1, v2, :cond_38

    .line 562
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v1}, LkB;->b()Ljava/lang/String;

    move-result-object v1

    .line 563
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_11

    .line 565
    :cond_38
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_settings:I

    if-ne v1, v2, :cond_4b

    .line 566
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_11

    .line 568
    :cond_4b
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_help:I

    if-ne v1, v2, :cond_57

    .line 569
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->j()V

    goto :goto_11

    .line 571
    :cond_57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_try_google_drive:I

    if-ne v1, v2, :cond_6d

    .line 572
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v1}, LkB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()Lo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/fragment/DocsUpgradedToDriveDialogFragment;->a(Ljava/lang/String;Lo;)V

    goto :goto_11

    .line 576
    :cond_6d
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_11
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 507
    const-string v0, "HomeScreenActivity"

    const-string v1, "in onPause"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 509
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->m()V

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    const-string v1, "/homeScreen"

    invoke-virtual {v0, p0, v1}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 511
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onPause()V

    .line 512
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 239
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->i:Z

    .line 241
    return-void
.end method

.method protected onResume()V
    .registers 4

    .prologue
    .line 491
    const-string v0, "HomeScreenActivity"

    const-string v1, "in onResume"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onResume()V

    .line 493
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->k()V

    .line 494
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->n()V

    .line 496
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->h()V

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Ldx;

    if-eqz v0, :cond_2a

    .line 498
    sget-object v0, Lkw;->a:Lkw;

    const-string v1, "prt"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Ldx;

    invoke-virtual {v0, v1, v2}, Lkw;->a(Ljava/lang/String;Ldx;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lky;

    sget-object v1, Lkw;->a:Lkw;

    invoke-virtual {v0, v1}, Lky;->a(Lkw;)V

    .line 500
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Ldx;

    .line 502
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    invoke-virtual {v0, p0}, LeQ;->a(Ljava/lang/Object;)V

    .line 503
    return-void
.end method

.method public onSearchRequested()Z
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-static {v0}, Lnl;->a(LkB;)Lnh;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Lnh;)V

    .line 726
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LkB;

    invoke-virtual {v1}, LkB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, v2, v1, v2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 728
    const/4 v0, 0x1

    return v0
.end method
