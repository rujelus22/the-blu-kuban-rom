.class public Lcom/google/android/apps/docs/app/NewMainProxyActivity;
.super Lcom/google/android/apps/docs/RoboFragmentActivity;
.source "NewMainProxyActivity.java"

# interfaces
.implements Lhz;


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Landroid/os/Bundle;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lfe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lhx;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ljava/lang/Class;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "StartingActivityOnLaunch"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;-><init>()V

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->b:Landroid/os/Handler;

    .line 104
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 501
    sget-object v0, LhA;->c:LhA;

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;LhA;)Landroid/content/Intent;

    move-result-object v0

    .line 505
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 512
    const-string v1, "wasTaskRoot"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 513
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;LhA;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 477
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 480
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 485
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 486
    const-string v1, "desiredApplicationMode"

    invoke-virtual {p2}, LhA;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    const-string v1, "launchingState"

    sget-object v2, LhE;->c:LhE;

    invoke-virtual {v2}, LhE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 488
    const-string v1, "launchingEvent"

    sget-object v2, LhD;->c:LhD;

    invoke-virtual {v2}, LhD;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 490
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;LkH;Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 323
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 324
    const-class v1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 325
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    const-string v1, "collectionResourceId"

    invoke-virtual {p2}, LkH;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;LmK;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 286
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, LhA;->c:LhA;

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;LhA;)Landroid/content/Intent;

    move-result-object v0

    .line 291
    const-string v1, "launchingAction"

    sget-object v2, LhC;->a:LhC;

    invoke-virtual {v2}, LhC;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    const-string v1, "mainFilter"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 293
    return-object v0
.end method

.method public static a(Landroid/content/Context;LkY;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 303
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v0, p1, LkY;->a:Ljava/lang/String;

    sget-object v1, LhA;->c:LhA;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;LhA;)Landroid/content/Intent;

    move-result-object v0

    .line 307
    const-string v1, "launchingAction"

    sget-object v2, LhC;->b:LhC;

    invoke-virtual {v2}, LhC;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    const-string v1, "resourceId"

    iget-object v2, p1, LkY;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;LhE;LhD;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 442
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 443
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 446
    const/high16 v0, 0x2400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 448
    const-string v0, "accountName"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    const-string v0, "launchingState"

    invoke-virtual {p2}, LhE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    const-string v0, "launchingEvent"

    invoke-virtual {p3}, LhD;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 452
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 453
    const-string v0, "proxyBundle"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 454
    const-string v3, "proxyBundle"

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 455
    const-string v0, "wasTaskRoot"

    const-string v3, "wasTaskRoot"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 458
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 459
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, LhV;

    invoke-direct {v1, p0}, LhV;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 465
    return-void
.end method

.method public static a(Landroid/content/Context;Lfb;Ljava/lang/String;Lev;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 410
    sget-object v0, Lfb;->b:Lfb;

    if-ne p1, v0, :cond_12

    const-string v0, "FirstTimeDrive"

    .line 414
    :goto_6
    :try_start_6
    invoke-interface {p3, p2}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v1

    .line 415
    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Let;->a(Ljava/lang/String;Z)V

    .line 416
    invoke-interface {p3, v1}, Lev;->a(Let;)V
    :try_end_11
    .catch Lew; {:try_start_6 .. :try_end_11} :catch_15

    .line 420
    :goto_11
    return-void

    .line 410
    :cond_12
    const-string v0, "FirstTimeDocs"

    goto :goto_6

    .line 417
    :catch_15
    move-exception v0

    .line 418
    const-string v0, "MainProxyActivity"

    const-string v1, "Failed to save account preference."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;LmK;LhB;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 391
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/tablet/TabletHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 392
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 394
    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 395
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    if-eqz p2, :cond_2f

    .line 397
    const-string v1, "mainFilter"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 398
    const-string v1, "docListTitle"

    invoke-virtual {p2}, LmK;->a()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    :cond_2f
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 401
    return-void
.end method

.method private static a(Landroid/content/Context;LkY;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 404
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Context;LkY;Z)Landroid/content/Intent;

    move-result-object v0

    .line 405
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 406
    return-void
.end method

.method public static a(Lcom/google/android/apps/docs/app/BaseActivity;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LhB;->a(Landroid/content/Intent;)LhB;

    move-result-object v1

    .line 360
    instance-of v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;

    if-eqz v0, :cond_44

    .line 361
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 363
    const-string v0, "launchingAction"

    sget-object v3, LhC;->c:LhC;

    invoke-static {v2, v0, v3}, LZz;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhC;

    .line 365
    sget-object v3, LhC;->a:LhC;

    invoke-virtual {v0, v3}, LhC;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 366
    const-string v0, "mainFilter"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LmK;

    .line 368
    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;LmK;LhB;)V

    .line 387
    :cond_2d
    :goto_2d
    return-void

    .line 370
    :cond_2e
    sget-object v3, LhC;->b:LhC;

    invoke-virtual {v0, v3}, LhC;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 371
    const-string v0, "resourceId"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-static {p1, v0}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v0

    .line 373
    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LkY;)V

    goto :goto_2d

    .line 378
    :cond_44
    sget-object v0, LhB;->b:LhB;

    if-ne v1, v0, :cond_4c

    .line 379
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2d

    .line 380
    :cond_4c
    sget-object v0, LhB;->c:LhB;

    if-ne v1, v0, :cond_5e

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 382
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()Lo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/DriveWelcomeDialogFragment;->a(Lo;)V

    goto :goto_2d

    .line 383
    :cond_5e
    sget-object v0, LhB;->d:LhB;

    if-ne v1, v0, :cond_2d

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()Lo;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/docs/fragment/DocsUpgradedToDriveDialogFragment;->a(Ljava/lang/String;Lo;)V

    goto :goto_2d
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->d()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->e()V

    return-void
.end method

.method private d()V
    .registers 7

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 147
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    const-string v2, "launchingState"

    sget-object v3, LhE;->c:LhE;

    invoke-static {v0, v2, v3}, LZz;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, LhE;

    .line 152
    const-string v2, "launchingEvent"

    sget-object v4, LhD;->c:LhD;

    invoke-static {v0, v2, v4}, LZz;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v4

    check-cast v4, LhD;

    .line 156
    sget-object v2, LhE;->c:LhE;

    if-ne v3, v2, :cond_46

    .line 157
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_2d

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 168
    :cond_2d
    :goto_2d
    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v5, "desiredApplicationMode"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_58

    sget-object v0, LhA;->b:LhA;

    :goto_39
    invoke-static {v2, v5, v0}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, LhA;

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Lhx;

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lhx;->a(Ljava/lang/String;LhA;LhE;LhD;Lhz;)V

    .line 174
    return-void

    .line 165
    :cond_46
    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v5, "proxyBundle"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_2d

    .line 168
    :cond_58
    sget-object v0, LhA;->a:LhA;

    goto :goto_39
.end method

.method private e()V
    .registers 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->b:Landroid/os/Handler;

    new-instance v1, LhT;

    invoke-direct {v1, p0}, LhT;-><init>(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 271
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 254
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/InvitationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 255
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 256
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v1, "proxyBundle"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 258
    const-string v1, "wasTaskRoot"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->isTaskRoot()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 260
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->startActivity(Landroid/content/Intent;)V

    .line 261
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->e()V

    .line 262
    return-void
.end method

.method public a(Ljava/lang/String;Lfb;LhB;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Lfe;

    invoke-virtual {v0}, Lfe;->a()V

    .line 189
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Ljava/lang/Class;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 193
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 196
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string v1, "triggerSync"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 200
    sget-object v1, LhB;->a:LhB;

    if-ne p3, v1, :cond_32

    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_37

    .line 201
    :cond_32
    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 204
    :cond_37
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->startActivity(Landroid/content/Intent;)V

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->e()V

    .line 206
    return-void
.end method

.method public a(Ljava/lang/String;LhB;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v1, "launchingAction"

    sget-object v2, LhC;->c:LhC;

    invoke-static {v0, v1, v2}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhC;

    .line 212
    sget-object v1, LhC;->a:LhC;

    invoke-virtual {v0, v1}, LhC;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v2, "mainFilter"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LmK;

    .line 215
    invoke-static {p0, p1, v1, p2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;LmK;LhB;)V

    .line 217
    :cond_21
    sget-object v1, LhC;->b:LhC;

    invoke-virtual {v0, v1}, LhC;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v1, "resourceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    invoke-static {p1, v0}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v0

    .line 220
    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LkY;)V

    .line 237
    :cond_38
    :goto_38
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->e()V

    .line 238
    return-void

    .line 222
    :cond_3c
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LhB;

    .line 224
    sget-object v1, LhB;->a:LhB;

    if-ne p2, v1, :cond_4d

    if-eqz v0, :cond_4d

    move-object p2, v0

    .line 228
    :cond_4d
    sget-object v0, LhB;->b:LhB;

    if-ne p2, v0, :cond_38

    .line 229
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_38
.end method

.method a()Z
    .registers 2

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public a_()V
    .registers 4

    .prologue
    .line 333
    new-instance v0, LhU;

    invoke-direct {v0, p0}, LhU;-><init>(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V

    .line 354
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LME;

    const-string v2, "com.google"

    invoke-interface {v1, v2, p0, v0}, LME;->a(Ljava/lang/String;Landroid/app/Activity;LMF;)V

    .line 355
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 242
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/CheckStatusActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 244
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const-string v1, "proxyBundle"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 246
    const-string v1, "wasTaskRoot"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->isTaskRoot()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 248
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->startActivity(Landroid/content/Intent;)V

    .line 249
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->e()V

    .line 250
    return-void
.end method

.method public isTaskRoot()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 178
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->isTaskRoot()Z

    move-result v1

    .line 179
    if-nez v1, :cond_13

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "wasTaskRoot"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_14

    :cond_13
    const/4 v0, 0x1

    :cond_14
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    const-string v0, "MainProxyActivity"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Lfe;

    invoke-virtual {v0}, Lfe;->a()V

    .line 124
    :cond_1a
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 125
    invoke-virtual {v1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_51

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_55

    const-string v0, "/launcherClickedDrive"

    .line 131
    :goto_34
    const-string v2, "MainProxyActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Launcher: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LeQ;

    invoke-virtual {v2, v0, v1}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 135
    :cond_51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->d()V

    .line 136
    return-void

    .line 129
    :cond_55
    const-string v0, "/launcherClickedDocs"

    goto :goto_34
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 141
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onDestroy()V

    .line 142
    return-void
.end method
