.class public Lcom/google/android/apps/docs/editors/dropdownmenu/FontDropDownMenu;
.super Lcom/google/android/apps/docs/editors/dropdownmenu/DropDownMenu;
.source "FontDropDownMenu.java"


# instance fields
.field private a:Luw;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/dropdownmenu/DropDownMenu;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 4
    .parameter

    .prologue
    .line 29
    sget v0, LsF;->font_dropdown:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/dropdownmenu/FontDropDownMenu;->a:Luw;

    invoke-virtual {v1, v0}, Luw;->a(Landroid/view/View;)V

    .line 31
    return-object v0
.end method

.method public a()Luw;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/dropdownmenu/FontDropDownMenu;->a:Luw;

    return-object v0
.end method

.method public a()V
    .registers 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/dropdownmenu/DropDownMenu;->a()V

    .line 37
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .registers 4
    .parameter

    .prologue
    .line 23
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/dropdownmenu/DropDownMenu;->a(Landroid/app/Activity;)V

    .line 24
    new-instance v0, Luw;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Luw;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/dropdownmenu/FontDropDownMenu;->a:Luw;

    .line 25
    return-void
.end method
