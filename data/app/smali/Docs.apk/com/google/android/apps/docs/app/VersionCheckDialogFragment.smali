.class public Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "VersionCheckDialogFragment.java"


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lin;
    .annotation runtime Laon;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "marketFlag"
    .end annotation
.end field

.field public m:I
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "tooOldTitle"
    .end annotation
.end field

.field public n:I
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "tooOldMessage"
    .end annotation
.end field

.field public o:I
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "tooOldClose"
    .end annotation
.end field

.field public p:I
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "tooOldUpgrade"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static a(Lo;)V
    .registers 3
    .parameter

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;-><init>()V

    .line 124
    const-string v1, "VersionCheck"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:LdL;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x2

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 72
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->m:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:Lfg;

    iget v3, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->n:I

    invoke-virtual {v2, v3}, Lfg;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->o:I

    new-instance v3, Lip;

    invoke-direct {v3, p0}, Lip;-><init>(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->p:I

    new-instance v3, Lio;

    invoke-direct {v3, p0}, Lio;-><init>(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 108
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 111
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:Lin;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, Lin;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()V

    .line 66
    :cond_12
    return-void
.end method
