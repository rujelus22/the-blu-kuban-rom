.class public Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;
.super Landroid/widget/LinearLayout;
.source "LinearBoxView.java"

# interfaces
.implements LzV;
.implements LzZ;


# instance fields
.field private a:I

.field private final a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<",
            "LzU;",
            "LzU;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LzX;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:I

    .line 34
    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->b:I

    .line 35
    new-instance v0, LzX;

    invoke-direct {v0, p0}, LzX;-><init>(LzZ;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LzX;

    .line 42
    new-instance v0, LDA;

    invoke-direct {v0}, LDA;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LDA;

    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:I

    .line 34
    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->b:I

    .line 35
    new-instance v0, LzX;

    invoke-direct {v0, p0}, LzX;-><init>(LzZ;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LzX;

    .line 51
    new-instance v0, LDA;

    invoke-direct {v0}, LDA;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LDA;

    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a()V

    .line 53
    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LDA;

    invoke-virtual {v0, p0}, LDA;->a(Ljava/lang/Object;)V

    .line 57
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->setClipChildren(Z)V

    .line 58
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->setClipToPadding(Z)V

    .line 59
    return-void
.end method


# virtual methods
.method public a()LDG;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDG",
            "<",
            "LzU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LDA;

    return-object v0
.end method

.method public bridge synthetic a()Landroid/view/View;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/view/ViewGroup;
    .registers 1

    .prologue
    .line 99
    return-object p0
.end method

.method public a()LzX;
    .registers 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LzX;

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter

    .prologue
    .line 139
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 140
    return-void
.end method

.method public a(LzU;)V
    .registers 3
    .parameter

    .prologue
    .line 128
    invoke-interface {p1}, LzU;->a()LDG;

    move-result-object v0

    invoke-virtual {v0}, LDG;->c()V

    .line 129
    invoke-interface {p1}, LzU;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->removeView(Landroid/view/View;)V

    .line 130
    return-void
.end method

.method public a(LzU;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_20

    .line 116
    instance-of v1, v0, LzU;

    invoke-static {v1}, Lagu;->b(Z)V

    .line 117
    check-cast v0, LzU;

    invoke-interface {v0}, LzU;->a()LDG;

    move-result-object v0

    .line 118
    invoke-interface {p1}, LzU;->a()LDG;

    move-result-object v1

    invoke-virtual {v0, v1}, LDG;->b(LDG;)V

    .line 119
    invoke-interface {p1}, LzU;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->addView(Landroid/view/View;I)V

    .line 124
    :goto_1f
    return-void

    .line 121
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LDA;

    invoke-interface {p1}, LzU;->a()LDG;

    move-result-object v1

    invoke-virtual {v0, v1}, LDA;->a(LDG;)V

    .line 122
    invoke-interface {p1}, LzU;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->addView(Landroid/view/View;)V

    goto :goto_1f
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LzX;

    invoke-virtual {v0, p1}, LzX;->a(Landroid/graphics/Canvas;)V

    .line 135
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LzX;

    invoke-virtual {v0, p1}, LzX;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 74
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 76
    :cond_b
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 80
    invoke-static {p0, p1}, LzW;->a(Landroid/view/View;Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 81
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 83
    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x4000

    .line 67
    iget v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->b:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 69
    return-void
.end method

.method public requestLayout()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 150
    .line 151
    :goto_1
    if-eqz p0, :cond_2f

    .line 152
    invoke-virtual {p0}, Landroid/view/View;->forceLayout()V

    .line 153
    invoke-virtual {p0, v4, v4}, Landroid/view/View;->measure(II)V

    .line 154
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 156
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 157
    instance-of v1, v0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;

    if-eqz v1, :cond_28

    .line 158
    check-cast v0, Landroid/view/View;

    :goto_26
    move-object p0, v0

    .line 167
    goto :goto_1

    .line 162
    :cond_28
    if-eqz v0, :cond_2d

    .line 163
    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 165
    :cond_2d
    const/4 v0, 0x0

    goto :goto_26

    .line 168
    :cond_2f
    return-void
.end method

.method public setHeight(I)V
    .registers 2
    .parameter

    .prologue
    .line 93
    iput p1, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->b:I

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->requestLayout()V

    .line 95
    return-void
.end method

.method public setIsClipped(Z)V
    .registers 3
    .parameter

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:LzX;

    invoke-virtual {v0, p1}, LzX;->a(Z)V

    .line 173
    return-void
.end method

.method public setMargin(IIII)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-static {p0, p1, p2, p3, p4}, LzW;->a(Landroid/view/View;IIII)V

    .line 105
    return-void
.end method

.method public setWidth(I)V
    .registers 2
    .parameter

    .prologue
    .line 87
    iput p1, p0, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->a:I

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/LinearBoxView;->requestLayout()V

    .line 89
    return-void
.end method
