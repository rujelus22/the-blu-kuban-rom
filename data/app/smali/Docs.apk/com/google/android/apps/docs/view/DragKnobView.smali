.class public Lcom/google/android/apps/docs/view/DragKnobView;
.super Landroid/widget/RelativeLayout;
.source "DragKnobView.java"


# instance fields
.field private a:LabE;

.field final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field private a:Landroid/view/MotionEvent;

.field private a:Landroid/widget/ImageView;

.field private a:Z

.field private b:Landroid/view/MotionEvent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/os/Handler;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Z

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/content/Context;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/os/Handler;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Z

    .line 71
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/content/Context;

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/os/Handler;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Z

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/content/Context;

    .line 67
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DragKnobView;)LabE;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:LabE;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DragKnobView;)Landroid/view/MotionEvent;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/view/MotionEvent;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DragKnobView;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/view/DragKnobView;)Landroid/view/MotionEvent;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->b:Landroid/view/MotionEvent;

    return-object v0
.end method


# virtual methods
.method public a(LabE;)V
    .registers 2
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:LabE;

    .line 76
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 131
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/content/Context;

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 81
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:LabE;

    if-eqz v0, :cond_11

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:LabE;

    invoke-interface {v0}, LabE;->a()V

    .line 85
    :cond_11
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/content/Context;

    invoke-static {v2}, LdY;->a(Landroid/content/Context;)V

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:LabE;

    if-nez v2, :cond_c

    .line 126
    :goto_b
    return v0

    .line 94
    :cond_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_29

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/view/MotionEvent;

    .line 96
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Z

    if-eqz v0, :cond_1b

    move v0, v1

    .line 97
    goto :goto_b

    .line 100
    :cond_1b
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Z

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/os/Handler;

    new-instance v2, LabC;

    invoke-direct {v2, p0}, LabC;-><init>(Lcom/google/android/apps/docs/view/DragKnobView;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 110
    goto :goto_b

    .line 113
    :cond_29
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_36

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:LabE;

    invoke-interface {v0, p1}, LabE;->a(Landroid/view/MotionEvent;)V

    move v0, v1

    .line 115
    goto :goto_b

    .line 118
    :cond_36
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->b:Landroid/view/MotionEvent;

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/os/Handler;

    new-instance v2, LabD;

    invoke-direct {v2, p0}, LabD;-><init>(Lcom/google/android/apps/docs/view/DragKnobView;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_b
.end method

.method public setKnobPictureContentDescription(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_e

    .line 136
    sget v0, Leh;->drag_knob_pic:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DragKnobView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/widget/ImageView;

    .line 139
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DragKnobView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method
