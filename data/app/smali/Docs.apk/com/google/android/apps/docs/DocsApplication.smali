.class public Lcom/google/android/apps/docs/DocsApplication;
.super Lcom/google/android/apps/docs/GuiceApplication;
.source "DocsApplication.java"


# static fields
.field private static volatile b:Z


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LKU;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LKZ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laov;",
            ">;"
        }
    .end annotation
.end field

.field public a:Llg;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/docs/GuiceApplication;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Z

    .line 111
    return-void
.end method

.method static a(Landroid/app/Application;)Ljava/util/Collection;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Laov;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    invoke-static {}, Laji;->a()Lajj;

    move-result-object v0

    new-instance v1, LdH;

    invoke-direct {v1, p0}, LdH;-><init>(Landroid/app/Application;)V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Labq;

    invoke-direct {v1}, Labq;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LdB;

    invoke-direct {v1}, LdB;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LYa;

    invoke-direct {v1}, LYa;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Lpd;

    invoke-direct {v1}, Lpd;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Lki;

    invoke-direct {v1}, Lki;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LUC;

    invoke-direct {v1}, LUC;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LMI;

    invoke-direct {v1}, LMI;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LrA;

    invoke-direct {v1}, LrA;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LeN;

    invoke-direct {v1}, LeN;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LVt;

    invoke-direct {v1}, LVt;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LNq;

    invoke-direct {v1, p0}, LNq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LUZ;

    invoke-direct {v1}, LUZ;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LPR;

    invoke-direct {v1}, LPR;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LkI;

    invoke-direct {v1}, LkI;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LYj;

    invoke-direct {v1}, LYj;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LPl;

    invoke-direct {v1}, LPl;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LSE;

    invoke-direct {v1}, LSE;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LlP;

    invoke-direct {v1}, LlP;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LLg;

    invoke-direct {v1}, LLg;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Ley;

    invoke-direct {v1}, Ley;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LnK;

    invoke-direct {v1}, LnK;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LacQ;

    invoke-direct {v1}, LacQ;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LPc;

    invoke-direct {v1}, LPc;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Lacw;

    invoke-direct {v1}, Lacw;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LlY;

    invoke-direct {v1}, LlY;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Ljk;

    invoke-direct {v1}, Ljk;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Lkd;

    invoke-direct {v1}, Lkd;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LnD;

    invoke-direct {v1}, LnD;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LSe;

    invoke-direct {v1}, LSe;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Lrk;

    invoke-direct {v1}, Lrk;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Lkx;

    invoke-direct {v1}, Lkx;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LSV;

    invoke-direct {v1}, LSV;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    invoke-virtual {v0}, Lajj;->a()Laji;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laov;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    if-eqz v0, :cond_7

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 228
    :goto_6
    return-object v0

    .line 222
    :cond_7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Ljava/util/Collection;

    move-result-object v0

    .line 224
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laov;

    .line 225
    iget-object v2, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/docs/DocsApplication;->a(Ljava/util/Map;Laov;)V

    goto :goto_16

    .line 228
    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    goto :goto_6
.end method

.method private a()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_9
    if-ge v0, v2, :cond_18

    aget-object v3, v1, v0

    .line 173
    const-string v4, "com.google.android.apps.docs"

    invoke-static {v3, v4, v5}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 174
    invoke-static {p0, v3, v5}, LXW;->a(Landroid/content/Context;Landroid/accounts/Account;Z)V

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 176
    :cond_18
    return-void
.end method

.method private b()V
    .registers 6

    .prologue
    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LKS;

    monitor-enter v1
    :try_end_3
    .catch LKV; {:try_start_0 .. :try_end_3} :catch_21
    .catch LLa; {:try_start_0 .. :try_end_3} :catch_2a

    .line 186
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LKU;

    iget-object v2, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LKS;

    invoke-interface {v0, v2}, LKU;->b(LKS;)V

    .line 187
    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v0

    .line 188
    iget-object v2, v0, Lfo;->c:Ljava/lang/String;

    if-eqz v2, :cond_1c

    .line 189
    iget-object v2, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LKZ;

    iget-object v0, v0, Lfo;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LKS;

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v4}, LKZ;->a(Ljava/lang/String;LKS;Z)V

    .line 191
    :cond_1c
    monitor-exit v1

    .line 197
    :goto_1d
    return-void

    .line 191
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    :try_start_20
    throw v0
    :try_end_21
    .catch LKV; {:try_start_20 .. :try_end_21} :catch_21
    .catch LLa; {:try_start_20 .. :try_end_21} :catch_2a

    .line 192
    :catch_21
    move-exception v0

    .line 193
    const-string v1, "DocsApplication"

    const-string v2, "Unable to load cached client flags, will use defaults until next sync."

    invoke-static {v1, v2, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1d

    .line 194
    :catch_2a
    move-exception v0

    .line 195
    const-string v1, "DocsApplication"

    const-string v2, "Unable to parse override client flags, will use cached flags only."

    invoke-static {v1, v2, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1d
.end method


# virtual methods
.method public a()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Laov;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-static {p0}, Lcom/google/android/apps/docs/DocsApplication;->a(Landroid/app/Application;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Laov;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 215
    return-void
.end method

.method protected a(Ljava/util/Map;Laov;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laov;",
            ">;",
            "Laov;",
            ")V"
        }
    .end annotation

    .prologue
    .line 277
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    return-void
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 208
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/GuiceApplication;->attachBaseContext(Landroid/content/Context;)V

    .line 209
    invoke-static {p1}, LaaF;->a(Landroid/content/Context;)V

    .line 210
    return-void
.end method

.method public onCreate()V
    .registers 4

    .prologue
    .line 124
    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v0

    iget-boolean v0, v0, Lfo;->a:Z

    if-nez v0, :cond_c

    .line 125
    const/4 v0, 0x5

    invoke-static {v0}, Laaz;->a(I)V

    .line 127
    :cond_c
    const-string v0, "DocsApplication"

    const-string v1, "in DocsApplication.onCreate"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceApplication;->onCreate()V

    .line 130
    sget-boolean v0, Lcom/google/android/apps/docs/DocsApplication;->b:Z

    if-eqz v0, :cond_1b

    .line 165
    :cond_1a
    :goto_1a
    return-void

    .line 134
    :cond_1b
    invoke-static {p0}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a(Landroid/content/Context;)V

    .line 135
    invoke-static {p0}, LVp;->a(Landroid/content/Context;)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Laoo;

    move-result-object v0

    invoke-static {v0, p0}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Laoo;

    move-result-object v0

    invoke-interface {v0, p0}, Laoo;->a(Ljava/lang/Object;)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Laoo;

    move-result-object v0

    const-class v1, Laav;

    invoke-interface {v0, v1}, Laoo;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 142
    monitor-enter p0

    .line 143
    const/4 v0, 0x1

    :try_start_3a
    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Z

    .line 144
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 145
    monitor-exit p0
    :try_end_40
    .catchall {:try_start_3a .. :try_end_40} :catchall_6f

    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->b()V

    .line 152
    new-instance v0, LdQ;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Purge obsolete data using "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Llg;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LdQ;-><init>(Lcom/google/android/apps/docs/DocsApplication;Ljava/lang/String;)V

    .line 159
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LaaJ;

    const/4 v1, 0x0

    const-string v2, "task_startup"

    invoke-interface {v0, v1, v2}, LaaJ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 163
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()V

    goto :goto_1a

    .line 145
    :catchall_6f
    move-exception v0

    :try_start_70
    monitor-exit p0
    :try_end_71
    .catchall {:try_start_70 .. :try_end_71} :catchall_6f

    throw v0
.end method

.method public onLowMemory()V
    .registers 2

    .prologue
    .line 201
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceApplication;->onLowMemory()V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Laoo;

    move-result-object v0

    invoke-static {v0, p0}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 204
    return-void
.end method

.method public onTerminate()V
    .registers 3

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Laoo;

    move-result-object v0

    const-class v1, LNK;

    invoke-interface {v0, v1}, Laoo;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNK;

    .line 311
    if-eqz v0, :cond_11

    .line 312
    invoke-interface {v0}, LNK;->c()V

    .line 314
    :cond_11
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceApplication;->onTerminate()V

    .line 315
    return-void
.end method
