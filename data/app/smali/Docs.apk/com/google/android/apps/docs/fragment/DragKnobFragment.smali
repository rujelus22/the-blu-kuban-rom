.class public Lcom/google/android/apps/docs/fragment/DragKnobFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "DragKnobFragment.java"

# interfaces
.implements LabE;


# instance fields
.field private a:F

.field private a:I

.field private a:J

.field public a:LLK;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/view/DragKnobView;

.field private a:Ljava/lang/Boolean;

.field private a:Z

.field private b:F

.field private b:I

.field private b:J

.field private b:Landroid/view/ViewGroup;

.field private b:Z

.field private c:F

.field private c:I

.field private c:Ljava/lang/String;

.field private d:F

.field private d:Landroid/view/View;

.field private d:Ljava/lang/String;

.field private e:F

.field private e:Landroid/view/View;

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 98
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Z

    .line 103
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Z

    .line 149
    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:I

    .line 155
    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    .line 188
    iput v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:I

    .line 189
    iput v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->m:I

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)F
    .registers 12
    .parameter

    .prologue
    .line 370
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b(Landroid/view/MotionEvent;)F

    move-result v0

    .line 371
    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:F

    sub-float v1, v0, v1

    .line 372
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    .line 373
    iget-wide v4, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:J

    sub-long v4, v2, v4

    .line 375
    float-to-double v6, v1

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-nez v6, :cond_1d

    const-wide/16 v6, 0x32

    cmp-long v6, v4, v6

    if-lez v6, :cond_25

    .line 376
    :cond_1d
    iput-wide v4, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:J

    .line 377
    iput v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->g:F

    .line 378
    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:F

    .line 379
    iput-wide v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:J

    .line 382
    :cond_25
    return v0
.end method

.method private a(Landroid/view/View;)F
    .registers 3
    .parameter

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Z

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    :goto_9
    return v0

    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    goto :goto_9
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/fragment/DragKnobFragment;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;-><init>()V

    .line 81
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 82
    invoke-static {v1, p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/os/Bundle;Z)V

    .line 83
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d(Landroid/os/Bundle;)V

    .line 84
    iput-object p1, v0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:Ljava/lang/String;

    .line 85
    iput-object p2, v0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Ljava/lang/String;

    .line 86
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/DragKnobFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:LdL;

    return-object v0
.end method

.method private a(FF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 309
    iput p1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:F

    .line 310
    iput p2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->f:F

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/View;F)V

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Landroid/view/View;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/View;F)V

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 314
    return-void
.end method

.method private static a(Landroid/os/Bundle;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 208
    const-string v0, "isBelowCovered"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 209
    return-void
.end method

.method private a(Landroid/view/View;F)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 479
    float-to-double v0, p2

    const-wide/high16 v2, 0x3fe0

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 480
    iget-boolean v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Z

    if-eqz v1, :cond_16

    .line 481
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 485
    :goto_15
    return-void

    .line 483
    :cond_16
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_15
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/DragKnobFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->r()V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    const/high16 v5, 0x4170

    .line 401
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 402
    const-wide/16 v3, 0xfa

    cmp-long v1, v1, v3

    if-lez v1, :cond_13

    .line 409
    :cond_12
    :goto_12
    return v0

    .line 406
    :cond_13
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:F

    sub-float/2addr v1, v2

    .line 407
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:F

    sub-float/2addr v2, v3

    .line 409
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_12

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_12

    const/4 v0, 0x1

    goto :goto_12
.end method

.method private b(Landroid/view/MotionEvent;)F
    .registers 3
    .parameter

    .prologue
    .line 471
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Z

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    :goto_8
    return v0

    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    goto :goto_8
.end method

.method private b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 444
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    .line 445
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->q()V

    .line 446
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    const-string v1, "DragKnobFragment"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Labr;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    .line 217
    return-void
.end method

.method private q()V
    .registers 5

    .prologue
    .line 291
    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    .line 292
    const/4 v0, 0x0

    .line 293
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 294
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 295
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Lcom/google/android/apps/docs/view/DragKnobView;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/docs/view/DragKnobView;->setKnobPictureContentDescription(Ljava/lang/CharSequence;)V

    .line 303
    :goto_18
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:LLK;

    invoke-interface {v2}, LLK;->a()V

    .line 305
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(FF)V

    .line 306
    return-void

    .line 297
    :cond_21
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 298
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Lcom/google/android/apps/docs/view/DragKnobView;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/docs/view/DragKnobView;->setKnobPictureContentDescription(Ljava/lang/CharSequence;)V

    .line 299
    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    .line 300
    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    goto :goto_18
.end method

.method private r()V
    .registers 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_9
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b(Z)V

    .line 415
    return-void

    .line 414
    :cond_d
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 325
    iput-object p2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    .line 326
    sget v0, Lej;->drag_knob:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DragKnobView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Lcom/google/android/apps/docs/view/DragKnobView;

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Lcom/google/android/apps/docs/view/DragKnobView;

    sget v1, Leh;->drag_knob_pic:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DragKnobView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 328
    new-instance v1, LLJ;

    invoke-direct {v1, p0}, LLJ;-><init>(Lcom/google/android/apps/docs/fragment/DragKnobFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Lcom/google/android/apps/docs/view/DragKnobView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/view/DragKnobView;->a(LabE;)V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Lcom/google/android/apps/docs/view/DragKnobView;

    return-object v0
.end method

.method public a()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1d

    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->m:I

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    if-ne v0, v3, :cond_1a

    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:I

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    if-eq v0, v3, :cond_1d

    .line 223
    :cond_1a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Landroid/view/View;

    .line 226
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_54

    .line 227
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Z

    if-nez v0, :cond_4c

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/View;)F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 229
    int-to-float v1, v0

    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4b

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4b

    .line 230
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->q()V

    .line 288
    :cond_4b
    :goto_4b
    return-void

    .line 233
    :cond_4c
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:F

    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->f:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(FF)V

    goto :goto_4b

    .line 239
    :cond_54
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_5f

    .line 242
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->p()V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4b

    .line 248
    :cond_5f
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->m:I

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:I

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 252
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v3, -0x2

    if-ne v0, v3, :cond_e9

    move v0, v1

    :goto_7d
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Z

    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lef;->drag_knob_width_visible_size:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:I

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:Landroid/view/View;

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Landroid/view/View;

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 266
    iget-boolean v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Z

    if-eqz v3, :cond_eb

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_a7
    const/4 v3, -0x1

    if-ne v0, v3, :cond_ee

    .line 269
    :goto_aa
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Z

    if-eqz v0, :cond_f0

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_b4
    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    .line 270
    if-eqz v1, :cond_bf

    .line 273
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    .line 280
    :cond_bf
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lef;->drag_knob_extra_touch_zone_per_side:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 282
    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    .line 283
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->k:F

    .line 284
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->k:F

    add-float/2addr v0, v1

    const/high16 v1, 0x3f00

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->l:F

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Lcom/google/android/apps/docs/view/DragKnobView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/view/DragKnobView;->setVisibility(I)V

    .line 287
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->q()V

    goto/16 :goto_4b

    :cond_e9
    move v0, v2

    .line 252
    goto :goto_7d

    .line 266
    :cond_eb
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_a7

    :cond_ee
    move v1, v2

    goto :goto_aa

    .line 269
    :cond_f0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_b4
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 343
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_1e

    .line 345
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    .line 346
    if-eqz p1, :cond_1f

    .line 347
    :goto_10
    if-eqz p1, :cond_1e

    .line 348
    const-string v0, "isBelowCovered"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    .line 351
    :cond_1e
    return-void

    .line 346
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_10
.end method

.method public a(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter

    .prologue
    .line 355
    const-string v0, "DragKnobFragment"

    const-string v1, "onStartDrag"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Z

    .line 357
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:F

    .line 358
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:F

    .line 359
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:F

    .line 360
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/View;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->h:F

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/View;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->i:F

    .line 362
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:J

    .line 363
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->g:F

    .line 364
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:F

    iput v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:F

    .line 365
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:J

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 367
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_b

    .line 450
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b(Z)V

    .line 452
    :cond_b
    return-void
.end method

.method public b(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter

    .prologue
    .line 387
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:F

    sub-float v1, v0, v1

    .line 388
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->h:F

    add-float/2addr v0, v1

    .line 389
    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_1e

    .line 390
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->j:F

    .line 391
    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->h:F

    sub-float v1, v0, v1

    .line 397
    :cond_17
    :goto_17
    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->i:F

    add-float/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(FF)V

    .line 398
    return-void

    .line 392
    :cond_1e
    iget v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->k:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_17

    .line 393
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->k:F

    .line 394
    iget v1, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->h:F

    sub-float v1, v0, v1

    goto :goto_17
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 318
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->c(Landroid/os/Bundle;)V

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/os/Bundle;Z)V

    .line 320
    return-void
.end method

.method public c(Landroid/view/MotionEvent;)V
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 419
    const-string v0, "DragKnobFragment"

    const-string v3, "onStopDrag"

    invoke-static {v0, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iput-boolean v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Z

    .line 422
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 424
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->r()V

    .line 441
    :goto_14
    return-void

    .line 428
    :cond_15
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iget v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->c:F

    sub-float/2addr v0, v3

    .line 429
    iget v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->h:F

    add-float/2addr v0, v3

    .line 430
    iget v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->l:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4b

    move v0, v1

    .line 432
    :goto_26
    iget-wide v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_47

    .line 433
    iget v3, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->g:F

    iget-wide v4, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    .line 434
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const v5, 0x3e99999a

    cmpl-float v4, v4, v5

    if-lez v4, :cond_47

    .line 435
    float-to-double v3, v3

    const-wide/16 v5, 0x0

    cmpg-double v0, v3, v5

    if-gez v0, :cond_4d

    :goto_46
    move v0, v1

    .line 440
    :cond_47
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b(Z)V

    goto :goto_14

    :cond_4b
    move v0, v2

    .line 430
    goto :goto_26

    :cond_4d
    move v1, v2

    .line 435
    goto :goto_46
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public j_()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 463
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->j_()V

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 466
    iput-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->d:Landroid/view/View;

    .line 467
    iput-object v2, p0, Lcom/google/android/apps/docs/fragment/DragKnobFragment;->b:Landroid/view/ViewGroup;

    .line 468
    return-void
.end method
