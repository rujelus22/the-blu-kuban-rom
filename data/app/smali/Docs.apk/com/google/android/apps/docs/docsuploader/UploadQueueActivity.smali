.class public Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "UploadQueueActivity.java"

# interfaces
.implements Lsf;


# instance fields
.field final a:LZA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZA",
            "<",
            "Lcom/google/android/apps/docs/docsuploader/UploadQueueService;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/content/ServiceConnection;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/ListView;

.field public a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

.field private a:Lcom/google/android/apps/docs/view/TitleBar;

.field private a:LrZ;

.field public a:Lse;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 70
    new-instance v0, Lsa;

    invoke-direct {v0, p0}, Lsa;-><init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:LZA;

    .line 96
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 188
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 190
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)Landroid/widget/ListView;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)LrZ;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:LrZ;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;LrZ;)LrZ;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:LrZ;

    return-object p1
.end method

.method private h()V
    .registers 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:LZA;

    invoke-static {p0, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Landroid/content/Context;LZA;)Landroid/content/ServiceConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Landroid/content/ServiceConnection;

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Landroid/content/ServiceConnection;

    if-nez v0, :cond_16

    .line 197
    sget v0, Len;->upload_queue_failed_to_start:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 201
    :cond_16
    return-void
.end method


# virtual methods
.method public f()V
    .registers 2

    .prologue
    .line 175
    new-instance v0, Lsc;

    invoke-direct {v0, p0}, Lsc;-><init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 181
    return-void
.end method

.method public g()V
    .registers 1

    .prologue
    .line 185
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 148
    sget v0, Lej;->upload_queue_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->setContentView(I)V

    .line 150
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Landroid/widget/ListView;

    .line 151
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Landroid/view/View;

    .line 152
    sget v0, Leh;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a()LMM;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-interface {v0, v1}, LMM;->a(LMZ;)V

    .line 157
    sget v1, Len;->upload_queue_title_bar:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LMM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "accountName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_4a

    .line 163
    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, LMM;->a(Landroid/widget/Button;Ljava/lang/String;)V

    .line 165
    :cond_4a
    return-void
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 206
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onPause()V

    .line 207
    return-void
.end method

.method protected onResume()V
    .registers 1

    .prologue
    .line 169
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onResume()V

    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->h()V

    .line 171
    return-void
.end method
