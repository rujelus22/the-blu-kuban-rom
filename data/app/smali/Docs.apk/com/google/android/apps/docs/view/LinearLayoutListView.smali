.class public Lcom/google/android/apps/docs/view/LinearLayoutListView;
.super Landroid/widget/FrameLayout;
.source "LinearLayoutListView.java"


# instance fields
.field public a:I

.field private a:LabV;

.field private a:LabW;

.field a:LabX;

.field private a:Lacd;

.field private final a:Landroid/database/DataSetObserver;

.field a:Landroid/os/Handler;

.field public a:Landroid/view/ViewGroup;

.field public a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/ListAdapter;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field public b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 300
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 301
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 307
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 308
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 314
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 234
    new-instance v0, LabP;

    invoke-direct {v0, p0}, LabP;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/database/DataSetObserver;

    .line 261
    iput v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    .line 268
    iput v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    .line 284
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    .line 287
    sget-object v0, LabX;->a:LabX;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    .line 290
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/os/Handler;

    .line 292
    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 294
    iput v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->c:I

    .line 317
    sget-object v0, Lep;->LinearLayoutListView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 319
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 321
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setChildWidth(I)V

    .line 323
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 325
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->g()V

    .line 326
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)LabV;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabV;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)Landroid/widget/ListAdapter;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method private a()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 455
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b()V

    .line 456
    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    .line 457
    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    .line 458
    return-void
.end method

.method private a(II)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 545
    if-ltz p1, :cond_66

    move v0, v1

    :goto_5
    invoke-static {v0}, Lagu;->a(Z)V

    .line 546
    if-gt p1, p2, :cond_68

    move v0, v1

    :goto_b
    invoke-static {v0}, Lagu;->a(Z)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    if-lez v0, :cond_6a

    move v0, v1

    :goto_18
    invoke-static {v0}, Lagu;->b(Z)V

    .line 549
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    if-lez v0, :cond_6c

    :goto_1f
    invoke-static {v1}, Lagu;->b(Z)V

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, LabX;->a(LabX;II)I

    move-result v1

    .line 552
    div-int v0, p1, v1

    .line 553
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v3, p2, -0x1

    div-int v1, v3, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 554
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-direct {v3, v1, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move v1, v0

    .line 555
    :goto_48
    if-gt v1, v2, :cond_6e

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 557
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-nez v4, :cond_62

    .line 558
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(I)Landroid/view/View;

    move-result-object v4

    .line 559
    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 560
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 555
    :cond_62
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_48

    :cond_66
    move v0, v2

    .line 545
    goto :goto_5

    :cond_68
    move v0, v2

    .line 546
    goto :goto_b

    :cond_6a
    move v0, v2

    .line 548
    goto :goto_18

    :cond_6c
    move v1, v2

    .line 549
    goto :goto_1f

    .line 563
    :cond_6e
    return-void
.end method

.method private a(ILandroid/view/ViewGroup;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 673
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ne v2, v0, :cond_1f

    :goto_8
    invoke-static {v0}, Lagu;->b(Z)V

    .line 674
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 679
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 680
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 682
    return-void

    :cond_1f
    move v0, v1

    .line 673
    goto :goto_8
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)Z
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    return v0
.end method

.method private b(I)Landroid/view/View;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 633
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_40

    move v0, v1

    :goto_a
    invoke-static {v0}, Lagu;->b(Z)V

    .line 635
    const/4 v0, 0x0

    .line 637
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_42

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 647
    :cond_26
    :goto_26
    if-eqz v0, :cond_2b

    .line 648
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 650
    :cond_2b
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 651
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 652
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 653
    new-instance v1, LabS;

    invoke-direct {v1, p0, p1}, LabS;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 663
    return-object v0

    .line 633
    :cond_40
    const/4 v0, 0x0

    goto :goto_a

    .line 639
    :cond_42
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/16 v3, 0x32

    if-lt v2, v3, :cond_26

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 644
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_26
.end method

.method private b()V
    .registers 2

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->f()V

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 466
    return-void
.end method

.method private b(I)V
    .registers 7
    .parameter

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabW;

    if-eqz v0, :cond_3b

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, LabX;->a(LabX;II)I

    move-result v0

    .line 586
    div-int v1, p1, v0

    .line 587
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    invoke-static {v2, v3, v4}, LabX;->a(LabX;II)I

    move-result v2

    .line 588
    add-int/2addr v2, p1

    .line 589
    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v2, v2, -0x1

    div-int v0, v2, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 590
    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 591
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabW;

    invoke-interface {v2, v1, v0}, LabW;->a(II)V

    .line 593
    :cond_3b
    return-void
.end method

.method private b(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, LabX;->a(LabX;II)I

    move-result v4

    .line 602
    div-int v3, p1, v4

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-static {v0, v1, v2}, LabX;->a(LabX;II)I

    move-result v0

    .line 604
    add-int v5, p2, v0

    .line 605
    add-int/2addr v0, p1

    .line 606
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, v4

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 609
    if-le p2, p1, :cond_4c

    .line 610
    div-int v0, p2, v4

    .line 611
    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v2, v3

    .line 612
    :goto_3b
    if-gt v2, v6, :cond_4c

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 614
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(ILandroid/view/ViewGroup;)V

    .line 612
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3b

    .line 619
    :cond_4c
    if-ge p2, p1, :cond_74

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v2, v5, -0x1

    div-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 621
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 622
    :goto_63
    if-lt v1, v2, :cond_74

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 624
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(ILandroid/view/ViewGroup;)V

    .line 622
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_63

    .line 627
    :cond_74
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->c()V

    return-void
.end method

.method private c()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    iput-boolean v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 475
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_11

    .line 485
    :goto_10
    return-void

    .line 479
    :cond_11
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->d()V

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v2

    invoke-static {v0, v1, v2}, LabX;->a(LabX;II)I

    move-result v0

    .line 482
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 483
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-static {v1, v2, v3}, LabX;->a(LabX;II)I

    move-result v1

    .line 484
    add-int/2addr v1, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(II)V

    goto :goto_10
.end method

.method private d()V
    .registers 6

    .prologue
    const/4 v1, -0x1

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    if-eq v0, v1, :cond_e

    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    if-ne v0, v1, :cond_11

    .line 494
    :cond_e
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->e()V

    .line 497
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 498
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-direct {v2, v0, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 499
    const/4 v0, 0x0

    :goto_21
    if-ge v0, v1, :cond_37

    .line 500
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 501
    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 502
    iget-object v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 499
    add-int/lit8 v0, v0, 0x1

    goto :goto_21

    .line 504
    :cond_37
    return-void
.end method

.method private e()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_5e

    const/4 v0, 0x1

    :goto_10
    invoke-static {v0}, Lagu;->b(Z)V

    .line 513
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(I)Landroid/view/View;

    move-result-object v2

    .line 515
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 516
    if-nez v0, :cond_22

    .line 517
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 519
    :cond_22
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 521
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->c:I

    .line 522
    if-ne v0, v3, :cond_2d

    .line 523
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getWidth()I

    move-result v0

    .line 526
    :cond_2d
    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 527
    const/high16 v3, -0x8000

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getHeight()I

    move-result v3

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/view/View;->measure(II)V

    .line 530
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    .line 531
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    return-void

    :cond_5e
    move v0, v1

    .line 511
    goto :goto_10
.end method

.method private f()V
    .registers 4

    .prologue
    .line 688
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1f

    .line 689
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 690
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1b

    .line 691
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(ILandroid/view/ViewGroup;)V

    .line 688
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 694
    :cond_1f
    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LabX;->a(LabX;Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Lacd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Lacd;

    .line 701
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Lacd;

    invoke-interface {v0}, Lacd;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    .line 702
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    invoke-static {v1}, LabX;->c(LabX;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 704
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->addView(Landroid/view/View;)V

    .line 706
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .registers 4
    .parameter

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 443
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_12

    .line 444
    :cond_10
    const/4 v0, 0x0

    .line 448
    :goto_11
    return-object v0

    .line 447
    :cond_12
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_11
.end method

.method public a(I)V
    .registers 7
    .parameter

    .prologue
    .line 414
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    if-lez v0, :cond_8

    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    if-gtz v0, :cond_9

    .line 434
    :cond_8
    :goto_8
    return-void

    .line 420
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, LabX;->a(LabX;II)I

    move-result v0

    .line 421
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-static {v1, v2, v3}, LabX;->a(LabX;II)I

    move-result v1

    .line 422
    mul-int v2, v0, p1

    .line 423
    add-int/lit8 v3, p1, 0x1

    mul-int/2addr v0, v3

    sub-int/2addr v0, v1

    .line 426
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v4

    invoke-static {v1, v3, v4}, LabX;->a(LabX;II)I

    move-result v1

    .line 427
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 428
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 431
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    invoke-static {v1}, LabX;->a(LabX;)I

    move-result v1

    mul-int/2addr v1, v0

    .line 432
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    invoke-static {v2}, LabX;->b(LabX;)I

    move-result v2

    mul-int/2addr v0, v2

    .line 433
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Lacd;

    invoke-interface {v2, v1, v0}, Lacd;->a(II)V

    goto :goto_8
.end method

.method public a(IIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 569
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    if-eqz v0, :cond_6

    .line 581
    :goto_5
    return-void

    .line 573
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    invoke-static {v0, p1, p2}, LabX;->a(LabX;II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 574
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    invoke-static {v1, p3, p4}, LabX;->a(LabX;II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 575
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getHeight()I

    move-result v4

    invoke-static {v2, v3, v4}, LabX;->a(LabX;II)I

    move-result v2

    .line 577
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(II)V

    .line 578
    add-int v1, v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(II)V

    .line 580
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(I)V

    goto :goto_5
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 332
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    if-ne v0, v1, :cond_34

    move v0, v1

    :goto_b
    const-string v2, "Adapters with multiple view types not supported."

    invoke-static {v0, v2}, Lagu;->a(ZLjava/lang/Object;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1b

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 340
    :cond_1b
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 342
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a()V

    .line 343
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/os/Handler;

    new-instance v1, LabQ;

    invoke-direct {v1, p0}, LabQ;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 352
    return-void

    .line 333
    :cond_34
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public setChildWidth(I)V
    .registers 2
    .parameter

    .prologue
    .line 388
    iput p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->c:I

    .line 389
    return-void
.end method

.method public setOnItemClickListener(LabV;)V
    .registers 2
    .parameter

    .prologue
    .line 397
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabV;

    .line 398
    return-void
.end method

.method public setOnScrollListener(LabW;)V
    .registers 2
    .parameter

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabW;

    .line 408
    return-void
.end method

.method public setOrientation(LabX;)V
    .registers 4
    .parameter

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b()V

    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->removeAllViews()V

    .line 367
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:LabX;

    .line 368
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->g()V

    .line 372
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/os/Handler;

    new-instance v1, LabR;

    invoke-direct {v1, p0}, LabR;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 381
    return-void
.end method
