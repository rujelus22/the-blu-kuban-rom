.class public Lcom/google/android/apps/docs/app/AccountsActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "AccountsActivity.java"


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/widget/Button;

.field private a:Landroid/widget/ListView;

.field private a:Lcom/google/android/apps/docs/view/TitleBar;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:[Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/AccountsActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->g()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/AccountsActivity;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/AccountsActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 185
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->setResult(ILandroid/content/Intent;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->finish()V

    .line 189
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/AccountsActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->f()V

    return-void
.end method

.method private f()V
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:[Landroid/accounts/Account;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:[Landroid/accounts/Account;

    array-length v0, v0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:[Landroid/accounts/Account;

    array-length v0, v0

    new-array v4, v0, [Ljava/lang/String;

    .line 135
    iget-object v5, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:[Landroid/accounts/Account;

    array-length v6, v5

    move v0, v1

    move v2, v1

    :goto_16
    if-ge v0, v6, :cond_24

    aget-object v7, v5, v0

    .line 136
    add-int/lit8 v3, v2, 0x1

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v7, v4, v2

    .line 135
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_16

    .line 139
    :cond_24
    new-instance v0, Landroid/widget/ArrayAdapter;

    sget v2, Lej;->accounts_list_row:I

    invoke-direct {v0, p0, v2, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/ListView;

    new-instance v2, LeY;

    invoke-direct {v2, p0, v4}, LeY;-><init>(Lcom/google/android/apps/docs/app/AccountsActivity;[Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/Button;

    new-instance v2, LeZ;

    invoke-direct {v2, p0}, LeZ;-><init>(Lcom/google/android/apps/docs/app/AccountsActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lel;->accounts_title_all_apps:I

    array-length v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 159
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    array-length v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a()LMM;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LMM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LME;

    const-string v1, "com.google"

    new-instance v2, Lfa;

    invoke-direct {v2, p0}, Lfa;-><init>(Lcom/google/android/apps/docs/app/AccountsActivity;)V

    invoke-interface {v0, v1, p0, v2}, LME;->a(Ljava/lang/String;Landroid/app/Activity;LMF;)V

    .line 182
    return-void
.end method


# virtual methods
.method protected a()Z
    .registers 2

    .prologue
    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method protected d()V
    .registers 2

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()V

    .line 93
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/ListView;

    .line 94
    sget v0, Leh;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    .line 95
    sget v0, Leh;->new_account_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/Button;

    .line 96
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LeQ;

    invoke-virtual {v1}, LeQ;->a()V

    .line 64
    iget-object v1, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LeQ;

    const-string v2, "/pickAccount"

    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 66
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3f

    .line 67
    const-string v1, "AccountsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->finish()V

    .line 87
    :goto_3e
    return-void

    .line 72
    :cond_3f
    sget v0, Lej;->accounts_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->setContentView(I)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-interface {v0, v1}, LMM;->a(LMZ;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    sget-object v1, Lfb;->b:Lfb;

    invoke-virtual {v0, v1}, Lfb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Leg;->home_bg_plain:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 78
    invoke-static {v0}, LZu;->a(Landroid/graphics/Bitmap;)LZu;

    move-result-object v0

    invoke-virtual {v0}, LZu;->a()LZu;

    move-result-object v0

    invoke-virtual {v0}, LZu;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 79
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 80
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setDither(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(Landroid/util/DisplayMetrics;)V

    .line 82
    sget v0, Leh;->root_layout:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3e

    .line 84
    :cond_91
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/Button;

    sget v1, Leg;->account_background_docs:I

    invoke-static {v0, v1}, Labr;->a(Landroid/view/View;I)V

    goto :goto_3e
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 109
    sget v1, Lek;->menu_accounts_activity:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 110
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LdL;

    invoke-interface {v1, p0}, LdL;->a(Landroid/content/Context;)V

    .line 116
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_account_settings:I

    if-ne v1, v2, :cond_14

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LME;

    invoke-interface {v1, p0}, LME;->a(Landroid/content/Context;)V

    .line 125
    :goto_13
    return v0

    .line 119
    :cond_14
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_send_feedback:I

    if-ne v1, v2, :cond_27

    .line 120
    new-instance v1, LZP;

    const-string v2, "android_docs"

    invoke-direct {v1, p0, v2}, LZP;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 122
    invoke-virtual {v1}, LZP;->a()V

    goto :goto_13

    .line 125
    :cond_27
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_13
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 100
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onResume()V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->f()V

    .line 103
    return-void
.end method
