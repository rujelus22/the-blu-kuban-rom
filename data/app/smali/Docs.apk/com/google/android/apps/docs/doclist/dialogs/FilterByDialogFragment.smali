.class public Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "FilterByDialogFragment.java"


# instance fields
.field public a:Loa;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 34
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static a(Lo;LmK;Laji;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lo;",
            "LmK;",
            "Laji",
            "<",
            "LmK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;-><init>()V

    .line 82
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 83
    const-string v2, "currentFilter"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 84
    const-string v2, "availableFilters"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->d(Landroid/os/Bundle;)V

    .line 86
    const-string v1, "FilterByDialogFragment"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 87
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 9
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 46
    sget v0, Len;->menu_filter_by:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 50
    const-string v0, "availableFilters"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 52
    const-string v3, "currentFilter"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LmK;

    .line 53
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 55
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 56
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_36
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_54

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LmK;

    .line 57
    iget-object v6, p0, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a:Lfg;

    invoke-virtual {v1}, LmK;->a()I

    move-result v1

    invoke-virtual {v6, v1}, Lfg;->a(I)I

    move-result v1

    .line 58
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_36

    .line 62
    :cond_54
    new-instance v1, LnZ;

    invoke-direct {v1, p0, v0}, LnZ;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;Ljava/util/List;)V

    .line 70
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
