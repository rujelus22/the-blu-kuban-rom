.class public Lcom/google/android/apps/docs/docsuploader/UploadQueueService;
.super Lcom/google/android/apps/docs/GuiceService;
.source "UploadQueueService.java"


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaB;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Landroid/app/NotificationManager;
    .annotation runtime Laon;
    .end annotation
.end field

.field final a:Landroid/os/IBinder;

.field public a:Lfe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsf;",
            ">;"
        }
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LrR;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LrZ;

.field public a:Lrx;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lsi;

.field a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/GuiceService;-><init>()V

    .line 53
    new-instance v0, LrZ;

    invoke-direct {v0}, LrZ;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    .line 54
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Ljava/util/List;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Z

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lsi;

    .line 326
    new-instance v0, Lsh;

    invoke-direct {v0, p0}, Lsh;-><init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Landroid/os/IBinder;

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 242
    const/high16 v0, 0x1000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 243
    const/high16 v0, 0x80

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 244
    const-string v0, "accountName"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_2d

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_2d

    .line 246
    sget-object v1, Lcom/google/android/apps/docs/providers/DocListProvider;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 248
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    :cond_2d
    const/4 v0, 0x0

    const/high16 v1, 0x800

    invoke-static {p1, v0, p2, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 252
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lfe;

    invoke-virtual {v0, p2}, Lfe;->a(Ljava/lang/String;)Lfb;

    move-result-object v0

    .line 225
    if-nez p3, :cond_16

    .line 226
    invoke-virtual {v0}, Lfb;->a()LmK;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;LmK;)Landroid/content/Intent;

    move-result-object v0

    .line 233
    :goto_10
    const/high16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 235
    return-object v0

    .line 229
    :cond_16
    invoke-static {p2, p3}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v0

    .line 230
    invoke-static {p1, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LkY;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_10
.end method

.method public static a(Landroid/content/Context;LZA;)Landroid/content/ServiceConnection;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LZA",
            "<",
            "Lcom/google/android/apps/docs/docsuploader/UploadQueueService;",
            ">;)",
            "Landroid/content/ServiceConnection;"
        }
    .end annotation

    .prologue
    .line 335
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 336
    new-instance v0, Lsg;

    invoke-direct {v0, p1}, Lsg;-><init>(LZA;)V

    .line 354
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 355
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 358
    :goto_16
    return-object v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LdL;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Llf;

    invoke-interface {v0, p1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_d

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LME;

    invoke-interface {v1, v0}, LME;->a(LkB;)V

    .line 207
    :cond_d
    return-void
.end method


# virtual methods
.method public declared-synchronized a()Ljava/lang/Thread;
    .registers 2

    .prologue
    .line 210
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lsi;

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    invoke-virtual {v0}, LrZ;->a()Z

    move-result v0

    if-nez v0, :cond_19

    .line 211
    new-instance v0, Lsi;

    invoke-direct {v0, p0}, Lsi;-><init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lsi;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lsi;

    invoke-virtual {v0}, Lsi;->start()V

    .line 214
    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lsi;
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_1d

    monitor-exit p0

    return-object v0

    .line 210
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()LrZ;
    .registers 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    return-object v0
.end method

.method public a()V
    .registers 13

    .prologue
    const/4 v11, 0x0

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    invoke-virtual {v0}, LrZ;->a()Z

    move-result v0

    if-nez v0, :cond_c3

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    invoke-virtual {v0}, LrZ;->a()Ljava/util/Map$Entry;

    move-result-object v10

    .line 279
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;

    invoke-virtual {v0}, Lsm;->a()LrK;

    move-result-object v4

    .line 280
    invoke-virtual {v4}, LrK;->c()Ljava/lang/String;

    move-result-object v6

    .line 281
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;

    iget-boolean v0, v0, Lsm;->a:Z

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 284
    invoke-virtual {v4}, LrK;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v9, v6, v1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v9, v1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 287
    invoke-static {v9, v6}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 288
    invoke-direct {p0, v9, v2}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 290
    new-instance v3, LTi;

    invoke-direct {v3, v9}, LTi;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, LTi;->a(LrK;)LTi;

    move-result-object v3

    invoke-virtual {v3, v6}, LTi;->b(Ljava/lang/String;)LTi;

    move-result-object v3

    invoke-virtual {v3, v0}, LTi;->c(Z)LTi;

    move-result-object v0

    invoke-virtual {v0, v11}, LTi;->d(Z)LTi;

    move-result-object v0

    invoke-virtual {v0}, LTi;->a()LTi;

    move-result-object v0

    invoke-virtual {v0}, LTi;->a()Landroid/content/Intent;

    move-result-object v0

    .line 298
    invoke-direct {p0, v9, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 300
    new-instance v0, LrY;

    new-instance v5, LPb;

    iget-object v7, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Landroid/app/NotificationManager;

    invoke-direct {v5, v7}, LPb;-><init>(Landroid/app/NotificationManager;)V

    iget-object v7, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Llf;

    iget-object v8, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LME;

    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LrY;-><init>(Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;LrK;LPa;Ljava/lang/String;Llf;LME;Landroid/content/Context;)V

    .line 304
    new-instance v2, Lsk;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsm;

    invoke-direct {v2, p0, v0, v1}, Lsk;-><init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;Lsn;Lsm;)V

    .line 307
    const-string v0, "UploadQueueService"

    const-string v1, "Starting upload of %s, account=%s, convert=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4}, LrK;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v11

    const/4 v5, 0x1

    aput-object v6, v3, v5

    const/4 v5, 0x2

    invoke-virtual {v4}, LrK;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lrx;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsm;

    invoke-interface {v1, v6, v0, v2}, Lrx;->a(Ljava/lang/String;Lsm;Lsn;)V

    .line 312
    invoke-direct {p0, v6}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsf;

    .line 315
    invoke-interface {v0}, Lsf;->f()V

    goto :goto_b3

    .line 319
    :cond_c3
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsf;

    .line 320
    invoke-interface {v0}, Lsf;->g()V

    goto :goto_c9

    .line 322
    :cond_d9
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lsm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrZ;

    invoke-virtual {v0, p1}, LrZ;->a(Ljava/util/Collection;)V

    .line 156
    return-void
.end method

.method public a(Lsf;)V
    .registers 3
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .registers 3

    .prologue
    .line 146
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceService;->onCreate()V

    .line 147
    const-string v0, "UploadQueueService"

    const-string v1, "upload queue service started"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return-void
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 266
    const-string v0, "UploadQueueService"

    const-string v1, "stopped service"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceService;->onDestroy()V

    .line 268
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 257
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/GuiceService;->onStartCommand(Landroid/content/Intent;II)I

    .line 258
    const-string v0, "UploadQueueService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received start id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " flags = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    const/4 v0, 0x1

    return v0
.end method
