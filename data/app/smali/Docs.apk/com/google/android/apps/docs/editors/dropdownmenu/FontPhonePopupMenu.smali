.class public Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;
.super Lcom/google/android/apps/docs/editors/dropdownmenu/PhonePopupMenu;
.source "FontPhonePopupMenu.java"


# instance fields
.field private a:Luw;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/dropdownmenu/PhonePopupMenu;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    sget v0, LsF;->font_picker:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;->a:Luw;

    invoke-virtual {v1, v0}, Luw;->a(Landroid/view/View;)V

    .line 49
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;->c(Landroid/view/View;)V

    .line 50
    return-object v0
.end method

.method public a()Luw;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;->a:Luw;

    return-object v0
.end method

.method public a()V
    .registers 1

    .prologue
    .line 55
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/dropdownmenu/PhonePopupMenu;->a()V

    .line 56
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .registers 4
    .parameter

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/dropdownmenu/PhonePopupMenu;->a(Landroid/app/Activity;)V

    .line 29
    new-instance v0, Luw;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Luw;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;->a:Luw;

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;->a:Luw;

    new-instance v1, Luv;

    invoke-direct {v1, p0}, Luv;-><init>(Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;)V

    invoke-virtual {v0, v1}, Luw;->a(Landroid/view/View$OnClickListener;)V

    .line 42
    return-void
.end method
