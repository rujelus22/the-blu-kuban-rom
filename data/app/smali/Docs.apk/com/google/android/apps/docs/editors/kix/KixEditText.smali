.class public Lcom/google/android/apps/docs/editors/kix/KixEditText;
.super Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;
.source "KixEditText.java"

# interfaces
.implements LsN;
.implements LxG;
.implements LyH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;",
        "LsN;",
        "LxG",
        "<",
        "Lcom/google/android/apps/docs/editors/kix/KixEditText;",
        ">;",
        "LyH;"
    }
.end annotation


# instance fields
.field private a:I

.field private a:LDY;

.field private a:LER;

.field a:LKS;

.field private a:LMw;

.field private a:Landroid/widget/Scroller;

.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lyj;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lyk;

.field private a:LzR;

.field private a:Lzw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lzw",
            "<",
            "Lcom/google/android/apps/docs/editors/kix/KixEditText;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lyg;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lyk;

.field private b:Z

.field private final c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lyi;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LAY;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private final e:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lyl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 185
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;-><init>(Landroid/content/Context;)V

    .line 148
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 150
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 152
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 154
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 156
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 160
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Landroid/widget/Scroller;

    .line 167
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    .line 170
    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    .line 171
    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    .line 173
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Z

    .line 174
    sget-object v0, Lyk;->a:Lyk;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    .line 176
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    .line 177
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Z

    .line 179
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Z

    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->z()V

    .line 187
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 191
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 148
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 150
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 152
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 154
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 156
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 160
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Landroid/widget/Scroller;

    .line 167
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    .line 170
    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    .line 171
    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    .line 173
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Z

    .line 174
    sget-object v0, Lyk;->a:Lyk;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    .line 176
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    .line 177
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Z

    .line 179
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Z

    .line 192
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->z()V

    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 197
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 148
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 150
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 152
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 154
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 156
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 160
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Landroid/widget/Scroller;

    .line 167
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    .line 170
    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    .line 171
    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    .line 173
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Z

    .line 174
    sget-object v0, Lyk;->a:Lyk;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    .line 176
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    .line 177
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Z

    .line 179
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Z

    .line 198
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->z()V

    .line 199
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)Landroid/widget/Scroller;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Landroid/widget/Scroller;

    return-object v0
.end method

.method private a(Lyk;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 393
    sget-object v0, Lyk;->d:Lyk;

    if-ne p1, v0, :cond_25

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Z

    if-nez v0, :cond_25

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, LsH;->cannot_edit:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 395
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 396
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->a:Lyk;

    if-ne v0, v1, :cond_23

    .line 429
    :cond_22
    :goto_22
    return-void

    .line 402
    :cond_23
    sget-object p1, Lyk;->b:Lyk;

    .line 405
    :cond_25
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    if-eq p1, v0, :cond_22

    .line 409
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_31
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyl;

    .line 412
    sget-object v1, Lyk;->d:Lyk;

    if-ne p1, v1, :cond_46

    move v1, v2

    :goto_42
    invoke-interface {v0, v1}, Lyl;->a_(Z)V

    goto :goto_31

    :cond_46
    move v1, v3

    goto :goto_42

    .line 415
    :cond_48
    sget-object v0, Lyk;->d:Lyk;

    if-ne p1, v0, :cond_61

    .line 416
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v0

    .line 417
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v1

    .line 421
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(II)V

    .line 422
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b(Z)V

    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->requestFocus()Z

    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->m()V

    goto :goto_22

    .line 427
    :cond_61
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b(Z)V

    goto :goto_22
.end method

.method private s()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 622
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v3

    if-eq v2, v3, :cond_18

    iget-boolean v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Z

    if-eqz v2, :cond_18

    move v2, v0

    .line 623
    :goto_11
    if-eqz v2, :cond_1a

    iget-boolean v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Z

    if-eqz v2, :cond_1a

    :goto_17
    return v0

    :cond_18
    move v2, v1

    .line 622
    goto :goto_11

    :cond_1a
    move v0, v1

    .line 623
    goto :goto_17
.end method

.method private t()Z
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->c:Lyk;

    if-ne v0, v1, :cond_9

    move v0, v2

    .line 659
    :goto_8
    return v0

    .line 639
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v4

    .line 640
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v5

    .line 641
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()Landroid/text/Editable;

    move-result-object v6

    .line 642
    if-eqz v6, :cond_59

    .line 643
    const-class v0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    invoke-interface {v6, v4, v5, v0}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    .line 644
    array-length v7, v0

    move v3, v2

    :goto_21
    if-ge v3, v7, :cond_59

    aget-object v8, v0, v3

    .line 645
    invoke-interface {v6, v8}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    if-gt v1, v4, :cond_55

    invoke-interface {v6, v8}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    if-lt v1, v5, :cond_55

    invoke-virtual {v8}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Z

    move-result v1

    if-eqz v1, :cond_55

    .line 648
    invoke-virtual {v8}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Ljava/util/List;

    move-result-object v1

    .line 649
    if-eqz v1, :cond_55

    .line 650
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_41
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_55

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 651
    invoke-virtual {v8, v1}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 652
    const/4 v0, 0x1

    goto :goto_8

    .line 644
    :cond_55
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_21

    :cond_59
    move v0, v2

    .line 659
    goto :goto_8
.end method

.method private z()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 206
    new-instance v0, LMw;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lyh;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lyh;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditText;Lyf;)V

    invoke-direct {v0, v1, v2}, LMw;-><init>(Landroid/content/Context;LMz;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LMw;

    .line 209
    iput-boolean v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Z

    .line 210
    iput-boolean v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Z

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)Laoo;

    move-result-object v0

    const-class v1, LKS;

    invoke-interface {v0, v1}, Laoo;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LKS;

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 221
    const/high16 v0, 0x1200

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setImeOptions(I)V

    .line 222
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setCursorVisible(Z)V

    .line 223
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setScroller(Landroid/widget/Scroller;)V

    .line 224
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->computeHorizontalScrollRange()I

    move-result v0

    return v0
.end method

.method protected a()LDY;
    .registers 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LDY;

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/apps/docs/editors/text/TextView;)LER;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LER;

    invoke-interface {v0}, LER;->p()V

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LER;

    return-object v0
.end method

.method protected a(ILandroid/text/Layout$Alignment;I)LEj;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 727
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a(ILandroid/text/Layout$Alignment;I)LEj;

    move-result-object v0

    .line 728
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LKS;

    const-string v2, "kixMaxTextCacheSize"

    const/16 v3, 0x32

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;I)I

    move-result v1

    .line 730
    if-lez v1, :cond_18

    .line 731
    new-instance v2, LzJ;

    invoke-direct {v2, v1}, LzJ;-><init>(I)V

    invoke-interface {v0, v2}, LEj;->a(LFc;)V

    .line 733
    :cond_18
    return-object v0
.end method

.method protected a(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .registers 2
    .parameter

    .prologue
    .line 722
    return-object p1
.end method

.method public a()Landroid/view/View;
    .registers 1

    .prologue
    .line 716
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 590
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v2

    .line 591
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v3

    .line 592
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()Landroid/text/Editable;

    move-result-object v4

    .line 593
    if-eqz v4, :cond_31

    .line 594
    const-class v0, LCb;

    invoke-interface {v4, v2, v3, v0}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LCb;

    .line 595
    array-length v5, v0

    const/4 v6, 0x1

    if-ne v5, v6, :cond_31

    .line 596
    const/4 v5, 0x0

    aget-object v0, v0, v5

    .line 597
    invoke-interface {v4, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    if-gt v5, v2, :cond_2a

    invoke-interface {v4, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    if-ge v2, v3, :cond_2c

    :cond_2a
    move-object v0, v1

    .line 604
    :goto_2b
    return-object v0

    .line 601
    :cond_2c
    invoke-virtual {v0}, LCb;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2b

    :cond_31
    move-object v0, v1

    .line 604
    goto :goto_2b
.end method

.method public a()V
    .registers 4

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-ne v0, v1, :cond_1c

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyi;

    .line 276
    invoke-interface {v0}, Lyi;->a()V

    goto :goto_c

    .line 281
    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 285
    :cond_30
    return-void
.end method

.method public a(FF)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 673
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    instance-of v0, v0, Lzq;

    if-eqz v0, :cond_15

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Lyk;

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-ne v0, v1, :cond_15

    .line 677
    sget-object v0, Lyk;->b:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 682
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Lzw;

    invoke-virtual {v0}, Lzw;->a()V

    .line 683
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->invalidate()V

    .line 684
    return-void
.end method

.method public a(FFFFF)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 705
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(F)V

    .line 706
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()LEj;

    move-result-object v0

    float-to-int v1, p2

    float-to-int v2, p3

    invoke-static {p0, v0, v1, v2}, LGt;->a(Lcom/google/android/apps/docs/editors/text/TextView;LEj;II)V

    .line 710
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->l()V

    .line 711
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->invalidate()V

    .line 712
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getScrollY()I

    move-result v0

    if-le p1, v0, :cond_16

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getScrollY()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_2b

    .line 535
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 536
    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, LKf;->a(III)I

    move-result v0

    .line 537
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getScrollX()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->scrollTo(II)V

    .line 539
    :cond_2b
    return-void
.end method

.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 455
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a(II)V

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_8

    .line 469
    :cond_7
    :goto_7
    return-void

    .line 463
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 464
    invoke-interface {v0, p1, p2}, Lyj;->a(II)V

    goto :goto_e

    .line 466
    :cond_1e
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Z

    if-nez v0, :cond_7

    .line 467
    if-eq p1, p2, :cond_29

    const/4 v0, 0x1

    :goto_25
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setCursorVisible(Z)V

    goto :goto_7

    :cond_29
    const/4 v0, 0x0

    goto :goto_25
.end method

.method public a(LAY;)V
    .registers 3
    .parameter

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    return-void
.end method

.method public a(LwF;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 237
    invoke-static {}, LGo;->a()LGo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 238
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Z

    if-eqz v0, :cond_17

    .line 240
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setEditable(Z)V

    .line 241
    sget-object v0, Lyk;->d:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 247
    :cond_14
    :goto_14
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Z

    .line 248
    return-void

    .line 243
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->a:Lyk;

    if-ne v0, v1, :cond_14

    .line 244
    sget-object v0, Lyk;->b:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    goto :goto_14
.end method

.method public a(LwF;LvZ;Lxa;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 233
    return-void
.end method

.method public a(Lyg;)V
    .registers 3
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    return-void
.end method

.method public a(Lyi;)V
    .registers 3
    .parameter

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    return-void
.end method

.method public a(Lyj;)V
    .registers 3
    .parameter

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    return-void
.end method

.method public a(Lyl;)V
    .registers 3
    .parameter

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 544
    return-void
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->c:Lyk;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected a(ZI)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 771
    if-nez p1, :cond_b

    iget v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    if-lt p2, v1, :cond_10

    iget v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    if-gt p2, v1, :cond_10

    .line 772
    :cond_b
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(I)Z

    move-result v0

    .line 788
    :cond_f
    :goto_f
    return v0

    .line 775
    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    .line 776
    iget v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    .line 777
    iget v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    .line 778
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()LEj;

    move-result-object v1

    .line 779
    if-eqz v1, :cond_f

    .line 780
    iget v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    invoke-interface {v1, v2}, LEj;->g(I)I

    move-result v2

    .line 781
    iget v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    invoke-interface {v1, v3}, LEj;->g(I)I

    move-result v1

    .line 784
    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 786
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(II)Z

    move-result v0

    goto :goto_f
.end method

.method public b()I
    .registers 2

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->v()V

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-ne v0, v1, :cond_22

    .line 298
    sget-object v0, Lyk;->b:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 300
    :cond_22
    return-void
.end method

.method public b(FF)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 688
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->u()V

    .line 689
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->cancelLongPress()V

    .line 691
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    instance-of v0, v0, Lzq;

    if-eqz v0, :cond_1a

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 693
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(II)I

    move-result v0

    .line 694
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setSelection(I)V

    .line 697
    :cond_1a
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->f()V

    .line 698
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->invalidate()V

    .line 699
    return-void
.end method

.method public b(Lyj;)V
    .registers 3
    .parameter

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 318
    return-void
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public c()V
    .registers 2

    .prologue
    .line 432
    sget-object v0, Lyk;->d:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 433
    return-void
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->b:Lyk;

    if-ne v0, v1, :cond_f

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Z

    if-eqz v0, :cond_f

    .line 474
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c()V

    .line 475
    const/4 v0, 0x1

    .line 478
    :goto_e
    return v0

    :cond_f
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->c()Z

    move-result v0

    goto :goto_e
.end method

.method public computeScroll()V
    .registers 4

    .prologue
    .line 523
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->computeScroll()V

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Lzw;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getScrollY()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lzw;->a(II)V

    .line 525
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 436
    sget-object v0, Lyk;->c:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 437
    return-void
.end method

.method public d()Z
    .registers 3

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-eq v0, v1, :cond_8

    .line 568
    const/4 v0, 0x0

    .line 570
    :goto_7
    return v0

    :cond_8
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->d()Z

    move-result v0

    goto :goto_7
.end method

.method public e()V
    .registers 3

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->c:Lyk;

    if-ne v0, v1, :cond_b

    .line 441
    sget-object v0, Lyk;->b:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 443
    :cond_b
    return-void
.end method

.method public e()Z
    .registers 3

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-eq v0, v1, :cond_8

    .line 576
    const/4 v0, 0x0

    .line 578
    :goto_7
    return v0

    :cond_8
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->e()Z

    move-result v0

    goto :goto_7
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 583
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e()Z

    move-result v0

    if-nez v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->g()Z

    move-result v0

    if-nez v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 613
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->s()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->t()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 668
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-eq v0, v1, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 9
    .parameter

    .prologue
    .line 745
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Z

    if-eqz v0, :cond_60

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    long-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 747
    :goto_12
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x14

    if-le v0, v1, :cond_22

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_12

    .line 750
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit16 v0, v0, 0x3e8

    int-to-double v1, v0

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    sub-double/2addr v3, v5

    div-double v0, v1, v3

    .line 751
    const-string v2, "KixEditText"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Framerate "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    :cond_60
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()LEj;

    move-result-object v0

    .line 755
    if-eqz v0, :cond_7f

    .line 756
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(I)I

    move-result v1

    .line 757
    invoke-interface {v0, v1}, LEj;->n(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:I

    .line 758
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(I)I

    move-result v1

    .line 759
    invoke-interface {v0, v1}, LEj;->h(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:I

    .line 761
    :cond_7f
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 762
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_88
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_98

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAY;

    .line 763
    invoke-interface {v0}, LAY;->a()V

    goto :goto_88

    .line 765
    :cond_98
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    if-eqz v0, :cond_17

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    invoke-interface {v0, p1}, LzR;->a(Landroid/view/MotionEvent;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    invoke-interface {v0}, LzR;->a()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 487
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->u()V

    .line 488
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->cancelLongPress()V

    .line 492
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LMw;

    invoke-virtual {v0, p1}, LMw;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 493
    const/4 v0, 0x1

    .line 496
    :goto_20
    return v0

    :cond_21
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_20
.end method

.method public setCanComment(Z)V
    .registers 2
    .parameter

    .prologue
    .line 269
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Z

    .line 270
    return-void
.end method

.method public setCustomCursorPopup(LDY;)V
    .registers 2
    .parameter

    .prologue
    .line 551
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LDY;

    .line 552
    return-void
.end method

.method public setCustomSelectionMode(LER;)V
    .registers 2
    .parameter

    .prologue
    .line 547
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LER;

    .line 548
    return-void
.end method

.method public setEditable(Z)V
    .registers 4
    .parameter

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Z

    if-ne v0, p1, :cond_5

    .line 266
    :goto_4
    return-void

    .line 255
    :cond_5
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Z

    .line 257
    if-nez p1, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Lyk;

    sget-object v1, Lyk;->d:Lyk;

    if-ne v0, v1, :cond_14

    .line 258
    sget-object v0, Lyk;->b:Lyk;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyk;)V

    .line 261
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyg;

    .line 262
    invoke-interface {v0, p1}, Lyg;->a(Z)V

    goto :goto_1a

    .line 265
    :cond_2a
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setCursorVisible(Z)V

    goto :goto_4
.end method

.method public setFastScroller(Lzw;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lzw",
            "<",
            "Lcom/google/android/apps/docs/editors/kix/KixEditText;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 508
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Lzw;

    .line 509
    return-void
.end method

.method public setIsTestMode(Z)V
    .registers 2
    .parameter

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c:Z

    .line 203
    return-void
.end method

.method public setProfilingEnabled(Z)V
    .registers 2
    .parameter

    .prologue
    .line 740
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d:Z

    .line 741
    return-void
.end method

.method public setScroller(Landroid/widget/Scroller;)V
    .registers 2
    .parameter

    .prologue
    .line 501
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->setScroller(Landroid/widget/Scroller;)V

    .line 503
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:Landroid/widget/Scroller;

    .line 504
    return-void
.end method

.method public setZoomManager(LzR;)V
    .registers 2
    .parameter

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a:LzR;

    .line 228
    return-void
.end method
