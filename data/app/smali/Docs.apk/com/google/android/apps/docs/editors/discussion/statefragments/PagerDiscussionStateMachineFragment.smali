.class public Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;
.super Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;
.source "PagerDiscussionStateMachineFragment.java"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;-><init>()V

    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x5

    return v0
.end method


# virtual methods
.method a(Landroid/view/ViewGroup;)Landroid/view/animation/Animation;
    .registers 4
    .parameter

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->i()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lsy;->discussion_horizontal_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 75
    :goto_1c
    new-instance v1, Ltu;

    invoke-direct {v1, p0}, Ltu;-><init>(Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;)V

    .line 87
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 89
    return-object v0

    .line 71
    :cond_25
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lsy;->discussion_vertical_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c
.end method

.method public a()Ltm;
    .registers 2

    .prologue
    .line 26
    sget-object v0, Ltm;->c:Ltm;

    return-object v0
.end method

.method public g()V
    .registers 4

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->g()V

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->k()Z

    move-result v1

    if-nez v1, :cond_24

    .line 35
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 38
    :cond_24
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a(Landroid/view/ViewGroup;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 40
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 41
    const-string v0, "PagerDiscussionStateMachineFragment"

    const-string v1, "onResume - starting animation"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a:I

    .line 43
    return-void
.end method

.method public h()V
    .registers 1

    .prologue
    .line 47
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->h()V

    .line 48
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 53
    iget v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_2e

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->k()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 55
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 56
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a:I

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->u()V

    .line 59
    :cond_2e
    return-void
.end method
