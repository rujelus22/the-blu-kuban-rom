.class public Lcom/google/android/apps/docs/view/DocListView;
.super Lcom/google/android/apps/docs/view/FastScrollView;
.source "DocListView.java"

# interfaces
.implements LabB;
.implements LmX;
.implements LmY;
.implements LqZ;


# instance fields
.field private a:I

.field private a:J

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LSF;

.field public a:LUL;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LWK;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LWy;

.field public a:LXS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZE;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LaaY;

.field public a:LaaZ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LabA;

.field private a:Landroid/database/Cursor;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/AdapterView$OnItemClickListener;

.field private a:Landroid/widget/Button;

.field private a:Landroid/widget/ListView;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/apps/docs/RoboFragmentActivity;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Lgd;

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LiG;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LiQ;

.field public a:LiX;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private final a:Ljava/lang/Thread;

.field public a:Lji;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LkB;

.field public a:LlE;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LmH;

.field private a:LmJ;

.field private a:LmK;

.field private a:LmR;

.field private a:LqT;

.field private a:Lrl;

.field private a:Z

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 221
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/view/FastScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 164
    iput-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    .line 184
    iput-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmK;

    .line 185
    invoke-static {}, LSF;->a()LSF;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LSF;

    .line 187
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:J

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Z

    .line 192
    iput-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Ljava/lang/String;

    .line 193
    sget-object v0, LabA;->c:LabA;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LabA;

    .line 217
    sget-object v0, Lrl;->b:Lrl;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lrl;

    .line 223
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/Thread;

    .line 226
    instance-of v0, p1, Lcom/google/android/apps/docs/RoboFragmentActivity;

    if-nez v0, :cond_31

    .line 227
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "DocListView cannot be created without a Context of kind Activity"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_31
    move-object v0, p1

    .line 231
    check-cast v0, Lcom/google/android/apps/docs/RoboFragmentActivity;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    instance-of v0, v0, Lgd;

    if-eqz v0, :cond_62

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    check-cast v0, Lgd;

    :goto_40
    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lgd;

    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()Laoo;

    move-result-object v0

    invoke-interface {v0, p0}, Laoo;->a(Ljava/lang/Object;)V

    .line 237
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LZE;

    invoke-interface {v1}, LZE;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 239
    new-instance v1, LaaY;

    invoke-direct {v1, p1, v0}, LaaY;-><init>(Landroid/content/Context;Landroid/text/format/Time;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaaY;

    .line 241
    invoke-virtual {p0, p0}, Lcom/google/android/apps/docs/view/DocListView;->setOverlayStatusListener(LqZ;)V

    .line 242
    return-void

    :cond_62
    move-object v0, v1

    .line 232
    goto :goto_40
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 93
    iput p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    return p1
.end method

.method private a(LlB;LkO;)I
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LlE;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LZM;

    invoke-static {v1, v2}, LlN;->a(Landroid/content/Context;LZM;)LlK;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LlN;->a(LlE;LlB;LkO;Landroid/content/Context;LkB;LlK;)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;)LWy;
    .registers 2
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;)Landroid/widget/AdapterView$OnItemClickListener;
    .registers 2
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;)Landroid/widget/ListView;
    .registers 2
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;)Lcom/google/android/apps/docs/RoboFragmentActivity;
    .registers 2
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;ILlf;LkB;)LkO;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 772
    invoke-interface {p0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 773
    invoke-static {p0}, LPW;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 774
    invoke-interface {p2, p3, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    return-object v0
.end method

.method private a(LmK;)LmK;
    .registers 3
    .parameter

    .prologue
    .line 962
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmK;

    if-nez v0, :cond_5

    :goto_4
    return-object p1

    :cond_5
    iget-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmK;

    goto :goto_4
.end method

.method private a(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 730
    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    if-ne p1, v0, :cond_17

    move v0, v1

    :goto_a
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/view/View;

    if-ne p1, v3, :cond_19

    :goto_13
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 732
    return-void

    :cond_17
    move v0, v2

    .line 730
    goto :goto_a

    :cond_19
    move v1, v2

    .line 731
    goto :goto_13
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/DocListView;->a(Z)V

    return-void
.end method

.method private a(Lnh;LqT;)V
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 488
    const-string v0, "DocListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in updateDatabaseQuery with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmJ;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmJ;

    invoke-virtual {v0}, LmJ;->a()Lnh;

    move-result-object v0

    invoke-virtual {p1, v0}, Lnh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 491
    :cond_28
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Z)V

    .line 494
    :cond_2c
    invoke-virtual {p2}, LqT;->c()Ljava/lang/String;

    move-result-object v0

    .line 496
    new-instance v1, LmJ;

    invoke-direct {v1, p1, v0}, LmJ;-><init>(Lnh;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmJ;

    .line 498
    iput-object p2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LqT;

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    invoke-interface {v1, p1, v0}, Llf;->a(Lnh;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/database/Cursor;

    .line 503
    sget-object v0, LPX;->p:LPX;

    invoke-virtual {v0}, LPX;->a()LPI;

    move-result-object v0

    invoke-virtual {v0}, LPI;->a()Ljava/lang/String;

    move-result-object v0

    .line 504
    const/16 v1, 0xe

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LPX;->p:LPX;

    invoke-virtual {v2}, LPX;->a()LPI;

    move-result-object v2

    invoke-virtual {v2}, LPI;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x1

    sget-object v2, LPX;->a:LPX;

    invoke-virtual {v2}, LPX;->a()LPI;

    move-result-object v2

    invoke-virtual {v2}, LPI;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x2

    sget-object v2, LPX;->r:LPX;

    invoke-virtual {v2}, LPX;->a()LPI;

    move-result-object v2

    invoke-virtual {v2}, LPI;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x3

    invoke-virtual {p2}, LqT;->a()LPI;

    move-result-object v2

    invoke-virtual {v2}, LPI;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x4

    sget-object v2, LPX;->g:LPX;

    invoke-virtual {v2}, LPX;->a()LPI;

    move-result-object v2

    invoke-virtual {v2}, LPI;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x5

    invoke-virtual {p2}, LqT;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x6

    sget-object v2, LPX;->u:LPX;

    invoke-virtual {v2}, LPX;->a()LPI;

    move-result-object v2

    invoke-virtual {v2}, LPI;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x7

    aput-object v0, v5, v1

    const/16 v1, 0x8

    aput-object v0, v5, v1

    const/16 v1, 0x9

    aput-object v0, v5, v1

    const/16 v1, 0xa

    aput-object v0, v5, v1

    const/16 v1, 0xb

    aput-object v0, v5, v1

    const/16 v1, 0xc

    aput-object v0, v5, v1

    const/16 v1, 0xd

    aput-object v0, v5, v1

    .line 523
    const/16 v0, 0xe

    new-array v6, v0, [I

    const/4 v0, 0x0

    sget v1, Leh;->doc_icon:I

    aput v1, v6, v0

    const/4 v0, 0x1

    sget v1, Leh;->title:I

    aput v1, v6, v0

    const/4 v0, 0x2

    sget v1, Leh;->star_cb:I

    aput v1, v6, v0

    const/4 v0, 0x3

    sget v1, Leh;->date:I

    aput v1, v6, v0

    const/4 v0, 0x4

    sget v1, Leh;->last_modifier_name:I

    aput v1, v6, v0

    const/4 v0, 0x5

    sget v1, Leh;->group_title:I

    aput v1, v6, v0

    const/4 v0, 0x6

    sget v1, Leh;->pin_mode_selected:I

    aput v1, v6, v0

    const/4 v0, 0x7

    sget v1, Leh;->preview_button:I

    aput v1, v6, v0

    const/16 v0, 0x8

    sget v1, Leh;->selected_checkbox:I

    aput v1, v6, v0

    const/16 v0, 0x9

    sget v1, Leh;->pin_status:I

    aput v1, v6, v0

    const/16 v0, 0xa

    sget v1, Leh;->pin_update:I

    aput v1, v6, v0

    const/16 v0, 0xb

    sget v1, Leh;->pin_waiting:I

    aput v1, v6, v0

    const/16 v0, 0xc

    sget v1, Leh;->pin_progress:I

    aput v1, v6, v0

    const/16 v0, 0xd

    sget v1, Leh;->pin_filler:I

    aput v1, v6, v0

    .line 539
    array-length v0, v5

    array-length v1, v6

    if-ne v0, v1, :cond_160

    const/4 v0, 0x1

    :goto_112
    const-string v1, "expected matching length"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/database/Cursor;

    invoke-virtual {p2, v0}, LqT;->a(Landroid/database/Cursor;)V

    .line 544
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lef;->tablet_doclist_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v8, v0

    .line 547
    new-instance v0, Labw;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    sget v3, Lej;->doc_entry_row:I

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/database/Cursor;

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Labw;-><init>(Lcom/google/android/apps/docs/view/DocListView;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[ILqT;I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmH;

    .line 600
    const/4 v0, 0x0

    .line 601
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    if-eqz v1, :cond_18d

    .line 602
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    invoke-interface {v1}, LiQ;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_142
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_162

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiE;

    .line 603
    instance-of v3, v0, LiW;

    if-eqz v3, :cond_1ff

    .line 604
    sget-object v1, LmK;->d:LmK;

    check-cast v0, LiW;

    invoke-virtual {v0}, LiW;->a()LmK;

    move-result-object v0

    invoke-virtual {v1, v0}, LmK;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_15e
    move v1, v0

    goto :goto_142

    .line 539
    :cond_160
    const/4 v0, 0x0

    goto :goto_112

    :cond_162
    move v10, v1

    .line 613
    :goto_163
    const/4 v0, 0x0

    .line 614
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    if-eqz v1, :cond_1ef

    .line 615
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    invoke-interface {v1}, LiQ;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_16f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiE;

    .line 616
    instance-of v3, v0, LiW;

    if-eqz v3, :cond_1fd

    .line 617
    sget-object v1, LmK;->b:LmK;

    check-cast v0, LiW;

    invoke-virtual {v0}, LiW;->a()LmK;

    move-result-object v0

    invoke-virtual {v1, v0}, LmK;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_18b
    move v1, v0

    goto :goto_16f

    .line 609
    :cond_18d
    sget-object v0, LmK;->a:LmK;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(LmK;)LmK;

    move-result-object v0

    sget-object v1, LmK;->d:LmK;

    invoke-virtual {v0, v1}, LmK;->equals(Ljava/lang/Object;)Z

    move-result v10

    goto :goto_163

    :cond_19a
    move v11, v1

    .line 627
    :goto_19b
    new-instance v0, LmR;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LUL;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaaY;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-virtual {v4}, LkB;->b()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LKS;

    iget-object v12, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lgl;

    move-object v4, p0

    move-object v5, p0

    move-object v6, p2

    move-object v9, p0

    invoke-direct/range {v0 .. v12}, LmR;-><init>(Llf;LUL;LaaY;LmY;LmX;LqT;Ljava/lang/String;LKS;LabB;ZZLgl;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmR;

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmH;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmR;

    invoke-virtual {v0, v1}, LmH;->a(LbW;)V

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmH;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 641
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Z

    if-nez v0, :cond_1da

    .line 642
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lji;

    invoke-interface {v0}, Lji;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 645
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Z

    .line 647
    :cond_1da
    invoke-virtual {p2}, LqT;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->setTextSize(I)V

    .line 648
    invoke-virtual {p2}, LqT;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->setOverlaySize(I)V

    .line 649
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->b()V

    .line 650
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->d()V

    .line 651
    return-void

    .line 621
    :cond_1ef
    sget-object v1, LmK;->a:LmK;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(LmK;)LmK;

    move-result-object v1

    sget-object v2, LmK;->b:LmK;

    if-ne v1, v2, :cond_1fb

    .line 622
    const/4 v11, 0x1

    goto :goto_19b

    :cond_1fb
    move v11, v0

    goto :goto_19b

    :cond_1fd
    move v0, v1

    goto :goto_18b

    :cond_1ff
    move v0, v1

    goto/16 :goto_15e
.end method

.method private a(Z)V
    .registers 10
    .parameter

    .prologue
    const/4 v3, 0x0

    const/16 v7, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    if-eqz v0, :cond_32

    move v0, v1

    :goto_a
    invoke-static {v0}, Lagu;->b(Z)V

    .line 382
    const-string v0, "DocListView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "in updateSyncMoreButton "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lgl;

    sget-object v4, Lgi;->q:Lgi;

    invoke-interface {v0, v4}, Lgl;->a(Lgi;)Z

    move-result v0

    if-nez v0, :cond_34

    .line 470
    :goto_31
    return-void

    :cond_32
    move v0, v2

    .line 379
    goto :goto_a

    .line 389
    :cond_34
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    if-eqz v0, :cond_40

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->a()Z

    move-result v0

    if-nez v0, :cond_51

    .line 391
    :cond_40
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->e()V

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    if-eqz v0, :cond_bf

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    invoke-interface {v0}, LiQ;->b()Ljava/lang/String;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_ae

    .line 398
    iput-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    .line 415
    :cond_51
    :goto_51
    if-eqz p1, :cond_8e

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    if-eqz v0, :cond_fd

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->c()Z

    move-result v0

    if-nez v0, :cond_fd

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->b()Z

    move-result v0

    if-nez v0, :cond_fd

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->a()Z

    move-result v0

    if-nez v0, :cond_fd

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LKS;

    const-string v3, "enableSyncMoreImplicitely"

    invoke-interface {v0, v3, v1}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_fd

    move v0, v1

    .line 420
    :goto_7a
    if-eqz v0, :cond_8e

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LKS;

    const-string v3, "syncMoreMaxFeedsToRetrieveImplicitly"

    invoke-interface {v0, v3, v1}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    .line 424
    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    new-instance v4, Laby;

    invoke-direct {v4, p0, v2}, Laby;-><init>(Lcom/google/android/apps/docs/view/DocListView;Z)V

    invoke-interface {v3, v4, v0}, LWy;->a(LWz;I)V

    .line 428
    :cond_8e
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    if-eqz v0, :cond_9a

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->c()Z

    move-result v0

    if-eqz v0, :cond_100

    .line 429
    :cond_9a
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a7

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 468
    :cond_a7
    :goto_a7
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->g()V

    .line 469
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->d()V

    goto :goto_31

    .line 400
    :cond_ae
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWK;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    invoke-interface {v4}, LiQ;->a()LWr;

    move-result-object v4

    invoke-interface {v0, v3, v4}, LWK;->a(LkB;LWr;)LWy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    goto :goto_51

    .line 403
    :cond_bf
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LSF;

    invoke-virtual {v0}, LSF;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_ef

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    iget-wide v5, p0, Lcom/google/android/apps/docs/view/DocListView;->a:J

    invoke-interface {v0, v4, v5, v6}, Llf;->a(LkB;J)LkH;

    move-result-object v0

    .line 406
    if-nez v0, :cond_ea

    move-object v0, v3

    .line 407
    :goto_d4
    sget-object v3, LmK;->a:LmK;

    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/view/DocListView;->a(LmK;)LmK;

    move-result-object v3

    invoke-static {v3, v0}, LWr;->a(LmK;Ljava/lang/String;)LWr;

    move-result-object v0

    .line 409
    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWK;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-interface {v3, v4, v0}, LWK;->a(LkB;LWr;)LWy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    goto/16 :goto_51

    .line 406
    :cond_ea
    invoke-virtual {v0}, LkH;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_d4

    .line 411
    :cond_ef
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWK;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LSF;

    invoke-interface {v0, v3, v4}, LWK;->a(LkB;LSF;)LWy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    goto/16 :goto_51

    :cond_fd
    move v0, v2

    .line 417
    goto/16 :goto_7a

    .line 433
    :cond_100
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-ne v0, v7, :cond_117

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    new-instance v3, Labv;

    invoke-direct {v3, p0}, Labv;-><init>(Lcom/google/android/apps/docs/view/DocListView;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 450
    :cond_117
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->a()Z

    move-result v3

    .line 451
    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    if-nez v3, :cond_136

    move v0, v1

    :goto_122
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 453
    if-eqz v3, :cond_138

    .line 454
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    sget v1, Len;->sync_more_in_progress:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 465
    :goto_12f
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a7

    :cond_136
    move v0, v2

    .line 451
    goto :goto_122

    .line 456
    :cond_138
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->a()J

    move-result-wide v3

    .line 457
    const-wide v5, 0x7fffffffffffffffL

    cmp-long v0, v3, v5

    if-nez v0, :cond_150

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    sget v1, Len;->sync_more:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_12f

    .line 460
    :cond_150
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 461
    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    sget v4, Len;->sync_more_before_template:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/docs/RoboFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 462
    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_12f
.end method

.method private d()V
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmH;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmH;

    invoke-virtual {v0}, LmH;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_c
    const/4 v0, 0x1

    .line 278
    :goto_d
    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/view/View;

    :goto_11
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/view/View;)V

    .line 279
    return-void

    .line 277
    :cond_15
    const/4 v0, 0x0

    goto :goto_d

    .line 278
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    goto :goto_11
.end method

.method private e()V
    .registers 3

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    if-eqz v0, :cond_12

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-virtual {v1}, LkB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    .line 341
    :cond_12
    return-void
.end method

.method private f()V
    .registers 4

    .prologue
    .line 712
    new-instance v0, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;-><init>()V

    .line 713
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->a()Lo;

    move-result-object v1

    const-string v2, "pin_warning"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 714
    return-void
.end method

.method private g()V
    .registers 6

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    if-eqz v0, :cond_49

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiG;

    sget-object v1, LmK;->d:LmK;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-virtual {v3}, LkB;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, LiG;->b(LmK;Ljava/lang/String;)LiE;

    move-result-object v0

    .line 835
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    invoke-interface {v1, v0}, LiQ;->a(LiE;)Z

    move-result v0

    .line 841
    :goto_1b
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    if-eqz v1, :cond_56

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v1}, LWy;->c()Z

    move-result v1

    if-eqz v1, :cond_56

    const/4 v1, 0x1

    .line 844
    :goto_28
    if-nez v1, :cond_36

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LabA;

    sget-object v3, LabA;->c:LabA;

    invoke-virtual {v1, v3}, LabA;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_36

    if-eqz v0, :cond_58

    .line 846
    :cond_36
    sget v0, Leh;->empty_list_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 847
    sget v0, Leh;->empty_list_syncing:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 853
    :goto_48
    return-void

    .line 837
    :cond_49
    sget-object v0, LmK;->a:LmK;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(LmK;)LmK;

    move-result-object v0

    sget-object v1, LmK;->d:LmK;

    invoke-virtual {v0, v1}, LmK;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1b

    :cond_56
    move v1, v2

    .line 841
    goto :goto_28

    .line 849
    :cond_58
    sget v0, Leh;->empty_list_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 850
    sget v0, Leh;->empty_list_syncing:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 851
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->h()V

    goto :goto_48
.end method

.method private h()V
    .registers 4

    .prologue
    .line 856
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 858
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LabA;

    sget-object v2, LabA;->a:LabA;

    invoke-virtual {v1, v2}, LabA;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 859
    sget v1, Len;->sync_waiting:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 863
    :goto_15
    sget v0, Leh;->doc_list_syncing_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 864
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 865
    return-void

    .line 861
    :cond_21
    sget v1, Len;->sync_waiting_subtitle:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_15
.end method

.method private i()V
    .registers 3

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 929
    return-void
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 310
    iget v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    .line 316
    :cond_e
    :goto_e
    return v0

    .line 312
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    .line 313
    const/4 v1, -0x1

    if-ne v0, v1, :cond_e

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    goto :goto_e
.end method

.method public a()Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method public a(I)LkO;
    .registers 5
    .parameter

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/database/Cursor;ILlf;LkB;)LkO;

    move-result-object v0

    return-object v0
.end method

.method public a()LmJ;
    .registers 2

    .prologue
    .line 899
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmJ;

    return-object v0
.end method

.method public a()Lrl;
    .registers 2

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lrl;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 953
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->a()V

    .line 955
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    if-eqz v0, :cond_f

    .line 956
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    invoke-interface {v0}, LWy;->a()V

    .line 957
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LWy;

    .line 959
    :cond_f
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 869
    return-void
.end method

.method public a(JLfS;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    invoke-interface {v0, p1, p2}, Llf;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 758
    if-nez v0, :cond_9

    .line 766
    :cond_8
    :goto_8
    return-void

    .line 762
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-interface {v1, v2, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 763
    if-eqz v0, :cond_8

    .line 764
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lgd;

    invoke-virtual {v0}, LkO;->i()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, p3}, Lgd;->a(Ljava/lang/String;LfS;)V

    goto :goto_8
.end method

.method public a(Landroid/database/Cursor;)V
    .registers 5
    .parameter

    .prologue
    .line 909
    const-string v0, "DocListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in swapCursor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->i()V

    .line 911
    if-nez p1, :cond_20

    .line 922
    :goto_1f
    return-void

    .line 915
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LqT;

    invoke-virtual {v0, p1}, LqT;->a(Landroid/database/Cursor;)V

    .line 916
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmH;

    invoke-virtual {v0, p1}, LmH;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 917
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/database/Cursor;

    .line 918
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmH;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LqT;

    invoke-virtual {v0, v1}, LmH;->a(LqT;)V

    .line 919
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->b()V

    .line 920
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(Z)V

    .line 921
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->d()V

    goto :goto_1f
.end method

.method public a(LiQ;)V
    .registers 5
    .parameter

    .prologue
    .line 478
    const-string v0, "DocListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in fillData(CriterionSet) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiQ;

    .line 480
    invoke-interface {p1}, LiQ;->a()Lnh;

    move-result-object v0

    .line 481
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lrl;

    invoke-virtual {v1}, Lrl;->a()LqT;

    move-result-object v1

    .line 482
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->g()V

    .line 484
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(Lnh;LqT;)V

    .line 485
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 938
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiX;

    invoke-interface {v1}, LiX;->a()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 939
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 940
    if-eqz v1, :cond_1a

    .line 941
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 945
    :goto_14
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiX;

    invoke-interface {v1, v0}, LiX;->a(Ljava/util/Set;)V

    .line 949
    return-void

    .line 943
    :cond_1a
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_14
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-virtual {v1}, LkB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, p2, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 811
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 812
    return-void
.end method

.method public a(LkM;)V
    .registers 6
    .parameter

    .prologue
    .line 689
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()Laoo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-static {v0, v1}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 691
    invoke-virtual {p1}, LkM;->g()Z

    move-result v0

    if-nez v0, :cond_3b

    const/4 v0, 0x1

    :goto_10
    invoke-virtual {p1, v0}, LkM;->b(Z)V

    .line 692
    invoke-virtual {p1}, LkM;->c()V

    .line 694
    invoke-virtual {p1}, LkM;->g()Z

    move-result v0

    .line 695
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 696
    invoke-virtual {p1}, LkM;->a()LkY;

    move-result-object v2

    .line 697
    if-eqz v0, :cond_41

    .line 698
    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaaZ;

    invoke-interface {v3}, LaaZ;->b()Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 699
    invoke-static {v1, v2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/content/Context;LkY;)V

    .line 707
    :goto_2f
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LeQ;

    const-string v2, "pinning"

    if-eqz v0, :cond_45

    const-string v0, "pin"

    :goto_37
    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    return-void

    .line 691
    :cond_3b
    const/4 v0, 0x0

    goto :goto_10

    .line 701
    :cond_3d
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->f()V

    goto :goto_2f

    .line 704
    :cond_41
    invoke-static {v1, v2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b(Landroid/content/Context;LkY;)V

    goto :goto_2f

    .line 707
    :cond_45
    const-string v0, "unpin"

    goto :goto_37
.end method

.method public a(LkY;LWX;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 660
    new-instance v0, LmV;

    sget-object v1, LmW;->a:LmW;

    invoke-direct {v0, v1, p1}, LmV;-><init>(LmW;LkY;)V

    .line 662
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/CustomButton;

    .line 663
    new-instance v1, LmV;

    sget-object v2, LmW;->b:LmW;

    invoke-direct {v1, v2, p1}, LmV;-><init>(LmW;LkY;)V

    .line 665
    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/view/ProgressButton;

    .line 666
    if-nez v0, :cond_22

    if-eqz v1, :cond_25

    .line 667
    :cond_22
    invoke-static {v0, v1, p2}, LaaH;->a(Lcom/google/android/apps/docs/view/CustomButton;Lcom/google/android/apps/docs/view/ProgressButton;LWX;)V

    .line 669
    :cond_25
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()Laoo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/RoboFragmentActivity;

    invoke-static {v0, v1}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-interface {v0, v1, p1}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 675
    if-eqz v0, :cond_36

    .line 676
    new-instance v1, LlT;

    invoke-direct {v1, v0}, LlT;-><init>(LkO;)V

    .line 677
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(LlB;LkO;)I

    move-result v0

    .line 678
    if-eqz v0, :cond_36

    .line 680
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    invoke-virtual {v1}, LkB;->a()V

    .line 681
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LZM;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    if-ne v0, v3, :cond_37

    sget v0, Len;->operation_on_starring_failed_as_offline:I

    :goto_2e
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 686
    :cond_36
    return-void

    .line 681
    :cond_37
    sget v0, Len;->operation_on_starring_failed:I

    goto :goto_2e
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 879
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 246
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->onAttachedToWindow()V

    .line 247
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    if-nez v0, :cond_f

    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 251
    :cond_f
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    if-eqz v0, :cond_c

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 264
    :cond_c
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->onDetachedFromWindow()V

    .line 265
    return-void
.end method

.method protected onFinishInflate()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 283
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->onFinishInflate()V

    .line 284
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    .line 285
    sget v0, Leh;->empty_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/view/View;

    .line 286
    sget-object v0, Lfb;->a:Lfb;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Laoz;

    invoke-interface {v1}, Laoz;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 287
    sget v0, Leh;->empty_list_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 288
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 290
    :cond_30
    sget v0, Leh;->sync_more_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/Button;

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    new-instance v1, Labu;

    invoke-direct {v1, p0}, Labu;-><init>(Lcom/google/android/apps/docs/view/DocListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 306
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 722
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x8000

    if-ne v0, v1, :cond_12

    .line 723
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 724
    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 726
    :cond_12
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/view/FastScrollView;->onMeasure(II)V

    .line 727
    return-void
.end method

.method public setAccount(LkB;)V
    .registers 2
    .parameter

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LkB;

    .line 328
    return-void
.end method

.method public setCollectionId(J)V
    .registers 3
    .parameter

    .prologue
    .line 347
    iput-wide p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:J

    .line 348
    return-void
.end method

.method public setMainFilter(LmK;)V
    .registers 3
    .parameter

    .prologue
    .line 739
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LmK;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LmK;

    .line 740
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->g()V

    .line 741
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 355
    return-void
.end method

.method public setSearchTerm(LSF;)V
    .registers 2
    .parameter

    .prologue
    .line 747
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 748
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LSF;

    .line 749
    return-void
.end method

.method public setSelectedResourceId(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 894
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Ljava/lang/String;

    .line 895
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->c()V

    .line 896
    return-void
.end method

.method public setSelectionMode(Z)V
    .registers 2
    .parameter

    .prologue
    .line 887
    iput-boolean p1, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Z

    .line 888
    return-void
.end method

.method public setSorting(Lrl;)V
    .registers 3
    .parameter

    .prologue
    .line 819
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrl;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lrl;

    .line 820
    return-void
.end method

.method public setSyncStatus(LabA;)V
    .registers 2
    .parameter

    .prologue
    .line 823
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LabA;

    .line 824
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->g()V

    .line 825
    return-void
.end method

.method public setTagName(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 270
    if-eqz p1, :cond_f

    .line 271
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    .line 272
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setTag(Ljava/lang/Object;)V

    .line 274
    :cond_f
    return-void

    .line 269
    :cond_10
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 873
    const-string v0, "%s[mainFilter=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LmK;->a:LmK;

    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/view/DocListView;->a(LmK;)LmK;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
