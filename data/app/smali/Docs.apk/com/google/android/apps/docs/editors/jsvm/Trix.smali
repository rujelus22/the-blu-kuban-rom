.class public Lcom/google/android/apps/docs/editors/jsvm/Trix;
.super Ljava/lang/Object;
.source "Trix.java"


# direct methods
.method private static native TrixTopLevelcreateV8Bootstrap(J)J
.end method

.method private static native V8BootstraphandleHttpDataBatch(JIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static native V8BootstraphandleHttpRequestFailure(JII)V
.end method

.method private static native V8BootstrapinvokeTimer(JD)I
.end method

.method public static synthetic a(JD)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->V8BootstrapinvokeTimer(JD)I

    move-result v0

    return v0
.end method

.method public static synthetic a()J
    .registers 2

    .prologue
    .line 11
    invoke-static {}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->createTrixContext()J

    move-result-wide v0

    return-wide v0
.end method

.method public static synthetic a(J)J
    .registers 4
    .parameter

    .prologue
    .line 11
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->TrixTopLevelcreateV8Bootstrap(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static synthetic a(JII)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->V8BootstraphandleHttpRequestFailure(JII)V

    return-void
.end method

.method public static synthetic a(JIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-static/range {p0 .. p7}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->V8BootstraphandleHttpDataBatch(JIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b()J
    .registers 2

    .prologue
    .line 11
    invoke-static {}, Lcom/google/android/apps/docs/editors/jsvm/Trix;->createTrixTopLevelInstance()J

    move-result-wide v0

    return-wide v0
.end method

.method private static native createTrixContext()J
.end method

.method private static native createTrixTopLevelInstance()J
.end method
