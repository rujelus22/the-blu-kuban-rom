.class public Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;
.super Lcom/google/android/apps/docs/editors/popup/SelectionPopup;
.source "TextSelectionPopup.java"

# interfaces
.implements LDY;
.implements LER;


# instance fields
.field private a:LBO;

.field private a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

.field private a:Lth;

.field private a:Lzc;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;-><init>()V

    return-void
.end method

.method private a()LKj;
    .registers 4

    .prologue
    .line 194
    new-instance v0, LKj;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lcom/google/android/apps/docs/editors/kix/KixEditText;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lth;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lth;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)Lzc;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lzc;

    return-object v0
.end method

.method private w()V
    .registers 4

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LEw;->text_select_handle_middle:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 96
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 97
    new-instance v2, LBP;

    invoke-direct {v2, v1, v0}, LBP;-><init>(II)V

    iput-object v2, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:LBO;

    .line 99
    return-void
.end method

.method private x()V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_94

    move v0, v1

    .line 129
    :goto_b
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()Z

    move-result v4

    .line 132
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v3

    if-gtz v3, :cond_97

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v5}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v3, v5, :cond_97

    move v3, v1

    .line 134
    :goto_2e
    iget-object v5, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v5}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v6}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v6

    if-eq v5, v6, :cond_99

    if-nez v0, :cond_99

    if-nez v3, :cond_99

    if-nez v4, :cond_99

    .line 137
    :goto_42
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Z

    .line 139
    sget v3, LsD;->editPopup:I

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(IZ)V

    .line 140
    const v3, 0x102001f

    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(IZ)V

    .line 141
    const v1, 0x1020020

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d()Z

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(IZ)V

    .line 142
    const v1, 0x1020021

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()Z

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(IZ)V

    .line 143
    const v1, 0x1020022

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e()Z

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(IZ)V

    .line 144
    sget v1, LsD;->follow_link:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(IZ)V

    .line 145
    sget v0, LsD;->comment:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->g()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(IZ)V

    .line 151
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Z

    if-eqz v0, :cond_93

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->s()V

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 154
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Z

    .line 156
    :cond_93
    return-void

    :cond_94
    move v0, v2

    .line 128
    goto/16 :goto_b

    :cond_97
    move v3, v2

    .line 132
    goto :goto_2e

    :cond_99
    move v1, v2

    .line 134
    goto :goto_42
.end method


# virtual methods
.method public a(II)Landroid/graphics/Point;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:LBO;

    new-instance v1, LBI;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-direct {v1, v2}, LBI;-><init>(Lcom/google/android/apps/docs/editors/text/EditText;)V

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a()LKj;

    move-result-object v2

    invoke-interface {v0, v1, v2, p1, p2}, LBO;->a(LBH;LKj;II)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 5
    .parameter

    .prologue
    .line 41
    sget v0, LsF;->kix_selection_popup:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 43
    new-instance v1, LBQ;

    invoke-direct {v1, p0}, LBQ;-><init>(Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;)V

    .line 81
    sget v2, LsD;->editPopup:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v2, 0x102001f

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v2, 0x1020020

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    const v2, 0x1020021

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v2, 0x1020022

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    sget v2, LsD;->follow_link:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    sget v2, LsD;->comment:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-object v0
.end method

.method protected a()V
    .registers 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->i()Z

    move-result v0

    if-nez v0, :cond_9

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->x()V

    .line 123
    :cond_9
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;->a()V

    .line 124
    return-void
.end method

.method public final a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V
    .registers 3
    .parameter

    .prologue
    .line 102
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    .line 103
    return-void
.end method

.method public a(Lth;)V
    .registers 2
    .parameter

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lth;

    .line 111
    return-void
.end method

.method public a(Lzc;)V
    .registers 3
    .parameter

    .prologue
    .line 106
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzc;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lzc;

    .line 107
    return-void
.end method

.method public b()Landroid/view/View;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    return-object v0
.end method

.method protected b(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->w()V

    .line 116
    return-void
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->j()Z

    move-result v0

    return v0
.end method

.method public p()V
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->w()V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->u()V

    .line 162
    return-void
.end method

.method public q()V
    .registers 2

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->v()V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->x()V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->y()V

    .line 175
    return-void
.end method

.method public r()V
    .registers 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->v()V

    .line 180
    return-void
.end method
