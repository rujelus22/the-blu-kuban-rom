.class public abstract Lcom/google/android/apps/docs/app/AbstractExportedActivity;
.super Landroid/app/Activity;
.source "AbstractExportedActivity.java"


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 23
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AbstractExportedActivity;->a:Ljava/lang/Class;

    .line 24
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Intent;)V
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractExportedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 30
    iget-object v1, p0, Lcom/google/android/apps/docs/app/AbstractExportedActivity;->a:Ljava/lang/Class;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 31
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AbstractExportedActivity;->a(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AbstractExportedActivity;->startActivity(Landroid/content/Intent;)V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractExportedActivity;->finish()V

    .line 34
    return-void
.end method
