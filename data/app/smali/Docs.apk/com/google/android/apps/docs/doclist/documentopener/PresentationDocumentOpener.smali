.class public Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;
.super Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;
.source "PresentationDocumentOpener.java"


# instance fields
.field private final a:LKS;

.field private final a:Landroid/content/Context;

.field private final a:Lgl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LKS;Lgl;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:LKS;

    .line 50
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:Lgl;

    .line 51
    return-void
.end method

.method private a(Landroid/net/Uri$Builder;)V
    .registers 10
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:LKS;

    const-string v1, "punchUriParametersToAdd"

    const-string v2, "touch=1, rm=minimal"

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    const-string v1, "\\s*([a-zA-Z]\\w*)\\s*=\\s*(\\w+)\\s*"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 99
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_18
    if-ge v0, v3, :cond_4f

    aget-object v4, v2, v0

    .line 100
    invoke-virtual {v1, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 101
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_41

    .line 102
    const-string v5, "PresentationDocumentOpener"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Client flag key \"punchUriParametersToAdd\" contains invalid parameter: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :goto_3e
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 106
    :cond_41
    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 107
    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 108
    invoke-virtual {p1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_3e

    .line 110
    :cond_4f
    return-void
.end method


# virtual methods
.method public a(Lpa;LkM;Landroid/os/Bundle;)Lnm;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-virtual {p2}, LkM;->a()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 57
    invoke-static {v0}, LoY;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-virtual {p2}, LkM;->a()LkB;

    move-result-object v2

    invoke-virtual {v2}, LkB;->b()Ljava/lang/String;

    move-result-object v3

    .line 60
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:Lgl;

    sget-object v4, Lgi;->n:Lgi;

    invoke-interface {v2, v4}, Lgl;->a(Lgi;)Z

    move-result v2

    if-eqz v2, :cond_87

    .line 61
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:LKS;

    const-string v4, "punchUriSignature"

    const-string v5, "/presentation/"

    invoke-interface {v2, v4, v5}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_87

    .line 64
    const-string v1, "PresentationDocumentOpener"

    const-string v2, "Opening the Punch HiFi WebView."

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 66
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 67
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 68
    const/4 v0, 0x0

    move v1, v0

    :goto_43
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_58

    .line 69
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 68
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_43

    .line 72
    :cond_58
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:LKS;

    const-string v1, "punchUriPathToAdd"

    const-string v2, "mobilepresent"

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 76
    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a(Landroid/net/Uri$Builder;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2}, LkM;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 80
    const-string v0, "resourceId"

    invoke-virtual {p2}, LkM;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 81
    new-instance v0, LpT;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:Landroid/content/Context;

    invoke-direct {v0, v2, p1, v3, v1}, LpT;-><init>(Landroid/content/Context;LpS;Ljava/lang/String;Landroid/content/Intent;)V

    .line 91
    :goto_86
    return-object v0

    .line 86
    :cond_87
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 89
    const-string v0, "ncl"

    const-string v1, "true"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 91
    new-instance v0, LpR;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/PresentationDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p2}, LkM;->c()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LpR;-><init>(Landroid/content/Context;LpS;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_86
.end method
