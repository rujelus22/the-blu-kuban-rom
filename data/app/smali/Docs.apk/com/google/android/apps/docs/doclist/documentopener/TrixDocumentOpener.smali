.class public Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;
.super Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;
.source "TrixDocumentOpener.java"


# instance fields
.field private final a:LKS;

.field private final a:Landroid/content/Context;

.field private final a:Lgl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgl;LKS;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:Lgl;

    .line 46
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:LKS;

    .line 47
    return-void
.end method

.method private a(LkM;Landroid/os/Bundle;)Landroid/content/Intent;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 65
    invoke-virtual {p1}, LkM;->a()Ljava/lang/String;

    move-result-object v0

    .line 66
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 67
    if-eqz p2, :cond_13

    const-string v2, "editMode"

    invoke-virtual {p2, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 68
    :cond_13
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LoY;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:Lgl;

    sget-object v3, Lgi;->w:Lgi;

    invoke-interface {v2, v3}, Lgl;->a(Lgi;)Z

    move-result v2

    if-eqz v2, :cond_6e

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:LKS;

    const-string v3, "trixEnableEditor"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6e

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v2, v3, :cond_6e

    .line 73
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:LKS;

    const-string v3, "trixDocumentUrlPattern"

    const-string v4, "/spreadsheet/ccc.*"

    invoke-interface {v2, v3, v4}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 76
    invoke-virtual {p1}, LkM;->a()LkB;

    move-result-object v0

    invoke-virtual {v0}, LkB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LkM;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LkM;->a()LkP;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LkP;)Landroid/content/Intent;

    move-result-object v0

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 79
    if-eqz v2, :cond_6e

    .line 80
    const-string v1, "KixDocumentOpener"

    const-string v2, "Opening the native Trix editor."

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :goto_6d
    return-object v0

    .line 87
    :cond_6e
    const-string v0, "KixDocumentOpener"

    const-string v2, "Opening generic WebView to edit Trix document."

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {p1}, LkM;->a()LkB;

    move-result-object v2

    invoke-virtual {v2}, LkB;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LkM;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_6d
.end method


# virtual methods
.method public a(Lpa;LkM;Landroid/os/Bundle;)Lnm;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a(LkM;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 55
    const-string v1, "resourceId"

    invoke-virtual {p2}, LkM;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v1, "userCanEdit"

    invoke-virtual {p2}, LkM;->k()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    const-string v1, "editMode"

    const-string v2, "editMode"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 59
    new-instance v1, LpT;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/TrixDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {p2}, LkM;->a()LkB;

    move-result-object v3

    invoke-virtual {v3}, LkB;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, v0}, LpT;-><init>(Landroid/content/Context;LpS;Ljava/lang/String;Landroid/content/Intent;)V

    .line 61
    return-object v1
.end method
