.class public Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "CollaboratorFragment.java"


# instance fields
.field protected a:Landroid/widget/ImageView;

.field protected a:Landroid/widget/LinearLayout;

.field protected a:Landroid/widget/TextView;

.field protected a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

.field protected d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    sget v0, LsF;->collaborator_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->d:Landroid/view/View;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->d:Landroid/view/View;

    sget v1, LsD;->collaborator_title_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Landroid/widget/TextView;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->d:Landroid/view/View;

    sget v1, LsD;->collaborator_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Landroid/widget/LinearLayout;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->d:Landroid/view/View;

    sget v1, LsD;->collaborator_title_arrow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Landroid/widget/ImageView;

    .line 44
    if-eqz p2, :cond_39

    .line 45
    sget v0, LsD;->editor:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    .line 47
    :cond_39
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->b(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->e(Landroid/os/Bundle;)V

    .line 55
    return-void
.end method

.method protected e(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    .line 58
    new-instance v0, LsK;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->d:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-direct/range {v0 .. v6}, LsK;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/LinearLayout;Landroid/widget/ImageView;LsN;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lyz;

    move-result-object v1

    .line 67
    invoke-virtual {v1, v0}, Lyz;->addCollaboratorListener(LyF;)V

    .line 68
    invoke-virtual {v1, v0}, Lyz;->addNetworkStatusListener(LyQ;)V

    .line 69
    return-void
.end method
