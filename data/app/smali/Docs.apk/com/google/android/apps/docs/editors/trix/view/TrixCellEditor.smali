.class public Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;
.super Landroid/widget/EditText;
.source "TrixCellEditor.java"


# instance fields
.field private a:LIk;

.field private a:LIl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->a:LIk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->a:LIk;

    invoke-interface {v0, p1, p2}, LIk;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 53
    const/4 v0, 0x1

    .line 56
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_d
.end method

.method protected onSelectionChanged(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->a:LIl;

    if-eqz v0, :cond_c

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->a:LIl;

    invoke-interface {v0, p1, p2}, LIl;->a(II)V

    .line 65
    :cond_c
    return-void
.end method

.method public setPreImeListener(LIk;)V
    .registers 2
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->a:LIk;

    .line 44
    return-void
.end method

.method public setSelectionChangedListener(LIl;)V
    .registers 2
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->a:LIl;

    .line 48
    return-void
.end method
