.class public Lcom/google/android/apps/docs/fragment/NavigationFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "NavigationFragment.java"

# interfaces
.implements LMb;
.implements Ljm;


# instance fields
.field private a:LLZ;

.field public a:LMe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LXX;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Landroid/widget/ExpandableListView;

.field private a:Landroid/widget/Toast;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field public a:LiG;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Liv;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ljl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<[",
            "Lii;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 60
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LdL;

    return-object v0
.end method

.method private a([LLY;)V
    .registers 12
    .parameter

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->h()Z

    move-result v9

    .line 214
    new-instance v0, LLZ;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Llf;

    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LiG;

    iget-object v6, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljl;

    iget-object v7, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LXX;

    move-object v2, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v9}, LLZ;-><init>(Landroid/content/Context;[LLY;Ljava/lang/String;Llf;LiG;Ljl;LXX;LMb;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    new-instance v1, LMc;

    invoke-direct {v1, p0}, LMc;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    new-instance v1, LMd;

    invoke-direct {v1, p0, p1}, LMd;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;[LLY;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 266
    const/4 v0, 0x0

    :goto_3b
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    invoke-virtual {v1}, LLZ;->getGroupCount()I

    move-result v1

    if-ge v0, v1, :cond_53

    .line 267
    aget-object v1, p1, v0

    invoke-virtual {v1}, LLY;->a()Z

    move-result v1

    if-eqz v1, :cond_50

    .line 268
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 266
    :cond_50
    add-int/lit8 v0, v0, 0x1

    goto :goto_3b

    .line 271
    :cond_53
    return-void
.end method

.method private a()[LLY;
    .registers 13

    .prologue
    const/4 v7, 0x0

    .line 167
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 168
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->h()Z

    move-result v10

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, [Lii;

    .line 171
    array-length v11, v6

    move v8, v7

    :goto_15
    if-ge v8, v11, :cond_70

    aget-object v2, v6, v8

    .line 174
    sget-object v0, Lik;->a:Lik;

    invoke-virtual {v2, v0}, Lii;->a(Lik;)Z

    move-result v0

    if-nez v0, :cond_31

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->d()Z

    move-result v0

    if-eqz v0, :cond_64

    sget-object v0, Lik;->c:Lik;

    invoke-virtual {v2, v0}, Lii;->a(Lik;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 177
    :cond_31
    invoke-virtual {v2}, Lii;->a()Z

    move-result v3

    .line 179
    if-nez v10, :cond_47

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    sget-object v1, Lfb;->b:Lfb;

    invoke-virtual {v0, v1}, Lfb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 180
    :cond_47
    invoke-virtual {v2}, Lii;->a()I

    move-result v4

    .line 185
    :goto_4b
    new-instance v0, LLY;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v2}, Lii;->b()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lii;->a()LmK;

    move-result-object v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, LLY;-><init>(Ljava/lang/String;LmK;ZIZ)V

    .line 188
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_64
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_15

    .line 182
    :cond_68
    if-eqz v3, :cond_6e

    sget v0, Leg;->ic_arrow:I

    :goto_6c
    move v4, v0

    goto :goto_4b

    :cond_6e
    move v0, v7

    goto :goto_6c

    .line 192
    :cond_70
    new-array v0, v7, [LLY;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LLY;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LdL;

    return-object v0
.end method

.method private h()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LZL;->b(Landroid/content/res/Resources;)Z

    move-result v3

    .line 276
    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_20

    move v2, v0

    .line 277
    :goto_1b
    if-eqz v3, :cond_22

    if-eqz v2, :cond_22

    :goto_1f
    return v0

    :cond_20
    move v2, v1

    .line 276
    goto :goto_1b

    :cond_22
    move v0, v1

    .line 277
    goto :goto_1f
.end method

.method private p()V
    .registers 4

    .prologue
    .line 330
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_17

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 332
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    invoke-virtual {v2, v1}, LLZ;->a(Landroid/view/View;)V

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 334
    :cond_17
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 144
    sget v0, Lej;->navigation_pane:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 145
    sget v0, Leh;->navigation_folders:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->h()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 148
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lef;->navigation_panel_narrow_width:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v3, -0x2

    invoke-direct {v2, v0, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    :cond_47
    return-object v1
.end method

.method public a(IZI)Landroid/view/animation/Animation;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 317
    const/16 v0, 0x1001

    if-ne p1, v0, :cond_23

    .line 318
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 325
    :goto_1d
    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 326
    :goto_22
    return-object v0

    .line 319
    :cond_23
    const/16 v0, 0x2002

    if-ne p1, v0, :cond_37

    .line 320
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v1, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_1d

    .line 322
    :cond_37
    const/4 v0, 0x0

    goto :goto_22
.end method

.method public a()V
    .registers 4

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()[LLY;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LLZ;->a([LLY;Ljava/lang/String;)V

    .line 291
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->h()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljl;

    invoke-interface {v0}, Ljl;->a()LiQ;

    move-result-object v0

    invoke-interface {v0}, LiQ;->a()Ljava/lang/String;

    move-result-object v0

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Toast;

    if-eqz v1, :cond_36

    .line 294
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 298
    :goto_2d
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 300
    :cond_32
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->p()V

    .line 301
    return-void

    .line 296
    :cond_36
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Toast;

    goto :goto_2d
.end method

.method public a(ILLY;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 340
    invoke-virtual {p2}, LLY;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 349
    :goto_6
    return-void

    .line 343
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    .line 344
    if-eqz v0, :cond_15

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    goto :goto_6

    .line 347
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    goto :goto_6
.end method

.method public b()V
    .registers 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LLZ;

    invoke-virtual {v0}, LLZ;->notifyDataSetChanged()V

    .line 284
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->p()V

    .line 285
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 197
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->b(Landroid/os/Bundle;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljl;

    invoke-interface {v0, p0}, Ljl;->a(Ljm;)V

    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()[LLY;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a([LLY;)V

    .line 202
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 305
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 309
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()[LLY;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a([LLY;)V

    .line 310
    return-void
.end method
