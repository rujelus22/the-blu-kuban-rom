.class public Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "SpreadsheetFragment.java"

# interfaces
.implements LGZ;
.implements LHa;


# instance fields
.field private a:I

.field private a:LGY;

.field private a:LHT;

.field private a:LIb;

.field private a:LJA;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LGY;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

.field private b:Landroid/view/ViewGroup;

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LIb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:I

    return-void
.end method

.method private d(LJD;)V
    .registers 4
    .parameter

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:I

    invoke-virtual {p1}, LJD;->a()I

    move-result v1

    if-ne v0, v1, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    if-eqz v0, :cond_11

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->invalidate()V

    .line 134
    :cond_11
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    sget v0, LsF;->trix_spreadsheet:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->b:Landroid/view/ViewGroup;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(ILJA;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LJA;

    if-eqz v0, :cond_5

    .line 124
    :goto_4
    return-void

    .line 105
    :cond_5
    iput p1, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:I

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->b:Landroid/view/ViewGroup;

    sget v1, LsD;->loading_spinner:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 108
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 112
    invoke-virtual {p2}, LJA;->a()LJA;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LJA;

    .line 114
    new-instance v0, LHT;

    invoke-direct {v0, p1}, LHT;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LHT;

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LHT;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v1}, LGY;->a()[LJD;

    move-result-object v1

    invoke-virtual {v0, v1}, LHT;->a([LJD;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LHT;

    invoke-interface {v0, v1}, LGY;->a(LHa;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIb;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LIb;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LJA;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LHT;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LIb;

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->setActiveSheet(ILJA;LHT;LGY;LIb;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LIb;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LJA;

    invoke-virtual {v1}, LJA;->a()LJv;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0, v1, v2}, LIb;->a(LJv;Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v0, p0}, LGY;->b(LGZ;)V

    goto :goto_4
.end method

.method public a(LJD;)V
    .registers 2
    .parameter

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->d(LJD;)V

    .line 139
    return-void
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 96
    return-void
.end method

.method public b(LJD;)V
    .registers 2
    .parameter

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->d(LJD;)V

    .line 144
    return-void
.end method

.method public c(LJD;)V
    .registers 2
    .parameter

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->d(LJD;)V

    .line 149
    return-void
.end method

.method public g_()V
    .registers 1

    .prologue
    .line 128
    return-void
.end method

.method public h_()V
    .registers 3

    .prologue
    .line 55
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h_()V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->b:Landroid/view/ViewGroup;

    sget v1, LsD;->spreadsheet_view:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGY;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v0}, LGY;->a()LJA;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_30

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v1}, LGY;->a()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a(ILJA;)V

    .line 67
    :goto_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v0, p0}, LGY;->a(LHa;)V

    .line 69
    return-void

    .line 65
    :cond_30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v0, p0}, LGY;->a(LGZ;)V

    goto :goto_2a
.end method

.method public i_()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()V

    .line 74
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v0, p0}, LGY;->b(LHa;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LHT;

    invoke-interface {v0, v1}, LGY;->b(LHa;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    invoke-interface {v0, p0}, LGY;->b(LGZ;)V

    .line 79
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LGY;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LJA;

    if-eqz v0, :cond_26

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LJA;

    invoke-virtual {v0}, LJA;->a()V

    .line 83
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LJA;

    .line 86
    :cond_26
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LIb;

    if-eqz v0, :cond_31

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LIb;

    invoke-virtual {v0}, LIb;->e()V

    .line 88
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:LIb;

    .line 91
    :cond_31
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->i_()V

    .line 92
    return-void
.end method
