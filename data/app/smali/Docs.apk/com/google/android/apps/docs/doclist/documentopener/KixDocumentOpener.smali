.class public Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;
.super Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;
.source "KixDocumentOpener.java"


# instance fields
.field private final a:LKS;

.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LKS;)V
    .registers 3
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:LKS;

    .line 46
    return-void
.end method

.method private a(LkM;Landroid/os/Bundle;)Landroid/content/Intent;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-virtual {p1}, LkM;->a()Ljava/lang/String;

    move-result-object v4

    .line 65
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 66
    if-eqz p2, :cond_66

    const-string v0, "editMode"

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_66

    move v0, v1

    .line 67
    :goto_15
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LoY;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 69
    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:LKS;

    const-string v6, "kixEnableNativeEditor"

    invoke-interface {v5, v6, v1}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_68

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x8

    if-lt v1, v5, :cond_68

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:LKS;

    const-string v5, "kixDocumentUrlPattern"

    const-string v6, "/document/d/[^/]*/edit.*"

    invoke-interface {v1, v5, v6}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-virtual {v4, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_68

    .line 74
    invoke-virtual {p1}, LkM;->a()LkB;

    move-result-object v1

    invoke-virtual {v1}, LkB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LkM;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, LkM;->a()LkP;

    move-result-object v5

    invoke-static {v3, v1, v4, v5}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LkP;)Landroid/content/Intent;

    move-result-object v1

    .line 76
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 77
    if-eqz v2, :cond_68

    .line 78
    const-string v0, "KixDocumentOpener"

    const-string v2, "Opening the native Kix editor."

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 96
    :goto_65
    return-object v0

    :cond_66
    move v0, v2

    .line 66
    goto :goto_15

    .line 84
    :cond_68
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:LKS;

    const-string v2, "docEditPath"

    const-string v4, "/document/m"

    invoke-interface {v1, v2, v4}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    if-eqz v0, :cond_b3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b3

    .line 86
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 87
    invoke-static {v3, v1}, LoY;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 88
    const-string v1, "id"

    invoke-virtual {p1}, LkM;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 89
    const-string v1, "source"

    const-string v2, "cm"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 90
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 93
    :goto_99
    const-string v1, "KixDocumentOpener"

    const-string v2, "Opening generic WebView to edit Kix document."

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {p1}, LkM;->a()LkB;

    move-result-object v2

    invoke-virtual {v2}, LkB;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LkM;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_65

    :cond_b3
    move-object v0, v3

    goto :goto_99
.end method


# virtual methods
.method public a(Lpa;LkM;Landroid/os/Bundle;)Lnm;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a(LkM;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 54
    const-string v1, "resourceId"

    invoke-virtual {p2}, LkM;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v1, "userCanEdit"

    invoke-virtual {p2}, LkM;->k()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 56
    const-string v1, "editMode"

    const-string v2, "editMode"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 58
    new-instance v1, LpT;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/KixDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {p2}, LkM;->a()LkB;

    move-result-object v3

    invoke-virtual {v3}, LkB;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, v0}, LpT;-><init>(Landroid/content/Context;LpS;Ljava/lang/String;Landroid/content/Intent;)V

    .line 60
    return-object v1
.end method
