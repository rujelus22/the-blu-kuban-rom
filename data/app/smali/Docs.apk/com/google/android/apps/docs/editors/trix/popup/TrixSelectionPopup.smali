.class public abstract Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;
.super Lcom/google/android/apps/docs/editors/popup/SelectionPopup;
.source "TrixSelectionPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private a:LHu;

.field private a:LHv;

.field private a:Landroid/graphics/Point;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/popup/SelectionPopup;-><init>()V

    .line 31
    iput p1, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:I

    .line 32
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LEY;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_36

    const/4 v0, 0x1

    move v1, v0

    .line 72
    :goto_d
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_38

    .line 73
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 74
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    if-eqz v1, :cond_33

    instance-of v3, v0, Lcom/google/android/apps/docs/editors/popup/PopupItem;

    if-eqz v3, :cond_33

    .line 78
    check-cast v0, Lcom/google/android/apps/docs/editors/popup/PopupItem;

    .line 80
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a()Z

    move-result v3

    if-eqz v3, :cond_33

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->b()Z

    move-result v3

    if-eqz v3, :cond_33

    .line 81
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a(I)V

    .line 72
    :cond_33
    add-int/lit8 v2, v2, 0x2

    goto :goto_d

    :cond_36
    move v1, v2

    .line 70
    goto :goto_d

    .line 85
    :cond_38
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Point;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:Landroid/graphics/Point;

    return-object v0
.end method

.method protected a(II)Landroid/graphics/Point;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:LHv;

    if-eqz v0, :cond_b

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:LHv;

    invoke-interface {v0, p1, p2}, LHv;->a(II)Landroid/graphics/Point;

    move-result-object v0

    .line 97
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 4
    .parameter

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:I

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 60
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 61
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a(Landroid/view/ViewGroup;)V

    .line 65
    return-object v0

    .line 58
    :cond_15
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(LHu;)V
    .registers 2
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:LHu;

    .line 40
    return-void
.end method

.method public a(LHv;)V
    .registers 2
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:LHv;

    .line 44
    return-void
.end method

.method public a(Landroid/graphics/Point;)V
    .registers 2
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:Landroid/graphics/Point;

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->u()V

    .line 53
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;)V
    .registers 2
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    .line 36
    return-void
.end method

.method protected b()Landroid/view/View;
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 104
    instance-of v0, p1, Lcom/google/android/apps/docs/editors/popup/PopupItem;

    if-eqz v0, :cond_32

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/docs/editors/popup/PopupItem;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->c()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 106
    check-cast p1, Lcom/google/android/apps/docs/editors/popup/PopupItem;

    .line 107
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a()Landroid/view/ViewGroup;

    move-result-object v0

    .line 108
    if-nez v0, :cond_2e

    .line 109
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 112
    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->setNestedContentView(Landroid/view/ViewGroup;)V

    .line 113
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a(Landroid/view/ViewGroup;)V

    .line 116
    :cond_2e
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->c(Landroid/view/View;)V

    .line 123
    :goto_31
    return-void

    .line 120
    :cond_32
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:LHu;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/popup/TrixSelectionPopup;->a:LHu;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-interface {v0, v1}, LHu;->a(I)V

    goto :goto_31
.end method
