.class public abstract Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "BaseSlidePickerFragment.java"

# interfaces
.implements LRl;


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LQR;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LQk;

.field public a:LQo;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LRc;

.field public a:LRj;

.field public a:LRk;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 32
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQk;

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 127
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 129
    new-instance v1, LQm;

    invoke-direct {v1, p0, v0}, LQm;-><init>(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQk;

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQk;

    invoke-interface {v0, v1}, LQR;->a(LQS;)V

    .line 167
    return-void

    .line 125
    :cond_1c
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private q()V
    .registers 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQk;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQk;

    invoke-interface {v0, v1}, LQR;->b(LQS;)V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQk;

    .line 173
    return-void

    .line 170
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private r()V
    .registers 5

    .prologue
    .line 176
    new-instance v0, LRc;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LRc;-><init>(LQR;LRj;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRc;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRc;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a(Landroid/widget/ListAdapter;)V

    .line 178
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    if-eqz v0, :cond_c

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    invoke-interface {v0}, LRj;->b()V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    .line 117
    :cond_c
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    if-nez v0, :cond_14

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRk;

    invoke-interface {v0, p0}, LRk;->a(LRl;)LRj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRj;

    .line 101
    :cond_14
    return-void
.end method

.method protected abstract a(Landroid/widget/ListAdapter;)V
.end method

.method public abstract b(I)V
.end method

.method public c(I)V
    .registers 8
    .parameter

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LeQ;

    const-string v1, "punch"

    const-string v2, "webViewPunchPickSlide"

    const/4 v3, 0x0

    int-to-long v4, p1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQo;

    if-eqz v0, :cond_18

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQo;

    invoke-interface {v0, p1}, LQo;->b_(I)V

    .line 187
    :cond_18
    return-void
.end method

.method public h_()V
    .registers 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h_()V

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->r()V

    .line 69
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->p()V

    .line 70
    return-void
.end method

.method public i()V
    .registers 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 109
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->i()V

    .line 110
    return-void
.end method

.method public i_()V
    .registers 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->i_()V

    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->q()V

    .line 77
    return-void
.end method
