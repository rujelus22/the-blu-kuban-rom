.class public Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;
.super Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;
.source "UploadDialogFragment.java"

# interfaces
.implements Low;


# instance fields
.field private a:LTa;

.field private a:LTb;

.field public a:LTc;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LTe;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lfg;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;-><init>()V

    .line 153
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)LTa;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)LTb;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTb;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)LTe;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTe;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LdL;

    return-object v0
.end method

.method private p()V
    .registers 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTe;

    if-eqz v0, :cond_9

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTe;

    invoke-virtual {v0}, LTe;->notifyDataSetChanged()V

    .line 347
    :cond_9
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 8
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 234
    new-instance v0, LTe;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [LTd;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTb;

    aput-object v5, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, LTe;-><init>(Landroid/content/Context;Ljava/util/List;LSX;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTe;

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 238
    invoke-virtual {v0}, Lfb;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTe;

    const/4 v2, -0x1

    new-instance v3, LSX;

    invoke-direct {v3, p0}, LSX;-><init>(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)V

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 247
    const v0, 0x104000a

    new-instance v2, LSY;

    invoke-direct {v2, p0}, LSY;-><init>(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 256
    const/high16 v0, 0x104

    new-instance v2, LSZ;

    invoke-direct {v2, p0}, LSZ;-><init>(Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 264
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/accounts/Account;)V
    .registers 4
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, LTa;->a(Ljava/lang/String;)V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTb;

    invoke-virtual {v0}, LTb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a(Ljava/lang/String;)V

    .line 340
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->p()V

    .line 341
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 214
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a(Landroid/os/Bundle;)V

    .line 216
    if-eqz p1, :cond_35

    .line 218
    :goto_5
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    new-instance v1, LTa;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LTa;-><init>(LSX;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    .line 220
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    invoke-virtual {v1, v0}, LTa;->a(Ljava/lang/String;)V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pickFolderRequestCode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 223
    new-instance v1, LTb;

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    invoke-direct {v1, v2, v0}, LTb;-><init>(LTa;I)V

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTb;

    .line 224
    const-string v0, "collectionResourceId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a(Ljava/lang/String;)V

    .line 226
    return-void

    .line 216
    :cond_35
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_5
.end method

.method a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 301
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    invoke-virtual {v1}, LTa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 305
    const-string v1, "root"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 307
    invoke-virtual {v0}, Lfb;->b()LmK;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, LmK;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 326
    :goto_2b
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTb;

    invoke-virtual {v1, p1, v0}, LTb;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->p()V

    .line 328
    :goto_33
    return-void

    .line 310
    :cond_34
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Llf;

    invoke-interface {v1, v0, p1}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v0

    .line 311
    if-nez v0, :cond_54

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTb;

    invoke-virtual {v0}, LTb;->b()Ljava/lang/String;

    move-result-object v0

    .line 313
    if-eqz v0, :cond_4a

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_50

    .line 315
    :cond_4a
    const-string v0, "root"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a(Ljava/lang/String;)V

    goto :goto_33

    .line 318
    :cond_50
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a(Ljava/lang/String;)V

    goto :goto_33

    .line 322
    :cond_54
    invoke-virtual {v0}, LkH;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_2b
.end method

.method b()Ljava/lang/String;
    .registers 7

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 270
    invoke-virtual {v0}, Lfb;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a(I)Ljava/lang/String;

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:Lfg;

    sget v1, Lel;->dialog_upload_file_to:I

    invoke-virtual {v0, v1}, Lfg;->a(I)I

    move-result v0

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "numberOfFilesToUpload"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 274
    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 286
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->c(Landroid/os/Bundle;)V

    .line 288
    const-string v0, "accountName"

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTa;

    invoke-virtual {v1}, LTa;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v0, "collectionResourceId"

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTb;

    invoke-virtual {v1}, LTb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    return-void
.end method

.method public l_()V
    .registers 1

    .prologue
    .line 352
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 279
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadDialogFragment;->a:LTc;

    invoke-interface {v0}, LTc;->a()V

    .line 282
    return-void
.end method
