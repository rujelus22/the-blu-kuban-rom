.class public Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;
.super Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;
.source "NoDiscussionsStateMachineFragment.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;-><init>()V

    return-void
.end method


# virtual methods
.method a(Landroid/view/ViewGroup;)Landroid/view/animation/Animation;
    .registers 4
    .parameter

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->i()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lsy;->discussion_horizontal_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 52
    :goto_1c
    new-instance v1, Ltt;

    invoke-direct {v1, p0, p1}, Ltt;-><init>(Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;Landroid/view/ViewGroup;)V

    .line 71
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 73
    return-object v0

    .line 48
    :cond_25
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lsy;->discussion_vertical_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1c
.end method

.method public a()Ltm;
    .registers 2

    .prologue
    .line 24
    sget-object v0, Ltm;->a:Ltm;

    return-object v0
.end method

.method public g()V
    .registers 3

    .prologue
    .line 29
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->g()V

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;->a:LsT;

    invoke-interface {v0}, LsT;->b()V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 33
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;->a(Landroid/view/ViewGroup;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 35
    const-string v0, "NoDiscussionsStateMachineFragment"

    const-string v1, "onResume - starting animation"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    return-void
.end method
