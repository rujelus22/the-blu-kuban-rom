.class public Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;
.super Ljava/lang/Object;
.source "UnknownDocumentOpener.java"

# interfaces
.implements LoZ;


# instance fields
.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

.field private final a:LpY;


# direct methods
.method public constructor <init>(LpY;Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;)V
    .registers 3
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;->a:LpY;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Lpa;LkM;Landroid/os/Bundle;)LamQ;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpa;",
            "LkM;",
            "Landroid/os/Bundle;",
            ")",
            "LamQ",
            "<",
            "Lnm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    invoke-static {p3}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v0

    .line 39
    sget-object v1, LfS;->a:LfS;

    if-ne v0, v1, :cond_20

    .line 40
    invoke-virtual {p2}, LkM;->g()Ljava/lang/String;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;->a:LpY;

    invoke-virtual {v1, v0}, LpY;->a(Ljava/lang/Object;)LoZ;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_19

    .line 43
    invoke-interface {v0, p1, p2, p3}, LoZ;->a(Lpa;LkM;Landroid/os/Bundle;)LamQ;

    move-result-object v0

    .line 48
    :goto_18
    return-object v0

    .line 45
    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;->a(Lpa;LkM;Landroid/os/Bundle;)LamQ;

    move-result-object v0

    goto :goto_18

    .line 48
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/UnknownDocumentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;->a(Lpa;LkM;Landroid/os/Bundle;)LamQ;

    move-result-object v0

    goto :goto_18
.end method
