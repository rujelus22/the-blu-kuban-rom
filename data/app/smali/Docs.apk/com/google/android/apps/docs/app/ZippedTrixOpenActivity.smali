.class public Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;
.super Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;
.source "ZippedTrixOpenActivity.java"


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;-><init>()V

    .line 56
    return-void
.end method

.method public static a(Landroid/content/Context;LkY;)Landroid/content/Intent;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 192
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 193
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    invoke-virtual {p1, v0}, LkY;->a(Landroid/content/Intent;)V

    .line 195
    return-object v0
.end method

.method private a(Ljava/util/zip/ZipFile;)Ljava/util/List;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 124
    const-string v0, "resources/sheets.js"

    invoke-virtual {p1, v0}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v0

    .line 125
    if-nez v0, :cond_f

    .line 126
    const/4 v0, 0x0

    .line 144
    :goto_e
    return-object v0

    .line 129
    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a:LZS;

    invoke-virtual {p1, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    invoke-interface {v2, v0}, LZS;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 130
    new-instance v2, Ljava/lang/String;

    sget-object v3, Lagg;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 131
    const/16 v0, 0x3d

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 133
    new-instance v2, Lani;

    invoke-direct {v2}, Lani;-><init>()V

    .line 134
    invoke-virtual {v2, v0}, Lani;->a(Ljava/lang/String;)Land;

    move-result-object v0

    invoke-virtual {v0}, Land;->a()Lang;

    move-result-object v0

    .line 135
    const-string v2, "sheets"

    invoke-virtual {v0, v2}, Lang;->a(Ljava/lang/String;)Land;

    move-result-object v0

    invoke-virtual {v0}, Land;->a()Lanc;

    move-result-object v2

    .line 136
    const/4 v0, 0x0

    :goto_4e
    invoke-virtual {v2}, Lanc;->a()I

    move-result v3

    if-ge v0, v3, :cond_89

    .line 137
    invoke-virtual {v2, v0}, Lanc;->a(I)Land;

    move-result-object v3

    invoke-virtual {v3}, Land;->a()Lang;

    move-result-object v3

    .line 138
    const-string v4, "sheet"

    invoke-virtual {v3, v4}, Lang;->a(Ljava/lang/String;)Land;

    move-result-object v4

    invoke-virtual {v4}, Land;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 139
    const-string v5, "file"

    invoke-virtual {v3, v5}, Lang;->a(Ljava/lang/String;)Land;

    move-result-object v3

    invoke-virtual {v3}, Land;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lagg;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v5}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 141
    new-instance v5, Lit;

    invoke-direct {v5, v4, v3}, Lit;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_4e

    :cond_89
    move-object v0, v1

    .line 144
    goto :goto_e
.end method

.method private b(Ljava/util/zip/ZipFile;)Ljava/util/List;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 159
    invoke-virtual {p1}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v2

    .line 161
    :cond_9
    :goto_9
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 162
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    .line 163
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 165
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".html"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 170
    new-instance v4, Ljava/util/Scanner;

    invoke-virtual {p1, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    sget-object v5, Lagg;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v5}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 171
    const-string v0, "<title>.*</title>"

    const/16 v5, 0x1f4

    invoke-virtual {v4, v0, v5}, Ljava/util/Scanner;->findWithinHorizon(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 172
    if-nez v0, :cond_68

    .line 173
    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, ".html"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 182
    :goto_52
    :try_start_52
    new-instance v4, Lit;

    const-string v5, "UTF-8"

    invoke-static {v3, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v0, v3}, Lit;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_60
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_52 .. :try_end_60} :catch_61

    goto :goto_9

    .line 183
    :catch_61
    move-exception v0

    .line 184
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 176
    :cond_68
    const-string v4, "<title>"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const-string v6, "</title>"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_52

    .line 188
    :cond_7e
    return-object v1
.end method


# virtual methods
.method protected a(LkM;Ljava/io/File;)Landroid/content/Intent;
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 71
    invoke-virtual {p1}, LkM;->c()Ljava/lang/String;

    move-result-object v5

    .line 72
    new-instance v6, LVg;

    new-instance v0, LVq;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a:LZS;

    invoke-direct {v0, v1, p2}, LVq;-><init>(LZS;Ljava/io/File;)V

    invoke-direct {v6, v0}, LVg;-><init>(LVb;)V

    .line 74
    invoke-virtual {p0, v6, v5}, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a(LVb;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 76
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    new-instance v1, Ljava/util/zip/ZipFile;

    invoke-direct {v1, p2}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a:LKS;

    const-string v3, "offlineKixReadFromIndexJson"

    invoke-interface {v0, v3, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 81
    if-eqz v0, :cond_d9

    .line 82
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a(Ljava/util/zip/ZipFile;)Ljava/util/List;

    move-result-object v0

    .line 84
    :goto_2e
    if-nez v0, :cond_34

    .line 86
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->b(Ljava/util/zip/ZipFile;)Ljava/util/List;

    move-result-object v0

    .line 89
    :cond_34
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v1, v2

    :goto_39
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_69

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lit;

    .line 90
    const-string v10, "<li id=\"sheet%d\"><a href=\"%s\">%s</a></li>"

    const/4 v3, 0x3

    new-array v11, v3, [Ljava/lang/Object;

    add-int/lit8 v3, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v11, v2

    const/4 v1, 0x1

    invoke-static {v0}, Lit;->a(Lit;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v1

    const/4 v1, 0x2

    invoke-static {v0}, Lit;->b(Lit;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v1

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 92
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v3

    .line 93
    goto :goto_39

    .line 97
    :cond_69
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a:LZS;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lem;->trix_offline:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-interface {v1, v2}, LZS;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    sget-object v2, Lagg;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string v1, "TRIX_TITLE"

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TRIX_ZIP"

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "<li>TRIX_SHEETS</li>"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 103
    const-string v1, "trix_offline/index.html"

    sget-object v2, Lagg;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "text/html"

    invoke-virtual {v6, v1, v0, v2}, LVg;->a(Ljava/lang/String;[BLjava/lang/String;)V

    .line 105
    const-string v0, "trix_offline/header.png"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a:LZS;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lem;->trix_offline_header:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-interface {v1, v2}, LZS;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    const-string v2, "image/png"

    invoke-virtual {v6, v0, v1, v2}, LVg;->a(Ljava/lang/String;[BLjava/lang/String;)V

    .line 110
    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "trix_offline/index.html"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, v4, v5}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_d9
    move-object v0, v4

    goto/16 :goto_2e
.end method
