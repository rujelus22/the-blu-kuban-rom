.class public Lcom/google/android/apps/docs/app/CommentStreamActivity;
.super Lcom/google/android/apps/docs/app/BaseDialogActivity;
.source "CommentStreamActivity.java"


# instance fields
.field private a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private a:Lcom/google/android/apps/docs/view/TitleBar;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Lfp;

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/lang/Runnable;

.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private a:LkM;

.field public a:LlX;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lma;

.field private a:LoC;

.field private a:LoL;

.field public b:Landroid/os/Handler;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:LoL;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;-><init>()V

    .line 108
    new-instance v0, Lfq;

    invoke-direct {v0, p0}, Lfq;-><init>(Lcom/google/android/apps/docs/app/CommentStreamActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lfp;

    .line 221
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 222
    new-instance v0, Lft;

    invoke-direct {v0, p0}, Lft;-><init>(Lcom/google/android/apps/docs/app/CommentStreamActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Ljava/lang/Runnable;

    .line 253
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->i:Z

    return-void
.end method

.method public static a(Landroid/content/Context;LkY;Ljava/lang/String;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 291
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/CommentStreamActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 292
    invoke-virtual {p1, v0}, LkY;->a(Landroid/content/Intent;)V

    .line 293
    const-string v1, "documentTitle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Landroid/view/View;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)LkM;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LkM;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)Lma;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lma;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)LoL;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LoL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->f()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CommentStreamActivity;Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 256
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->i:Z

    if-nez v0, :cond_20

    move v0, v1

    :goto_6
    invoke-static {v0}, Lagu;->b(Z)V

    .line 257
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 258
    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v2, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LoC;

    invoke-virtual {v2, p2}, LoC;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 260
    iget-object v2, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->b:LoL;

    invoke-interface {v2, v0}, LoL;->a(Landroid/os/Bundle;)V

    .line 261
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->i:Z

    .line 262
    return-void

    .line 256
    :cond_20
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/CommentStreamActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->g()V

    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->i:Z

    if-eqz v0, :cond_c

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->b:LoL;

    invoke-interface {v0}, LoL;->b()V

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->i:Z

    .line 269
    :cond_c
    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->f()V

    .line 277
    sget v0, Len;->comment_post_error:I

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a()Lo;

    move-result-object v1

    invoke-virtual {v1}, Lo;->a()Lz;

    move-result-object v1

    sget v2, Leh;->comments_activity_holder:I

    invoke-virtual {v1, v2, v0}, Lz;->b(ILandroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 281
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 159
    const-class v0, Lfp;

    if-ne p1, v0, :cond_7

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lfp;

    .line 162
    :goto_6
    return-object v0

    :cond_7
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_6
.end method

.method protected d()V
    .registers 2

    .prologue
    .line 216
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->d()V

    .line 218
    sget v0, Leh;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    .line 219
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 167
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lgl;

    sget-object v1, Lgi;->b:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 170
    sget v0, Lej;->comments_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->setContentView(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LeQ;

    const-string v1, "/comments"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 176
    sget v0, Lec;->quick_action_slide:I

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 177
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 178
    new-instance v1, LoE;

    new-instance v2, Lfu;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lfu;-><init>(Lcom/google/android/apps/docs/app/CommentStreamActivity;Lfq;)V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LoE;-><init>(LoH;Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 180
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LoL;

    .line 183
    new-instance v0, LoC;

    invoke-direct {v0}, LoC;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LoC;

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LoC;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->b:LoL;

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 187
    invoke-static {v0}, LkY;->a(Landroid/content/Intent;)LkY;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Llf;

    iget-object v2, v0, LkY;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 189
    iget-object v2, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Llf;

    iget-object v3, v0, LkY;->b:Ljava/lang/String;

    invoke-interface {v2, v1, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LkM;

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LkM;

    if-nez v1, :cond_92

    .line 191
    const-string v1, "CommentStreamActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Document is null, failing out, for resourceId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, LkY;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->finish()V

    .line 212
    :goto_91
    return-void

    .line 197
    :cond_92
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LkM;

    invoke-virtual {v0}, LkM;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-interface {v0, v1}, LMM;->a(LMZ;)V

    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LkM;

    invoke-virtual {v1}, LkM;->c()Ljava/lang/String;

    move-result-object v1

    sget v2, Len;->menu_comments:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LMM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    sget v0, Len;->comment_loading_document:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfs;

    invoke-direct {v1, p0}, Lfs;-><init>(Lcom/google/android/apps/docs/app/CommentStreamActivity;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LlX;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LkM;

    invoke-virtual {v1}, LkM;->i()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LkM;

    invoke-virtual {v2}, LkM;->a()LkB;

    move-result-object v2

    invoke-virtual {v2}, LkB;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LlX;->a(Ljava/lang/String;Ljava/lang/String;)Lma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lma;

    goto :goto_91
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 249
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onPause()V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lma;->b(Ljava/lang/Runnable;)Z

    .line 251
    return-void
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 243
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onResume()V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lma;->a(Ljava/lang/Runnable;)Z

    .line 245
    return-void
.end method
