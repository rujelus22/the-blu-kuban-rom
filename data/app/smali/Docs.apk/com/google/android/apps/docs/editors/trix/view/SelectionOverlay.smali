.class public Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;
.super Landroid/view/View;
.source "SelectionOverlay.java"


# instance fields
.field private a:LJb;

.field private a:Landroid/graphics/Rect;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method private b()Landroid/graphics/Rect;
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:LJb;

    invoke-virtual {v0}, LJb;->a()LJl;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, LJl;->a()Z

    move-result v1

    if-eqz v1, :cond_b3

    .line 60
    invoke-virtual {v0}, LJl;->a()LIX;

    move-result-object v0

    invoke-virtual {v0}, LIX;->a()LJq;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, LJq;->a()I

    move-result v2

    invoke-virtual {v0}, LJq;->b()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(II)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LJb;

    .line 66
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, LJq;->c()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0}, LJq;->d()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(II)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    .line 68
    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LJb;

    .line 70
    invoke-virtual {v0}, LJq;->a()I

    move-result v3

    invoke-virtual {v0}, LJq;->b()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(II)LJq;

    move-result-object v3

    .line 71
    invoke-virtual {v0}, LJq;->c()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0}, LJq;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(II)LJq;

    move-result-object v0

    .line 74
    new-instance v4, Landroid/graphics/Point;

    invoke-virtual {v3}, LJq;->a()I

    move-result v5

    invoke-virtual {v3}, LJq;->b()I

    move-result v3

    invoke-direct {v4, v5, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 75
    new-instance v3, Landroid/graphics/Point;

    invoke-virtual {v0}, LJq;->c()I

    move-result v5

    invoke-virtual {v0}, LJq;->d()I

    move-result v0

    invoke-direct {v3, v5, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 77
    iget v0, v4, Landroid/graphics/Point;->x:I

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v4, Landroid/graphics/Point;->x:I

    .line 78
    iget v0, v4, Landroid/graphics/Point;->y:I

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v4, Landroid/graphics/Point;->y:I

    .line 79
    iget v0, v3, Landroid/graphics/Point;->x:I

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v3, Landroid/graphics/Point;->x:I

    .line 80
    iget v0, v3, Landroid/graphics/Point;->y:I

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v3, Landroid/graphics/Point;->y:I

    .line 82
    new-instance v0, Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Point;->x:I

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v6

    add-int/2addr v5, v6

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v1

    add-int/2addr v1, v4

    iget v4, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v6

    add-int/2addr v4, v6

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v2

    add-int/2addr v2, v3

    invoke-direct {v0, v5, v1, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 88
    :goto_b2
    return-object v0

    :cond_b3
    const/4 v0, 0x0

    goto :goto_b2
.end method


# virtual methods
.method public a()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    return-object v0
.end method

.method public a(LJb;Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:LJb;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    .line 51
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter

    .prologue
    const/high16 v2, 0x4000

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->b()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_40

    .line 96
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 97
    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    const/high16 v1, 0x4040

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 99
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 100
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 117
    :cond_40
    return-void
.end method
