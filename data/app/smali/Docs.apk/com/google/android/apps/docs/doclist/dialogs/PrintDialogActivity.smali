.class public Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "PrintDialogActivity.java"


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/view/View;

.field private a:Landroid/webkit/WebView;

.field private a:Landroid/widget/Button;

.field private a:LdG;

.field public a:Ljava/lang/Class;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DocListActivity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:LkY;

.field private a:LoB;

.field private final a:Lqd;

.field private a:Lqe;

.field private final b:Landroid/os/Handler;

.field private b:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 66
    const-string v0, "https://www.google.com/cloudprint/dialog.html?skin=holo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->b:Landroid/os/Handler;

    .line 364
    new-instance v0, Loy;

    invoke-direct {v0, p0}, Loy;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Lqd;

    return-void
.end method

.method public static a(Landroid/content/Context;LkO;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 282
    invoke-static {p1}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(LkO;)Ljava/lang/String;

    move-result-object v1

    .line 284
    if-nez v1, :cond_8

    .line 285
    const/4 v0, 0x0

    .line 294
    :goto_7
    return-object v0

    .line 288
    :cond_8
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 290
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    const-string v1, "documentTitle"

    invoke-virtual {p1}, LkO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    invoke-virtual {p1}, LkO;->a()LkY;

    move-result-object v1

    invoke-virtual {v1, v0}, LkY;->a(Landroid/content/Intent;)V

    goto :goto_7
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Landroid/webkit/WebView;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)LdG;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdG;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;LdG;)LdG;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdG;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LKS;

    const-string v1, "printUrl"

    sget-object v2, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 192
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 193
    const-string v2, "hl"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 194
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 195
    const-string v1, "contentType"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 196
    const-string v1, "content"

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LkY;

    iget-object v2, v2, LkY;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 198
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LkO;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 262
    invoke-virtual {p0}, LkO;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    .line 263
    const/4 v0, 0x0

    .line 277
    :goto_7
    return-object v0

    .line 266
    :cond_8
    invoke-virtual {p0}, LkO;->a()LkP;

    move-result-object v0

    .line 268
    sget-object v1, LkP;->a:LkP;

    if-ne v0, v1, :cond_13

    .line 269
    const-string v0, "google.kix"

    goto :goto_7

    .line 270
    :cond_13
    sget-object v1, LkP;->d:LkP;

    if-ne v0, v1, :cond_1a

    .line 271
    const-string v0, "google.spreadsheet"

    goto :goto_7

    .line 272
    :cond_1a
    sget-object v1, LkP;->c:LkP;

    if-ne v0, v1, :cond_21

    .line 273
    const-string v0, "google.presentation"

    goto :goto_7

    .line 274
    :cond_21
    sget-object v1, LkP;->e:LkP;

    if-ne v0, v1, :cond_28

    .line 275
    const-string v0, "google.drawing"

    goto :goto_7

    .line 277
    :cond_28
    const-string v0, "google.drive"

    goto :goto_7
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Lqe;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Lqe;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;Lqe;)Lqe;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Lqe;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .registers 10
    .parameter

    .prologue
    const-wide v6, 0x3fc999999999999aL

    const-wide v4, 0x3fa999999999999aL

    .line 202
    .line 206
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    .line 208
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 209
    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    .line 211
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_52

    .line 212
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 213
    iget v2, v0, Landroid/graphics/Point;->x:I

    .line 214
    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 222
    invoke-static {v1}, LZL;->f(Landroid/content/res/Resources;)Z

    move-result v3

    .line 223
    invoke-static {v1}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v1

    .line 225
    if-eqz v3, :cond_53

    if-eqz v1, :cond_53

    .line 226
    int-to-double v0, v0

    const-wide v3, 0x3fc3333333333333L

    mul-double/2addr v0, v3

    double-to-int v1, v0

    .line 227
    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v0, v2

    .line 239
    :goto_46
    invoke-static {p1, v0}, LacF;->a(Landroid/view/View;I)V

    .line 240
    invoke-static {p1, v1}, LacF;->c(Landroid/view/View;I)V

    .line 241
    invoke-static {p1, v0}, LacF;->b(Landroid/view/View;I)V

    .line 242
    invoke-static {p1, v1}, LacF;->d(Landroid/view/View;I)V

    .line 243
    :cond_52
    return-void

    .line 228
    :cond_53
    if-eqz v3, :cond_63

    if-nez v1, :cond_63

    .line 229
    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v1, v0

    .line 230
    int-to-double v2, v2

    const-wide v4, 0x3fb999999999999aL

    mul-double/2addr v2, v4

    double-to-int v0, v2

    goto :goto_46

    .line 231
    :cond_63
    if-nez v3, :cond_6e

    if-eqz v1, :cond_6e

    .line 232
    int-to-double v0, v0

    mul-double/2addr v0, v6

    double-to-int v1, v0

    .line 233
    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v0, v2

    goto :goto_46

    .line 235
    :cond_6e
    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v1, v0

    .line 236
    int-to-double v2, v2

    mul-double/2addr v2, v4

    double-to-int v0, v2

    goto :goto_46
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->f()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->b(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 435
    const-string v0, "PrintDialogActivity"

    const-string v1, "showUrl(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    if-eqz p1, :cond_18

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LoB;

    invoke-virtual {v0, p1}, LoB;->a(Ljava/lang/String;)V

    .line 440
    :cond_18
    return-void
.end method

.method private b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 252
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->b:Landroid/view/View;

    if-eqz p1, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 253
    return-void

    .line 252
    :cond_9
    const/4 v0, 0x4

    goto :goto_5
.end method

.method private f()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 443
    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->b(Z)V

    .line 444
    sget v0, Len;->print_error:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 445
    const-string v1, "PrintDialogActivity"

    const-string v2, "showErrorMessage - %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LZM;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/webkit/WebView;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, LZM;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 447
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 247
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Landroid/view/View;)V

    .line 249
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 14
    .parameter

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x1

    .line 125
    const-string v0, "PrintDialogActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 130
    const-string v2, "documentTitle"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-static {v0}, LkY;->a(Landroid/content/Intent;)LkY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LkY;

    .line 133
    const-string v0, "PrintDialogActivity"

    const-string v3, "Printing %s [resourceId=%s, type=%s]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v8

    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LkY;

    iget-object v5, v5, LkY;->b:Ljava/lang/String;

    aput-object v5, v4, v11

    const/4 v5, 0x2

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Ljava/lang/String;

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lej;->print_dialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 141
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->setContentView(Landroid/view/View;)V

    .line 143
    sget v1, Leh;->progress_block:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->b:Landroid/view/View;

    .line 147
    sget v1, Leh;->dialog_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/view/View;

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Landroid/view/View;)V

    .line 150
    sget v1, Leh;->cancel_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/widget/Button;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/widget/Button;

    new-instance v1, Lox;

    invoke-direct {v1, p0}, Lox;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a()Lo;

    move-result-object v0

    .line 160
    sget v1, Leh;->webview_fragment:I

    invoke-virtual {v0, v1}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/WebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->b()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/webkit/WebView;

    .line 163
    new-instance v0, LoB;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Lqd;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LkY;

    iget-object v4, v1, LkY;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LKS;

    iget-object v6, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Ljava/lang/Class;

    iget-object v7, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LPm;

    const-string v1, "webview"

    invoke-virtual {p0, v1, v8}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LNe;

    iget-object v10, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->b:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v10}, LoB;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;Landroid/content/Context;Lqd;Ljava/lang/String;LKS;Ljava/lang/Class;LPm;Landroid/content/SharedPreferences;LNe;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LoB;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LoB;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 176
    invoke-virtual {v0, v11}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Landroid/webkit/WebView;

    new-instance v1, LoA;

    invoke-direct {v1, p0}, LoA;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)V

    const-string v2, "AndroidPrintDialog"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-direct {p0, v11}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->b(Z)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public onDestroy()V
    .registers 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdG;

    if-eqz v0, :cond_11

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdG;

    invoke-virtual {v0}, LdG;->a()V

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a:LdG;

    .line 431
    :cond_11
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 432
    return-void
.end method
