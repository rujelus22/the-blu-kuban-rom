.class public abstract Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;
.super Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;
.source "ContactSharingDialogFragment.java"


# static fields
.field private static final a:LeG;


# instance fields
.field private a:LUB;

.field private a:Laji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laji",
            "<",
            "LTP;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lgq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgq",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation
.end field

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 36
    sget-object v0, LeG;->b:LeG;

    sput-object v0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LeG;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;-><init>()V

    return-void
.end method

.method private q()V
    .registers 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Lgq;

    invoke-virtual {v0}, Lgq;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUq;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, LUq;->a(Ljava/lang/String;)LUB;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LUB;

    .line 145
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 93
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->d(Z)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "options"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Laji;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Laji;

    .line 99
    new-instance v0, Lgq;

    const-class v1, LUq;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b:Laoz;

    invoke-direct {v0, v1, v2}, Lgq;-><init>(Ljava/lang/Class;Laoz;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Lgq;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Lgq;

    invoke-virtual {v0}, Lgq;->a()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2d

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()V

    .line 137
    :goto_2c
    return-void

    .line 109
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "shareeAccountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c:Ljava/lang/String;

    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->q()V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LUB;

    if-nez v0, :cond_55

    .line 113
    const-string v0, "ContactSharingDialogFragment"

    const-string v1, "No contact exists in Acl list with email address \"%s\"."

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()V

    goto :goto_2c

    .line 119
    :cond_55
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LUB;

    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-virtual {v0}, LUp;->a()LeG;

    move-result-object v0

    .line 122
    sget-object v1, LeG;->f:LeG;

    if-ne v0, v1, :cond_68

    .line 123
    sget-object v0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LeG;

    .line 126
    :cond_68
    invoke-static {v0}, LTP;->a(LeG;)LTP;

    move-result-object v1

    .line 127
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Laji;

    invoke-virtual {v2, v1}, Laji;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 128
    if-gez v1, :cond_8c

    .line 132
    invoke-virtual {v0}, LeG;->a()LeI;

    move-result-object v0

    new-array v1, v4, [LeD;

    invoke-static {v0, v1}, LeG;->a(LeI;[LeD;)LeG;

    move-result-object v0

    .line 133
    invoke-static {v0}, LTP;->a(LeG;)LTP;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Laji;

    invoke-virtual {v1, v0}, Laji;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 136
    :goto_88
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c(I)V

    goto :goto_2c

    :cond_8c
    move v0, v1

    goto :goto_88
.end method

.method public a(LkY;LZj;Llf;Landroid/content/Context;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b()Z

    move-result v0

    if-nez v0, :cond_33

    const/4 v0, 0x1

    :goto_8
    invoke-static {v0}, Lagu;->a(Z)V

    .line 73
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 75
    invoke-static {p1, p2, p3}, LTP;->a(LkY;LZj;Llf;)Laji;

    move-result-object v3

    .line 79
    invoke-static {p4, v3}, LTP;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 82
    sget v4, Len;->dialog_contact_sharing:I

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v2, v4, v0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a(Landroid/os/Bundle;I[Ljava/lang/String;)V

    .line 85
    const-string v0, "shareeAccountName"

    invoke-virtual {v2, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v0, "options"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 87
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->d(Landroid/os/Bundle;)V

    .line 88
    return-void

    :cond_33
    move v0, v1

    .line 72
    goto :goto_8
.end method

.method public b(I)V
    .registers 6
    .parameter

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->q()V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LUB;

    if-nez v0, :cond_f

    .line 151
    const-string v0, "ContactSharingDialogFragment"

    const-string v1, "Selected item in contact sharing dialog is not defined."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_e
    :goto_e
    return-void

    .line 154
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LUB;

    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v1

    .line 155
    invoke-static {v1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Laji;

    invoke-virtual {v0, p1}, Laji;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTP;

    .line 157
    invoke-virtual {v0}, LTP;->a()LeG;

    move-result-object v0

    .line 158
    invoke-virtual {v1}, LUp;->a()LeG;

    move-result-object v2

    if-eq v0, v2, :cond_e

    .line 161
    new-instance v2, LUp;

    invoke-virtual {v1}, LUp;->b()Z

    move-result v3

    invoke-direct {v2, v1, v0, v3}, LUp;-><init>(LUp;LeG;Z)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LUB;

    invoke-virtual {v0, v2}, LUB;->a(LUp;)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->p()V

    goto :goto_e
.end method

.method protected abstract p()V
.end method
