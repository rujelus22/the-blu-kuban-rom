.class public Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "MenuHandler.java"

# interfaces
.implements LyH;


# static fields
.field private static a:I


# instance fields
.field private a:J

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Landroid/os/Handler;

.field public final a:Landroid/view/View$OnClickListener;

.field protected a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LBG;",
            "Landroid/widget/ToggleButton;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LuI;

.field private a:LuK;

.field private a:Lun;

.field private a:Luw;

.field public a:LzH;

.field protected a:Z

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/widget/ToggleButton;",
            "LBG;",
            ">;"
        }
    .end annotation
.end field

.field private b:LuK;

.field private b:Z

.field private c:LuK;

.field private c:Z

.field private d:Landroid/view/View;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 84
    const/16 v0, 0x12c

    sput v0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:I

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/os/Handler;

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:Z

    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Z

    .line 159
    new-instance v0, LBn;

    invoke-direct {v0, p0}, LBn;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/view/View$OnClickListener;

    .line 202
    new-instance v0, LBy;

    invoke-direct {v0, p0}, LBy;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuI;

    .line 211
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Ljava/util/Map;

    .line 213
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Ljava/util/Map;

    return-void
.end method

.method private a()J
    .registers 3

    .prologue
    .line 425
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private a()LCh;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v0

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v1

    .line 435
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v2, v0, v1}, LzH;->a(II)LCh;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Ljava/util/Map;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)Lun;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lun;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Lun;)Lun;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lun;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)Luw;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Luw;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Luw;)Luw;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Luw;

    return-object p1
.end method

.method private a(J)V
    .registers 5
    .parameter

    .prologue
    .line 415
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Z

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/os/Handler;

    new-instance v1, LBF;

    invoke-direct {v1, p0}, LBF;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 422
    return-void
.end method

.method private a(LBG;)V
    .registers 4
    .parameter

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:Z

    if-eqz v0, :cond_5

    .line 356
    :goto_4
    return-void

    .line 339
    :cond_5
    sget-object v0, LBx;->a:[I

    invoke-virtual {p1}, LBG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_32

    .line 355
    :goto_10
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->u()V

    goto :goto_4

    .line 341
    :pswitch_14
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->e()V

    goto :goto_10

    .line 344
    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->f()V

    goto :goto_10

    .line 347
    :pswitch_20
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->g()V

    goto :goto_10

    .line 350
    :pswitch_26
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->h()V

    goto :goto_10

    .line 353
    :pswitch_2c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->i()V

    goto :goto_10

    .line 339
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_14
        :pswitch_1a
        :pswitch_20
        :pswitch_26
        :pswitch_2c
    .end packed-switch
.end method

.method private a(LBG;LCh;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    .line 540
    if-eqz p2, :cond_c

    if-nez v0, :cond_d

    .line 546
    :cond_c
    :goto_c
    return-void

    .line 544
    :cond_d
    invoke-virtual {p1, p2}, LBG;->a(LCh;)Z

    move-result v1

    .line 545
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto :goto_c
.end method

.method private a(LBG;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    invoke-static {p1}, LBG;->a(LBG;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1}, LBG;->b(LBG;)I

    move-result v2

    sget v3, LsA;->toolbar_regular_background:I

    sget v4, LsA;->toolbar_selected_background:I

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, LJV;->a(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 480
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 481
    if-eqz p2, :cond_2c

    .line 482
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    :cond_2c
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 486
    return-void
.end method

.method private a(Landroid/widget/ToggleButton;)V
    .registers 6
    .parameter

    .prologue
    .line 663
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->v()V

    .line 664
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    .line 666
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    if-nez v1, :cond_4b

    .line 667
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->i()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 668
    new-instance v1, Lcom/google/android/apps/docs/editors/dropdownmenu/FontDropDownMenu;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/FontDropDownMenu;-><init>()V

    .line 669
    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    const-string v2, "FontSelectionPopup"

    invoke-virtual {v0, v1, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 670
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    .line 677
    :goto_29
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/os/Handler;

    new-instance v1, LBp;

    invoke-direct {v1, p0, p1}, LBp;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 689
    :goto_33
    return-void

    .line 672
    :cond_34
    new-instance v1, Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/FontPhonePopupMenu;-><init>()V

    .line 673
    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    sget v2, LsD;->phone_font_selection_popup_holder:I

    const-string v3, "FontSelectionPopup"

    invoke-virtual {v0, v2, v1, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 675
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    goto :goto_29

    .line 687
    :cond_4b
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d(Landroid/widget/ToggleButton;)V

    goto :goto_33
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V
    .registers 1
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->w()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Landroid/widget/ToggleButton;)V

    return-void
.end method

.method private a(LuK;)V
    .registers 3
    .parameter

    .prologue
    .line 855
    if-eqz p1, :cond_b

    invoke-interface {p1}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 856
    invoke-interface {p1}, LuK;->p()V

    .line 858
    :cond_b
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)Z
    .registers 2
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->i()Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    return-object v0
.end method

.method private b(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 532
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsA;->toolbar_regular_background:I

    sget v2, LsA;->toolbar_selected_background:I

    const/4 v3, 0x1

    invoke-static {v0, p1, v1, v2, v3}, LJV;->a(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 534
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    invoke-virtual {v1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 535
    return-void
.end method

.method private b(Landroid/widget/ToggleButton;)V
    .registers 6
    .parameter

    .prologue
    .line 692
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->v()V

    .line 693
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    .line 694
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    if-nez v1, :cond_4b

    .line 695
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->i()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 696
    new-instance v1, Lcom/google/android/apps/docs/editors/dropdownmenu/ColorDropDownMenu;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/ColorDropDownMenu;-><init>()V

    .line 697
    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    const-string v2, "ColorPopup"

    invoke-virtual {v0, v1, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 698
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    .line 705
    :goto_29
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/os/Handler;

    new-instance v1, LBq;

    invoke-direct {v1, p0, p1}, LBq;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 716
    :goto_33
    return-void

    .line 700
    :cond_34
    new-instance v1, Lcom/google/android/apps/docs/editors/dropdownmenu/ColorPhonePopupMenu;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/ColorPhonePopupMenu;-><init>()V

    .line 701
    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    sget v2, LsD;->phone_color_popup_holder:I

    const-string v3, "ColorPopup"

    invoke-virtual {v0, v2, v1, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 703
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    goto :goto_29

    .line 714
    :cond_4b
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->e(Landroid/widget/ToggleButton;)V

    goto :goto_33
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V
    .registers 1
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->x()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(Landroid/widget/ToggleButton;)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)LuK;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    return-object v0
.end method

.method private c(I)V
    .registers 8
    .parameter

    .prologue
    .line 870
    packed-switch p1, :pswitch_data_34

    .line 883
    sget v0, LsC;->action_text_align_left:I

    move v1, v0

    .line 886
    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    if-eqz v0, :cond_26

    .line 887
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    sget v2, LsD;->toolbar_alignment_button:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    .line 888
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsA;->toolbar_regular_background:I

    sget v4, LsA;->toolbar_selected_background:I

    const/4 v5, 0x0

    invoke-static {v2, v1, v3, v4, v5}, LJV;->a(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 890
    if-eqz v0, :cond_26

    .line 891
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 894
    :cond_26
    return-void

    .line 872
    :pswitch_27
    sget v0, LsC;->action_text_align_center:I

    move v1, v0

    .line 873
    goto :goto_6

    .line 875
    :pswitch_2b
    sget v0, LsC;->action_text_align_right:I

    move v1, v0

    .line 876
    goto :goto_6

    .line 878
    :pswitch_2f
    sget v0, LsC;->action_text_align_justified:I

    move v1, v0

    .line 879
    goto :goto_6

    .line 870
    nop

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_27
        :pswitch_2b
        :pswitch_2f
    .end packed-switch
.end method

.method private c(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBG;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;)V

    .line 333
    return-void
.end method

.method private c(Landroid/widget/ToggleButton;)V
    .registers 6
    .parameter

    .prologue
    .line 719
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->v()V

    .line 720
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    .line 721
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    if-nez v1, :cond_4b

    .line 722
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->i()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 723
    new-instance v1, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentDropDownMenu;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentDropDownMenu;-><init>()V

    .line 724
    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    const-string v2, "AlignmentPopup"

    invoke-virtual {v0, v1, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 725
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    .line 732
    :goto_29
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/os/Handler;

    new-instance v1, LBr;

    invoke-direct {v1, p0, p1}, LBr;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 741
    :goto_33
    return-void

    .line 727
    :cond_34
    new-instance v1, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentPhonePopupMenu;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentPhonePopupMenu;-><init>()V

    .line 728
    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    sget v2, LsD;->phone_alignment_popup_holder:I

    const-string v3, "AlignmentPopup"

    invoke-virtual {v0, v2, v1, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 730
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    goto :goto_29

    .line 739
    :cond_4b
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->f(Landroid/widget/ToggleButton;)V

    goto :goto_33
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V
    .registers 1
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->t()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(Landroid/widget/ToggleButton;)V

    return-void
.end method

.method private d(Landroid/widget/ToggleButton;)V
    .registers 5
    .parameter

    .prologue
    .line 759
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->a()LCh;

    move-result-object v1

    .line 760
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Luw;

    sget-object v0, LCi;->h:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Luw;->a(Ljava/lang/String;)V

    .line 761
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Luw;

    sget-object v0, LCi;->i:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Luw;->a(Ljava/lang/Integer;)V

    .line 763
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Luw;

    new-instance v1, LBs;

    invoke-direct {v1, p0}, LBs;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {v0, v1}, Luw;->a(LuD;)V

    .line 779
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    new-instance v1, LBt;

    invoke-direct {v1, p0, p1}, LBt;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    invoke-interface {v0, v1}, LuK;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 786
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-interface {v0, p1}, LuK;->b(Landroid/view/View;)V

    .line 787
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-interface {v0}, LuK;->a()V

    .line 788
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 789
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V
    .registers 1
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->u()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d(Landroid/widget/ToggleButton;)V

    return-void
.end method

.method private e(Landroid/widget/ToggleButton;)V
    .registers 5
    .parameter

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->a()LCh;

    move-result-object v1

    .line 793
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lun;

    sget-object v0, LCi;->d:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lun;->b(Ljava/lang/String;)V

    .line 794
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lun;

    sget-object v0, LCi;->e:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lun;->a(Ljava/lang/String;)V

    .line 795
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lun;

    new-instance v1, LBu;

    invoke-direct {v1, p0}, LBu;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {v0, v1}, Lun;->a(Lur;)V

    .line 809
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    new-instance v1, LBv;

    invoke-direct {v1, p0, p1}, LBv;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    invoke-interface {v0, v1}, LuK;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-interface {v0, p1}, LuK;->b(Landroid/view/View;)V

    .line 816
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-interface {v0}, LuK;->a()V

    .line 817
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 818
    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->e(Landroid/widget/ToggleButton;)V

    return-void
.end method

.method private f(Landroid/widget/ToggleButton;)V
    .registers 5
    .parameter

    .prologue
    .line 821
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()LCh;

    move-result-object v0

    .line 822
    sget-object v1, LCi;->g:LCi;

    invoke-virtual {v0, v1}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 823
    if-nez v0, :cond_3e

    const/4 v0, -0x1

    move v1, v0

    .line 824
    :goto_10
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->i()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 825
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    check-cast v0, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentDropDownMenu;

    .line 826
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentDropDownMenu;->b(I)V

    .line 827
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuI;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentDropDownMenu;->a(LuI;)V

    .line 833
    :goto_22
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(I)V

    .line 834
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    new-instance v1, LBw;

    invoke-direct {v1, p0, p1}, LBw;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V

    invoke-interface {v0, v1}, LuK;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-interface {v0, p1}, LuK;->b(Landroid/view/View;)V

    .line 841
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-interface {v0}, LuK;->a()V

    .line 842
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 843
    return-void

    .line 823
    :cond_3e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v1, v0

    goto :goto_10

    .line 829
    :cond_44
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    check-cast v0, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentPhonePopupMenu;

    .line 830
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentPhonePopupMenu;->c(I)V

    .line 831
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuI;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentPhonePopupMenu;->a(LuI;)V

    goto :goto_22
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Landroid/widget/ToggleButton;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->f(Landroid/widget/ToggleButton;)V

    return-void
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 897
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->d(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method private t()V
    .registers 5

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Z

    if-nez v0, :cond_d

    .line 412
    :cond_c
    :goto_c
    return-void

    .line 400
    :cond_d
    iget-wide v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:J

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 401
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1e

    .line 404
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(J)V

    goto :goto_c

    .line 408
    :cond_1e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Z

    .line 411
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->u()V

    goto :goto_c
.end method

.method private u()V
    .registers 4

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->a()LCh;

    move-result-object v1

    .line 450
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LCh;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 452
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lun;

    sget-object v0, LCi;->d:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lun;->b(Ljava/lang/String;)V

    .line 453
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lun;

    sget-object v0, LCi;->e:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lun;->a(Ljava/lang/String;)V

    .line 456
    :cond_2f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    if-eqz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 457
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Luw;

    sget-object v0, LCi;->h:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Luw;->a(Ljava/lang/String;)V

    .line 458
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Luw;

    sget-object v0, LCi;->i:LCi;

    invoke-virtual {v1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Luw;->a(Ljava/lang/Integer;)V

    .line 461
    :cond_55
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()LCh;

    move-result-object v0

    .line 462
    if-eqz v0, :cond_83

    .line 463
    sget-object v1, LCi;->g:LCi;

    invoke-virtual {v0, v1}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 464
    if-nez v0, :cond_84

    const/4 v0, -0x1

    move v1, v0

    .line 465
    :goto_67
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c(I)V

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    if-eqz v0, :cond_83

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_83

    .line 467
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->i()Z

    move-result v0

    if-eqz v0, :cond_8a

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    check-cast v0, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentDropDownMenu;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentDropDownMenu;->b(I)V

    .line 474
    :cond_83
    :goto_83
    return-void

    .line 464
    :cond_84
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v1, v0

    goto :goto_67

    .line 470
    :cond_8a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    check-cast v0, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentPhonePopupMenu;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/dropdownmenu/AlignmentPhonePopupMenu;->c(I)V

    goto :goto_83
.end method

.method private v()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 744
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 745
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-interface {v0}, LuK;->p()V

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    sget v1, LsD;->toolbar_alignment_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 748
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    if-eqz v0, :cond_3d

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-interface {v0}, LuK;->p()V

    .line 750
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    sget v1, LsD;->toolbar_color_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 752
    :cond_3d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 753
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-interface {v0}, LuK;->p()V

    .line 754
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    sget v1, LsD;->toolbar_font_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 756
    :cond_5b
    return-void
.end method

.method private w()V
    .registers 2

    .prologue
    .line 901
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->c()V

    .line 902
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->u()V

    .line 903
    return-void
.end method

.method private x()V
    .registers 2

    .prologue
    .line 906
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    invoke-interface {v0}, LzH;->d()V

    .line 907
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->u()V

    .line 908
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 571
    sget v0, LsF;->legacy_toolbar:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 572
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(Landroid/view/View;)V

    .line 573
    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 643
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->k()Z

    move-result v0

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_21

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setCursorVisible(Z)V

    .line 646
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->q()V

    .line 647
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(I)V

    .line 649
    :cond_21
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Z

    if-nez v0, :cond_19

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Z

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()V

    .line 372
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->r()V

    .line 373
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->q()V

    .line 375
    :cond_19
    return-void
.end method

.method protected a(LCh;)V
    .registers 4
    .parameter

    .prologue
    .line 439
    sget-object v0, LBG;->a:LBG;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;LCh;)V

    .line 440
    sget-object v0, LBG;->b:LBG;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;LCh;)V

    .line 441
    sget-object v0, LBG;->c:LBG;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;LCh;)V

    .line 443
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()LCh;

    move-result-object v0

    .line 444
    sget-object v1, LBG;->d:LBG;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;LCh;)V

    .line 445
    sget-object v1, LBG;->e:LBG;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;LCh;)V

    .line 446
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V
    .registers 3
    .parameter

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    .line 228
    new-instance v0, LBz;

    invoke-direct {v0, p0}, LBz;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyj;)V

    .line 235
    new-instance v0, LBA;

    invoke-direct {v0, p0}, LBA;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyg;)V

    .line 244
    new-instance v0, LBB;

    invoke-direct {v0, p0}, LBB;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyl;)V

    .line 256
    new-instance v0, LBC;

    invoke-direct {v0, p0}, LBC;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyi;)V

    .line 266
    new-instance v0, LBD;

    invoke-direct {v0, p0, p1}, LBD;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;Lcom/google/android/apps/docs/editors/kix/KixEditText;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Landroid/text/TextWatcher;)V

    .line 287
    return-void
.end method

.method public a(LwF;)V
    .registers 3
    .parameter

    .prologue
    .line 365
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Z

    .line 366
    return-void
.end method

.method public a(LwF;LvZ;Lxa;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 361
    return-void
.end method

.method public a(LzH;)V
    .registers 2
    .parameter

    .prologue
    .line 294
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LzH;

    .line 295
    return-void
.end method

.method public a(Lzj;)V
    .registers 3
    .parameter

    .prologue
    .line 301
    new-instance v0, LBE;

    invoke-direct {v0, p0}, LBE;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;)V

    invoke-virtual {p1, v0}, Lzj;->a(Lzo;)V

    .line 307
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 612
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    if-eqz v1, :cond_24

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_24

    .line 613
    const/4 v1, 0x4

    if-ne p1, v1, :cond_15

    .line 614
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->h()Z

    move-result v0

    .line 636
    :goto_14
    return v0

    .line 615
    :cond_15
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_24

    invoke-static {p2}, LKe;->a(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 617
    sparse-switch p1, :sswitch_data_40

    .line 636
    :cond_24
    const/4 v0, 0x0

    goto :goto_14

    .line 619
    :sswitch_26
    sget-object v1, LBG;->a:LBG;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;)V

    goto :goto_14

    .line 622
    :sswitch_2c
    sget-object v1, LBG;->b:LBG;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;)V

    goto :goto_14

    .line 625
    :sswitch_32
    sget-object v1, LBG;->c:LBG;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;)V

    goto :goto_14

    .line 628
    :sswitch_38
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->w()V

    goto :goto_14

    .line 631
    :sswitch_3c
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->x()V

    goto :goto_14

    .line 617
    :sswitch_data_40
    .sparse-switch
        0x1e -> :sswitch_26
        0x25 -> :sswitch_2c
        0x31 -> :sswitch_32
        0x35 -> :sswitch_3c
        0x36 -> :sswitch_38
    .end sparse-switch
.end method

.method protected b(I)V
    .registers 4
    .parameter

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/os/Handler;

    new-instance v1, LBo;

    invoke-direct {v1, p0, p1}, LBo;-><init>(Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 566
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->b(Landroid/os/Bundle;)V

    .line 219
    return-void
.end method

.method public b(Landroid/view/View;)V
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 494
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    .line 496
    if-eqz p1, :cond_bb

    .line 497
    invoke-static {}, LBG;->values()[LBG;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_c
    if-ge v3, v5, :cond_22

    aget-object v6, v4, v3

    .line 498
    sget-object v0, LBG;->f:LBG;

    if-eq v6, v0, :cond_20

    sget-object v0, LBG;->g:LBG;

    if-eq v6, v0, :cond_20

    move v0, v1

    :goto_19
    invoke-direct {p0, v6, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LBG;Z)V

    .line 497
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_c

    :cond_20
    move v0, v2

    .line 498
    goto :goto_19

    .line 502
    :cond_22
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Integer;

    sget v3, LsD;->toolbar_undo_button:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    sget v2, LsD;->toolbar_redo_button:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget v2, LsD;->toolbar_font_button:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget v2, LsD;->toolbar_indent_button:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget v2, LsD;->toolbar_outdent_button:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_58
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 508
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_58

    .line 511
    :cond_72
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lgl;

    sget-object v1, Lgi;->t:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    if-nez v0, :cond_91

    .line 512
    sget v0, LsD;->toolbar_font_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 513
    sget v0, LsD;->toolbar_font_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 516
    :cond_91
    sget v0, LsC;->action_undo:I

    sget v1, LsD;->toolbar_undo_button:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(II)V

    .line 517
    sget v0, LsC;->action_redo:I

    sget v1, LsD;->toolbar_redo_button:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(II)V

    .line 518
    sget v0, LsC;->action_increase_indent:I

    sget v1, LsD;->toolbar_indent_button:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(II)V

    .line 519
    sget v0, LsC;->action_decrease_indent:I

    sget v1, LsD;->toolbar_outdent_button:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b(II)V

    .line 521
    sget v0, LsD;->toolbar_done_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 522
    if-eqz v0, :cond_ba

    .line 523
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 529
    :cond_ba
    :goto_ba
    return-void

    .line 526
    :cond_bb
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto :goto_ba
.end method

.method public g()V
    .registers 2

    .prologue
    .line 315
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 316
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:Z

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    if-eqz v0, :cond_11

    .line 318
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->q()V

    .line 320
    :cond_11
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 327
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:Z

    .line 328
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 329
    return-void
.end method

.method public h()Z
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4a

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    if-eqz v0, :cond_4c

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-interface {v0}, LuK;->h()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-interface {v0}, LuK;->p()V

    move v0, v1

    .line 587
    :goto_20
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    if-eqz v3, :cond_32

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-interface {v3}, LuK;->h()Z

    move-result v3

    if-eqz v3, :cond_32

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-interface {v0}, LuK;->p()V

    move v0, v1

    .line 591
    :cond_32
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    if-eqz v3, :cond_44

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-interface {v3}, LuK;->h()Z

    move-result v3

    if-eqz v3, :cond_44

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-interface {v0}, LuK;->p()V

    move v0, v1

    .line 595
    :cond_44
    if-eqz v0, :cond_49

    .line 596
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->p()V

    .line 600
    :cond_49
    :goto_49
    return v2

    :cond_4a
    move v2, v1

    goto :goto_49

    :cond_4c
    move v0, v2

    goto :goto_20
.end method

.method public p()V
    .registers 3

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->r()V

    .line 656
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()V

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    if-eqz v0, :cond_13

    .line 658
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 660
    :cond_13
    return-void
.end method

.method public q()V
    .registers 5

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a()J

    move-result-wide v0

    sget v2, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:J

    .line 388
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:Z

    if-eqz v0, :cond_13

    .line 393
    :cond_12
    :goto_12
    return-void

    .line 392
    :cond_13
    sget v0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(J)V

    goto :goto_12
.end method

.method public r()V
    .registers 2

    .prologue
    .line 846
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:LuK;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LuK;)V

    .line 847
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->b:LuK;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LuK;)V

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->c:LuK;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LuK;)V

    .line 849
    return-void
.end method

.method protected s()V
    .registers 2

    .prologue
    .line 863
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Z

    .line 864
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()V

    .line 865
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a:Z

    .line 866
    return-void
.end method
