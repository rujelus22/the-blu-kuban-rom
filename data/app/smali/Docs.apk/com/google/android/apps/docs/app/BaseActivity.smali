.class public Lcom/google/android/apps/docs/app/BaseActivity;
.super Lcom/google/android/apps/docs/RoboFragmentActivity;
.source "BaseActivity.java"

# interfaces
.implements Lff;
.implements LoW;


# instance fields
.field private a:LMM;

.field public a:LMO;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field public a:LeW;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lfg;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:Landroid/os/Handler;

.field private i:Z

.field private j:Z

.field private volatile k:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;-><init>()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->i:Z

    .line 48
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/BaseActivity;->j:Z

    .line 53
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/BaseActivity;->k:Z

    return-void
.end method

.method private f()V
    .registers 3

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 140
    sget-object v1, Lfb;->a:Lfb;

    invoke-virtual {v0, v1}, Lfb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 142
    sget v0, Leo;->CakemixTheme_Docs:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/BaseActivity;->setTheme(I)V

    .line 144
    :cond_1b
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->g()V

    .line 145
    return-void
.end method

.method private g()V
    .registers 2

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->j:Z

    if-nez v0, :cond_c

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->j:Z

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMO;

    invoke-interface {v0, p0}, LMO;->a(Landroid/app/Activity;)V

    .line 152
    :cond_c
    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    if-nez v0, :cond_10

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMO;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()LNa;

    move-result-object v1

    invoke-interface {v0, p0, v1}, LMO;->a(Landroid/app/Activity;LNa;)LMM;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    .line 166
    :cond_10
    return-void
.end method

.method private i()V
    .registers 3

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()Ljava/lang/String;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_11

    .line 292
    iget-object v1, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LeW;

    invoke-interface {v1, v0}, LeW;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->finish()V

    .line 296
    :cond_11
    return-void
.end method


# virtual methods
.method public a()LMM;
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    return-object v0
.end method

.method protected a()LNa;
    .registers 2

    .prologue
    .line 170
    new-instance v0, LfA;

    invoke-direct {v0, p0}, LfA;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method public a(I)Landroid/view/View;
    .registers 3
    .parameter

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 134
    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    return-object v0
.end method

.method public a()Lfb;
    .registers 2

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 300
    const-class v0, Lfg;

    if-ne p1, v0, :cond_7

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:Lfg;

    .line 310
    :goto_6
    return-object v0

    .line 304
    :cond_7
    const-class v0, LMM;

    if-ne p1, v0, :cond_e

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    goto :goto_6

    .line 310
    :cond_e
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/RoboFragmentActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_6
.end method

.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a()LoT;
    .registers 3

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()Laoo;

    move-result-object v0

    const-class v1, LoT;

    invoke-interface {v0, v1}, Laoo;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LoT;

    return-object v0
.end method

.method protected final a(Lz;)V
    .registers 4
    .parameter

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 361
    invoke-virtual {p1}, Lz;->a()I

    .line 370
    :goto_9
    return-void

    .line 363
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->b:Landroid/os/Handler;

    new-instance v1, Lfj;

    invoke-direct {v1, p0, p1}, Lfj;-><init>(Lcom/google/android/apps/docs/app/BaseActivity;Lz;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_9
.end method

.method protected a()Z
    .registers 2

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method protected b()Z
    .registers 2

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->i:Z

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 195
    const/4 v0, 0x1

    return v0
.end method

.method public d()V
    .registers 1

    .prologue
    .line 130
    return-void
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->k:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public e()V
    .registers 1

    .prologue
    .line 256
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 210
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 211
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->g()V

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->k:Z

    .line 213
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->b:Landroid/os/Handler;

    .line 214
    if-nez p1, :cond_2a

    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()LoT;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()Lo;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, LoT;->a(Lo;Landroid/content/Context;)Z

    move-result v0

    .line 217
    if-nez v0, :cond_2a

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->b:Landroid/os/Handler;

    new-instance v1, Lfi;

    invoke-direct {v1, p0}, Lfi;-><init>(Lcom/google/android/apps/docs/app/BaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 226
    :cond_2a
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    invoke-interface {v0, p1}, LMM;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_13

    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_13
    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onPause()V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->i:Z

    .line 79
    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 260
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    if-eqz v1, :cond_18

    .line 265
    iget-object v1, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    invoke-virtual {v0}, Lfb;->a()I

    move-result v0

    invoke-interface {v1, v0}, LMM;->a(I)V

    .line 267
    :cond_18
    return-void
.end method

.method protected onPostResume()V
    .registers 3

    .prologue
    .line 70
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onPostResume()V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->k:Z

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()LoT;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->a()Lo;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, LoT;->a(Lo;Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onResume()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->i:Z

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    if-eqz v0, :cond_f

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    invoke-interface {v0}, LMM;->b()V

    .line 65
    :cond_f
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->i()V

    .line 66
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 271
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 275
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_c

    .line 276
    iput-boolean v2, p0, Lcom/google/android/apps/docs/app/BaseActivity;->k:Z

    .line 281
    :cond_c
    const-string v0, "BaseActivityIsRestart"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 282
    return-void
.end method

.method public onStart()V
    .registers 2

    .prologue
    .line 230
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onStart()V

    .line 231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->k:Z

    .line 232
    return-void
.end method

.method public onStop()V
    .registers 2

    .prologue
    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->k:Z

    .line 237
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onStop()V

    .line 238
    return-void
.end method

.method public setContentView(I)V
    .registers 2
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->f()V

    .line 100
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->setContentView(I)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()V

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->h()V

    .line 103
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->f()V

    .line 110
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->setContentView(Landroid/view/View;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()V

    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->h()V

    .line 113
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->f()V

    .line 120
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/RoboFragmentActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()V

    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->h()V

    .line 123
    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMM;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, LMM;->a(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    .line 188
    return-void
.end method
