.class public Lcom/google/android/apps/docs/app/OcrCameraActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "OcrCameraActivity.java"


# instance fields
.field private a:Landroid/net/Uri;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 179
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 192
    if-eqz p1, :cond_22

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 193
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    const/high16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 195
    const-class v1, Lcom/google/android/apps/docs/app/OcrCameraActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 196
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string v1, "collectionResourceId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    return-object v0

    .line 192
    :cond_22
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private a(Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_15

    .line 99
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->setResult(I)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->finish()V

    .line 126
    :goto_14
    return-void

    .line 103
    :cond_15
    const-string v0, "collectionResourceId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->b:Ljava/lang/String;

    .line 104
    if-eqz p2, :cond_31

    .line 105
    const-string v0, "keySaveImageUri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 106
    if-eqz v0, :cond_2f

    :goto_29
    invoke-static {v1}, Lagu;->a(Z)V

    .line 107
    iput-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Landroid/net/Uri;

    goto :goto_14

    :cond_2f
    move v1, v2

    .line 106
    goto :goto_29

    .line 108
    :cond_31
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 110
    :try_start_3d
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 111
    const-string v1, "title"

    sget v2, Len;->ocr_image_title_drivev2:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v1, "description"

    sget v2, Len;->ocr_image_description_drivev2:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v1, "mime_type"

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Landroid/net/Uri;

    .line 115
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 116
    const-string v1, "output"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 117
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_7e
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3d .. :try_end_7e} :catch_7f
    .catch Ljava/lang/IllegalStateException; {:try_start_3d .. :try_end_7e} :catch_84

    goto :goto_14

    .line 118
    :catch_7f
    move-exception v0

    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->f()V

    goto :goto_14

    .line 120
    :catch_84
    move-exception v0

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->f()V

    goto :goto_14

    .line 124
    :cond_89
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->finish()V

    goto :goto_14
.end method

.method public static a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 170
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera.autofocus"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private f()V
    .registers 3

    .prologue
    .line 91
    const-string v0, "OcrCameraActivity"

    const-string v1, "No external storage present, cannot take picture."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    sget v0, Len;->camera_ocr_no_sdcard:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->finish()V

    .line 94
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 138
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 139
    if-ne p1, v5, :cond_5c

    .line 140
    const/4 v0, -0x1

    if-ne p2, v0, :cond_5d

    .line 142
    sget v0, Len;->camera_ocr_default_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyyMMdd-HHmm"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 146
    new-instance v1, LTi;

    invoke-direct {v1, p0}, LTi;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Landroid/net/Uri;

    const-string v3, "image/jpeg"

    invoke-virtual {v1, v2, v3}, LTi;->a(Landroid/net/Uri;Ljava/lang/String;)LTi;

    move-result-object v1

    invoke-virtual {v1, v0}, LTi;->a(Ljava/lang/String;)LTi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LTi;->b(Ljava/lang/String;)LTi;

    move-result-object v0

    invoke-virtual {v0, v5}, LTi;->a(Z)LTi;

    move-result-object v0

    invoke-virtual {v0, v5}, LTi;->d(Z)LTi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LTi;->c(Ljava/lang/String;)LTi;

    move-result-object v0

    invoke-virtual {v0}, LTi;->a()Landroid/content/Intent;

    move-result-object v0

    .line 155
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->startActivity(Landroid/content/Intent;)V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->finish()V

    .line 164
    :cond_5c
    :goto_5c
    return-void

    .line 158
    :cond_5d
    if-eqz p2, :cond_68

    .line 159
    sget v0, Len;->camera_ocr_error_capture:I

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 161
    :cond_68
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->finish()V

    goto :goto_5c
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:LeQ;

    invoke-virtual {v1}, LeQ;->a()V

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:LeQ;

    const-string v2, "/ocrCamera"

    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 74
    return-void
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 79
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 80
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 86
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->setIntent(Landroid/content/Intent;)V

    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 88
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 131
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 132
    const-string v0, "keySaveImageUri"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 133
    return-void
.end method
