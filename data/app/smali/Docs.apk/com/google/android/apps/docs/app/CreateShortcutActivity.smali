.class public Lcom/google/android/apps/docs/app/CreateShortcutActivity;
.super Lcom/google/android/apps/docs/RoboFragmentActivity;
.source "CreateShortcutActivity.java"

# interfaces
.implements Low;


# instance fields
.field private a:Ljava/lang/String;

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;-><init>()V

    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a(Landroid/content/Context;Ljava/lang/String;)LhX;

    move-result-object v0

    const-string v1, "root"

    invoke-virtual {v0, v1}, LhX;->a(Ljava/lang/String;)LhX;

    move-result-object v0

    invoke-virtual {v0}, LhX;->a()Landroid/content/Intent;

    move-result-object v0

    .line 76
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 77
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 78
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->setResult(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->finish()V

    .line 95
    return-void
.end method

.method private g()V
    .registers 6

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 100
    if-nez v0, :cond_16

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->f()V

    .line 118
    :goto_15
    return-void

    .line 105
    :cond_16
    invoke-virtual {v0}, LkO;->a()LkY;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LkY;)Landroid/content/Intent;

    move-result-object v1

    .line 107
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 108
    const-string v3, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 110
    invoke-virtual {v0}, LkO;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LkO;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LkO;->d()Z

    move-result v4

    invoke-static {v1, v3, v4}, LkO;->b(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    .line 112
    invoke-static {p0, v1}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 113
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 114
    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0}, LkO;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->setResult(ILandroid/content/Intent;)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->finish()V

    goto :goto_15
.end method


# virtual methods
.method public a(Landroid/accounts/Account;)V
    .registers 3
    .parameter

    .prologue
    .line 65
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Ljava/lang/String;

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->e()V

    .line 67
    return-void
.end method

.method public l_()V
    .registers 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->f()V

    .line 61
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    if-nez p1, :cond_10

    .line 83
    const/4 v0, -0x1

    if-ne p2, v0, :cond_11

    .line 84
    const-string v0, "resourceId"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->b:Ljava/lang/String;

    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->g()V

    .line 90
    :cond_10
    :goto_10
    return-void

    .line 87
    :cond_11
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->f()V

    goto :goto_10
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    if-eqz p1, :cond_15

    .line 38
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Ljava/lang/String;

    .line 39
    const-string v0, "resourceId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->b:Ljava/lang/String;

    .line 42
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_21

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a()Lo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(Lo;)V

    .line 49
    :goto_20
    return-void

    .line 44
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_29

    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->e()V

    goto :goto_20

    .line 47
    :cond_29
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->g()V

    goto :goto_20
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 54
    const-string v0, "accountName"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "resourceId"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method
