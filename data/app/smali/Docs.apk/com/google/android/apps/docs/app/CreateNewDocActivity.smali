.class public Lcom/google/android/apps/docs/app/CreateNewDocActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "CreateNewDocActivity.java"


# instance fields
.field public a:LVK;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DocFeed"
    .end annotation
.end field

.field public a:LWY;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 108
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method private static a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    const-string v0, "accountName"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    const-string v0, "collectionResourceId"

    invoke-virtual {p0, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-virtual {p1, p0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 97
    return-void
.end method

.method private a(Landroid/net/Uri;)Z
    .registers 8
    .parameter

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b(Landroid/net/Uri;)Z

    move-result v0

    invoke-static {v0}, Lagu;->a(Z)V

    .line 186
    :try_start_7
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_19

    .line 188
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_19
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_19} :catch_1b

    .line 195
    :cond_19
    const/4 v0, 0x1

    :goto_1a
    return v0

    .line 190
    :catch_1b
    move-exception v0

    .line 191
    sget v1, Len;->upload_notification_failure_ticker:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 192
    iget-object v2, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LZM;

    invoke-interface {v2, v1, v0}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 193
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    const-string v1, "com.google.android.apps.docs.app.CreateNewDocActivity.UPLOAD_FILE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method private b(Landroid/net/Uri;)Z
    .registers 4
    .parameter

    .prologue
    .line 199
    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 200
    return v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;)V
    .registers 5
    .parameter

    .prologue
    .line 315
    invoke-static {p1}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;)Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    move-result-object v0

    .line 322
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->d(Z)V

    .line 323
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a()Lo;

    move-result-object v1

    const-string v2, "editTitleDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v9

    .line 216
    iget v0, p2, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 217
    new-instance v4, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 218
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 219
    new-instance v0, Lfw;

    const-string v2, "Create new entry"

    move-object v1, p0

    move-object v5, p0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v9}, Lfw;-><init>(Lcom/google/android/apps/docs/app/CreateNewDocActivity;Ljava/lang/String;Landroid/os/Handler;Ljava/util/concurrent/atomic/AtomicReference;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;LkB;)V

    .line 306
    invoke-virtual {v0}, Les;->start()V

    .line 307
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b:Landroid/os/Handler;

    new-instance v1, Lfv;

    invoke-direct {v1, p0}, Lfv;-><init>(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 122
    return-void
.end method

.method g()V
    .registers 4

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LeQ;

    const-string v1, "/createNewDoc"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    move-result-object v0

    .line 341
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->d(Z)V

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a()Lo;

    move-result-object v1

    const-string v2, "createNewEntryDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 343
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 155
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 157
    packed-switch p1, :pswitch_data_74

    .line 178
    const-string v0, "CreateNewDocActivity"

    const-string v1, "Unexpected activity request code: %d"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :goto_20
    return-void

    .line 159
    :pswitch_21
    const/4 v0, -0x1

    if-ne p2, v0, :cond_67

    .line 160
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 161
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_34

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_63

    .line 162
    :cond_34
    new-instance v1, LSP;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, LSP;-><init>(Landroid/content/ContentResolver;)V

    .line 163
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 164
    const-string v3, "mime_type"

    invoke-virtual {v1, v0, v3, v2}, LSP;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 166
    new-instance v2, LTi;

    invoke-direct {v2, p0}, LTi;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0, v1}, LTi;->a(Landroid/net/Uri;Ljava/lang/String;)LTi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LTi;->b(Ljava/lang/String;)LTi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LTi;->c(Ljava/lang/String;)LTi;

    move-result-object v0

    invoke-virtual {v0}, LTi;->a()Landroid/content/Intent;

    move-result-object v0

    .line 169
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->startActivity(Landroid/content/Intent;)V

    .line 175
    :cond_63
    :goto_63
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->finish()V

    goto :goto_20

    .line 171
    :cond_67
    if-eqz p2, :cond_63

    .line 172
    sget v0, Len;->gallery_select_error:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_63

    .line 157
    nop

    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_21
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 126
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 128
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Ljava/lang/String;

    .line 129
    const-string v1, "collectionResourceId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Ljava/lang/String;

    if-eqz v1, :cond_5c

    .line 132
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->g()V

    .line 150
    :goto_2b
    return-void

    .line 134
    :cond_2c
    const-string v1, "com.google.android.apps.docs.app.CreateNewDocActivity.UPLOAD_FILE"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LeQ;

    const-string v1, "/createNewFromUpload"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 137
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 138
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2b

    .line 142
    :cond_55
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->setResult(I)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->finish()V

    goto :goto_2b

    .line 146
    :cond_5c
    const-string v0, "CreateNewDocActivity"

    const-string v1, "Account name is not specified in the intent."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->setResult(I)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->finish()V

    goto :goto_2b
.end method
