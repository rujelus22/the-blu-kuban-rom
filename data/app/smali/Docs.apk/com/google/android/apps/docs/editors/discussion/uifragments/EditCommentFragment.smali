.class public Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;
.super Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;
.source "EditCommentFragment.java"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LtK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lmz;

.field private a:LtK;

.field private a:LtL;

.field private a:Ltd;

.field private a:Z

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;-><init>()V

    .line 59
    return-void
.end method

.method public static a()Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;
    .registers 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;-><init>()V

    .line 129
    return-object v0
.end method

.method public static synthetic a()Ljava/util/Map;
    .registers 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ljava/util/Map;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;)LtL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->t()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;Ltd;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->b(Ltd;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    if-nez v0, :cond_8

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->u()V

    .line 401
    :goto_7
    return-void

    .line 380
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    sget-object v1, LtK;->b:LtK;

    if-ne v0, v1, :cond_26

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    invoke-interface {v0, v1, p1}, Lma;->b(Lmz;Ljava/lang/String;)LlV;

    move-result-object v0

    .line 386
    :goto_16
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Landroid/os/Handler;

    new-instance v2, LtH;

    invoke-direct {v2, p0, v0}, LtH;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;LlV;)V

    invoke-interface {v0, v1, v2}, LlV;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->e()V

    goto :goto_7

    .line 384
    :cond_26
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    invoke-interface {v0, v1, p1}, Lma;->a(Lmz;Ljava/lang/String;)LlV;

    move-result-object v0

    goto :goto_16
.end method

.method private a(Ltd;Ljava/lang/String;LtK;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LAQ;

    invoke-virtual {v0}, LAQ;->a()V

    .line 194
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    .line 195
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->c:Ljava/lang/String;

    .line 196
    iput-object p3, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0, p4}, LtL;->a(Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method private b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "deleteCommentDialog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->u()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    invoke-virtual {v0}, Ltd;->b()Ljava/lang/String;

    move-result-object v0

    .line 405
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lma;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->c:Ljava/lang/String;

    invoke-interface {v1, p1, v0, v2}, Lma;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LlV;

    move-result-object v1

    .line 407
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Landroid/os/Handler;

    new-instance v3, LtI;

    invoke-direct {v3, p0, v1, v0}, LtI;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;LlV;Ljava/lang/String;)V

    invoke-interface {v1, v2, v3}, LlV;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->e()V

    .line 422
    return-void

    .line 404
    :cond_22
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private b(Ltd;)V
    .registers 3
    .parameter

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->d()V

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltb;

    invoke-interface {v0, p1}, Ltb;->b(Ltd;)V

    .line 432
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->d()V

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltb;

    invoke-interface {v0, p1}, Ltb;->c(Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method private t()V
    .registers 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->d()V

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltb;

    invoke-interface {v0}, Ltb;->r()V

    .line 427
    return-void
.end method

.method private u()V
    .registers 3

    .prologue
    .line 440
    sget v0, LsH;->discussion_error:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(II)V

    .line 441
    return-void
.end method

.method private v()V
    .registers 3

    .prologue
    .line 444
    sget v0, LsH;->discussion_longer_comment:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(II)V

    .line 445
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Z

    invoke-interface {v0, p1, v1}, LtL;->a(Landroid/view/LayoutInflater;Z)Landroid/view/View;

    move-result-object v0

    .line 266
    return-object v0
.end method

.method public a()LtK;
    .registers 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    return-object v0
.end method

.method public a()Ltm;
    .registers 2

    .prologue
    .line 134
    sget-object v0, Ltm;->d:Ltm;

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    sget-object v1, LtK;->c:LtK;

    if-ne v0, v1, :cond_16

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    invoke-virtual {v0}, Ltd;->b()Ljava/lang/String;

    move-result-object v0

    .line 294
    :goto_10
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->c(Ljava/lang/String;)V

    .line 298
    :goto_13
    return-void

    .line 293
    :cond_14
    const/4 v0, 0x0

    goto :goto_10

    .line 296
    :cond_16
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->t()V

    goto :goto_13
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 211
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a(Landroid/os/Bundle;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    if-nez v0, :cond_10

    .line 213
    new-instance v0, LtM;

    invoke-direct {v0, p0}, LtM;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    .line 215
    :cond_10
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->d(Z)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_78

    .line 217
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->e(Z)V

    .line 222
    :goto_1c
    if-eqz p1, :cond_7c

    .line 223
    invoke-static {p1}, Ltd;->a(Landroid/os/Bundle;)Ltd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    .line 224
    const-string v0, "context"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 225
    const-string v0, "context"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->c:Ljava/lang/String;

    .line 227
    :cond_34
    const-string v0, "oldText"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 228
    const-string v0, "oldText"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v1, v0}, LtL;->a(Ljava/lang/String;)V

    .line 231
    :cond_47
    const-string v0, "action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 232
    sget-object v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ljava/util/Map;

    const-string v1, "action"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    .line 234
    :cond_5f
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    .line 235
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Z

    .line 240
    :goto_64
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Lo;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/RoboDialogFragment;

    .line 242
    if-eqz v0, :cond_77

    .line 243
    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/RoboDialogFragment;->a()V

    .line 245
    :cond_77
    return-void

    .line 219
    :cond_78
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->e(Z)V

    goto :goto_1c

    .line 237
    :cond_7c
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Z

    goto :goto_64
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 259
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 260
    return-void
.end method

.method protected a(Ljava/util/SortedSet;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    const-string v0, "EditCommentFragment"

    const-string v1, "Updating comment entry"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    sget-object v1, LtK;->c:LtK;

    if-ne v0, v1, :cond_e

    .line 159
    :cond_d
    :goto_d
    return-void

    .line 144
    :cond_e
    invoke-interface {p1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltd;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 146
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    .line 148
    :cond_2c
    invoke-interface {v0}, Lmz;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_34
    :goto_34
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 149
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ltd;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 150
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    goto :goto_34

    .line 154
    :cond_4f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    if-nez v0, :cond_d

    .line 156
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->u()V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()V

    goto :goto_d
.end method

.method public a(Lmx;)V
    .registers 3
    .parameter

    .prologue
    .line 203
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a(Lmx;)V

    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->h()Z

    move-result v0

    if-nez v0, :cond_e

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->b()V

    .line 207
    :cond_e
    return-void
.end method

.method public a(Ltd;)V
    .registers 5
    .parameter

    .prologue
    .line 167
    const-string v0, ""

    sget-object v1, LtK;->a:LtK;

    const-string v2, ""

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(Ltd;Ljava/lang/String;LtK;Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public a(Ltd;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 177
    const-string v0, ""

    sget-object v1, LtK;->b:LtK;

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(Ltd;Ljava/lang/String;LtK;Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 349
    const/16 v0, 0x800

    if-lt p1, v0, :cond_7

    .line 350
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->v()V

    .line 352
    :cond_7
    return-void
.end method

.method public b(Ltd;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 188
    sget-object v0, LtK;->c:LtK;

    const-string v1, ""

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(Ltd;Ljava/lang/String;LtK;Ljava/lang/String;)V

    .line 189
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 249
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->c(Landroid/os/Bundle;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Ltd;

    invoke-static {p1, v0}, Ltd;->a(Landroid/os/Bundle;Ltd;)V

    .line 251
    const-string v0, "context"

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, LsD;->comment_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 253
    const-string v1, "oldText"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v0, "action"

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    invoke-virtual {v1}, LtK;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 275
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->g()V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->c()V

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    if-eqz v0, :cond_11

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->a()V

    .line 281
    :cond_11
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->d()V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LAQ;

    invoke-virtual {v0}, LAQ;->a()V

    .line 288
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->h()V

    .line 289
    return-void
.end method

.method public p()V
    .registers 4

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    if-nez v0, :cond_8

    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->u()V

    .line 309
    :goto_7
    return-void

    .line 305
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    invoke-interface {v0}, Lmz;->a()Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    invoke-interface {v0}, Lmz;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_2e

    const/4 v0, 0x1

    .line 306
    :goto_1d
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v1}, LtL;->d()V

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Lo;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a(Lo;Ljava/lang/String;Z)V

    goto :goto_7

    .line 305
    :cond_2e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public q()V
    .registers 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->r()V

    .line 317
    return-void
.end method

.method public r()V
    .registers 5

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    if-nez v0, :cond_8

    .line 321
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->u()V

    .line 346
    :goto_7
    return-void

    .line 324
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    invoke-interface {v0}, Lmz;->a()Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_29

    const/4 v0, 0x1

    .line 325
    :goto_11
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lma;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Lmz;

    invoke-interface {v1, v2}, Lma;->c(Lmz;)LlV;

    move-result-object v1

    .line 326
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:Landroid/os/Handler;

    new-instance v3, LtG;

    invoke-direct {v3, p0, v1, v0}, LtG;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;LlV;Z)V

    invoke-interface {v1, v2, v3}, LlV;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtL;

    invoke-interface {v0}, LtL;->e()V

    goto :goto_7

    .line 324
    :cond_29
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public s()V
    .registers 4

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, LsD;->comment_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 356
    if-nez v0, :cond_18

    .line 357
    const-string v0, ""

    .line 359
    :cond_18
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x800

    if-le v1, v2, :cond_23

    .line 362
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->v()V

    .line 364
    :cond_23
    sget-object v1, LtJ;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a:LtK;

    invoke-virtual {v2}, LtK;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_38

    .line 370
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(Ljava/lang/String;)V

    .line 372
    :goto_33
    return-void

    .line 366
    :pswitch_34
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->b(Ljava/lang/String;)V

    goto :goto_33

    .line 364
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_34
    .end packed-switch
.end method
