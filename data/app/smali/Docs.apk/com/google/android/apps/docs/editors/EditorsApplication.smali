.class public Lcom/google/android/apps/docs/editors/EditorsApplication;
.super Lcom/google/android/apps/docs/DocsApplication;
.source "EditorsApplication.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;-><init>()V

    .line 33
    return-void
.end method

.method static b(Landroid/app/Application;)Ljava/util/Collection;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Laov;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Laji;->a()Lajj;

    move-result-object v0

    new-instance v1, Lsp;

    invoke-direct {v1}, Lsp;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Lkl;

    invoke-direct {v1}, Lkl;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LuT;

    invoke-direct {v1}, LuT;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LyY;

    invoke-direct {v1}, LyY;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, Ltc;

    invoke-direct {v1}, Ltc;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    new-instance v1, LHk;

    invoke-direct {v1}, LHk;-><init>()V

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Object;)Lajj;

    move-result-object v0

    invoke-virtual {v0}, Lajj;->a()Laji;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()Ljava/util/Collection;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Laov;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {}, Laji;->a()Lajj;

    move-result-object v0

    invoke-super {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Iterable;)Lajj;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/docs/editors/EditorsApplication;->b(Landroid/app/Application;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajj;->a(Ljava/lang/Iterable;)Lajj;

    move-result-object v0

    invoke-virtual {v0}, Lajj;->a()Laji;

    move-result-object v0

    return-object v0
.end method
