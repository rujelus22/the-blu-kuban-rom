.class public Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;
.super Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;
.source "DeleteDialogFragment.java"


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LlS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lja;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 81
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v1, "resIds"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 83
    new-instance v1, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;-><init>()V

    .line 84
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->d(Landroid/os/Bundle;)V

    .line 85
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/Set;

    return-object v0
.end method

.method public static synthetic a(Llf;LlS;LkO;Ljava/lang/String;LlK;)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->b(Llf;LlS;LkO;Ljava/lang/String;LlK;)Z

    move-result v0

    return v0
.end method

.method private static b(Llf;LlS;LkO;Ljava/lang/String;LlK;)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-virtual {p2}, LkO;->a()LkB;

    move-result-object v1

    .line 147
    invoke-virtual {p2}, LkO;->a()LkY;

    move-result-object v0

    .line 149
    const-string v2, "*"

    invoke-interface {p1, v0, p3, v2, p4}, LlS;->b(LkY;Ljava/lang/String;Ljava/lang/String;LlK;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 150
    const/4 v0, 0x0

    .line 179
    :goto_11
    return v0

    .line 156
    :cond_12
    invoke-virtual {p2}, LkO;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, LkY;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 157
    if-eqz p3, :cond_42

    if-nez v0, :cond_42

    .line 159
    invoke-interface {p0, p2}, Llf;->a(LkO;)Ljava/util/Map;

    move-result-object v0

    .line 160
    invoke-interface {p0, v1, p3}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v2

    .line 161
    if-eqz v2, :cond_3d

    .line 162
    invoke-virtual {v2}, LkH;->c()J

    move-result-wide v2

    .line 163
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkX;

    .line 164
    if-eqz v0, :cond_3d

    .line 165
    invoke-virtual {v0}, LkX;->b()V

    .line 177
    :cond_3d
    :goto_3d
    invoke-virtual {v1}, LkB;->a()V

    .line 179
    const/4 v0, 0x1

    goto :goto_11

    .line 171
    :cond_42
    :try_start_42
    invoke-virtual {p2}, LkO;->b()V
    :try_end_45
    .catch Landroid/database/SQLException; {:try_start_42 .. :try_end_45} :catch_46

    goto :goto_3d

    .line 172
    :catch_46
    move-exception v0

    .line 173
    const-string v2, "DeleteDialogFactory"

    const-string v3, "Failed to delete entry in database"

    invoke-static {v2, v3, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3d
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 10
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Laoo;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 103
    if-eqz v1, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_20
    move-object v0, v3

    .line 138
    :goto_21
    return-object v0

    .line 109
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v5, :cond_a0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Llf;

    invoke-interface {v2, v1, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v2

    .line 112
    if-nez v2, :cond_40

    move-object v0, v3

    .line 113
    goto :goto_21

    .line 116
    :cond_40
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Lfg;

    sget v1, Len;->delete_collection:I

    invoke-virtual {v0, v1}, Lfg;->a(I)I

    move-result v0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v4, Len;->ask_confirmation_for_document_deletion:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, LkO;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-virtual {v2}, LkO;->o()Z

    move-result v2

    if-eqz v2, :cond_9d

    :goto_60
    move-object v2, v1

    move v1, v0

    .line 127
    :goto_62
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v4

    .line 129
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Landroid/app/Dialog;)Landroid/view/View;

    move-result-object v5

    .line 130
    sget v0, Leh;->new_name:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 131
    invoke-virtual {p0, v4, v7, v3}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 132
    invoke-virtual {v4, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 134
    sget v0, Leh;->new_name:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 135
    sget v0, Leh;->btn_ok:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sget v1, Len;->button_yes:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 136
    sget v0, Leh;->first_label:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v4

    .line 138
    goto :goto_21

    .line 120
    :cond_9d
    sget v0, Len;->delete_document:I

    goto :goto_60

    .line 122
    :cond_a0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Len;->ask_confirmation_for_document_deletion_multiple:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 124
    sget v0, Len;->delete_multiple:I

    move-object v2, v1

    move v1, v0

    goto :goto_62
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->c:Ljava/lang/String;

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "resIds"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LalO;->a(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/Set;

    .line 94
    return-void
.end method

.method protected p()V
    .registers 6

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    sget v2, Len;->status_deleting:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 186
    new-instance v1, Lot;

    invoke-direct {v1, p0}, Lot;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v2

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lja;

    .line 189
    invoke-interface {v0}, Lja;->a()LiQ;

    move-result-object v0

    .line 190
    invoke-interface {v0}, LiQ;->c()Ljava/lang/String;

    move-result-object v0

    .line 191
    if-nez v2, :cond_38

    .line 192
    const-string v0, "DeleteDialogFactory"

    const-string v1, "Account is null."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a()V

    .line 225
    :goto_37
    return-void

    .line 196
    :cond_38
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a:Ljava/util/concurrent/Executor;

    new-instance v4, LnA;

    invoke-direct {v4, p0, v2, v0, v1}, LnA;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;LkB;Ljava/lang/String;Lot;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_37
.end method
