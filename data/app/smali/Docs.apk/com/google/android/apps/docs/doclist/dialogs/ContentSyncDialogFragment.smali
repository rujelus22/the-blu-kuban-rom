.class public Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "ContentSyncDialogFragment.java"


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LXX;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;
    .registers 4
    .parameter

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;-><init>()V

    .line 112
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 113
    const-string v2, "accountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->d(Landroid/os/Bundle;)V

    .line 116
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LdL;

    return-object v0
.end method

.method private static a(Landroid/content/Context;LME;LkB;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-static {p0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 101
    sget v0, Len;->error_sync:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 106
    :goto_10
    invoke-interface {p1, p2}, LME;->a(LkB;)V

    .line 107
    return-void

    .line 103
    :cond_14
    sget v0, Len;->sync_waiting:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_10
.end method

.method public static a(Landroid/content/Context;Lo;LkB;LXX;LME;LaaJ;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 123
    invoke-virtual {p2}, LkB;->b()Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-static {p0}, LZJ;->a(Landroid/content/Context;)LZK;

    move-result-object v1

    .line 125
    invoke-interface {p5, v1}, LaaJ;->a(LZK;)Z

    move-result v1

    .line 126
    if-eqz v1, :cond_22

    .line 127
    const/4 v1, 0x1

    invoke-virtual {p3, p2, v1}, LXX;->a(LkB;Z)Z

    move-result v1

    .line 128
    if-eqz v1, :cond_1e

    .line 129
    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a(Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;

    move-result-object v0

    .line 130
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 138
    :goto_1d
    return-void

    .line 132
    :cond_1e
    invoke-static {p0, p4, p2}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a(Landroid/content/Context;LME;LkB;)V

    goto :goto_1d

    .line 135
    :cond_22
    invoke-virtual {p3, v0}, LXX;->a(Ljava/lang/String;)V

    .line 136
    invoke-static {p0, p4, p2}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a(Landroid/content/Context;LME;LkB;)V

    goto :goto_1d
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->f(Z)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LdL;

    return-object v0
.end method

.method private f(Z)V
    .registers 5
    .parameter

    .prologue
    .line 89
    if-eqz p1, :cond_9

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LXX;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LXX;->a(Ljava/lang/String;)V

    .line 92
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LME;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a(Landroid/content/Context;LME;LkB;)V

    .line 94
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a:LdL;

    invoke-interface {v1, v0}, LdL;->a(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "accountName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;->c:Ljava/lang/String;

    .line 57
    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Len;->pin_sync_broadband_warning_update_files:I

    new-instance v3, Lnr;

    invoke-direct {v3, p0, v0}, Lnr;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x104

    new-instance v3, Lnq;

    invoke-direct {v3, p0, v0}, Lnq;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/ContentSyncDialogFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lej;->sync_broadband_warning_dialog:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 85
    return-object v0
.end method
