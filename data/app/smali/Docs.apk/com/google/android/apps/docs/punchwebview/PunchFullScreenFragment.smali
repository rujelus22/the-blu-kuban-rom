.class public Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "PunchFullScreenFragment.java"


# instance fields
.field private a:I

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LQR;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LRC;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LSh;

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/view/WindowManager;

.field private a:Landroid/webkit/WebView;

.field a:Landroid/widget/FrameLayout;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LSg;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/ViewGroup;

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LRb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 48
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;)Landroid/view/ViewGroup;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;Landroid/webkit/WebView;)Landroid/webkit/WebView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    return-object p1
.end method

.method static a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;
    .registers 1

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;-><init>()V

    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 246
    new-instance v1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;-><init>(Landroid/content/Context;)V

    .line 247
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setGravity(I)V

    .line 248
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 252
    sget v0, Leh;->loading_spinner:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 254
    sget v0, Leh;->punch_web_view_container:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    .line 257
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 230
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_e

    .line 234
    if-eqz p1, :cond_f

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 240
    :cond_e
    :goto_e
    return-void

    .line 237
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    goto :goto_e
.end method

.method private b(I)V
    .registers 5
    .parameter

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 182
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LZM;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    return-void
.end method

.method private r()V
    .registers 7

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    if-nez v0, :cond_c

    .line 188
    const-string v0, "PunchFullScreenFragment"

    const-string v1, "early exit in cleanup, as it was never fully created"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :goto_b
    return-void

    .line 192
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LdL;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LKS;

    invoke-static {v0}, LSh;->a(LKS;)LSh;

    move-result-object v0

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LRC;

    invoke-virtual {v0}, LSh;->a()LRy;

    move-result-object v3

    invoke-interface {v2, v3}, LRC;->a(LRy;)V

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LRC;

    invoke-virtual {v0}, LSh;->a()Z

    move-result v0

    invoke-interface {v2, v0}, LRC;->b(Z)V

    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->u()V

    .line 203
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a(Z)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSg;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-interface {v0, v2}, LSg;->a(Landroid/webkit/WebView;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 208
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    new-instance v3, LQP;

    invoke-direct {v3, p0, v0}, LQP;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;Landroid/view/Window;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 218
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:I

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_b
.end method

.method private s()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 265
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 266
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 267
    return-void
.end method

.method private t()V
    .registers 4

    .prologue
    .line 270
    const-string v0, "PunchFullScreenFragment"

    const-string v1, "in showPopup"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    new-instance v0, LQQ;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LQQ;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    .line 272
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    move-result-object v0

    .line 273
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->s()V

    .line 274
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setWebView(Landroid/webkit/WebView;)V

    .line 276
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 277
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 281
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 282
    const/16 v1, 0x77

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 283
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 284
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 285
    or-int/lit16 v1, v1, 0x200

    .line 286
    or-int/lit16 v1, v1, 0x100

    .line 287
    const v2, -0x10001

    and-int/2addr v1, v2

    .line 288
    or-int/lit16 v1, v1, 0x400

    .line 289
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 291
    const/16 v1, 0x3eb

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 293
    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 294
    const-string v1, "Punch Fullscreen"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 297
    return-void
.end method

.method private u()V
    .registers 3

    .prologue
    .line 300
    const-string v0, "PunchFullScreenFragment"

    const-string v1, "in hidePopup"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 303
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_b

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 86
    :cond_b
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 109
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 110
    const-string v0, "PunchFullScreenFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in onCreate savedInstanceState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    if-eqz p1, :cond_26

    .line 114
    const-string v0, "PunchFullScreenFragment"

    const-string v1, "discarding reconstruction."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :goto_25
    return-void

    .line 118
    :cond_26
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LKS;

    invoke-static {v0}, LSh;->a(LKS;)LSh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LSh;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LRC;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LSh;

    invoke-virtual {v2}, LSh;->b()LRy;

    move-result-object v2

    invoke-interface {v0, v2}, LRC;->a(LRy;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LRC;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LSh;

    invoke-virtual {v2}, LSh;->b()Z

    move-result v2

    invoke-interface {v0, v2}, LRC;->b(Z)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 124
    const-string v0, "window"

    invoke-virtual {v2, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/view/WindowManager;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSg;

    invoke-interface {v0}, LSg;->c()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->t()V

    .line 132
    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRb;

    invoke-static {v3, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/view/View;LRb;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/webkit/WebView;

    new-instance v3, LQO;

    invoke-direct {v3, p0, v2}, LQO;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;Landroid/app/Activity;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v3, v4, v5}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 146
    invoke-virtual {v2}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:I

    .line 152
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-lt v0, v3, :cond_a5

    .line 153
    const/4 v0, 0x6

    .line 155
    :goto_9e
    invoke-virtual {v2, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->q()V

    goto :goto_25

    :cond_a5
    move v0, v1

    goto :goto_9e
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->c(Landroid/os/Bundle;)V

    .line 97
    const-string v0, "PunchFullScreenFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onSaveInstanceState outState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const-string v0, "hasSavedInstance"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 105
    return-void
.end method

.method public i()V
    .registers 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 224
    const-string v0, "PunchFullScreenFragment"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->r()V

    .line 226
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->i()V

    .line 227
    return-void
.end method

.method public p()V
    .registers 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_a

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 92
    :cond_a
    return-void
.end method

.method public q()V
    .registers 4

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LSh;

    sget-object v1, LSh;->a:LSh;

    invoke-virtual {v0, v1}, LSh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LQR;

    invoke-interface {v0}, LQR;->b()Ljava/lang/Boolean;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_1d

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 163
    sget v0, Len;->punch_missing_features_notification:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b(I)V

    .line 178
    :cond_1d
    :goto_1d
    return-void

    .line 166
    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LQR;

    invoke-interface {v0}, LQR;->a()Ljava/lang/Boolean;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_1d

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LKS;

    const-string v1, "punchEnableApproximateTransitionsNotification"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LSh;

    sget-object v1, LSh;->b:LSh;

    invoke-virtual {v0, v1}, LSh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 171
    sget v0, Len;->punch_approximate_transitions_notification:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b(I)V

    goto :goto_1d

    .line 172
    :cond_47
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LSh;

    sget-object v1, LSh;->c:LSh;

    invoke-virtual {v0, v1}, LSh;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LRC;

    invoke-interface {v0}, LRC;->a()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 174
    sget v0, Len;->punch_approximate_transitions_notification:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b(I)V

    goto :goto_1d
.end method
