.class public Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;
.super Lcom/google/android/apps/docs/GuiceService;
.source "ContentSyncService.java"


# static fields
.field public static final a:J


# instance fields
.field private a:I

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LVH;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LWV;

.field public a:LXP;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LYf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZE;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LZO;

.field public a:LaaB;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaW;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/lang/Object;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LXP;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/ExecutorService;

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:J

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LkY;",
            "Ljava/util/concurrent/Future",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 122
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:J

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/docs/GuiceService;-><init>()V

    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/util/Map;

    .line 171
    new-instance v0, LWV;

    invoke-direct {v0}, LWV;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    .line 173
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    .line 175
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    .line 229
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 502
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "exportFormat"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "format"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 504
    return-object v0
.end method

.method private a(LkM;)Landroid/util/Pair;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkM;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 514
    invoke-virtual {p1}, LkM;->a()LkY;

    move-result-object v0

    .line 516
    invoke-virtual {p1}, LkM;->b()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 517
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LYf;

    invoke-interface {v1, v0}, LYf;->a(LkY;)Ljava/lang/String;

    move-result-object v0

    .line 521
    :goto_10
    if-nez v0, :cond_1b

    .line 522
    const/4 v0, 0x0

    .line 573
    :goto_13
    return-object v0

    .line 519
    :cond_14
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LVH;

    invoke-interface {v1, v0}, LVH;->a(LkY;)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 525
    :cond_1b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 538
    sget-object v0, LWT;->b:[I

    invoke-virtual {p1}, LkM;->a()LkP;

    move-result-object v2

    invoke-virtual {v2}, LkP;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_aa

    .line 565
    invoke-virtual {p1}, LkM;->b()Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 566
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LYf;

    invoke-interface {v0}, LYf;->a()Ljava/lang/String;

    move-result-object v0

    .line 573
    :goto_3a
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_13

    .line 540
    :pswitch_41
    const-string v0, "zip"

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 547
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "honorPageSize"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "footnotesAsEndnotes"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "showComments"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 550
    invoke-virtual {p1}, LkM;->a()LkP;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(LkP;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3a

    .line 553
    :pswitch_70
    const-string v0, "pdf"

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 554
    const-string v0, "application/pdf"

    goto :goto_3a

    .line 557
    :pswitch_79
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LKS;

    const-string v2, "offlineKixReadFromIndexJson"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 559
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "exportFormat"

    const-string v3, "zip"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "idxp"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 561
    invoke-virtual {p1}, LkM;->a()LkP;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(LkP;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3a

    .line 568
    :cond_a5
    invoke-virtual {p1}, LkM;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_3a

    .line 538
    :pswitch_data_aa
    .packed-switch 0x1
        :pswitch_41
        :pswitch_70
        :pswitch_79
    .end packed-switch
.end method

.method private a(LkY;LXM;)Ljava/util/concurrent/Future;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkY;",
            "LXM;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, LWR;

    invoke-direct {v1, p0, p1, p2}, LWR;-><init>(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;LkY;LXM;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 404
    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0}, LWV;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v1}, LWV;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v1}, LWV;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v1}, LWV;->a()I

    move-result v1

    add-int/2addr v0, v1

    if-lez v0, :cond_7a

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/String;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.sync.syncadapter.OVERALL_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "accountName"

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tasks_active"

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v2}, LWV;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tasks_completed"

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v2}, LWV;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tasks_failed"

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v2}, LWV;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tasks_canceled"

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v2}, LWV;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "total_bytes_loaded"

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v2}, LWV;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "service_start_time"

    iget-wide v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 225
    const-string v1, "com.google.android.apps.docs.permission.SYNC_STATUS"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 227
    :cond_7a
    return-void
.end method

.method public static a(Landroid/content/Context;LkY;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 603
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/content/Context;LkY;Z)V

    .line 604
    return-void
.end method

.method public static a(Landroid/content/Context;LkY;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 610
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 611
    const-string v1, "com.google.android.apps.docs.sync.syncadapter.SYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 612
    invoke-virtual {p1, v0}, LkY;->a(Landroid/content/Intent;)V

    .line 613
    const-string v1, "hide_notification"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 614
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 615
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;LkY;LWX;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkY;LWX;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;LkY;LXM;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkY;LXM;)V

    return-void
.end method

.method private a(LkM;LXM;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/util/Map;

    invoke-virtual {p1}, LkM;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXP;

    .line 583
    if-nez v0, :cond_10

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LXP;

    .line 587
    :cond_10
    invoke-virtual {p1}, LkM;->a()LkY;

    move-result-object v2

    .line 588
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkM;)Landroid/util/Pair;

    move-result-object v4

    .line 589
    if-eqz v4, :cond_35

    .line 590
    iget-object v1, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 591
    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 592
    iget-object v1, v2, LkY;->b:Ljava/lang/String;

    iget-object v2, v2, LkY;->a:Ljava/lang/String;

    sget-object v5, LUK;->a:LUK;

    invoke-virtual {v5}, LUK;->name()Ljava/lang/String;

    move-result-object v5

    move-object v6, p2

    invoke-interface/range {v0 .. v6}, LXP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LXM;)V

    .line 597
    return-void

    .line 595
    :cond_35
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null content URL"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(LkY;LWX;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)LWX;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_11

    invoke-virtual {v0}, LWX;->a()LWW;

    move-result-object v0

    sget-object v1, LWW;->e:LWW;

    if-ne v0, v1, :cond_11

    .line 188
    :goto_10
    return-void

    .line 185
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1, p2}, LWV;->b(LkY;LWX;)V

    .line 186
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b(LkY;LWX;)V

    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a()V

    goto :goto_10
.end method

.method private a(LkY;LXM;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 409
    .line 410
    const/4 v1, 0x1

    .line 412
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v2, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 413
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v0

    .line 414
    if-nez v0, :cond_72

    .line 416
    const-string v0, "ContentSyncService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Document "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deleted before content is synced"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_33
    .catchall {:try_start_1 .. :try_end_33} :catchall_429
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_33} :catch_105
    .catch LasH; {:try_start_1 .. :try_end_33} :catch_169
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_33} :catch_1c1
    .catch Latc; {:try_start_1 .. :try_end_33} :catch_219
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_33} :catch_271
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_33} :catch_2c9
    .catch LNt; {:try_start_1 .. :try_end_33} :catch_321
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_33} :catch_379
    .catch LNv; {:try_start_1 .. :try_end_33} :catch_3d1

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_36
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_44
    .catchall {:try_start_36 .. :try_end_44} :catchall_6f

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v2, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v2

    .line 465
    if-eqz v2, :cond_69

    .line 466
    invoke-virtual {v2}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-nez v3, :cond_60

    .line 468
    :cond_60
    const-wide/16 v3, 0x1

    add-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v2}, LkM;->c()V

    .line 475
    :cond_69
    :try_start_69
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_6e
    .catch Ljava/lang/InterruptedException; {:try_start_69 .. :try_end_6e} :catch_480

    .line 483
    :goto_6e
    return-void

    .line 459
    :catchall_6f
    move-exception v0

    :try_start_70
    monitor-exit v1
    :try_end_71
    .catchall {:try_start_70 .. :try_end_71} :catchall_6f

    throw v0

    .line 421
    :cond_72
    :try_start_72
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LaaW;

    invoke-interface {v2}, LaaW;->b()V

    .line 423
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 424
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkM;LXM;)V

    .line 426
    iget-object v4, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v4, p1}, LWV;->a(LkY;)LWX;

    move-result-object v4

    invoke-virtual {v4}, LWX;->a()J

    move-result-wide v4

    long-to-int v4, v4

    .line 427
    iget-object v5, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LeQ;

    const-string v6, "pinning"

    const-string v7, "content_sync_succeed"

    const/4 v8, 0x0

    int-to-long v9, v4

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v6, v7, v8, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 430
    const/4 v1, 0x0

    .line 431
    const-string v4, "ContentSyncService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Syncing file number  document id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, LkM;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " completed.  Time taken: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v2, v5, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c5
    .catchall {:try_start_72 .. :try_end_c5} :catchall_fe

    .line 434
    :try_start_c5
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LaaW;

    invoke-interface {v0}, LaaW;->c()V
    :try_end_ca
    .catchall {:try_start_c5 .. :try_end_ca} :catchall_429
    .catch Landroid/os/RemoteException; {:try_start_c5 .. :try_end_ca} :catch_105
    .catch LasH; {:try_start_c5 .. :try_end_ca} :catch_169
    .catch Ljava/io/IOException; {:try_start_c5 .. :try_end_ca} :catch_1c1
    .catch Latc; {:try_start_c5 .. :try_end_ca} :catch_219
    .catch Ljava/lang/InterruptedException; {:try_start_c5 .. :try_end_ca} :catch_271
    .catch Landroid/accounts/AuthenticatorException; {:try_start_c5 .. :try_end_ca} :catch_2c9
    .catch LNt; {:try_start_c5 .. :try_end_ca} :catch_321
    .catch Ljava/net/URISyntaxException; {:try_start_c5 .. :try_end_ca} :catch_379
    .catch LNv; {:try_start_c5 .. :try_end_ca} :catch_3d1

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_cd
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_db
    .catchall {:try_start_cd .. :try_end_db} :catchall_150

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v2, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v2

    .line 465
    if-eqz v2, :cond_f7

    .line 466
    invoke-virtual {v2}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_153

    .line 480
    :cond_f7
    :goto_f7
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 434
    :catchall_fe
    move-exception v0

    :try_start_ff
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LaaW;

    invoke-interface {v2}, LaaW;->c()V

    throw v0
    :try_end_105
    .catchall {:try_start_ff .. :try_end_105} :catchall_429
    .catch Landroid/os/RemoteException; {:try_start_ff .. :try_end_105} :catch_105
    .catch LasH; {:try_start_ff .. :try_end_105} :catch_169
    .catch Ljava/io/IOException; {:try_start_ff .. :try_end_105} :catch_1c1
    .catch Latc; {:try_start_ff .. :try_end_105} :catch_219
    .catch Ljava/lang/InterruptedException; {:try_start_ff .. :try_end_105} :catch_271
    .catch Landroid/accounts/AuthenticatorException; {:try_start_ff .. :try_end_105} :catch_2c9
    .catch LNt; {:try_start_ff .. :try_end_105} :catch_321
    .catch Ljava/net/URISyntaxException; {:try_start_ff .. :try_end_105} :catch_379
    .catch LNv; {:try_start_ff .. :try_end_105} :catch_3d1

    .line 436
    :catch_105
    move-exception v0

    move v2, v1

    .line 437
    :try_start_107
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_10a
    .catchall {:try_start_107 .. :try_end_10a} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_10d
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_11b
    .catchall {:try_start_10d .. :try_end_11b} :catchall_15c

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_144

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_139

    if-eqz v2, :cond_144

    .line 468
    :cond_139
    if-eqz v2, :cond_15f

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_13e
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_144
    if-eqz v2, :cond_162

    .line 475
    :try_start_146
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_14b
    .catch Ljava/lang/InterruptedException; {:try_start_146 .. :try_end_14b} :catch_14d

    goto/16 :goto_6e

    .line 476
    :catch_14d
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_150
    move-exception v0

    :try_start_151
    monitor-exit v1
    :try_end_152
    .catchall {:try_start_151 .. :try_end_152} :catchall_150

    throw v0

    .line 468
    :cond_153
    const-wide/16 v0, 0x0

    invoke-virtual {v2, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v2}, LkM;->c()V

    goto :goto_f7

    .line 459
    :catchall_15c
    move-exception v0

    :try_start_15d
    monitor-exit v1
    :try_end_15e
    .catchall {:try_start_15d .. :try_end_15e} :catchall_15c

    throw v0

    .line 468
    :cond_15f
    const-wide/16 v0, 0x0

    goto :goto_13e

    .line 480
    :cond_162
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 438
    :catch_169
    move-exception v0

    move v2, v1

    .line 439
    :try_start_16b
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_16e
    .catchall {:try_start_16b .. :try_end_16e} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_171
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_17f
    .catchall {:try_start_171 .. :try_end_17f} :catchall_1b4

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_1a8

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_19d

    if-eqz v2, :cond_1a8

    .line 468
    :cond_19d
    if-eqz v2, :cond_1b7

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_1a2
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_1a8
    if-eqz v2, :cond_1ba

    .line 475
    :try_start_1aa
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_1af
    .catch Ljava/lang/InterruptedException; {:try_start_1aa .. :try_end_1af} :catch_1b1

    goto/16 :goto_6e

    .line 476
    :catch_1b1
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_1b4
    move-exception v0

    :try_start_1b5
    monitor-exit v1
    :try_end_1b6
    .catchall {:try_start_1b5 .. :try_end_1b6} :catchall_1b4

    throw v0

    .line 468
    :cond_1b7
    const-wide/16 v0, 0x0

    goto :goto_1a2

    .line 480
    :cond_1ba
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 440
    :catch_1c1
    move-exception v0

    move v2, v1

    .line 441
    :try_start_1c3
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_1c6
    .catchall {:try_start_1c3 .. :try_end_1c6} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_1c9
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_1d7
    .catchall {:try_start_1c9 .. :try_end_1d7} :catchall_20c

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_200

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_1f5

    if-eqz v2, :cond_200

    .line 468
    :cond_1f5
    if-eqz v2, :cond_20f

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_1fa
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_200
    if-eqz v2, :cond_212

    .line 475
    :try_start_202
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_207
    .catch Ljava/lang/InterruptedException; {:try_start_202 .. :try_end_207} :catch_209

    goto/16 :goto_6e

    .line 476
    :catch_209
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_20c
    move-exception v0

    :try_start_20d
    monitor-exit v1
    :try_end_20e
    .catchall {:try_start_20d .. :try_end_20e} :catchall_20c

    throw v0

    .line 468
    :cond_20f
    const-wide/16 v0, 0x0

    goto :goto_1fa

    .line 480
    :cond_212
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 442
    :catch_219
    move-exception v0

    move v2, v1

    .line 443
    :try_start_21b
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_21e
    .catchall {:try_start_21b .. :try_end_21e} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_221
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_22f
    .catchall {:try_start_221 .. :try_end_22f} :catchall_264

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_258

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_24d

    if-eqz v2, :cond_258

    .line 468
    :cond_24d
    if-eqz v2, :cond_267

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_252
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_258
    if-eqz v2, :cond_26a

    .line 475
    :try_start_25a
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_25f
    .catch Ljava/lang/InterruptedException; {:try_start_25a .. :try_end_25f} :catch_261

    goto/16 :goto_6e

    .line 476
    :catch_261
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_264
    move-exception v0

    :try_start_265
    monitor-exit v1
    :try_end_266
    .catchall {:try_start_265 .. :try_end_266} :catchall_264

    throw v0

    .line 468
    :cond_267
    const-wide/16 v0, 0x0

    goto :goto_252

    .line 480
    :cond_26a
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 444
    :catch_271
    move-exception v0

    move v2, v1

    .line 445
    :try_start_273
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_276
    .catchall {:try_start_273 .. :try_end_276} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_279
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_287
    .catchall {:try_start_279 .. :try_end_287} :catchall_2bc

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_2b0

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_2a5

    if-eqz v2, :cond_2b0

    .line 468
    :cond_2a5
    if-eqz v2, :cond_2bf

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_2aa
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_2b0
    if-eqz v2, :cond_2c2

    .line 475
    :try_start_2b2
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_2b7
    .catch Ljava/lang/InterruptedException; {:try_start_2b2 .. :try_end_2b7} :catch_2b9

    goto/16 :goto_6e

    .line 476
    :catch_2b9
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_2bc
    move-exception v0

    :try_start_2bd
    monitor-exit v1
    :try_end_2be
    .catchall {:try_start_2bd .. :try_end_2be} :catchall_2bc

    throw v0

    .line 468
    :cond_2bf
    const-wide/16 v0, 0x0

    goto :goto_2aa

    .line 480
    :cond_2c2
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 446
    :catch_2c9
    move-exception v0

    move v2, v1

    .line 447
    :try_start_2cb
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_2ce
    .catchall {:try_start_2cb .. :try_end_2ce} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_2d1
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_2df
    .catchall {:try_start_2d1 .. :try_end_2df} :catchall_314

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_308

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_2fd

    if-eqz v2, :cond_308

    .line 468
    :cond_2fd
    if-eqz v2, :cond_317

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_302
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_308
    if-eqz v2, :cond_31a

    .line 475
    :try_start_30a
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_30f
    .catch Ljava/lang/InterruptedException; {:try_start_30a .. :try_end_30f} :catch_311

    goto/16 :goto_6e

    .line 476
    :catch_311
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_314
    move-exception v0

    :try_start_315
    monitor-exit v1
    :try_end_316
    .catchall {:try_start_315 .. :try_end_316} :catchall_314

    throw v0

    .line 468
    :cond_317
    const-wide/16 v0, 0x0

    goto :goto_302

    .line 480
    :cond_31a
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 448
    :catch_321
    move-exception v0

    move v2, v1

    .line 449
    :try_start_323
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_326
    .catchall {:try_start_323 .. :try_end_326} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_329
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_337
    .catchall {:try_start_329 .. :try_end_337} :catchall_36c

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_360

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_355

    if-eqz v2, :cond_360

    .line 468
    :cond_355
    if-eqz v2, :cond_36f

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_35a
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_360
    if-eqz v2, :cond_372

    .line 475
    :try_start_362
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_367
    .catch Ljava/lang/InterruptedException; {:try_start_362 .. :try_end_367} :catch_369

    goto/16 :goto_6e

    .line 476
    :catch_369
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_36c
    move-exception v0

    :try_start_36d
    monitor-exit v1
    :try_end_36e
    .catchall {:try_start_36d .. :try_end_36e} :catchall_36c

    throw v0

    .line 468
    :cond_36f
    const-wide/16 v0, 0x0

    goto :goto_35a

    .line 480
    :cond_372
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 450
    :catch_379
    move-exception v0

    move v2, v1

    .line 451
    :try_start_37b
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_37e
    .catchall {:try_start_37b .. :try_end_37e} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_381
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_38f
    .catchall {:try_start_381 .. :try_end_38f} :catchall_3c4

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_3b8

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_3ad

    if-eqz v2, :cond_3b8

    .line 468
    :cond_3ad
    if-eqz v2, :cond_3c7

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_3b2
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_3b8
    if-eqz v2, :cond_3ca

    .line 475
    :try_start_3ba
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_3bf
    .catch Ljava/lang/InterruptedException; {:try_start_3ba .. :try_end_3bf} :catch_3c1

    goto/16 :goto_6e

    .line 476
    :catch_3c1
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_3c4
    move-exception v0

    :try_start_3c5
    monitor-exit v1
    :try_end_3c6
    .catchall {:try_start_3c5 .. :try_end_3c6} :catchall_3c4

    throw v0

    .line 468
    :cond_3c7
    const-wide/16 v0, 0x0

    goto :goto_3b2

    .line 480
    :cond_3ca
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 452
    :catch_3d1
    move-exception v0

    move v2, v1

    .line 453
    :try_start_3d3
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Ljava/lang/Exception;LkY;)V
    :try_end_3d6
    .catchall {:try_start_3d3 .. :try_end_3d6} :catchall_47c

    .line 455
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_3d9
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_3e7
    .catchall {:try_start_3d9 .. :try_end_3e7} :catchall_41c

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v3, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 465
    if-eqz v3, :cond_410

    .line 466
    invoke-virtual {v3}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_405

    if-eqz v2, :cond_410

    .line 468
    :cond_405
    if-eqz v2, :cond_41f

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    :goto_40a
    invoke-virtual {v3, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v3}, LkM;->c()V

    .line 473
    :cond_410
    if-eqz v2, :cond_422

    .line 475
    :try_start_412
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_417
    .catch Ljava/lang/InterruptedException; {:try_start_412 .. :try_end_417} :catch_419

    goto/16 :goto_6e

    .line 476
    :catch_419
    move-exception v0

    goto/16 :goto_6e

    .line 459
    :catchall_41c
    move-exception v0

    :try_start_41d
    monitor-exit v1
    :try_end_41e
    .catchall {:try_start_41d .. :try_end_41e} :catchall_41c

    throw v0

    .line 468
    :cond_41f
    const-wide/16 v0, 0x0

    goto :goto_40a

    .line 480
    :cond_422
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto/16 :goto_6e

    .line 455
    :catchall_429
    move-exception v0

    move-object v2, v0

    move v3, v1

    :goto_42c
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_42f
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p1}, LWV;->a(LkY;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 459
    monitor-exit v1
    :try_end_43d
    .catchall {:try_start_42f .. :try_end_43d} :catchall_46e

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Llf;

    iget-object v4, p1, LkY;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v4}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v4

    .line 465
    if-eqz v4, :cond_466

    .line 466
    invoke-virtual {v4}, LkM;->d()J

    move-result-wide v0

    .line 467
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-nez v5, :cond_45b

    if-eqz v3, :cond_466

    .line 468
    :cond_45b
    if-eqz v3, :cond_471

    const-wide/16 v5, 0x1

    add-long/2addr v0, v5

    :goto_460
    invoke-virtual {v4, v0, v1}, LkM;->d(J)V

    .line 469
    invoke-virtual {v4}, LkM;->c()V

    .line 473
    :cond_466
    if-eqz v3, :cond_474

    .line 475
    :try_start_468
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_46d
    .catch Ljava/lang/InterruptedException; {:try_start_468 .. :try_end_46d} :catch_47a

    .line 480
    :goto_46d
    throw v2

    .line 459
    :catchall_46e
    move-exception v0

    :try_start_46f
    monitor-exit v1
    :try_end_470
    .catchall {:try_start_46f .. :try_end_470} :catchall_46e

    throw v0

    .line 468
    :cond_471
    const-wide/16 v0, 0x0

    goto :goto_460

    .line 480
    :cond_474
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    goto :goto_46d

    .line 476
    :catch_47a
    move-exception v0

    goto :goto_46d

    .line 455
    :catchall_47c
    move-exception v0

    move v3, v2

    move-object v2, v0

    goto :goto_42c

    .line 476
    :catch_480
    move-exception v0

    goto/16 :goto_6e
.end method

.method private b()V
    .registers 3

    .prologue
    .line 384
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 385
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0}, LWV;->a()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 386
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a()V

    .line 387
    iget v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->stopSelf(I)V

    .line 389
    :cond_13
    monitor-exit v1

    .line 390
    return-void

    .line 389
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public static b(Landroid/content/Context;LkY;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 622
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 623
    const-string v1, "com.google.android.apps.docs.sync.syncadapter.CANCEL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 624
    invoke-virtual {p1, v0}, LkY;->a(Landroid/content/Intent;)V

    .line 625
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 626
    return-void
.end method

.method private b(LkY;LWX;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 191
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.sync.syncadapter.NEW_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "status"

    invoke-virtual {p2}, LWX;->a()LWW;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 192
    invoke-virtual {p1, v0}, LkY;->a(Landroid/content/Intent;)V

    .line 193
    sget-object v1, LWT;->a:[I

    invoke-virtual {p2}, LWX;->a()LWW;

    move-result-object v2

    invoke-virtual {v2}, LWW;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_62

    .line 207
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, LWX;->a()LWW;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :pswitch_40
    const-string v1, "bytes_loaded"

    invoke-virtual {p2}, LWX;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 199
    const-string v1, "bytes_expected"

    invoke-virtual {p2}, LWX;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 209
    :goto_52
    :pswitch_52
    const-string v1, "com.google.android.apps.docs.permission.SYNC_STATUS"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 210
    return-void

    .line 202
    :pswitch_58
    const-string v1, "error_code"

    invoke-virtual {p2}, LWX;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_52

    .line 193
    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_52
        :pswitch_52
        :pswitch_40
        :pswitch_58
        :pswitch_52
    .end packed-switch
.end method


# virtual methods
.method a(Ljava/lang/Exception;LkY;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 487
    const-string v0, "ContentSyncService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error syncing id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, LkY;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 489
    const/4 v0, -0x1

    invoke-static {v0}, LWX;->a(I)LWX;

    move-result-object v0

    .line 490
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkY;LWX;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, p2}, LWV;->a(LkY;)LWX;

    move-result-object v0

    .line 495
    if-nez v0, :cond_3b

    const-wide/16 v0, 0x0

    .line 497
    :goto_2c
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LeQ;

    const-string v3, "pinning"

    const-string v4, "content_sync_error"

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v4, v5, v0}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 499
    return-void

    .line 495
    :cond_3b
    invoke-virtual {v0}, LWX;->a()J

    move-result-wide v0

    goto :goto_2c
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter

    .prologue
    .line 293
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .registers 9

    .prologue
    const/4 v1, 0x4

    .line 266
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceService;->onCreate()V

    .line 267
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v2, v1

    invoke-direct/range {v0 .. v6}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 270
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_1d

    .line 274
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 276
    :cond_1d
    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/util/concurrent/ExecutorService;

    .line 278
    new-instance v0, LZO;

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LKS;

    const-string v2, "contentSyncBackoffMinWait"

    const/16 v3, 0x3e8

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LKS;

    const-string v4, "contentSyncBackoffWaitGrowthFactor"

    const-wide/high16 v5, 0x4000

    invoke-interface {v3, v4, v5, v6}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LKS;

    const-string v6, "contentSyncBackoffMaxWait"

    const v7, 0x927c0

    invoke-interface {v5, v6, v7}, LKS;->a(Ljava/lang/String;I)I

    move-result v5

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, LZO;-><init>(JDJ)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZO;

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 288
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:J

    .line 289
    return-void
.end method

.method public onDestroy()V
    .registers 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 302
    invoke-super {p0}, Lcom/google/android/apps/docs/GuiceService;->onDestroy()V

    .line 303
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x2

    .line 315
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/GuiceService;->onStartCommand(Landroid/content/Intent;II)I

    .line 316
    invoke-static {p1}, LkY;->a(Landroid/content/Intent;)LkY;

    move-result-object v1

    .line 318
    iget-object v0, v1, LkY;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/String;

    .line 320
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 321
    const-string v2, "Action should not be null"

    invoke-static {v0, v2}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    const-string v2, "com.google.android.apps.docs.sync.syncadapter.SYNC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_60

    .line 323
    const-string v0, "hide_notification"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 324
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 325
    :try_start_27
    iget-object v3, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v3, v1}, LWV;->a(LkY;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 326
    monitor-exit v2

    .line 377
    :goto_30
    return v9

    .line 328
    :cond_31
    new-instance v3, LWU;

    iget-object v4, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZE;

    invoke-direct {v3, p0, v1, v4}, LWU;-><init>(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;LkY;LZE;)V

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkY;LXM;)Ljava/util/concurrent/Future;

    move-result-object v3

    .line 329
    invoke-static {}, LWX;->a()LWX;

    move-result-object v4

    .line 330
    invoke-virtual {v4, v0}, LWX;->a(Z)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, v1, v4}, LWV;->a(LkY;LWX;)V

    .line 333
    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkY;LWX;)V

    .line 334
    iput p3, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:I

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LeQ;

    const-string v1, "pinning"

    const-string v3, "request_content_sync"

    invoke-virtual {v0, v1, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    monitor-exit v2

    goto :goto_30

    :catchall_5d
    move-exception v0

    monitor-exit v2
    :try_end_5f
    .catchall {:try_start_27 .. :try_end_5f} :catchall_5d

    throw v0

    .line 341
    :cond_60
    const-string v2, "com.google.android.apps.docs.sync.syncadapter.CANCEL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_af

    .line 342
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 343
    :try_start_6b
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, v1}, LWV;->a(LkY;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 346
    invoke-static {}, LWX;->c()LWX;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkY;LWX;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 348
    if-eqz v0, :cond_88

    .line 353
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 356
    :cond_88
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, v1}, LWV;->a(LkY;)LWX;

    move-result-object v0

    .line 357
    iget-object v3, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LeQ;

    const-string v4, "pinning"

    const-string v5, "content_sync_cancelled"

    const/4 v6, 0x0

    invoke-virtual {v0}, LWX;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v6, v0}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, v1}, LWV;->a(LkY;)V

    .line 363
    :cond_a5
    iput p3, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:I

    .line 364
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 365
    monitor-exit v2

    goto :goto_30

    :catchall_ac
    move-exception v0

    monitor-exit v2
    :try_end_ae
    .catchall {:try_start_6b .. :try_end_ae} :catchall_ac

    throw v0

    .line 368
    :cond_af
    const-string v2, "com.google.android.apps.docs.sync.syncadapter.QUERY_STATUS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c9

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LWV;

    invoke-virtual {v0, v1}, LWV;->a(LkY;)LWX;

    move-result-object v0

    .line 370
    if-eqz v0, :cond_c2

    .line 371
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(LkY;LWX;)V

    .line 374
    :cond_c2
    iput p3, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:I

    .line 375
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    goto/16 :goto_30

    .line 380
    :cond_c9
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
