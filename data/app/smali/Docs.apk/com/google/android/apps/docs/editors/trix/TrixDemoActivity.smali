.class public Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;
.super Lcom/google/android/apps/docs/RoboFragmentActivity;
.source "TrixDemoActivity.java"


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field public a:LHq;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LKt;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LNf;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/TrixGLGridView;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 32
    const-string v0, "TrixDemoActivity"

    sput-object v0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;-><init>()V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->k:Z

    .line 54
    new-instance v0, LHb;

    invoke-direct {v0, p0}, LHb;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LNf;

    return-void
.end method

.method public static synthetic a()Ljava/lang/String;
    .registers 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->e()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->k:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->j:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->j:Z

    return v0
.end method

.method private d()V
    .registers 5

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->j:Z

    if-nez v0, :cond_f

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LNe;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->d:Ljava/lang/String;

    const-string v2, "wise"

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LNf;

    invoke-interface {v0, v1, v2, v3}, LNe;->a(Ljava/lang/String;Ljava/lang/String;LNf;)V

    .line 123
    :cond_f
    return-void
.end method

.method private e()V
    .registers 5

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LHq;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LHq;->a(Ljava/lang/String;Ljava/lang/String;LHs;)V

    .line 127
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    sget v0, LsF;->trix_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->setContentView(I)V

    .line 85
    sget v0, LsD;->trix_grid_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/TrixGLGridView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixGLGridView;

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LKt;

    invoke-virtual {v1, v0}, LKt;->a(Landroid/content/Intent;)V

    .line 90
    new-instance v1, LJX;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LKt;

    invoke-virtual {v2}, LKt;->a()Z

    move-result v2

    invoke-direct {v1, v0, v2}, LJX;-><init>(Landroid/content/Intent;Z)V

    .line 91
    invoke-virtual {v1, p0}, LJX;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->d:Ljava/lang/String;

    .line 93
    invoke-virtual {v1}, LJX;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->b:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_42

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LKS;

    const-string v2, "trixDebugDocumentId"

    const-string v3, "0AjnK2jXQOslKcjRzYmRhUzUtdXJsbEVmQWlWSjR1SlE"

    invoke-interface {v0, v2, v3}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->b:Ljava/lang/String;

    .line 100
    :cond_42
    const-string v0, "spreadsheet"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LJX;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->c:Ljava/lang/String;

    .line 101
    invoke-virtual {v1}, LJX;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->i:Z

    .line 102
    return-void
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixGLGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixGLGridView;->onPause()V

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->k:Z

    .line 116
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onPause()V

    .line 117
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 105
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onResume()V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->k:Z

    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->d()V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixGLGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixGLGridView;->onResume()V

    .line 110
    return-void
.end method
