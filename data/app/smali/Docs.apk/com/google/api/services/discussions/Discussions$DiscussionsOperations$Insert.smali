.class public Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions"


# instance fields
.field private targetId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;Ljava/lang/String;Lcom/google/api/services/discussions/model/Discussion;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;->this$1:Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;

    .line 253
    iget-object v0, p1, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->this$0:Lcom/google/api/services/discussions/Discussions;

    sget-object v1, Laeh;->f:Laeh;

    const-string v2, "targets/{targetId}/discussions"

    invoke-direct {p0, v0, v1, v2, p3}, Lcom/google/api/services/discussions/DiscussionsRequest;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 254
    const-string v0, "Required parameter targetId must be specified."

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;->targetId:Ljava/lang/String;

    .line 255
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Discussion;
    .registers 3

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;->a()Laen;

    move-result-object v1

    .line 266
    const-class v0, Lcom/google/api/services/discussions/model/Discussion;

    invoke-virtual {v1, v0}, Laen;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion;

    .line 268
    invoke-virtual {v1}, Laen;->a()Laee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Discussion;->a(Laee;)V

    .line 269
    return-object v0
.end method
