.class public Lcom/google/api/services/discussions/Discussions$Posts$Patch;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions/{discussionId}/posts/{postId}"


# instance fields
.field private discussionId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private postId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private targetId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$Posts;
