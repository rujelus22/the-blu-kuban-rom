.class public Lcom/google/api/services/discussions/DiscussionsRequest;
.super LaeK;
.source "DiscussionsRequest.java"


# instance fields
.field private alt:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private fields:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private key:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private oauthToken:Ljava/lang/String;
    .annotation runtime Lafu;
        a = "oauth_token"
    .end annotation
.end field

.field private prettyPrint:Ljava/lang/Boolean;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private quotaUser:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private userIp:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, LaeK;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/discussions/DiscussionsRequest;
    .registers 2
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/api/services/discussions/DiscussionsRequest;->oauthToken:Ljava/lang/String;

    .line 115
    return-object p0
.end method
