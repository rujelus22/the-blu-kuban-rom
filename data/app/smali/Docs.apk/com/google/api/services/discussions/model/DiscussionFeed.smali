.class public final Lcom/google/api/services/discussions/model/DiscussionFeed;
.super LaeN;
.source "DiscussionFeed.java"


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation runtime Lafu;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/discussions/model/Discussion;",
            ">;"
        }
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private nextPageToken:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private nextStartFrom:Lafo;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private responseHeaders:Laee;

.field private title:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 74
    const-class v0, Lcom/google/api/services/discussions/model/Discussion;

    invoke-static {v0}, Lafj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, LaeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lafo;
    .registers 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/api/services/discussions/model/DiscussionFeed;->nextStartFrom:Lafo;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/discussions/model/Discussion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/api/services/discussions/model/DiscussionFeed;->items:Ljava/util/List;

    return-object v0
.end method

.method public a(Laee;)V
    .registers 2
    .parameter

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/api/services/discussions/model/DiscussionFeed;->responseHeaders:Laee;

    .line 202
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/api/services/discussions/model/DiscussionFeed;->nextPageToken:Ljava/lang/String;

    return-object v0
.end method
