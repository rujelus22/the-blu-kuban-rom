.class public Lcom/google/api/services/discussions/DiscussionsScopes;
.super Ljava/lang/Object;
.source "DiscussionsScopes.java"


# static fields
.field public static final DISCUSSIONS:Ljava/lang/String; = "https://www.googleapis.com/auth/discussions"

.field public static final DISCUSSIONS_READONLY:Ljava/lang/String; = "https://www.googleapis.com/auth/discussions.readonly"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
