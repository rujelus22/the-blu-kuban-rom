.class public Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions/{discussionId}"


# instance fields
.field private discussionId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private targetId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 521
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;->this$1:Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;

    .line 522
    iget-object v0, p1, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->this$0:Lcom/google/api/services/discussions/Discussions;

    sget-object v1, Laeh;->a:Laeh;

    const-string v2, "targets/{targetId}/discussions/{discussionId}"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/services/discussions/DiscussionsRequest;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 523
    const-string v0, "Required parameter targetId must be specified."

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;->targetId:Ljava/lang/String;

    .line 524
    const-string v0, "Required parameter discussionId must be specified."

    invoke-static {p3, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;->discussionId:Ljava/lang/String;

    .line 525
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;->a()Laen;

    move-result-object v0

    .line 534
    invoke-virtual {v0}, Laen;->a()V

    .line 535
    return-void
.end method
