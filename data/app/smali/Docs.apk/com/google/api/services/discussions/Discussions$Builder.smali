.class public final Lcom/google/api/services/discussions/Discussions$Builder;
.super LadS;
.source "Discussions.java"


# direct methods
.method constructor <init>(Laeq;LaeP;Laeb;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1705
    invoke-direct {p0, p1, p2, p3}, LadS;-><init>(Laeq;LaeP;Laeb;)V

    .line 1706
    return-void
.end method


# virtual methods
.method public bridge synthetic a(LaeL;)LaeH;
    .registers 3
    .parameter

    .prologue
    .line 1694
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(LaeL;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Laeb;)LaeH;
    .registers 3
    .parameter

    .prologue
    .line 1694
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(Laeb;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public a(LaeL;)Lcom/google/api/services/discussions/Discussions$Builder;
    .registers 2
    .parameter

    .prologue
    .line 1768
    invoke-super {p0, p1}, LadS;->a(LaeL;)LaeH;

    .line 1769
    return-object p0
.end method

.method public a(Laeb;)Lcom/google/api/services/discussions/Discussions$Builder;
    .registers 2
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1749
    invoke-super {p0, p1}, LadS;->a(Laeb;)LaeH;

    .line 1750
    return-object p0
.end method

.method public a()Lcom/google/api/services/discussions/Discussions;
    .registers 10

    .prologue
    .line 1725
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1726
    new-instance v0, Lcom/google/api/services/discussions/Discussions;

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Laeq;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()LaeL;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Laem;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()LaeP;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()LaeR;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Laeb;

    move-result-object v6

    invoke-virtual {v6}, Laeb;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->c()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/api/services/discussions/Discussions;-><init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;)V

    .line 1735
    :goto_2b
    return-object v0

    :cond_2c
    new-instance v0, Lcom/google/api/services/discussions/Discussions;

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Laeq;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()LaeL;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Laem;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()LaeP;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()LaeR;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->c()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/api/services/discussions/Discussions;-><init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2b
.end method
