.class public Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$List;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions"


# instance fields
.field private maxResults:Ljava/lang/Long;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private pageToken:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private startFrom:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private targetId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$List;->this$1:Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;

    .line 355
    iget-object v0, p1, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->this$0:Lcom/google/api/services/discussions/Discussions;

    sget-object v1, Laeh;->b:Laeh;

    const-string v2, "targets/{targetId}/discussions"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/services/discussions/DiscussionsRequest;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 356
    const-string v0, "Required parameter targetId must be specified."

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$List;->targetId:Ljava/lang/String;

    .line 357
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/DiscussionFeed;
    .registers 3

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$List;->a()Laen;

    move-result-object v1

    .line 367
    const-class v0, Lcom/google/api/services/discussions/model/DiscussionFeed;

    invoke-virtual {v1, v0}, Laen;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/DiscussionFeed;

    .line 369
    invoke-virtual {v1}, Laen;->a()Laee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/DiscussionFeed;->a(Laee;)V

    .line 370
    return-object v0
.end method
