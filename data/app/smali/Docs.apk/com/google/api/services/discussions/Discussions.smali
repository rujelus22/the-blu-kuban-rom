.class public Lcom/google/api/services/discussions/Discussions;
.super LadR;
.source "Discussions.java"


# static fields
.field public static final DEFAULT_BASE_PATH:Ljava/lang/String; = "/discussions/v1/"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_BASE_URL:Ljava/lang/String; = "https://www.googleapis.com/discussions/v1/"

.field public static final DEFAULT_ROOT_URL:Ljava/lang/String; = "https://www.googleapis.com/"

.field public static final DEFAULT_SERVICE_PATH:Ljava/lang/String; = "discussions/v1/"


# direct methods
.method constructor <init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 150
    invoke-direct/range {p0 .. p7}, LadR;-><init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method constructor <init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 180
    invoke-direct/range {p0 .. p8}, LadR;-><init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method public static a(Laeq;LaeP;)Lcom/google/api/services/discussions/Discussions$Builder;
    .registers 5
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lcom/google/api/services/discussions/Discussions$Builder;

    new-instance v1, Laeb;

    const-string v2, "https://www.googleapis.com/discussions/v1/"

    invoke-direct {v1, v2}, Laeb;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/google/api/services/discussions/Discussions$Builder;-><init>(Laeq;LaeP;Laeb;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/Discussions$Authors;
    .registers 2

    .prologue
    .line 1591
    new-instance v0, Lcom/google/api/services/discussions/Discussions$Authors;

    invoke-direct {v0, p0}, Lcom/google/api/services/discussions/Discussions$Authors;-><init>(Lcom/google/api/services/discussions/Discussions;)V

    return-object v0
.end method

.method public a()Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;
    .registers 2

    .prologue
    .line 218
    new-instance v0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;

    invoke-direct {v0, p0}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;-><init>(Lcom/google/api/services/discussions/Discussions;)V

    return-object v0
.end method

.method public a()Lcom/google/api/services/discussions/Discussions$Posts;
    .registers 2

    .prologue
    .line 742
    new-instance v0, Lcom/google/api/services/discussions/Discussions$Posts;

    invoke-direct {v0, p0}, Lcom/google/api/services/discussions/Discussions$Posts;-><init>(Lcom/google/api/services/discussions/Discussions;)V

    return-object v0
.end method

.method protected a(LaeK;)V
    .registers 2
    .parameter

    .prologue
    .line 192
    invoke-super {p0, p1}, LadR;->a(LaeK;)V

    .line 193
    return-void
.end method
