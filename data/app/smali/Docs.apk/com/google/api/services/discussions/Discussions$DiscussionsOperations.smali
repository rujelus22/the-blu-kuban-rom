.class public Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;
.super Ljava/lang/Object;
.source "Discussions.java"


# instance fields
.field final synthetic this$0:Lcom/google/api/services/discussions/Discussions;


# direct methods
.method public constructor <init>(Lcom/google/api/services/discussions/Discussions;)V
    .registers 2
    .parameter

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->this$0:Lcom/google/api/services/discussions/Discussions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/api/services/discussions/model/Discussion;)Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 240
    new-instance v0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;-><init>(Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;Ljava/lang/String;Lcom/google/api/services/discussions/model/Discussion;)V

    .line 241
    iget-object v1, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->this$0:Lcom/google/api/services/discussions/Discussions;

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/Discussions;->a(LaeK;)V

    .line 242
    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$List;
    .registers 4
    .parameter

    .prologue
    .line 342
    new-instance v0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$List;

    invoke-direct {v0, p0, p1}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$List;-><init>(Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;Ljava/lang/String;)V

    .line 343
    iget-object v1, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->this$0:Lcom/google/api/services/discussions/Discussions;

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/Discussions;->a(LaeK;)V

    .line 344
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 509
    new-instance v0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;-><init>(Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object v1, p0, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->this$0:Lcom/google/api/services/discussions/Discussions;

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/Discussions;->a(LaeK;)V

    .line 511
    return-object v0
.end method
