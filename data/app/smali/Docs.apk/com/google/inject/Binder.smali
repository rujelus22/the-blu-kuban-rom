.class public interface abstract Lcom/google/inject/Binder;
.super Ljava/lang/Object;
.source "Binder.java"


# virtual methods
.method public abstract a(LaoL;)LaoM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LaoL",
            "<TT;>;)",
            "LaoM",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Class;)LaoM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LaoM",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a()LaoN;
.end method

.method public abstract a(Laop;)LaoQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;)",
            "LaoQ",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a()Laow;
.end method

.method public abstract a(Laop;)Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Class;)Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)Lcom/google/inject/Binder;
.end method

.method public abstract a(Laov;)V
.end method

.method public abstract a(LarB;)V
.end method

.method public abstract a(Ljava/lang/Class;LaoC;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LaoC;",
            ")V"
        }
    .end annotation
.end method

.method public varargs abstract a(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public varargs abstract a([Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation
.end method
