.class final LahQ;
.super LahR;
.source "LocalCache.java"

# interfaces
.implements Lahz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LahR",
        "<TK;TV;>;",
        "Lahz",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field a:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile b:J

.field b:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILahz;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const-wide v1, 0x7fffffffffffffffL

    .line 1548
    invoke-direct {p0, p1, p2, p3, p4}, LahR;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILahz;)V

    .line 1553
    iput-wide v1, p0, LahQ;->a:J

    .line 1565
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahQ;->a:Lahz;

    .line 1578
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahQ;->b:Lahz;

    .line 1593
    iput-wide v1, p0, LahQ;->b:J

    .line 1605
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahQ;->c:Lahz;

    .line 1618
    invoke-static {}, LagZ;->a()Lahz;

    move-result-object v0

    iput-object v0, p0, LahQ;->d:Lahz;

    .line 1549
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 1557
    iget-wide v0, p0, LahQ;->a:J

    return-wide v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 1562
    iput-wide p1, p0, LahQ;->a:J

    .line 1563
    return-void
.end method

.method public a(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1575
    iput-object p1, p0, LahQ;->a:Lahz;

    .line 1576
    return-void
.end method

.method public b()J
    .registers 3

    .prologue
    .line 1597
    iget-wide v0, p0, LahQ;->b:J

    return-wide v0
.end method

.method public b()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1570
    iget-object v0, p0, LahQ;->a:Lahz;

    return-object v0
.end method

.method public b(J)V
    .registers 3
    .parameter

    .prologue
    .line 1602
    iput-wide p1, p0, LahQ;->b:J

    .line 1603
    return-void
.end method

.method public b(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1588
    iput-object p1, p0, LahQ;->b:Lahz;

    .line 1589
    return-void
.end method

.method public c()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1583
    iget-object v0, p0, LahQ;->b:Lahz;

    return-object v0
.end method

.method public c(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1615
    iput-object p1, p0, LahQ;->c:Lahz;

    .line 1616
    return-void
.end method

.method public d()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1610
    iget-object v0, p0, LahQ;->c:Lahz;

    return-object v0
.end method

.method public d(Lahz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1628
    iput-object p1, p0, LahQ;->d:Lahz;

    .line 1629
    return-void
.end method

.method public e()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1623
    iget-object v0, p0, LahQ;->d:Lahz;

    return-object v0
.end method
