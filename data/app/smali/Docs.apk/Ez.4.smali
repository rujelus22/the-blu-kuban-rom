.class public LEz;
.super Ljava/lang/Object;
.source "RecordingRenderer.java"

# interfaces
.implements LFf;


# instance fields
.field private a:LFe;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LEN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LEz;->a:Ljava/util/ArrayList;

    .line 67
    new-instance v0, LDZ;

    invoke-direct {v0}, LDZ;-><init>()V

    iput-object v0, p0, LEz;->a:LFe;

    return-void
.end method

.method private static a(Landroid/graphics/Paint;LFd;)F
    .registers 5
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x4348

    .line 79
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    .line 81
    invoke-virtual {p0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 82
    invoke-interface {p1, p0}, LFd;->a(Landroid/graphics/Paint;)F

    move-result v1

    mul-float/2addr v1, v0

    div-float/2addr v1, v2

    .line 83
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 85
    return v1
.end method

.method private a(Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Paint$FontMetricsInt;
    .registers 4
    .parameter

    .prologue
    .line 261
    if-nez p1, :cond_4

    .line 262
    const/4 v0, 0x0

    .line 272
    :goto_3
    return-object v0

    .line 265
    :cond_4
    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    .line 266
    iget v1, p1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 267
    iget v1, p1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 268
    iget v1, p1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 269
    iget v1, p1, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    .line 270
    iget v1, p1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    goto :goto_3
.end method

.method private a(LFd;LEO;FFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 371
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, p5}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 372
    invoke-direct {p0, p6}, LEz;->a(Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    .line 373
    new-instance v0, LFh;

    move-object v1, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LFh;-><init>(LFd;Landroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;FF)V

    .line 376
    iget-object v1, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v2, LEH;

    invoke-direct {v2, p0, v0, p2}, LEH;-><init>(LEz;LFh;LEO;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    return-void
.end method

.method private a(LFd;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 310
    invoke-interface {p1, p5}, LFd;->a(Landroid/graphics/Paint;)F

    move-result v0

    .line 311
    invoke-static {p5, p1}, LEz;->a(Landroid/graphics/Paint;LFd;)F

    move-result v1

    .line 313
    div-float/2addr v0, v1

    invoke-static {p2, p3, p4, v0}, LFa;->a(Landroid/graphics/Path;FFF)Landroid/graphics/Path;

    move-result-object v0

    .line 314
    iget-object v1, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v2, LEP;

    invoke-direct {v2, v0, p5}, LEP;-><init>(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    return-void
.end method

.method private b(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 294
    invoke-static/range {p1 .. p7}, LFa;->a(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Path;

    move-result-object v2

    .line 297
    new-instance v1, LEC;

    invoke-direct {v1, p0, p1, p2, p3}, LEC;-><init>(LEz;Ljava/lang/CharSequence;II)V

    move-object v0, p0

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 304
    invoke-direct/range {v0 .. v5}, LEz;->a(LFd;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    .line 305
    return-void
.end method

.method private b([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 278
    invoke-static/range {p1 .. p7}, LFa;->a([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Path;

    move-result-object v2

    .line 281
    new-instance v1, LEB;

    invoke-direct {v1, p0, p1, p2, p3}, LEB;-><init>(LEz;[CII)V

    move-object v0, p0

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 288
    invoke-direct/range {v0 .. v5}, LEz;->a(LFd;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    .line 289
    return-void
.end method

.method private c(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 348
    sub-int v3, p3, p2

    .line 350
    new-instance v1, LEF;

    invoke-direct {v1, p0, v0, v3}, LEF;-><init>(LEz;Ljava/lang/CharSequence;I)V

    .line 357
    new-instance v2, LEG;

    invoke-direct {v2, p0, v0, v3}, LEG;-><init>(LEz;Ljava/lang/CharSequence;I)V

    move-object v0, p0

    move v3, p4

    move v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 365
    invoke-direct/range {v0 .. v6}, LEz;->a(LFd;LEO;FFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V

    .line 366
    return-void
.end method

.method private c([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 320
    new-array v3, p3, [C

    .line 321
    const/4 v0, 0x0

    :goto_3
    if-ge v0, p3, :cond_e

    .line 322
    add-int v1, p2, v0

    aget-char v1, p1, v1

    aput-char v1, v3, v0

    .line 321
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 325
    :cond_e
    new-instance v1, LED;

    invoke-direct {v1, p0, v3, p3}, LED;-><init>(LEz;[CI)V

    .line 332
    new-instance v2, LEE;

    invoke-direct {v2, p0, v3, p3}, LEE;-><init>(LEz;[CI)V

    move-object v0, p0

    move v3, p4

    move v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 340
    invoke-direct/range {v0 .. v6}, LEz;->a(LFd;LEO;FFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V

    .line 341
    return-void
.end method


# virtual methods
.method public a()LFe;
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, LEz;->a:LFe;

    return-object v0
.end method

.method public a(FFFF)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 133
    return-void
.end method

.method public a(FFFFLandroid/graphics/Paint;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 215
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6, p5}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 216
    iget-object v7, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v0, LEJ;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, LEJ;-><init>(LEz;FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v1, LEI;

    invoke-direct {v1, p0, p1}, LEI;-><init>(LEz;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    return-void
.end method

.method public a(IF)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v1, LEA;

    invoke-direct {v1, p0, p1, p2}, LEA;-><init>(LEz;IF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    return-void
.end method

.method public a(LFe;)V
    .registers 2
    .parameter

    .prologue
    .line 118
    iput-object p1, p0, LEz;->a:LFe;

    .line 119
    return-void
.end method

.method public declared-synchronized a(LFf;F)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 106
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LEz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEN;

    .line 107
    invoke-interface {v0, p1, p2}, LEN;->a(LFf;F)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_17

    goto :goto_7

    .line 106
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0

    .line 109
    :cond_1a
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Landroid/graphics/Canvas;F)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 95
    monitor-enter p0

    :try_start_1
    new-instance v0, LDU;

    invoke-direct {v0, p1}, LDU;-><init>(Landroid/graphics/Canvas;)V

    .line 96
    invoke-virtual {p0, v0, p2}, LEz;->a(LFf;F)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 97
    monitor-exit p0

    return-void

    .line 95
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 238
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 239
    iget-object v1, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v2, LEL;

    invoke-direct {v2, p0, p1, v0}, LEL;-><init>(LEz;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    return-void
.end method

.method public a(Landroid/text/style/ReplacementSpan;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 251
    new-instance v11, Landroid/graphics/Paint;

    move-object/from16 v0, p9

    invoke-direct {v11, v0}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 252
    iget-object v12, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v1, LEM;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-direct/range {v1 .. v11}, LEM;-><init>(LEz;Landroid/text/style/ReplacementSpan;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    return-void
.end method

.method public a(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    invoke-virtual {p0}, LEz;->a()LFe;

    move-result-object v0

    invoke-interface {v0, p7, p6}, LFe;->a(Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Paint;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 160
    invoke-direct/range {p0 .. p7}, LEz;->b(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V

    .line 164
    :goto_d
    return-void

    .line 162
    :cond_e
    invoke-direct/range {p0 .. p7}, LEz;->c(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V

    goto :goto_d
.end method

.method public a([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 170
    invoke-virtual {p0}, LEz;->a()LFe;

    move-result-object v0

    invoke-interface {v0, p7, p6}, LFe;->a(Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Paint;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 171
    invoke-direct/range {p0 .. p7}, LEz;->b([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V

    .line 175
    :goto_d
    return-void

    .line 173
    :cond_e
    invoke-direct/range {p0 .. p7}, LEz;->c([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V

    goto :goto_d
.end method

.method public b(FFFFLandroid/graphics/Paint;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6, p5}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 228
    iget-object v7, p0, LEz;->a:Ljava/util/ArrayList;

    new-instance v0, LEK;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, LEK;-><init>(LEz;FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    return-void
.end method
