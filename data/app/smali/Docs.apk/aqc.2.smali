.class final Laqc;
.super LaoY;
.source "LinkedProviderBindingImpl.java"

# interfaces
.implements Laps;
.implements LarF;
.implements Larn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LaoY",
        "<TT;>;",
        "Laps;",
        "LarF",
        "<TT;>;",
        "Larn;"
    }
.end annotation


# instance fields
.field final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<+",
            "LatG",
            "<+TT;>;>;"
        }
    .end annotation
.end field

.field final a:Laps;


# direct methods
.method private constructor <init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Laop;Laps;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Object;",
            "LapY",
            "<+TT;>;",
            "LaqC;",
            "Laop",
            "<+",
            "LatG",
            "<+TT;>;>;",
            "Laps;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct/range {p0 .. p5}, LaoY;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;)V

    .line 41
    iput-object p6, p0, Laqc;->a:Laop;

    .line 42
    iput-object p7, p0, Laqc;->a:Laps;

    .line 43
    return-void
.end method

.method constructor <init>(Ljava/lang/Object;Laop;LaqC;Laop;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Laop",
            "<TT;>;",
            "LaqC;",
            "Laop",
            "<+",
            "LatG",
            "<+TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, LaoY;-><init>(Ljava/lang/Object;Laop;LaqC;)V

    .line 54
    iput-object p4, p0, Laqc;->a:Laop;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Laqc;->a:Laps;

    .line 56
    return-void
.end method

.method static a(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Laop;Laps;)Laqc;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LapL;",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Object;",
            "LapY",
            "<+TT;>;",
            "LaqC;",
            "Laop",
            "<+",
            "LatG",
            "<+TT;>;>;",
            "Laps;",
            ")",
            "Laqc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Laqc;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Laqc;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Laop;Laps;)V

    return-object v0
.end method


# virtual methods
.method public a(Laop;)LaoY;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<TT;>;)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 89
    new-instance v0, Laqc;

    invoke-virtual {p0}, Laqc;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Laqc;->a()LaqC;

    move-result-object v2

    iget-object v3, p0, Laqc;->a:Laop;

    invoke-direct {v0, v1, p1, v2, v3}, Laqc;-><init>(Ljava/lang/Object;Laop;LaqC;Laop;)V

    return-object v0
.end method

.method public a(LaqC;)LaoY;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaqC;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Laqc;

    invoke-virtual {p0}, Laqc;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Laqc;->a()Laop;

    move-result-object v2

    iget-object v3, p0, Laqc;->a:Laop;

    invoke-direct {v0, v1, v2, p1, v3}, Laqc;-><init>(Ljava/lang/Object;Laop;LaqC;Laop;)V

    return-object v0
.end method

.method public a(LapL;Lapu;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Laqc;->a:Laps;

    if-eqz v0, :cond_9

    .line 76
    iget-object v0, p0, Laqc;->a:Laps;

    invoke-interface {v0, p1, p2}, Laps;->a(LapL;Lapu;)V

    .line 78
    :cond_9
    return-void
.end method

.method public c()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Laqc;->a:Laop;

    invoke-static {v0}, Larg;->a(Laop;)Larg;

    move-result-object v0

    invoke-static {v0}, Lajm;->a(Ljava/lang/Object;)Lajm;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 108
    instance-of v1, p1, Laqc;

    if-eqz v1, :cond_2e

    .line 109
    check-cast p1, Laqc;

    .line 110
    invoke-virtual {p0}, Laqc;->a()Laop;

    move-result-object v1

    invoke-virtual {p1}, Laqc;->a()Laop;

    move-result-object v2

    invoke-virtual {v1, v2}, Laop;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    invoke-virtual {p0}, Laqc;->a()LaqC;

    move-result-object v1

    invoke-virtual {p1}, Laqc;->a()LaqC;

    move-result-object v2

    invoke-virtual {v1, v2}, LaqC;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, Laqc;->a:Laop;

    iget-object v2, p1, Laqc;->a:Laop;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/4 v0, 0x1

    .line 114
    :cond_2e
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 120
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Laqc;->a()Laop;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Laqc;->a()LaqC;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Laqc;->a:Laop;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 98
    const-class v0, LarF;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {p0}, Laqc;->a()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {p0}, Laqc;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "scope"

    invoke-virtual {p0}, Laqc;->a()LaqC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "provider"

    iget-object v2, p0, Laqc;->a:Laop;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
