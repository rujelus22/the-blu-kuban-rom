.class public LabU;
.super Landroid/widget/ScrollView;
.source "LinearLayoutListView.java"


# instance fields
.field a:I

.field final synthetic a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

.field b:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 212
    iput-object p1, p0, LabU;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    .line 213
    invoke-direct {p0, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 214
    invoke-virtual {p0}, LabU;->getScrollX()I

    move-result v0

    iput v0, p0, LabU;->a:I

    .line 215
    invoke-virtual {p0}, LabU;->getScrollY()I

    move-result v0

    iput v0, p0, LabU;->b:I

    .line 216
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 220
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 221
    invoke-virtual {p0}, LabU;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 225
    iget v0, p0, LabU;->a:I

    if-ne v0, p1, :cond_12

    iget v0, p0, LabU;->b:I

    if-eq v0, p2, :cond_1b

    .line 226
    :cond_12
    iput p1, p0, LabU;->a:I

    .line 227
    iput p2, p0, LabU;->b:I

    .line 228
    iget-object v0, p0, LabU;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(IIII)V

    .line 230
    :cond_1b
    return-void
.end method
