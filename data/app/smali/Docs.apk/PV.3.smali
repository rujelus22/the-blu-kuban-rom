.class public final enum LPV;
.super Ljava/lang/Enum;
.source "DocumentTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPV;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPV;

.field private static final synthetic a:[LPV;

.field public static final enum b:LPV;

.field public static final enum c:LPV;

.field public static final enum d:LPV;

.field public static final enum e:LPV;

.field public static final enum f:LPV;

.field public static final enum g:LPV;

.field public static final enum h:LPV;

.field public static final enum i:LPV;

.field public static final enum j:LPV;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/16 v9, 0xe

    const/4 v8, 0x0

    .line 43
    new-instance v0, LPV;

    const-string v1, "DO_SYNC"

    invoke-static {}, LPU;->b()LPU;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "doSync"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->a:LPV;

    .line 46
    new-instance v0, LPV;

    const-string v1, "ENTRY_ID"

    invoke-static {}, LPU;->b()LPU;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "entryId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, LQa;->a([Ljava/lang/String;)LQa;

    move-result-object v3

    invoke-static {}, LPW;->a()LPW;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->b:LPV;

    .line 50
    new-instance v0, LPV;

    const-string v1, "CONTENT_ID"

    invoke-static {}, LPU;->b()LPU;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "contentId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-static {}, LPS;->a()LPS;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, LQb;->b:LQb;

    invoke-virtual {v3, v4, v5, v6}, LQa;->a(LPN;LPI;LQb;)LQa;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->c:LPV;

    .line 59
    new-instance v0, LPV;

    const-string v1, "PDF_CONTENT_ID"

    invoke-static {}, LPU;->b()LPU;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    const/16 v3, 0x1e

    new-instance v4, LQa;

    const-string v5, "pdfContentId"

    sget-object v6, LQc;->a:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-static {}, LPS;->a()LPS;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, LQb;->b:LQb;

    invoke-virtual {v4, v5, v6, v7}, LQa;->a(LPN;LPI;LQb;)LQa;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->d:LPV;

    .line 63
    new-instance v0, LPV;

    const-string v1, "HTML_URI"

    const/4 v2, 0x4

    invoke-static {}, LPU;->b()LPU;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    new-instance v4, LQa;

    const-string v5, "htmlUri"

    sget-object v6, LQc;->c:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3, v9, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->e:LPV;

    .line 66
    new-instance v0, LPV;

    const-string v1, "APP_ID"

    const/4 v2, 0x5

    invoke-static {}, LPU;->b()LPU;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    new-instance v4, LQa;

    const-string v5, "appId"

    sget-object v6, LQc;->a:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-static {}, LPw;->a()LPw;

    move-result-object v5

    invoke-virtual {v4, v5}, LQa;->a(LPN;)LQa;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->f:LPV;

    .line 70
    new-instance v0, LPV;

    const-string v1, "PIN_RETRY_COUNT"

    const/4 v2, 0x6

    invoke-static {}, LPU;->b()LPU;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    const/16 v4, 0x18

    new-instance v5, LQa;

    const-string v6, "pinRetryCount"

    sget-object v7, LQc;->a:LQc;

    invoke-direct {v5, v6, v7}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LQa;->a(Ljava/lang/Object;)LQa;

    move-result-object v5

    invoke-virtual {v5}, LQa;->b()LQa;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->g:LPV;

    .line 73
    new-instance v0, LPV;

    const-string v1, "MD5_CHECKSUM"

    const/4 v2, 0x7

    invoke-static {}, LPU;->b()LPU;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    const/16 v4, 0x19

    new-instance v5, LQa;

    const-string v6, "md5Checksum"

    sget-object v7, LQc;->c:LQc;

    invoke-direct {v5, v6, v7}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3, v4, v5}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->h:LPV;

    .line 76
    new-instance v0, LPV;

    const-string v1, "CONTENT_FRESH"

    const/16 v2, 0x8

    invoke-static {}, LPU;->b()LPU;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    const/16 v4, 0x19

    new-instance v5, LQa;

    const-string v6, "contentFresh"

    sget-object v7, LQc;->a:LQc;

    invoke-direct {v5, v6, v7}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LQa;->a(Ljava/lang/Object;)LQa;

    move-result-object v5

    invoke-virtual {v5}, LQa;->b()LQa;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->i:LPV;

    .line 79
    new-instance v0, LPV;

    const-string v1, "PDF_CONTENT_FRESH"

    const/16 v2, 0x9

    invoke-static {}, LPU;->b()LPU;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    const/16 v4, 0x1e

    new-instance v5, LQa;

    const-string v6, "pdfContentFresh"

    sget-object v7, LQc;->a:LQc;

    invoke-direct {v5, v6, v7}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LQa;->a(Ljava/lang/Object;)LQa;

    move-result-object v5

    invoke-virtual {v5}, LQa;->b()LQa;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPV;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPV;->j:LPV;

    .line 42
    const/16 v0, 0xa

    new-array v0, v0, [LPV;

    sget-object v1, LPV;->a:LPV;

    aput-object v1, v0, v8

    sget-object v1, LPV;->b:LPV;

    aput-object v1, v0, v10

    sget-object v1, LPV;->c:LPV;

    aput-object v1, v0, v11

    sget-object v1, LPV;->d:LPV;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, LPV;->e:LPV;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LPV;->f:LPV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LPV;->g:LPV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LPV;->h:LPV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LPV;->i:LPV;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LPV;->j:LPV;

    aput-object v2, v0, v1

    sput-object v0, LPV;->a:[LPV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 86
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPV;->a:LPI;

    .line 87
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPV;
    .registers 2
    .parameter

    .prologue
    .line 42
    const-class v0, LPV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPV;

    return-object v0
.end method

.method public static values()[LPV;
    .registers 1

    .prologue
    .line 42
    sget-object v0, LPV;->a:[LPV;

    invoke-virtual {v0}, [LPV;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPV;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, LPV;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 42
    invoke-virtual {p0}, LPV;->a()LPI;

    move-result-object v0

    return-object v0
.end method
