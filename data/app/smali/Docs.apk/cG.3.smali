.class LcG;
.super Ljava/lang/Object;
.source "GAThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:J

.field final synthetic a:LcF;

.field final synthetic a:Ljava/util/Map;


# direct methods
.method constructor <init>(LcF;Ljava/util/Map;J)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, LcG;->a:LcF;

    iput-object p2, p0, LcG;->a:Ljava/util/Map;

    iput-wide p3, p0, LcG;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .prologue
    .line 112
    iget-object v0, p0, LcG;->a:LcF;

    invoke-static {v0}, LcF;->a(LcF;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, LcG;->a:LcF;

    iget-object v1, p0, LcG;->a:Ljava/util/Map;

    invoke-static {v0, v1}, LcF;->a(LcF;Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 125
    :cond_12
    :goto_12
    return-void

    .line 115
    :cond_13
    iget-object v0, p0, LcG;->a:Ljava/util/Map;

    const-string v1, "clientId"

    iget-object v2, p0, LcG;->a:LcF;

    invoke-static {v2}, LcF;->a(LcF;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, LcG;->a:LcF;

    invoke-static {v0}, LcF;->b(LcF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3f

    .line 117
    iget-object v0, p0, LcG;->a:Ljava/util/Map;

    const-string v1, "campaign"

    iget-object v2, p0, LcG;->a:LcF;

    invoke-static {v2}, LcF;->b(LcF;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, LcG;->a:LcF;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LcF;->a(LcF;Ljava/lang/String;)Ljava/lang/String;

    .line 120
    :cond_3f
    iget-object v0, p0, LcG;->a:LcF;

    iget-object v1, p0, LcG;->a:Ljava/util/Map;

    invoke-static {v0, v1}, LcF;->a(LcF;Ljava/util/Map;)V

    .line 121
    iget-object v0, p0, LcG;->a:LcF;

    iget-object v1, p0, LcG;->a:Ljava/util/Map;

    invoke-static {v0, v1}, LcF;->b(LcF;Ljava/util/Map;)V

    .line 122
    iget-object v0, p0, LcG;->a:LcF;

    iget-object v1, p0, LcG;->a:Ljava/util/Map;

    invoke-static {v0, v1}, LcF;->c(LcF;Ljava/util/Map;)V

    .line 123
    iget-object v0, p0, LcG;->a:LcF;

    invoke-static {v0}, LcF;->a(LcF;)LcU;

    move-result-object v0

    iget-object v1, p0, LcG;->a:Ljava/util/Map;

    invoke-static {v0, v1}, LcR;->a(LcU;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 124
    iget-object v0, p0, LcG;->a:LcF;

    invoke-static {v0}, LcF;->a(LcF;)Ldg;

    move-result-object v0

    iget-wide v2, p0, LcG;->a:J

    iget-object v4, p0, LcG;->a:LcF;

    iget-object v5, p0, LcG;->a:Ljava/util/Map;

    invoke-static {v4, v5}, LcF;->a(LcF;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LcG;->a:LcF;

    invoke-static {v5}, LcF;->a(LcF;)Ljava/util/List;

    move-result-object v5

    invoke-interface/range {v0 .. v5}, Ldg;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    goto :goto_12
.end method
