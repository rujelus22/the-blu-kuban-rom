.class public final LrC;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LrR;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lrx;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LrU;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lry;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lqr;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LrQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 36
    iput-object p1, p0, LrC;->a:LYD;

    .line 37
    const-class v0, LrR;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LrC;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LrC;->a:LZb;

    .line 40
    const-class v0, Lrx;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LrC;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LrC;->b:LZb;

    .line 43
    const-class v0, LrU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LrC;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LrC;->c:LZb;

    .line 46
    const-class v0, Lry;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LrC;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LrC;->d:LZb;

    .line 49
    const-class v0, LrS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LrC;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LrC;->e:LZb;

    .line 52
    const-class v0, LrQ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LrC;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LrC;->f:LZb;

    .line 55
    return-void
.end method

.method static synthetic a(LrC;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LrC;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 130
    const-class v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    new-instance v1, LrD;

    invoke-direct {v1, p0}, LrD;-><init>(LrC;)V

    invoke-virtual {p0, v0, v1}, LrC;->a(Ljava/lang/Class;Laou;)V

    .line 138
    const-class v0, LrQ;

    new-instance v1, LrE;

    invoke-direct {v1, p0}, LrE;-><init>(LrC;)V

    invoke-virtual {p0, v0, v1}, LrC;->a(Ljava/lang/Class;Laou;)V

    .line 146
    const-class v0, LrB;

    new-instance v1, LrF;

    invoke-direct {v1, p0}, LrF;-><init>(LrC;)V

    invoke-virtual {p0, v0, v1}, LrC;->a(Ljava/lang/Class;Laou;)V

    .line 154
    const-class v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    new-instance v1, LrG;

    invoke-direct {v1, p0}, LrG;-><init>(LrC;)V

    invoke-virtual {p0, v0, v1}, LrC;->a(Ljava/lang/Class;Laou;)V

    .line 162
    const-class v0, LrR;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LrC;->a:LZb;

    invoke-virtual {p0, v0, v1}, LrC;->a(Laop;LZb;)V

    .line 163
    const-class v0, Lrx;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LrC;->b:LZb;

    invoke-virtual {p0, v0, v1}, LrC;->a(Laop;LZb;)V

    .line 164
    const-class v0, LrU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LrC;->c:LZb;

    invoke-virtual {p0, v0, v1}, LrC;->a(Laop;LZb;)V

    .line 165
    const-class v0, Lry;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LrC;->d:LZb;

    invoke-virtual {p0, v0, v1}, LrC;->a(Laop;LZb;)V

    .line 166
    const-class v0, LrS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LrC;->e:LZb;

    invoke-virtual {p0, v0, v1}, LrC;->a(Laop;LZb;)V

    .line 167
    const-class v0, LrQ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LrC;->f:LZb;

    invoke-virtual {p0, v0, v1}, LrC;->a(Laop;LZb;)V

    .line 168
    iget-object v0, p0, LrC;->a:LZb;

    iget-object v1, p0, LrC;->a:LYD;

    iget-object v1, v1, LYD;->a:LrC;

    iget-object v1, v1, LrC;->e:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 170
    iget-object v0, p0, LrC;->b:LZb;

    iget-object v1, p0, LrC;->a:LYD;

    iget-object v1, v1, LYD;->a:LrC;

    iget-object v1, v1, LrC;->d:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 172
    iget-object v0, p0, LrC;->c:LZb;

    iget-object v1, p0, LrC;->a:LYD;

    iget-object v1, v1, LYD;->a:LrC;

    iget-object v1, v1, LrC;->f:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 174
    iget-object v0, p0, LrC;->d:LZb;

    new-instance v1, LrH;

    invoke-direct {v1, p0}, LrH;-><init>(LrC;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 198
    iget-object v0, p0, LrC;->e:LZb;

    new-instance v1, LrI;

    invoke-direct {v1, p0}, LrI;-><init>(LrC;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 207
    iget-object v0, p0, LrC;->f:LZb;

    new-instance v1, LrJ;

    invoke-direct {v1, p0}, LrJ;-><init>(LrC;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 218
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 63
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V
    .registers 3
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->h:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfe;

    iput-object v0, p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lfe;

    .line 89
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->f:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaB;

    iput-object v0, p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LaaB;

    .line 95
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:La;

    iget-object v0, v0, La;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Landroid/app/NotificationManager;

    .line 101
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:LrC;

    iget-object v0, v0, LrC;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx;

    iput-object v0, p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Lrx;

    .line 107
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LME;

    .line 113
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Llf;

    .line 119
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:LrC;

    iget-object v0, v0, LrC;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LrR;

    iput-object v0, p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:LrR;

    .line 125
    return-void
.end method

.method public a(LrB;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LrC;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZv;

    iput-object v0, p1, LrB;->a:LZv;

    .line 79
    return-void
.end method

.method public a(LrQ;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LrC;->a:LYD;

    iget-object v0, v0, LYD;->a:LrC;

    invoke-virtual {v0, p1}, LrC;->a(LrB;)V

    .line 69
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 222
    return-void
.end method
