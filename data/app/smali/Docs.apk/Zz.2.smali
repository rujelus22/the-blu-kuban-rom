.class public LZz;
.super Ljava/lang/Object;
.source "BundleUtility.java"


# direct methods
.method public static a(Landroid/os/Bundle;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-static {p0, p1}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 34
    instance-of v1, v0, Ljava/lang/Integer;

    invoke-static {v1}, Lagu;->b(Z)V

    .line 35
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p1, p2}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 97
    if-eqz p0, :cond_10

    .line 98
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_10

    .line 102
    :try_start_8
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_f} :catch_11

    move-result-object p2

    .line 109
    :cond_10
    :goto_10
    return-object p2

    .line 104
    :catch_11
    move-exception v0

    goto :goto_10
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-static {p0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 74
    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-static {p0, p1}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 47
    instance-of v1, v0, Ljava/lang/String;

    invoke-static {v1}, Lagu;->b(Z)V

    .line 48
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)[I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-static {p0, p1}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 60
    instance-of v1, v0, [I

    invoke-static {v1}, Lagu;->b(Z)V

    .line 61
    check-cast v0, [I

    check-cast v0, [I

    return-object v0
.end method
