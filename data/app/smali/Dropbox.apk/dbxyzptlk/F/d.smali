.class public final Ldbxyzptlk/F/d;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Ldbxyzptlk/F/d;)Z
    .registers 2
    .parameter

    .prologue
    .line 719
    iget-boolean v0, p0, Ldbxyzptlk/F/d;->g:Z

    return v0
.end method

.method static synthetic b(Ldbxyzptlk/F/d;)Z
    .registers 2
    .parameter

    .prologue
    .line 719
    iget-boolean v0, p0, Ldbxyzptlk/F/d;->h:Z

    return v0
.end method

.method static synthetic c(Ldbxyzptlk/F/d;)Z
    .registers 2
    .parameter

    .prologue
    .line 719
    iget-boolean v0, p0, Ldbxyzptlk/F/d;->i:Z

    return v0
.end method

.method static synthetic d(Ldbxyzptlk/F/d;)F
    .registers 2
    .parameter

    .prologue
    .line 719
    iget v0, p0, Ldbxyzptlk/F/d;->c:F

    return v0
.end method

.method static synthetic e(Ldbxyzptlk/F/d;)F
    .registers 2
    .parameter

    .prologue
    .line 719
    iget v0, p0, Ldbxyzptlk/F/d;->a:F

    return v0
.end method

.method static synthetic f(Ldbxyzptlk/F/d;)F
    .registers 2
    .parameter

    .prologue
    .line 719
    iget v0, p0, Ldbxyzptlk/F/d;->b:F

    return v0
.end method

.method static synthetic g(Ldbxyzptlk/F/d;)F
    .registers 2
    .parameter

    .prologue
    .line 719
    iget v0, p0, Ldbxyzptlk/F/d;->d:F

    return v0
.end method

.method static synthetic h(Ldbxyzptlk/F/d;)F
    .registers 2
    .parameter

    .prologue
    .line 719
    iget v0, p0, Ldbxyzptlk/F/d;->e:F

    return v0
.end method

.method static synthetic i(Ldbxyzptlk/F/d;)F
    .registers 2
    .parameter

    .prologue
    .line 719
    iget v0, p0, Ldbxyzptlk/F/d;->f:F

    return v0
.end method


# virtual methods
.method public final a()F
    .registers 2

    .prologue
    .line 754
    iget v0, p0, Ldbxyzptlk/F/d;->a:F

    return v0
.end method

.method protected final a(FFFFFF)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, 0x3f80

    const/4 v2, 0x0

    .line 745
    iput p1, p0, Ldbxyzptlk/F/d;->a:F

    .line 746
    iput p2, p0, Ldbxyzptlk/F/d;->b:F

    .line 747
    cmpl-float v1, p3, v2

    if-nez v1, :cond_c

    move p3, v0

    :cond_c
    iput p3, p0, Ldbxyzptlk/F/d;->c:F

    .line 748
    cmpl-float v1, p4, v2

    if-nez v1, :cond_13

    move p4, v0

    :cond_13
    iput p4, p0, Ldbxyzptlk/F/d;->d:F

    .line 749
    cmpl-float v1, p5, v2

    if-nez v1, :cond_1e

    :goto_19
    iput v0, p0, Ldbxyzptlk/F/d;->e:F

    .line 750
    iput p6, p0, Ldbxyzptlk/F/d;->f:F

    .line 751
    return-void

    :cond_1e
    move v0, p5

    .line 749
    goto :goto_19
.end method

.method public final a(FFZFZFFZF)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, 0x3f80

    const/4 v2, 0x0

    .line 732
    iput p1, p0, Ldbxyzptlk/F/d;->a:F

    .line 733
    iput p2, p0, Ldbxyzptlk/F/d;->b:F

    .line 734
    iput-boolean p3, p0, Ldbxyzptlk/F/d;->g:Z

    .line 735
    cmpl-float v1, p4, v2

    if-nez v1, :cond_e

    move p4, v0

    :cond_e
    iput p4, p0, Ldbxyzptlk/F/d;->c:F

    .line 736
    iput-boolean p5, p0, Ldbxyzptlk/F/d;->h:Z

    .line 737
    cmpl-float v1, p6, v2

    if-nez v1, :cond_17

    move p6, v0

    :cond_17
    iput p6, p0, Ldbxyzptlk/F/d;->d:F

    .line 738
    cmpl-float v1, p7, v2

    if-nez v1, :cond_24

    :goto_1d
    iput v0, p0, Ldbxyzptlk/F/d;->e:F

    .line 739
    iput-boolean p8, p0, Ldbxyzptlk/F/d;->i:Z

    .line 740
    iput p9, p0, Ldbxyzptlk/F/d;->f:F

    .line 741
    return-void

    :cond_24
    move v0, p7

    .line 738
    goto :goto_1d
.end method

.method public final b()F
    .registers 2

    .prologue
    .line 758
    iget v0, p0, Ldbxyzptlk/F/d;->b:F

    return v0
.end method

.method public final c()F
    .registers 2

    .prologue
    .line 762
    iget-boolean v0, p0, Ldbxyzptlk/F/d;->g:Z

    if-nez v0, :cond_7

    const/high16 v0, 0x3f80

    :goto_6
    return v0

    :cond_7
    iget v0, p0, Ldbxyzptlk/F/d;->c:F

    goto :goto_6
.end method
