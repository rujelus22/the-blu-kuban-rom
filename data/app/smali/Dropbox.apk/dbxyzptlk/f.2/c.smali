.class public final Ldbxyzptlk/f/c;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:[B

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:[I


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method

.method private a(I)Ldbxyzptlk/f/i;
    .registers 13
    .parameter

    .prologue
    const/4 v3, 0x0

    const/16 v5, 0xf

    const/4 v9, 0x3

    const-wide/high16 v7, 0x4000

    const/4 v1, 0x1

    .line 162
    iget-object v0, p0, Ldbxyzptlk/f/c;->g:[I

    aget v4, v0, p1

    .line 163
    iget-object v0, p0, Ldbxyzptlk/f/c;->a:[B

    aget-byte v0, v0, v4

    .line 164
    and-int/lit16 v2, v0, 0xf0

    shr-int/lit8 v2, v2, 0x4

    .line 165
    and-int/lit8 v0, v0, 0xf

    .line 166
    packed-switch v2, :pswitch_data_572

    .line 402
    :pswitch_18
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown object type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 405
    :goto_30
    const/4 v0, 0x0

    :goto_31
    return-object v0

    .line 169
    :pswitch_32
    sparse-switch v0, :sswitch_data_592

    goto :goto_30

    .line 172
    :sswitch_36
    const/4 v0, 0x0

    goto :goto_31

    .line 176
    :sswitch_38
    new-instance v0, Ldbxyzptlk/f/h;

    invoke-direct {v0, v3}, Ldbxyzptlk/f/h;-><init>(Z)V

    goto :goto_31

    .line 180
    :sswitch_3e
    new-instance v0, Ldbxyzptlk/f/h;

    invoke-direct {v0, v1}, Ldbxyzptlk/f/h;-><init>(Z)V

    goto :goto_31

    .line 184
    :sswitch_44
    const/4 v0, 0x0

    goto :goto_31

    .line 191
    :pswitch_46
    int-to-double v0, v0

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v1, v0

    .line 192
    int-to-long v5, v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-gez v0, :cond_6a

    .line 193
    new-instance v0, Ldbxyzptlk/f/h;

    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x1

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v1, v4

    invoke-static {v2, v5, v1}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v1

    invoke-direct {v0, v1, v3}, Ldbxyzptlk/f/h;-><init>([BI)V

    goto :goto_31

    .line 195
    :cond_6a
    new-instance v0, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "To little heap space available! Wanted to read "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes, but only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " are available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :pswitch_9b
    int-to-double v2, v0

    invoke-static {v7, v8, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    .line 201
    int-to-long v5, v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-gez v0, :cond_c0

    .line 202
    new-instance v0, Ldbxyzptlk/f/h;

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x1

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    invoke-static {v3, v5, v2}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/f/h;-><init>([BI)V

    goto/16 :goto_31

    .line 204
    :cond_c0
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "To little heap space available! Wanted to read "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes, but only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " are available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :pswitch_f1
    if-eq v0, v9, :cond_111

    .line 210
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown date type :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Parsing anyway..."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 212
    :cond_111
    new-instance v0, Ldbxyzptlk/f/f;

    iget-object v1, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v2, v4, 0x1

    add-int/lit8 v3, v4, 0x9

    invoke-static {v1, v2, v3}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ldbxyzptlk/f/f;-><init>([B)V

    goto/16 :goto_31

    .line 218
    :pswitch_122
    if-ne v0, v5, :cond_164

    .line 219
    iget-object v0, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v2

    .line 220
    and-int/lit16 v2, v0, 0xf0

    div-int/lit8 v2, v2, 0xf

    .line 221
    if-eq v2, v1, :cond_148

    .line 222
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UNEXPECTED LENGTH-INT TYPE! "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 224
    :cond_148
    and-int/lit8 v0, v0, 0xf

    .line 225
    int-to-double v0, v0

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 226
    add-int/lit8 v1, v0, 0x2

    .line 227
    if-ge v0, v9, :cond_183

    .line 228
    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v3, v4, 0x2

    add-int/lit8 v5, v4, 0x2

    add-int/2addr v0, v5

    invoke-static {v2, v3, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v2

    long-to-int v0, v2

    .line 233
    :cond_164
    :goto_164
    int-to-long v2, v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v5

    cmp-long v2, v2, v5

    if-gez v2, :cond_198

    .line 234
    new-instance v2, Ldbxyzptlk/f/e;

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int v5, v4, v1

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    invoke-static {v3, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ldbxyzptlk/f/e;-><init>([B)V

    move-object v0, v2

    goto/16 :goto_31

    .line 230
    :cond_183
    new-instance v2, Ljava/math/BigInteger;

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v6, v4, 0x2

    add-int/2addr v0, v6

    invoke-static {v3, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    goto :goto_164

    .line 236
    :cond_198
    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "To little heap space available! Wanted to read "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes, but only "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " are available."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 243
    :pswitch_1c9
    if-ne v0, v5, :cond_20b

    .line 244
    iget-object v0, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v2

    .line 245
    and-int/lit16 v2, v0, 0xf0

    div-int/lit8 v2, v2, 0xf

    .line 246
    if-eq v2, v1, :cond_1ef

    .line 247
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UNEXPECTED LENGTH-INT TYPE! "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 249
    :cond_1ef
    and-int/lit8 v0, v0, 0xf

    .line 250
    int-to-double v0, v0

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 251
    add-int/lit8 v1, v0, 0x2

    .line 252
    if-ge v0, v9, :cond_22c

    .line 253
    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v3, v4, 0x2

    add-int/lit8 v5, v4, 0x2

    add-int/2addr v0, v5

    invoke-static {v2, v3, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v2

    long-to-int v0, v2

    .line 258
    :cond_20b
    :goto_20b
    int-to-long v2, v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v5

    cmp-long v2, v2, v5

    if-gez v2, :cond_241

    .line 259
    new-instance v2, Ldbxyzptlk/f/k;

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int v5, v4, v1

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    invoke-static {v3, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    const-string v1, "ASCII"

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/f/k;-><init>([BLjava/lang/String;)V

    move-object v0, v2

    goto/16 :goto_31

    .line 255
    :cond_22c
    new-instance v2, Ljava/math/BigInteger;

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v6, v4, 0x2

    add-int/2addr v0, v6

    invoke-static {v3, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    goto :goto_20b

    .line 261
    :cond_241
    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "To little heap space available! Wanted to read "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes, but only "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " are available."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :pswitch_272
    if-ne v0, v5, :cond_2b4

    .line 269
    iget-object v0, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v2

    .line 270
    and-int/lit16 v2, v0, 0xf0

    div-int/lit8 v2, v2, 0xf

    .line 271
    if-eq v2, v1, :cond_298

    .line 272
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UNEXPECTED LENGTH-INT TYPE! "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 274
    :cond_298
    and-int/lit8 v0, v0, 0xf

    .line 275
    int-to-double v0, v0

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 276
    add-int/lit8 v1, v0, 0x2

    .line 277
    if-ge v0, v9, :cond_2d6

    .line 278
    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v3, v4, 0x2

    add-int/lit8 v5, v4, 0x2

    add-int/2addr v0, v5

    invoke-static {v2, v3, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v2

    long-to-int v0, v2

    .line 284
    :cond_2b4
    :goto_2b4
    mul-int/lit8 v2, v0, 0x2

    .line 285
    int-to-long v5, v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    cmp-long v0, v5, v7

    if-gez v0, :cond_2eb

    .line 286
    new-instance v0, Ldbxyzptlk/f/k;

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int v5, v4, v1

    add-int/2addr v1, v4

    add-int/2addr v1, v2

    invoke-static {v3, v5, v1}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v1

    const-string v2, "UTF-16BE"

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/f/k;-><init>([BLjava/lang/String;)V

    goto/16 :goto_31

    .line 280
    :cond_2d6
    new-instance v2, Ljava/math/BigInteger;

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v6, v4, 0x2

    add-int/2addr v0, v6

    invoke-static {v3, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    goto :goto_2b4

    .line 288
    :cond_2eb
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "To little heap space available! Wanted to read "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes, but only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " are available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293
    :pswitch_31c
    add-int/lit8 v1, v0, 0x1

    .line 294
    int-to-long v2, v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v5

    cmp-long v0, v2, v5

    if-gez v0, :cond_341

    .line 295
    new-instance v0, Ldbxyzptlk/f/m;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x1

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v1, v4

    invoke-static {v3, v5, v1}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/f/m;-><init>(Ljava/lang/String;[B)V

    goto/16 :goto_31

    .line 297
    :cond_341
    new-instance v0, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "To little heap space available! Wanted to read "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes, but only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " are available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :pswitch_372
    if-ne v0, v5, :cond_3b4

    .line 305
    iget-object v0, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v2

    .line 306
    and-int/lit16 v2, v0, 0xf0

    div-int/lit8 v2, v2, 0xf

    .line 307
    if-eq v2, v1, :cond_398

    .line 308
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UNEXPECTED LENGTH-INT TYPE! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 310
    :cond_398
    and-int/lit8 v0, v0, 0xf

    .line 311
    int-to-double v0, v0

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 312
    add-int/lit8 v1, v0, 0x2

    .line 313
    if-ge v0, v9, :cond_3cc

    .line 314
    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v6, v4, 0x2

    add-int/2addr v0, v6

    invoke-static {v2, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v5

    long-to-int v0, v5

    .line 319
    :cond_3b4
    :goto_3b4
    iget v2, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v2, v0

    int-to-long v5, v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    cmp-long v2, v5, v7

    if-lez v2, :cond_3e1

    .line 320
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "To little heap space available!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_3cc
    new-instance v2, Ljava/math/BigInteger;

    iget-object v5, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v6, v4, 0x2

    add-int/lit8 v7, v4, 0x2

    add-int/2addr v0, v7

    invoke-static {v5, v6, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    goto :goto_3b4

    .line 322
    :cond_3e1
    new-instance v2, Ldbxyzptlk/f/d;

    invoke-direct {v2, v0}, Ldbxyzptlk/f/d;-><init>(I)V

    .line 323
    :goto_3e6
    if-ge v3, v0, :cond_40b

    .line 324
    iget-object v5, p0, Ldbxyzptlk/f/c;->a:[B

    add-int v6, v4, v1

    iget v7, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v7, v3

    add-int/2addr v6, v7

    add-int v7, v4, v1

    add-int/lit8 v8, v3, 0x1

    iget v9, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v8, v9

    add-int/2addr v7, v8

    invoke-static {v5, v6, v7}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v5

    invoke-static {v5}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v5

    long-to-int v5, v5

    .line 327
    invoke-direct {p0, v5}, Ldbxyzptlk/f/c;->a(I)Ldbxyzptlk/f/i;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Ldbxyzptlk/f/d;->a(ILdbxyzptlk/f/i;)V

    .line 323
    add-int/lit8 v3, v3, 0x1

    goto :goto_3e6

    :cond_40b
    move-object v0, v2

    .line 329
    goto/16 :goto_31

    .line 336
    :pswitch_40e
    if-ne v0, v5, :cond_450

    .line 337
    iget-object v0, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v2

    .line 338
    and-int/lit16 v2, v0, 0xf0

    div-int/lit8 v2, v2, 0xf

    .line 339
    if-eq v2, v1, :cond_434

    .line 340
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UNEXPECTED LENGTH-INT TYPE! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 342
    :cond_434
    and-int/lit8 v0, v0, 0xf

    .line 343
    int-to-double v0, v0

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 344
    add-int/lit8 v1, v0, 0x2

    .line 345
    if-ge v0, v9, :cond_468

    .line 346
    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v6, v4, 0x2

    add-int/2addr v0, v6

    invoke-static {v2, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v5

    long-to-int v0, v5

    .line 351
    :cond_450
    :goto_450
    iget v2, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v2, v0

    int-to-long v5, v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    cmp-long v2, v5, v7

    if-lez v2, :cond_47d

    .line 352
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "To little heap space available!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_468
    new-instance v2, Ljava/math/BigInteger;

    iget-object v5, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v6, v4, 0x2

    add-int/lit8 v7, v4, 0x2

    add-int/2addr v0, v7

    invoke-static {v5, v6, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    goto :goto_450

    .line 354
    :cond_47d
    new-instance v2, Ldbxyzptlk/f/j;

    invoke-direct {v2}, Ldbxyzptlk/f/j;-><init>()V

    .line 355
    :goto_482
    if-ge v3, v0, :cond_4a7

    .line 356
    iget-object v5, p0, Ldbxyzptlk/f/c;->a:[B

    add-int v6, v4, v1

    iget v7, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v7, v3

    add-int/2addr v6, v7

    add-int v7, v4, v1

    add-int/lit8 v8, v3, 0x1

    iget v9, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v8, v9

    add-int/2addr v7, v8

    invoke-static {v5, v6, v7}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v5

    invoke-static {v5}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v5

    long-to-int v5, v5

    .line 359
    invoke-direct {p0, v5}, Ldbxyzptlk/f/c;->a(I)Ldbxyzptlk/f/i;

    move-result-object v5

    invoke-virtual {v2, v5}, Ldbxyzptlk/f/j;->a(Ldbxyzptlk/f/i;)V

    .line 355
    add-int/lit8 v3, v3, 0x1

    goto :goto_482

    :cond_4a7
    move-object v0, v2

    .line 361
    goto/16 :goto_31

    .line 367
    :pswitch_4aa
    if-ne v0, v5, :cond_4ec

    .line 368
    iget-object v0, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v2

    .line 369
    and-int/lit16 v2, v0, 0xf0

    div-int/lit8 v2, v2, 0xf

    .line 370
    if-eq v2, v1, :cond_4d0

    .line 371
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UNEXPECTED LENGTH-INT TYPE! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 373
    :cond_4d0
    and-int/lit8 v0, v0, 0xf

    .line 374
    int-to-double v0, v0

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 375
    add-int/lit8 v1, v0, 0x2

    .line 376
    if-ge v0, v9, :cond_506

    .line 377
    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v6, v4, 0x2

    add-int/2addr v0, v6

    invoke-static {v2, v5, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v5

    long-to-int v0, v5

    .line 382
    :cond_4ec
    :goto_4ec
    mul-int/lit8 v2, v0, 0x2

    iget v5, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v2, v5

    int-to-long v5, v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v7

    cmp-long v2, v5, v7

    if-lez v2, :cond_51b

    .line 383
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "To little heap space available!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 379
    :cond_506
    new-instance v2, Ljava/math/BigInteger;

    iget-object v5, p0, Ldbxyzptlk/f/c;->a:[B

    add-int/lit8 v6, v4, 0x2

    add-int/lit8 v7, v4, 0x2

    add-int/2addr v0, v7

    invoke-static {v5, v6, v0}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    goto :goto_4ec

    .line 386
    :cond_51b
    new-instance v2, Ldbxyzptlk/f/g;

    invoke-direct {v2}, Ldbxyzptlk/f/g;-><init>()V

    .line 387
    :goto_520
    if-ge v3, v0, :cond_56e

    .line 388
    iget-object v5, p0, Ldbxyzptlk/f/c;->a:[B

    add-int v6, v4, v1

    iget v7, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v7, v3

    add-int/2addr v6, v7

    add-int v7, v4, v1

    add-int/lit8 v8, v3, 0x1

    iget v9, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v8, v9

    add-int/2addr v7, v8

    invoke-static {v5, v6, v7}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v5

    invoke-static {v5}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v5

    long-to-int v5, v5

    .line 391
    iget-object v6, p0, Ldbxyzptlk/f/c;->a:[B

    add-int v7, v4, v1

    iget v8, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v8, v0

    add-int/2addr v7, v8

    iget v8, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v8, v3

    add-int/2addr v7, v8

    add-int v8, v4, v1

    iget v9, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v9, v0

    add-int/2addr v8, v9

    add-int/lit8 v9, v3, 0x1

    iget v10, p0, Ldbxyzptlk/f/c;->c:I

    mul-int/2addr v9, v10

    add-int/2addr v8, v9

    invoke-static {v6, v7, v8}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v6

    invoke-static {v6}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v6

    long-to-int v6, v6

    .line 394
    invoke-direct {p0, v5}, Ldbxyzptlk/f/c;->a(I)Ldbxyzptlk/f/i;

    move-result-object v5

    .line 395
    invoke-direct {p0, v6}, Ldbxyzptlk/f/c;->a(I)Ldbxyzptlk/f/i;

    move-result-object v6

    .line 397
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v6}, Ldbxyzptlk/f/g;->a(Ljava/lang/String;Ldbxyzptlk/f/i;)V

    .line 387
    add-int/lit8 v3, v3, 0x1

    goto :goto_520

    :cond_56e
    move-object v0, v2

    .line 399
    goto/16 :goto_31

    .line 166
    nop

    :pswitch_data_572
    .packed-switch 0x0
        :pswitch_32
        :pswitch_46
        :pswitch_9b
        :pswitch_f1
        :pswitch_122
        :pswitch_1c9
        :pswitch_272
        :pswitch_18
        :pswitch_31c
        :pswitch_18
        :pswitch_372
        :pswitch_18
        :pswitch_40e
        :pswitch_4aa
    .end packed-switch

    .line 169
    :sswitch_data_592
    .sparse-switch
        0x0 -> :sswitch_36
        0x8 -> :sswitch_38
        0x9 -> :sswitch_3e
        0xf -> :sswitch_44
    .end sparse-switch
.end method

.method public static a(Ljava/io/File;)Ldbxyzptlk/f/i;
    .registers 5
    .parameter

    .prologue
    .line 146
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_45

    .line 147
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "To little heap space available! Wanted to read "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes, but only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " are available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_45
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v0}, Ldbxyzptlk/f/c;->a(Ljava/io/InputStream;)Ldbxyzptlk/f/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;)Ldbxyzptlk/f/i;
    .registers 2
    .parameter

    .prologue
    .line 134
    const v0, 0x7fffffff

    invoke-static {p0, v0}, Ldbxyzptlk/f/l;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 135
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 136
    invoke-static {v0}, Ldbxyzptlk/f/c;->a([B)Ldbxyzptlk/f/i;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Ldbxyzptlk/f/i;
    .registers 2
    .parameter

    .prologue
    .line 72
    new-instance v0, Ldbxyzptlk/f/c;

    invoke-direct {v0}, Ldbxyzptlk/f/c;-><init>()V

    .line 73
    invoke-direct {v0, p0}, Ldbxyzptlk/f/c;->e([B)Ldbxyzptlk/f/i;

    move-result-object v0

    return-object v0
.end method

.method public static a([BII)[B
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 460
    sub-int v0, p2, p1

    .line 461
    if-gez v0, :cond_33

    .line 462
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startIndex ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > endIndex ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464
    :cond_33
    new-array v1, v0, [B

    .line 465
    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 466
    return-object v1
.end method

.method public static final b([B)J
    .registers 7
    .parameter

    .prologue
    .line 414
    const-wide/16 v1, 0x0

    .line 415
    array-length v3, p0

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v3, :cond_12

    aget-byte v4, p0, v0

    .line 416
    const/16 v5, 0x8

    shl-long/2addr v1, v5

    .line 417
    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    or-long/2addr v1, v4

    .line 415
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 419
    :cond_12
    const-wide v3, 0xffffffffL

    and-long v0, v1, v3

    .line 420
    return-wide v0
.end method

.method public static final c([B)J
    .registers 7
    .parameter

    .prologue
    .line 429
    const-wide/16 v1, 0x0

    .line 430
    array-length v3, p0

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v3, :cond_12

    aget-byte v4, p0, v0

    .line 431
    const/16 v5, 0x8

    shl-long/2addr v1, v5

    .line 432
    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    or-long/2addr v1, v4

    .line 430
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 434
    :cond_12
    return-wide v1
.end method

.method public static final d([B)D
    .registers 4
    .parameter

    .prologue
    .line 443
    array-length v0, p0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_e

    .line 444
    invoke-static {p0}, Ldbxyzptlk/f/c;->c([B)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 446
    :goto_d
    return-wide v0

    .line 445
    :cond_e
    array-length v0, p0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1d

    .line 446
    invoke-static {p0}, Ldbxyzptlk/f/c;->c([B)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    float-to-double v0, v0

    goto :goto_d

    .line 448
    :cond_1d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad byte array length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private e([B)Ldbxyzptlk/f/i;
    .registers 10
    .parameter

    .prologue
    const/16 v7, 0x18

    const/16 v6, 0x10

    const/4 v5, 0x7

    const/4 v0, 0x0

    const/16 v4, 0x8

    .line 83
    iput-object p1, p0, Ldbxyzptlk/f/c;->a:[B

    .line 84
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    invoke-static {v2, v0, v4}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 85
    const-string v2, "bplist"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_36

    .line 86
    new-instance v0, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The given data is no binary property list. Wrong magic bytes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_36
    iget-object v1, p0, Ldbxyzptlk/f/c;->a:[B

    iget-object v2, p0, Ldbxyzptlk/f/c;->a:[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x20

    iget-object v3, p0, Ldbxyzptlk/f/c;->a:[B

    array-length v3, v3

    invoke-static {v1, v2, v3}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v1

    .line 99
    const/4 v2, 0x6

    invoke-static {v1, v2, v5}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v2

    invoke-static {v2}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Ldbxyzptlk/f/c;->b:I

    .line 101
    invoke-static {v1, v5, v4}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v2

    invoke-static {v2}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Ldbxyzptlk/f/c;->c:I

    .line 103
    invoke-static {v1, v4, v6}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v2

    invoke-static {v2}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Ldbxyzptlk/f/c;->d:I

    .line 105
    invoke-static {v1, v6, v7}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v2

    invoke-static {v2}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Ldbxyzptlk/f/c;->e:I

    .line 107
    const/16 v2, 0x20

    invoke-static {v1, v7, v2}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, p0, Ldbxyzptlk/f/c;->f:I

    .line 113
    iget v1, p0, Ldbxyzptlk/f/c;->d:I

    new-array v1, v1, [I

    iput-object v1, p0, Ldbxyzptlk/f/c;->g:[I

    .line 115
    :goto_84
    iget v1, p0, Ldbxyzptlk/f/c;->d:I

    if-ge v0, v1, :cond_a8

    .line 116
    iget-object v1, p0, Ldbxyzptlk/f/c;->a:[B

    iget v2, p0, Ldbxyzptlk/f/c;->f:I

    iget v3, p0, Ldbxyzptlk/f/c;->b:I

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    iget v3, p0, Ldbxyzptlk/f/c;->f:I

    add-int/lit8 v4, v0, 0x1

    iget v5, p0, Ldbxyzptlk/f/c;->b:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ldbxyzptlk/f/c;->a([BII)[B

    move-result-object v1

    .line 117
    iget-object v2, p0, Ldbxyzptlk/f/c;->g:[I

    invoke-static {v1}, Ldbxyzptlk/f/c;->b([B)J

    move-result-wide v3

    long-to-int v1, v3

    aput v1, v2, v0

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_84

    .line 123
    :cond_a8
    iget v0, p0, Ldbxyzptlk/f/c;->e:I

    invoke-direct {p0, v0}, Ldbxyzptlk/f/c;->a(I)Ldbxyzptlk/f/i;

    move-result-object v0

    return-object v0
.end method
