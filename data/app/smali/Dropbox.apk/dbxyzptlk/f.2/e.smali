.class public final Ldbxyzptlk/f/e;
.super Ldbxyzptlk/f/i;
.source "panda.py"


# instance fields
.field private b:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 53
    invoke-direct {p0}, Ldbxyzptlk/f/i;-><init>()V

    .line 56
    const-string v0, "\\s+"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-static {v0}, Ldbxyzptlk/f/b;->a(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/f/e;->b:[B

    .line 58
    return-void
.end method

.method public constructor <init>([B)V
    .registers 2
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ldbxyzptlk/f/i;-><init>()V

    .line 45
    iput-object p1, p0, Ldbxyzptlk/f/e;->b:[B

    .line 46
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 118
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    check-cast p1, Ldbxyzptlk/f/e;

    iget-object v0, p1, Ldbxyzptlk/f/e;->b:[B

    iget-object v1, p0, Ldbxyzptlk/f/e;->b:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 123
    .line 124
    iget-object v0, p0, Ldbxyzptlk/f/e;->b:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x14f

    .line 125
    return v0
.end method
