.class public final Ldbxyzptlk/f/l;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public static a(Ljava/io/File;)Ldbxyzptlk/f/i;
    .registers 6
    .parameter

    .prologue
    const/16 v4, 0x8

    .line 74
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 75
    new-instance v1, Ljava/lang/String;

    invoke-static {v0, v4}, Ldbxyzptlk/f/l;->a(Ljava/io/InputStream;I)[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([BII)V

    .line 76
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 77
    const-string v0, "bplist"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 78
    invoke-static {p0}, Ldbxyzptlk/f/c;->a(Ljava/io/File;)Ldbxyzptlk/f/i;

    move-result-object v0

    .line 82
    :goto_20
    return-object v0

    .line 79
    :cond_21
    const-string v0, "<?xml"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 80
    invoke-static {p0}, Ldbxyzptlk/f/n;->a(Ljava/io/File;)Ldbxyzptlk/f/i;

    move-result-object v0

    goto :goto_20

    .line 81
    :cond_2e
    const-string v0, "("

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3e

    const-string v0, "{"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 82
    :cond_3e
    invoke-static {p0}, Ldbxyzptlk/f/a;->a(Ljava/io/File;)Ldbxyzptlk/f/i;

    move-result-object v0

    goto :goto_20

    .line 84
    :cond_43
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The given data is not a valid property list. For supported format see http://code.google.com/p/plist"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected static a(Ljava/io/InputStream;I)[B
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 57
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 58
    :goto_5
    if-lez p1, :cond_e

    .line 59
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 60
    const/4 v2, -0x1

    if-ne v1, v2, :cond_13

    .line 64
    :cond_e
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 61
    :cond_13
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 62
    add-int/lit8 p1, p1, -0x1

    .line 63
    goto :goto_5
.end method
