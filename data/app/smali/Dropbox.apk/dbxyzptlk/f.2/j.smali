.class public final Ldbxyzptlk/f/j;
.super Ldbxyzptlk/f/i;
.source "panda.py"


# instance fields
.field private b:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 46
    invoke-direct {p0}, Ldbxyzptlk/f/i;-><init>()V

    .line 47
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    .line 48
    return-void
.end method


# virtual methods
.method public final a(Ldbxyzptlk/f/i;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 163
    if-nez p1, :cond_4

    .line 173
    :cond_3
    :goto_3
    return v0

    .line 166
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_3

    .line 169
    check-cast p1, Ldbxyzptlk/f/j;

    .line 170
    iget-object v1, p0, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    iget-object v2, p1, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    if-eq v1, v2, :cond_24

    iget-object v1, p0, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    iget-object v2, p1, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 173
    :cond_24
    const/4 v0, 0x1

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 156
    .line 157
    iget-object v0, p0, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    if-eqz v0, :cond_d

    iget-object v0, p0, Ldbxyzptlk/f/j;->b:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_a
    add-int/lit16 v0, v0, 0xcb

    .line 158
    return v0

    .line 157
    :cond_d
    const/4 v0, 0x0

    goto :goto_a
.end method
