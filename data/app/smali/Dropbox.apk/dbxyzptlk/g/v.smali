.class public final Ldbxyzptlk/g/v;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ldbxyzptlk/g/x;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/g/x;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p2, p0, Ldbxyzptlk/g/v;->a:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Ldbxyzptlk/g/v;->b:Ldbxyzptlk/g/x;

    .line 29
    return-void
.end method

.method private a(Lcom/dropbox/android/taskqueue/m;)Ldbxyzptlk/g/y;
    .registers 4
    .parameter

    .prologue
    .line 37
    sget-object v0, Ldbxyzptlk/g/w;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/m;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_14

    .line 43
    sget-object v0, Ldbxyzptlk/g/y;->c:Ldbxyzptlk/g/y;

    :goto_d
    return-object v0

    .line 39
    :pswitch_e
    sget-object v0, Ldbxyzptlk/g/y;->b:Ldbxyzptlk/g/y;

    goto :goto_d

    .line 41
    :pswitch_11
    sget-object v0, Ldbxyzptlk/g/y;->a:Ldbxyzptlk/g/y;

    goto :goto_d

    .line 37
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_e
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/taskqueue/m;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/g/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/x;->a(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 10
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/v;->a(Landroid/content/Context;[Ljava/lang/Void;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 49
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .line 50
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/m;->b()Lcom/dropbox/android/taskqueue/n;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/n;->a:Lcom/dropbox/android/taskqueue/n;

    if-ne v0, v1, :cond_12

    .line 51
    iget-object v0, p0, Ldbxyzptlk/g/v;->b:Ldbxyzptlk/g/x;

    iget-object v1, p0, Ldbxyzptlk/g/v;->a:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Ldbxyzptlk/g/x;->a(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V

    .line 55
    :goto_11
    return-void

    .line 53
    :cond_12
    iget-object v0, p0, Ldbxyzptlk/g/v;->b:Ldbxyzptlk/g/x;

    invoke-direct {p0, p2}, Ldbxyzptlk/g/v;->a(Lcom/dropbox/android/taskqueue/m;)Ldbxyzptlk/g/y;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ldbxyzptlk/g/x;->a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/g/y;)V

    goto :goto_11
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 60
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 10
    check-cast p2, Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/v;->a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V

    return-void
.end method
