.class final Ldbxyzptlk/g/E;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ldbxyzptlk/g/B;


# direct methods
.method private constructor <init>(Ldbxyzptlk/g/B;)V
    .registers 2
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ldbxyzptlk/g/B;Ldbxyzptlk/g/C;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1}, Ldbxyzptlk/g/E;-><init>(Ldbxyzptlk/g/B;)V

    return-void
.end method

.method private a()V
    .registers 23

    .prologue
    .line 133
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v10

    .line 134
    invoke-virtual {v10}, Ldbxyzptlk/l/m;->x()Ljava/lang/String;

    move-result-object v8

    .line 136
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    iget-object v11, v1, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    .line 138
    const-wide/16 v6, 0x0

    .line 139
    const-wide/16 v4, 0x0

    .line 140
    const-wide/16 v2, 0x0

    .line 142
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    .line 143
    const/4 v1, 0x1

    move-object v9, v8

    move-wide/from16 v18, v6

    move-wide/from16 v7, v18

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    .line 144
    :goto_22
    if-eqz v1, :cond_8d

    .line 146
    invoke-virtual {v11, v9}, Ldbxyzptlk/r/i;->j(Ljava/lang/String;)Ldbxyzptlk/n/g;

    move-result-object v14

    .line 147
    const-wide/16 v15, 0x1

    add-long v3, v2, v15

    .line 148
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 149
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 150
    iget-object v1, v14, Ldbxyzptlk/n/g;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_3c
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_62

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/n/e;

    .line 151
    iget-object v2, v1, Ldbxyzptlk/n/e;->b:Ljava/lang/Object;

    if-eqz v2, :cond_5c

    iget-object v2, v1, Ldbxyzptlk/n/e;->b:Ljava/lang/Object;

    check-cast v2, Ldbxyzptlk/r/m;

    iget-object v2, v2, Ldbxyzptlk/r/m;->a:Ldbxyzptlk/n/k;

    if-eqz v2, :cond_5c

    .line 152
    iget-object v1, v1, Ldbxyzptlk/n/e;->b:Ljava/lang/Object;

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3c

    .line 154
    :cond_5c
    iget-object v1, v1, Ldbxyzptlk/n/e;->a:Ljava/lang/String;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3c

    .line 157
    :cond_62
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-long v1, v1

    add-long/2addr v7, v1

    .line 158
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-long v1, v1

    add-long/2addr v5, v1

    .line 163
    if-nez v9, :cond_8b

    const/4 v1, 0x1

    .line 164
    :goto_71
    move-object/from16 v0, p0

    iget-object v2, v0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-static {v2}, Ldbxyzptlk/g/B;->c(Ldbxyzptlk/g/B;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v2, v0, v15, v1}, Lcom/dropbox/android/provider/CameraUploadsProvider;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    .line 166
    iget-boolean v1, v14, Ldbxyzptlk/n/g;->d:Z

    .line 167
    iget-object v2, v14, Ldbxyzptlk/n/g;->b:Ljava/lang/String;

    .line 170
    invoke-virtual {v10, v2}, Ldbxyzptlk/l/m;->c(Ljava/lang/String;)V

    move-object v9, v2

    move-wide/from16 v18, v3

    move-wide/from16 v2, v18

    .line 171
    goto :goto_22

    .line 163
    :cond_8b
    const/4 v1, 0x0

    goto :goto_71

    .line 173
    :cond_8d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 174
    invoke-static {}, Lcom/dropbox/android/util/i;->H()Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v4, "added"

    invoke-virtual {v1, v4, v7, v8}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v4, "removed"

    invoke-virtual {v1, v4, v5, v6}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v4, "pages"

    invoke-virtual {v1, v4, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "dur"

    sub-long v3, v9, v12

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    .line 176
    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 112
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    sget-object v1, Ldbxyzptlk/g/F;->a:Ldbxyzptlk/g/F;

    invoke-static {v0, v1}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Ldbxyzptlk/g/F;)V

    .line 114
    :try_start_7
    invoke-direct {p0}, Ldbxyzptlk/g/E;->a()V
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_77
    .catch Ldbxyzptlk/o/a; {:try_start_7 .. :try_end_a} :catch_38

    .line 120
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-static {v0}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;)Ldbxyzptlk/g/F;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/g/F;->c:Ldbxyzptlk/g/F;

    if-eq v0, v1, :cond_1b

    .line 121
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    sget-object v1, Ldbxyzptlk/g/F;->b:Ldbxyzptlk/g/F;

    invoke-static {v0, v1}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Ldbxyzptlk/g/F;)V

    .line 123
    :cond_1b
    invoke-static {}, Ldbxyzptlk/g/B;->c()Ldbxyzptlk/g/B;

    move-result-object v1

    monitor-enter v1

    .line 124
    :try_start_20
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Z)Z

    .line 125
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-static {v0}, Ldbxyzptlk/g/B;->b(Ldbxyzptlk/g/B;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 126
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-virtual {v0}, Ldbxyzptlk/g/B;->a()V

    .line 128
    :cond_33
    monitor-exit v1

    .line 130
    :goto_34
    return-void

    .line 128
    :catchall_35
    move-exception v0

    monitor-exit v1
    :try_end_37
    .catchall {:try_start_20 .. :try_end_37} :catchall_35

    throw v0

    .line 115
    :catch_38
    move-exception v0

    .line 116
    :try_start_39
    invoke-static {}, Ldbxyzptlk/g/B;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UpdateTask error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 117
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    sget-object v1, Ldbxyzptlk/g/F;->c:Ldbxyzptlk/g/F;

    invoke-static {v0, v1}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Ldbxyzptlk/g/F;)V
    :try_end_49
    .catchall {:try_start_39 .. :try_end_49} :catchall_77

    .line 120
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-static {v0}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;)Ldbxyzptlk/g/F;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/g/F;->c:Ldbxyzptlk/g/F;

    if-eq v0, v1, :cond_5a

    .line 121
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    sget-object v1, Ldbxyzptlk/g/F;->b:Ldbxyzptlk/g/F;

    invoke-static {v0, v1}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Ldbxyzptlk/g/F;)V

    .line 123
    :cond_5a
    invoke-static {}, Ldbxyzptlk/g/B;->c()Ldbxyzptlk/g/B;

    move-result-object v1

    monitor-enter v1

    .line 124
    :try_start_5f
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Z)Z

    .line 125
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-static {v0}, Ldbxyzptlk/g/B;->b(Ldbxyzptlk/g/B;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 126
    iget-object v0, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-virtual {v0}, Ldbxyzptlk/g/B;->a()V

    .line 128
    :cond_72
    monitor-exit v1

    goto :goto_34

    :catchall_74
    move-exception v0

    monitor-exit v1
    :try_end_76
    .catchall {:try_start_5f .. :try_end_76} :catchall_74

    throw v0

    .line 120
    :catchall_77
    move-exception v0

    iget-object v1, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-static {v1}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;)Ldbxyzptlk/g/F;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/g/F;->c:Ldbxyzptlk/g/F;

    if-eq v1, v2, :cond_89

    .line 121
    iget-object v1, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    sget-object v2, Ldbxyzptlk/g/F;->b:Ldbxyzptlk/g/F;

    invoke-static {v1, v2}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Ldbxyzptlk/g/F;)V

    .line 123
    :cond_89
    invoke-static {}, Ldbxyzptlk/g/B;->c()Ldbxyzptlk/g/B;

    move-result-object v1

    monitor-enter v1

    .line 124
    :try_start_8e
    iget-object v2, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/B;Z)Z

    .line 125
    iget-object v2, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-static {v2}, Ldbxyzptlk/g/B;->b(Ldbxyzptlk/g/B;)Z

    move-result v2

    if-eqz v2, :cond_a1

    .line 126
    iget-object v2, p0, Ldbxyzptlk/g/E;->a:Ldbxyzptlk/g/B;

    invoke-virtual {v2}, Ldbxyzptlk/g/B;->a()V

    .line 128
    :cond_a1
    monitor-exit v1
    :try_end_a2
    .catchall {:try_start_8e .. :try_end_a2} :catchall_a3

    throw v0

    :catchall_a3
    move-exception v0

    :try_start_a4
    monitor-exit v1
    :try_end_a5
    .catchall {:try_start_a4 .. :try_end_a5} :catchall_a3

    throw v0
.end method
