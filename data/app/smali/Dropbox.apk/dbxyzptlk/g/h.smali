.class public Ldbxyzptlk/g/h;
.super Ldbxyzptlk/t/a;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/l;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field protected volatile a:Z

.field b:Ljava/lang/ref/WeakReference;

.field protected c:Lcom/dropbox/android/taskqueue/m;

.field private e:Z

.field private final f:Lcom/dropbox/android/taskqueue/DownloadTask;

.field private g:Z

.field private h:J

.field private i:Ldbxyzptlk/g/i;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 24
    const-class v0, Ldbxyzptlk/g/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/h;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 26
    iput-boolean v1, p0, Ldbxyzptlk/g/h;->a:Z

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/g/h;->e:Z

    .line 38
    iput-boolean v1, p0, Ldbxyzptlk/g/h;->g:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/g/h;->c:Lcom/dropbox/android/taskqueue/m;

    .line 51
    new-instance v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-direct {v0, p1, p2}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    iput-object v0, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    .line 52
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/io/File;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/taskqueue/DownloadTask;->a(Lcom/dropbox/android/taskqueue/l;)V

    .line 89
    iget-object v0, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->c()Lcom/dropbox/android/taskqueue/m;

    .line 90
    iget-object v0, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->h()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 23
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/h;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/io/File;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 114
    iget-boolean v0, p0, Ldbxyzptlk/g/h;->a:Z

    if-eqz v0, :cond_5

    .line 147
    :cond_4
    :goto_4
    return-void

    .line 118
    :cond_5
    if-eqz p2, :cond_3b

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 120
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->m()Lcom/dropbox/android/util/ah;

    move-result-object v0

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/ah;->b(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Ldbxyzptlk/g/h;->i:Ldbxyzptlk/g/i;

    if-eqz v0, :cond_25

    .line 124
    iget-object v0, p0, Ldbxyzptlk/g/h;->i:Ldbxyzptlk/g/i;

    invoke-interface {v0, p2, p1}, Ldbxyzptlk/g/i;->a(Ljava/io/File;Landroid/content/Context;)V

    .line 127
    :cond_25
    iget-object v0, p0, Ldbxyzptlk/g/h;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Ldbxyzptlk/g/h;->e:Z

    if-eqz v0, :cond_4

    .line 128
    iget-object v0, p0, Ldbxyzptlk/g/h;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/I;

    .line 129
    if-eqz v0, :cond_4

    .line 130
    invoke-virtual {v0}, Lcom/dropbox/android/widget/I;->dismiss()V

    goto :goto_4

    .line 134
    :cond_3b
    iget-object v0, p0, Ldbxyzptlk/g/h;->c:Lcom/dropbox/android/taskqueue/m;

    invoke-static {v0, p1}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/taskqueue/m;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_4b

    .line 136
    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 140
    :cond_4b
    iget-object v0, p0, Ldbxyzptlk/g/h;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    .line 141
    iget-object v0, p0, Ldbxyzptlk/g/h;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/I;

    .line 142
    if-eqz v0, :cond_4

    .line 143
    invoke-virtual {v0}, Lcom/dropbox/android/widget/I;->dismiss()V

    goto :goto_4
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->f()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_3e

    .line 74
    sget-object v1, Ldbxyzptlk/g/h;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Downloading file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " exception: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :goto_34
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, p2, v1}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 79
    return-void

    .line 76
    :cond_3e
    sget-object v0, Ldbxyzptlk/g/h;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Downloading file, exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_34
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 23
    check-cast p2, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/h;->a(Landroid/content/Context;Ljava/io/File;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 151
    iput-boolean v0, p0, Ldbxyzptlk/g/h;->g:Z

    .line 152
    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/g/h;->publishProgress([Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;JJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 169
    iput-boolean v0, p0, Ldbxyzptlk/g/h;->g:Z

    .line 170
    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/g/h;->publishProgress([Ljava/lang/Object;)V

    .line 171
    iput-wide p5, p0, Ldbxyzptlk/g/h;->h:J

    .line 172
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/k;Lcom/dropbox/android/taskqueue/m;Landroid/net/Uri;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 157
    iput-boolean v0, p0, Ldbxyzptlk/g/h;->g:Z

    .line 158
    iput-object p2, p0, Ldbxyzptlk/g/h;->c:Lcom/dropbox/android/taskqueue/m;

    .line 159
    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/g/h;->publishProgress([Ljava/lang/Object;)V

    .line 160
    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/I;)V
    .registers 3
    .parameter

    .prologue
    .line 59
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ldbxyzptlk/g/h;->b:Ljava/lang/ref/WeakReference;

    .line 60
    return-void
.end method

.method public final a(Ldbxyzptlk/g/i;)V
    .registers 2
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Ldbxyzptlk/g/h;->i:Ldbxyzptlk/g/i;

    .line 64
    return-void
.end method

.method protected final varargs a([Ljava/lang/Long;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x3e8

    .line 95
    iget-object v0, p0, Ldbxyzptlk/g/h;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_37

    .line 96
    iget-object v0, p0, Ldbxyzptlk/g/h;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/I;

    .line 97
    if-eqz v0, :cond_37

    .line 98
    iget-boolean v1, p0, Ldbxyzptlk/g/h;->g:Z

    if-eqz v1, :cond_2c

    .line 99
    iput-boolean v6, p0, Ldbxyzptlk/g/h;->g:Z

    .line 100
    iget-object v1, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DownloadTask;->f()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_2c

    iget-wide v2, v1, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2c

    .line 103
    iget-wide v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    div-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/I;->b(I)V

    .line 107
    :cond_2c
    aget-object v1, p1, v6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, v4

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/I;->a(I)V

    .line 110
    :cond_37
    return-void
.end method

.method public final b()Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->f()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/g/h;->a:Z

    .line 165
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/g/h;->a:Z

    .line 83
    iget-object v0, p0, Ldbxyzptlk/g/h;->f:Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->b()V

    .line 84
    return-void
.end method

.method public final c(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 176
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    iget-wide v2, p0, Ldbxyzptlk/g/h;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ldbxyzptlk/g/h;->publishProgress([Ljava/lang/Object;)V

    .line 177
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 23
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Ldbxyzptlk/g/h;->a([Ljava/lang/Long;)V

    return-void
.end method
