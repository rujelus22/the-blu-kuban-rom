.class public Ldbxyzptlk/g/j;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 21
    const-class v0, Ldbxyzptlk/g/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/j;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 97
    iput-object p2, p0, Ldbxyzptlk/g/j;->b:Ljava/lang/String;

    .line 98
    iput-object p3, p0, Ldbxyzptlk/g/j;->c:Ljava/lang/String;

    .line 99
    iput-object p4, p0, Ldbxyzptlk/g/j;->d:Ljava/lang/String;

    .line 100
    iput-object p5, p0, Ldbxyzptlk/g/j;->e:Ljava/lang/String;

    .line 101
    iput-boolean p6, p0, Ldbxyzptlk/g/j;->f:Z

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 85
    iput-object p2, p0, Ldbxyzptlk/g/j;->b:Ljava/lang/String;

    .line 86
    iput-object p3, p0, Ldbxyzptlk/g/j;->c:Ljava/lang/String;

    .line 87
    iput-boolean p4, p0, Ldbxyzptlk/g/j;->f:Z

    .line 89
    iput-object v0, p0, Ldbxyzptlk/g/j;->d:Ljava/lang/String;

    .line 90
    iput-object v0, p0, Ldbxyzptlk/g/j;->e:Ljava/lang/String;

    .line 91
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/g/a;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0b0046

    .line 113
    :try_start_4
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 114
    iget-object v1, p0, Ldbxyzptlk/g/j;->d:Ljava/lang/String;

    if-eqz v1, :cond_29

    .line 115
    iget-object v1, p0, Ldbxyzptlk/g/j;->b:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/g/j;->c:Ljava/lang/String;

    iget-object v3, p0, Ldbxyzptlk/g/j;->d:Ljava/lang/String;

    iget-object v4, p0, Ldbxyzptlk/g/j;->e:Ljava/lang/String;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    :try_end_18
    .catch Lcom/dropbox/android/filemanager/c; {:try_start_4 .. :try_end_18} :catch_31
    .catch Ldbxyzptlk/o/j; {:try_start_4 .. :try_end_18} :catch_38
    .catch Ldbxyzptlk/o/i; {:try_start_4 .. :try_end_18} :catch_51
    .catch Ldbxyzptlk/o/d; {:try_start_4 .. :try_end_18} :catch_9e
    .catch Ldbxyzptlk/o/a; {:try_start_4 .. :try_end_18} :catch_ad

    .line 120
    :goto_18
    :try_start_18
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;
    :try_end_1b
    .catch Ldbxyzptlk/o/a; {:try_start_18 .. :try_end_1b} :catch_c2
    .catch Lcom/dropbox/android/filemanager/c; {:try_start_18 .. :try_end_1b} :catch_31
    .catch Ldbxyzptlk/o/j; {:try_start_18 .. :try_end_1b} :catch_38
    .catch Ldbxyzptlk/o/i; {:try_start_18 .. :try_end_1b} :catch_51
    .catch Ldbxyzptlk/o/d; {:try_start_18 .. :try_end_1b} :catch_9e

    .line 123
    :goto_1b
    :try_start_1b
    iget-boolean v0, p0, Ldbxyzptlk/g/j;->f:Z

    if-eqz v0, :cond_22

    .line 126
    invoke-virtual {p0}, Ldbxyzptlk/g/j;->f()V

    .line 129
    :cond_22
    new-instance v0, Ldbxyzptlk/g/o;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/g/o;-><init>(Ldbxyzptlk/g/k;)V

    .line 157
    :goto_28
    return-object v0

    .line 117
    :cond_29
    iget-object v1, p0, Ldbxyzptlk/g/j;->b:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/g/j;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_30
    .catch Lcom/dropbox/android/filemanager/c; {:try_start_1b .. :try_end_30} :catch_31
    .catch Ldbxyzptlk/o/j; {:try_start_1b .. :try_end_30} :catch_38
    .catch Ldbxyzptlk/o/i; {:try_start_1b .. :try_end_30} :catch_51
    .catch Ldbxyzptlk/o/d; {:try_start_1b .. :try_end_30} :catch_9e
    .catch Ldbxyzptlk/o/a; {:try_start_1b .. :try_end_30} :catch_ad

    goto :goto_18

    .line 130
    :catch_31
    move-exception v0

    .line 131
    new-instance v0, Ldbxyzptlk/g/n;

    invoke-direct {v0, v7}, Ldbxyzptlk/g/n;-><init>(Ldbxyzptlk/g/k;)V

    goto :goto_28

    .line 132
    :catch_38
    move-exception v0

    .line 134
    sget-object v1, Ldbxyzptlk/g/j;->a:Ljava/lang/String;

    const-string v2, "Error logging in"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const v1, 0x7f0b0024

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/o/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 136
    new-instance v0, Ldbxyzptlk/g/l;

    invoke-direct {v0, v1}, Ldbxyzptlk/g/l;-><init>(Ljava/lang/String;)V

    goto :goto_28

    .line 137
    :catch_51
    move-exception v0

    .line 138
    sget-object v1, Ldbxyzptlk/g/j;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error logging in or creating new account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget v1, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v2, 0x190

    if-ne v1, v2, :cond_8c

    .line 141
    invoke-virtual {v0}, Ldbxyzptlk/o/i;->a()Z

    move-result v0

    if-eqz v0, :cond_84

    .line 142
    const v0, 0x7f0b002d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 150
    :goto_7d
    new-instance v1, Ldbxyzptlk/g/l;

    invoke-direct {v1, v0}, Ldbxyzptlk/g/l;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_28

    .line 144
    :cond_84
    const v0, 0x7f0b002b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7d

    .line 147
    :cond_8c
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 148
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/o/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7d

    .line 151
    :catch_9e
    move-exception v0

    .line 152
    const v0, 0x7f0b0045

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 153
    new-instance v0, Ldbxyzptlk/g/l;

    invoke-direct {v0, v1}, Ldbxyzptlk/g/l;-><init>(Ljava/lang/String;)V

    goto/16 :goto_28

    .line 154
    :catch_ad
    move-exception v0

    .line 155
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 156
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 157
    new-instance v0, Ldbxyzptlk/g/l;

    invoke-direct {v0, v1}, Ldbxyzptlk/g/l;-><init>(Ljava/lang/String;)V

    goto/16 :goto_28

    .line 121
    :catch_c2
    move-exception v0

    goto/16 :goto_1b
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 20
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/j;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/g/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 163
    invoke-interface {p2, p1}, Ldbxyzptlk/g/a;->a(Landroid/content/Context;)V

    .line 164
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 106
    sget-object v0, Ldbxyzptlk/g/j;->a:Ljava/lang/String;

    const-string v1, "Error in Logging in."

    invoke-static {v0, v1, p2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 107
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, p2, v1}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 108
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 20
    check-cast p2, Ldbxyzptlk/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/j;->a(Landroid/content/Context;Ldbxyzptlk/g/a;)V

    return-void
.end method
