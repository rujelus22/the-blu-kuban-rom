.class public final Ldbxyzptlk/g/p;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p2, p0, Ldbxyzptlk/g/p;->a:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Boolean;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 35
    :try_start_1
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 36
    iget-object v1, p0, Ldbxyzptlk/g/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->b(Ljava/lang/String;)V
    :try_end_a
    .catch Ldbxyzptlk/o/d; {:try_start_1 .. :try_end_a} :catch_13
    .catch Ldbxyzptlk/o/a; {:try_start_1 .. :try_end_a} :catch_19

    .line 38
    :try_start_a
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;
    :try_end_d
    .catch Ldbxyzptlk/o/a; {:try_start_a .. :try_end_d} :catch_28
    .catch Ldbxyzptlk/o/d; {:try_start_a .. :try_end_d} :catch_13

    .line 41
    :goto_d
    const/4 v0, 0x1

    :try_start_e
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_11
    .catch Ldbxyzptlk/o/d; {:try_start_e .. :try_end_11} :catch_13
    .catch Ldbxyzptlk/o/a; {:try_start_e .. :try_end_11} :catch_19

    move-result-object v0

    .line 47
    :goto_12
    return-object v0

    .line 42
    :catch_13
    move-exception v0

    .line 44
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_12

    .line 45
    :catch_19
    move-exception v0

    .line 46
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 47
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_12

    .line 39
    :catch_28
    move-exception v0

    goto :goto_d
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 18
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/p;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Boolean;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 53
    instance-of v0, p1, Ldbxyzptlk/g/q;

    if-nez v0, :cond_5

    .line 63
    :goto_4
    return-void

    .line 57
    :cond_5
    check-cast p1, Ldbxyzptlk/g/q;

    .line 58
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 59
    invoke-interface {p1}, Ldbxyzptlk/g/q;->h()V

    goto :goto_4

    .line 61
    :cond_11
    invoke-interface {p1}, Ldbxyzptlk/g/q;->i()V

    goto :goto_4
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, p2, v1}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 69
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 18
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/p;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    return-void
.end method
