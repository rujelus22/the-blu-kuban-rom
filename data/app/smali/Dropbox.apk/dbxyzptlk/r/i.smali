.class public final Ldbxyzptlk/r/i;
.super Ldbxyzptlk/n/a;
.source "panda.py"


# static fields
.field private static final d:Ljava/util/concurrent/ConcurrentHashMap;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 92
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Ldbxyzptlk/r/i;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 97
    const-class v0, Ldbxyzptlk/n/o;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/n/o;

    .line 98
    sget-object v2, Ldbxyzptlk/r/i;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ldbxyzptlk/n/o;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_11

    .line 100
    :cond_27
    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/q/m;Ldbxyzptlk/n/s;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/n/a;-><init>(Ldbxyzptlk/q/m;Ldbxyzptlk/n/s;)V

    .line 109
    return-void
.end method

.method static synthetic a(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-static {p0, p1, p2}, Ldbxyzptlk/r/i;->b(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method static synthetic a(Ldbxyzptlk/r/i;Ljava/lang/String;Ljava/util/List;JZLjava/lang/String;)Ldbxyzptlk/n/k;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct/range {p0 .. p6}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/util/List;JZLjava/lang/String;)Ldbxyzptlk/n/k;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/List;JZLjava/lang/String;)Ldbxyzptlk/n/k;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1358
    const-string v0, "path"

    invoke-static {p1, v0}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1360
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 1362
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 1363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1366
    :cond_23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/commit_file/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1367
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "block_hashes"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ldbxyzptlk/D/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "overwrite"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "parent_rev"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    if-eqz p6, :cond_91

    :goto_64
    aput-object p6, v4, v0

    const/4 v0, 0x6

    const-string v1, "size"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1375
    :try_start_72
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->h()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->b(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ldbxyzptlk/n/m;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/n/m;->b:Lorg/apache/http/HttpResponse;

    .line 1380
    new-instance v1, Ldbxyzptlk/n/k;

    invoke-static {v0}, Ldbxyzptlk/n/w;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V
    :try_end_8f
    .catch Ldbxyzptlk/o/i; {:try_start_72 .. :try_end_8f} :catch_94

    move-object v0, v1

    .line 1384
    :goto_90
    return-object v0

    .line 1367
    :cond_91
    const-string p6, ""

    goto :goto_64

    .line 1382
    :catch_94
    move-exception v0

    .line 1383
    invoke-static {v0}, Ldbxyzptlk/r/i;->a(Ldbxyzptlk/o/i;)V

    .line 1384
    const/4 v0, 0x0

    goto :goto_90
.end method

.method private a(Ljava/util/Map;)Ldbxyzptlk/r/p;
    .registers 4
    .parameter

    .prologue
    .line 460
    new-instance v0, Ldbxyzptlk/r/p;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Ldbxyzptlk/r/p;-><init>(Ljava/util/Map;Ldbxyzptlk/r/j;)V

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/r/i;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/r/r;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct/range {p0 .. p5}, Ldbxyzptlk/r/i;->a(Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/r/r;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/r/r;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1260
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 1261
    const-wide/32 v0, 0x400000

    cmp-long v0, p2, v0

    if-lez v0, :cond_12

    .line 1262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Block length too large"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1265
    :cond_12
    const-string v0, "/upload_block"

    .line 1266
    if-eqz p4, :cond_2d

    .line 1267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1270
    :cond_2d
    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "r2"

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1272
    new-instance v2, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 1273
    iget-object v0, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v0, v2}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 1274
    new-instance v1, Lorg/apache/http/entity/InputStreamEntity;

    invoke-direct {v1, p1, p2, p3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 1275
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 1278
    if-eqz p5, :cond_5f

    .line 1279
    new-instance v0, Ldbxyzptlk/n/u;

    invoke-direct {v0, v1, p5}, Ldbxyzptlk/n/u;-><init>(Lorg/apache/http/HttpEntity;Ldbxyzptlk/n/t;)V

    .line 1282
    :goto_54
    invoke-virtual {v2, v0}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1283
    new-instance v0, Ldbxyzptlk/r/r;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/r/r;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Ldbxyzptlk/q/m;)V

    return-object v0

    :cond_5f
    move-object v0, v1

    goto :goto_54
.end method

.method static synthetic a(Ldbxyzptlk/r/i;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;I)Ldbxyzptlk/r/t;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct/range {p0 .. p13}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;I)Ldbxyzptlk/r/t;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/util/List;JLjava/lang/String;Ljava/lang/String;I)Ldbxyzptlk/r/t;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1324
    const-string v1, "fileName"

    move-object/from16 v0, p7

    invoke-static {v1, v0}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    const-string v1, "cuHash8"

    move-object/from16 v0, p11

    invoke-static {v1, v0}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    const-string v1, "cuHashFull"

    move-object/from16 v0, p12

    invoke-static {v1, v0}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 1330
    const-string v1, "/"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_37

    .line 1331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p7

    .line 1334
    :cond_37
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/commit_camera_upload/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    move/from16 v7, p13

    .line 1335
    invoke-direct/range {v1 .. v7}, Ldbxyzptlk/r/i;->a(JJLjava/lang/String;I)Ljava/util/HashMap;

    move-result-object v3

    .line 1336
    const-string v1, "block_hashes"

    invoke-static/range {p8 .. p8}, Ldbxyzptlk/D/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337
    const-string v1, "size"

    invoke-static/range {p9 .. p10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338
    const-string v1, "cu_hash_full"

    move-object/from16 v0, p12

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339
    const-string v1, "mime_type"

    invoke-interface {v3, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342
    :try_start_7a
    sget-object v1, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v2, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v2}, Ldbxyzptlk/q/m;->h()Ljava/lang/String;

    move-result-object v2

    const-string v4, "r2"

    invoke-static {v3}, Ldbxyzptlk/r/i;->b(Ljava/util/Map;)[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    move-object v3, v8

    invoke-static/range {v1 .. v6}, Ldbxyzptlk/n/w;->b(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ldbxyzptlk/n/m;

    move-result-object v1

    iget-object v2, v1, Ldbxyzptlk/n/m;->b:Lorg/apache/http/HttpResponse;

    .line 1347
    new-instance v3, Ldbxyzptlk/n/k;

    invoke-static {v2}, Ldbxyzptlk/n/w;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v3, v1}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    .line 1348
    const-string v1, "dropbox-chillout"

    const/4 v4, 0x0

    invoke-static {v2, v1, v4}, Ldbxyzptlk/r/i;->b(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F

    move-result v2

    .line 1349
    new-instance v1, Ldbxyzptlk/r/t;

    invoke-direct {v1, v3, v2}, Ldbxyzptlk/r/t;-><init>(Ldbxyzptlk/n/k;F)V
    :try_end_a8
    .catch Ldbxyzptlk/o/i; {:try_start_7a .. :try_end_a8} :catch_a9

    .line 1352
    :goto_a8
    return-object v1

    .line 1350
    :catch_a9
    move-exception v1

    .line 1351
    invoke-static {v1}, Ldbxyzptlk/r/i;->a(Ldbxyzptlk/o/i;)V

    .line 1352
    const/4 v1, 0x0

    goto :goto_a8
.end method

.method private a(JJLjava/lang/String;I)Ljava/util/HashMap;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1013
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1014
    const-string v0, "device_manufacturer"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1015
    const-string v0, "device_model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1016
    const-string v0, "device_uid"

    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/l/m;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1017
    const-string v0, "client_platform"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1018
    const-string v0, "client_buildstring"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v3

    iget-object v3, v3, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1019
    const-string v2, "file_mtime"

    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-lez v0, :cond_94

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    :goto_68
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1020
    const-string v0, "client_timeoffset"

    invoke-virtual {v1, v0, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1021
    const-string v0, "client_import_time"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1022
    if-lez p6, :cond_84

    .line 1023
    const-string v0, "file_number"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1025
    :cond_84
    const-string v0, "locale"

    iget-object v2, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v2}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1026
    return-object v1

    .line 1019
    :cond_94
    const-string v0, ""

    goto :goto_68
.end method

.method private static a(Ldbxyzptlk/o/i;)V
    .registers 3
    .parameter

    .prologue
    .line 1306
    .line 1307
    iget v0, p0, Ldbxyzptlk/o/i;->b:I

    const/16 v1, 0x19c

    if-ne v0, v1, :cond_1c

    iget-object v0, p0, Ldbxyzptlk/o/i;->f:Ljava/util/Map;

    if-eqz v0, :cond_1c

    .line 1308
    iget-object v0, p0, Ldbxyzptlk/o/i;->f:Ljava/util/Map;

    const-string v1, "need_blocks"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1309
    if-eqz v0, :cond_1c

    .line 1311
    check-cast v0, Ljava/util/List;

    .line 1312
    new-instance v1, Ldbxyzptlk/r/A;

    invoke-direct {v1, v0}, Ldbxyzptlk/r/A;-><init>(Ljava/util/List;)V

    throw v1

    .line 1315
    :cond_1c
    throw p0
.end method

.method private static b(Lorg/apache/http/HttpResponse;Ljava/lang/String;F)F
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1179
    invoke-interface {p0, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 1180
    if-eqz v0, :cond_e

    .line 1182
    :try_start_6
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_d} :catch_f

    move-result p2

    .line 1185
    :cond_e
    :goto_e
    return p2

    .line 1183
    :catch_f
    move-exception v0

    goto :goto_e
.end method

.method private static b(Ljava/util/Map;)[Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 1030
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    .line 1031
    const/4 v0, 0x0

    .line 1032
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_12
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1033
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v3, v2

    .line 1034
    add-int/lit8 v1, v2, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 1035
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_12

    .line 1037
    :cond_34
    return-object v3
.end method

.method static synthetic c(Ljava/util/Map;Ljava/lang/String;)J
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-static {p0, p1}, Ldbxyzptlk/r/i;->b(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic d(Ljava/util/Map;Ljava/lang/String;)J
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-static {p0, p1}, Ldbxyzptlk/r/i;->b(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/io/FileInputStream;ZLjava/lang/String;Ldbxyzptlk/r/g;)Ldbxyzptlk/r/C;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1213
    .line 1214
    new-instance v7, Ldbxyzptlk/r/x;

    new-instance v0, Ldbxyzptlk/r/k;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/r/k;-><init>(Ldbxyzptlk/r/i;Ldbxyzptlk/r/i;Ljava/lang/String;ZLjava/lang/String;)V

    const/4 v6, 0x0

    move-object v1, v7

    move-object v2, p2

    move-object v3, p5

    move-object v4, p0

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Ldbxyzptlk/r/x;-><init>(Ljava/io/FileInputStream;Ldbxyzptlk/r/g;Ldbxyzptlk/r/i;Ldbxyzptlk/r/y;Ldbxyzptlk/r/l;)V

    return-object v7
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/FileInputStream;Ldbxyzptlk/r/g;Ldbxyzptlk/r/l;)Ldbxyzptlk/r/C;
    .registers 27
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1192
    new-instance v13, Ldbxyzptlk/r/x;

    new-instance v0, Ldbxyzptlk/r/j;

    move-object v1, p0

    move-object/from16 v2, p10

    move-object/from16 v3, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p3

    move-object v10, p1

    move-object/from16 v11, p11

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, Ldbxyzptlk/r/j;-><init>(Ldbxyzptlk/r/i;Ljava/io/FileInputStream;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/r/g;I)V

    move-object v1, v13

    move-object/from16 v2, p10

    move-object/from16 v3, p11

    move-object v4, p0

    move-object v5, v0

    move-object/from16 v6, p12

    invoke-direct/range {v1 .. v6}, Ldbxyzptlk/r/x;-><init>(Ljava/io/FileInputStream;Ldbxyzptlk/r/g;Ldbxyzptlk/r/i;Ldbxyzptlk/r/y;Ldbxyzptlk/r/l;)V

    return-object v13
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/InputStream;JLdbxyzptlk/n/t;)Ldbxyzptlk/r/C;
    .registers 25
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1047
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 1049
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/camera_upload/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v3, p0

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move/from16 v9, p9

    .line 1051
    invoke-direct/range {v3 .. v9}, Ldbxyzptlk/r/i;->a(JJLjava/lang/String;I)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v3}, Ldbxyzptlk/r/i;->b(Ljava/util/Map;)[Ljava/lang/String;

    move-result-object v3

    .line 1053
    iget-object v4, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v4}, Ldbxyzptlk/q/m;->h()Ljava/lang/String;

    move-result-object v4

    const-string v5, "r2"

    invoke-static {v4, v5, v10, v3}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1057
    new-instance v5, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v5, v3}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 1058
    iget-object v3, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v3, v5}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 1060
    const-string v3, "Content-Type"

    invoke-virtual {v5, v3, p2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    new-instance v4, Lorg/apache/http/entity/InputStreamEntity;

    move-object/from16 v0, p10

    move-wide/from16 v1, p11

    invoke-direct {v4, v0, v1, v2}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 1063
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 1067
    if-eqz p13, :cond_6c

    .line 1068
    new-instance v3, Ldbxyzptlk/n/u;

    move-object/from16 v0, p13

    invoke-direct {v3, v4, v0}, Ldbxyzptlk/n/u;-><init>(Lorg/apache/http/HttpEntity;Ldbxyzptlk/n/t;)V

    .line 1071
    :goto_61
    invoke-virtual {v5, v3}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1073
    new-instance v3, Ldbxyzptlk/r/s;

    iget-object v4, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-direct {v3, p0, v5, v4}, Ldbxyzptlk/r/s;-><init>(Ldbxyzptlk/r/i;Lorg/apache/http/client/methods/HttpUriRequest;Ldbxyzptlk/q/m;)V

    return-object v3

    :cond_6c
    move-object v3, v4

    goto :goto_61
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ldbxyzptlk/r/p;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 562
    const-string v0, "username"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const-string v0, "password"

    invoke-static {v0, p2}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const/16 v0, 0xc

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/dropbox/android/util/aP;->m:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/dropbox/android/util/aP;->v:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p2, v4, v0

    const/4 v0, 0x4

    const-string v1, "first_name"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p3, v4, v0

    const/4 v0, 0x6

    const-string v1, "last_name"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    aput-object p4, v4, v0

    const/16 v0, 0x8

    const-string v1, "source"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    sget-object v1, Lcom/dropbox/android/util/aP;->j:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xa

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/16 v0, 0xb

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 574
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/account"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    .line 583
    check-cast v0, Ljava/util/Map;

    .line 584
    invoke-direct {p0, v0}, Ldbxyzptlk/r/i;->a(Ljava/util/Map;)Ldbxyzptlk/r/p;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/r/u;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 351
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/media_transcode/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 355
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "hls"

    aput-object v1, v0, v3

    const-string v1, "mpegts"

    aput-object v1, v0, v5

    .line 359
    const/16 v1, 0x12

    new-array v4, v1, [Ljava/lang/String;

    const-string v1, "locale"

    aput-object v1, v4, v3

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const-string v1, "screen_resolution"

    aput-object v1, v4, v6

    const/4 v1, 0x3

    aput-object p2, v4, v1

    const/4 v1, 0x4

    const-string v3, "connection_type"

    aput-object v3, v4, v1

    const/4 v1, 0x5

    aput-object p3, v4, v1

    const/4 v1, 0x6

    const-string v3, "container"

    aput-object v3, v4, v1

    const/4 v1, 0x7

    const-string v3, ","

    invoke-static {v3, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    const/16 v0, 0x8

    const-string v1, "model"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xa

    const-string v1, "app_version"

    aput-object v1, v4, v0

    const/16 v0, 0xb

    aput-object p4, v4, v0

    const/16 v0, 0xc

    const-string v1, "sys_version"

    aput-object v1, v4, v0

    const/16 v0, 0xd

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xe

    const-string v1, "platform"

    aput-object v1, v4, v0

    const/16 v0, 0xf

    const-string v1, "android"

    aput-object v1, v4, v0

    const/16 v0, 0x10

    const-string v1, "manufacturer"

    aput-object v1, v4, v0

    const/16 v0, 0x11

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 372
    :try_start_98
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 374
    new-instance v1, Ldbxyzptlk/r/u;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/r/u;-><init>(Ljava/util/Map;Ldbxyzptlk/r/j;)V
    :try_end_b0
    .catch Ldbxyzptlk/o/i; {:try_start_98 .. :try_end_b0} :catch_b1

    return-object v1

    .line 375
    :catch_b1
    move-exception v0

    .line 376
    iget v1, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v2, 0x19f

    if-ne v1, v2, :cond_be

    .line 378
    new-instance v0, Ldbxyzptlk/r/B;

    invoke-direct {v0}, Ldbxyzptlk/r/B;-><init>()V

    throw v0

    .line 380
    :cond_be
    throw v0
.end method

.method public final a(Ldbxyzptlk/r/h;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    .line 593
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 595
    const/16 v0, 0x22

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/dropbox/android/util/aP;->g:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "android"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/dropbox/android/util/aP;->f:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p1, Ldbxyzptlk/r/h;->a:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    sget-object v1, Lcom/dropbox/android/util/aP;->D:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p1, Ldbxyzptlk/r/h;->b:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x6

    sget-object v1, Lcom/dropbox/android/util/aP;->d:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, ""

    aput-object v1, v4, v0

    const/16 v0, 0x8

    sget-object v1, Lcom/dropbox/android/util/aP;->k:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x9

    iget-object v1, p1, Ldbxyzptlk/r/h;->e:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xa

    sget-object v1, Lcom/dropbox/android/util/aP;->l:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xb

    iget-object v1, p1, Ldbxyzptlk/r/h;->h:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xc

    sget-object v1, Lcom/dropbox/android/util/aP;->A:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xd

    iget-object v1, p1, Ldbxyzptlk/r/h;->i:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xe

    sget-object v1, Lcom/dropbox/android/util/aP;->C:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xf

    iget-object v1, p1, Ldbxyzptlk/r/h;->d:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x10

    sget-object v1, Lcom/dropbox/android/util/aP;->o:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x11

    iget-object v1, p1, Ldbxyzptlk/r/h;->o:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x12

    sget-object v1, Lcom/dropbox/android/util/aP;->r:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x13

    iget-object v1, p1, Ldbxyzptlk/r/h;->g:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x14

    sget-object v1, Lcom/dropbox/android/util/aP;->x:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x15

    iget-object v1, p1, Ldbxyzptlk/r/h;->l:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x16

    sget-object v1, Lcom/dropbox/android/util/aP;->t:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x17

    iget-object v1, p1, Ldbxyzptlk/r/h;->n:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x18

    sget-object v1, Lcom/dropbox/android/util/aP;->e:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x19

    iget-object v1, p1, Ldbxyzptlk/r/h;->m:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x1a

    sget-object v1, Lcom/dropbox/android/util/aP;->z:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x1b

    iget-object v1, p1, Ldbxyzptlk/r/h;->j:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x1c

    sget-object v1, Lcom/dropbox/android/util/aP;->q:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x1d

    iget-object v1, p1, Ldbxyzptlk/r/h;->k:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x1e

    sget-object v1, Lcom/dropbox/android/util/aP;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x1f

    invoke-virtual {p1}, Ldbxyzptlk/r/h;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x20

    sget-object v1, Lcom/dropbox/android/util/aP;->a:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0x21

    iget-object v1, p1, Ldbxyzptlk/r/h;->p:Ljava/util/Map;

    invoke-static {v1}, Ldbxyzptlk/D/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 616
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aP;->n:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 622
    const-string v1, "gandalf_info"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 624
    invoke-static {v1}, Ldbxyzptlk/l/u;->a(Ljava/util/Map;)V

    .line 626
    iget-object v1, p1, Ldbxyzptlk/r/h;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/util/List;I)Ljava/util/List;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 763
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 765
    if-gtz p2, :cond_7

    .line 766
    const/16 p2, 0x61a8

    .line 769
    :cond_7
    iget-object v0, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v0}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/q/n;->toString()Ljava/lang/String;

    move-result-object v1

    .line 770
    new-instance v2, Ldbxyzptlk/D/a;

    invoke-direct {v2}, Ldbxyzptlk/D/a;-><init>()V

    .line 771
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 772
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldbxyzptlk/D/a;->add(Ljava/lang/Object;)Z

    goto :goto_1a

    .line 775
    :cond_3b
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "paths"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-virtual {v2}, Ldbxyzptlk/D/a;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "file_limit"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 781
    const-string v2, "/iphone/files_batch/"

    .line 783
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/D/a;

    .line 787
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 789
    invoke-virtual {v0}, Ldbxyzptlk/D/a;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_85
    :goto_85
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 790
    instance-of v3, v0, Ljava/util/Map;

    if-eqz v3, :cond_85

    .line 791
    new-instance v3, Ldbxyzptlk/n/k;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v3, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    .line 792
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_85

    .line 796
    :cond_9e
    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/j/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/io/InputStream;JZ)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 636
    const/16 v3, 0x10

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lcom/dropbox/android/util/aP;->g:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    sget-object v5, Lcom/dropbox/android/util/aP;->k:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    const/4 v4, 0x4

    const-string v5, "log_level"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-virtual {p3}, Ldbxyzptlk/j/e;->name()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    sget-object v5, Lcom/dropbox/android/util/aP;->f:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    if-nez p4, :cond_36

    const-string p4, "0"

    :cond_36
    aput-object p4, v3, v4

    const/16 v4, 0x8

    sget-object v5, Lcom/dropbox/android/util/aP;->D:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x9

    aput-object p5, v3, v4

    const/16 v4, 0xa

    sget-object v5, Lcom/dropbox/android/util/aP;->l:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xb

    aput-object p6, v3, v4

    const/16 v4, 0xc

    sget-object v5, Lcom/dropbox/android/util/aP;->A:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xd

    aput-object p7, v3, v4

    const/16 v4, 0xe

    const-string v5, "ts"

    aput-object v5, v3, v4

    const/16 v4, 0xf

    invoke-static/range {p8 .. p9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 646
    iget-object v4, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v4}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v4

    const-string v5, "r2"

    sget-object v6, Lcom/dropbox/android/util/aP;->u:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v3}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 649
    new-instance v4, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v4, v3}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 650
    iget-object v3, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v3, v4}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 652
    new-instance v5, Lorg/apache/http/entity/InputStreamEntity;

    move-object/from16 v0, p10

    move-wide/from16 v1, p11

    invoke-direct {v5, v0, v1, v2}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 653
    if-eqz p13, :cond_ae

    const-string v3, "gzip"

    :goto_99
    invoke-virtual {v5, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 654
    const-string v3, "text/plain"

    invoke-virtual {v5, v3}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 655
    const/4 v3, 0x0

    invoke-virtual {v5, v3}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 656
    invoke-virtual {v4, v5}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 668
    iget-object v3, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static {v3, v4}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/q/m;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    .line 669
    return-void

    .line 653
    :cond_ae
    const-string v3, "application/octet-stream"

    goto :goto_99
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 673
    const/16 v0, 0xa

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/dropbox/android/util/aP;->g:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/dropbox/android/util/aP;->k:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p2, v4, v0

    const/4 v0, 0x4

    sget-object v1, Lcom/dropbox/android/util/aP;->l:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p3, v4, v0

    const/4 v0, 0x6

    sget-object v1, Lcom/dropbox/android/util/aP;->A:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x7

    aput-object p4, v4, v0

    const/16 v0, 0x8

    const-string v1, "feedback"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    aput-object p5, v4, v0

    .line 680
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/feedback"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    .line 682
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/r/p;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 469
    const-string v0, "username"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v0, "password"

    invoke-static {v0, p2}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "email"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    const-string v1, "password"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p2, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 478
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/login"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    .line 487
    check-cast v0, Ljava/util/Map;

    .line 489
    invoke-direct {p0, v0}, Ldbxyzptlk/r/i;->a(Ljava/util/Map;)Ldbxyzptlk/r/p;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/r/p;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 496
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "checkpoint_token"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    const-string v1, "twofactor_code"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p2, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 502
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/twofactor_verify"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    .line 511
    check-cast v0, Ljava/util/Map;

    .line 513
    invoke-direct {p0, v0}, Ldbxyzptlk/r/i;->a(Ljava/util/Map;)Ldbxyzptlk/r/p;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ldbxyzptlk/r/w;
    .registers 8

    .prologue
    .line 170
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 173
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/account/info"

    const-string v3, "r2"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "locale"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v6}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 179
    new-instance v1, Ldbxyzptlk/r/w;

    invoke-direct {v1, v0}, Ldbxyzptlk/r/w;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final d(Ljava/lang/String;)Ldbxyzptlk/r/D;
    .registers 4
    .parameter

    .prologue
    .line 433
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 434
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 435
    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1, v0}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 436
    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static {v1, v0}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/q/m;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 439
    invoke-static {v0}, Ldbxyzptlk/n/w;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 440
    new-instance v1, Ldbxyzptlk/r/D;

    invoke-direct {v1, v0}, Ldbxyzptlk/r/D;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final d()Ljava/util/List;
    .registers 7

    .prologue
    .line 801
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 803
    const-string v2, "/camera_upload_hashes"

    .line 805
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    const/4 v4, 0x0

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 808
    new-instance v1, Ljava/util/LinkedList;

    const-string v2, "hashes_8"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/D/a;

    invoke-direct {v1, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method public final e(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 448
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 449
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 450
    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1, v0}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 451
    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static {v1, v0}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/q/m;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 454
    invoke-static {v0}, Ldbxyzptlk/n/w;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 455
    const-string v1, "progress"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    .line 522
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "checkpoint_token"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 527
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/twofactor_resend"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    .line 536
    check-cast v0, Ljava/util/Map;

    .line 538
    const-string v1, "twofactor_desc"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    .line 549
    const-string v0, "email"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "email"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    .line 551
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/password_reset"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    .line 558
    return-void
.end method

.method public final h(Ljava/lang/String;)Ldbxyzptlk/r/o;
    .registers 8
    .parameter

    .prologue
    .line 704
    invoke-virtual {p0}, Ldbxyzptlk/r/i;->b()V

    .line 706
    const-string v2, "/dtoken"

    .line 707
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "consumer_key"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 711
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 715
    new-instance v1, Ldbxyzptlk/r/o;

    invoke-direct {v1, v0}, Ldbxyzptlk/r/o;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final i(Ljava/lang/String;)Ldbxyzptlk/r/p;
    .registers 8
    .parameter

    .prologue
    .line 723
    const-string v0, "/validate_htc_token"

    .line 724
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "htc_token"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    .line 729
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/validate_htc_token"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 731
    invoke-direct {p0, v0}, Ldbxyzptlk/r/i;->a(Ljava/util/Map;)Ldbxyzptlk/r/p;

    move-result-object v0

    return-object v0
.end method

.method public final j(Ljava/lang/String;)Ldbxyzptlk/n/g;
    .registers 8
    .parameter

    .prologue
    .line 834
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "cursor"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 839
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/camera_uploads_delta"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/r/i;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    .line 842
    :try_start_2f
    new-instance v1, Ldbxyzptlk/p/k;

    invoke-direct {v1, v0}, Ldbxyzptlk/p/k;-><init>(Ljava/lang/Object;)V

    sget-object v0, Ldbxyzptlk/r/m;->d:Ldbxyzptlk/r/n;

    invoke-static {v1, v0}, Ldbxyzptlk/n/g;->a(Ldbxyzptlk/p/k;Ldbxyzptlk/p/c;)Ldbxyzptlk/n/g;
    :try_end_39
    .catch Ldbxyzptlk/p/b; {:try_start_2f .. :try_end_39} :catch_3b

    move-result-object v0

    return-object v0

    .line 843
    :catch_3b
    move-exception v0

    .line 844
    new-instance v1, Ldbxyzptlk/o/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing /camera_uploads_delta results: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ldbxyzptlk/p/b;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldbxyzptlk/o/f;-><init>(Ljava/lang/String;)V

    throw v1
.end method
