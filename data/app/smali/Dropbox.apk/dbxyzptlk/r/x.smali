.class final Ldbxyzptlk/r/x;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/r/C;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/io/FileInputStream;

.field private final c:Ldbxyzptlk/n/t;

.field private final d:Ldbxyzptlk/r/a;

.field private final e:Ldbxyzptlk/r/i;

.field private final f:Ldbxyzptlk/r/y;

.field private final g:Ldbxyzptlk/r/l;

.field private h:Ldbxyzptlk/r/r;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1396
    const-class v0, Ldbxyzptlk/r/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/r/x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/FileInputStream;Ldbxyzptlk/r/g;Ldbxyzptlk/r/i;Ldbxyzptlk/r/y;Ldbxyzptlk/r/l;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1423
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/r/x;->h:Ldbxyzptlk/r/r;

    .line 1424
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/r/x;->i:Z

    .line 1414
    iput-object p1, p0, Ldbxyzptlk/r/x;->b:Ljava/io/FileInputStream;

    .line 1415
    iget-object v0, p2, Ldbxyzptlk/r/g;->d:Ldbxyzptlk/n/t;

    iput-object v0, p0, Ldbxyzptlk/r/x;->c:Ldbxyzptlk/n/t;

    .line 1416
    iget-object v0, p2, Ldbxyzptlk/r/g;->a:Ldbxyzptlk/r/a;

    iput-object v0, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    .line 1417
    iput-object p3, p0, Ldbxyzptlk/r/x;->e:Ldbxyzptlk/r/i;

    .line 1418
    iput-object p4, p0, Ldbxyzptlk/r/x;->f:Ldbxyzptlk/r/y;

    .line 1419
    iput-object p5, p0, Ldbxyzptlk/r/x;->g:Ldbxyzptlk/r/l;

    .line 1420
    return-void
.end method

.method private static a(Ljava/io/FileInputStream;J)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const-wide/32 v4, 0x7fffffff

    .line 1442
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_19

    const/4 v0, 0x1

    .line 1443
    :goto_b
    if-nez v0, :cond_11

    cmp-long v0, p1, v4

    if-gtz v0, :cond_1b

    .line 1444
    :cond_11
    invoke-virtual {p0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1465
    :cond_18
    return-void

    :cond_19
    move v0, v2

    .line 1442
    goto :goto_b

    .line 1448
    :cond_1b
    sget-object v0, Ldbxyzptlk/r/x;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "safeSeeking to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1450
    invoke-virtual {p0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1451
    sub-long v0, p1, v4

    .line 1455
    const/16 v3, 0x2000

    new-array v3, v3, [B

    .line 1456
    :goto_40
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_18

    .line 1459
    array-length v4, v3

    int-to-long v4, v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {p0, v3, v2, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    .line 1460
    if-ltz v4, :cond_18

    .line 1463
    int-to-long v4, v4

    sub-long/2addr v0, v4

    .line 1464
    goto :goto_40
.end method

.method private b()V
    .registers 4

    .prologue
    const-wide/16 v1, -0x1

    .line 1427
    monitor-enter p0

    .line 1428
    :try_start_3
    iget-boolean v0, p0, Ldbxyzptlk/r/x;->i:Z

    if-eqz v0, :cond_12

    .line 1429
    new-instance v0, Ldbxyzptlk/o/g;

    const-wide/16 v1, -0x1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/o/g;-><init>(J)V

    throw v0

    .line 1431
    :catchall_f
    move-exception v0

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0

    :cond_12
    :try_start_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_f

    .line 1432
    iget-object v0, p0, Ldbxyzptlk/r/x;->g:Ldbxyzptlk/r/l;

    if-eqz v0, :cond_25

    iget-object v0, p0, Ldbxyzptlk/r/x;->g:Ldbxyzptlk/r/l;

    invoke-interface {v0}, Ldbxyzptlk/r/l;->a()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1433
    new-instance v0, Ldbxyzptlk/o/g;

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/o/g;-><init>(J)V

    throw v0

    .line 1435
    :cond_25
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 2

    .prologue
    .line 1523
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Ldbxyzptlk/r/x;->h:Ldbxyzptlk/r/r;

    if-eqz v0, :cond_a

    .line 1524
    iget-object v0, p0, Ldbxyzptlk/r/x;->h:Ldbxyzptlk/r/r;

    invoke-virtual {v0}, Ldbxyzptlk/r/r;->a()V

    .line 1526
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/r/x;->i:Z
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 1527
    monitor-exit p0

    return-void

    .line 1523
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Ljava/lang/Object;
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 1469
    invoke-direct {p0}, Ldbxyzptlk/r/x;->b()V

    .line 1471
    :try_start_4
    iget-object v0, p0, Ldbxyzptlk/r/x;->f:Ldbxyzptlk/r/y;

    iget-object v1, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    invoke-virtual {v1}, Ldbxyzptlk/r/a;->d()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    invoke-virtual {v2}, Ldbxyzptlk/r/a;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Ldbxyzptlk/r/y;->b(Ljava/util/List;J)Ljava/lang/Object;
    :try_end_15
    .catch Ldbxyzptlk/r/A; {:try_start_4 .. :try_end_15} :catch_17

    move-result-object v0

    .line 1509
    :goto_16
    return-object v0

    .line 1472
    :catch_17
    move-exception v0

    .line 1473
    iget-object v1, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    iget-object v0, v0, Ldbxyzptlk/r/A;->a:Ljava/util/List;

    invoke-virtual {v1, v0}, Ldbxyzptlk/r/a;->a(Ljava/util/Collection;)V

    .line 1477
    :try_start_1f
    iget-object v0, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    iget-object v1, p0, Ldbxyzptlk/r/x;->c:Ldbxyzptlk/n/t;

    invoke-virtual {v0, v1}, Ldbxyzptlk/r/a;->a(Ldbxyzptlk/n/t;)Ldbxyzptlk/n/t;

    move-result-object v5

    move v6, v7

    .line 1479
    :goto_28
    iget-object v0, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    invoke-virtual {v0}, Ldbxyzptlk/r/a;->c()Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 1480
    iget-object v0, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    invoke-virtual {v0}, Ldbxyzptlk/r/a;->b()Ldbxyzptlk/r/c;

    move-result-object v4

    .line 1484
    iget-object v0, p0, Ldbxyzptlk/r/x;->b:Ljava/io/FileInputStream;

    iget-wide v1, v4, Ldbxyzptlk/r/c;->b:J

    invoke-static {v0, v1, v2}, Ldbxyzptlk/r/x;->a(Ljava/io/FileInputStream;J)V

    .line 1485
    monitor-enter p0
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_3e} :catch_6c

    .line 1486
    :try_start_3e
    invoke-direct {p0}, Ldbxyzptlk/r/x;->b()V

    .line 1487
    iget-object v0, p0, Ldbxyzptlk/r/x;->e:Ldbxyzptlk/r/i;

    iget-object v1, p0, Ldbxyzptlk/r/x;->b:Ljava/io/FileInputStream;

    iget-wide v2, v4, Ldbxyzptlk/r/c;->c:J

    iget-object v4, v4, Ldbxyzptlk/r/c;->a:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/r/i;->a(Ldbxyzptlk/r/i;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/r/r;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/r/x;->h:Ldbxyzptlk/r/r;

    .line 1488
    monitor-exit p0
    :try_end_50
    .catchall {:try_start_3e .. :try_end_50} :catchall_69

    .line 1489
    :try_start_50
    sget-object v0, Ldbxyzptlk/r/x;->a:Ljava/lang/String;

    const-string v1, "Starting block upload.."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_57
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_57} :catch_6c

    .line 1491
    :try_start_57
    iget-object v0, p0, Ldbxyzptlk/r/x;->h:Ldbxyzptlk/r/r;

    invoke-virtual {v0}, Ldbxyzptlk/r/r;->b()Ljava/lang/String;

    move-result-object v0

    .line 1492
    iget-object v1, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    invoke-virtual {v1, v0}, Ldbxyzptlk/r/a;->a(Ljava/lang/String;)V

    .line 1493
    monitor-enter p0
    :try_end_63
    .catch Ldbxyzptlk/o/d; {:try_start_57 .. :try_end_63} :catch_76
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_63} :catch_6c

    .line 1494
    const/4 v0, 0x0

    :try_start_64
    iput-object v0, p0, Ldbxyzptlk/r/x;->h:Ldbxyzptlk/r/r;

    .line 1495
    monitor-exit p0
    :try_end_67
    .catchall {:try_start_64 .. :try_end_67} :catchall_73

    move v6, v7

    .line 1506
    goto :goto_28

    .line 1488
    :catchall_69
    move-exception v0

    :try_start_6a
    monitor-exit p0
    :try_end_6b
    .catchall {:try_start_6a .. :try_end_6b} :catchall_69

    :try_start_6b
    throw v0
    :try_end_6c
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_6c} :catch_6c

    .line 1516
    :catch_6c
    move-exception v0

    .line 1517
    new-instance v1, Ldbxyzptlk/o/d;

    invoke-direct {v1, v0}, Ldbxyzptlk/o/d;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 1495
    :catchall_73
    move-exception v0

    :try_start_74
    monitor-exit p0
    :try_end_75
    .catchall {:try_start_74 .. :try_end_75} :catchall_73

    :try_start_75
    throw v0
    :try_end_76
    .catch Ldbxyzptlk/o/d; {:try_start_75 .. :try_end_76} :catch_76
    .catch Ljava/io/IOException; {:try_start_75 .. :try_end_76} :catch_6c

    .line 1496
    :catch_76
    move-exception v0

    .line 1497
    :try_start_77
    sget-object v1, Ldbxyzptlk/r/x;->a:Ljava/lang/String;

    const-string v2, "Block upload error"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1498
    add-int/lit8 v1, v6, 0x1

    .line 1499
    const/4 v2, 0x3

    if-ge v1, v2, :cond_8c

    .line 1500
    sget-object v0, Ldbxyzptlk/r/x;->a:Ljava/lang/String;

    const-string v2, "Trying again.."

    invoke-static {v0, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v1

    .line 1501
    goto :goto_28

    .line 1503
    :cond_8c
    throw v0

    .line 1507
    :cond_8d
    invoke-direct {p0}, Ldbxyzptlk/r/x;->b()V
    :try_end_90
    .catch Ljava/io/IOException; {:try_start_77 .. :try_end_90} :catch_6c

    .line 1509
    :try_start_90
    iget-object v0, p0, Ldbxyzptlk/r/x;->f:Ldbxyzptlk/r/y;

    iget-object v1, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    invoke-virtual {v1}, Ldbxyzptlk/r/a;->d()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/r/x;->d:Ldbxyzptlk/r/a;

    invoke-virtual {v2}, Ldbxyzptlk/r/a;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Ldbxyzptlk/r/y;->b(Ljava/util/List;J)Ljava/lang/Object;
    :try_end_a1
    .catch Ldbxyzptlk/r/A; {:try_start_90 .. :try_end_a1} :catch_a4
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_a1} :catch_6c

    move-result-object v0

    goto/16 :goto_16

    .line 1510
    :catch_a4
    move-exception v0

    .line 1513
    :try_start_a5
    sget-object v1, Ldbxyzptlk/r/x;->a:Ljava/lang/String;

    const-string v2, "NeedBlocksException when we just uploaded everything"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1514
    new-instance v0, Ldbxyzptlk/o/a;

    const-string v1, "NeedBlocksException when we just uploaded everything"

    invoke-direct {v0, v1}, Ldbxyzptlk/o/a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b4
    .catch Ljava/io/IOException; {:try_start_a5 .. :try_end_b4} :catch_6c
.end method
