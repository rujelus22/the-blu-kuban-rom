.class Ldbxyzptlk/o/b;
.super Ldbxyzptlk/o/a;
.source "panda.py"


# instance fields
.field public a:Ldbxyzptlk/o/c;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpResponse;)V
    .registers 4
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Ldbxyzptlk/o/a;-><init>()V

    .line 158
    invoke-virtual {p0}, Ldbxyzptlk/o/b;->fillInStackTrace()Ljava/lang/Throwable;

    .line 159
    if-eqz p1, :cond_28

    .line 160
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 161
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    iput v1, p0, Ldbxyzptlk/o/b;->b:I

    .line 162
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/o/b;->c:Ljava/lang/String;

    .line 163
    const-string v0, "server"

    invoke-static {p1, v0}, Ldbxyzptlk/o/b;->a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/o/b;->d:Ljava/lang/String;

    .line 164
    const-string v0, "location"

    invoke-static {p1, v0}, Ldbxyzptlk/o/b;->a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/o/b;->e:Ljava/lang/String;

    .line 166
    :cond_28
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/HttpResponse;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 176
    invoke-direct {p0, p1}, Ldbxyzptlk/o/b;-><init>(Lorg/apache/http/HttpResponse;)V

    .line 178
    if-eqz p2, :cond_16

    instance-of v0, p2, Ljava/util/Map;

    if-eqz v0, :cond_16

    .line 179
    check-cast p2, Ljava/util/Map;

    iput-object p2, p0, Ldbxyzptlk/o/b;->f:Ljava/util/Map;

    .line 180
    new-instance v0, Ldbxyzptlk/o/c;

    iget-object v1, p0, Ldbxyzptlk/o/b;->f:Ljava/util/Map;

    invoke-direct {v0, v1}, Ldbxyzptlk/o/c;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    .line 182
    :cond_16
    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 244
    invoke-interface {p0, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 245
    if-eqz v1, :cond_b

    .line 246
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 248
    :cond_b
    return-object v0
.end method

.method public static a(Lorg/apache/http/HttpResponse;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 219
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 220
    const/16 v3, 0x12e

    if-ne v2, v3, :cond_3e

    .line 221
    const-string v2, "location"

    invoke-static {p0, v2}, Ldbxyzptlk/o/b;->a(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 223
    if-eqz v2, :cond_42

    .line 224
    const-string v3, "://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 225
    if-le v3, v4, :cond_42

    .line 226
    add-int/lit8 v3, v3, 0x3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 227
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 228
    if-le v3, v4, :cond_42

    .line 229
    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 230
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "dropbox.com"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_42

    .line 239
    :cond_3d
    :goto_3d
    return v0

    .line 236
    :cond_3e
    const/16 v3, 0x130

    if-eq v2, v3, :cond_3d

    :cond_42
    move v0, v1

    .line 239
    goto :goto_3d
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 207
    iget-object v0, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    if-eqz v0, :cond_18

    iget-object v0, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    iget-object v0, v0, Ldbxyzptlk/o/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_18

    iget-object v0, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    iget-object v0, v0, Ldbxyzptlk/o/c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_18

    .line 208
    iget-object v0, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    iget-object p1, v0, Ldbxyzptlk/o/c;->b:Ljava/lang/String;

    .line 210
    :cond_18
    return-object p1
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 190
    iget v0, p0, Ldbxyzptlk/o/b;->b:I

    const/16 v1, 0x190

    if-ne v0, v1, :cond_18

    iget-object v0, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    if-eqz v0, :cond_18

    iget-object v0, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    iget-object v0, v0, Ldbxyzptlk/o/c;->a:Ljava/lang/String;

    const-string v1, "taken"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DropboxHttpException ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/o/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldbxyzptlk/o/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/o/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 196
    iget-object v1, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    if-eqz v1, :cond_52

    .line 197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/o/b;->a:Ldbxyzptlk/o/c;

    iget-object v1, v1, Ldbxyzptlk/o/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 199
    :cond_52
    return-object v0
.end method
