.class public final Ldbxyzptlk/n/j;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/Date;


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .registers 3
    .parameter

    .prologue
    .line 1212
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/n/j;-><init>(Ljava/util/Map;Z)V

    .line 1213
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;Ldbxyzptlk/n/b;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1196
    invoke-direct {p0, p1}, Ldbxyzptlk/n/j;-><init>(Ljava/util/Map;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225
    const-string v0, "url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1226
    const-string v1, "expires"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1227
    if-eqz v1, :cond_38

    .line 1228
    invoke-static {v1}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/n/j;->b:Ljava/util/Date;

    .line 1233
    :goto_1b
    if-nez p2, :cond_35

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 1234
    const-string v1, "https://"

    const-string v2, "http://"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1235
    const-string v1, ":443/"

    const-string v2, "/"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1237
    :cond_35
    iput-object v0, p0, Ldbxyzptlk/n/j;->a:Ljava/lang/String;

    .line 1238
    return-void

    .line 1230
    :cond_38
    const/4 v1, 0x0

    iput-object v1, p0, Ldbxyzptlk/n/j;->b:Ljava/util/Date;

    goto :goto_1b
.end method

.method synthetic constructor <init>(Ljava/util/Map;ZLdbxyzptlk/n/b;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1196
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/n/j;-><init>(Ljava/util/Map;Z)V

    return-void
.end method
