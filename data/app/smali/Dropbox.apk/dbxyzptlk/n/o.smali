.class public final enum Ldbxyzptlk/n/o;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Ldbxyzptlk/n/o;

.field public static final enum b:Ldbxyzptlk/n/o;

.field public static final enum c:Ldbxyzptlk/n/o;

.field public static final enum d:Ldbxyzptlk/n/o;

.field public static final enum e:Ldbxyzptlk/n/o;

.field public static final enum f:Ldbxyzptlk/n/o;

.field public static final enum g:Ldbxyzptlk/n/o;

.field public static final enum h:Ldbxyzptlk/n/o;

.field public static final enum i:Ldbxyzptlk/n/o;

.field private static final synthetic k:[Ldbxyzptlk/n/o;


# instance fields
.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1249
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "ICON_32x32"

    const-string v2, "small"

    invoke-direct {v0, v1, v4, v2}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->a:Ldbxyzptlk/n/o;

    .line 1251
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "ICON_64x64"

    const-string v2, "medium"

    invoke-direct {v0, v1, v5, v2}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->b:Ldbxyzptlk/n/o;

    .line 1253
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "ICON_128x128"

    const-string v2, "large"

    invoke-direct {v0, v1, v6, v2}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->c:Ldbxyzptlk/n/o;

    .line 1255
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "ICON_256x256"

    const-string v2, "256x256"

    invoke-direct {v0, v1, v7, v2}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->d:Ldbxyzptlk/n/o;

    .line 1260
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "BESTFIT_320x240"

    const-string v2, "320x240_bestfit"

    invoke-direct {v0, v1, v8, v2}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->e:Ldbxyzptlk/n/o;

    .line 1262
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "BESTFIT_480x320"

    const/4 v2, 0x5

    const-string v3, "480x320_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->f:Ldbxyzptlk/n/o;

    .line 1264
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "BESTFIT_640x480"

    const/4 v2, 0x6

    const-string v3, "640x480_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->g:Ldbxyzptlk/n/o;

    .line 1266
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "BESTFIT_960x640"

    const/4 v2, 0x7

    const-string v3, "960x640_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->h:Ldbxyzptlk/n/o;

    .line 1268
    new-instance v0, Ldbxyzptlk/n/o;

    const-string v1, "BESTFIT_1024x768"

    const/16 v2, 0x8

    const-string v3, "1024x768_bestfit"

    invoke-direct {v0, v1, v2, v3}, Ldbxyzptlk/n/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ldbxyzptlk/n/o;->i:Ldbxyzptlk/n/o;

    .line 1244
    const/16 v0, 0x9

    new-array v0, v0, [Ldbxyzptlk/n/o;

    sget-object v1, Ldbxyzptlk/n/o;->a:Ldbxyzptlk/n/o;

    aput-object v1, v0, v4

    sget-object v1, Ldbxyzptlk/n/o;->b:Ldbxyzptlk/n/o;

    aput-object v1, v0, v5

    sget-object v1, Ldbxyzptlk/n/o;->c:Ldbxyzptlk/n/o;

    aput-object v1, v0, v6

    sget-object v1, Ldbxyzptlk/n/o;->d:Ldbxyzptlk/n/o;

    aput-object v1, v0, v7

    sget-object v1, Ldbxyzptlk/n/o;->e:Ldbxyzptlk/n/o;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ldbxyzptlk/n/o;->f:Ldbxyzptlk/n/o;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldbxyzptlk/n/o;->g:Ldbxyzptlk/n/o;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldbxyzptlk/n/o;->h:Ldbxyzptlk/n/o;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldbxyzptlk/n/o;->i:Ldbxyzptlk/n/o;

    aput-object v2, v0, v1

    sput-object v0, Ldbxyzptlk/n/o;->k:[Ldbxyzptlk/n/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1272
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1273
    iput-object p3, p0, Ldbxyzptlk/n/o;->j:Ljava/lang/String;

    .line 1274
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/n/o;
    .registers 2
    .parameter

    .prologue
    .line 1244
    const-class v0, Ldbxyzptlk/n/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/n/o;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/n/o;
    .registers 1

    .prologue
    .line 1244
    sget-object v0, Ldbxyzptlk/n/o;->k:[Ldbxyzptlk/n/o;

    invoke-virtual {v0}, [Ldbxyzptlk/n/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/n/o;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1277
    iget-object v0, p0, Ldbxyzptlk/n/o;->j:Ljava/lang/String;

    return-object v0
.end method
