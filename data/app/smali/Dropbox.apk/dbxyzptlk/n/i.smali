.class public final Ldbxyzptlk/n/i;
.super Ljava/io/FilterInputStream;
.source "panda.py"


# instance fields
.field private final a:Lorg/apache/http/client/methods/HttpUriRequest;

.field private final b:Ldbxyzptlk/n/h;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 637
    invoke-direct {p0, v1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 639
    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 640
    if-nez v0, :cond_12

    .line 641
    new-instance v0, Ldbxyzptlk/o/a;

    const-string v1, "Didn\'t get entity from HttpResponse"

    invoke-direct {v0, v1}, Ldbxyzptlk/o/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647
    :cond_12
    :try_start_12
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/n/i;->in:Ljava/io/InputStream;
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_18} :catch_22

    .line 652
    iput-object p1, p0, Ldbxyzptlk/n/i;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 653
    new-instance v0, Ldbxyzptlk/n/h;

    invoke-direct {v0, p2, v1}, Ldbxyzptlk/n/h;-><init>(Lorg/apache/http/HttpResponse;Ldbxyzptlk/n/b;)V

    iput-object v0, p0, Ldbxyzptlk/n/i;->b:Ldbxyzptlk/n/h;

    .line 654
    return-void

    .line 648
    :catch_22
    move-exception v0

    .line 649
    new-instance v1, Ldbxyzptlk/o/d;

    invoke-direct {v1, v0}, Ldbxyzptlk/o/d;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method static synthetic a(Ldbxyzptlk/n/i;Ljava/io/OutputStream;Ldbxyzptlk/n/t;Ldbxyzptlk/n/r;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 628
    invoke-direct {p0, p1, p2, p3}, Ldbxyzptlk/n/i;->a(Ljava/io/OutputStream;Ldbxyzptlk/n/t;Ldbxyzptlk/n/r;)V

    return-void
.end method

.method private a(Ljava/io/OutputStream;Ldbxyzptlk/n/t;Ldbxyzptlk/n/r;)V
    .registers 21
    .parameter
    .parameter
    .parameter

    .prologue
    .line 710
    const/4 v4, 0x0

    .line 711
    const-wide/16 v2, 0x0

    .line 712
    const-wide/16 v5, 0x0

    .line 713
    move-object/from16 v0, p0

    iget-object v1, v0, Ldbxyzptlk/n/i;->b:Ldbxyzptlk/n/h;

    invoke-virtual {v1}, Ldbxyzptlk/n/h;->b()J

    move-result-wide v8

    .line 716
    :try_start_d
    new-instance v7, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_14
    .catchall {:try_start_d .. :try_end_14} :catchall_bb
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_14} :catch_c0

    .line 718
    const/16 v1, 0x1000

    :try_start_16
    new-array v10, v1, [B
    :try_end_18
    .catchall {:try_start_16 .. :try_end_18} :catchall_be
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_18} :catch_c3

    move-wide v15, v5

    move-wide v5, v2

    move-wide v3, v15

    .line 721
    :cond_1b
    :goto_1b
    :try_start_1b
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Ldbxyzptlk/n/i;->read([B)I

    move-result v1

    .line 722
    if-gez v1, :cond_85

    .line 723
    const-wide/16 v1, 0x0

    cmp-long v1, v8, v1

    if-ltz v1, :cond_5a

    cmp-long v1, v5, v8

    if-gez v1, :cond_5a

    .line 725
    new-instance v1, Ldbxyzptlk/o/g;

    invoke-direct {v1, v5, v6}, Ldbxyzptlk/o/g;-><init>(J)V

    throw v1
    :try_end_33
    .catchall {:try_start_1b .. :try_end_33} :catchall_be
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_33} :catch_33

    .line 756
    :catch_33
    move-exception v1

    move-wide v2, v5

    move-object v4, v7

    .line 757
    :goto_36
    :try_start_36
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 758
    if-eqz v1, :cond_a9

    const-string v5, "No space"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a9

    .line 761
    new-instance v1, Ldbxyzptlk/o/e;

    invoke-direct {v1}, Ldbxyzptlk/o/e;-><init>()V

    throw v1
    :try_end_4a
    .catchall {:try_start_36 .. :try_end_4a} :catchall_4a

    .line 772
    :catchall_4a
    move-exception v1

    move-object v7, v4

    :goto_4c
    if-eqz v7, :cond_51

    .line 774
    :try_start_4e
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_b5

    .line 777
    :cond_51
    :goto_51
    if-eqz p1, :cond_56

    .line 779
    :try_start_53
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_56} :catch_b7

    .line 785
    :cond_56
    :goto_56
    :try_start_56
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/n/i;->close()V
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_59} :catch_b9

    .line 786
    :goto_59
    throw v1

    .line 728
    :cond_5a
    :try_start_5a
    move-object/from16 v0, p3

    invoke-static {v0, v8, v9}, Ldbxyzptlk/n/q;->b(Ldbxyzptlk/n/r;J)V

    .line 746
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->flush()V

    .line 747
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->flush()V
    :try_end_65
    .catchall {:try_start_5a .. :try_end_65} :catchall_be
    .catch Ljava/io/IOException; {:try_start_5a .. :try_end_65} :catch_33

    .line 750
    :try_start_65
    move-object/from16 v0, p1

    instance-of v1, v0, Ljava/io/FileOutputStream;

    if-eqz v1, :cond_77

    .line 751
    move-object/from16 v0, p1

    check-cast v0, Ljava/io/FileOutputStream;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V
    :try_end_77
    .catchall {:try_start_65 .. :try_end_77} :catchall_be
    .catch Ljava/io/SyncFailedException; {:try_start_65 .. :try_end_77} :catch_c7
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_77} :catch_33

    .line 772
    :cond_77
    :goto_77
    if-eqz v7, :cond_7c

    .line 774
    :try_start_79
    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_7c
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_7c} :catch_af

    .line 777
    :cond_7c
    :goto_7c
    if-eqz p1, :cond_81

    .line 779
    :try_start_7e
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V
    :try_end_81
    .catch Ljava/io/IOException; {:try_start_7e .. :try_end_81} :catch_b1

    .line 785
    :cond_81
    :goto_81
    :try_start_81
    invoke-virtual/range {p0 .. p0}, Ldbxyzptlk/n/i;->close()V
    :try_end_84
    .catch Ljava/io/IOException; {:try_start_81 .. :try_end_84} :catch_b3

    .line 788
    :goto_84
    return-void

    .line 732
    :cond_85
    int-to-long v11, v1

    :try_start_86
    move-object/from16 v0, p3

    invoke-static {v0, v11, v12}, Ldbxyzptlk/n/q;->a(Ldbxyzptlk/n/r;J)V

    .line 733
    const/4 v2, 0x0

    invoke-virtual {v7, v10, v2, v1}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 735
    int-to-long v1, v1

    add-long/2addr v5, v1

    .line 737
    if-eqz p2, :cond_1b

    .line 738
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 739
    sub-long v11, v1, v3

    invoke-virtual/range {p2 .. p2}, Ldbxyzptlk/n/t;->a()J

    move-result-wide v13

    cmp-long v11, v11, v13

    if-lez v11, :cond_c9

    .line 741
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6, v8, v9}, Ldbxyzptlk/n/t;->a(JJ)V
    :try_end_a6
    .catchall {:try_start_86 .. :try_end_a6} :catchall_be
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_a6} :catch_33

    :goto_a6
    move-wide v3, v1

    .line 743
    goto/16 :goto_1b

    .line 769
    :cond_a9
    :try_start_a9
    new-instance v1, Ldbxyzptlk/o/g;

    invoke-direct {v1, v2, v3}, Ldbxyzptlk/o/g;-><init>(J)V

    throw v1
    :try_end_af
    .catchall {:try_start_a9 .. :try_end_af} :catchall_4a

    .line 775
    :catch_af
    move-exception v1

    goto :goto_7c

    .line 780
    :catch_b1
    move-exception v1

    goto :goto_81

    .line 786
    :catch_b3
    move-exception v1

    goto :goto_84

    .line 775
    :catch_b5
    move-exception v2

    goto :goto_51

    .line 780
    :catch_b7
    move-exception v2

    goto :goto_56

    .line 786
    :catch_b9
    move-exception v2

    goto :goto_59

    .line 772
    :catchall_bb
    move-exception v1

    move-object v7, v4

    goto :goto_4c

    :catchall_be
    move-exception v1

    goto :goto_4c

    .line 756
    :catch_c0
    move-exception v1

    goto/16 :goto_36

    :catch_c3
    move-exception v1

    move-object v4, v7

    goto/16 :goto_36

    .line 753
    :catch_c7
    move-exception v1

    goto :goto_77

    :cond_c9
    move-wide v1, v3

    goto :goto_a6
.end method


# virtual methods
.method public final a()Ldbxyzptlk/n/h;
    .registers 2

    .prologue
    .line 674
    iget-object v0, p0, Ldbxyzptlk/n/i;->b:Ldbxyzptlk/n/h;

    return-object v0
.end method

.method public final close()V
    .registers 2

    .prologue
    .line 667
    iget-object v0, p0, Ldbxyzptlk/n/i;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 668
    return-void
.end method
