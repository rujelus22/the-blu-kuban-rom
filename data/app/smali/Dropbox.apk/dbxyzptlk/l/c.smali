.class public final Ldbxyzptlk/l/c;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/SharedPreferences;


# instance fields
.field final a:Ldbxyzptlk/l/w;

.field final b:Ljava/lang/String;

.field final c:Landroid/database/sqlite/SQLiteDatabase;

.field final d:Ljava/util/HashMap;

.field private e:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    .line 43
    invoke-static {p1}, Ldbxyzptlk/l/w;->a(Landroid/content/Context;)Ldbxyzptlk/l/w;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/l/c;->a:Ldbxyzptlk/l/w;

    .line 44
    iput-object p2, p0, Ldbxyzptlk/l/c;->b:Ljava/lang/String;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/l/c;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 46
    invoke-direct {p0, p2}, Ldbxyzptlk/l/c;->c(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    .line 53
    invoke-static {p2}, Ldbxyzptlk/l/w;->a(Landroid/content/Context;)Ldbxyzptlk/l/w;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/l/c;->a:Ldbxyzptlk/l/w;

    .line 54
    iput-object p3, p0, Ldbxyzptlk/l/c;->b:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Ldbxyzptlk/l/c;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 56
    invoke-direct {p0, p3}, Ldbxyzptlk/l/c;->c(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method private a()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Ldbxyzptlk/l/c;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_7

    .line 61
    iget-object v0, p0, Ldbxyzptlk/l/c;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 63
    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Ldbxyzptlk/l/c;->a:Ldbxyzptlk/l/w;

    invoke-virtual {v0}, Ldbxyzptlk/l/w;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    goto :goto_6
.end method

.method private a(Ljava/lang/String;Ldbxyzptlk/l/f;)Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 182
    iget-object v2, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    monitor-enter v2

    .line 183
    :try_start_4
    iget-object v0, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_12

    .line 188
    instance-of v3, v0, Ldbxyzptlk/l/e;

    if-nez v3, :cond_12

    .line 189
    monitor-exit v2

    .line 201
    :goto_11
    return-object v0

    .line 193
    :cond_12
    instance-of v3, v0, Ldbxyzptlk/l/e;

    if-eqz v3, :cond_2d

    .line 194
    check-cast v0, Ldbxyzptlk/l/e;

    iget-object v0, v0, Ldbxyzptlk/l/e;->a:Ljava/lang/String;

    .line 196
    :goto_1a
    if-nez v0, :cond_1f

    .line 197
    monitor-exit v2

    move-object v0, v1

    goto :goto_11

    .line 199
    :cond_1f
    invoke-virtual {p2, v0}, Ldbxyzptlk/l/f;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 200
    iget-object v1, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    monitor-exit v2

    goto :goto_11

    .line 202
    :catchall_2a
    move-exception v0

    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_4 .. :try_end_2c} :catchall_2a

    throw v0

    :cond_2d
    move-object v0, v1

    goto :goto_1a
.end method

.method static a(Ljava/util/Set;)Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    .line 267
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 268
    const/4 v0, 0x1

    .line 269
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 271
    const-string v4, "\\^"

    const-string v5, "^^"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 273
    const-string v4, "~"

    const-string v5, "^t"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 274
    if-nez v1, :cond_34

    .line 275
    const/16 v0, 0x7e

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 279
    :goto_2f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .line 280
    goto :goto_b

    .line 277
    :cond_34
    const/4 v0, 0x0

    goto :goto_2f

    .line 281
    :cond_36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 31
    invoke-static {p0}, Ldbxyzptlk/l/c;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/util/Set;
    .registers 8
    .parameter

    .prologue
    .line 251
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 254
    const-string v0, "~"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 255
    array-length v3, v2

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v3, :cond_27

    aget-object v4, v2, v0

    .line 257
    const-string v5, "\\^t"

    const-string v6, "~"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 259
    const-string v5, "\\^\\^"

    const-string v6, "^"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 260
    invoke-virtual {v1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 263
    :cond_27
    return-object v1
.end method

.method private c(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    .line 311
    invoke-direct {p0}, Ldbxyzptlk/l/c;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 312
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE IF NOT EXISTS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " INTEGER PRIMARY KEY AUTOINCREMENT, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "pref_name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " TEXT NOT NULL UNIQUE, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "pref_value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " TEXT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p0}, Ldbxyzptlk/l/c;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 321
    iget-object v1, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    monitor-enter v1

    .line 322
    :try_start_51
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_59
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 323
    iget-object v3, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    new-instance v5, Ldbxyzptlk/l/e;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v5, v0}, Ldbxyzptlk/l/e;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_59

    .line 325
    :catchall_7a
    move-exception v0

    monitor-exit v1
    :try_end_7c
    .catchall {:try_start_51 .. :try_end_7c} :catchall_7a

    throw v0

    :cond_7d
    :try_start_7d
    monitor-exit v1
    :try_end_7e
    .catchall {:try_start_7d .. :try_end_7e} :catchall_7a

    .line 326
    return-void
.end method


# virtual methods
.method final a(Ljava/util/Collection;)V
    .registers 7
    .parameter

    .prologue
    .line 71
    iget-object v1, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    monitor-enter v1

    .line 72
    :try_start_3
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 73
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_2f

    .line 74
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 76
    invoke-interface {v1, p0, v0}, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    goto :goto_1f

    .line 73
    :catchall_2f
    move-exception v0

    :try_start_30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    throw v0

    .line 79
    :cond_32
    return-void
.end method

.method public final contains(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    .line 96
    iget-object v1, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    monitor-enter v1

    .line 97
    :try_start_3
    iget-object v0, p0, Ldbxyzptlk/l/c;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_c
    monitor-exit v1

    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    .line 98
    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    throw v0
.end method

.method public final edit()Landroid/content/SharedPreferences$Editor;
    .registers 2

    .prologue
    .line 103
    new-instance v0, Ldbxyzptlk/l/b;

    invoke-direct {v0, p0}, Ldbxyzptlk/l/b;-><init>(Ldbxyzptlk/l/c;)V

    return-object v0
.end method

.method public final getAll()Ljava/util/Map;
    .registers 11

    .prologue
    const/4 v8, 0x0

    .line 113
    invoke-direct {p0}, Ldbxyzptlk/l/c;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 115
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 120
    :try_start_a
    iget-object v1, p0, Ldbxyzptlk/l/c;->b:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_15
    .catchall {:try_start_a .. :try_end_15} :catchall_37

    move-result-object v1

    .line 122
    :goto_16
    :try_start_16
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 123
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 124
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-virtual {v9, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_29
    .catchall {:try_start_16 .. :try_end_29} :catchall_2a

    goto :goto_16

    .line 128
    :catchall_2a
    move-exception v0

    :goto_2b
    if-eqz v1, :cond_30

    .line 129
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_30
    throw v0

    .line 128
    :cond_31
    if-eqz v1, :cond_36

    .line 129
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_36
    return-object v9

    .line 128
    :catchall_37
    move-exception v0

    move-object v1, v8

    goto :goto_2b
.end method

.method public final getBoolean(Ljava/lang/String;Z)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 207
    sget-object v0, Ldbxyzptlk/l/f;->a:Ldbxyzptlk/l/f;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/l/c;->a(Ljava/lang/String;Ldbxyzptlk/l/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 208
    if-nez v0, :cond_b

    .line 211
    :goto_a
    return p2

    :cond_b
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_a
.end method

.method public final getFloat(Ljava/lang/String;F)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 216
    sget-object v0, Ldbxyzptlk/l/f;->f:Ldbxyzptlk/l/f;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/l/c;->a(Ljava/lang/String;Ldbxyzptlk/l/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 217
    if-nez v0, :cond_b

    .line 220
    :goto_a
    return p2

    :cond_b
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p2

    goto :goto_a
.end method

.method public final getInt(Ljava/lang/String;I)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 225
    sget-object v0, Ldbxyzptlk/l/f;->b:Ldbxyzptlk/l/f;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/l/c;->a(Ljava/lang/String;Ldbxyzptlk/l/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 226
    if-nez v0, :cond_b

    .line 229
    :goto_a
    return p2

    :cond_b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_a
.end method

.method public final getLong(Ljava/lang/String;J)J
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 234
    sget-object v0, Ldbxyzptlk/l/f;->c:Ldbxyzptlk/l/f;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/l/c;->a(Ljava/lang/String;Ldbxyzptlk/l/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 235
    if-nez v0, :cond_b

    .line 238
    :goto_a
    return-wide p2

    :cond_b
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    goto :goto_a
.end method

.method public final getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 243
    sget-object v0, Ldbxyzptlk/l/f;->d:Ldbxyzptlk/l/f;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/l/c;->a(Ljava/lang/String;Ldbxyzptlk/l/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 244
    if-nez v0, :cond_b

    .line 247
    :goto_a
    return-object p2

    :cond_b
    move-object p2, v0

    goto :goto_a
.end method

.method public final getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 286
    sget-object v0, Ldbxyzptlk/l/f;->e:Ldbxyzptlk/l/f;

    invoke-direct {p0, p1, v0}, Ldbxyzptlk/l/c;->a(Ljava/lang/String;Ldbxyzptlk/l/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 287
    if-nez v0, :cond_b

    .line 290
    :goto_a
    return-object p2

    :cond_b
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_a
.end method

.method public final registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 4
    .parameter

    .prologue
    .line 296
    iget-object v1, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    monitor-enter v1

    .line 297
    :try_start_3
    iget-object v0, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 298
    monitor-exit v1

    .line 299
    return-void

    .line 298
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public final unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 4
    .parameter

    .prologue
    .line 304
    iget-object v1, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    monitor-enter v1

    .line 305
    :try_start_3
    iget-object v0, p0, Ldbxyzptlk/l/c;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 306
    monitor-exit v1

    .line 307
    return-void

    .line 306
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method
