.class public final enum Ldbxyzptlk/l/s;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Ldbxyzptlk/l/s;

.field public static final enum b:Ldbxyzptlk/l/s;

.field private static final synthetic c:[Ldbxyzptlk/l/s;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    new-instance v0, Ldbxyzptlk/l/s;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/l/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/l/s;->a:Ldbxyzptlk/l/s;

    .line 71
    new-instance v0, Ldbxyzptlk/l/s;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v3}, Ldbxyzptlk/l/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbxyzptlk/l/s;->b:Ldbxyzptlk/l/s;

    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Ldbxyzptlk/l/s;

    sget-object v1, Ldbxyzptlk/l/s;->a:Ldbxyzptlk/l/s;

    aput-object v1, v0, v2

    sget-object v1, Ldbxyzptlk/l/s;->b:Ldbxyzptlk/l/s;

    aput-object v1, v0, v3

    sput-object v0, Ldbxyzptlk/l/s;->c:[Ldbxyzptlk/l/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ldbxyzptlk/l/s;
    .registers 4
    .parameter

    .prologue
    .line 74
    if-nez p0, :cond_5

    .line 75
    sget-object v0, Ldbxyzptlk/l/s;->a:Ldbxyzptlk/l/s;

    .line 82
    :goto_4
    return-object v0

    .line 79
    :cond_5
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/l/s;->valueOf(Ljava/lang/String;)Ldbxyzptlk/l/s;
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_c} :catch_e

    move-result-object v0

    goto :goto_4

    .line 80
    :catch_e
    move-exception v0

    .line 81
    invoke-static {}, Ldbxyzptlk/l/m;->K()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown twofactor delivery mode ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), defaulting to SMS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    sget-object v0, Ldbxyzptlk/l/s;->a:Ldbxyzptlk/l/s;

    goto :goto_4
.end method

.method public static valueOf(Ljava/lang/String;)Ldbxyzptlk/l/s;
    .registers 2
    .parameter

    .prologue
    .line 69
    const-class v0, Ldbxyzptlk/l/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/l/s;

    return-object v0
.end method

.method public static values()[Ldbxyzptlk/l/s;
    .registers 1

    .prologue
    .line 69
    sget-object v0, Ldbxyzptlk/l/s;->c:[Ldbxyzptlk/l/s;

    invoke-virtual {v0}, [Ldbxyzptlk/l/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbxyzptlk/l/s;

    return-object v0
.end method
