.class public final Lcom/actionbarsherlock/R$string;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final abs__action_bar_home_description:I = 0x7f0b0000

.field public static final abs__action_bar_up_description:I = 0x7f0b0001

.field public static final abs__action_menu_overflow_description:I = 0x7f0b0002

.field public static final abs__action_mode_done:I = 0x7f0b0003

.field public static final abs__activity_chooser_view_dialog_title_default:I = 0x7f0b0005

.field public static final abs__activity_chooser_view_see_all:I = 0x7f0b0004

.field public static final abs__activitychooserview_choose_application:I = 0x7f0b0007

.field public static final abs__share_action_provider_share_with:I = 0x7f0b0006

.field public static final abs__shareactionprovider_share_with:I = 0x7f0b0008

.field public static final abs__shareactionprovider_share_with_application:I = 0x7f0b0009

.field public static final accounts_pref_header_dropbox_settings:I = 0x7f0b019d

.field public static final accounts_pref_item_settings_desc:I = 0x7f0b019f

.field public static final accounts_pref_item_settings_name:I = 0x7f0b019e

.field public static final already_dropbox_user:I = 0x7f0b001c

.field public static final app_name:I = 0x7f0b0016

.field public static final auth_allow:I = 0x7f0b0193

.field public static final auth_cancel:I = 0x7f0b0192

.field public static final auth_error_dialog_title:I = 0x7f0b018f

.field public static final auth_improperly_configured:I = 0x7f0b0190

.field public static final auth_please_wait:I = 0x7f0b0191

.field public static final auth_retry:I = 0x7f0b0194

.field public static final auth_retry_connect_info:I = 0x7f0b0195

.field public static final browser_progress_folder_does_not_exist:I = 0x7f0b0042

.field public static final browser_progress_loading_folder:I = 0x7f0b003a

.field public static final browser_progress_no_data_finished:I = 0x7f0b003e

.field public static final browser_progress_no_data_offline:I = 0x7f0b003d

.field public static final browser_progress_no_search_data_offline:I = 0x7f0b003c

.field public static final browser_progress_search_no_data_finished:I = 0x7f0b0040

.field public static final browser_progress_search_too_short:I = 0x7f0b003f

.field public static final browser_progress_searching:I = 0x7f0b003b

.field public static final browser_progress_unauthorized:I = 0x7f0b0041

.field public static final browser_up_to_parent:I = 0x7f0b0043

.field public static final cache_cleared:I = 0x7f0b00f8

.field public static final camera_upload_all_done_text:I = 0x7f0b011b

.field public static final camera_upload_backlog_finished_notification_body:I = 0x7f0b01bb

.field public static final camera_upload_backlog_finished_notification_ticker:I = 0x7f0b01b9

.field public static final camera_upload_backlog_finished_notification_title:I = 0x7f0b01ba

.field public static final camera_upload_battery_notification_body:I = 0x7f0b01b5

.field public static final camera_upload_battery_notification_title:I = 0x7f0b01b4

.field public static final camera_upload_button_pause:I = 0x7f0b012d

.field public static final camera_upload_button_resume:I = 0x7f0b012e

.field public static final camera_upload_button_uploaded:I = 0x7f0b012f

.field public static final camera_upload_details_file_time:I = 0x7f0b012a

.field public static final camera_upload_details_file_time_today:I = 0x7f0b012b

.field public static final camera_upload_details_file_time_yesterday:I = 0x7f0b012c

.field public static final camera_upload_error_generic_msg:I = 0x7f0b00cc

.field public static final camera_upload_error_generic_title:I = 0x7f0b00cb

.field public static final camera_upload_error_perms_msg:I = 0x7f0b00ce

.field public static final camera_upload_error_server_msg:I = 0x7f0b00cd

.field public static final camera_upload_first_finished_notification_body:I = 0x7f0b01be

.field public static final camera_upload_first_finished_notification_ticker:I = 0x7f0b01bc

.field public static final camera_upload_first_finished_notification_title:I = 0x7f0b01bd

.field public static final camera_upload_first_tooltip_message:I = 0x7f0b0117

.field public static final camera_upload_get_more_space_button:I = 0x7f0b0133

.field public static final camera_upload_mult_remaining:I = 0x7f0b0127

.field public static final camera_upload_mult_remaining_parens:I = 0x7f0b0129

.field public static final camera_upload_no_photos_text:I = 0x7f0b011c

.field public static final camera_upload_one_remaining:I = 0x7f0b0126

.field public static final camera_upload_one_remaining_parens:I = 0x7f0b0128

.field public static final camera_upload_out_of_quota_text:I = 0x7f0b011d

.field public static final camera_upload_prefs_3g_limit:I = 0x7f0b00fd

.field public static final camera_upload_prefs_3g_limit_25mb:I = 0x7f0b00fe

.field public static final camera_upload_prefs_3g_limit_none:I = 0x7f0b00ff

.field public static final camera_upload_prefs_connection:I = 0x7f0b00fc

.field public static final camera_upload_prefs_title:I = 0x7f0b00f9

.field public static final camera_upload_prefs_turn_off:I = 0x7f0b00fb

.field public static final camera_upload_prefs_turn_on:I = 0x7f0b00fa

.field public static final camera_upload_promo_body:I = 0x7f0b01b8

.field public static final camera_upload_promo_ticker:I = 0x7f0b01b6

.field public static final camera_upload_promo_title:I = 0x7f0b01b7

.field public static final camera_upload_quota_notification_body:I = 0x7f0b01b3

.field public static final camera_upload_quota_notification_title:I = 0x7f0b01b2

.field public static final camera_upload_settings_switch_off:I = 0x7f0b01ca

.field public static final camera_upload_settings_switch_on:I = 0x7f0b01c9

.field public static final camera_upload_setup_title:I = 0x7f0b0116

.field public static final camera_upload_status_all_done:I = 0x7f0b0119

.field public static final camera_upload_status_almost_full:I = 0x7f0b0125

.field public static final camera_upload_status_finding:I = 0x7f0b0121

.field public static final camera_upload_status_finding_short:I = 0x7f0b0122

.field public static final camera_upload_status_item_title:I = 0x7f0b0118

.field public static final camera_upload_status_low_battery:I = 0x7f0b0123

.field public static final camera_upload_status_low_plugged_battery:I = 0x7f0b0124

.field public static final camera_upload_status_no_photos:I = 0x7f0b011a

.field public static final camera_upload_status_quota_paused:I = 0x7f0b011e

.field public static final camera_upload_status_waiting_for_faster_network:I = 0x7f0b011f

.field public static final camera_upload_status_waiting_for_wifi:I = 0x7f0b0120

.field public static final camera_upload_tour_button_start:I = 0x7f0b0112

.field public static final camera_upload_tour_connection_3g_warning:I = 0x7f0b0107

.field public static final camera_upload_tour_connection_choice:I = 0x7f0b0104

.field public static final camera_upload_tour_connection_wifi_3g:I = 0x7f0b0106

.field public static final camera_upload_tour_connection_wifi_only:I = 0x7f0b0105

.field public static final camera_upload_tour_enable_button:I = 0x7f0b010c

.field public static final camera_upload_tour_message2:I = 0x7f0b0115

.field public static final camera_upload_tour_off_button:I = 0x7f0b010a

.field public static final camera_upload_tour_on_button:I = 0x7f0b0109

.field public static final camera_upload_tour_page1_text:I = 0x7f0b0103

.field public static final camera_upload_tour_page1_title:I = 0x7f0b0102

.field public static final camera_upload_tour_page1_title_new:I = 0x7f0b0101

.field public static final camera_upload_tour_skip_button:I = 0x7f0b010d

.field public static final camera_upload_tour_splash_header:I = 0x7f0b010e

.field public static final camera_upload_tour_splash_message:I = 0x7f0b0110

.field public static final camera_upload_tour_splash_message2:I = 0x7f0b0111

.field public static final camera_upload_tour_splash_title:I = 0x7f0b010f

.field public static final camera_upload_tour_title:I = 0x7f0b0100

.field public static final camera_upload_tour_title2:I = 0x7f0b0113

.field public static final camera_upload_tour_title3:I = 0x7f0b0114

.field public static final camera_upload_tour_turn_on_button:I = 0x7f0b010b

.field public static final camera_upload_tour_upload_existing_choice:I = 0x7f0b0108

.field public static final camera_uploads_grid_empty:I = 0x7f0b0132

.field public static final camera_uploads_grid_loading:I = 0x7f0b0131

.field public static final camera_uploads_grid_title_v2:I = 0x7f0b0130

.field public static final cancel:I = 0x7f0b0018

.field public static final choose_directory_button:I = 0x7f0b015f

.field public static final choose_directory_title:I = 0x7f0b015e

.field public static final choose_file_title:I = 0x7f0b0160

.field public static final copy_link_clipboard:I = 0x7f0b008f

.field public static final copy_to_clipboard:I = 0x7f0b0091

.field public static final delete_confirm:I = 0x7f0b0095

.field public static final delete_dialog_message:I = 0x7f0b0094

.field public static final delete_dialog_title:I = 0x7f0b0093

.field public static final delete_folder_confirm:I = 0x7f0b0099

.field public static final delete_folder_dialog_message:I = 0x7f0b0098

.field public static final delete_folder_dialog_title:I = 0x7f0b0097

.field public static final directory_shortcut_button:I = 0x7f0b0164

.field public static final directory_shortcut_title:I = 0x7f0b0163

.field public static final download_cancel_button:I = 0x7f0b00ab

.field public static final download_dialog_how_send:I = 0x7f0b008c

.field public static final download_dialog_how_view:I = 0x7f0b008a

.field public static final enter_twofactor_code_didnt_receive_button:I = 0x7f0b01a8

.field public static final enter_twofactor_code_didnt_receive_desc:I = 0x7f0b01ae

.field public static final enter_twofactor_code_didnt_receive_need_help_button:I = 0x7f0b01b0

.field public static final enter_twofactor_code_didnt_receive_send_new_button:I = 0x7f0b01af

.field public static final enter_twofactor_code_didnt_receive_title:I = 0x7f0b01ad

.field public static final enter_twofactor_code_submit:I = 0x7f0b01a7

.field public static final enter_twofactor_code_title:I = 0x7f0b01a6

.field public static final error_bad_login:I = 0x7f0b0024

.field public static final error_email_does_not_exist:I = 0x7f0b01a5

.field public static final error_email_taken:I = 0x7f0b002d

.field public static final error_generic:I = 0x7f0b01b1

.field public static final error_import:I = 0x7f0b0047

.field public static final error_import_security:I = 0x7f0b0048

.field public static final error_invalid_email:I = 0x7f0b002b

.field public static final error_invalid_first_name:I = 0x7f0b0029

.field public static final error_invalid_last_name:I = 0x7f0b002a

.field public static final error_invalid_password:I = 0x7f0b002c

.field public static final error_login_needed_to_access:I = 0x7f0b004b

.field public static final error_network_error:I = 0x7f0b0045

.field public static final error_no_mime_viewer:I = 0x7f0b0077

.field public static final error_no_sdcard:I = 0x7f0b0044

.field public static final error_one_account:I = 0x7f0b002e

.field public static final error_text_upload_not_imp:I = 0x7f0b004a

.field public static final error_twofactor_code_code_invalid:I = 0x7f0b01a9

.field public static final error_unknown:I = 0x7f0b0046

.field public static final error_view_security:I = 0x7f0b0049

.field public static final export_error:I = 0x7f0b00ac

.field public static final export_overwrite_dialog_confirm:I = 0x7f0b00b0

.field public static final export_overwrite_dialog_message:I = 0x7f0b00af

.field public static final export_overwrite_dialog_title:I = 0x7f0b00ae

.field public static final export_successful:I = 0x7f0b00ad

.field public static final export_to_sd:I = 0x7f0b0092

.field public static final favorites_empty_head:I = 0x7f0b00b6

.field public static final favorites_empty_text:I = 0x7f0b00b7

.field public static final favorites_error:I = 0x7f0b00c0

.field public static final favorites_message_checking:I = 0x7f0b00b9

.field public static final favorites_message_no_connection:I = 0x7f0b00bf

.field public static final favorites_message_out_of_date_and_updating_plur:I = 0x7f0b00be

.field public static final favorites_message_out_of_date_and_updating_sing:I = 0x7f0b00bd

.field public static final favorites_message_out_of_date_plur:I = 0x7f0b00bc

.field public static final favorites_message_out_of_date_sing:I = 0x7f0b00bb

.field public static final favorites_message_up_to_date:I = 0x7f0b00ba

.field public static final favorites_refresh_button:I = 0x7f0b00b8

.field public static final file_not_deleted_error:I = 0x7f0b0096

.field public static final file_size_and_mtime:I = 0x7f0b0058

.field public static final folder_name:I = 0x7f0b0162

.field public static final folder_not_deleted_error:I = 0x7f0b009a

.field public static final forgot_password:I = 0x7f0b01a0

.field public static final forgot_password_leadin:I = 0x7f0b01a2

.field public static final forgot_password_submit:I = 0x7f0b01a1

.field public static final forgot_password_wait_message:I = 0x7f0b01a4

.field public static final gallery_delete:I = 0x7f0b016b

.field public static final gallery_deleting_photo:I = 0x7f0b016d

.field public static final gallery_export:I = 0x7f0b016c

.field public static final gallery_favorite:I = 0x7f0b0169

.field public static final gallery_picker_empty:I = 0x7f0b013b

.field public static final gallery_picker_sep_camera_roll:I = 0x7f0b0139

.field public static final gallery_picker_sep_sd_card:I = 0x7f0b013a

.field public static final gallery_share:I = 0x7f0b016a

.field public static final help_camera_quota:I = 0x7f0b0014

.field public static final help_favorites:I = 0x7f0b0012

.field public static final help_title:I = 0x7f0b015d

.field public static final help_two_factor:I = 0x7f0b0015

.field public static final help_uploads:I = 0x7f0b0013

.field public static final help_web_site:I = 0x7f0b000b

.field public static final intro_video_url:I = 0x7f0b000c

.field public static final intro_video_url_large:I = 0x7f0b000d

.field public static final localpicker_internal_storage:I = 0x7f0b00d2

.field public static final localpicker_sdcard_storage:I = 0x7f0b00d3

.field public static final localpicker_select_folder_button:I = 0x7f0b00cf

.field public static final localpicker_storage:I = 0x7f0b00d1

.field public static final localpicker_upload_button:I = 0x7f0b00d0

.field public static final lock_code_change_new_prompt:I = 0x7f0b0154

.field public static final lock_code_change_new_reenter_prompt:I = 0x7f0b0155

.field public static final lock_code_change_old_prompt:I = 0x7f0b0153

.field public static final lock_code_error:I = 0x7f0b0156

.field public static final lock_code_error_different:I = 0x7f0b0159

.field public static final lock_code_error_last_try:I = 0x7f0b0157

.field public static final lock_code_error_unlinking:I = 0x7f0b0158

.field public static final lock_code_lockout_plur:I = 0x7f0b015b

.field public static final lock_code_lockout_sing:I = 0x7f0b015c

.field public static final lock_code_new_prompt:I = 0x7f0b0150

.field public static final lock_code_new_reenter_prompt:I = 0x7f0b0151

.field public static final lock_code_remove_erase_on_failure_prompt:I = 0x7f0b0152

.field public static final lock_code_settings_change:I = 0x7f0b014c

.field public static final lock_code_settings_erase_data:I = 0x7f0b014d

.field public static final lock_code_settings_erase_data_prompt:I = 0x7f0b014e

.field public static final lock_code_settings_title:I = 0x7f0b0149

.field public static final lock_code_settings_toggle_off:I = 0x7f0b014b

.field public static final lock_code_settings_toggle_on:I = 0x7f0b014a

.field public static final lock_code_success:I = 0x7f0b015a

.field public static final lock_code_unlock:I = 0x7f0b014f

.field public static final login_email_prompt:I = 0x7f0b0020

.field public static final login_password_prompt:I = 0x7f0b0021

.field public static final login_title:I = 0x7f0b001f

.field public static final login_to_dropbox:I = 0x7f0b0022

.field public static final login_wait_message:I = 0x7f0b0023

.field public static final menu_add_file:I = 0x7f0b0089

.field public static final menu_help:I = 0x7f0b0084

.field public static final menu_new_folder:I = 0x7f0b0087

.field public static final menu_new_text_file:I = 0x7f0b0088

.field public static final menu_refresh:I = 0x7f0b0085

.field public static final menu_search:I = 0x7f0b0086

.field public static final menu_settings:I = 0x7f0b0083

.field public static final my_dropbox_name:I = 0x7f0b001a

.field public static final new_account_first_name_prompt:I = 0x7f0b0026

.field public static final new_account_last_name_prompt:I = 0x7f0b0027

.field public static final new_account_register:I = 0x7f0b0028

.field public static final new_account_title:I = 0x7f0b0025

.field public static final new_account_wait_message:I = 0x7f0b0035

.field public static final new_file_name_dialog_message:I = 0x7f0b0181

.field public static final new_file_name_dialog_title:I = 0x7f0b0180

.field public static final new_file_name_hint:I = 0x7f0b0182

.field public static final new_folder_confirm:I = 0x7f0b009c

.field public static final new_folder_dialog_title:I = 0x7f0b009b

.field public static final new_folder_not_created_exists:I = 0x7f0b009f

.field public static final new_folder_not_created_network_error:I = 0x7f0b009e

.field public static final new_folder_not_created_unknown_error:I = 0x7f0b00a0

.field public static final new_folder_not_created_unknown_error_local:I = 0x7f0b00a1

.field public static final new_folder_progress:I = 0x7f0b009d

.field public static final new_to_dropbox:I = 0x7f0b001d

.field public static final new_to_dropbox_tblt:I = 0x7f0b001e

.field public static final new_twofactor_code_sent:I = 0x7f0b01ac

.field public static final no_sdcard_dialog_title:I = 0x7f0b0036

.field public static final noviewer_dialog_message:I = 0x7f0b00b2

.field public static final noviewer_dialog_title:I = 0x7f0b00b1

.field public static final ok:I = 0x7f0b0019

.field public static final password_reset_sent:I = 0x7f0b01a3

.field public static final photo_tab_sep_last_week_v2:I = 0x7f0b0137

.field public static final photo_tab_sep_this_month_v2:I = 0x7f0b0138

.field public static final photo_tab_sep_this_week_v2:I = 0x7f0b0136

.field public static final photo_tab_sep_today:I = 0x7f0b0134

.field public static final photo_tab_sep_yesterday:I = 0x7f0b0135

.field public static final picker_multiple_selected:I = 0x7f0b00d6

.field public static final picker_one_selected:I = 0x7f0b00d5

.field public static final picker_zero_selected:I = 0x7f0b00d4

.field public static final privacy_url:I = 0x7f0b0011

.field public static final quickaction_cancel:I = 0x7f0b0175

.field public static final quickaction_clear:I = 0x7f0b0176

.field public static final quickaction_delete:I = 0x7f0b0172

.field public static final quickaction_export:I = 0x7f0b0173

.field public static final quickaction_favorite:I = 0x7f0b016f

.field public static final quickaction_more:I = 0x7f0b016e

.field public static final quickaction_rename:I = 0x7f0b0171

.field public static final quickaction_share:I = 0x7f0b0170

.field public static final quickaction_view_in_folder:I = 0x7f0b0174

.field public static final rename_conflict_dialog_message:I = 0x7f0b00b4

.field public static final rename_conflict_dialog_title:I = 0x7f0b00b3

.field public static final rename_file_confirm:I = 0x7f0b00a3

.field public static final rename_file_dialog_title:I = 0x7f0b00a2

.field public static final rename_file_error:I = 0x7f0b00a4

.field public static final rename_folder_confirm:I = 0x7f0b00a6

.field public static final rename_folder_dialog_title:I = 0x7f0b00a5

.field public static final rename_folder_error:I = 0x7f0b00a7

.field public static final rename_overwrite:I = 0x7f0b00b5

.field public static final resend_twofactor_code_wait_message:I = 0x7f0b01ab

.field public static final search_hint:I = 0x7f0b0167

.field public static final search_label:I = 0x7f0b0168

.field public static final search_name:I = 0x7f0b001b

.field public static final search_prompt:I = 0x7f0b0165

.field public static final search_settings_description:I = 0x7f0b0166

.field public static final server_locale:I = 0x7f0b000a

.field public static final settings_about_title:I = 0x7f0b00ec

.field public static final settings_account_title:I = 0x7f0b00e6

.field public static final settings_advanced_title:I = 0x7f0b00f4

.field public static final settings_cache_prompt:I = 0x7f0b00f5

.field public static final settings_cache_size:I = 0x7f0b00f6

.field public static final settings_clear_cache:I = 0x7f0b00f7

.field public static final settings_legal:I = 0x7f0b00f0

.field public static final settings_lock_code:I = 0x7f0b00ea

.field public static final settings_name_prompt:I = 0x7f0b00e7

.field public static final settings_privacy:I = 0x7f0b00f2

.field public static final settings_send_feedback:I = 0x7f0b00f3

.field public static final settings_send_feedback_address:I = 0x7f0b000e

.field public static final settings_send_feedback_dialog_send:I = 0x7f0b013f

.field public static final settings_send_feedback_dialog_title:I = 0x7f0b013e

.field public static final settings_send_feedback_error:I = 0x7f0b0142

.field public static final settings_send_feedback_progress:I = 0x7f0b0140

.field public static final settings_send_feedback_success:I = 0x7f0b0141

.field public static final settings_send_log_address:I = 0x7f0b000f

.field public static final settings_send_log_body:I = 0x7f0b0144

.field public static final settings_send_log_subject:I = 0x7f0b0143

.field public static final settings_space_format:I = 0x7f0b00e9

.field public static final settings_space_prompt:I = 0x7f0b00e8

.field public static final settings_tell_friends:I = 0x7f0b00ef

.field public static final settings_tell_friends_email_body:I = 0x7f0b013d

.field public static final settings_tell_friends_subject:I = 0x7f0b013c

.field public static final settings_title:I = 0x7f0b00e5

.field public static final settings_tos:I = 0x7f0b00f1

.field public static final settings_unlink_device:I = 0x7f0b00eb

.field public static final settings_unlink_dialog_cancel:I = 0x7f0b0148

.field public static final settings_unlink_dialog_message:I = 0x7f0b0146

.field public static final settings_unlink_dialog_title:I = 0x7f0b0145

.field public static final settings_unlink_dialog_unlink:I = 0x7f0b0147

.field public static final settings_version_prompt:I = 0x7f0b00ed

.field public static final settings_watch_video:I = 0x7f0b00ee

.field public static final share_dialog_how_share_file:I = 0x7f0b008d

.field public static final share_dialog_how_share_folder:I = 0x7f0b008e

.field public static final sharing_dialog_message:I = 0x7f0b00a8

.field public static final sharing_link_error:I = 0x7f0b0090

.field public static final status_deleting:I = 0x7f0b004c

.field public static final status_downloading:I = 0x7f0b0052

.field public static final status_downloading_progress:I = 0x7f0b0054

.field public static final status_downloading_waiting:I = 0x7f0b0053

.field public static final status_exporting_progress:I = 0x7f0b0056

.field public static final status_exporting_waiting:I = 0x7f0b0055

.field public static final status_out_of_date:I = 0x7f0b0057

.field public static final status_renaming:I = 0x7f0b004d

.field public static final status_uploading_progress:I = 0x7f0b004f

.field public static final status_uploading_quota_insufficient:I = 0x7f0b0050

.field public static final status_uploading_waiting:I = 0x7f0b004e

.field public static final status_waiting_for_connection:I = 0x7f0b0051

.field public static final stream_dialog_how_play:I = 0x7f0b008b

.field public static final streaming_dialog_message:I = 0x7f0b00a9

.field public static final streaming_no_connection:I = 0x7f0b00aa

.field public static final tab_browser_name:I = 0x7f0b0037

.field public static final tab_favorites_name:I = 0x7f0b0038

.field public static final tab_uploads_name:I = 0x7f0b0039

.field public static final task_status_canceled:I = 0x7f0b007f

.field public static final task_status_canceled_to_retry:I = 0x7f0b0080

.field public static final task_status_failure:I = 0x7f0b0081

.field public static final task_status_forbidden:I = 0x7f0b0082

.field public static final task_status_memory_error:I = 0x7f0b007d

.field public static final task_status_network_error:I = 0x7f0b007b

.field public static final task_status_none:I = 0x7f0b0079

.field public static final task_status_storage_error:I = 0x7f0b007c

.field public static final task_status_success:I = 0x7f0b007a

.field public static final task_status_temp_server_error:I = 0x7f0b007e

.field public static final text_edit_quit:I = 0x7f0b0179

.field public static final text_edit_save:I = 0x7f0b0178

.field public static final text_editor_app_name:I = 0x7f0b0177

.field public static final text_editor_default_title:I = 0x7f0b017f

.field public static final text_editor_error_opening:I = 0x7f0b0183

.field public static final text_editor_error_opening_security:I = 0x7f0b0187

.field public static final text_editor_error_saving:I = 0x7f0b0185

.field public static final text_editor_error_too_large:I = 0x7f0b0184

.field public static final text_editor_file_already_exists:I = 0x7f0b0186

.field public static final text_editor_quit_dialog_message:I = 0x7f0b017c

.field public static final text_editor_quit_dialog_title:I = 0x7f0b017b

.field public static final text_editor_quit_discard:I = 0x7f0b017e

.field public static final text_editor_quit_save:I = 0x7f0b017d

.field public static final text_editor_saved:I = 0x7f0b017a

.field public static final thumbnail_load_failed:I = 0x7f0b0078

.field public static final time_failed_description:I = 0x7f0b005d

.field public static final time_interval_day:I = 0x7f0b0066

.field public static final time_interval_days:I = 0x7f0b0065

.field public static final time_interval_hour:I = 0x7f0b0068

.field public static final time_interval_hours:I = 0x7f0b0067

.field public static final time_interval_minute:I = 0x7f0b006a

.field public static final time_interval_minutes:I = 0x7f0b0069

.field public static final time_interval_month:I = 0x7f0b0062

.field public static final time_interval_months:I = 0x7f0b0061

.field public static final time_interval_second:I = 0x7f0b006c

.field public static final time_interval_seconds:I = 0x7f0b006b

.field public static final time_interval_week:I = 0x7f0b0064

.field public static final time_interval_weeks:I = 0x7f0b0063

.field public static final time_interval_year:I = 0x7f0b0060

.field public static final time_interval_years:I = 0x7f0b005f

.field public static final time_modified_description:I = 0x7f0b005b

.field public static final time_uploaded_description:I = 0x7f0b005c

.field public static final tos_dialog_accept:I = 0x7f0b0032

.field public static final tos_dialog_back:I = 0x7f0b0033

.field public static final tos_dialog_message:I = 0x7f0b0030

.field public static final tos_dialog_title:I = 0x7f0b002f

.field public static final tos_dialog_view:I = 0x7f0b0031

.field public static final tos_title:I = 0x7f0b0034

.field public static final tos_url:I = 0x7f0b0010

.field public static final tour_back:I = 0x7f0b018d

.field public static final tour_done:I = 0x7f0b018c

.field public static final tour_next:I = 0x7f0b018e

.field public static final tour_page_1:I = 0x7f0b0188

.field public static final tour_page_2:I = 0x7f0b0189

.field public static final tour_page_3:I = 0x7f0b018a

.field public static final tour_page_4:I = 0x7f0b018b

.field public static final unit_byte:I = 0x7f0b006e

.field public static final unit_bytes:I = 0x7f0b006f

.field public static final unit_eb:I = 0x7f0b0075

.field public static final unit_format:I = 0x7f0b0076

.field public static final unit_gb:I = 0x7f0b0072

.field public static final unit_kb:I = 0x7f0b0070

.field public static final unit_mb:I = 0x7f0b0071

.field public static final unit_pb:I = 0x7f0b0074

.field public static final unit_tb:I = 0x7f0b0073

.field public static final unit_zero_bytes:I = 0x7f0b006d

.field public static final update_button_later:I = 0x7f0b019c

.field public static final update_button_update:I = 0x7f0b019a

.field public static final update_button_update_now:I = 0x7f0b019b

.field public static final update_optional_dialog_message:I = 0x7f0b0198

.field public static final update_optional_dialog_title:I = 0x7f0b0196

.field public static final update_required_dialog_message:I = 0x7f0b0199

.field public static final update_required_dialog_title:I = 0x7f0b0197

.field public static final upload_button_media:I = 0x7f0b00c4

.field public static final upload_button_other:I = 0x7f0b00c5

.field public static final upload_empty_head:I = 0x7f0b00c2

.field public static final upload_empty_text:I = 0x7f0b00c3

.field public static final upload_error_deleted_msg_v3:I = 0x7f0b00ca

.field public static final upload_error_generic_msg_v3:I = 0x7f0b00c7

.field public static final upload_error_generic_title:I = 0x7f0b00c6

.field public static final upload_error_perms_msg_v3:I = 0x7f0b00c9

.field public static final upload_error_server_msg_v3:I = 0x7f0b00c8

.field public static final upload_failed_notification_body:I = 0x7f0b01c8

.field public static final upload_failed_notification_title_v2:I = 0x7f0b01c7

.field public static final upload_failed_time:I = 0x7f0b005e

.field public static final upload_file_conflict_1_of_n_files_msg:I = 0x7f0b00db

.field public static final upload_file_conflict_1_of_n_files_overwrite_button:I = 0x7f0b00dc

.field public static final upload_file_conflict_1_of_n_files_title:I = 0x7f0b00da

.field public static final upload_file_conflict_1_of_n_files_upload_others_button:I = 0x7f0b00dd

.field public static final upload_file_conflict_all_files_msg:I = 0x7f0b00e3

.field public static final upload_file_conflict_all_files_overwrite_button:I = 0x7f0b00e4

.field public static final upload_file_conflict_all_files_title:I = 0x7f0b00e2

.field public static final upload_file_conflict_already_exists:I = 0x7f0b00d7

.field public static final upload_file_conflict_already_exists_msg:I = 0x7f0b00d8

.field public static final upload_file_conflict_overwrite:I = 0x7f0b00d9

.field public static final upload_file_conflict_x_of_n_files_msg:I = 0x7f0b00df

.field public static final upload_file_conflict_x_of_n_files_overwrite_button:I = 0x7f0b00e0

.field public static final upload_file_conflict_x_of_n_files_title:I = 0x7f0b00de

.field public static final upload_file_conflict_x_of_n_files_upload_others_button:I = 0x7f0b00e1

.field public static final upload_quota_notification_body:I = 0x7f0b01c6

.field public static final upload_quota_notification_title:I = 0x7f0b01c5

.field public static final upload_recent_uploads_header:I = 0x7f0b00c1

.field public static final upload_to_dropbox:I = 0x7f0b0161

.field public static final uploading_complete_notification_1_file:I = 0x7f0b01c3

.field public static final uploading_complete_notification_mult_files:I = 0x7f0b01c4

.field public static final uploading_complete_notification_title:I = 0x7f0b01c2

.field public static final uploading_multiple_files:I = 0x7f0b005a

.field public static final uploading_notification_1_remaining:I = 0x7f0b01c0

.field public static final uploading_notification_mult_remaining:I = 0x7f0b01c1

.field public static final uploading_notification_title:I = 0x7f0b01bf

.field public static final uploading_single_file:I = 0x7f0b0059

.field public static final verify_twofactor_code_wait_message:I = 0x7f0b01aa

.field public static final video_cant_seek_still_transcoding:I = 0x7f0b01cb

.field public static final video_content_truncated:I = 0x7f0b01cc

.field public static final web_view_app_name:I = 0x7f0b0017


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
