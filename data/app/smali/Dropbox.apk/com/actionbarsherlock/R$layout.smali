.class public final Lcom/actionbarsherlock/R$layout;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final abs__action_bar_home:I = 0x7f030000

.field public static final abs__action_bar_tab:I = 0x7f030001

.field public static final abs__action_bar_tab_bar_view:I = 0x7f030002

.field public static final abs__action_bar_title_item:I = 0x7f030003

.field public static final abs__action_menu_item_layout:I = 0x7f030004

.field public static final abs__action_menu_layout:I = 0x7f030005

.field public static final abs__action_mode_bar:I = 0x7f030006

.field public static final abs__action_mode_close_item:I = 0x7f030007

.field public static final abs__activity_chooser_view:I = 0x7f030008

.field public static final abs__activity_chooser_view_list_item:I = 0x7f030009

.field public static final abs__dialog_title_holo:I = 0x7f03000a

.field public static final abs__list_menu_item_checkbox:I = 0x7f03000b

.field public static final abs__list_menu_item_icon:I = 0x7f03000c

.field public static final abs__list_menu_item_layout:I = 0x7f03000d

.field public static final abs__list_menu_item_radio:I = 0x7f03000e

.field public static final abs__popup_menu_item_layout:I = 0x7f03000f

.field public static final abs__screen_action_bar:I = 0x7f030010

.field public static final abs__screen_action_bar_overlay:I = 0x7f030011

.field public static final abs__screen_simple:I = 0x7f030012

.field public static final abs__screen_simple_overlay_action_mode:I = 0x7f030013

.field public static final auth_screen:I = 0x7f030014

.field public static final bottom_bar_light:I = 0x7f030015

.field public static final bottom_bar_light_only_cancel:I = 0x7f030016

.field public static final bottom_buttons_dark:I = 0x7f030017

.field public static final bottom_buttons_light:I = 0x7f030018

.field public static final bottom_buttons_light_only_cancel:I = 0x7f030019

.field public static final camera_toggle:I = 0x7f03001a

.field public static final camera_upload_details:I = 0x7f03001b

.field public static final camera_upload_details_thumb:I = 0x7f03001c

.field public static final camera_upload_grid:I = 0x7f03001d

.field public static final camera_upload_settings:I = 0x7f03001e

.field public static final camera_upload_tour_page:I = 0x7f03001f

.field public static final camera_upload_turn_on_status_bar:I = 0x7f030020

.field public static final dev_settings_screen:I = 0x7f030021

.field public static final didnt_receive_twofactor_code_screen:I = 0x7f030022

.field public static final download_dialog:I = 0x7f030023

.field public static final dropbox_item_list_view:I = 0x7f030024

.field public static final enter_twofactor_code_screen:I = 0x7f030025

.field public static final favorites_screen:I = 0x7f030026

.field public static final favorites_screen_empty_inner:I = 0x7f030027

.field public static final file_chooser_dialog:I = 0x7f030028

.field public static final file_chooser_dialog_only_cancel:I = 0x7f030029

.field public static final filelist_screen:I = 0x7f03002a

.field public static final forgot_password_screen:I = 0x7f03002b

.field public static final frag_container:I = 0x7f03002c

.field public static final gallery_item:I = 0x7f03002d

.field public static final gallery_picker:I = 0x7f03002e

.field public static final gallery_screen:I = 0x7f03002f

.field public static final generic_tour:I = 0x7f030030

.field public static final help_screen:I = 0x7f030031

.field public static final intent_picker_dialog:I = 0x7f030032

.field public static final intent_picker_list_item:I = 0x7f030033

.field public static final item_base_name:I = 0x7f030034

.field public static final item_base_name_and_status:I = 0x7f030035

.field public static final item_base_progress:I = 0x7f030036

.field public static final item_base_quickaction_button:I = 0x7f030037

.field public static final item_base_status:I = 0x7f030038

.field public static final item_base_thumbnail:I = 0x7f030039

.field public static final item_base_thumbnail_and_icon:I = 0x7f03003a

.field public static final item_camera_upload:I = 0x7f03003b

.field public static final item_dropbox_entry:I = 0x7f03003c

.field public static final item_in_progress_upload:I = 0x7f03003d

.field public static final item_separator_dark:I = 0x7f03003e

.field public static final item_separator_light:I = 0x7f03003f

.field public static final item_up_folder:I = 0x7f030040

.field public static final local_file_browser_list:I = 0x7f030041

.field public static final localitem_listitem:I = 0x7f030042

.field public static final lock_code_screen:I = 0x7f030043

.field public static final login_frag_container:I = 0x7f030044

.field public static final login_or_newacct_screen:I = 0x7f030045

.field public static final login_screen:I = 0x7f030046

.field public static final media_controller:I = 0x7f030047

.field public static final new_account_screen:I = 0x7f030048

.field public static final new_folder_dialog:I = 0x7f030049

.field public static final notification_progress:I = 0x7f03004a

.field public static final picker_bottom_panel_dark:I = 0x7f03004b

.field public static final picker_bottom_panel_light:I = 0x7f03004c

.field public static final pin_keyboard:I = 0x7f03004d

.field public static final quickaction_bar:I = 0x7f03004e

.field public static final quickaction_button_cancel:I = 0x7f03004f

.field public static final quickaction_button_clear:I = 0x7f030050

.field public static final quickaction_button_delete:I = 0x7f030051

.field public static final quickaction_button_export:I = 0x7f030052

.field public static final quickaction_button_favorite:I = 0x7f030053

.field public static final quickaction_button_more:I = 0x7f030054

.field public static final quickaction_button_rename:I = 0x7f030055

.field public static final quickaction_button_share:I = 0x7f030056

.field public static final quickaction_button_view_in_folder:I = 0x7f030057

.field public static final send_feedback_dialog:I = 0x7f030058

.field public static final sherlock_spinner_dropdown_item:I = 0x7f030059

.field public static final sherlock_spinner_item:I = 0x7f03005a

.field public static final tablet_auth_screen:I = 0x7f03005b

.field public static final tablet_camera_upload_details:I = 0x7f03005c

.field public static final tablet_gallery_screen:I = 0x7f03005d

.field public static final tablet_generic_tour:I = 0x7f03005e

.field public static final tablet_lock_code_screen:I = 0x7f03005f

.field public static final tablet_login_frag_container:I = 0x7f030060

.field public static final text_editor:I = 0x7f030061

.field public static final thumb_grid_item:I = 0x7f030062

.field public static final uploads_screen:I = 0x7f030063

.field public static final uploads_screen_empty_inner:I = 0x7f030064

.field public static final video_icon_details:I = 0x7f030065

.field public static final video_player:I = 0x7f030066

.field public static final web_view_with_buttons:I = 0x7f030067


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
