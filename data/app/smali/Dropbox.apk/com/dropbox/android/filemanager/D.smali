.class final Lcom/dropbox/android/filemanager/D;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/V;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/x;

.field private b:I

.field private c:I

.field private final d:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Lcom/dropbox/android/filemanager/x;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 940
    iput-object p1, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 945
    iput v0, p0, Lcom/dropbox/android/filemanager/D;->b:I

    .line 948
    iput v0, p0, Lcom/dropbox/android/filemanager/D;->c:I

    .line 1015
    new-instance v0, Lcom/dropbox/android/filemanager/E;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/E;-><init>(Lcom/dropbox/android/filemanager/D;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/D;->d:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/filemanager/x;Lcom/dropbox/android/filemanager/y;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 940
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/D;-><init>(Lcom/dropbox/android/filemanager/x;)V

    return-void
.end method

.method private declared-synchronized b()V
    .registers 5

    .prologue
    .line 1023
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/x;->d(Lcom/dropbox/android/filemanager/x;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/D;->d:Ljava/lang/Runnable;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 1024
    monitor-exit p0

    return-void

    .line 1023
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/dropbox/android/taskqueue/H;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 977
    monitor-enter p0

    :try_start_3
    const-class v2, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    invoke-virtual {p1, v2}, Lcom/dropbox/android/taskqueue/H;->b(Ljava/lang/Class;)I

    move-result v2

    .line 978
    iget v3, p0, Lcom/dropbox/android/filemanager/D;->b:I

    if-eq v2, v3, :cond_60

    .line 980
    iget v3, p0, Lcom/dropbox/android/filemanager/D;->b:I

    if-le v2, v3, :cond_16

    .line 983
    sget-object v3, Lcom/dropbox/android/util/aC;->g:Lcom/dropbox/android/util/aC;

    invoke-static {v3}, Lcom/dropbox/android/service/NotificationService;->a(Lcom/dropbox/android/util/aC;)V

    .line 985
    :cond_16
    iput v2, p0, Lcom/dropbox/android/filemanager/D;->b:I

    .line 986
    iget v2, p0, Lcom/dropbox/android/filemanager/D;->b:I

    if-gtz v2, :cond_62

    .line 987
    :goto_1c
    if-eqz v0, :cond_64

    .line 990
    iget-object v0, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/x;->c(Lcom/dropbox/android/filemanager/x;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aC;->g:Lcom/dropbox/android/util/aC;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 991
    sget-object v0, Lcom/dropbox/android/util/aC;->g:Lcom/dropbox/android/util/aC;

    invoke-static {v0}, Lcom/dropbox/android/service/NotificationService;->a(Lcom/dropbox/android/util/aC;)V

    .line 994
    iget-object v0, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/x;->c(Lcom/dropbox/android/filemanager/x;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/aC;->c:Lcom/dropbox/android/util/aC;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 995
    iget v0, p0, Lcom/dropbox/android/filemanager/D;->c:I

    if-lez v0, :cond_60

    .line 998
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 999
    const-string v1, "ARG_NUM_UPLOADS"

    iget v2, p0, Lcom/dropbox/android/filemanager/D;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1000
    const/4 v1, 0x0

    iput v1, p0, Lcom/dropbox/android/filemanager/D;->c:I

    .line 1001
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/D;->c()V

    .line 1002
    iget-object v1, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/x;->c(Lcom/dropbox/android/filemanager/x;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aC;->h:Lcom/dropbox/android/util/aC;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v0, v3, v4}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z

    .line 1003
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/D;->b()V
    :try_end_60
    .catchall {:try_start_3 .. :try_end_60} :catchall_8a

    .line 1013
    :cond_60
    :goto_60
    monitor-exit p0

    return-void

    :cond_62
    move v0, v1

    .line 986
    goto :goto_1c

    .line 1007
    :cond_64
    :try_start_64
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 1008
    const-string v1, "ARG_NUM_UPLOADS"

    iget v2, p0, Lcom/dropbox/android/filemanager/D;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1009
    iget-object v1, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/x;->c(Lcom/dropbox/android/filemanager/x;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aC;->h:Lcom/dropbox/android/util/aC;

    invoke-static {v1, v2}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 1010
    iget-object v1, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-static {v1}, Lcom/dropbox/android/filemanager/x;->c(Lcom/dropbox/android/filemanager/x;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/util/aC;->g:Lcom/dropbox/android/util/aC;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-static {v1, v2, v0, v3, v4}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z
    :try_end_89
    .catchall {:try_start_64 .. :try_end_89} :catchall_8a

    goto :goto_60

    .line 977
    :catchall_8a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .registers 3

    .prologue
    .line 1027
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/x;->d(Lcom/dropbox/android/filemanager/x;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/D;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    .line 1028
    monitor-exit p0

    return-void

    .line 1027
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 2

    .prologue
    .line 951
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lcom/dropbox/android/filemanager/D;->b:I

    .line 952
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/filemanager/D;->c:I
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    .line 953
    monitor-exit p0

    return-void

    .line 951
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/dropbox/android/taskqueue/H;)V
    .registers 2
    .parameter

    .prologue
    .line 960
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/D;->b(Lcom/dropbox/android/taskqueue/H;)V

    .line 961
    invoke-static {p1}, Lcom/dropbox/android/util/N;->a(Lcom/dropbox/android/taskqueue/H;)V

    .line 962
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/H;Lcom/dropbox/android/taskqueue/k;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 956
    return-void
.end method

.method public final declared-synchronized b(Lcom/dropbox/android/taskqueue/H;Lcom/dropbox/android/taskqueue/k;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 966
    monitor-enter p0

    :try_start_1
    instance-of v0, p2, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    if-eqz v0, :cond_1b

    .line 967
    iget v0, p0, Lcom/dropbox/android/filemanager/D;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/D;->c:I

    .line 968
    iget-object v0, p0, Lcom/dropbox/android/filemanager/D;->a:Lcom/dropbox/android/filemanager/x;

    check-cast p2, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->t()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/filemanager/x;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/util/DropboxPath;

    .line 969
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/D;->b(Lcom/dropbox/android/taskqueue/H;)V
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_2a

    .line 974
    :cond_19
    :goto_19
    monitor-exit p0

    return-void

    .line 970
    :cond_1b
    :try_start_1b
    instance-of v0, p2, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    if-eqz v0, :cond_19

    .line 971
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->m()V

    .line 972
    invoke-static {p1}, Lcom/dropbox/android/util/N;->a(Lcom/dropbox/android/taskqueue/H;)V
    :try_end_29
    .catchall {:try_start_1b .. :try_end_29} :catchall_2a

    goto :goto_19

    .line 966
    :catchall_2a
    move-exception v0

    monitor-exit p0

    throw v0
.end method
