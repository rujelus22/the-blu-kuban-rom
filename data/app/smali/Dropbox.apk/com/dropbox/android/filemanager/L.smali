.class final Lcom/dropbox/android/filemanager/L;
.super Lcom/dropbox/android/filemanager/S;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/R;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 764
    const/4 v0, 0x0

    invoke-direct {p0, p6, p4, p5, v0}, Lcom/dropbox/android/filemanager/S;-><init>(Lcom/dropbox/android/filemanager/R;IILcom/dropbox/android/filemanager/J;)V

    .line 765
    iput-object p1, p0, Lcom/dropbox/android/filemanager/L;->a:Ljava/lang/String;

    .line 766
    iput-object p2, p0, Lcom/dropbox/android/filemanager/L;->e:Ljava/lang/String;

    .line 767
    iput-boolean p3, p0, Lcom/dropbox/android/filemanager/L;->f:Z

    .line 768
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/T;
    .registers 12
    .parameter

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    .line 773
    iget-object v0, p0, Lcom/dropbox/android/filemanager/L;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->c(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 774
    if-eqz v0, :cond_64

    .line 775
    iget v1, p0, Lcom/dropbox/android/filemanager/L;->d:I

    invoke-static {v0, v1, p1}, Lcom/dropbox/android/filemanager/I;->a(Landroid/graphics/Bitmap;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 776
    if-eq v3, v0, :cond_37

    .line 777
    iget-object v0, p0, Lcom/dropbox/android/filemanager/L;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 778
    iget-object v0, p0, Lcom/dropbox/android/filemanager/L;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 779
    invoke-static {v2}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/O;

    move-result-object v4

    .line 780
    invoke-virtual {v4}, Lcom/dropbox/android/filemanager/O;->c()Landroid/net/Uri;

    move-result-object v0

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/L;->f:Z

    invoke-static {v4, p1, v2}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/filemanager/L;->d:I

    iget-object v6, p0, Lcom/dropbox/android/filemanager/L;->a:Ljava/lang/String;

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    .line 789
    :cond_37
    :goto_37
    new-instance v0, Lcom/dropbox/android/filemanager/T;

    invoke-direct {v0, v3, v7, v8, v9}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/graphics/Bitmap;ZJ)V

    .line 818
    :goto_3c
    return-object v0

    .line 782
    :cond_3d
    iget-object v0, p0, Lcom/dropbox/android/filemanager/L;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 783
    invoke-static {p1, v0}, Lcom/dropbox/android/filemanager/I;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 784
    if-eqz v2, :cond_37

    .line 785
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/O;->c()Landroid/net/Uri;

    move-result-object v0

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/L;->f:Z

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/filemanager/L;->d:I

    iget-object v6, p0, Lcom/dropbox/android/filemanager/L;->a:Ljava/lang/String;

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    goto :goto_37

    .line 792
    :cond_64
    iget-object v0, p0, Lcom/dropbox/android/filemanager/L;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 793
    iget-object v0, p0, Lcom/dropbox/android/filemanager/L;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 794
    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/O;

    move-result-object v1

    .line 795
    if-eqz v1, :cond_bb

    .line 796
    invoke-static {v1, p1, v0}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    .line 797
    if-lez v0, :cond_bb

    .line 798
    iget v2, p0, Lcom/dropbox/android/filemanager/L;->d:I

    invoke-virtual {v1, p1, v0, v2}, Lcom/dropbox/android/filemanager/O;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 799
    if-eqz v1, :cond_bb

    .line 800
    new-instance v0, Lcom/dropbox/android/filemanager/T;

    invoke-direct {v0, v1, v7, v8, v9}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/graphics/Bitmap;ZJ)V

    goto :goto_3c

    .line 805
    :cond_8c
    iget-object v0, p0, Lcom/dropbox/android/filemanager/L;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 806
    invoke-static {p1, v0}, Lcom/dropbox/android/filemanager/I;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 808
    if-eqz v1, :cond_b3

    .line 809
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/dropbox/android/filemanager/L;->d:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/dropbox/android/filemanager/O;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 813
    :goto_ab
    if-eqz v1, :cond_bb

    .line 814
    new-instance v0, Lcom/dropbox/android/filemanager/T;

    invoke-direct {v0, v1, v7, v8, v9}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/graphics/Bitmap;ZJ)V

    goto :goto_3c

    .line 811
    :cond_b3
    iget v1, p0, Lcom/dropbox/android/filemanager/L;->d:I

    invoke-static {v0, v7, v7, v1}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;ZZI)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    goto :goto_ab

    .line 818
    :cond_bb
    const/4 v0, 0x0

    goto :goto_3c
.end method
