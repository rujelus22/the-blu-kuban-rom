.class public final Lcom/dropbox/android/filemanager/I;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Lcom/dropbox/android/filemanager/I;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/filemanager/I;->a:Lcom/dropbox/android/filemanager/I;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->c:Ljava/util/LinkedList;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/dropbox/android/filemanager/U;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/U;-><init>(Lcom/dropbox/android/filemanager/I;)V

    .line 52
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/U;->setPriority(I)V

    .line 53
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/U;->start()V

    .line 54
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;ILandroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const-wide/high16 v11, 0x3ff8

    const/4 v10, 0x0

    .line 906
    const/4 v0, 0x3

    if-ne p1, v0, :cond_51

    .line 907
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 908
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 914
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090030

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 916
    int-to-double v6, v1

    int-to-double v8, v5

    mul-double/2addr v8, v11

    cmpl-double v0, v6, v8

    if-gtz v0, :cond_28

    int-to-double v6, v4

    int-to-double v8, v5

    mul-double/2addr v8, v11

    cmpl-double v0, v6, v8

    if-lez v0, :cond_51

    .line 921
    :cond_28
    if-le v1, v4, :cond_52

    .line 922
    sub-int v0, v1, v4

    div-int/lit8 v1, v0, 0x2

    move v3, v4

    .line 929
    :goto_2f
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v6, v3

    int-to-float v7, v4

    invoke-direct {v0, v10, v10, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 930
    new-instance v6, Landroid/graphics/RectF;

    int-to-float v7, v5

    int-to-float v5, v5

    invoke-direct {v6, v10, v10, v7, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 931
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 932
    sget-object v7, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v0, v6, v7}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 934
    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 935
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    move-object p0, v0

    .line 939
    :cond_51
    return-object p0

    .line 925
    :cond_52
    sub-int v0, v4, v1

    div-int/lit8 v0, v0, 0x2

    move v4, v1

    move v3, v1

    move v1, v2

    move v2, v0

    .line 926
    goto :goto_2f
.end method

.method public static a(Landroid/net/Uri;ZLjava/lang/String;ILandroid/content/Context;I)Landroid/graphics/Bitmap;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 832
    invoke-static {p2}, Lcom/dropbox/android/filemanager/I;->c(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 833
    const/4 v3, 0x0

    .line 834
    if-eqz v0, :cond_16

    .line 835
    invoke-static {v0, p3, p4}, Lcom/dropbox/android/filemanager/I;->a(Landroid/graphics/Bitmap;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 836
    if-eq v3, v0, :cond_16

    move-object v0, p0

    move v1, p1

    move-object v2, p4

    move v4, p5

    move v5, p3

    move-object v6, p2

    .line 837
    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/I;->b(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    .line 840
    :cond_16
    return-object v3
.end method

.method protected static a(Ljava/lang/String;ZZI)Landroid/graphics/Bitmap;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 243
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v0, v3, :cond_94

    .line 244
    if-nez p1, :cond_ba

    .line 248
    :try_start_a
    const-string v0, "android.media.MediaFile"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 249
    const-string v0, "getFileType"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 250
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 251
    if-eqz v0, :cond_b8

    .line 252
    const-string v4, "android.media.MediaFile$MediaFileType"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 253
    const-string v5, "fileType"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 254
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 255
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 256
    const-string v4, "isVideoFileType"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 257
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_5f
    .catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_5f} :catch_b6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_a .. :try_end_5f} :catch_b4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_5f} :catch_b2
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_5f} :catch_b0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_5f} :catch_ae
    .catch Ljava/lang/ClassNotFoundException; {:try_start_a .. :try_end_5f} :catch_ac
    .catch Ljava/lang/NoSuchFieldException; {:try_start_a .. :try_end_5f} :catch_aa

    move-result v0

    :goto_60
    move v1, v0

    .line 270
    :goto_61
    if-eqz v1, :cond_68

    .line 271
    :try_start_63
    invoke-static {p0, p3}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 294
    :goto_67
    return-object v0

    .line 274
    :cond_68
    const-class v0, Landroid/media/ThumbnailUtils;

    const-string v1, "createImageThumbnail"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 276
    const/4 v1, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_a0

    .line 278
    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_93
    .catch Ljava/lang/SecurityException; {:try_start_63 .. :try_end_93} :catch_a8
    .catch Ljava/lang/NoSuchMethodException; {:try_start_63 .. :try_end_93} :catch_a6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_63 .. :try_end_93} :catch_a4
    .catch Ljava/lang/IllegalAccessException; {:try_start_63 .. :try_end_93} :catch_a2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_63 .. :try_end_93} :catch_9f

    goto :goto_67

    .line 288
    :cond_94
    if-eqz p1, :cond_9a

    if-eqz p2, :cond_9a

    move-object v0, v2

    .line 289
    goto :goto_67

    .line 291
    :cond_9a
    invoke-static {p0, p3}, Lcom/dropbox/android/util/aW;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_67

    .line 285
    :catch_9f
    move-exception v0

    :cond_a0
    :goto_a0
    move-object v0, v2

    .line 294
    goto :goto_67

    .line 284
    :catch_a2
    move-exception v0

    goto :goto_a0

    .line 283
    :catch_a4
    move-exception v0

    goto :goto_a0

    .line 282
    :catch_a6
    move-exception v0

    goto :goto_a0

    .line 281
    :catch_a8
    move-exception v0

    goto :goto_a0

    .line 265
    :catch_aa
    move-exception v0

    goto :goto_61

    .line 264
    :catch_ac
    move-exception v0

    goto :goto_61

    .line 263
    :catch_ae
    move-exception v0

    goto :goto_61

    .line 262
    :catch_b0
    move-exception v0

    goto :goto_61

    .line 261
    :catch_b2
    move-exception v0

    goto :goto_61

    .line 260
    :catch_b4
    move-exception v0

    goto :goto_61

    .line 259
    :catch_b6
    move-exception v0

    goto :goto_61

    :cond_b8
    move v0, v1

    goto :goto_60

    :cond_ba
    move v1, p2

    goto :goto_61
.end method

.method protected static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 334
    invoke-static {p1}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/O;

    move-result-object v1

    .line 335
    if-eqz v1, :cond_16

    .line 336
    invoke-static {v1, p0, p1}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v2

    .line 337
    if-lez v2, :cond_16

    .line 338
    new-instance v0, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 341
    :goto_15
    return-object v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 323
    .line 324
    invoke-static {}, Lcom/dropbox/android/filemanager/O;->a()[Lcom/dropbox/android/filemanager/O;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_1d

    aget-object v3, v1, v0

    .line 325
    invoke-static {v3, p0, p1}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    .line 326
    if-lez v4, :cond_1a

    .line 327
    new-instance v0, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 330
    :goto_19
    return-object v0

    .line 324
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 330
    :cond_1d
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public static a()Lcom/dropbox/android/filemanager/I;
    .registers 1

    .prologue
    .line 65
    sget-object v0, Lcom/dropbox/android/filemanager/I;->a:Lcom/dropbox/android/filemanager/I;

    if-nez v0, :cond_a

    .line 66
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 68
    :cond_a
    sget-object v0, Lcom/dropbox/android/filemanager/I;->a:Lcom/dropbox/android/filemanager/I;

    return-object v0
.end method

.method protected static a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/O;
    .registers 7
    .parameter

    .prologue
    .line 302
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 303
    invoke-static {}, Lcom/dropbox/android/filemanager/O;->a()[Lcom/dropbox/android/filemanager/O;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_f
    if-ge v1, v4, :cond_2a

    aget-object v0, v3, v1

    .line 304
    invoke-static {v0}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 308
    :goto_25
    return-object v0

    .line 303
    :cond_26
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 308
    :cond_2a
    const/4 v0, 0x0

    goto :goto_25
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/I;)Ljava/util/LinkedList;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->c:Ljava/util/LinkedList;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 57
    sget-object v0, Lcom/dropbox/android/filemanager/I;->a:Lcom/dropbox/android/filemanager/I;

    if-nez v0, :cond_c

    .line 58
    new-instance v0, Lcom/dropbox/android/filemanager/I;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/I;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/dropbox/android/filemanager/I;->a:Lcom/dropbox/android/filemanager/I;

    .line 62
    return-void

    .line 60
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method static synthetic a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-static/range {p0 .. p6}, Lcom/dropbox/android/filemanager/I;->b(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/S;)V
    .registers 4
    .parameter

    .prologue
    .line 345
    iget-object v1, p0, Lcom/dropbox/android/filemanager/I;->c:Ljava/util/LinkedList;

    monitor-enter v1

    .line 346
    :try_start_3
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 348
    monitor-exit v1

    .line 349
    return-void

    .line 348
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public static a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 72
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/I;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    return-object v0
.end method

.method protected static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 312
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 313
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 314
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 318
    :cond_2c
    return-object p0
.end method

.method private static b(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 854
    const/4 v0, 0x0

    .line 856
    invoke-static {}, Lcom/dropbox/android/util/af;->g()Ljava/io/File;

    move-result-object v1

    .line 857
    if-eqz v1, :cond_b

    .line 858
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 861
    :cond_b
    if-eqz v0, :cond_13

    invoke-virtual {p6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_27

    .line 865
    :cond_13
    invoke-static {}, Lcom/dropbox/android/util/af;->h()Ljava/io/File;

    move-result-object v1

    .line 866
    if-eqz v1, :cond_27

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 867
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 871
    :cond_27
    if-nez v0, :cond_2a

    .line 902
    :cond_29
    :goto_29
    return-void

    .line 875
    :cond_2a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/.thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 878
    :try_start_6b
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 879
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5a

    invoke-virtual {p3, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v2

    .line 880
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 881
    if-eqz v2, :cond_29

    .line 882
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 883
    if-eqz p1, :cond_c1

    .line 884
    const-string v2, "_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    const-string v0, "width"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 886
    const-string v0, "height"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 887
    const-string v0, "kind"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 888
    const-string v0, "video_id"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 897
    :goto_b5
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_29

    .line 899
    :catch_be
    move-exception v0

    goto/16 :goto_29

    .line 890
    :cond_c1
    const-string v2, "_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    const-string v0, "width"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 892
    const-string v0, "height"

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 893
    const-string v0, "kind"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 894
    const-string v0, "image_id"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_f2
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_f2} :catch_be

    goto :goto_b5
.end method

.method public static c(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 3
    .parameter

    .prologue
    .line 824
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 825
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 826
    invoke-static {}, Lcom/dropbox/android/util/aU;->a()[B

    move-result-object v1

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 827
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    new-instance v0, Lcom/dropbox/android/filemanager/L;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/filemanager/L;-><init>(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/R;)V

    .line 132
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    .line 133
    return-object v0
.end method

.method protected final a(Landroid/net/Uri;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/dropbox/android/filemanager/I;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v3

    .line 146
    if-eqz v3, :cond_3d

    .line 147
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v2, v1, p3}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 148
    if-eqz v4, :cond_3d

    .line 149
    new-instance v2, Lcom/dropbox/android/filemanager/T;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/O;->d()Z

    move-result v5

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v6, v1, p3}, Lcom/dropbox/android/filemanager/O;->b(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)J

    move-result-wide v0

    invoke-direct {v2, v4, v5, v0, v1}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/graphics/Bitmap;ZJ)V

    move-object v0, v2

    .line 160
    :goto_3c
    return-object v0

    .line 156
    :cond_3d
    if-eqz v3, :cond_56

    .line 157
    new-instance v0, Lcom/dropbox/android/filemanager/Q;

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/O;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/Q;-><init>(Lcom/dropbox/android/filemanager/O;IIILcom/dropbox/android/filemanager/R;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    .line 160
    :cond_56
    const/4 v0, 0x0

    goto :goto_3c
.end method

.method public final a(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    invoke-static {p1}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 102
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;

    move-result-object v0

    .line 104
    :goto_e
    return-object v0

    :cond_f
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/I;->c(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;

    move-result-object v0

    goto :goto_e
.end method

.method protected final a(Landroid/net/Uri;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 164
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/dropbox/android/filemanager/I;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v2

    .line 165
    if-eqz v2, :cond_2c

    .line 166
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v3, v1, p2}, Lcom/dropbox/android/filemanager/O;->c(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 167
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2c

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 176
    :cond_2b
    :goto_2b
    return-void

    .line 173
    :cond_2c
    if-eqz v2, :cond_2b

    .line 174
    new-instance v0, Lcom/dropbox/android/filemanager/Q;

    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/O;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/Q;-><init>(Lcom/dropbox/android/filemanager/O;IIILcom/dropbox/android/filemanager/R;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    goto :goto_2b
.end method

.method public final a(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-static {p1}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 110
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;I)V

    .line 114
    :goto_d
    return-void

    .line 112
    :cond_e
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/filemanager/I;->b(Ljava/lang/String;I)V

    goto :goto_d
.end method

.method protected final b(Landroid/net/Uri;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 234
    new-instance v0, Lcom/dropbox/android/filemanager/K;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/K;-><init>(Landroid/net/Uri;IILcom/dropbox/android/filemanager/R;)V

    .line 235
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    .line 236
    return-object v0
.end method

.method public final b(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-static {p1}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 118
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/dropbox/android/filemanager/I;->b(Landroid/net/Uri;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;

    move-result-object v0

    .line 120
    :goto_e
    return-object v0

    :cond_f
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/I;->d(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;

    move-result-object v0

    goto :goto_e
.end method

.method protected final b(Ljava/lang/String;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 210
    invoke-static {p1}, Lcom/dropbox/android/filemanager/I;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/dropbox/android/filemanager/I;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v4

    .line 213
    if-eqz v4, :cond_32

    .line 214
    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v6, v1, p2}, Lcom/dropbox/android/filemanager/O;->c(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_32

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 225
    :goto_31
    return-void

    .line 220
    :cond_32
    if-eqz v4, :cond_4a

    .line 221
    new-instance v0, Lcom/dropbox/android/filemanager/Q;

    iget-object v1, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/O;

    iget-object v2, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/Q;-><init>(Lcom/dropbox/android/filemanager/O;IIILcom/dropbox/android/filemanager/R;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    goto :goto_31

    .line 223
    :cond_4a
    new-instance v0, Lcom/dropbox/android/filemanager/N;

    invoke-direct {v0, v2, v3, p2, v5}, Lcom/dropbox/android/filemanager/N;-><init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    goto :goto_31
.end method

.method protected final c(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-static {p1}, Lcom/dropbox/android/filemanager/I;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 188
    iget-object v0, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/dropbox/android/filemanager/I;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    .line 190
    if-eqz v3, :cond_41

    .line 191
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v4, v1, p3}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 192
    if-eqz v4, :cond_41

    .line 193
    new-instance v2, Lcom/dropbox/android/filemanager/T;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/O;->d()Z

    move-result v5

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/filemanager/O;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/I;->b:Landroid/content/Context;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v6, v1, p3}, Lcom/dropbox/android/filemanager/O;->b(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)J

    move-result-wide v0

    invoke-direct {v2, v4, v5, v0, v1}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/graphics/Bitmap;ZJ)V

    move-object v0, v2

    .line 206
    :goto_40
    return-object v0

    .line 200
    :cond_41
    if-eqz v3, :cond_5c

    .line 201
    new-instance v0, Lcom/dropbox/android/filemanager/Q;

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/dropbox/android/filemanager/O;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/Q;-><init>(Lcom/dropbox/android/filemanager/O;IIILcom/dropbox/android/filemanager/R;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    .line 206
    :goto_5a
    const/4 v0, 0x0

    goto :goto_40

    .line 203
    :cond_5c
    new-instance v0, Lcom/dropbox/android/filemanager/N;

    invoke-direct {v0, v2, p2, p3, p4}, Lcom/dropbox/android/filemanager/N;-><init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    goto :goto_5a
.end method

.method protected final d(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 228
    new-instance v0, Lcom/dropbox/android/filemanager/M;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/dropbox/android/filemanager/M;-><init>(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)V

    .line 229
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/I;->a(Lcom/dropbox/android/filemanager/S;)V

    .line 230
    return-object v0
.end method
