.class abstract Lcom/dropbox/android/filemanager/S;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field protected final b:Lcom/dropbox/android/filemanager/R;

.field protected final c:I

.field protected final d:I


# direct methods
.method private constructor <init>(Lcom/dropbox/android/filemanager/R;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 627
    iput-object p1, p0, Lcom/dropbox/android/filemanager/S;->b:Lcom/dropbox/android/filemanager/R;

    .line 628
    iput p2, p0, Lcom/dropbox/android/filemanager/S;->c:I

    .line 629
    iput p3, p0, Lcom/dropbox/android/filemanager/S;->d:I

    .line 630
    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/filemanager/R;IILcom/dropbox/android/filemanager/J;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 620
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/S;-><init>(Lcom/dropbox/android/filemanager/R;II)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/S;Landroid/content/Context;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 620
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/S;->b(Landroid/content/Context;)V

    return-void
.end method

.method private final b(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 635
    invoke-virtual {p0, p1}, Lcom/dropbox/android/filemanager/S;->a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/T;

    move-result-object v0

    .line 637
    iget-object v1, p0, Lcom/dropbox/android/filemanager/S;->b:Lcom/dropbox/android/filemanager/R;

    if-nez v1, :cond_10

    .line 639
    if-eqz v0, :cond_f

    .line 640
    iget-object v0, v0, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 645
    :cond_f
    :goto_f
    return-void

    .line 643
    :cond_10
    iget-object v1, p0, Lcom/dropbox/android/filemanager/S;->b:Lcom/dropbox/android/filemanager/R;

    iget v2, p0, Lcom/dropbox/android/filemanager/S;->c:I

    invoke-interface {v1, v2, v0}, Lcom/dropbox/android/filemanager/R;->a(ILcom/dropbox/android/filemanager/T;)V

    goto :goto_f
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/T;
.end method
