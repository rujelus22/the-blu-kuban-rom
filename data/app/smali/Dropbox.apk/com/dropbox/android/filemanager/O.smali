.class public Lcom/dropbox/android/filemanager/O;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:[Lcom/dropbox/android/filemanager/O;


# instance fields
.field private final b:Landroid/net/Uri;

.field private final c:Landroid/net/Uri;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 353
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/filemanager/O;->a:[Lcom/dropbox/android/filemanager/O;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Z)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    iput-object p1, p0, Lcom/dropbox/android/filemanager/O;->b:Landroid/net/Uri;

    .line 361
    iput-object p2, p0, Lcom/dropbox/android/filemanager/O;->c:Landroid/net/Uri;

    .line 362
    iput-boolean p3, p0, Lcom/dropbox/android/filemanager/O;->d:Z

    .line 363
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/net/Uri;)I
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 520
    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 521
    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-ltz v2, :cond_e

    .line 522
    long-to-int v0, v0

    .line 541
    :goto_d
    return v0

    .line 526
    :cond_e
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    .line 529
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 530
    if-eqz v1, :cond_37

    .line 532
    :try_start_22
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_34

    .line 533
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 534
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2f
    .catchall {:try_start_22 .. :try_end_2f} :catchall_39

    move-result v0

    .line 537
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_d

    :cond_34
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 541
    :cond_37
    const/4 v0, -0x1

    goto :goto_d

    .line 537
    :catchall_39
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)I
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 499
    const-string v3, "_data = ?"

    .line 500
    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v1

    .line 501
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 503
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/O;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 504
    if-eqz v1, :cond_30

    .line 506
    :try_start_1b
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2d

    .line 507
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 508
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_28
    .catchall {:try_start_1b .. :try_end_28} :catchall_32

    move-result v0

    .line 511
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 515
    :goto_2c
    return v0

    .line 511
    :cond_2d
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 515
    :cond_30
    const/4 v0, -0x1

    goto :goto_2c

    .line 511
    :catchall_32
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;Landroid/net/Uri;)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/O;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/O;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/O;->b(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/O;)Landroid/net/Uri;
    .registers 2
    .parameter

    .prologue
    .line 351
    iget-object v0, p0, Lcom/dropbox/android/filemanager/O;->b:Landroid/net/Uri;

    return-object v0
.end method

.method private a(Landroid/content/Context;I)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 545
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    .line 546
    const-string v3, "_id = ?"

    .line 547
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 548
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/O;->b:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 550
    if-eqz v1, :cond_30

    .line 552
    :try_start_1f
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 553
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 554
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2c
    .catchall {:try_start_1f .. :try_end_2c} :catchall_35

    move-result-object v5

    .line 558
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 561
    :cond_30
    :goto_30
    return-object v5

    .line 558
    :cond_31
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_30

    :catchall_35
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a()[Lcom/dropbox/android/filemanager/O;
    .registers 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 366
    sget-object v0, Lcom/dropbox/android/filemanager/O;->a:[Lcom/dropbox/android/filemanager/O;

    if-nez v0, :cond_61

    .line 367
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dropbox/android/filemanager/O;

    new-instance v1, Lcom/dropbox/android/filemanager/P;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v1, v2, v3, v5}, Lcom/dropbox/android/filemanager/P;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/dropbox/android/filemanager/O;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Landroid/provider/MediaStore$Images$Thumbnails;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v1, v2, v3, v5}, Lcom/dropbox/android/filemanager/O;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v1, v0, v6

    const/4 v1, 0x2

    new-instance v2, Lcom/dropbox/android/filemanager/O;

    const-string v3, "phoneStorage"

    invoke-static {v3}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "phoneStorage"

    invoke-static {v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4, v5}, Lcom/dropbox/android/filemanager/O;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/dropbox/android/filemanager/P;

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Landroid/provider/MediaStore$Video$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4, v6}, Lcom/dropbox/android/filemanager/P;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/dropbox/android/filemanager/O;

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Landroid/provider/MediaStore$Video$Thumbnails;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4, v6}, Lcom/dropbox/android/filemanager/O;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/dropbox/android/filemanager/O;

    const-string v3, "phoneStorage"

    invoke-static {v3}, Landroid/provider/MediaStore$Video$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "phoneStorage"

    invoke-static {v4}, Landroid/provider/MediaStore$Video$Thumbnails;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4, v6}, Lcom/dropbox/android/filemanager/O;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/filemanager/O;->a:[Lcom/dropbox/android/filemanager/O;

    .line 380
    :cond_61
    sget-object v0, Lcom/dropbox/android/filemanager/O;->a:[Lcom/dropbox/android/filemanager/O;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)J
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/O;->c(Landroid/content/Context;II)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 410
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/O;->d(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    .line 411
    if-eqz v2, :cond_12

    .line 412
    iget-object v0, p0, Lcom/dropbox/android/filemanager/O;->c:Landroid/net/Uri;

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/O;->d:Z

    move v3, p3

    move-object v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;ZLjava/lang/String;ILandroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 414
    :goto_11
    return-object v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private c(Landroid/content/Context;II)J
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 418
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/O;->d:Z

    if-nez v0, :cond_a

    move-wide v0, v6

    .line 434
    :cond_9
    :goto_9
    return-wide v0

    .line 421
    :cond_a
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "duration"

    aput-object v0, v2, v1

    .line 422
    const-string v3, "_id = ?"

    .line 423
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 424
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/O;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 427
    if-eqz v2, :cond_38

    :try_start_27
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 428
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_31
    .catchall {:try_start_27 .. :try_end_31} :catchall_3f

    move-result-wide v0

    .line 433
    if-eqz v2, :cond_9

    .line 434
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_9

    .line 433
    :cond_38
    if-eqz v2, :cond_3d

    .line 434
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3d
    move-wide v0, v6

    goto :goto_9

    .line 433
    :catchall_3f
    move-exception v0

    if-eqz v2, :cond_45

    .line 434
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_45
    throw v0
.end method

.method static synthetic c(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)Ljava/lang/String;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/O;->d(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/content/Context;II)Ljava/lang/String;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v5, 0x3

    const/4 v1, 0x1

    .line 441
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v7

    .line 450
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/O;->d:Z

    if-eqz v0, :cond_62

    .line 451
    if-ne p3, v5, :cond_50

    .line 452
    const-string v3, "video_id = ? AND (kind = ? OR kind = ?)"

    .line 454
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 456
    const-string v5, "kind DESC"

    .line 478
    :goto_29
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/O;->c:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 481
    if-eqz v1, :cond_4f

    .line 483
    :try_start_35
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 484
    :cond_3b
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 485
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 486
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z
    :try_end_48
    .catchall {:try_start_35 .. :try_end_48} :catchall_93

    move-result v3

    if-nez v3, :cond_3b

    .line 491
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v6, v0

    .line 495
    :cond_4f
    :goto_4f
    return-object v6

    .line 458
    :cond_50
    const-string v3, "video_id = ? AND kind = ?"

    .line 460
    new-array v4, v8, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    move-object v5, v6

    .line 461
    goto :goto_29

    .line 464
    :cond_62
    if-ne p3, v5, :cond_7d

    .line 465
    const-string v3, "image_id = ? AND (kind = ? OR kind = ?)"

    .line 467
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 469
    const-string v5, "kind DESC"

    goto :goto_29

    .line 471
    :cond_7d
    const-string v3, "image_id = ? AND kind = ?"

    .line 473
    new-array v4, v8, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    move-object v5, v6

    .line 474
    goto :goto_29

    .line 491
    :cond_8f
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_4f

    :catchall_93
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 396
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/O;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 397
    if-eqz v6, :cond_1a

    .line 398
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/O;->d:Z

    invoke-static {v6, v0, v1, p3}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;ZZI)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 399
    if-eqz v3, :cond_19

    .line 401
    iget-object v0, p0, Lcom/dropbox/android/filemanager/O;->c:Landroid/net/Uri;

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/O;->d:Z

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v6}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;ZLandroid/content/Context;Landroid/graphics/Bitmap;IILjava/lang/String;)V

    .line 406
    :cond_19
    :goto_19
    return-object v3

    :cond_1a
    const/4 v3, 0x0

    goto :goto_19
.end method

.method public final b()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/dropbox/android/filemanager/O;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/dropbox/android/filemanager/O;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/O;->d:Z

    return v0
.end method
