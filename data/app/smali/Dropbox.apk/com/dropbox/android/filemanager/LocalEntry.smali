.class public Lcom/dropbox/android/filemanager/LocalEntry;
.super Ldbxyzptlk/n/k;
.source "panda.py"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final w:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 16
    const-class v0, Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/LocalEntry;->w:Ljava/lang/String;

    .line 33
    new-instance v0, Lcom/dropbox/android/filemanager/G;

    invoke-direct {v0}, Lcom/dropbox/android/filemanager/G;-><init>()V

    sput-object v0, Lcom/dropbox/android/filemanager/LocalEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 66
    invoke-direct {p0}, Ldbxyzptlk/n/k;-><init>()V

    .line 67
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    invoke-direct {p0}, Ldbxyzptlk/n/k;-><init>()V

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_7d

    move v0, v1

    :goto_1e
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_7f

    move v0, v1

    :goto_4b
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_81

    move v0, v1

    :goto_60
    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_83

    :goto_7a
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Z

    .line 64
    return-void

    :cond_7d
    move v0, v2

    .line 49
    goto :goto_1e

    :cond_7f
    move v0, v2

    .line 56
    goto :goto_4b

    :cond_81
    move v0, v2

    .line 59
    goto :goto_60

    :cond_83
    move v1, v2

    .line 63
    goto :goto_7a
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/dropbox/android/filemanager/G;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/LocalEntry;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Ldbxyzptlk/n/k;)Landroid/content/ContentValues;
    .registers 6
    .parameter

    .prologue
    .line 201
    if-nez p0, :cond_a

    .line 202
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t create content values from a null entry"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_a
    iget-object v0, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 204
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t create content values from an entry w/ a null path"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_1a
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 208
    const-string v0, "path"

    iget-object v2, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v0, "bytes"

    iget-wide v2, p0, Ldbxyzptlk/n/k;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 211
    :try_start_31
    iget-object v0, p0, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    if-eqz v0, :cond_3c

    .line 212
    const-string v0, "revision"

    iget-object v2, p0, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_3c
    iget-object v0, p0, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    if-eqz v0, :cond_47

    .line 215
    const-string v0, "hash"

    iget-object v2, p0, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_47
    iget-object v0, p0, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    if-eqz v0, :cond_52

    .line 218
    const-string v0, "icon"

    iget-object v2, p0, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :cond_52
    const-string v0, "is_dir"

    iget-boolean v2, p0, Ldbxyzptlk/n/k;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 221
    const-string v0, "modified"

    iget-object v2, p0, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    if-eqz v0, :cond_6f

    .line 223
    const-string v0, "root"

    iget-object v2, p0, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :cond_6f
    iget-object v0, p0, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    if-eqz v0, :cond_7a

    .line 226
    const-string v0, "size"

    iget-object v2, p0, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :cond_7a
    iget-object v0, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    if-eqz v0, :cond_85

    .line 229
    const-string v0, "mime_type"

    iget-object v2, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_85
    const-string v0, "thumb_exists"

    iget-boolean v2, p0, Ldbxyzptlk/n/k;->s:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 232
    const-string v0, "parent_path"

    invoke-virtual {p0}, Ldbxyzptlk/n/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v0, "_display_name"

    invoke-virtual {p0}, Ldbxyzptlk/n/k;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v0, "canon_path"

    iget-object v2, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-static {v2}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v0, "canon_parent_path"

    invoke-virtual {p0}, Ldbxyzptlk/n/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_ba
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_ba} :catch_bb

    .line 239
    :goto_ba
    return-object v1

    .line 236
    :catch_bb
    move-exception v0

    .line 237
    sget-object v2, Lcom/dropbox/android/filemanager/LocalEntry;->w:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error creating values from entry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-static {v4}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_ba
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/ContentValues;)Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 246
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;
    :try_end_6
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_6} :catch_9b

    .line 249
    :goto_6
    const-string v1, "path"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    .line 250
    const-string v1, "bytes"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    .line 251
    const-string v1, "revision"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 252
    const-string v1, "revision"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    .line 254
    :cond_2a
    const-string v1, "hash"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 255
    const-string v1, "hash"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    .line 257
    :cond_3a
    const-string v1, "icon"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 258
    const-string v1, "icon"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    .line 260
    :cond_4a
    const-string v1, "is_dir"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    .line 261
    const-string v1, "modified"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    .line 262
    const-string v1, "root"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6e

    .line 263
    const-string v1, "root"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    .line 265
    :cond_6e
    const-string v1, "size"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7e

    .line 266
    const-string v1, "size"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    .line 268
    :cond_7e
    const-string v1, "mime_type"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8e

    .line 269
    const-string v1, "mime_type"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    .line 271
    :cond_8e
    const-string v1, "thumb_exists"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    .line 273
    return-object v0

    .line 247
    :catch_9b
    move-exception v0

    move-object v0, p0

    goto/16 :goto_6
.end method


# virtual methods
.method public final a()Z
    .registers 3

    .prologue
    .line 296
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public describeContents()I
    .registers 2

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 125
    if-ne p0, p1, :cond_5

    .line 195
    :cond_4
    :goto_4
    return v0

    .line 127
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 128
    goto :goto_4

    .line 129
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 130
    goto :goto_4

    .line 131
    :cond_15
    check-cast p1, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 132
    iget-wide v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    iget-wide v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_21

    move v0, v1

    .line 133
    goto :goto_4

    .line 134
    :cond_21
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-nez v2, :cond_2b

    .line 135
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-eqz v2, :cond_37

    move v0, v1

    .line 136
    goto :goto_4

    .line 137
    :cond_2b
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_37

    move v0, v1

    .line 138
    goto :goto_4

    .line 139
    :cond_37
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    if-nez v2, :cond_41

    .line 140
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    if-eqz v2, :cond_4d

    move v0, v1

    .line 141
    goto :goto_4

    .line 142
    :cond_41
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4d

    move v0, v1

    .line 143
    goto :goto_4

    .line 144
    :cond_4d
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    if-nez v2, :cond_57

    .line 145
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    if-eqz v2, :cond_63

    move v0, v1

    .line 146
    goto :goto_4

    .line 147
    :cond_57
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_63

    move v0, v1

    .line 148
    goto :goto_4

    .line 149
    :cond_63
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    if-nez v2, :cond_6d

    .line 150
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    if-eqz v2, :cond_79

    move v0, v1

    .line 151
    goto :goto_4

    .line 152
    :cond_6d
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_79

    move v0, v1

    .line 153
    goto :goto_4

    .line 154
    :cond_79
    iget-boolean v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eq v2, v3, :cond_81

    move v0, v1

    .line 155
    goto :goto_4

    .line 156
    :cond_81
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-nez v2, :cond_8c

    .line 157
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-eqz v2, :cond_99

    move v0, v1

    .line 158
    goto/16 :goto_4

    .line 159
    :cond_8c
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_99

    move v0, v1

    .line 160
    goto/16 :goto_4

    .line 161
    :cond_99
    iget-wide v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:J

    iget-wide v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a4

    move v0, v1

    .line 162
    goto/16 :goto_4

    .line 163
    :cond_a4
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    if-nez v2, :cond_af

    .line 164
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    if-eqz v2, :cond_bc

    move v0, v1

    .line 165
    goto/16 :goto_4

    .line 166
    :cond_af
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_bc

    move v0, v1

    .line 167
    goto/16 :goto_4

    .line 168
    :cond_bc
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-nez v2, :cond_c7

    .line 169
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-eqz v2, :cond_d4

    move v0, v1

    .line 170
    goto/16 :goto_4

    .line 171
    :cond_c7
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d4

    move v0, v1

    .line 172
    goto/16 :goto_4

    .line 173
    :cond_d4
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    if-nez v2, :cond_df

    .line 174
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    if-eqz v2, :cond_ec

    move v0, v1

    .line 175
    goto/16 :goto_4

    .line 176
    :cond_df
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_ec

    move v0, v1

    .line 177
    goto/16 :goto_4

    .line 178
    :cond_ec
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-nez v2, :cond_f7

    .line 179
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-eqz v2, :cond_104

    move v0, v1

    .line 180
    goto/16 :goto_4

    .line 181
    :cond_f7
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_104

    move v0, v1

    .line 182
    goto/16 :goto_4

    .line 183
    :cond_104
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    if-nez v2, :cond_10f

    .line 184
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    if-eqz v2, :cond_11c

    move v0, v1

    .line 185
    goto/16 :goto_4

    .line 186
    :cond_10f
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11c

    move v0, v1

    .line 187
    goto/16 :goto_4

    .line 188
    :cond_11c
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    if-nez v2, :cond_127

    .line 189
    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    if-eqz v2, :cond_134

    move v0, v1

    .line 190
    goto/16 :goto_4

    .line 191
    :cond_127
    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_134

    move v0, v1

    .line 192
    goto/16 :goto_4

    .line 193
    :cond_134
    iget-boolean v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Z

    iget-boolean v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->t:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 194
    goto/16 :goto_4
.end method

.method public hashCode()I
    .registers 10

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/16 v8, 0x20

    const/4 v1, 0x0

    .line 98
    .line 100
    iget-wide v4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    iget-wide v6, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/lit8 v0, v0, 0x1f

    .line 101
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-nez v0, :cond_81

    move v0, v1

    :goto_17
    add-int/2addr v0, v4

    .line 102
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    if-nez v0, :cond_88

    move v0, v1

    :goto_1f
    add-int/2addr v0, v4

    .line 103
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    if-nez v0, :cond_8f

    move v0, v1

    :goto_27
    add-int/2addr v0, v4

    .line 105
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    if-nez v0, :cond_96

    move v0, v1

    :goto_2f
    add-int/2addr v0, v4

    .line 106
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_9d

    move v0, v2

    :goto_37
    add-int/2addr v0, v4

    .line 107
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    if-nez v0, :cond_9f

    move v0, v1

    :goto_3f
    add-int/2addr v0, v4

    .line 109
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:J

    iget-wide v6, p0, Lcom/dropbox/android/filemanager/LocalEntry;->e:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 111
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    if-nez v0, :cond_a6

    move v0, v1

    :goto_51
    add-int/2addr v0, v4

    .line 113
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    if-nez v0, :cond_ad

    move v0, v1

    :goto_59
    add-int/2addr v0, v4

    .line 115
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    if-nez v0, :cond_b4

    move v0, v1

    :goto_61
    add-int/2addr v0, v4

    .line 116
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-nez v0, :cond_bb

    move v0, v1

    :goto_69
    add-int/2addr v0, v4

    .line 117
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    if-nez v0, :cond_c2

    move v0, v1

    :goto_71
    add-int/2addr v0, v4

    .line 118
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    if-nez v4, :cond_c9

    :goto_78
    add-int/2addr v0, v1

    .line 119
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Z

    if-eqz v1, :cond_d0

    :goto_7f
    add-int/2addr v0, v2

    .line 120
    return v0

    .line 101
    :cond_81
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_17

    .line 102
    :cond_88
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1f

    .line 103
    :cond_8f
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_27

    .line 105
    :cond_96
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2f

    :cond_9d
    move v0, v3

    .line 106
    goto :goto_37

    .line 107
    :cond_9f
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3f

    .line 111
    :cond_a6
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_51

    .line 113
    :cond_ad
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_59

    .line 115
    :cond_b4
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_61

    .line 116
    :cond_bb
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_69

    .line 117
    :cond_c2
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_71

    .line 118
    :cond_c9
    iget-object v1, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_78

    :cond_d0
    move v2, v3

    .line 119
    goto :goto_7f
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    iget-wide v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_68

    move v0, v1

    :goto_16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    if-eqz v0, :cond_6a

    move v0, v1

    :goto_3c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 87
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-eqz v0, :cond_6c

    move v0, v1

    :goto_4e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 90
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->t:Z

    if-eqz v0, :cond_6e

    :goto_64
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 94
    return-void

    :cond_68
    move v0, v2

    .line 79
    goto :goto_16

    :cond_6a
    move v0, v2

    .line 86
    goto :goto_3c

    :cond_6c
    move v0, v2

    .line 89
    goto :goto_4e

    :cond_6e
    move v1, v2

    .line 93
    goto :goto_64
.end method
