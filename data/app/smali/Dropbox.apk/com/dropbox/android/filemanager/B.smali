.class final Lcom/dropbox/android/filemanager/B;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/l;


# instance fields
.field private final a:Ldbxyzptlk/k/g;

.field private final b:Ldbxyzptlk/k/j;


# direct methods
.method constructor <init>(Ldbxyzptlk/k/g;Ldbxyzptlk/k/j;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629
    iput-object p1, p0, Lcom/dropbox/android/filemanager/B;->a:Ldbxyzptlk/k/g;

    .line 630
    iput-object p2, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    .line 631
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 635
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;JJ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 640
    iget-object v0, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->a:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/j;->a(Lcom/dropbox/android/taskqueue/m;)V

    .line 641
    iget-object v0, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    const/high16 v1, 0x42c8

    long-to-float v2, p3

    long-to-float v3, p5

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/j;->a(F)V

    .line 642
    iget-object v0, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    invoke-virtual {v0, p5, p6}, Ldbxyzptlk/k/j;->a(J)V

    .line 643
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/k;Lcom/dropbox/android/taskqueue/m;Landroid/net/Uri;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 654
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/m;->a()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 655
    iget-object v0, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    const/high16 v1, -0x4080

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/j;->a(F)V

    .line 656
    iget-object v0, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    invoke-virtual {v0, p2}, Ldbxyzptlk/k/j;->a(Lcom/dropbox/android/taskqueue/m;)V

    .line 662
    :goto_12
    return-void

    .line 658
    :cond_13
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/k;->e()Ldbxyzptlk/k/i;

    move-result-object v0

    .line 659
    invoke-virtual {p1, p0}, Lcom/dropbox/android/taskqueue/k;->b(Lcom/dropbox/android/taskqueue/l;)V

    .line 660
    iget-object v1, p0, Lcom/dropbox/android/filemanager/B;->a:Ldbxyzptlk/k/g;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/k/g;->b(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    goto :goto_12
.end method

.method public final b(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 666
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/k;->e()Ldbxyzptlk/k/i;

    move-result-object v0

    .line 667
    invoke-virtual {p1, p0}, Lcom/dropbox/android/taskqueue/k;->b(Lcom/dropbox/android/taskqueue/l;)V

    .line 668
    iget-object v1, p0, Lcom/dropbox/android/filemanager/B;->a:Ldbxyzptlk/k/g;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/k/g;->b(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    .line 669
    return-void
.end method

.method public final c(Lcom/dropbox/android/taskqueue/k;Landroid/net/Uri;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 647
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/k;->e()Ldbxyzptlk/k/i;

    move-result-object v0

    .line 648
    invoke-virtual {p1, p0}, Lcom/dropbox/android/taskqueue/k;->b(Lcom/dropbox/android/taskqueue/l;)V

    .line 649
    iget-object v1, p0, Lcom/dropbox/android/filemanager/B;->a:Ldbxyzptlk/k/g;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/B;->b:Ldbxyzptlk/k/j;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/k/g;->b(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    .line 650
    return-void
.end method
