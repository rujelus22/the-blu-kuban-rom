.class final Lcom/dropbox/android/filemanager/K;
.super Lcom/dropbox/android/filemanager/S;
.source "panda.py"


# instance fields
.field private final a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;IILcom/dropbox/android/filemanager/R;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 728
    const/4 v0, 0x0

    invoke-direct {p0, p4, p2, p3, v0}, Lcom/dropbox/android/filemanager/S;-><init>(Lcom/dropbox/android/filemanager/R;IILcom/dropbox/android/filemanager/J;)V

    .line 729
    iput-object p1, p0, Lcom/dropbox/android/filemanager/K;->a:Landroid/net/Uri;

    .line 730
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/T;
    .registers 8
    .parameter

    .prologue
    .line 734
    iget-object v0, p0, Lcom/dropbox/android/filemanager/K;->a:Landroid/net/Uri;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/I;->a(Landroid/net/Uri;)Lcom/dropbox/android/filemanager/O;

    move-result-object v2

    .line 736
    if-eqz v2, :cond_31

    .line 737
    iget-object v0, p0, Lcom/dropbox/android/filemanager/K;->a:Landroid/net/Uri;

    invoke-static {v2, p1, v0}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    .line 738
    if-lez v3, :cond_31

    .line 739
    iget v0, p0, Lcom/dropbox/android/filemanager/K;->d:I

    invoke-static {v2, p1, v3, v0}, Lcom/dropbox/android/filemanager/O;->a(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 740
    if-nez v0, :cond_33

    .line 741
    iget v0, p0, Lcom/dropbox/android/filemanager/K;->d:I

    invoke-virtual {v2, p1, v3, v0}, Lcom/dropbox/android/filemanager/O;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 743
    :goto_1f
    if-eqz v1, :cond_31

    .line 744
    new-instance v0, Lcom/dropbox/android/filemanager/T;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/O;->d()Z

    move-result v4

    iget v5, p0, Lcom/dropbox/android/filemanager/K;->d:I

    invoke-static {v2, p1, v3, v5}, Lcom/dropbox/android/filemanager/O;->b(Lcom/dropbox/android/filemanager/O;Landroid/content/Context;II)J

    move-result-wide v2

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/dropbox/android/filemanager/T;-><init>(Landroid/graphics/Bitmap;ZJ)V

    .line 752
    :goto_30
    return-object v0

    :cond_31
    const/4 v0, 0x0

    goto :goto_30

    :cond_33
    move-object v1, v0

    goto :goto_1f
.end method
