.class public Lcom/dropbox/android/filemanager/t;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/dropbox/android/provider/h;

.field private e:Z

.field private f:Ldbxyzptlk/k/g;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 29
    const-class v0, Lcom/dropbox/android/filemanager/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/t;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/provider/h;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/t;->b:Ljava/util/ArrayList;

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/t;->c:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/dropbox/android/filemanager/t;->d:Lcom/dropbox/android/provider/h;

    .line 46
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->b()Ldbxyzptlk/k/g;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/t;->f:Ldbxyzptlk/k/g;

    .line 47
    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/ContentValues;ZLcom/dropbox/android/filemanager/W;)Landroid/content/ContentValues;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 220
    .line 222
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-eqz v0, :cond_69

    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    if-eqz v0, :cond_69

    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_69

    const/4 v0, 0x1

    .line 224
    :goto_14
    new-instance v2, Ljava/io/File;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6b

    .line 227
    if-nez p3, :cond_25

    if-eqz v0, :cond_29

    .line 230
    :cond_25
    invoke-virtual {p4, p1}, Lcom/dropbox/android/filemanager/W;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v1

    .line 247
    :cond_29
    :goto_29
    if-eqz v1, :cond_68

    .line 248
    iget-object v0, p0, Lcom/dropbox/android/filemanager/t;->f:Ldbxyzptlk/k/g;

    new-instance v1, Ldbxyzptlk/k/i;

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;)Ldbxyzptlk/k/f;

    move-result-object v0

    .line 249
    if-nez v0, :cond_68

    .line 251
    sget-object v0, Lcom/dropbox/android/filemanager/t;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File changed remotely, so re-downloading: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    new-instance v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/t;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 253
    iget-object v1, p0, Lcom/dropbox/android/filemanager/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    invoke-static {p1}, Lcom/dropbox/android/filemanager/u;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 259
    :cond_68
    return-object p2

    :cond_69
    move v0, v1

    .line 222
    goto :goto_14

    .line 234
    :cond_6b
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/t;->e:Z

    if-eqz v0, :cond_29

    .line 236
    if-nez p2, :cond_76

    .line 237
    new-instance p2, Landroid/content/ContentValues;

    invoke-direct {p2}, Landroid/content/ContentValues;-><init>()V

    .line 239
    :cond_76
    const-string v0, "local_bytes"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 240
    const-string v0, "local_modified"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 241
    const-string v0, "local_revision"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 244
    invoke-virtual {p4, p1}, Lcom/dropbox/android/filemanager/W;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v1

    goto :goto_29
.end method

.method private a(Lcom/dropbox/android/util/V;Ldbxyzptlk/n/k;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 137
    invoke-virtual {p1}, Lcom/dropbox/android/util/V;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 139
    invoke-static {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ldbxyzptlk/n/k;)Landroid/content/ContentValues;

    move-result-object v1

    .line 142
    :try_start_9
    const-string v2, "dropbox"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_9 .. :try_end_f} :catch_10

    .line 151
    :cond_f
    :goto_f
    return-void

    .line 143
    :catch_10
    move-exception v2

    .line 144
    const-string v2, "dropbox"

    const-string v3, "canon_path = ?"

    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p2, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-static {v6}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 147
    if-eq v0, v7, :cond_f

    .line 148
    sget-object v1, Lcom/dropbox/android/filemanager/t;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong number of database entries for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f
.end method

.method private a(Lcom/dropbox/android/util/V;Ldbxyzptlk/n/k;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/W;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 164
    const/4 v0, 0x0

    .line 168
    if-eqz p2, :cond_65

    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    if-eqz v1, :cond_65

    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    iget-object v4, p2, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_65

    move v4, v2

    .line 169
    :goto_14
    if-eqz p2, :cond_67

    iget-object v1, p2, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    if-eqz v1, :cond_67

    iget-object v1, p2, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    iget-object v5, p3, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_67

    move v1, v2

    .line 171
    :goto_25
    if-nez v4, :cond_29

    if-eqz v1, :cond_69

    :cond_29
    move v1, v3

    move v4, v2

    .line 178
    :goto_2b
    if-nez v4, :cond_2f

    if-eqz v1, :cond_33

    .line 179
    :cond_2f
    invoke-static {p2}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ldbxyzptlk/n/k;)Landroid/content/ContentValues;

    move-result-object v0

    .line 182
    :cond_33
    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    if-eqz v1, :cond_74

    iget-object v1, p3, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_74

    .line 183
    invoke-direct {p0, p3, v0, v4, p4}, Lcom/dropbox/android/filemanager/t;->a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/ContentValues;ZLcom/dropbox/android/filemanager/W;)Landroid/content/ContentValues;

    move-result-object v0

    .line 201
    :cond_43
    :goto_43
    if-eqz v0, :cond_64

    .line 203
    invoke-virtual {p1}, Lcom/dropbox/android/util/V;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 205
    new-array v4, v2, [Ljava/lang/String;

    iget-object v5, p3, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v5}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    .line 206
    const-string v3, "dropbox"

    const-string v5, "canon_path = ?"

    invoke-virtual {v1, v3, v0, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 208
    if-eq v0, v2, :cond_64

    .line 209
    sget-object v0, Lcom/dropbox/android/filemanager/t;->a:Ljava/lang/String;

    const-string v1, "Error updating entry"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_64
    return-void

    :cond_65
    move v4, v3

    .line 168
    goto :goto_14

    :cond_67
    move v1, v3

    .line 169
    goto :goto_25

    .line 174
    :cond_69
    if-eqz p2, :cond_b8

    invoke-virtual {p2, p3}, Ldbxyzptlk/n/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b8

    move v1, v2

    move v4, v3

    .line 175
    goto :goto_2b

    .line 185
    :cond_74
    invoke-virtual {p4, p3}, Lcom/dropbox/android/filemanager/W;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 186
    iget-object v1, p0, Lcom/dropbox/android/filemanager/t;->f:Ldbxyzptlk/k/g;

    new-instance v4, Ldbxyzptlk/k/i;

    new-instance v5, Lcom/dropbox/android/util/DropboxPath;

    iget-object v6, p3, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v4}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;)Ldbxyzptlk/k/f;

    move-result-object v1

    .line 187
    if-nez v1, :cond_43

    .line 191
    sget-object v1, Lcom/dropbox/android/filemanager/t;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No local copy of the file, and should download: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p3, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    new-instance v1, Lcom/dropbox/android/taskqueue/DownloadTask;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/t;->c:Landroid/content/Context;

    invoke-direct {v1, v4, p3}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 193
    iget-object v4, p0, Lcom/dropbox/android/filemanager/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-static {p3}, Lcom/dropbox/android/filemanager/u;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_43

    :cond_b8
    move v1, v3

    move v4, v3

    goto/16 :goto_2b
.end method

.method private b()V
    .registers 4

    .prologue
    .line 128
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->i()Lcom/dropbox/android/taskqueue/p;

    move-result-object v1

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/filemanager/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    .line 131
    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/p;->b(Lcom/dropbox/android/taskqueue/k;)V

    goto :goto_e

    .line 133
    :cond_1e
    iget-object v0, p0, Lcom/dropbox/android/filemanager/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 134
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/t;->b()V

    .line 122
    return-void
.end method

.method public final a(Ldbxyzptlk/n/k;)V
    .registers 4
    .parameter

    .prologue
    .line 154
    new-instance v1, Lcom/dropbox/android/util/V;

    iget-object v0, p0, Lcom/dropbox/android/filemanager/t;->d:Lcom/dropbox/android/provider/h;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/V;-><init>(Lcom/dropbox/android/provider/h;)V

    .line 156
    :try_start_7
    invoke-direct {p0, v1, p1}, Lcom/dropbox/android/filemanager/t;->a(Lcom/dropbox/android/util/V;Ldbxyzptlk/n/k;)V

    .line 157
    invoke-virtual {v1}, Lcom/dropbox/android/util/V;->b()V
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_11

    .line 159
    invoke-virtual {v1}, Lcom/dropbox/android/util/V;->c()V

    .line 161
    return-void

    .line 159
    :catchall_11
    move-exception v0

    invoke-virtual {v1}, Lcom/dropbox/android/util/V;->c()V

    throw v0
.end method

.method public final a(Ljava/util/List;Ljava/util/List;Lcom/dropbox/android/filemanager/W;)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-static {}, Lcom/dropbox/android/util/af;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/t;->e:Z

    .line 64
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 66
    if-eqz p2, :cond_27

    .line 67
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_11
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/n/k;

    .line 68
    iget-boolean v3, v0, Ldbxyzptlk/n/k;->t:Z

    if-nez v3, :cond_11

    .line 70
    iget-object v3, v0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_11

    .line 75
    :cond_27
    new-instance v3, Lcom/dropbox/android/util/V;

    iget-object v0, p0, Lcom/dropbox/android/filemanager/t;->d:Lcom/dropbox/android/provider/h;

    invoke-direct {v3, v0}, Lcom/dropbox/android/util/V;-><init>(Lcom/dropbox/android/provider/h;)V

    .line 79
    :try_start_2e
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_32
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_85

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 80
    sget-object v1, Lcom/dropbox/android/filemanager/W;->a:Lcom/dropbox/android/filemanager/W;

    if-ne p3, v1, :cond_5e

    .line 82
    const/4 v1, 0x0

    invoke-direct {p0, v3, v1, v0, p3}, Lcom/dropbox/android/filemanager/t;->a(Lcom/dropbox/android/util/V;Ldbxyzptlk/n/k;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/W;)V
    :try_end_46
    .catchall {:try_start_2e .. :try_end_46} :catchall_77
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_46} :catch_47

    goto :goto_32

    .line 110
    :catch_47
    move-exception v0

    .line 111
    :try_start_48
    sget-object v1, Lcom/dropbox/android/filemanager/t;->a:Ljava/lang/String;

    const-string v2, "Exception in insert()"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 112
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;)V
    :try_end_56
    .catchall {:try_start_48 .. :try_end_56} :catchall_77

    .line 114
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->d()Z

    move-result v0

    .line 115
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->c()V

    .line 117
    :goto_5d
    return v0

    .line 83
    :cond_5e
    :try_start_5e
    iget-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7f

    .line 85
    iget-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/n/k;

    .line 88
    invoke-direct {p0, v3, v1, v0, p3}, Lcom/dropbox/android/filemanager/t;->a(Lcom/dropbox/android/util/V;Ldbxyzptlk/n/k;Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/filemanager/W;)V

    .line 91
    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_76
    .catchall {:try_start_5e .. :try_end_76} :catchall_77
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_76} :catch_47

    goto :goto_32

    .line 114
    :catchall_77
    move-exception v0

    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->d()Z

    .line 115
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->c()V

    throw v0

    .line 94
    :cond_7f
    :try_start_7f
    iget-object v1, p0, Lcom/dropbox/android/filemanager/t;->c:Landroid/content/Context;

    invoke-static {v1, v3, v0}, Lcom/dropbox/android/filemanager/u;->a(Landroid/content/Context;Lcom/dropbox/android/util/V;Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_32

    .line 100
    :cond_85
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_a9

    .line 102
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_93
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 104
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/n/k;

    .line 105
    invoke-direct {p0, v3, v0}, Lcom/dropbox/android/filemanager/t;->a(Lcom/dropbox/android/util/V;Ldbxyzptlk/n/k;)V

    goto :goto_93

    .line 108
    :cond_a9
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->b()V
    :try_end_ac
    .catchall {:try_start_7f .. :try_end_ac} :catchall_77
    .catch Ljava/lang/Exception; {:try_start_7f .. :try_end_ac} :catch_47

    .line 114
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->d()Z

    move-result v0

    .line 115
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->c()V

    goto :goto_5d
.end method
