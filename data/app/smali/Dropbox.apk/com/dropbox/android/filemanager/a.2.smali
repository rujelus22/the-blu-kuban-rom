.class public Lcom/dropbox/android/filemanager/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;

.field private static c:Lcom/dropbox/android/filemanager/a;


# instance fields
.field public a:Ldbxyzptlk/r/i;

.field private final d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 54
    const-class v0, Lcom/dropbox/android/filemanager/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/filemanager/a;->c:Lcom/dropbox/android/filemanager/a;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    .line 66
    invoke-virtual {p0, p1}, Lcom/dropbox/android/filemanager/a;->c(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method public static a()Lcom/dropbox/android/filemanager/a;
    .registers 1

    .prologue
    .line 79
    sget-object v0, Lcom/dropbox/android/filemanager/a;->c:Lcom/dropbox/android/filemanager/a;

    if-nez v0, :cond_a

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 82
    :cond_a
    sget-object v0, Lcom/dropbox/android/filemanager/a;->c:Lcom/dropbox/android/filemanager/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 71
    sget-object v0, Lcom/dropbox/android/filemanager/a;->c:Lcom/dropbox/android/filemanager/a;

    if-nez v0, :cond_c

    .line 72
    new-instance v0, Lcom/dropbox/android/filemanager/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/dropbox/android/filemanager/a;->c:Lcom/dropbox/android/filemanager/a;

    .line 76
    return-void

    .line 74
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Ldbxyzptlk/r/p;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 278
    monitor-enter p0

    :try_start_1
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v1, "handling logged in user"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->i()V

    .line 282
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    invoke-virtual {p2}, Ldbxyzptlk/r/p;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/j/b;->a(J)V

    .line 284
    invoke-static {}, Lcom/dropbox/android/util/i;->i()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "uid"

    invoke-virtual {p2}, Ldbxyzptlk/r/p;->d()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 285
    invoke-static {}, Lcom/dropbox/android/util/i;->P()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 287
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    sget-object v1, Lcom/dropbox/android/util/aC;->a:Lcom/dropbox/android/util/aC;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/r/i;->a()Ldbxyzptlk/q/m;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/r/E;

    invoke-virtual {p2}, Ldbxyzptlk/r/p;->c()Ldbxyzptlk/q/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/r/E;->a(Ldbxyzptlk/q/k;)V

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-static {v0}, Ldbxyzptlk/l/a;->b(Landroid/content/Context;)Ldbxyzptlk/l/a;

    move-result-object v0

    invoke-virtual {p2}, Ldbxyzptlk/r/p;->d()J

    move-result-wide v1

    invoke-virtual {p2}, Ldbxyzptlk/r/p;->c()Ldbxyzptlk/q/k;

    move-result-object v3

    invoke-virtual {v0, p1, v1, v2, v3}, Ldbxyzptlk/l/a;->a(Ljava/lang/String;JLdbxyzptlk/q/k;)V

    .line 293
    invoke-static {}, Lcom/dropbox/android/util/i;->an()V

    .line 295
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/content/Context;)V

    .line 298
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->d()V
    :try_end_64
    .catchall {:try_start_1 .. :try_end_64} :catchall_66

    .line 300
    monitor-exit p0

    return-void

    .line 278
    :catchall_66
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(Landroid/content/Context;)Lcom/dropbox/android/filemanager/a;
    .registers 2
    .parameter

    .prologue
    .line 86
    sget-object v0, Lcom/dropbox/android/filemanager/a;->c:Lcom/dropbox/android/filemanager/a;

    if-nez v0, :cond_a

    .line 88
    new-instance v0, Lcom/dropbox/android/filemanager/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/a;-><init>(Landroid/content/Context;)V

    .line 90
    :goto_9
    return-object v0

    :cond_a
    sget-object v0, Lcom/dropbox/android/filemanager/a;->c:Lcom/dropbox/android/filemanager/a;

    goto :goto_9
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 221
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->I()Ldbxyzptlk/l/t;

    move-result-object v0

    .line 223
    if-nez v0, :cond_12

    .line 224
    new-instance v0, Ldbxyzptlk/o/a;

    const-string v1, "Tried to log in without twofactor checkpoint token"

    invoke-direct {v0, v1}, Ldbxyzptlk/o/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_12
    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/l/t;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ldbxyzptlk/r/i;->c(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/r/p;

    move-result-object v1

    .line 228
    invoke-static {}, Lcom/dropbox/android/util/i;->S()Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V

    .line 230
    invoke-virtual {v0}, Ldbxyzptlk/l/t;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/r/p;)V

    .line 231
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0, p1, p2}, Ldbxyzptlk/r/i;->b(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/r/p;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Ldbxyzptlk/r/p;->a()Z

    move-result v1

    if-eqz v1, :cond_29

    .line 208
    invoke-virtual {v0}, Ldbxyzptlk/r/p;->b()Ldbxyzptlk/l/r;

    move-result-object v0

    .line 209
    sget-object v1, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v2, "Partially authenticated - need twofactor"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v1

    .line 212
    new-instance v2, Ldbxyzptlk/l/t;

    invoke-direct {v2, v0, p1}, Ldbxyzptlk/l/t;-><init>(Ldbxyzptlk/l/r;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/l/m;->a(Ldbxyzptlk/l/t;)V

    .line 213
    new-instance v0, Lcom/dropbox/android/filemanager/c;

    invoke-direct {v0}, Lcom/dropbox/android/filemanager/c;-><init>()V

    throw v0

    .line 215
    :cond_29
    sget-object v1, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v2, "Successfully authenticated"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/r/p;)V

    .line 218
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 266
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ldbxyzptlk/r/p;

    move-result-object v0

    .line 267
    sget-object v1, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v2, "Successfully created new user"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/r/p;)V

    .line 269
    return-void
.end method

.method public final b()Ljava/lang/String;
    .registers 8

    .prologue
    .line 242
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->I()Ldbxyzptlk/l/t;

    move-result-object v6

    .line 244
    if-nez v6, :cond_12

    .line 245
    new-instance v0, Ldbxyzptlk/o/a;

    const-string v1, "Tried to resend twofactor code without having checkpoint token"

    invoke-direct {v0, v1}, Ldbxyzptlk/o/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_12
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v6}, Ldbxyzptlk/l/t;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/r/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 251
    if-eqz v4, :cond_3a

    .line 252
    new-instance v0, Ldbxyzptlk/l/t;

    invoke-virtual {v6}, Ldbxyzptlk/l/t;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Ldbxyzptlk/l/t;->c()J

    move-result-wide v2

    invoke-virtual {v6}, Ldbxyzptlk/l/t;->e()Ldbxyzptlk/l/s;

    move-result-object v5

    invoke-virtual {v6}, Ldbxyzptlk/l/t;->f()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/l/t;-><init>(Ljava/lang/String;JLjava/lang/String;Ldbxyzptlk/l/s;Ljava/lang/String;)V

    .line 258
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/l/m;->a(Ldbxyzptlk/l/t;)V

    .line 261
    :cond_3a
    return-object v4
.end method

.method public final b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 272
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0, p1}, Ldbxyzptlk/r/i;->i(Ljava/lang/String;)Ldbxyzptlk/r/p;

    move-result-object v0

    .line 273
    sget-object v1, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v2, "Successfully authenticated"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    invoke-virtual {v0}, Ldbxyzptlk/r/p;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;Ldbxyzptlk/r/p;)V

    .line 275
    return-void
.end method

.method public final c(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 95
    invoke-static {p1}, Ldbxyzptlk/l/a;->b(Landroid/content/Context;)Ldbxyzptlk/l/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/a;->j()Ldbxyzptlk/q/k;

    move-result-object v1

    .line 96
    if-eqz v1, :cond_1e

    .line 97
    new-instance v0, Ldbxyzptlk/r/E;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/r/E;-><init>(Ldbxyzptlk/q/k;Landroid/content/Context;)V

    .line 191
    :goto_11
    new-instance v1, Lcom/dropbox/android/filemanager/b;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/b;-><init>(Lcom/dropbox/android/filemanager/a;)V

    .line 201
    new-instance v2, Ldbxyzptlk/r/i;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/r/i;-><init>(Ldbxyzptlk/q/m;Ldbxyzptlk/n/s;)V

    iput-object v2, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    .line 202
    return-void

    .line 99
    :cond_1e
    new-instance v0, Ldbxyzptlk/r/E;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Ldbxyzptlk/r/E;-><init>(Landroid/content/Context;)V

    goto :goto_11
.end method

.method public final declared-synchronized c()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 303
    monitor-enter p0

    :try_start_3
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/r/i;->a()Ldbxyzptlk/q/m;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/r/E;

    invoke-virtual {v0}, Ldbxyzptlk/r/E;->c()Z
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_4d

    move-result v0

    if-eqz v0, :cond_14

    move v0, v1

    .line 317
    :goto_12
    monitor-exit p0

    return v0

    .line 306
    :cond_14
    :try_start_14
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-static {v0}, Ldbxyzptlk/l/a;->b(Landroid/content/Context;)Ldbxyzptlk/l/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/a;->j()Ldbxyzptlk/q/k;

    move-result-object v3

    .line 307
    if-eqz v3, :cond_44

    .line 308
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/r/i;->a()Ldbxyzptlk/q/m;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/r/E;

    invoke-virtual {v0, v3}, Ldbxyzptlk/r/E;->a(Ldbxyzptlk/q/k;)V

    .line 309
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/r/i;->a()Ldbxyzptlk/q/m;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/r/E;

    invoke-virtual {v0}, Ldbxyzptlk/r/E;->c()Z

    move-result v0

    if-eqz v0, :cond_3b

    move v0, v1

    .line 310
    goto :goto_12

    .line 312
    :cond_3b
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v1, "Failed user authentication for stored login tokens."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 313
    goto :goto_12

    .line 316
    :cond_44
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v1, "No stored login token."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4b
    .catchall {:try_start_14 .. :try_end_4b} :catchall_4d

    move v0, v2

    .line 317
    goto :goto_12

    .line 303
    :catchall_4d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ldbxyzptlk/r/w;
    .registers 8

    .prologue
    .line 324
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/r/i;->c()Ldbxyzptlk/r/w;

    move-result-object v0

    .line 326
    if-eqz v0, :cond_3c

    .line 327
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v1

    .line 332
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v2

    .line 333
    invoke-virtual {v2}, Ldbxyzptlk/l/a;->j()Ldbxyzptlk/q/k;

    move-result-object v3

    .line 334
    if-eqz v3, :cond_39

    invoke-virtual {v1}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;

    move-result-object v4

    if-eqz v4, :cond_32

    invoke-virtual {v1}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;

    move-result-object v4

    iget-object v4, v4, Ldbxyzptlk/r/w;->h:Ljava/lang/String;

    if-eqz v4, :cond_32

    invoke-virtual {v1}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;

    move-result-object v4

    iget-object v4, v4, Ldbxyzptlk/r/w;->h:Ljava/lang/String;

    iget-object v5, v0, Ldbxyzptlk/r/w;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_39

    .line 335
    :cond_32
    iget-object v4, v0, Ldbxyzptlk/r/w;->h:Ljava/lang/String;

    iget-wide v5, v0, Ldbxyzptlk/r/w;->f:J

    invoke-virtual {v2, v4, v5, v6, v3}, Ldbxyzptlk/l/a;->a(Ljava/lang/String;JLdbxyzptlk/q/k;)V

    .line 338
    :cond_39
    invoke-virtual {v1, v0}, Ldbxyzptlk/l/m;->a(Ldbxyzptlk/r/w;)V

    .line 341
    :cond_3c
    return-object v0
.end method

.method public final declared-synchronized e()Z
    .registers 5

    .prologue
    .line 348
    monitor-enter p0

    :try_start_1
    invoke-static {}, Lcom/dropbox/android/util/T;->a()V

    .line 349
    sget-object v0, Lcom/dropbox/android/filemanager/a;->b:Ljava/lang/String;

    const-string v1, "Deauthenticating dropbox."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->c()Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_5f

    move-result v0

    if-nez v0, :cond_14

    .line 352
    const/4 v0, 0x0

    .line 379
    :goto_12
    monitor-exit p0

    return v0

    .line 354
    :cond_14
    :try_start_14
    invoke-static {}, Lcom/dropbox/android/util/i;->Q()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 356
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->a(Landroid/content/Context;)V

    .line 359
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    .line 360
    invoke-virtual {v0}, Ldbxyzptlk/l/m;->b()V

    .line 363
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/c;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 365
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 366
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->d()V

    .line 368
    invoke-static {}, Ldbxyzptlk/j/f;->a()V

    .line 370
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;)V

    .line 373
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/r/i;->a()Ldbxyzptlk/q/m;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/r/E;

    invoke-virtual {v0}, Ldbxyzptlk/r/E;->d()V

    .line 375
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/a;->i()V

    .line 376
    invoke-static {}, Lcom/dropbox/android/util/i;->an()V

    .line 378
    iget-object v0, p0, Lcom/dropbox/android/filemanager/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/content/Context;)V
    :try_end_5d
    .catchall {:try_start_14 .. :try_end_5d} :catchall_5f

    .line 379
    const/4 v0, 0x1

    goto :goto_12

    .line 348
    :catchall_5f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .registers 2

    .prologue
    .line 386
    monitor-enter p0

    :try_start_1
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/a;->k()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_14

    .line 387
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/a;->e()Z

    move-result v0

    .line 388
    if-eqz v0, :cond_14

    .line 389
    invoke-static {}, Lcom/dropbox/android/util/bi;->a()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    .line 392
    :cond_14
    monitor-exit p0

    return-void

    .line 386
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 399
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/l/m;->I()Ldbxyzptlk/l/t;

    move-result-object v1

    .line 400
    if-eqz v1, :cond_20

    .line 401
    invoke-virtual {v1}, Ldbxyzptlk/l/r;->d()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 403
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ldbxyzptlk/l/m;->a(Ldbxyzptlk/l/t;)V

    .line 405
    :cond_19
    invoke-virtual {v1}, Ldbxyzptlk/l/r;->d()Z

    move-result v1

    if-nez v1, :cond_20

    const/4 v0, 0x1

    .line 407
    :cond_20
    return v0
.end method

.method public final h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 417
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->I()Ldbxyzptlk/l/t;

    move-result-object v0

    .line 418
    if-nez v0, :cond_c

    .line 419
    const/4 v0, 0x0

    .line 421
    :goto_b
    return-object v0

    :cond_c
    invoke-virtual {v0}, Ldbxyzptlk/l/r;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_b
.end method

.method public final i()V
    .registers 3

    .prologue
    .line 428
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->I()Ldbxyzptlk/l/t;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 429
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->a(Ldbxyzptlk/l/t;)V

    .line 431
    :cond_12
    return-void
.end method
