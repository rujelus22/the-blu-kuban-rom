.class final Lcom/dropbox/android/filemanager/P;
.super Lcom/dropbox/android/filemanager/O;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Z)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 569
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/O;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    .line 570
    return-void
.end method

.method private b(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 582
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 583
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 584
    invoke-static {}, Lcom/dropbox/android/util/aU;->a()[B

    move-result-object v1

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 585
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/P;->d()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 586
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    int-to-long v2, p2

    invoke-static {v1, v2, v3, p3, v0}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 589
    :goto_1e
    return-object v0

    :cond_1f
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    int-to-long v2, p2

    invoke-static {v1, v2, v3, p3, v0}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1e
.end method


# virtual methods
.method protected final a(Landroid/content/Context;II)Landroid/graphics/Bitmap;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 574
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/P;->b(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 575
    if-nez v0, :cond_a

    .line 576
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/O;->a(Landroid/content/Context;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 578
    :cond_a
    return-object v0
.end method
