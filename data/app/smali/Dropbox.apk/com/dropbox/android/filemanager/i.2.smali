.class public final Lcom/dropbox/android/filemanager/i;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/E;


# static fields
.field private static final g:I

.field private static final j:Lcom/dropbox/android/taskqueue/v;

.field private static m:Ljava/lang/String;


# instance fields
.field protected a:Ljava/util/HashMap;

.field protected final b:Lcom/dropbox/android/util/H;

.field protected final c:Lcom/dropbox/android/filemanager/r;

.field protected final d:Ljava/util/HashMap;

.field protected e:Lcom/dropbox/android/filemanager/o;

.field protected f:Z

.field private h:I

.field private final i:Ldbxyzptlk/n/o;

.field private k:Ljava/util/concurrent/BlockingQueue;

.field private l:Ljava/util/concurrent/ThreadPoolExecutor;

.field private n:Ljava/util/HashMap;

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 38
    invoke-static {}, Lcom/dropbox/android/util/bc;->f()I

    move-result v0

    sput v0, Lcom/dropbox/android/filemanager/i;->g:I

    .line 75
    sget-object v0, Lcom/dropbox/android/taskqueue/v;->c:Lcom/dropbox/android/taskqueue/v;

    sput-object v0, Lcom/dropbox/android/filemanager/i;->j:Lcom/dropbox/android/taskqueue/v;

    .line 106
    const-class v0, Lcom/dropbox/android/filemanager/d;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/i;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/o;Lcom/dropbox/android/filemanager/i;)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    .line 66
    new-instance v0, Lcom/dropbox/android/filemanager/r;

    invoke-direct {v0, p0, v8}, Lcom/dropbox/android/filemanager/r;-><init>(Lcom/dropbox/android/filemanager/i;Lcom/dropbox/android/filemanager/j;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Lcom/dropbox/android/filemanager/r;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    .line 71
    iput-boolean v9, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/i;->h:I

    .line 99
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->k:Ljava/util/concurrent/BlockingQueue;

    .line 100
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/i;->k:Ljava/util/concurrent/BlockingQueue;

    new-instance v7, Lcom/dropbox/android/filemanager/q;

    invoke-direct {v7, v8}, Lcom/dropbox/android/filemanager/q;-><init>(Lcom/dropbox/android/filemanager/j;)V

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 109
    iput v9, p0, Lcom/dropbox/android/filemanager/i;->o:I

    .line 118
    iput-object p2, p0, Lcom/dropbox/android/filemanager/i;->e:Lcom/dropbox/android/filemanager/o;

    .line 119
    new-instance v0, Lcom/dropbox/android/util/H;

    sget v1, Lcom/dropbox/android/filemanager/i;->g:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/H;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    .line 120
    iget-object v0, p3, Lcom/dropbox/android/filemanager/i;->i:Ldbxyzptlk/n/o;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ldbxyzptlk/n/o;

    .line 121
    iget-object v0, p3, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    .line 122
    iput-object v8, p3, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    .line 123
    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/o;Ldbxyzptlk/n/o;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    .line 66
    new-instance v0, Lcom/dropbox/android/filemanager/r;

    invoke-direct {v0, p0, v2}, Lcom/dropbox/android/filemanager/r;-><init>(Lcom/dropbox/android/filemanager/i;Lcom/dropbox/android/filemanager/j;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Lcom/dropbox/android/filemanager/r;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    .line 71
    iput-boolean v8, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/filemanager/i;->h:I

    .line 99
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->k:Ljava/util/concurrent/BlockingQueue;

    .line 100
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/dropbox/android/filemanager/i;->k:Ljava/util/concurrent/BlockingQueue;

    new-instance v7, Lcom/dropbox/android/filemanager/q;

    invoke-direct {v7, v2}, Lcom/dropbox/android/filemanager/q;-><init>(Lcom/dropbox/android/filemanager/j;)V

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 109
    iput v8, p0, Lcom/dropbox/android/filemanager/i;->o:I

    .line 112
    new-instance v0, Lcom/dropbox/android/util/H;

    sget v1, Lcom/dropbox/android/filemanager/i;->g:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/H;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    .line 113
    iput-object p2, p0, Lcom/dropbox/android/filemanager/i;->e:Lcom/dropbox/android/filemanager/o;

    .line 114
    iput-object p3, p0, Lcom/dropbox/android/filemanager/i;->i:Ldbxyzptlk/n/o;

    .line 115
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/i;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->d()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/i;Lcom/dropbox/android/filemanager/p;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/i;->a(Lcom/dropbox/android/filemanager/p;I)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/i;Lcom/dropbox/android/filemanager/p;Landroid/util/Pair;I)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/i;->a(Lcom/dropbox/android/filemanager/p;Landroid/util/Pair;I)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/i;Ljava/lang/String;Ldbxyzptlk/n/o;Lcom/dropbox/android/taskqueue/m;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/i;->b(Ljava/lang/String;Ldbxyzptlk/n/o;Lcom/dropbox/android/taskqueue/m;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/i;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/i;->b(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/p;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 288
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/filemanager/i;->j:Lcom/dropbox/android/taskqueue/v;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/p;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/i;->i:Ldbxyzptlk/n/o;

    iget-object v5, p1, Lcom/dropbox/android/filemanager/p;->c:Landroid/content/res/Resources;

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;Landroid/content/res/Resources;)Landroid/util/Pair;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->c:Lcom/dropbox/android/filemanager/r;

    new-instance v2, Lcom/dropbox/android/filemanager/l;

    invoke-direct {v2, p0, p1, v0, p2}, Lcom/dropbox/android/filemanager/l;-><init>(Lcom/dropbox/android/filemanager/i;Lcom/dropbox/android/filemanager/p;Landroid/util/Pair;I)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/filemanager/r;->post(Ljava/lang/Runnable;)Z

    .line 297
    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/p;Landroid/util/Pair;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 300
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/android/taskqueue/D;

    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/D;->b:Z

    if-nez v0, :cond_1c

    .line 301
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    iget-object v1, p1, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/E;)V

    .line 302
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    :cond_1c
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    if-nez v0, :cond_74

    .line 306
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_7d

    .line 307
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_75

    .line 308
    new-instance v1, Lcom/dropbox/android/util/E;

    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/E;-><init>(Landroid/graphics/Bitmap;)V

    .line 309
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p3, v1}, Lcom/dropbox/android/util/H;->a(ILcom/dropbox/android/util/E;)V

    .line 310
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6e

    .line 311
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_54
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/s;

    .line 312
    const/4 v3, 0x0

    invoke-interface {v0, p3, v1, v3}, Lcom/dropbox/android/filemanager/s;->a(ILcom/dropbox/android/util/E;Z)V

    goto :goto_54

    .line 314
    :cond_65
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    :cond_6e
    invoke-virtual {v1}, Lcom/dropbox/android/util/E;->b()V

    .line 317
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    .line 326
    :cond_74
    :goto_74
    return-void

    .line 319
    :cond_75
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected Bitmap from cache."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_7d
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p3}, Lcom/dropbox/android/util/H;->b(I)V

    .line 323
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    goto :goto_74
.end method

.method private a(Ljava/lang/String;I)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 168
    const/4 v0, 0x0

    .line 169
    iget v1, p0, Lcom/dropbox/android/filemanager/i;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dropbox/android/filemanager/i;->o:I

    .line 170
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    if-eqz v1, :cond_2e

    .line 171
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 173
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/E;

    invoke-virtual {v1, p2, v0}, Lcom/dropbox/android/util/H;->a(ILcom/dropbox/android/util/E;)V

    .line 174
    const/4 v0, 0x1

    .line 176
    :cond_21
    iget v1, p0, Lcom/dropbox/android/filemanager/i;->o:I

    iget-object v2, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-le v1, v2, :cond_2e

    .line 177
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->b()V

    .line 180
    :cond_2e
    return v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    if-eqz v0, :cond_21

    .line 160
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/E;

    .line 161
    invoke-virtual {v0}, Lcom/dropbox/android/util/E;->b()V

    goto :goto_e

    .line 163
    :cond_1e
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    .line 165
    :cond_21
    return-void
.end method

.method private b(Ljava/lang/String;Ldbxyzptlk/n/o;Lcom/dropbox/android/taskqueue/m;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 378
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ldbxyzptlk/n/o;

    if-ne p2, v0, :cond_1c

    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 379
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/E;)V

    .line 382
    :cond_1c
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    .line 383
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->i:Ldbxyzptlk/n/o;

    if-ne p3, v0, :cond_57

    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 356
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 357
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    .line 358
    invoke-virtual {v0, p1, p0}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/E;)V

    .line 360
    iget-boolean v1, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    if-nez v1, :cond_57

    .line 361
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->e:Lcom/dropbox/android/filemanager/o;

    invoke-interface {v1, v6}, Lcom/dropbox/android/filemanager/o;->a(I)Lcom/dropbox/android/filemanager/p;

    move-result-object v2

    .line 362
    sget-object v1, Lcom/dropbox/android/filemanager/i;->j:Lcom/dropbox/android/taskqueue/v;

    iget-object v5, v2, Lcom/dropbox/android/filemanager/p;->c:Landroid/content/res/Resources;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;Landroid/content/res/Resources;)Landroid/util/Pair;

    move-result-object v0

    .line 364
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_57

    .line 365
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    instance-of v1, v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_5b

    .line 366
    new-instance v1, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 367
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Lcom/dropbox/android/filemanager/r;

    invoke-virtual {v0, v6, v1}, Lcom/dropbox/android/filemanager/r;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 374
    :cond_57
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    .line 375
    return-void

    .line 369
    :cond_5b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected Bitmap from cache."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 243
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Lcom/dropbox/android/filemanager/r;

    new-instance v1, Lcom/dropbox/android/filemanager/j;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/j;-><init>(Lcom/dropbox/android/filemanager/i;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/r;->post(Ljava/lang/Runnable;)Z

    .line 249
    return-void
.end method

.method private d()V
    .registers 6

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    if-nez v0, :cond_2f

    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_2f

    .line 257
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0}, Lcom/dropbox/android/util/H;->b()I

    move-result v0

    .line 259
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2f

    .line 260
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/util/H;->b(I)V

    .line 262
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->e:Lcom/dropbox/android/filemanager/o;

    invoke-interface {v1, v0}, Lcom/dropbox/android/filemanager/o;->a(I)Lcom/dropbox/android/filemanager/p;

    move-result-object v1

    .line 263
    if-eqz v1, :cond_58

    .line 265
    iget-object v2, v1, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lcom/dropbox/android/filemanager/i;->a(Ljava/lang/String;I)Z

    move-result v2

    .line 266
    if-eqz v2, :cond_30

    .line 267
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    .line 285
    :cond_2f
    :goto_2f
    return-void

    .line 271
    :cond_30
    iget-object v2, p0, Lcom/dropbox/android/filemanager/i;->d:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v2

    iget-object v3, v1, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/lang/String;Ljava/lang/ref/WeakReference;)V

    .line 274
    iget-object v2, p0, Lcom/dropbox/android/filemanager/i;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v3, Lcom/dropbox/android/filemanager/k;

    invoke-direct {v3, p0, v1, v0}, Lcom/dropbox/android/filemanager/k;-><init>(Lcom/dropbox/android/filemanager/i;Lcom/dropbox/android/filemanager/p;I)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_2f

    .line 281
    :cond_58
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    goto :goto_2f
.end method


# virtual methods
.method public final a(ILcom/dropbox/android/filemanager/s;)Lcom/dropbox/android/util/E;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 196
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->d(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 197
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->c(I)Lcom/dropbox/android/util/E;

    move-result-object v0

    .line 204
    :goto_e
    return-object v0

    .line 199
    :cond_f
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->e:Lcom/dropbox/android/filemanager/o;

    invoke-interface {v0, p1}, Lcom/dropbox/android/filemanager/o;->a(I)Lcom/dropbox/android/filemanager/p;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/filemanager/i;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 200
    if-eqz v0, :cond_24

    .line 201
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->c(I)Lcom/dropbox/android/util/E;

    move-result-object v0

    goto :goto_e

    .line 203
    :cond_24
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/filemanager/i;->b(ILcom/dropbox/android/filemanager/s;)V

    .line 204
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final a()V
    .registers 6

    .prologue
    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/i;->f:Z

    .line 140
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->b()V

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0}, Lcom/dropbox/android/util/H;->c()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 142
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    .line 143
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 144
    iget-object v3, p0, Lcom/dropbox/android/filemanager/i;->n:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/i;->e:Lcom/dropbox/android/filemanager/o;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v4, v1}, Lcom/dropbox/android/filemanager/o;->a(I)Lcom/dropbox/android/filemanager/p;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/filemanager/p;->a:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1f

    .line 147
    :cond_47
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0}, Lcom/dropbox/android/util/H;->a()V

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 149
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 184
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->b(I)V

    .line 185
    return-void
.end method

.method public final a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 189
    iput p2, p0, Lcom/dropbox/android/filemanager/i;->h:I

    .line 190
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/util/H;->a(II)V

    .line 191
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    .line 192
    return-void
.end method

.method public final a(Ljava/lang/String;Ldbxyzptlk/n/o;Lcom/dropbox/android/taskqueue/m;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 397
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Lcom/dropbox/android/filemanager/r;

    new-instance v1, Lcom/dropbox/android/filemanager/n;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/n;-><init>(Lcom/dropbox/android/filemanager/i;Ljava/lang/String;Ldbxyzptlk/n/o;Lcom/dropbox/android/taskqueue/m;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/r;->post(Ljava/lang/Runnable;)Z

    .line 403
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 387
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->c:Lcom/dropbox/android/filemanager/r;

    new-instance v1, Lcom/dropbox/android/filemanager/m;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/dropbox/android/filemanager/m;-><init>(Lcom/dropbox/android/filemanager/i;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/r;->post(Ljava/lang/Runnable;)Z

    .line 393
    return-void
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 231
    if-eqz p1, :cond_f

    .line 232
    iget v0, p0, Lcom/dropbox/android/filemanager/i;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_e

    .line 233
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    iget v1, p0, Lcom/dropbox/android/filemanager/i;->h:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/H;->a(I)V

    .line 239
    :cond_e
    :goto_e
    return-void

    .line 236
    :cond_f
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->b:Lcom/dropbox/android/util/H;

    sget v1, Lcom/dropbox/android/filemanager/i;->g:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/H;->a(I)V

    .line 237
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/i;->c()V

    goto :goto_e
.end method

.method protected final b(ILcom/dropbox/android/filemanager/s;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    :goto_1b
    return-void

    .line 214
    :cond_1c
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 215
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v1, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1b
.end method

.method public final c(ILcom/dropbox/android/filemanager/s;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 223
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 224
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 225
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    :cond_2a
    return-void
.end method

.method protected final finalize()V
    .registers 2

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/filemanager/i;->l:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    .line 95
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 97
    return-void

    .line 95
    :catchall_9
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
