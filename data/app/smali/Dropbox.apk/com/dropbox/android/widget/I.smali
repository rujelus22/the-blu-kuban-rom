.class public final Lcom/dropbox/android/widget/I;
.super Landroid/app/AlertDialog;
.source "panda.py"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ProgressBar;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 49
    iput-object p2, p0, Lcom/dropbox/android/widget/I;->h:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 104
    iget-object v2, p0, Lcom/dropbox/android/widget/I;->c:Landroid/widget/ProgressBar;

    if-gtz p1, :cond_22

    move v0, v1

    :goto_6
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 105
    iget-boolean v0, p0, Lcom/dropbox/android/widget/I;->f:Z

    if-eqz v0, :cond_24

    .line 106
    if-lez p1, :cond_1c

    iget-boolean v0, p0, Lcom/dropbox/android/widget/I;->g:Z

    if-nez v0, :cond_1c

    .line 109
    iput-boolean v1, p0, Lcom/dropbox/android/widget/I;->g:Z

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->c:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/dropbox/android/widget/I;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 112
    :cond_1c
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 116
    :goto_21
    return-void

    .line 104
    :cond_22
    const/4 v0, 0x0

    goto :goto_6

    .line 114
    :cond_24
    iput p1, p0, Lcom/dropbox/android/widget/I;->e:I

    goto :goto_21
.end method

.method public final b(I)V
    .registers 3
    .parameter

    .prologue
    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/I;->g:Z

    .line 120
    iput p1, p0, Lcom/dropbox/android/widget/I;->d:I

    .line 121
    return-void
.end method

.method public final dismiss()V
    .registers 2

    .prologue
    .line 126
    :try_start_0
    invoke-super {p0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_3} :catch_4

    .line 131
    :goto_3
    return-void

    .line 127
    :catch_4
    move-exception v0

    goto :goto_3
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 54
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/I;->requestWindowFeature(I)Z

    .line 55
    invoke-virtual {p0}, Lcom/dropbox/android/widget/I;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 57
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/I;->setCancelable(Z)V

    .line 59
    const v1, 0x7f030023

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 60
    const v0, 0x7f060066

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/I;->c:Landroid/widget/ProgressBar;

    .line 61
    const v0, 0x7f060064

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/I;->a:Landroid/widget/ImageView;

    .line 62
    const v0, 0x7f060065

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/I;->b:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f060067

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 65
    new-instance v2, Lcom/dropbox/android/widget/J;

    invoke-direct {v2, p0}, Lcom/dropbox/android/widget/J;-><init>(Lcom/dropbox/android/widget/I;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/I;->setTitle(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/I;->setView(Landroid/view/View;)V

    .line 74
    iget v0, p0, Lcom/dropbox/android/widget/I;->e:I

    if-lez v0, :cond_59

    .line 75
    iget v0, p0, Lcom/dropbox/android/widget/I;->e:I

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/I;->a(I)V

    .line 77
    :cond_59
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/widget/I;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/I;->h:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ar;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 81
    if-eqz v0, :cond_71

    .line 82
    iget-object v1, p0, Lcom/dropbox/android/widget/I;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    :cond_71
    iget-object v0, p0, Lcom/dropbox/android/widget/I;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/dropbox/android/widget/I;->h:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 89
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 93
    invoke-super {p0}, Landroid/app/AlertDialog;->onStart()V

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/I;->f:Z

    .line 95
    return-void
.end method

.method protected final onStop()V
    .registers 2

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/AlertDialog;->onStop()V

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/I;->f:Z

    .line 101
    return-void
.end method
