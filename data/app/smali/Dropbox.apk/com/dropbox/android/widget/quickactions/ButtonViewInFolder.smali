.class public Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private e:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 2
    .parameter

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 16
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 20
    const v0, 0x7f030057

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .registers 6
    .parameter

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.BROWSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 32
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    iget-object v2, p0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/LocalEntry;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 34
    const-string v1, "EXTRA_FILE_SCROLL_TO"

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 35
    const-string v1, "EXTRA_DONT_CLEAR_FLAGS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 36
    const/high16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 37
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 38
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 26
    const v0, 0x7f0b0174

    return v0
.end method
