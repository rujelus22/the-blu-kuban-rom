.class final enum Lcom/dropbox/android/widget/aG;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/aG;

.field public static final enum b:Lcom/dropbox/android/widget/aG;

.field public static final enum c:Lcom/dropbox/android/widget/aG;

.field private static final synthetic d:[Lcom/dropbox/android/widget/aG;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/dropbox/android/widget/aG;

    const-string v1, "SEPARATOR"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/aG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/aG;->a:Lcom/dropbox/android/widget/aG;

    .line 35
    new-instance v0, Lcom/dropbox/android/widget/aG;

    const-string v1, "CAMERA_STATUS"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/widget/aG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/aG;->b:Lcom/dropbox/android/widget/aG;

    .line 36
    new-instance v0, Lcom/dropbox/android/widget/aG;

    const-string v1, "TURN_ON"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/widget/aG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/aG;->c:Lcom/dropbox/android/widget/aG;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/widget/aG;

    sget-object v1, Lcom/dropbox/android/widget/aG;->a:Lcom/dropbox/android/widget/aG;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/widget/aG;->b:Lcom/dropbox/android/widget/aG;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/widget/aG;->c:Lcom/dropbox/android/widget/aG;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/widget/aG;->d:[Lcom/dropbox/android/widget/aG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/aG;
    .registers 2
    .parameter

    .prologue
    .line 33
    const-class v0, Lcom/dropbox/android/widget/aG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/aG;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/aG;
    .registers 1

    .prologue
    .line 33
    sget-object v0, Lcom/dropbox/android/widget/aG;->d:[Lcom/dropbox/android/widget/aG;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/aG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/aG;

    return-object v0
.end method
