.class public final enum Lcom/dropbox/android/widget/X;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/widget/X;

.field public static final enum b:Lcom/dropbox/android/widget/X;

.field public static final enum c:Lcom/dropbox/android/widget/X;

.field public static final enum d:Lcom/dropbox/android/widget/X;

.field public static final enum e:Lcom/dropbox/android/widget/X;

.field private static final synthetic f:[Lcom/dropbox/android/widget/X;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/dropbox/android/widget/X;

    const-string v1, "BROWSER"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/X;->a:Lcom/dropbox/android/widget/X;

    .line 31
    new-instance v0, Lcom/dropbox/android/widget/X;

    const-string v1, "BROWSER_DIRONLY"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/widget/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/X;->b:Lcom/dropbox/android/widget/X;

    .line 32
    new-instance v0, Lcom/dropbox/android/widget/X;

    const-string v1, "FAVORITES"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/widget/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/X;->c:Lcom/dropbox/android/widget/X;

    .line 33
    new-instance v0, Lcom/dropbox/android/widget/X;

    const-string v1, "GRID_GALLERY"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/widget/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/X;->d:Lcom/dropbox/android/widget/X;

    .line 34
    new-instance v0, Lcom/dropbox/android/widget/X;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/widget/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/widget/X;->e:Lcom/dropbox/android/widget/X;

    .line 29
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/widget/X;

    sget-object v1, Lcom/dropbox/android/widget/X;->a:Lcom/dropbox/android/widget/X;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/widget/X;->b:Lcom/dropbox/android/widget/X;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/widget/X;->c:Lcom/dropbox/android/widget/X;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/widget/X;->d:Lcom/dropbox/android/widget/X;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/widget/X;->e:Lcom/dropbox/android/widget/X;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/widget/X;->f:[Lcom/dropbox/android/widget/X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/widget/X;
    .registers 2
    .parameter

    .prologue
    .line 29
    const-class v0, Lcom/dropbox/android/widget/X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/X;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/widget/X;
    .registers 1

    .prologue
    .line 29
    sget-object v0, Lcom/dropbox/android/widget/X;->f:[Lcom/dropbox/android/widget/X;

    invoke-virtual {v0}, [Lcom/dropbox/android/widget/X;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/X;

    return-object v0
.end method
