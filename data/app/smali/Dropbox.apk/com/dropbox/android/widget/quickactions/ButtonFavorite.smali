.class public Lcom/dropbox/android/widget/quickactions/ButtonFavorite;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# static fields
.field protected static final e:Ljava/lang/String;


# instance fields
.field private f:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 12
    const-class v0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 4
    .parameter

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->f:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 19
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_11

    .line 20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t create a favorite button for a directory!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_11
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 31
    const v0, 0x7f030053

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .registers 5
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->f:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-nez v0, :cond_11

    const/4 v0, 0x1

    .line 42
    :goto_7
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->f:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/filemanager/LocalEntry;Z)V

    .line 43
    return-void

    .line 41
    :cond_11
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected final a(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 26
    check-cast p1, Landroid/widget/ToggleButton;

    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;->f:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    invoke-virtual {p1, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 27
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 36
    const v0, 0x7f0b016f

    return v0
.end method
