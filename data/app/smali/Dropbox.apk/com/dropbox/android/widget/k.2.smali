.class final Lcom/dropbox/android/widget/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/dropbox/android/filemanager/T;

.field final synthetic c:Lcom/dropbox/android/widget/j;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/j;ILcom/dropbox/android/filemanager/T;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 224
    iput-object p1, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iput p2, p0, Lcom/dropbox/android/widget/k;->a:I

    iput-object p3, p0, Lcom/dropbox/android/widget/k;->b:Lcom/dropbox/android/filemanager/T;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 9

    .prologue
    .line 229
    iget v0, p0, Lcom/dropbox/android/widget/k;->a:I

    iget-object v1, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v1, v1, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    invoke-static {v1}, Lcom/dropbox/android/widget/CameraUploadItemView;->e(Lcom/dropbox/android/widget/CameraUploadItemView;)I

    move-result v1

    if-ne v0, v1, :cond_54

    .line 230
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->b:Lcom/dropbox/android/filemanager/T;

    if-eqz v0, :cond_53

    .line 231
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v0, v0, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/widget/k;->b:Lcom/dropbox/android/filemanager/T;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-direct {v1, v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v0, v0, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v3, v3, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    iget-boolean v3, v3, Lcom/dropbox/android/widget/CameraUploadItemView;->a:Z

    iget-object v4, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v4, v4, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    invoke-static {v4}, Lcom/dropbox/android/widget/CameraUploadItemView;->d(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v5, v5, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    invoke-static {v5}, Lcom/dropbox/android/widget/CameraUploadItemView;->f(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/FrameLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v6, v6, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    invoke-static {v6}, Lcom/dropbox/android/widget/CameraUploadItemView;->g(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/widget/k;->c:Lcom/dropbox/android/widget/j;

    iget-object v7, v7, Lcom/dropbox/android/widget/j;->a:Lcom/dropbox/android/widget/CameraUploadItemView;

    invoke-static {v7}, Lcom/dropbox/android/widget/CameraUploadItemView;->h(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 239
    :cond_53
    :goto_53
    return-void

    .line 236
    :cond_54
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->b:Lcom/dropbox/android/filemanager/T;

    if-eqz v0, :cond_53

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/widget/k;->b:Lcom/dropbox/android/filemanager/T;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_53
.end method
