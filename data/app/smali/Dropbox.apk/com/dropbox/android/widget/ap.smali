.class final Lcom/dropbox/android/widget/ap;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/dropbox/android/filemanager/T;

.field final synthetic c:Lcom/dropbox/android/widget/ao;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/ao;ILcom/dropbox/android/filemanager/T;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    iput-object p1, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iput p2, p0, Lcom/dropbox/android/widget/ap;->a:I

    iput-object p3, p0, Lcom/dropbox/android/widget/ap;->b:Lcom/dropbox/android/filemanager/T;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 10

    .prologue
    .line 149
    iget v0, p0, Lcom/dropbox/android/widget/ap;->a:I

    iget-object v1, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v1, v1, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v1}, Lcom/dropbox/android/widget/an;->a(Lcom/dropbox/android/widget/an;)I

    move-result v1

    if-ne v0, v1, :cond_98

    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v0, v0, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v0}, Lcom/dropbox/android/widget/an;->b(Lcom/dropbox/android/widget/an;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_98

    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->b:Lcom/dropbox/android/filemanager/T;

    if-eqz v0, :cond_98

    .line 150
    new-instance v8, Lcom/dropbox/android/util/E;

    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->b:Lcom/dropbox/android/filemanager/T;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-direct {v8, v0}, Lcom/dropbox/android/util/E;-><init>(Landroid/graphics/Bitmap;)V

    .line 151
    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v0, v0, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v0, v8}, Lcom/dropbox/android/widget/an;->a(Lcom/dropbox/android/widget/an;Lcom/dropbox/android/util/E;)Lcom/dropbox/android/util/E;

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v0, v0, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v0}, Lcom/dropbox/android/widget/an;->c(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v0, v0, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v0}, Lcom/dropbox/android/widget/an;->d(Lcom/dropbox/android/widget/an;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v2, v2, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v2}, Lcom/dropbox/android/widget/an;->d(Lcom/dropbox/android/widget/an;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v8}, Lcom/dropbox/android/util/E;->c()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v3, v3, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v3}, Lcom/dropbox/android/widget/an;->e(Lcom/dropbox/android/widget/an;)Z

    move-result v3

    iget-object v4, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v4, v4, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v4}, Lcom/dropbox/android/widget/an;->f(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v5, v5, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v5}, Lcom/dropbox/android/widget/an;->g(Lcom/dropbox/android/widget/an;)Landroid/widget/FrameLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v6, v6, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v6}, Lcom/dropbox/android/widget/an;->h(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v7, v7, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v7}, Lcom/dropbox/android/widget/an;->i(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 161
    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v0, v0, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v0}, Lcom/dropbox/android/widget/an;->j(Lcom/dropbox/android/widget/an;)Lcom/dropbox/android/util/F;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/ap;->c:Lcom/dropbox/android/widget/ao;

    iget-object v1, v1, Lcom/dropbox/android/widget/ao;->a:Lcom/dropbox/android/widget/an;

    invoke-static {v1}, Lcom/dropbox/android/widget/an;->b(Lcom/dropbox/android/widget/an;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Lcom/dropbox/android/util/F;->a(Ljava/lang/String;Lcom/dropbox/android/util/E;)V

    .line 165
    :cond_97
    :goto_97
    return-void

    .line 162
    :cond_98
    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->b:Lcom/dropbox/android/filemanager/T;

    if-eqz v0, :cond_97

    .line 163
    iget-object v0, p0, Lcom/dropbox/android/widget/ap;->b:Lcom/dropbox/android/filemanager/T;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_97
.end method
