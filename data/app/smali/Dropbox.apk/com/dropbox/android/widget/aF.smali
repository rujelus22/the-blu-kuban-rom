.class public final Lcom/dropbox/android/widget/aF;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/h;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/aD;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Lcom/dropbox/android/taskqueue/Q;

.field private g:Z

.field private h:Ldbxyzptlk/k/i;

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/widget/aD;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 223
    iput-object p1, p0, Lcom/dropbox/android/widget/aF;->a:Lcom/dropbox/android/widget/aD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-boolean v0, p0, Lcom/dropbox/android/widget/aF;->i:Z

    .line 233
    iput-boolean v0, p0, Lcom/dropbox/android/widget/aF;->j:Z

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/aF;->k:Z

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 310
    const-string v2, "camera_upload_local_uri"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 311
    iget-object v3, p0, Lcom/dropbox/android/widget/aF;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_67

    .line 312
    iget-object v3, p0, Lcom/dropbox/android/widget/aF;->c:Ljava/lang/String;

    if-nez v3, :cond_1e

    .line 313
    iget-boolean v3, p0, Lcom/dropbox/android/widget/aF;->j:Z

    if-nez v3, :cond_55

    .line 317
    iput-boolean v0, p0, Lcom/dropbox/android/widget/aF;->j:Z

    .line 323
    :cond_1e
    :goto_1e
    iget-object v3, p0, Lcom/dropbox/android/widget/aF;->c:Ljava/lang/String;

    iput-object v3, p0, Lcom/dropbox/android/widget/aF;->b:Ljava/lang/String;

    .line 324
    iput-object v2, p0, Lcom/dropbox/android/widget/aF;->c:Ljava/lang/String;

    .line 325
    const-string v2, "camera_upload_local_mime_type"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/widget/aF;->d:Ljava/lang/String;

    .line 328
    invoke-static {}, Lcom/dropbox/android/filemanager/I;->a()Lcom/dropbox/android/filemanager/I;

    move-result-object v3

    .line 329
    const-string v2, "camera_upload_pending_paths_json"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 331
    :try_start_3e
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v2, v1

    .line 332
    :goto_44
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_5f

    .line 333
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v3, v5, v6}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;I)V
    :try_end_52
    .catch Lorg/json/JSONException; {:try_start_3e .. :try_end_52} :catch_58

    .line 332
    add-int/lit8 v2, v2, 0x1

    goto :goto_44

    .line 319
    :cond_55
    iput-boolean v0, p0, Lcom/dropbox/android/widget/aF;->i:Z

    goto :goto_1e

    .line 335
    :catch_58
    move-exception v0

    .line 337
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 340
    :cond_5f
    iget-object v2, p0, Lcom/dropbox/android/widget/aF;->a:Lcom/dropbox/android/widget/aD;

    invoke-static {v2}, Lcom/dropbox/android/widget/aD;->a(Lcom/dropbox/android/widget/aD;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/dropbox/android/widget/aF;->k:Z

    .line 343
    :cond_67
    const-string v2, "camera_upload_num_remaining"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/dropbox/android/widget/aF;->e:I

    .line 345
    const-string v2, "camera_upload_status"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 346
    invoke-static {v2}, Lcom/dropbox/android/taskqueue/Q;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/Q;

    move-result-object v2

    iput-object v2, p0, Lcom/dropbox/android/widget/aF;->f:Lcom/dropbox/android/taskqueue/Q;

    .line 347
    const-string v2, "id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 348
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_a9

    .line 349
    new-instance v4, Ldbxyzptlk/k/i;

    invoke-direct {v4, v2, v3}, Ldbxyzptlk/k/i;-><init>(J)V

    iput-object v4, p0, Lcom/dropbox/android/widget/aF;->h:Ldbxyzptlk/k/i;

    .line 354
    :goto_9a
    const-string v2, "camera_upload_initial_scan"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 355
    if-ne v2, v0, :cond_ad

    :goto_a6
    iput-boolean v0, p0, Lcom/dropbox/android/widget/aF;->g:Z

    .line 356
    return-void

    .line 351
    :cond_a9
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dropbox/android/widget/aF;->h:Ldbxyzptlk/k/i;

    goto :goto_9a

    :cond_ad
    move v0, v1

    .line 355
    goto :goto_a6
.end method

.method static synthetic a(Lcom/dropbox/android/widget/aF;Landroid/database/Cursor;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/aF;->a(Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/dropbox/android/widget/aF;->b:Ljava/lang/String;

    .line 241
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dropbox/android/widget/aF;->b:Ljava/lang/String;

    .line 242
    return-object v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/dropbox/android/widget/aF;->b:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/dropbox/android/widget/aF;->c:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/dropbox/android/widget/aF;->k:Z

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final c()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 252
    iget-boolean v1, p0, Lcom/dropbox/android/widget/aF;->i:Z

    if-eqz v1, :cond_c

    .line 255
    iput-boolean v0, p0, Lcom/dropbox/android/widget/aF;->i:Z

    .line 256
    iget-boolean v1, p0, Lcom/dropbox/android/widget/aF;->k:Z

    if-eqz v1, :cond_c

    .line 257
    const/4 v0, 0x1

    .line 260
    :cond_c
    return v0
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/dropbox/android/widget/aF;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/dropbox/android/widget/aF;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 290
    iget v0, p0, Lcom/dropbox/android/widget/aF;->e:I

    return v0
.end method

.method public final g()Lcom/dropbox/android/taskqueue/Q;
    .registers 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/dropbox/android/widget/aF;->f:Lcom/dropbox/android/taskqueue/Q;

    return-object v0
.end method

.method public final h()Z
    .registers 2

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/dropbox/android/widget/aF;->g:Z

    return v0
.end method

.method public final i()Ldbxyzptlk/k/i;
    .registers 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/dropbox/android/widget/aF;->h:Ldbxyzptlk/k/i;

    return-object v0
.end method
