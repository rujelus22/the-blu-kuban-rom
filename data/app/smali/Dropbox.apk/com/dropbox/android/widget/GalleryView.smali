.class public Lcom/dropbox/android/widget/GalleryView;
.super Landroid/widget/RelativeLayout;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/F/b;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:Ljava/util/ArrayList;

.field private B:Ljava/util/Map;

.field private C:I

.field private D:I

.field private E:Z

.field private final F:Ldbxyzptlk/F/a;

.field private final G:Ldbxyzptlk/F/c;

.field private final H:Ldbxyzptlk/F/c;

.field private I:Z

.field private J:F

.field private K:F

.field private L:F

.field private M:F

.field private N:F

.field private O:F

.field private P:F

.field private Q:F

.field private R:Z

.field private S:J

.field private T:I

.field private U:I

.field private b:Z

.field private c:Z

.field private d:Lcom/dropbox/android/widget/ac;

.field private final e:Ljava/lang/Runnable;

.field private final f:Ljava/lang/Runnable;

.field private final g:Lcom/dropbox/android/taskqueue/q;

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private final l:Lcom/dropbox/android/widget/ab;

.field private m:Z

.field private n:F

.field private o:J

.field private p:J

.field private q:Landroid/widget/TextView;

.field private r:Landroid/view/View;

.field private s:Landroid/graphics/drawable/Drawable;

.field private t:Landroid/graphics/drawable/Drawable;

.field private u:Landroid/widget/ProgressBar;

.field private v:Landroid/widget/ProgressBar;

.field private w:I

.field private x:I

.field private y:Lcom/dropbox/android/widget/aC;

.field private z:Lcom/dropbox/android/widget/aC;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 62
    const-class v0, Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/GalleryView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 213
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/widget/GalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 214
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/widget/GalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 218
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 221
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    .line 78
    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    .line 85
    iput-object v3, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ac;

    .line 87
    new-instance v0, Lcom/dropbox/android/widget/Z;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/Z;-><init>(Lcom/dropbox/android/widget/GalleryView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    .line 97
    new-instance v0, Lcom/dropbox/android/widget/aa;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aa;-><init>(Lcom/dropbox/android/widget/GalleryView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->f:Ljava/lang/Runnable;

    .line 125
    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->h:Z

    .line 133
    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    .line 134
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 140
    iput v1, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    .line 146
    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    .line 171
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->w:I

    .line 172
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->x:I

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    .line 176
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->B:Ljava/util/Map;

    .line 177
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    .line 178
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    .line 179
    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->E:Z

    .line 181
    new-instance v0, Ldbxyzptlk/F/a;

    invoke-direct {v0, p0}, Ldbxyzptlk/F/a;-><init>(Ldbxyzptlk/F/b;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:Ldbxyzptlk/F/a;

    .line 182
    new-instance v0, Ldbxyzptlk/F/c;

    invoke-direct {v0}, Ldbxyzptlk/F/c;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    .line 184
    new-instance v0, Ldbxyzptlk/F/c;

    invoke-direct {v0}, Ldbxyzptlk/F/c;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->H:Ldbxyzptlk/F/c;

    .line 186
    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->I:Z

    .line 202
    iput-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->R:Z

    .line 223
    new-instance v0, Lcom/dropbox/android/widget/ab;

    invoke-direct {v0, p0, v3}, Lcom/dropbox/android/widget/ab;-><init>(Lcom/dropbox/android/widget/GalleryView;Lcom/dropbox/android/widget/Z;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->l:Lcom/dropbox/android/widget/ab;

    .line 224
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->g:Lcom/dropbox/android/taskqueue/q;

    .line 226
    const/high16 v0, -0x100

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->setBackgroundColor(I)V

    .line 227
    return-void
.end method

.method private a(Lcom/dropbox/android/widget/ad;F)F
    .registers 6
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x4080

    .line 1130
    invoke-virtual {p1}, Lcom/dropbox/android/widget/ad;->e()F

    move-result v0

    .line 1132
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v1

    if-ne p1, v1, :cond_11

    .line 1133
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->F:Ldbxyzptlk/F/a;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/F/a;->a(FF)V

    .line 1136
    :cond_11
    invoke-static {p2, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1137
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1139
    return v0
.end method

.method private a(Lcom/dropbox/android/widget/ad;Z)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1108
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2d

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 1109
    :goto_15
    if-eqz p2, :cond_2c

    .line 1110
    const v1, 0x3d4ccccd

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 1111
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->d(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    sub-float/2addr v2, v0

    cmpg-float v2, v2, v1

    if-gez v2, :cond_2c

    .line 1112
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->d(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    sub-float/2addr v0, v1

    .line 1115
    :cond_2c
    return v0

    .line 1108
    :cond_2d
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->d(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    goto :goto_15
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->w:I

    return p1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/widget/ac;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ac;

    return-object v0
.end method

.method private a(F)V
    .registers 8
    .parameter

    .prologue
    const/high16 v2, 0x4000

    const/4 v4, 0x0

    .line 963
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(FF)Landroid/util/Pair;

    move-result-object v2

    .line 964
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move-object v0, p0

    move v3, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/GalleryView;->b(FFFFF)V

    .line 965
    return-void
.end method

.method private a(FF)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/high16 v5, 0x4000

    .line 953
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v3

    .line 954
    invoke-virtual {v3}, Lcom/dropbox/android/widget/ad;->f()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 955
    invoke-virtual {v3}, Lcom/dropbox/android/widget/ad;->e()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(F)V

    .line 960
    :goto_13
    return-void

    .line 957
    :cond_14
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->b(FF)Landroid/util/Pair;

    move-result-object v2

    .line 958
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ad;->e()F

    move-result v0

    const/high16 v3, 0x4040

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    sub-float v4, v0, p1

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    sub-float v5, v0, p2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/GalleryView;->b(FFFFF)V

    goto :goto_13
.end method

.method private a(FFFFF)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v6, 0x4000

    .line 986
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 988
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->f(Lcom/dropbox/android/widget/ad;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_13

    .line 1006
    :goto_12
    return-void

    .line 992
    :cond_13
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    .line 995
    const/high16 v2, 0x4080

    invoke-static {p3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 996
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/widget/ad;->e()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 998
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->l(Lcom/dropbox/android/widget/ad;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    sub-float/2addr v4, p1

    sub-float v5, v2, v1

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    add-float/2addr v3, p4

    .line 999
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v4

    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->m(Lcom/dropbox/android/widget/ad;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    sub-float/2addr v5, p2

    sub-float v1, v2, v1

    mul-float/2addr v1, v5

    add-float/2addr v1, v4

    add-float/2addr v1, p5

    .line 1001
    invoke-virtual {p0, v3, v1, v2}, Lcom/dropbox/android/widget/GalleryView;->setCurrentImagePosScale(FFF)V

    .line 1004
    invoke-direct {p0, v0, v2}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;F)V

    .line 1005
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    goto :goto_12
.end method

.method private a(I)V
    .registers 6
    .parameter

    .prologue
    .line 815
    if-ltz p1, :cond_4a

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_4a

    .line 819
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-lt p1, v0, :cond_4b

    const/4 v0, 0x1

    :goto_f
    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    .line 820
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-eq v0, p1, :cond_23

    .line 825
    invoke-static {}, Lcom/dropbox/android/util/i;->Y()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "image.index"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 827
    :cond_23
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    .line 828
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 831
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;F)V

    .line 833
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->k(Lcom/dropbox/android/widget/ad;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 835
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->o()V

    .line 837
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->n()V

    .line 838
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;F)V

    .line 840
    :cond_4a
    return-void

    .line 819
    :cond_4b
    const/4 v0, -0x1

    goto :goto_f
.end method

.method private a(Landroid/widget/ProgressBar;)V
    .registers 3
    .parameter

    .prologue
    .line 772
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 773
    return-void
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 4
    .parameter

    .prologue
    .line 936
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->q:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 937
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->r:Landroid/view/View;

    iget-boolean v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 938
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;Landroid/widget/ProgressBar;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/GalleryView;->b(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/GalleryView;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    return p1
.end method

.method private a(Lcom/dropbox/android/widget/ad;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v9, -0x1

    const/high16 v8, 0x4040

    const/high16 v7, -0x3fc0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1036
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1037
    iget-wide v5, p0, Lcom/dropbox/android/widget/GalleryView;->p:J

    sub-long v5, v0, v5

    long-to-float v2, v5

    .line 1038
    iput-wide v0, p0, Lcom/dropbox/android/widget/GalleryView;->p:J

    .line 1040
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-nez v0, :cond_18

    move v0, v4

    .line 1104
    :goto_17
    return v0

    .line 1047
    :cond_18
    const/4 v0, 0x0

    .line 1048
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-eq v1, v9, :cond_107

    .line 1049
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-nez v1, :cond_9f

    .line 1051
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v0, v4, :cond_95

    .line 1052
    invoke-direct {p0, p1, v3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;Z)F

    move-result v0

    neg-float v0, v0

    move v1, v0

    .line 1065
    :goto_2b
    const/high16 v0, 0x40c0

    div-float v0, v1, v0

    .line 1075
    const v2, 0x3dcccccd

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v5, v5

    mul-float/2addr v2, v5

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1076
    const v2, -0x42333333

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v5, v5

    mul-float/2addr v2, v5

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1077
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->n:F

    .line 1081
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-ne v0, v4, :cond_bb

    cmpg-float v0, v1, v8

    if-gez v0, :cond_bb

    .line 1082
    invoke-static {}, Lcom/dropbox/android/util/i;->K()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v5, "index"

    iget v6, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v6, v6, -0x1

    int-to-long v6, v6

    invoke-virtual {v0, v5, v6, v7}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 1083
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    .line 1084
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->k(Lcom/dropbox/android/widget/ad;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    move v0, v4

    .line 1095
    :goto_7a
    if-ne v0, v4, :cond_ff

    .line 1096
    invoke-direct {p0, v3}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    .line 1097
    iput v9, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 1101
    :goto_81
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 1103
    :goto_84
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v1, v2

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    invoke-static {p1, v1, v2, v3}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    goto :goto_17

    .line 1055
    :cond_95
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v0, v0

    invoke-direct {p0, p1, v3}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;Z)F

    move-result v1

    sub-float/2addr v0, v1

    move v1, v0

    goto :goto_2b

    .line 1057
    :cond_9f
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-ne v1, v4, :cond_ae

    .line 1058
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v0, v0

    invoke-direct {p0, p1, v4}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;Z)F

    move-result v1

    sub-float/2addr v0, v1

    move v1, v0

    goto/16 :goto_2b

    .line 1059
    :cond_ae
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_104

    .line 1060
    invoke-direct {p0, p1, v4}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;Z)F

    move-result v0

    neg-float v0, v0

    move v1, v0

    goto/16 :goto_2b

    .line 1086
    :cond_bb
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v5, 0x2

    if-ne v0, v5, :cond_f0

    cmpl-float v0, v1, v7

    if-lez v0, :cond_f0

    .line 1087
    invoke-static {}, Lcom/dropbox/android/util/i;->K()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v5, "index"

    iget v6, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    invoke-virtual {v0, v5, v6, v7}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 1088
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    .line 1089
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v5, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->k(Lcom/dropbox/android/widget/ad;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    move v0, v4

    .line 1090
    goto :goto_7a

    .line 1091
    :cond_f0
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-nez v0, :cond_101

    cmpg-float v0, v1, v8

    if-gez v0, :cond_101

    cmpl-float v0, v1, v7

    if-lez v0, :cond_101

    move v0, v4

    .line 1092
    goto/16 :goto_7a

    :cond_ff
    move v1, v2

    .line 1099
    goto :goto_81

    :cond_101
    move v0, v3

    goto/16 :goto_7a

    :cond_104
    move v1, v0

    goto/16 :goto_2b

    :cond_107
    move v1, v0

    move v0, v3

    goto/16 :goto_84
.end method

.method private b(Lcom/dropbox/android/widget/ad;Z)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1119
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2e

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 1120
    :goto_15
    if-eqz p2, :cond_2d

    .line 1121
    const v1, 0x3d4ccccd

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 1122
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->e(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    sub-float v2, v0, v2

    cmpg-float v2, v2, v1

    if-gez v2, :cond_2d

    .line 1123
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->e(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    add-float/2addr v0, v1

    .line 1126
    :cond_2d
    return v0

    .line 1119
    :cond_2e
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->e(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    goto :goto_15
.end method

.method static synthetic b(Lcom/dropbox/android/widget/GalleryView;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/GalleryView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->x:I

    return p1
.end method

.method private b(FF)Landroid/util/Pair;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4000

    .line 969
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v0

    .line 972
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    sub-float/2addr v1, p1

    .line 973
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    sub-float/2addr v2, p2

    .line 976
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    div-float/2addr v1, v3

    .line 977
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    div-float/2addr v2, v3

    .line 979
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->l(Lcom/dropbox/android/widget/ad;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    sub-float v1, v3, v1

    .line 980
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->m(Lcom/dropbox/android/widget/ad;)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    sub-float/2addr v0, v2

    .line 982
    new-instance v2, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private b(FFFFF)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1009
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->J:F

    .line 1010
    iput p2, p0, Lcom/dropbox/android/widget/GalleryView;->K:F

    .line 1011
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->L:F

    .line 1012
    iput p3, p0, Lcom/dropbox/android/widget/GalleryView;->M:F

    .line 1013
    iput p4, p0, Lcom/dropbox/android/widget/GalleryView;->N:F

    .line 1014
    iput p5, p0, Lcom/dropbox/android/widget/GalleryView;->O:F

    .line 1015
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/widget/GalleryView;->S:J

    .line 1016
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->P:F

    .line 1017
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->Q:F

    .line 1018
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->R:Z

    .line 1019
    return-void
.end method

.method private b(Landroid/widget/ProgressBar;)V
    .registers 3
    .parameter

    .prologue
    .line 776
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 777
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/GalleryView;Landroid/widget/ProgressBar;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/GalleryView;->a(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method private b(Lcom/dropbox/android/widget/ad;F)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x4000

    .line 1152
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;F)F

    move-result v2

    .line 1154
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v0

    if-ne p1, v0, :cond_10

    .line 1155
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->s()V

    .line 1158
    :cond_10
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    invoke-static {p1, v0, v1, v2}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    .line 1161
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    .line 1162
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    .line 1163
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_6c

    .line 1165
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->d(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_57

    .line 1166
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    div-float/2addr v0, v5

    .line 1174
    :cond_3b
    :goto_3b
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->h(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_86

    .line 1176
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->i(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_71

    .line 1177
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->h(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    div-float/2addr v1, v5

    .line 1185
    :cond_53
    :goto_53
    invoke-static {p1, v0, v1, v2}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    .line 1186
    return-void

    .line 1167
    :cond_57
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->e(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3b

    .line 1168
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v0, v0

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    div-float/2addr v3, v5

    sub-float/2addr v0, v3

    goto :goto_3b

    .line 1172
    :cond_6c
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v0, v0

    div-float/2addr v0, v5

    goto :goto_3b

    .line 1178
    :cond_71
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->j(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_53

    .line 1179
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v1, v1

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->h(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    div-float/2addr v3, v5

    sub-float/2addr v1, v3

    goto :goto_53

    .line 1183
    :cond_86
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v1, v1

    div-float/2addr v1, v5

    goto :goto_53
.end method

.method private b(I)Z
    .registers 4
    .parameter

    .prologue
    .line 877
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-eq p1, v0, :cond_b

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_d

    :cond_b
    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method static synthetic c(Lcom/dropbox/android/widget/GalleryView;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(I)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 883
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, v0, :cond_b

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method static synthetic c(Lcom/dropbox/android/widget/GalleryView;I)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/GalleryView;->c(I)Z

    move-result v0

    return v0
.end method

.method private d(I)V
    .registers 2
    .parameter

    .prologue
    .line 941
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    .line 942
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/widget/GalleryView;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->p()V

    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/taskqueue/q;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->g:Lcom/dropbox/android/taskqueue/q;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/GalleryView;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    return v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/GalleryView;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    return v0
.end method

.method static synthetic g()Ljava/lang/String;
    .registers 1

    .prologue
    .line 61
    sget-object v0, Lcom/dropbox/android/widget/GalleryView;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/GalleryView;)Lcom/dropbox/android/widget/ab;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->l:Lcom/dropbox/android/widget/ab;

    return-object v0
.end method

.method private h()V
    .registers 4

    .prologue
    const v2, 0x7f0200f1

    .line 643
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->s:Landroid/graphics/drawable/Drawable;

    .line 644
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->t:Landroid/graphics/drawable/Drawable;

    .line 646
    const v0, 0x7f060087

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/widget/ProgressBar;

    .line 647
    const v0, 0x7f060088

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Landroid/widget/ProgressBar;

    .line 649
    invoke-static {}, Lcom/dropbox/android/util/bc;->b()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 654
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setBackgroundResource(I)V

    .line 655
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setBackgroundResource(I)V

    .line 658
    :cond_43
    invoke-static {}, Lcom/dropbox/android/widget/aC;->a()Landroid/text/TextPaint;

    move-result-object v0

    .line 659
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 661
    new-instance v1, Lcom/dropbox/android/widget/aC;

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/widget/aC;-><init>(Landroid/text/TextPaint;Landroid/text/Layout$Alignment;)V

    iput-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->y:Lcom/dropbox/android/widget/aC;

    .line 662
    new-instance v1, Lcom/dropbox/android/widget/aC;

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/widget/aC;-><init>(Landroid/text/TextPaint;Landroid/text/Layout$Alignment;)V

    iput-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->z:Lcom/dropbox/android/widget/aC;

    .line 664
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->postInvalidate()V

    .line 665
    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/widget/GalleryView;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->t:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private i()V
    .registers 9

    .prologue
    const-wide/high16 v4, 0x3ff0

    .line 751
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/dropbox/android/widget/GalleryView;->S:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    mul-double/2addr v0, v4

    const-wide v2, 0x406f400000000000L

    div-double/2addr v0, v2

    .line 752
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    .line 755
    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v6, v0

    .line 757
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->N:F

    mul-float/2addr v0, v6

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->P:F

    sub-float v4, v0, v1

    .line 758
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->O:F

    mul-float/2addr v0, v6

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->Q:F

    sub-float v5, v0, v1

    .line 759
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->P:F

    add-float/2addr v0, v4

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->P:F

    .line 760
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->Q:F

    add-float/2addr v0, v5

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->Q:F

    .line 762
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->J:F

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->K:F

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->L:F

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->M:F

    iget v7, p0, Lcom/dropbox/android/widget/GalleryView;->L:F

    sub-float/2addr v3, v7

    mul-float/2addr v3, v6

    add-float/2addr v3, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/widget/GalleryView;->a(FFFFF)V

    .line 764
    const/high16 v0, 0x3f80

    sub-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f1a36e2eb1c432dL

    cmpg-double v0, v0, v2

    if-gez v0, :cond_61

    .line 765
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->R:Z

    .line 769
    :goto_60
    return-void

    .line 767
    :cond_61
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    goto :goto_60
.end method

.method static synthetic j(Lcom/dropbox/android/widget/GalleryView;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->s:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private j()V
    .registers 2

    .prologue
    .line 780
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/widget/ProgressBar;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->b(Landroid/widget/ProgressBar;)V

    .line 781
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Landroid/widget/ProgressBar;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->b(Landroid/widget/ProgressBar;)V

    .line 782
    return-void
.end method

.method static synthetic k(Lcom/dropbox/android/widget/GalleryView;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->w:I

    return v0
.end method

.method private k()V
    .registers 2

    .prologue
    .line 843
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    .line 844
    return-void
.end method

.method static synthetic l(Lcom/dropbox/android/widget/GalleryView;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->x:I

    return v0
.end method

.method private l()V
    .registers 4

    .prologue
    .line 847
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 848
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_19

    .line 849
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ad;->d()V

    .line 848
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 851
    :cond_19
    return-void
.end method

.method static synthetic m(Lcom/dropbox/android/widget/GalleryView;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->n()V

    return-void
.end method

.method private m()[I
    .registers 5

    .prologue
    .line 859
    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x4

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x5

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x6

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x7

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->D:I

    mul-int/lit8 v3, v3, 0x8

    add-int/2addr v2, v3

    aput v2, v0, v1

    return-object v0
.end method

.method private declared-synchronized n()V
    .registers 8

    .prologue
    .line 888
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->g:Lcom/dropbox/android/taskqueue/q;

    sget-object v1, Lcom/dropbox/android/taskqueue/v;->a:Lcom/dropbox/android/taskqueue/v;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;)V

    .line 890
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Z
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_4e

    if-nez v0, :cond_e

    .line 919
    :cond_c
    :goto_c
    monitor-exit p0

    return-void

    .line 894
    :cond_e
    :try_start_e
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 896
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->m()[I

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1b
    if-ge v1, v4, :cond_c

    aget v5, v3, v1

    .line 898
    if-ltz v5, :cond_23

    if-lt v5, v2, :cond_27

    .line 896
    :cond_23
    :goto_23
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1b

    .line 902
    :cond_27
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 904
    invoke-direct {p0, v5}, Lcom/dropbox/android/widget/GalleryView;->c(I)Z

    move-result v6

    .line 906
    if-eqz v6, :cond_4a

    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->f(Lcom/dropbox/android/widget/ad;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-nez v6, :cond_4a

    .line 909
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ad;->c()Lcom/dropbox/android/taskqueue/D;

    move-result-object v0

    .line 911
    iget-boolean v0, v0, Lcom/dropbox/android/taskqueue/D;->b:Z

    if-eqz v0, :cond_23

    invoke-direct {p0, v5}, Lcom/dropbox/android/widget/GalleryView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_23

    goto :goto_c

    .line 916
    :cond_4a
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ad;->b()V
    :try_end_4d
    .catchall {:try_start_e .. :try_end_4d} :catchall_4e

    goto :goto_23

    .line 888
    :catchall_4e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private o()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 922
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 923
    :goto_8
    if-ge v2, v3, :cond_2a

    .line 924
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->h:Z

    if-eqz v0, :cond_28

    move v0, v1

    .line 925
    :goto_f
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    sub-int/2addr v4, v0

    if-lt v2, v4, :cond_19

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/2addr v0, v4

    if-le v2, v0, :cond_24

    .line 926
    :cond_19
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ad;->d()V

    .line 923
    :cond_24
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 924
    :cond_28
    const/4 v0, 0x1

    goto :goto_f

    .line 929
    :cond_2a
    return-void
.end method

.method private p()V
    .registers 2

    .prologue
    .line 932
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->h:Z

    .line 933
    return-void
.end method

.method private q()Lcom/dropbox/android/widget/ad;
    .registers 3

    .prologue
    .line 945
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_17

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-ltz v0, :cond_17

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_19

    .line 946
    :cond_17
    const/4 v0, 0x0

    .line 948
    :goto_18
    return-object v0

    :cond_19
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    goto :goto_18
.end method

.method private r()V
    .registers 6

    .prologue
    const/4 v4, 0x2

    .line 1022
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1023
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1027
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_32

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_1d
    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    .line 1029
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_3b

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_2f
    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    .line 1031
    return-void

    .line 1027
    :cond_32
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1d

    .line 1029
    :cond_3b
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2f
.end method

.method private s()V
    .registers 3

    .prologue
    .line 1143
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v0

    .line 1145
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->f(Lcom/dropbox/android/widget/ad;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 1146
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ad;->f()Z

    move-result v0

    .line 1147
    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->I:Z

    .line 1149
    :cond_10
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 280
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    return v0
.end method

.method public final a(Ldbxyzptlk/F/c;)Lcom/dropbox/android/widget/ad;
    .registers 5
    .parameter

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    if-eqz v0, :cond_a

    .line 356
    :cond_8
    const/4 v0, 0x0

    .line 378
    :goto_9
    return-object v0

    .line 359
    :cond_a
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_35

    .line 360
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-lez v0, :cond_68

    .line 361
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 362
    invoke-virtual {p1}, Ldbxyzptlk/F/c;->f()F

    move-result v1

    invoke-virtual {p1}, Ldbxyzptlk/F/c;->g()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/ad;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_68

    .line 363
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    goto :goto_9

    .line 367
    :cond_35
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_68

    .line 368
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_68

    .line 369
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 370
    invoke-virtual {p1}, Ldbxyzptlk/F/c;->f()F

    move-result v1

    invoke-virtual {p1}, Ldbxyzptlk/F/c;->g()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/ad;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_68

    .line 371
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    goto :goto_9

    .line 377
    :cond_68
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    goto :goto_9
.end method

.method public final a(Lcom/dropbox/android/widget/ad;Ldbxyzptlk/F/c;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, -0x1

    const/high16 v6, 0x4140

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 388
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/widget/GalleryView;->o:J

    .line 389
    if-eqz p2, :cond_35

    .line 390
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v0, p2}, Ldbxyzptlk/F/c;->a(Ldbxyzptlk/F/c;)V

    .line 391
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v0}, Ldbxyzptlk/F/c;->h()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 393
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->H:Ldbxyzptlk/F/c;

    invoke-virtual {v0, p2}, Ldbxyzptlk/F/c;->a(Ldbxyzptlk/F/c;)V

    .line 394
    iput v3, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    .line 396
    iput v7, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 398
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    if-eqz v0, :cond_32

    .line 400
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 401
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 454
    :cond_32
    :goto_32
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 456
    :cond_35
    return-void

    .line 405
    :cond_36
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    if-eqz v0, :cond_3d

    .line 406
    iput-boolean v4, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    goto :goto_32

    .line 409
    :cond_3d
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v0}, Ldbxyzptlk/F/c;->f()F

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->H:Ldbxyzptlk/F/c;

    invoke-virtual {v1}, Ldbxyzptlk/F/c;->f()F

    move-result v1

    sub-float/2addr v0, v1

    .line 410
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v1}, Ldbxyzptlk/F/c;->g()F

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->H:Ldbxyzptlk/F/c;

    invoke-virtual {v2}, Ldbxyzptlk/F/c;->g()F

    move-result v2

    sub-float/2addr v1, v2

    .line 411
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    .line 412
    const/high16 v1, 0x4310

    cmpg-float v0, v0, v1

    if-gez v0, :cond_83

    .line 414
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 415
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 417
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    if-eqz v0, :cond_ad

    .line 418
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v0}, Ldbxyzptlk/F/c;->f()F

    move-result v0

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v1}, Ldbxyzptlk/F/c;->g()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->a(FF)V

    .line 419
    iput-boolean v4, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    .line 429
    :goto_7f
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    if-ne v0, v7, :cond_35

    .line 435
    :cond_83
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 436
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v1, v3, :cond_d8

    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;Z)F

    move-result v1

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v2, v2

    div-float/2addr v2, v6

    cmpl-float v1, v1, v2

    if-lez v1, :cond_d8

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-lez v1, :cond_d8

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->n:F

    const v2, 0x3dcccccd

    cmpg-float v1, v1, v2

    if-gez v1, :cond_d8

    .line 440
    iput v3, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    goto :goto_32

    .line 422
    :cond_ad
    iput-boolean v3, p0, Lcom/dropbox/android/widget/GalleryView;->m:Z

    .line 423
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v1}, Ldbxyzptlk/F/c;->f()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v2}, Ldbxyzptlk/F/c;->g()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/widget/ad;->a(II)Z

    move-result v0

    .line 425
    if-eqz v0, :cond_d5

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->f:Ljava/lang/Runnable;

    :goto_cf
    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/widget/GalleryView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_7f

    :cond_d5
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->e:Ljava/lang/Runnable;

    goto :goto_cf

    .line 441
    :cond_d8
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v1, v5, :cond_105

    invoke-direct {p0, v0, v3}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;Z)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v1, v1

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v2, v2

    div-float/2addr v2, v6

    sub-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_105

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_105

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->n:F

    const v1, -0x42333333

    cmpl-float v0, v0, v1

    if-lez v0, :cond_105

    .line 445
    iput v5, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    goto/16 :goto_32

    .line 447
    :cond_105
    iput v4, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 448
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    if-ne v0, v5, :cond_32

    .line 449
    invoke-static {}, Lcom/dropbox/android/util/i;->M()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->c()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;D)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    goto/16 :goto_32
.end method

.method public final a(Lcom/dropbox/android/widget/ad;Ldbxyzptlk/F/d;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 464
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v4

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v6

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v7

    const/4 v9, 0x0

    move-object v0, p2

    move v8, v5

    invoke-virtual/range {v0 .. v9}, Ldbxyzptlk/F/d;->a(FFZFZFFZF)V

    .line 467
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ldbxyzptlk/F/c;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    check-cast p1, Lcom/dropbox/android/widget/ad;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;Ldbxyzptlk/F/c;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ldbxyzptlk/F/d;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    check-cast p1, Lcom/dropbox/android/widget/ad;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;Ldbxyzptlk/F/d;)V

    return-void
.end method

.method public final a(Lcom/dropbox/android/widget/ad;Ldbxyzptlk/F/d;Ldbxyzptlk/F/c;)Z
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->c:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    if-eqz v0, :cond_a

    .line 475
    :cond_8
    const/4 v0, 0x1

    .line 605
    :goto_9
    return v0

    .line 477
    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 478
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_105

    .line 479
    invoke-virtual {p3}, Ldbxyzptlk/F/c;->a()Z

    move-result v0

    if-eqz v0, :cond_4d

    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-eqz v0, :cond_34

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->d(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    const/high16 v1, -0x3f60

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_34

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->e(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    add-int/lit8 v1, v1, 0x5

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4d

    .line 481
    :cond_34
    const/4 v0, 0x2

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    .line 482
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    .line 483
    invoke-static {}, Lcom/dropbox/android/util/i;->L()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "scale"

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    float-to-double v5, v2

    invoke-virtual {v0, v1, v5, v6}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;D)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 490
    :cond_4d
    :goto_4d
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->G:Ldbxyzptlk/F/c;

    invoke-virtual {v0, p3}, Ldbxyzptlk/F/c;->a(Ldbxyzptlk/F/c;)V

    .line 492
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6d

    .line 493
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ac;

    if-eqz v0, :cond_60

    .line 494
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ac;

    invoke-interface {v0}, Lcom/dropbox/android/widget/ac;->b()V

    .line 497
    :cond_60
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->f(Lcom/dropbox/android/widget/ad;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_6d

    .line 498
    invoke-virtual {p2}, Ldbxyzptlk/F/d;->c()F

    move-result v0

    .line 499
    invoke-direct {p0, p1, v0}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;F)V

    .line 503
    :cond_6d
    invoke-virtual {p2}, Ldbxyzptlk/F/d;->a()F

    move-result v0

    .line 504
    invoke-virtual {p2}, Ldbxyzptlk/F/d;->b()F

    move-result v1

    .line 506
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    sub-float v5, v0, v2

    .line 507
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    sub-float v6, v1, v2

    .line 509
    iget-wide v7, p0, Lcom/dropbox/android/widget/GalleryView;->o:J

    sub-long/2addr v7, v3

    long-to-float v2, v7

    .line 510
    const/high16 v7, 0x42c8

    cmpl-float v7, v2, v7

    if-lez v7, :cond_8d

    .line 511
    const/high16 v2, 0x42c8

    .line 514
    :cond_8d
    const/4 v7, 0x0

    cmpl-float v7, v2, v7

    if-eqz v7, :cond_98

    .line 515
    div-float v2, v5, v2

    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->n:F

    .line 516
    iput-wide v3, p0, Lcom/dropbox/android/widget/GalleryView;->o:J

    .line 519
    :cond_98
    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_115

    .line 538
    :goto_9d
    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_d0

    .line 557
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->d(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v5

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_b8

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->e(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v5

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_cc

    :cond_b8
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_15b

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_15b

    .line 568
    :cond_cc
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    .line 589
    :cond_d0
    :goto_d0
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->h(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1b4

    .line 591
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->i(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v6

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_191

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->j(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v6

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_191

    .line 593
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->h(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    const/high16 v2, 0x4000

    div-float/2addr v1, v2

    .line 601
    :cond_f8
    :goto_f8
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    .line 604
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 605
    const/4 v0, 0x1

    goto/16 :goto_9

    .line 485
    :cond_105
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4d

    .line 486
    invoke-virtual {p3}, Ldbxyzptlk/F/c;->a()Z

    move-result v0

    if-nez v0, :cond_4d

    .line 487
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->k:I

    goto/16 :goto_4d

    .line 522
    :cond_115
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_155

    .line 523
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    add-float/2addr v2, v0

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_13a

    .line 525
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v0, v0

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    sub-float/2addr v0, v2

    .line 527
    :cond_13a
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    sub-float v2, v0, v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_14f

    .line 529
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    const/high16 v2, 0x4000

    div-float/2addr v0, v2

    .line 535
    :cond_14f
    :goto_14f
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    goto/16 :goto_9d

    .line 533
    :cond_155
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    goto :goto_14f

    .line 570
    :cond_15b
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->d(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v5

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_171

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->e(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v5

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_185

    :cond_171
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->g(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_18b

    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_18b

    .line 581
    :cond_185
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    goto/16 :goto_d0

    .line 584
    :cond_18b
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    goto/16 :goto_d0

    .line 594
    :cond_191
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->i(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v6

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_f8

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->j(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    add-float/2addr v2, v6

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_f8

    .line 596
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    int-to-float v1, v1

    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->h(Lcom/dropbox/android/widget/ad;)F

    move-result v2

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    goto/16 :goto_f8

    .line 599
    :cond_1b4
    invoke-static {p1}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    goto/16 :goto_f8
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ldbxyzptlk/F/d;Ldbxyzptlk/F/c;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    check-cast p1, Lcom/dropbox/android/widget/ad;

    invoke-virtual {p0, p1, p2, p3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;Ldbxyzptlk/F/d;Ldbxyzptlk/F/c;)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 2

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v0

    .line 285
    if-nez v0, :cond_8

    .line 286
    const/4 v0, 0x0

    .line 288
    :goto_7
    return-object v0

    :cond_8
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ad;->a()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    goto :goto_7
.end method

.method public final synthetic b(Ldbxyzptlk/F/c;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/GalleryView;->a(Ldbxyzptlk/F/c;)Lcom/dropbox/android/widget/ad;

    move-result-object v0

    return-object v0
.end method

.method public final c()F
    .registers 2

    .prologue
    .line 293
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v0

    .line 294
    if-nez v0, :cond_9

    .line 295
    const/high16 v0, 0x3f80

    .line 297
    :goto_8
    return v0

    :cond_9
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v0

    goto :goto_8
.end method

.method public final d()[F
    .registers 5

    .prologue
    const/4 v2, 0x2

    .line 302
    new-array v0, v2, [F

    .line 303
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v1

    .line 304
    if-nez v1, :cond_f

    .line 305
    new-array v0, v2, [F

    fill-array-data v0, :array_1e

    .line 309
    :goto_e
    return-object v0

    .line 307
    :cond_f
    const/4 v2, 0x0

    invoke-static {v1}, Lcom/dropbox/android/widget/ad;->b(Lcom/dropbox/android/widget/ad;)F

    move-result v3

    aput v3, v0, v2

    .line 308
    const/4 v2, 0x1

    invoke-static {v1}, Lcom/dropbox/android/widget/ad;->c(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    aput v1, v0, v2

    goto :goto_e

    .line 305
    :array_1e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public final e()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 318
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-lez v0, :cond_17

    .line 319
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_18

    .line 320
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 324
    :cond_d
    :goto_d
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-nez v0, :cond_14

    .line 325
    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    .line 327
    :cond_14
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 329
    :cond_17
    return-void

    .line 321
    :cond_18
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_d

    .line 322
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    goto :goto_d
.end method

.method public final f()V
    .registers 4

    .prologue
    const/4 v2, 0x2

    .line 332
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1f

    .line 333
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_20

    .line 334
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    .line 338
    :cond_15
    :goto_15
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-nez v0, :cond_1c

    .line 339
    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/GalleryView;->d(I)V

    .line 341
    :cond_1c
    invoke-virtual {p0}, Lcom/dropbox/android/widget/GalleryView;->invalidate()V

    .line 343
    :cond_1f
    return-void

    .line 335
    :cond_20
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_15

    .line 336
    iput v2, p0, Lcom/dropbox/android/widget/GalleryView;->j:I

    goto :goto_15
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 623
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->r()V

    .line 624
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_20

    .line 625
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 626
    iget-boolean v1, p0, Lcom/dropbox/android/widget/GalleryView;->I:Z

    if-nez v1, :cond_21

    .line 627
    invoke-virtual {v0}, Lcom/dropbox/android/widget/ad;->e()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;F)V

    .line 632
    :cond_20
    :goto_20
    return-void

    .line 629
    :cond_21
    invoke-static {v0}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;F)V

    goto :goto_20
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 674
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 676
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_f

    .line 748
    :cond_e
    :goto_e
    return-void

    .line 679
    :cond_f
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    .line 681
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->s()V

    .line 684
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    .line 686
    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;)Z

    move-result v3

    .line 687
    if-eqz v3, :cond_2a

    iget-boolean v4, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    if-eqz v4, :cond_2a

    .line 688
    iput-boolean v2, p0, Lcom/dropbox/android/widget/GalleryView;->b:Z

    .line 691
    :cond_2a
    if-ne v1, v6, :cond_bd

    .line 692
    if-eqz v3, :cond_93

    .line 693
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/ad;

    .line 694
    if-eq v1, v0, :cond_136

    .line 696
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-static {v1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v4

    invoke-static {v1, v0, v3, v4}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    :goto_4b
    move-object v0, v1

    .line 723
    :cond_4c
    :goto_4c
    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/ad;->a(Landroid/graphics/Canvas;)V

    .line 725
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_114

    move v5, v6

    .line 726
    :goto_56
    if-eqz v5, :cond_117

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->y:Lcom/dropbox/android/widget/aC;

    move-object v4, v1

    .line 727
    :goto_5b
    if-eqz v5, :cond_11c

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->z:Lcom/dropbox/android/widget/aC;

    move-object v3, v1

    .line 729
    :goto_60
    if-eqz v5, :cond_121

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/widget/ProgressBar;

    move-object v2, v1

    .line 730
    :goto_65
    if-eqz v5, :cond_126

    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->v:Landroid/widget/ProgressBar;

    .line 732
    :goto_69
    invoke-virtual {v0, v4, v2, p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/aC;Landroid/widget/ProgressBar;Landroid/graphics/Canvas;)V

    .line 734
    const/4 v0, -0x1

    .line 735
    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v2, v7, :cond_12a

    .line 736
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v0, v0, 0x1

    .line 741
    :cond_75
    :goto_75
    if-ltz v0, :cond_8a

    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_8a

    .line 742
    iget-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ad;

    invoke-virtual {v0, v3, v1, p1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/aC;Landroid/widget/ProgressBar;Landroid/graphics/Canvas;)V

    .line 745
    :cond_8a
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->R:Z

    if-eqz v0, :cond_e

    .line 746
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->i()V

    goto/16 :goto_e

    .line 699
    :cond_93
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    if-lez v1, :cond_4c

    .line 700
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/ad;

    .line 701
    invoke-direct {p0, v0, v6}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;Z)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 702
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v1}, Lcom/dropbox/android/widget/ad;->e()F

    move-result v5

    invoke-static {v1, v3, v4, v5}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    .line 703
    invoke-virtual {v1, p1}, Lcom/dropbox/android/widget/ad;->a(Landroid/graphics/Canvas;)V

    goto :goto_4c

    .line 706
    :cond_bd
    if-ne v1, v7, :cond_4c

    .line 707
    if-eqz v3, :cond_e1

    .line 708
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/ad;

    .line 709
    if-eq v1, v0, :cond_134

    .line 711
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-static {v1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;)F

    move-result v4

    invoke-static {v1, v0, v3, v4}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    :goto_de
    move-object v0, v1

    .line 713
    goto/16 :goto_4c

    .line 714
    :cond_e1
    iget v1, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    iget-object v3, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_4c

    .line 715
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    iget v3, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/ad;

    .line 716
    invoke-direct {p0, v0, v6}, Lcom/dropbox/android/widget/GalleryView;->b(Lcom/dropbox/android/widget/ad;Z)F

    move-result v3

    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    .line 718
    iget v4, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v1}, Lcom/dropbox/android/widget/ad;->e()F

    move-result v5

    invoke-static {v1, v3, v4, v5}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    .line 719
    invoke-virtual {v1, p1}, Lcom/dropbox/android/widget/ad;->a(Landroid/graphics/Canvas;)V

    goto/16 :goto_4c

    :cond_114
    move v5, v2

    .line 725
    goto/16 :goto_56

    .line 726
    :cond_117
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->z:Lcom/dropbox/android/widget/aC;

    move-object v4, v1

    goto/16 :goto_5b

    .line 727
    :cond_11c
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->y:Lcom/dropbox/android/widget/aC;

    move-object v3, v1

    goto/16 :goto_60

    .line 729
    :cond_121
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->v:Landroid/widget/ProgressBar;

    move-object v2, v1

    goto/16 :goto_65

    .line 730
    :cond_126
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/widget/ProgressBar;

    goto/16 :goto_69

    .line 737
    :cond_12a
    iget v2, p0, Lcom/dropbox/android/widget/GalleryView;->i:I

    if-ne v2, v6, :cond_75

    .line 738
    iget v0, p0, Lcom/dropbox/android/widget/GalleryView;->C:I

    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_75

    :cond_134
    move-object v1, v0

    goto :goto_de

    :cond_136
    move-object v1, v0

    goto/16 :goto_4b
.end method

.method protected onFinishInflate()V
    .registers 2

    .prologue
    .line 636
    const v0, 0x7f060086

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->q:Landroid/widget/TextView;

    .line 637
    const v0, 0x7f06008a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/GalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->r:Landroid/view/View;

    .line 639
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->h()V

    .line 640
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 794
    iput p2, p0, Lcom/dropbox/android/widget/GalleryView;->U:I

    .line 795
    iput p1, p0, Lcom/dropbox/android/widget/GalleryView;->T:I

    .line 796
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 614
    iget-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->R:Z

    if-eqz v0, :cond_6

    .line 616
    const/4 v0, 0x1

    .line 618
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->F:Ldbxyzptlk/F/a;

    invoke-virtual {v0, p1}, Ldbxyzptlk/F/a;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_5
.end method

.method public onWindowFocusChanged(Z)V
    .registers 2
    .parameter

    .prologue
    .line 786
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowFocusChanged(Z)V

    .line 787
    if-nez p1, :cond_8

    .line 788
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->j()V

    .line 790
    :cond_8
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 3
    .parameter

    .prologue
    .line 800
    if-nez p1, :cond_c

    .line 801
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Z

    .line 802
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->r()V

    .line 803
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->k()V

    .line 808
    :goto_b
    return-void

    .line 805
    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/GalleryView;->E:Z

    .line 806
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->l()V

    goto :goto_b
.end method

.method public setCurrentImagePosScale(FFF)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->q()Lcom/dropbox/android/widget/ad;

    move-result-object v0

    .line 263
    if-eqz v0, :cond_16

    .line 264
    invoke-direct {p0, v0, p3}, Lcom/dropbox/android/widget/GalleryView;->a(Lcom/dropbox/android/widget/ad;F)F

    move-result v1

    .line 265
    invoke-static {v0, p1, p2, v1}, Lcom/dropbox/android/widget/ad;->a(Lcom/dropbox/android/widget/ad;FFF)Z

    .line 266
    invoke-direct {p0}, Lcom/dropbox/android/widget/GalleryView;->s()V

    .line 267
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->l:Lcom/dropbox/android/widget/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ab;->sendEmptyMessage(I)Z

    .line 269
    :cond_16
    return-void
.end method

.method public setImages(Ljava/lang/Iterable;Ldbxyzptlk/n/o;I)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 235
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 236
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 238
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 240
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->B:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 242
    iget-object v1, p0, Lcom/dropbox/android/widget/GalleryView;->B:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/widget/ad;

    .line 246
    :goto_2a
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_e

    .line 244
    :cond_31
    new-instance v1, Lcom/dropbox/android/widget/ad;

    invoke-direct {v1, p0, v0, p2}, Lcom/dropbox/android/widget/ad;-><init>(Lcom/dropbox/android/widget/GalleryView;Lcom/dropbox/android/filemanager/LocalEntry;Ldbxyzptlk/n/o;)V

    goto :goto_2a

    .line 250
    :cond_37
    invoke-static {}, Lcom/dropbox/android/util/i;->J()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "size"

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "index"

    int-to-long v4, p3

    invoke-virtual {v0, v1, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 252
    iput-object v2, p0, Lcom/dropbox/android/widget/GalleryView;->A:Ljava/util/ArrayList;

    .line 253
    iput-object v3, p0, Lcom/dropbox/android/widget/GalleryView;->B:Ljava/util/Map;

    .line 256
    invoke-direct {p0, p3}, Lcom/dropbox/android/widget/GalleryView;->a(I)V

    .line 258
    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->l:Lcom/dropbox/android/widget/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ab;->sendEmptyMessage(I)Z

    .line 259
    return-void
.end method

.method public setTouchListener(Lcom/dropbox/android/widget/ac;)V
    .registers 2
    .parameter

    .prologue
    .line 272
    iput-object p1, p0, Lcom/dropbox/android/widget/GalleryView;->d:Lcom/dropbox/android/widget/ac;

    .line 273
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter

    .prologue
    .line 669
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/dropbox/android/widget/GalleryView;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method
