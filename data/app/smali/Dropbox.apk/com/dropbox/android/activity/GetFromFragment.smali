.class public Lcom/dropbox/android/activity/GetFromFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/g/i;


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    const-class v0, Lcom/dropbox/android/activity/GetFromFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/GetFromFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GetFromFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_HIDE_QUICKACTIONS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 42
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 91
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 92
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Z

    move-result v1

    if-nez v1, :cond_18

    .line 93
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    .line 95
    :goto_17
    return-object v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method protected final a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 5
    .parameter

    .prologue
    .line 107
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->b(Ljava/lang/String;)V

    .line 109
    new-instance v0, Ldbxyzptlk/g/h;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ldbxyzptlk/g/h;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 110
    invoke-virtual {v0, p0}, Ldbxyzptlk/g/h;->a(Ldbxyzptlk/g/i;)V

    .line 111
    invoke-virtual {v0}, Ldbxyzptlk/g/h;->f()V

    .line 112
    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Ldbxyzptlk/g/h;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Landroid/support/v4/app/j;)V

    .line 113
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 114
    return-void
.end method

.method public final a(Ljava/io/File;Landroid/content/Context;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v2

    .line 123
    invoke-static {}, Lcom/dropbox/android/util/i;->O()Lcom/dropbox/android/util/s;

    move-result-object v3

    const-string v4, "request.mime.type"

    invoke-virtual {v3, v4, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v3, "result.mime.type"

    invoke-virtual {v0, v3, v1}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v3, "caller"

    if-eqz v2, :cond_57

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    :goto_34
    invoke-virtual {v1, v3, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 127
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 128
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 129
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 130
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 131
    return-void

    .line 123
    :cond_57
    const/4 v0, 0x0

    goto :goto_34
.end method

.method protected final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 81
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/widget/X;
    .registers 2

    .prologue
    .line 86
    sget-object v0, Lcom/dropbox/android/widget/X;->a:Lcom/dropbox/android/widget/X;

    return-object v0
.end method

.method protected final d()I
    .registers 2

    .prologue
    .line 101
    const v0, 0x7f0b003e

    return v0
.end method

.method protected final e()I
    .registers 2

    .prologue
    .line 54
    const v0, 0x7f030029

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 61
    new-instance v0, Lcom/dropbox/android/activity/ar;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ar;-><init>(Lcom/dropbox/android/activity/GetFromFragment;)V

    .line 67
    iget-object v1, p0, Lcom/dropbox/android/activity/GetFromFragment;->k:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget-object v0, p0, Lcom/dropbox/android/activity/GetFromFragment;->k:Landroid/widget/Button;

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 70
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GetFromFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 71
    const-string v1, "PROMPT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    if-nez v0, :cond_2c

    .line 73
    const v0, 0x7f0b0160

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GetFromFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    :cond_2c
    iget-object v1, p0, Lcom/dropbox/android/activity/GetFromFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 47
    const v0, 0x7f060030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/GetFromFragment;->k:Landroid/widget/Button;

    .line 48
    const v0, 0x7f060075

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/GetFromFragment;->l:Landroid/widget/TextView;

    .line 49
    return-object v1
.end method
