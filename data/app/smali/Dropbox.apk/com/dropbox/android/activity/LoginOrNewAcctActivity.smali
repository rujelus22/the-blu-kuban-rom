.class public Lcom/dropbox/android/activity/LoginOrNewAcctActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/C;
.implements Lcom/dropbox/android/activity/Q;
.implements Lcom/dropbox/android/activity/aP;
.implements Lcom/dropbox/android/activity/aU;
.implements Lcom/dropbox/android/activity/ab;
.implements Lcom/dropbox/android/activity/bk;
.implements Ldbxyzptlk/g/A;
.implements Ldbxyzptlk/g/M;
.implements Ldbxyzptlk/g/m;
.implements Ldbxyzptlk/g/q;


# instance fields
.field private a:Lcom/dropbox/android/activity/bc;

.field private b:Lcom/dropbox/android/activity/bd;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/bd;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->c:Z

    .line 392
    return-void
.end method

.method private a(I)V
    .registers 4
    .parameter

    .prologue
    .line 674
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 675
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 676
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 524
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;Ljava/lang/String;)V

    .line 525
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;ILjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v0

    .line 529
    invoke-virtual {v0, p2, p1, p3}, Landroid/support/v4/app/u;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 530
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/support/v4/app/u;->a(I)Landroid/support/v4/app/u;

    .line 531
    invoke-virtual {v0, p4}, Landroid/support/v4/app/u;->a(Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 532
    invoke-virtual {v0}, Landroid/support/v4/app/u;->c()I

    .line 533
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 517
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 521
    :goto_d
    return-void

    .line 519
    :cond_e
    const v0, 0x7f0600b5

    invoke-direct {p0, p1, v0, p2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    goto :goto_d
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginOrNewAcctActivity;Lcom/dropbox/android/activity/bd;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Lcom/dropbox/android/activity/bd;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/activity/bd;)V
    .registers 9
    .parameter

    .prologue
    .line 662
    new-instance v0, Ldbxyzptlk/g/j;

    iget-object v2, p1, Lcom/dropbox/android/activity/bd;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/activity/bd;->b:Ljava/lang/String;

    iget-object v4, p1, Lcom/dropbox/android/activity/bd;->c:Ljava/lang/String;

    iget-object v5, p1, Lcom/dropbox/android/activity/bd;->d:Ljava/lang/String;

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/g/j;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 669
    sget-object v1, Lcom/dropbox/android/activity/aW;->a:Lcom/dropbox/android/activity/aW;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/aW;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/j;->a(I)V

    .line 670
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/j;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 671
    return-void
.end method

.method private b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 536
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v0

    .line 537
    const v1, 0x7f0600f9

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v4/app/u;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 538
    const/16 v1, 0x1003

    invoke-virtual {v0, v1}, Landroid/support/v4/app/u;->a(I)Landroid/support/v4/app/u;

    .line 539
    invoke-virtual {v0}, Landroid/support/v4/app/u;->c()I

    .line 540
    return-void
.end method

.method private m()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 346
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    .line 347
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 348
    if-eqz v0, :cond_1e

    instance-of v1, v0, Landroid/content/Intent;

    if-eqz v1, :cond_1e

    .line 349
    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->startActivity(Landroid/content/Intent;)V

    .line 355
    :cond_1a
    :goto_1a
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->finish()V

    .line 356
    return-void

    .line 350
    :cond_1e
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 351
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0, v2}, Ldbxyzptlk/l/m;->i(Z)V

    .line 352
    new-array v0, v2, [Lcom/dropbox/android/activity/bT;

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/android/activity/bT;->g:Lcom/dropbox/android/activity/bT;

    aput-object v2, v0, v1

    .line 353
    invoke-static {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;[Lcom/dropbox/android/activity/bT;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1a
.end method

.method private n()V
    .registers 7

    .prologue
    const v5, 0x7f0600b5

    .line 405
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v2

    .line 407
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 410
    sget-object v0, Lcom/dropbox/android/activity/aV;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_80

    .line 418
    new-instance v1, Lcom/dropbox/android/activity/LoginFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/LoginFragment;-><init>()V

    .line 419
    sget-object v0, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    .line 423
    :goto_29
    new-instance v3, Lcom/dropbox/android/activity/LoginBrandFragment;

    invoke-direct {v3}, Lcom/dropbox/android/activity/LoginBrandFragment;-><init>()V

    sget-object v4, Lcom/dropbox/android/activity/LoginBrandFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v3, v4}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 424
    const v3, 0x7f0600f9

    invoke-virtual {v2, v3, v1, v0}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 446
    :goto_39
    invoke-virtual {v2}, Landroid/support/v4/app/u;->b()I

    .line 449
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    sget-object v1, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/bc;

    if-ne v0, v1, :cond_4f

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->g()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 450
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e()V

    .line 452
    :cond_4f
    return-void

    .line 412
    :pswitch_50
    new-instance v1, Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/NewAccountFragment;-><init>()V

    .line 413
    sget-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    goto :goto_29

    .line 428
    :cond_58
    sget-object v0, Lcom/dropbox/android/activity/aV;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_86

    .line 439
    :pswitch_65
    new-instance v1, Lcom/dropbox/android/activity/LoginBrandFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/LoginBrandFragment;-><init>()V

    .line 440
    sget-object v0, Lcom/dropbox/android/activity/LoginBrandFragment;->a:Ljava/lang/String;

    .line 443
    :goto_6c
    invoke-virtual {v2, v5, v1, v0}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    goto :goto_39

    .line 430
    :pswitch_70
    new-instance v1, Lcom/dropbox/android/activity/LoginFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/LoginFragment;-><init>()V

    .line 431
    sget-object v0, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    goto :goto_6c

    .line 434
    :pswitch_78
    new-instance v1, Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/NewAccountFragment;-><init>()V

    .line 435
    sget-object v0, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    goto :goto_6c

    .line 410
    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_50
    .end packed-switch

    .line 428
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_78
        :pswitch_65
        :pswitch_70
    .end packed-switch
.end method

.method private o()Z
    .registers 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    sget-object v1, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/bc;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private p()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 549
    invoke-static {}, Lcom/dropbox/android/activity/DropboxBrowser;->b()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 550
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/activity/bT;

    sget-object v1, Lcom/dropbox/android/activity/bT;->c:Lcom/dropbox/android/activity/bT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/bT;->d:Lcom/dropbox/android/activity/bT;

    aput-object v1, v0, v2

    .line 554
    :goto_13
    invoke-static {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;[Lcom/dropbox/android/activity/bT;)Landroid/content/Intent;

    move-result-object v0

    .line 558
    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 559
    return-void

    .line 552
    :cond_1b
    new-array v0, v2, [Lcom/dropbox/android/activity/bT;

    sget-object v1, Lcom/dropbox/android/activity/bT;->a:Lcom/dropbox/android/activity/bT;

    aput-object v1, v0, v3

    goto :goto_13
.end method

.method private q()V
    .registers 4

    .prologue
    .line 679
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 680
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 681
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 683
    :cond_1a
    return-void
.end method

.method private r()V
    .registers 3

    .prologue
    .line 716
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LoginFragment;

    .line 717
    if-eqz v0, :cond_11

    .line 718
    invoke-virtual {v0}, Lcom/dropbox/android/activity/LoginFragment;->a()V

    .line 720
    :cond_11
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 656
    new-instance v0, Ldbxyzptlk/g/r;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Ldbxyzptlk/g/r;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 657
    sget-object v1, Lcom/dropbox/android/activity/aW;->e:Lcom/dropbox/android/activity/aW;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/aW;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/r;->a(I)V

    .line 658
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/r;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 659
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 570
    invoke-static {}, Lcom/dropbox/android/util/X;->a()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 573
    invoke-static {}, Lcom/dropbox/android/util/X;->b()Ljava/lang/String;

    move-result-object p1

    .line 574
    invoke-static {}, Lcom/dropbox/android/util/X;->c()Ljava/lang/String;

    move-result-object p2

    .line 595
    :goto_e
    new-instance v0, Ldbxyzptlk/g/j;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Ldbxyzptlk/g/j;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 596
    sget-object v1, Lcom/dropbox/android/activity/aW;->b:Lcom/dropbox/android/activity/aW;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/aW;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/j;->a(I)V

    .line 597
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/j;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 598
    :goto_23
    return-void

    .line 576
    :cond_24
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendlog"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 578
    invoke-static {p0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Landroid/content/Context;)V

    goto :goto_23

    .line 582
    :cond_34
    invoke-static {p1}, Lcom/dropbox/android/util/aT;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_41

    .line 583
    const v0, 0x7f0b002b

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(I)V

    goto :goto_23

    .line 587
    :cond_41
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_4f

    .line 588
    const v0, 0x7f0b002c

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(I)V

    goto :goto_23

    .line 592
    :cond_4f
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->q()V

    goto :goto_e
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 604
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendlog"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 606
    invoke-static {p0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Landroid/content/Context;)V

    .line 633
    :goto_10
    return-void

    .line 610
    :cond_11
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v2, :cond_1e

    .line 611
    const v0, 0x7f0b0029

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(I)V

    goto :goto_10

    .line 615
    :cond_1e
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v2, :cond_2b

    .line 616
    const v0, 0x7f0b002a

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(I)V

    goto :goto_10

    .line 620
    :cond_2b
    invoke-static {p3}, Lcom/dropbox/android/util/aT;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_38

    .line 621
    const v0, 0x7f0b002b

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(I)V

    goto :goto_10

    .line 625
    :cond_38
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_46

    .line 626
    const v0, 0x7f0b002c

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(I)V

    goto :goto_10

    .line 630
    :cond_46
    new-instance v0, Lcom/dropbox/android/activity/bd;

    invoke-direct {v0, p3, p4, p1, p2}, Lcom/dropbox/android/activity/bd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/bd;

    .line 631
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/bd;

    invoke-static {v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->a(Lcom/dropbox/android/activity/bd;)Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;

    move-result-object v0

    .line 632
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    goto :goto_10
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 505
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->q()V

    .line 506
    invoke-static {}, Lcom/dropbox/android/util/i;->T()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 508
    invoke-static {}, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->a()Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;

    move-result-object v1

    .line 509
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_21

    const v0, 0x7f0600f9

    :goto_1b
    sget-object v2, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    .line 513
    return-void

    .line 509
    :cond_21
    const v0, 0x7f0600b5

    goto :goto_1b
.end method

.method public final b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 637
    invoke-static {p1}, Lcom/dropbox/android/util/aT;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 638
    const v0, 0x7f0b002b

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(I)V

    .line 645
    :goto_c
    return-void

    .line 642
    :cond_d
    new-instance v0, Ldbxyzptlk/g/z;

    invoke-direct {v0, p0, p1}, Ldbxyzptlk/g/z;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 643
    sget-object v1, Lcom/dropbox/android/activity/aW;->c:Lcom/dropbox/android/activity/aW;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/aW;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/z;->a(I)V

    .line 644
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/z;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_c
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 457
    new-instance v0, Lcom/dropbox/android/activity/LoginFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LoginFragment;-><init>()V

    .line 458
    sget-object v1, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    .line 459
    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 460
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 484
    invoke-static {p1}, Lcom/dropbox/android/activity/ForgotPasswordFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/ForgotPasswordFragment;

    move-result-object v1

    .line 485
    sget-object v2, Lcom/dropbox/android/activity/ForgotPasswordFragment;->a:Ljava/lang/String;

    .line 486
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_17

    const v0, 0x7f0600f9

    :goto_13
    invoke-direct {p0, v1, v0, v2}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;)V

    .line 490
    return-void

    .line 486
    :cond_17
    const v0, 0x7f0600b5

    goto :goto_13
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 465
    new-instance v0, Lcom/dropbox/android/activity/NewAccountFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/NewAccountFragment;-><init>()V

    .line 466
    sget-object v1, Lcom/dropbox/android/activity/NewAccountFragment;->a:Ljava/lang/String;

    .line 467
    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 468
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 733
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 734
    invoke-static {p1}, Lcom/dropbox/android/activity/LoginFragment;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 741
    :goto_13
    const v0, 0x7f0b01a3

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 745
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 746
    return-void

    .line 736
    :cond_23
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LoginFragment;

    .line 737
    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/LoginFragment;->b(Ljava/lang/String;)V

    .line 738
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->b()V

    goto :goto_13
.end method

.method public final e()V
    .registers 5

    .prologue
    .line 493
    invoke-static {}, Lcom/dropbox/android/util/i;->R()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 495
    invoke-static {}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->a()Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;

    move-result-object v1

    .line 496
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_20

    const v0, 0x7f0600f9

    :goto_18
    sget-object v2, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->a:Ljava/lang/String;

    const-string v3, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Landroid/support/v4/app/Fragment;ILjava/lang/String;Ljava/lang/String;)V

    .line 501
    return-void

    .line 496
    :cond_20
    const v0, 0x7f0600b5

    goto :goto_18
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 687
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->c:Z

    .line 688
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->r()V

    .line 689
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->m()V

    .line 690
    return-void
.end method

.method public finish()V
    .registers 4

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 361
    const-string v1, "accountAuthenticatorResponse"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 362
    const-string v1, "accountAuthenticatorResponse"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    .line 370
    const/4 v2, 0x4

    iget-boolean v1, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->c:Z

    if-eqz v1, :cond_22

    const-string v1, "success"

    :goto_1b
    invoke-virtual {v0, v2, v1}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    .line 372
    :cond_1e
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->finish()V

    .line 373
    return-void

    .line 370
    :cond_22
    const-string v1, "canceled"

    goto :goto_1b
.end method

.method public final g()V
    .registers 4

    .prologue
    .line 695
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/j;->a(Ljava/lang/String;I)V

    .line 698
    return-void
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 702
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->c:Z

    .line 703
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->m()V

    .line 704
    return-void
.end method

.method public final h_()V
    .registers 3

    .prologue
    .line 649
    new-instance v0, Ldbxyzptlk/g/K;

    invoke-direct {v0, p0}, Ldbxyzptlk/g/K;-><init>(Landroid/content/Context;)V

    .line 650
    sget-object v1, Lcom/dropbox/android/activity/aW;->d:Lcom/dropbox/android/activity/aW;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/aW;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/K;->a(I)V

    .line 651
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/K;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 652
    return-void
.end method

.method public final i()V
    .registers 1

    .prologue
    .line 712
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->finish()V

    .line 713
    return-void
.end method

.method public final j()V
    .registers 1

    .prologue
    .line 727
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->r()V

    .line 728
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->e()V

    .line 729
    return-void
.end method

.method public final k()V
    .registers 4

    .prologue
    .line 751
    .line 752
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/j;->a(Ljava/lang/String;I)V

    .line 754
    const v0, 0x7f0b01ac

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 758
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 759
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 377
    packed-switch p1, :pswitch_data_e

    .line 390
    :cond_3
    :goto_3
    :pswitch_3
    return-void

    .line 382
    :pswitch_4
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 386
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/bd;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a(Lcom/dropbox/android/activity/bd;)V

    goto :goto_3

    .line 377
    nop

    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Landroid/support/v4/app/j;->c()I

    move-result v1

    .line 321
    if-lez v1, :cond_25

    .line 322
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(I)Landroid/support/v4/app/k;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_25

    const-string v1, "ENTER_TWOFACTOR_CODE_FRAG_STATE"

    invoke-interface {v0}, Landroid/support/v4/app/k;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 332
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->i()V

    .line 336
    :cond_25
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setResult(I)V

    .line 337
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onBackPressed()V

    .line 338
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 251
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 253
    if-eqz p1, :cond_17

    .line 254
    const-string v0, "PENDING_NEW_ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 255
    const-string v0, "PENDING_NEW_ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/bd;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/bd;

    .line 259
    :cond_17
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 261
    const v0, 0x7f030044

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->setContentView(I)V

    .line 263
    sget-object v0, Lcom/dropbox/android/activity/bc;->a:Lcom/dropbox/android/activity/bc;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    .line 264
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_52

    .line 265
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 266
    const-string v1, "com.dropbox.intent.action.LOGIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_75

    .line 267
    sget-object v0, Lcom/dropbox/android/activity/bc;->b:Lcom/dropbox/android/activity/bc;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    .line 275
    :cond_52
    :goto_52
    if-nez p1, :cond_57

    .line 276
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->n()V

    .line 279
    :cond_57
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    .line 282
    invoke-static {}, Lcom/dropbox/android/activity/auth/DropboxAuth;->d()Z

    move-result v1

    if-nez v1, :cond_74

    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->o()Z

    move-result v1

    if-nez v1, :cond_74

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->C()Z

    move-result v1

    if-nez v1, :cond_74

    .line 283
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->k(Z)V

    .line 284
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->p()V

    .line 286
    :cond_74
    return-void

    .line 268
    :cond_75
    const-string v1, "com.dropbox.intent.action.SIGN_UP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_82

    .line 269
    sget-object v0, Lcom/dropbox/android/activity/bc;->c:Lcom/dropbox/android/activity/bc;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    goto :goto_52

    .line 270
    :cond_82
    const-string v1, "com.dropbox.intent.action.HTC_TOKEN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 271
    sget-object v0, Lcom/dropbox/android/activity/bc;->d:Lcom/dropbox/android/activity/bc;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    goto :goto_52
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 563
    invoke-static {p1}, Lcom/dropbox/android/activity/aW;->a(I)Lcom/dropbox/android/activity/aW;

    move-result-object v0

    .line 564
    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/aW;->b(Lcom/dropbox/android/activity/LoginOrNewAcctActivity;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 298
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onResume()V

    .line 301
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 302
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 303
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->m()V

    .line 306
    :cond_10
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->a:Lcom/dropbox/android/activity/bc;

    sget-object v1, Lcom/dropbox/android/activity/bc;->d:Lcom/dropbox/android/activity/bc;

    if-ne v0, v1, :cond_36

    .line 307
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 308
    if-eqz v0, :cond_36

    .line 309
    new-instance v1, Ldbxyzptlk/g/p;

    invoke-direct {v1, p0, v0}, Ldbxyzptlk/g/p;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 310
    sget-object v0, Lcom/dropbox/android/activity/aW;->b:Lcom/dropbox/android/activity/aW;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/aW;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ldbxyzptlk/g/p;->a(I)V

    .line 311
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Ldbxyzptlk/g/p;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 314
    :cond_36
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 290
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/bd;

    if-eqz v0, :cond_e

    .line 292
    const-string v0, "PENDING_NEW_ACCOUNT"

    iget-object v1, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;->b:Lcom/dropbox/android/activity/bd;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 294
    :cond_e
    return-void
.end method
