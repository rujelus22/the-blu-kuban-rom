.class public Lcom/dropbox/android/activity/DropboxActionBarActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"

# interfaces
.implements Lcom/actionbarsherlock/app/ActionBar$TabListener;


# static fields
.field public static a:Z

.field private static c:Z


# instance fields
.field b:Lcom/dropbox/android/activity/delegate/a;

.field private d:Lcom/dropbox/android/activity/R;

.field private f:Lcom/dropbox/android/activity/F;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    .line 42
    sput-boolean v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a:Z

    .line 43
    sput-boolean v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 46
    new-instance v0, Lcom/dropbox/android/activity/delegate/a;

    invoke-direct {v0}, Lcom/dropbox/android/activity/delegate/a;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->b:Lcom/dropbox/android/activity/delegate/a;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d:Lcom/dropbox/android/activity/R;

    .line 355
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DropboxActionBarActivity;Lcom/dropbox/android/activity/G;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/G;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/activity/G;)V
    .registers 6
    .parameter

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->getTabCount()I

    move-result v1

    .line 328
    const/4 v0, 0x0

    :goto_9
    if-ge v0, v1, :cond_20

    .line 329
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/actionbarsherlock/app/ActionBar;->getTabAt(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v2

    .line 330
    invoke-virtual {v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_21

    .line 331
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->selectTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 335
    :cond_20
    return-void

    .line 328
    :cond_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_9
.end method

.method private d()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 146
    const-string v1, "ACTION_CAMERA_UPLOAD_DETAILS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    const-string v1, "ACTION_CAMERA_UPLOAD_GALLERY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 148
    :cond_19
    sget-object v1, Lcom/dropbox/android/activity/G;->c:Lcom/dropbox/android/activity/G;

    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/G;)V

    .line 150
    const-string v1, "ACTION_CAMERA_UPLOAD_DETAILS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 151
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->startActivity(Landroid/content/Intent;)V

    .line 185
    :cond_30
    :goto_30
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/g/B;->a(Landroid/content/Context;)Ldbxyzptlk/g/B;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/g/B;->a()V

    .line 186
    return-void

    .line 154
    :cond_3c
    const-string v1, "ACTION_MOST_RECENT_UPLOAD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_74

    .line 155
    sget-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/G;)V

    .line 157
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->o()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    .line 158
    if-eqz v1, :cond_30

    .line 159
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 160
    sget-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Landroid/net/Uri;Z)V

    goto :goto_30

    .line 162
    :cond_74
    const-string v1, "ACTION_UPLOAD_FAILURE_DLG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d7

    .line 167
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 168
    if-nez v0, :cond_cf

    const/4 v0, 0x1

    move v1, v0

    .line 170
    :goto_88
    if-nez v1, :cond_d1

    .line 171
    sget-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/G;)V

    .line 172
    sget-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Landroid/net/Uri;Z)V

    .line 176
    :goto_a2
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "EXTRA_STATUS"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/taskqueue/m;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    .line 177
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "EXTRA_FILENAME"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 178
    invoke-static {v1, v0, v2}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a(ZLcom/dropbox/android/taskqueue/m;Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/UploadErrorDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a(Landroid/support/v4/app/j;)V

    goto/16 :goto_30

    :cond_cf
    move v1, v2

    .line 168
    goto :goto_88

    .line 174
    :cond_d1
    sget-object v0, Lcom/dropbox/android/activity/G;->c:Lcom/dropbox/android/activity/G;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/G;)V

    goto :goto_a2

    .line 180
    :cond_d7
    sget-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/G;)V

    .line 181
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 182
    sget-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Landroid/net/Uri;Z)V

    goto/16 :goto_30
.end method

.method private e()Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 217
    invoke-static {p0}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4e

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 219
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v3

    .line 220
    invoke-virtual {v3}, Ldbxyzptlk/l/m;->t()Z

    move-result v0

    if-nez v0, :cond_4e

    .line 222
    invoke-static {}, Lcom/dropbox/android/util/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_4e

    invoke-virtual {v3}, Ldbxyzptlk/l/m;->g()Z

    move-result v0

    if-nez v0, :cond_4e

    .line 224
    invoke-static {}, Lcom/dropbox/android/activity/DropboxBrowser;->b()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 225
    new-array v0, v1, [Lcom/dropbox/android/activity/bT;

    sget-object v4, Lcom/dropbox/android/activity/bT;->f:Lcom/dropbox/android/activity/bT;

    aput-object v4, v0, v2

    .line 229
    :goto_34
    invoke-static {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;[Lcom/dropbox/android/activity/bT;)Landroid/content/Intent;

    move-result-object v0

    .line 230
    const/16 v2, 0xf

    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 231
    invoke-virtual {v3, v1}, Ldbxyzptlk/l/m;->i(Z)V

    move v0, v1

    .line 236
    :goto_41
    return v0

    .line 227
    :cond_42
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/dropbox/android/activity/bT;

    sget-object v4, Lcom/dropbox/android/activity/bT;->b:Lcom/dropbox/android/activity/bT;

    aput-object v4, v0, v2

    sget-object v2, Lcom/dropbox/android/activity/bT;->f:Lcom/dropbox/android/activity/bT;

    aput-object v2, v0, v1

    goto :goto_34

    :cond_4e
    move v0, v2

    .line 236
    goto :goto_41
.end method

.method private f()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 249
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 250
    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 251
    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 253
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setNavigationMode(I)V

    .line 255
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    const v2, 0x7f020172

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 256
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/activity/G;->c:Lcom/dropbox/android/activity/G;

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    const v2, 0x7f020178

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 257
    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->newTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/activity/G;->b:Lcom/dropbox/android/activity/G;

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    const v2, 0x7f02017e

    invoke-virtual {v1, v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setIcon(I)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->setTabListener(Lcom/actionbarsherlock/app/ActionBar$TabListener;)Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->addTab(Lcom/actionbarsherlock/app/ActionBar$Tab;)V

    .line 258
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .registers 7
    .parameter

    .prologue
    .line 377
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/j/g;->c:Ljava/lang/String;

    .line 378
    if-eqz v0, :cond_14

    const-string v1, "com."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 379
    :cond_14
    const-string v0, "com.dropbox.android"

    .line 381
    :cond_16
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "market://details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 382
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 384
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/a;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3f

    .line 397
    :cond_3e
    :goto_3e
    return-void

    .line 388
    :cond_3f
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    .line 389
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v2

    iget-object v2, v2, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    .line 390
    if-nez p1, :cond_56

    invoke-virtual {v0, v2}, Ldbxyzptlk/l/m;->f(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x3

    if-ge v3, v4, :cond_3e

    .line 393
    :cond_56
    invoke-virtual {v0, v2}, Ldbxyzptlk/l/m;->g(Ljava/lang/String;)V

    .line 396
    invoke-static {v1, p1}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a(Landroid/content/Intent;Z)Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a(Landroid/support/v4/app/j;)V

    goto :goto_3e
.end method

.method public getResources()Landroid/content/res/Resources;
    .registers 3

    .prologue
    .line 340
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/dropbox/android/util/bj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 341
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f:Lcom/dropbox/android/activity/F;

    if-nez v0, :cond_17

    .line 342
    new-instance v0, Lcom/dropbox/android/activity/F;

    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/F;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f:Lcom/dropbox/android/activity/F;

    .line 344
    :cond_17
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f:Lcom/dropbox/android/activity/F;

    .line 346
    :goto_19
    return-object v0

    :cond_1a
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_19
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 312
    const/16 v0, 0xf

    if-ne p1, v0, :cond_1c

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1c

    .line 313
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->g()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 314
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e:Lcom/dropbox/android/activity/base/a;

    new-instance v1, Lcom/dropbox/android/activity/E;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/E;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/a;->a(Ljava/lang/Runnable;)Z

    .line 323
    :cond_1b
    :goto_1b
    return-void

    .line 319
    :cond_1c
    if-eqz p3, :cond_1b

    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.dropbox.android.file_added"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 321
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1b
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->getSelectedTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/G;

    .line 263
    sget-object v1, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    if-ne v0, v1, :cond_1f

    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/MainBrowserFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/MainBrowserFragment;->i()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 268
    :goto_1e
    return-void

    .line 266
    :cond_1f
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onBackPressed()V

    goto :goto_1e
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 124
    invoke-static {p0}, Lcom/dropbox/android/activity/G;->a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    .line 125
    new-instance v0, Lcom/dropbox/android/activity/R;

    invoke-direct {v0, p1}, Lcom/dropbox/android/activity/R;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d:Lcom/dropbox/android/activity/R;

    .line 129
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->setContentView(I)V

    .line 131
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->f()V

    .line 132
    if-nez p1, :cond_1c

    .line 134
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d()V

    .line 142
    :goto_1b
    return-void

    .line 137
    :cond_1c
    const-string v0, "key_tab_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    const-string v0, "key_tab_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/G;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/G;

    move-result-object v0

    .line 140
    :goto_2e
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Lcom/dropbox/android/activity/G;)V

    goto :goto_1b

    .line 137
    :cond_32
    sget-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    goto :goto_2e
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    .line 272
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z

    move-result v0

    .line 273
    sget-object v1, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/actionbarsherlock/app/ActionBar;->getSelectedTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/G;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 274
    iget-object v2, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->b:Lcom/dropbox/android/activity/delegate/a;

    invoke-virtual {v2, p1, v1}, Lcom/dropbox/android/activity/delegate/a;->a(Lcom/actionbarsherlock/view/Menu;Z)Z

    move-result v1

    .line 275
    if-nez v0, :cond_20

    if-eqz v1, :cond_22

    :cond_20
    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 241
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->setIntent(Landroid/content/Intent;)V

    .line 242
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e:Lcom/dropbox/android/activity/base/a;

    new-instance v1, Lcom/dropbox/android/activity/D;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/D;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/base/a;->a(Ljava/lang/Runnable;)Z

    .line 246
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->b:Lcom/dropbox/android/activity/delegate/a;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/activity/delegate/a;->a(Landroid/app/Activity;Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 281
    const/4 v0, 0x1

    .line 283
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_d
.end method

.method protected onResume()V
    .registers 5

    .prologue
    .line 190
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onResume()V

    .line 191
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->e()Z

    move-result v0

    .line 192
    if-nez v0, :cond_e

    .line 193
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d:Lcom/dropbox/android/activity/R;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/R;->a(Landroid/support/v4/app/FragmentActivity;)V

    .line 197
    :cond_e
    sget-boolean v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->c:Z

    if-nez v0, :cond_39

    .line 198
    const/4 v0, 0x1

    sput-boolean v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->c:Z

    .line 199
    new-instance v0, Ldbxyzptlk/g/T;

    invoke-direct {v0, p0}, Ldbxyzptlk/g/T;-><init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V

    .line 200
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/T;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 204
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/filemanager/V;->c(Landroid/content/Context;)Lcom/dropbox/android/filemanager/W;

    move-result-object v0

    .line 205
    new-instance v1, Lcom/dropbox/android/provider/y;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->l()Landroid/app/Activity;

    move-result-object v2

    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/dropbox/android/provider/y;-><init>(Landroid/content/Context;Lcom/dropbox/android/provider/h;Lcom/dropbox/android/filemanager/W;)V

    .line 207
    invoke-virtual {v1}, Lcom/dropbox/android/provider/y;->start()V

    .line 211
    :cond_38
    :goto_38
    return-void

    .line 208
    :cond_39
    sget-boolean v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a:Z

    if-eqz v0, :cond_38

    .line 209
    sget-boolean v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a:Z

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Z)V

    goto :goto_38
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->d:Lcom/dropbox/android/activity/R;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/R;->a(Landroid/os/Bundle;)V

    .line 115
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar;->getSelectedTab()Lcom/actionbarsherlock/app/ActionBar$Tab;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_21

    .line 117
    const-string v1, "key_tab_id"

    invoke-virtual {v0}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/G;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_21
    return-void
.end method

.method public onTabReselected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/u;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 300
    return-void
.end method

.method public onTabSelected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/u;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 289
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/G;

    .line 290
    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->c()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 291
    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/app/u;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 295
    :goto_13
    return-void

    .line 293
    :cond_14
    const v1, 0x7f06007f

    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->a()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v2, v0}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    goto :goto_13
.end method

.method public onTabUnselected(Lcom/actionbarsherlock/app/ActionBar$Tab;Landroid/support/v4/app/u;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 304
    invoke-virtual {p1}, Lcom/actionbarsherlock/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/G;

    .line 305
    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->c()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 306
    invoke-virtual {v0}, Lcom/dropbox/android/activity/G;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/app/u;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 308
    :cond_13
    return-void
.end method
