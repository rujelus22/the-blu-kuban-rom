.class public Lcom/dropbox/android/activity/SearchActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"


# instance fields
.field private a:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/SearchActivity;->a:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 25
    :cond_1f
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->finish()V

    .line 45
    :cond_22
    :goto_22
    return-void

    .line 30
    :cond_23
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SearchActivity;->setContentView(I)V

    .line 32
    if-nez p1, :cond_22

    .line 35
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    sget-object v1, Lcom/dropbox/android/f;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "query"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 37
    iput-object v0, p0, Lcom/dropbox/android/activity/SearchActivity;->a:Landroid/net/Uri;

    .line 39
    new-instance v0, Lcom/dropbox/android/activity/SearchFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/SearchFragment;-><init>()V

    .line 40
    iget-object v1, p0, Lcom/dropbox/android/activity/SearchActivity;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/SearchFragment;->a(Landroid/net/Uri;Z)V

    .line 41
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v1

    .line 42
    const v2, 0x7f06007f

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 43
    invoke-virtual {v1}, Landroid/support/v4/app/u;->b()I

    goto :goto_22
.end method

.method public onSearchRequested()Z
    .registers 2

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method
