.class final Lcom/dropbox/android/activity/Y;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/S;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/FavoritesFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/FavoritesFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 292
    iput-object p1, p0, Lcom/dropbox/android/activity/Y;->a:Lcom/dropbox/android/activity/FavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/provider/P;Landroid/database/Cursor;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 301
    sget-object v0, Lcom/dropbox/android/activity/Z;->a:[I

    invoke-static {p2}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_34

    .line 311
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected item type in this fragement"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :pswitch_17
    invoke-static {p2}, Lcom/dropbox/android/provider/K;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 304
    iget-boolean v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v1, :cond_27

    .line 305
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not expecting directory in favorites view"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_27
    iget-object v1, p0, Lcom/dropbox/android/activity/Y;->a:Lcom/dropbox/android/activity/FavoritesFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/FavoritesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/e;->a:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-static {v1, v0, v2, v3}, Lcom/dropbox/android/util/bd;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/net/Uri;Z)V

    .line 313
    return-void

    .line 301
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_17
    .end packed-switch
.end method

.method public final a(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 295
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;)Z

    .line 296
    const/4 v0, 0x1

    return v0
.end method
