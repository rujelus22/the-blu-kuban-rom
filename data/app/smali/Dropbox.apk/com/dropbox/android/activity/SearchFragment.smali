.class public Lcom/dropbox/android/activity/SearchFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"


# static fields
.field private static final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 16
    const-class v0, Lcom/dropbox/android/activity/SearchFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/SearchFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    return-void
.end method

.method private j()Ljava/lang/String;
    .registers 5

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25
    const v1, 0x7f0b001b

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/SearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .registers 4

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->d()I

    move-result v0

    if-nez v0, :cond_10

    .line 31
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This is unexpected"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_10
    iget-object v0, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1b

    .line 33
    const/4 v0, 0x0

    .line 37
    :goto_1a
    return-object v0

    .line 34
    :cond_1b
    iget-object v0, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_29

    .line 35
    invoke-direct {p0}, Lcom/dropbox/android/activity/SearchFragment;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_1a

    .line 37
    :cond_29
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    iget-object v2, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v2}, Lcom/dropbox/android/util/aq;->d()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/aq;->a(I)Lcom/dropbox/android/util/ap;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1a
.end method

.method protected final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->d()I

    move-result v0

    if-nez v0, :cond_10

    .line 44
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This is unexpected"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_10
    iget-object v0, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1e

    .line 46
    invoke-direct {p0}, Lcom/dropbox/android/activity/SearchFragment;->j()Ljava/lang/String;

    move-result-object v0

    .line 48
    :goto_1d
    return-object v0

    :cond_1e
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1d
.end method

.method public final c()Lcom/dropbox/android/widget/X;
    .registers 2

    .prologue
    .line 20
    sget-object v0, Lcom/dropbox/android/widget/X;->e:Lcom/dropbox/android/widget/X;

    return-object v0
.end method

.method protected final d()I
    .registers 2

    .prologue
    .line 54
    const v0, 0x7f0b0040

    return v0
.end method

.method protected final e()I
    .registers 2

    .prologue
    .line 77
    const v0, 0x7f03002a

    return v0
.end method

.method protected final h()V
    .registers 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_28

    .line 68
    sget-object v0, Lcom/dropbox/android/activity/SearchFragment;->j:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to browseParent with a history stack of size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v2}, Lcom/dropbox/android/util/aq;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :goto_27
    return-void

    .line 71
    :cond_28
    iget-object v0, p0, Lcom/dropbox/android/activity/SearchFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->c()Lcom/dropbox/android/util/ap;

    .line 72
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SearchFragment;->g()V

    goto :goto_27
.end method
