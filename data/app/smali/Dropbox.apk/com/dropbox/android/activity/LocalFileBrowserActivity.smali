.class public Lcom/dropbox/android/activity/LocalFileBrowserActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/aF;
.implements Lcom/dropbox/android/activity/delegate/j;
.implements Lcom/dropbox/android/util/e;
.implements Ldbxyzptlk/g/I;


# static fields
.field static final synthetic b:Z


# instance fields
.field protected a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 34
    const-class v0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->b:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 107
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->c(Landroid/net/Uri;)V

    .line 115
    :goto_13
    return-void

    .line 109
    :cond_14
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 110
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 112
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 113
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    goto :goto_13
.end method

.method public final a(Landroid/net/Uri;Ljava/util/Set;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 98
    sget-boolean v0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->b:Z

    if-nez v0, :cond_12

    if-eqz p2, :cond_c

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_c
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 99
    :cond_12
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {p0, p0, p2, v0}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Landroid/content/Context;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Z

    .line 100
    return-void
.end method

.method protected final a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;Landroid/net/Uri;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 153
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0, p3}, Ldbxyzptlk/l/m;->a(Landroid/net/Uri;)V

    .line 155
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setResult(I)V

    .line 156
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    .line 158
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 159
    if-nez v0, :cond_2d

    .line 160
    invoke-static {}, Lcom/dropbox/android/util/ba;->a()Lcom/dropbox/android/util/ba;

    move-result-object v0

    const v1, 0x7f0b00ac

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/ba;->a(I[Ljava/lang/Object;)V

    .line 164
    :goto_2c
    return-void

    .line 162
    :cond_2d
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    invoke-virtual {v1, v0, p2, p4}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/filemanager/LocalEntry;Ljava/io/File;Z)V

    goto :goto_2c
.end method

.method public final a(Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    iget-boolean v2, p0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a:Z

    if-nez v2, :cond_11

    .line 169
    sget-object v2, Lcom/dropbox/android/activity/aH;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/util/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_34

    .line 185
    :cond_11
    :goto_11
    :pswitch_11
    return-void

    .line 175
    :pswitch_12
    iput-boolean v0, p0, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a:Z

    .line 176
    new-instance v2, Ldbxyzptlk/g/G;

    sget-object v3, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    if-ne v3, p1, :cond_32

    :goto_1a
    invoke-direct {v2, p0, v0, p2, p3}, Ldbxyzptlk/g/G;-><init>(Landroid/content/Context;ZLjava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    .line 181
    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Ldbxyzptlk/g/G;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 182
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->a(Ljava/lang/String;)V

    goto :goto_11

    :cond_32
    move v0, v1

    .line 176
    goto :goto_1a

    .line 169
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
    .end packed-switch
.end method

.method public final a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 189
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 190
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.dropbox.android.file_added"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 191
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 192
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 194
    :cond_1b
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    .line 195
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .registers 5
    .parameter

    .prologue
    .line 206
    invoke-static {p1}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->a(Landroid/net/Uri;)Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;

    move-result-object v0

    .line 207
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/delegate/NewLocalFolderDialogFragment;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method protected final c(Landroid/net/Uri;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 119
    const-string v1, "EXPORT_DB_PROVIDER_URI"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 120
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 122
    invoke-static {p1}, Lcom/dropbox/android/provider/FileSystemProvider;->b(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 123
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_49

    .line 126
    invoke-virtual {p0, v1, v2, p1, v6}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;Landroid/net/Uri;Z)V

    .line 150
    :goto_48
    return-void

    .line 129
    :cond_49
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 130
    const v3, 0x7f0b00ae

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 132
    const v3, 0x7f0b00af

    invoke-virtual {p0, v3}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 133
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 135
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 137
    new-instance v3, Lcom/dropbox/android/activity/aG;

    invoke-direct {v3, p0, v1, v2, p1}, Lcom/dropbox/android/activity/aG;-><init>(Lcom/dropbox/android/activity/LocalFileBrowserActivity;Lcom/dropbox/android/util/DropboxPath;Ljava/io/File;Landroid/net/Uri;)V

    .line 144
    const v1, 0x7f0b00b0

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 145
    const v1, 0x7f0b0018

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 146
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 148
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_48
.end method

.method public final d(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "FILE_BROWSER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    .line 213
    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/net/Uri;)V

    .line 214
    return-void
.end method

.method public final g_()V
    .registers 2

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setResult(I)V

    .line 93
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->finish()V

    .line 94
    return-void
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "FILE_BROWSER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    .line 81
    if-eqz v0, :cond_15

    .line 82
    invoke-virtual {v0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 88
    :goto_14
    return-void

    .line 87
    :cond_15
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onBackPressed()V

    goto :goto_14
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->setContentView(I)V

    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "FILE_BROWSER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    .line 54
    if-nez v0, :cond_56

    .line 55
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 58
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_57

    .line 59
    sget-object v0, Lcom/dropbox/android/activity/aE;->b:Lcom/dropbox/android/activity/aE;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/aE;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    :goto_2d
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 66
    const-string v2, "key_Mode"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;-><init>()V

    .line 69
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->setArguments(Landroid/os/Bundle;)V

    .line 70
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->setRetainInstance(Z)V

    .line 72
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowserActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v1

    .line 73
    const v2, 0x7f06007f

    const-string v3, "FILE_BROWSER"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 74
    invoke-virtual {v1}, Landroid/support/v4/app/u;->b()I

    .line 76
    :cond_56
    return-void

    .line 61
    :cond_57
    const-string v1, "BROWSE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2d
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 201
    invoke-static {p0}, Lcom/dropbox/android/util/a;->a(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
