.class final Lcom/dropbox/android/activity/dialog/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V
    .registers 2
    .parameter

    .prologue
    .line 135
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/k;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/k;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_LOCAL_ENTRY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/k;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 141
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_32

    .line 142
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The dialog should not allow empty names."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_32
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/k;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_64

    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/k;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v6

    .line 145
    :goto_44
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/k;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 147
    new-instance v0, Lcom/dropbox/android/activity/dialog/l;

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/k;->a:Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object v1, p0

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/dialog/l;-><init>(Lcom/dropbox/android/activity/dialog/k;Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V

    .line 171
    invoke-virtual {v0}, Ldbxyzptlk/g/J;->f()V

    .line 172
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/J;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 174
    return-void

    .line 144
    :cond_64
    const/4 v6, 0x0

    goto :goto_44
.end method
