.class public Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/content/DialogInterface$OnClickListener;

.field c:Landroid/content/DialogInterface$OnClickListener;

.field private d:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    .line 135
    new-instance v0, Lcom/dropbox/android/activity/dialog/k;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/k;-><init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/content/DialogInterface$OnClickListener;

    .line 177
    new-instance v0, Lcom/dropbox/android/activity/dialog/m;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/m;-><init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)Landroid/widget/EditText;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;
    .registers 3
    .parameter

    .prologue
    .line 42
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v1, "ARG_LOCAL_ENTRY"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 44
    new-instance v1, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    invoke-direct {v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;-><init>()V

    .line 45
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 46
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 62
    sget-object v0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 8
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 71
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_LOCAL_ENTRY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    .line 73
    iget-boolean v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v1, :cond_9a

    const v1, 0x7f0b00a5

    .line 75
    :goto_1d
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 77
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 79
    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    .line 80
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    const v3, 0x1020003

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setId(I)V

    .line 81
    if-nez p1, :cond_7c

    .line 82
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setLines(I)V

    .line 83
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 84
    iget-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->t(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    .line 85
    iget-object v4, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v4, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    const/4 v5, 0x0

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v4, v5, v1}, Landroid/widget/EditText;->setSelection(II)V

    .line 87
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    const v3, 0x80001

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 90
    :cond_7c
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 92
    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_9e

    const v0, 0x7f0b00a6

    .line 93
    :goto_88
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->b:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    const v0, 0x7f0b0018

    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->c:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 96
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 73
    :cond_9a
    const v1, 0x7f0b00a2

    goto :goto_1d

    .line 92
    :cond_9e
    const v0, 0x7f0b00a3

    goto :goto_88
.end method

.method public onStart()V
    .registers 3

    .prologue
    .line 101
    invoke-super {p0}, Lcom/dropbox/android/util/DialogFragmentBase;->onStart()V

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/dialog/i;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dialog/i;-><init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->d:Landroid/widget/EditText;

    new-instance v1, Lcom/dropbox/android/activity/dialog/j;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dialog/j;-><init>(Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 133
    return-void
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 57
    sget-object v0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 51
    sget-object v0, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 53
    return-void
.end method
