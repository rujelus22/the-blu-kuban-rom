.class public Lcom/dropbox/android/activity/GalleryActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field a:Lcom/dropbox/android/widget/GalleryView;

.field protected b:F

.field protected c:[F

.field protected d:Landroid/database/Cursor;

.field private g:I

.field private h:Z

.field private i:Landroid/view/ViewGroup;

.field private j:Landroid/view/ViewGroup;

.field private k:Landroid/view/View;

.field private final l:Landroid/os/Handler;

.field private m:Ljava/lang/Runnable;

.field private n:Landroid/view/animation/AlphaAnimation;

.field private o:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 57
    const-class v0, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:I

    .line 68
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->b:F

    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_1a

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->c:[F

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    return-void

    .line 69
    :array_1a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;Landroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation$AnimationListener;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Landroid/view/animation/Animation$AnimationListener;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryActivity;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-ne p1, v0, :cond_5

    .line 226
    :goto_4
    return-void

    .line 224
    :cond_5
    if-eqz p1, :cond_d

    const-wide/16 v0, 0x0

    .line 225
    :goto_9
    invoke-direct {p0, p1, v0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(ZJ)V

    goto :goto_4

    .line 224
    :cond_d
    const-wide/16 v0, 0x12c

    goto :goto_9
.end method

.method private a(ZJ)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/high16 v0, 0x3f80

    const/4 v1, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 229
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v4}, Lcom/dropbox/android/widget/GalleryView;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    if-nez v4, :cond_f

    .line 296
    :goto_e
    return-void

    .line 232
    :cond_f
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v4}, Lcom/dropbox/android/widget/GalleryView;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    .line 233
    iget-object v5, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    iget-boolean v4, v4, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    invoke-virtual {v5, v4}, Landroid/view/View;->setSelected(Z)V

    .line 235
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    if-eqz v4, :cond_39

    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v4}, Landroid/view/animation/AlphaAnimation;->hasStarted()Z

    move-result v4

    if-eqz v4, :cond_39

    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v4}, Landroid/view/animation/AlphaAnimation;->hasEnded()Z

    move-result v4

    if-nez v4, :cond_39

    .line 236
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v3, :cond_8b

    .line 237
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v4}, Landroid/view/animation/AlphaAnimation;->cancel()V

    .line 246
    :cond_39
    :goto_39
    const-wide/16 v4, 0x0

    cmp-long v4, p2, v4

    if-lez v4, :cond_a0

    .line 248
    if-eqz p1, :cond_9c

    move v3, v1

    .line 249
    :goto_42
    if-eqz p1, :cond_9e

    .line 251
    :goto_44
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v3, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    .line 252
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 253
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 257
    new-instance v0, Lcom/dropbox/android/activity/aj;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/activity/aj;-><init>(Lcom/dropbox/android/activity/GalleryActivity;Z)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Landroid/view/animation/Animation$AnimationListener;

    .line 275
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 277
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_76

    .line 279
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 282
    :cond_76
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 283
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_88

    .line 284
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 295
    :cond_88
    :goto_88
    iput-boolean p1, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    goto :goto_e

    .line 239
    :cond_8b
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->clearAnimation()V

    .line 240
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Landroid/view/animation/Animation$AnimationListener;

    if-eqz v4, :cond_39

    .line 241
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->o:Landroid/view/animation/Animation$AnimationListener;

    iget-object v5, p0, Lcom/dropbox/android/activity/GalleryActivity;->n:Landroid/view/animation/AlphaAnimation;

    invoke-interface {v4, v5}, Landroid/view/animation/Animation$AnimationListener;->onAnimationEnd(Landroid/view/animation/Animation;)V

    goto :goto_39

    :cond_9c
    move v3, v0

    .line 248
    goto :goto_42

    :cond_9e
    move v0, v1

    .line 249
    goto :goto_44

    .line 288
    :cond_a0
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    if-eqz p1, :cond_b4

    move v0, v2

    :goto_a5
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_88

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz p1, :cond_b6

    :goto_b0
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_88

    :cond_b4
    move v0, v3

    .line 288
    goto :goto_a5

    :cond_b6
    move v2, v3

    .line 291
    goto :goto_b0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/view/ViewGroup;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/view/ViewGroup;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .registers 1

    .prologue
    .line 54
    sget-object v0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .registers 7

    .prologue
    .line 111
    const v0, 0x7f060085

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    .line 112
    const v0, 0x7f060089

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    .line 114
    const v0, 0x7f06008a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    .line 115
    const v0, 0x7f06008b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 116
    const v1, 0x7f06008c

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 117
    const v2, 0x7f06008d

    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 119
    new-instance v3, Lcom/dropbox/android/activity/ac;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/ac;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    iput-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    .line 128
    new-instance v3, Lcom/dropbox/android/activity/ad;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/ad;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    .line 143
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 144
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    if-eqz v4, :cond_4e

    .line 145
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->j:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 148
    :cond_4e
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->k:Landroid/view/View;

    new-instance v5, Lcom/dropbox/android/activity/ae;

    invoke-direct {v5, p0}, Lcom/dropbox/android/activity/ae;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 170
    new-instance v4, Lcom/dropbox/android/activity/af;

    invoke-direct {v4, p0}, Lcom/dropbox/android/activity/af;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    new-instance v0, Lcom/dropbox/android/activity/ag;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ag;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 189
    new-instance v0, Lcom/dropbox/android/activity/ah;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ah;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    .line 197
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    new-instance v1, Lcom/dropbox/android/activity/ai;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ai;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/GalleryView;->setTouchListener(Lcom/dropbox/android/widget/ac;)V

    .line 217
    return-void
.end method


# virtual methods
.method public final d()V
    .registers 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v7, -0x1

    .line 430
    sget-object v0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    const-string v1, "Refreshing gallery."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->e()Landroid/net/Uri;

    move-result-object v1

    .line 434
    sget-object v2, Lcom/dropbox/android/provider/K;->a:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/activity/GalleryActivity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->d:Landroid/database/Cursor;

    .line 436
    const/4 v1, 0x1

    .line 438
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "SELECTED_PATH"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 439
    sget-object v2, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Selected is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/GalleryView;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    if-eqz v2, :cond_4d

    .line 443
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    move v1, v6

    .line 446
    :cond_4d
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 450
    :try_start_52
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->d:Landroid/database/Cursor;

    const/4 v4, -0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->moveToPosition(I)Z
    :try_end_58
    .catch Ljava/lang/IllegalStateException; {:try_start_52 .. :try_end_58} :catch_a5
    .catch Landroid/database/StaleDataException; {:try_start_52 .. :try_end_58} :catch_b9

    move v2, v7

    .line 451
    :cond_59
    :goto_59
    :try_start_59
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->d:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_8b

    .line 452
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->d:Landroid/database/Cursor;

    invoke-static {v4}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v4

    .line 453
    sget-object v5, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    if-ne v4, v5, :cond_59

    .line 454
    iget-object v4, p0, Lcom/dropbox/android/activity/GalleryActivity;->d:Landroid/database/Cursor;

    invoke-static {v4}, Lcom/dropbox/android/provider/K;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v4

    .line 455
    if-eqz v4, :cond_59

    .line 456
    invoke-static {v4}, Lcom/dropbox/android/util/ae;->a(Ldbxyzptlk/n/k;)Z

    move-result v5

    if-eqz v5, :cond_59

    .line 457
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    iget-object v4, v4, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_59

    .line 459
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
    :try_end_87
    .catch Ljava/lang/IllegalStateException; {:try_start_59 .. :try_end_87} :catch_f2
    .catch Landroid/database/StaleDataException; {:try_start_59 .. :try_end_87} :catch_f0

    move-result v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_59

    :cond_8b
    move v0, v2

    .line 473
    :goto_8c
    if-eqz v1, :cond_cd

    iget v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:I

    if-eq v2, v7, :cond_cd

    .line 475
    iget v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:I

    .line 482
    :cond_94
    :goto_94
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_d8

    .line 483
    sget-object v0, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    const-string v1, "No images left in gallery, finishing."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->finish()V

    .line 493
    :goto_a4
    return-void

    .line 465
    :catch_a5
    move-exception v0

    move v2, v7

    .line 466
    :goto_a7
    sget-object v4, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    const-string v5, "Illegal state in Gallery!"

    invoke-static {v4, v5}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v4

    sget-object v5, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v4, v0, v5}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    move v0, v2

    .line 471
    goto :goto_8c

    .line 468
    :catch_b9
    move-exception v0

    move v2, v7

    .line 469
    :goto_bb
    sget-object v4, Lcom/dropbox/android/activity/GalleryActivity;->f:Ljava/lang/String;

    const-string v5, "Stale data in gallery!"

    invoke-static {v4, v5}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v4

    sget-object v5, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v4, v0, v5}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    move v0, v2

    goto :goto_8c

    .line 476
    :cond_cd
    if-nez v1, :cond_94

    if-ne v0, v7, :cond_94

    .line 479
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->a()I

    move-result v0

    goto :goto_94

    .line 489
    :cond_d8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 490
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 492
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-static {}, Lcom/dropbox/android/util/bc;->g()Ldbxyzptlk/n/o;

    move-result-object v2

    invoke-virtual {v1, v3, v2, v0}, Lcom/dropbox/android/widget/GalleryView;->setImages(Ljava/lang/Iterable;Ldbxyzptlk/n/o;I)V

    goto :goto_a4

    .line 468
    :catch_f0
    move-exception v0

    goto :goto_bb

    .line 465
    :catch_f2
    move-exception v0

    goto :goto_a7
.end method

.method public final e()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final e_()V
    .registers 2

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    .line 300
    return-void

    .line 299
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 423
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/GalleryView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 424
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 425
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 90
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->setContentView(I)V

    .line 93
    const v0, 0x7f060084

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/GalleryView;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    .line 95
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryActivity;->g()V

    .line 97
    if-eqz p1, :cond_55

    .line 98
    const-string v0, "mSavedInstanceSelectedIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->g:I

    .line 99
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->d()V

    .line 100
    const-string v0, "selectedScale"

    const/high16 v1, 0x3f80

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->b:F

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->c:[F

    const-string v1, "selectedCenterX"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v1

    aput v1, v0, v3

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->c:[F

    const-string v1, "selectedCenterY"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v1

    aput v1, v0, v4

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->c:[F

    aget v1, v1, v3

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->c:[F

    aget v2, v2, v4

    iget v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->b:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/widget/GalleryView;->setCurrentImagePosScale(FFF)V

    .line 108
    :goto_54
    return-void

    .line 105
    :cond_55
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->d()V

    goto :goto_54
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 319
    packed-switch p1, :pswitch_data_52

    .line 393
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :cond_9
    :goto_9
    return-object v0

    .line 321
    :pswitch_a
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 322
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 323
    const v1, 0x7f0b016d

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 324
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 325
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_9

    .line 328
    :pswitch_20
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 334
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 335
    const-string v2, "Delete"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 336
    const v2, 0x7f0b0094

    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 337
    const v2, 0x7f0b0018

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 339
    new-instance v0, Lcom/dropbox/android/activity/ak;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ak;-><init>(Lcom/dropbox/android/activity/GalleryActivity;)V

    .line 389
    const v2, 0x7f0b0095

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 391
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_9

    .line 319
    :pswitch_data_52
    .packed-switch 0x3b6
        :pswitch_20
        :pswitch_a
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 503
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_11

    .line 504
    sparse-switch p1, :sswitch_data_5a

    .line 549
    :cond_11
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_15
    return v0

    .line 509
    :sswitch_16
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 510
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    goto :goto_15

    .line 516
    :sswitch_21
    iget-boolean v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-eqz v2, :cond_30

    .line 518
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 519
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    goto :goto_15

    .line 524
    :cond_30
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->e()V

    move v0, v1

    .line 525
    goto :goto_15

    .line 528
    :sswitch_37
    iget-boolean v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->h:Z

    if-eqz v2, :cond_46

    .line 530
    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v3, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 531
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Z)V

    goto :goto_15

    .line 536
    :cond_46
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->f()V

    move v0, v1

    .line 537
    goto :goto_15

    .line 541
    :sswitch_4d
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 542
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryActivity;->e_()V

    move v0, v1

    .line 543
    goto :goto_15

    .line 504
    nop

    :sswitch_data_5a
    .sparse-switch
        0x13 -> :sswitch_16
        0x14 -> :sswitch_16
        0x15 -> :sswitch_21
        0x16 -> :sswitch_37
        0x17 -> :sswitch_16
        0x52 -> :sswitch_4d
    .end sparse-switch
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 410
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onPause()V

    .line 412
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/q;->a()V

    .line 413
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 304
    packed-switch p1, :pswitch_data_14

    .line 312
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 313
    :goto_6
    :pswitch_6
    return-void

    .line 306
    :pswitch_7
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    .line 307
    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 304
    nop

    :pswitch_data_14
    .packed-switch 0x3b6
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method protected onResume()V
    .registers 5

    .prologue
    .line 417
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onResume()V

    .line 418
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 419
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 399
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 401
    const-string v0, "mSavedInstanceSelectedIndex"

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 402
    const-string v0, "selectedScale"

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/GalleryView;->c()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 403
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->d()[F

    move-result-object v0

    .line 404
    const-string v1, "selectedCenterX"

    const/4 v2, 0x0

    aget v2, v0, v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 405
    const-string v1, "selectedCenterY"

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 406
    return-void
.end method
