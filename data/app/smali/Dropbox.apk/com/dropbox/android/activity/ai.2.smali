.class final Lcom/dropbox/android/activity/ai;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/ac;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/GalleryActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 197
    iput-object p1, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/GalleryActivity;->b(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 202
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->e_()V

    .line 203
    return-void
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 5
    .parameter

    .prologue
    .line 214
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/GalleryActivity;->e()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2}, Lcom/dropbox/android/util/bd;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/net/Uri;Z)V

    .line 215
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 208
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/GalleryActivity;->b(Lcom/dropbox/android/activity/GalleryActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/activity/ai;->a:Lcom/dropbox/android/activity/GalleryActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->a(Lcom/dropbox/android/activity/GalleryActivity;Z)V

    .line 210
    return-void
.end method
