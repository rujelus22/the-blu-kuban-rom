.class public Lcom/dropbox/android/activity/CameraUploadDetailsActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"


# instance fields
.field private a:Z

.field private b:Landroid/os/Bundle;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    .line 17
    const-string v0, "details"

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->c:Ljava/lang/String;

    return-void
.end method

.method private a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;-><init>()V

    .line 84
    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    if-eqz v2, :cond_19

    .line 85
    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 86
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    .line 90
    :cond_19
    const v2, 0x7f06007f

    const-string v3, "details"

    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/u;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 91
    if-eqz p1, :cond_27

    .line 92
    invoke-virtual {v0}, Landroid/support/v4/app/u;->b()I

    .line 96
    :goto_26
    return-void

    .line 94
    :cond_27
    invoke-virtual {v0}, Landroid/support/v4/app/u;->c()I

    goto :goto_26
.end method

.method private d()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 51
    iget-boolean v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->e:Lcom/dropbox/android/activity/base/a;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/a;->g()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 52
    iput-boolean v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    .line 54
    const/4 v0, 0x1

    :try_start_11
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a(Z)V
    :try_end_14
    .catch Ljava/lang/IllegalStateException; {:try_start_11 .. :try_end_14} :catch_15

    .line 66
    :cond_14
    :goto_14
    return-void

    .line 55
    :catch_15
    move-exception v0

    .line 62
    iput-boolean v2, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    .line 63
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a(Z)V

    goto :goto_14
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a:Z

    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "details"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_1e

    .line 38
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    .line 39
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 41
    :cond_1e
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->d()V

    .line 42
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 21
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->setContentView(I)V

    .line 24
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 25
    const v0, 0x7f0b0118

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->setTitle(I)V

    .line 27
    if-nez p1, :cond_1c

    .line 28
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->a(Z)V

    .line 30
    :cond_1c
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 70
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_e

    .line 71
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->finish()V

    .line 72
    const/4 v0, 0x1

    .line 74
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_d
.end method

.method protected onResume()V
    .registers 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onResume()V

    .line 47
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;->d()V

    .line 48
    return-void
.end method
