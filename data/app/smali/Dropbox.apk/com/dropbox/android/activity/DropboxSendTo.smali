.class public Lcom/dropbox/android/activity/DropboxSendTo;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/util/e;
.implements Ldbxyzptlk/g/I;


# instance fields
.field protected a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxSendTo;->a:Z

    return-void
.end method

.method private a(Ljava/util/Collection;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 159
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    .line 160
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    .line 161
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 162
    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/dropbox/android/activity/DropboxSendTo;->checkUriPermission(Landroid/net/Uri;III)I

    move-result v0

    .line 163
    if-nez v0, :cond_d

    move v0, v1

    .line 167
    :goto_20
    return v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_20
.end method

.method private d()Ljava/util/Collection;
    .registers 5

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 129
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 131
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 132
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_1e

    .line 134
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 155
    :cond_1e
    :goto_1e
    return-object v2

    .line 136
    :cond_1f
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_41

    .line 137
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 138
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 139
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 140
    if-eqz v0, :cond_1e

    .line 141
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1e

    .line 144
    :cond_41
    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 145
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 146
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 147
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5f
    :goto_5f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 149
    instance-of v3, v0, Landroid/net/Uri;

    if-eqz v3, :cond_5f

    .line 150
    check-cast v0, Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5f
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 95
    iget-boolean v2, p0, Lcom/dropbox/android/activity/DropboxSendTo;->a:Z

    if-nez v2, :cond_11

    .line 96
    sget-object v2, Lcom/dropbox/android/activity/L;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/util/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_38

    .line 113
    :cond_11
    :goto_11
    :pswitch_11
    return-void

    .line 102
    :pswitch_12
    iput-boolean v0, p0, Lcom/dropbox/android/activity/DropboxSendTo;->a:Z

    .line 103
    new-instance v2, Ldbxyzptlk/g/G;

    sget-object v3, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    if-ne v3, p1, :cond_35

    :goto_1a
    invoke-direct {v2, p0, v0, p2, p3}, Ldbxyzptlk/g/G;-><init>(Landroid/content/Context;ZLjava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    .line 108
    invoke-virtual {v2}, Ldbxyzptlk/g/G;->f()V

    .line 109
    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Ldbxyzptlk/g/G;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->a(Ljava/lang/String;)V

    goto :goto_11

    :cond_35
    move v0, v1

    .line 103
    goto :goto_1a

    .line 96
    nop

    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
    .end packed-switch
.end method

.method public final a(Ljava/util/List;)V
    .registers 2
    .parameter

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->finish()V

    .line 119
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 42
    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/DropboxSendTo;->requestWindowFeature(I)Z

    .line 43
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxSendTo;->setContentView(I)V

    .line 48
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-nez v0, :cond_55

    .line 56
    invoke-direct {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->d()Ljava/util/Collection;

    move-result-object v0

    .line 57
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/DropboxSendTo;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 58
    const v0, 0x7f0b004b

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 59
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->finish()V

    .line 85
    :cond_2e
    :goto_2e
    return-void

    .line 65
    :cond_2f
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 66
    const-string v1, "share_screenshot"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 67
    const-string v1, "share_favicon"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 69
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 70
    const-string v2, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 71
    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/DropboxSendTo;->startActivity(Landroid/content/Intent;)V

    .line 72
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->finish()V

    goto :goto_2e

    .line 76
    :cond_55
    if-nez p1, :cond_2e

    .line 78
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 79
    new-instance v1, Lcom/dropbox/android/activity/SendToFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/SendToFragment;-><init>()V

    .line 80
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/SendToFragment;->a(Landroid/net/Uri;Z)V

    .line 81
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxSendTo;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v0

    .line 82
    const v2, 0x7f06007f

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 83
    invoke-virtual {v0}, Landroid/support/v4/app/u;->b()I

    goto :goto_2e
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .registers 3
    .parameter

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method
