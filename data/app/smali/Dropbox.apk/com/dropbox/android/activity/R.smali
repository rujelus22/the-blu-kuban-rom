.class public final Lcom/dropbox/android/activity/R;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v1, p0, Lcom/dropbox/android/activity/R;->a:Z

    .line 28
    if-eqz p1, :cond_10

    .line 29
    const-string v0, "mWarnIfNoExternalStorage"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/R;->a:Z

    .line 31
    :cond_10
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 37
    const-string v0, "mWarnIfNoExternalStorage"

    iget-boolean v1, p0, Lcom/dropbox/android/activity/R;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    return-void
.end method

.method public final a(Landroid/support/v4/app/FragmentActivity;)V
    .registers 5
    .parameter

    .prologue
    .line 46
    invoke-static {}, Lcom/dropbox/android/util/af;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/R;->a:Z

    .line 54
    :cond_9
    :goto_9
    return-void

    .line 49
    :cond_a
    iget-boolean v0, p0, Lcom/dropbox/android/activity/R;->a:Z

    if-eqz v0, :cond_9

    .line 50
    new-instance v0, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/R;->a:Z

    goto :goto_9
.end method
