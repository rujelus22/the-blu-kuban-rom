.class public Lcom/dropbox/android/activity/CameraUploadSettingsActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/CheckBox;

.field private f:Landroid/widget/RadioButton;

.field private g:Landroid/widget/CompoundButton;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 31
    const-class v0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/CompoundButton;

    return-void
.end method

.method private static a(Ldbxyzptlk/l/m;)Landroid/os/Bundle;
    .registers 4
    .parameter

    .prologue
    .line 55
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 56
    const-string v0, "ENABLED"

    invoke-virtual {p0}, Ldbxyzptlk/l/m;->g()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    const-string v0, "USE_3G"

    invoke-virtual {p0}, Ldbxyzptlk/l/m;->q()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    const-string v2, "UPLOAD_EXISTING"

    invoke-virtual {p0}, Ldbxyzptlk/l/m;->s()Z

    move-result v0

    if-nez v0, :cond_24

    const/4 v0, 0x1

    :goto_20
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 59
    return-object v1

    .line 58
    :cond_24
    const/4 v0, 0x0

    goto :goto_20
.end method

.method static synthetic a(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/RadioButton;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CompoundButton;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/CompoundButton;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .registers 1

    .prologue
    .line 29
    sget-object v0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->d:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter

    .prologue
    const/4 v10, 0x4

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 69
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    const-string v1, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 74
    const-string v1, "FULL_SCREEN"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    invoke-virtual {p0, v0, v2}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 79
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->finish()V

    .line 213
    :goto_2b
    return-void

    .line 84
    :cond_2c
    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->setContentView(I)V

    .line 86
    const v0, 0x7f060032

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/CompoundButton;

    .line 89
    const v0, 0x7f060031

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b:Landroid/widget/Button;

    .line 90
    const v0, 0x7f060030

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c:Landroid/widget/Button;

    .line 91
    const v0, 0x7f060056

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->d:Landroid/widget/CheckBox;

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c:Landroid/widget/Button;

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 94
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b:Landroid/widget/Button;

    const v1, 0x7f0b017d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 96
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v3

    .line 98
    const v0, 0x7f060051

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 99
    const v1, 0x7f060053

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/RadioButton;

    .line 100
    const v1, 0x7f060050

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 101
    const v1, 0x7f060052

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 102
    const v1, 0x7f060054

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 104
    const v1, 0x7f06004f

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 109
    if-nez p1, :cond_a8

    .line 110
    invoke-static {v3}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a(Ldbxyzptlk/l/m;)Landroid/os/Bundle;

    move-result-object p1

    .line 113
    :cond_a8
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/CompoundButton;

    const-string v8, "ENABLED"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v1, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 115
    const-string v1, "USE_3G"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f6

    .line 116
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/RadioButton;

    invoke-virtual {v1, v12}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 117
    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 118
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    :goto_c6
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->d:Landroid/widget/CheckBox;

    const-string v8, "UPLOAD_EXISTING"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    const/4 v1, 0x5

    new-array v8, v1, [Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->d:Landroid/widget/CheckBox;

    aput-object v1, v8, v2

    aput-object v0, v8, v12

    const/4 v1, 0x2

    iget-object v9, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/RadioButton;

    aput-object v9, v8, v1

    const/4 v1, 0x3

    aput-object v4, v8, v1

    aput-object v5, v8, v10

    .line 130
    const-string v1, "ENABLED"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 131
    array-length v10, v8

    move v1, v2

    :goto_ec
    if-ge v1, v10, :cond_102

    aget-object v11, v8, v1

    .line 132
    invoke-virtual {v11, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_ec

    .line 120
    :cond_f6
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/RadioButton;

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 121
    invoke-virtual {v0, v12}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 122
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_c6

    .line 134
    :cond_102
    const v1, 0x7f06004e

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v9, :cond_16c

    const/high16 v1, 0x3f80

    :goto_10d
    invoke-virtual {v10, v1}, Landroid/view/View;->setAlpha(F)V

    .line 137
    invoke-static {}, Lcom/dropbox/android/util/aa;->b()Z

    move-result v1

    if-nez v1, :cond_134

    .line 138
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    .line 139
    const v1, 0x7f060055

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 141
    iput v12, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 142
    iget-object v2, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    :cond_134
    new-instance v1, Lcom/dropbox/android/activity/t;

    invoke-direct {v1, p0, v0, v6}, Lcom/dropbox/android/activity/t;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Landroid/widget/RadioButton;Landroid/view/View;)V

    .line 157
    new-instance v2, Lcom/dropbox/android/activity/u;

    invoke-direct {v2, p0, v0, v6}, Lcom/dropbox/android/activity/u;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Landroid/widget/RadioButton;Landroid/view/View;)V

    .line 166
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    invoke-virtual {v4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/CompoundButton;

    new-instance v1, Lcom/dropbox/android/activity/v;

    invoke-direct {v1, p0, v8}, Lcom/dropbox/android/activity/v;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;[Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/w;

    invoke-direct {v1, p0, v3}, Lcom/dropbox/android/activity/w;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Ldbxyzptlk/l/m;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/x;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/x;-><init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2b

    .line 134
    :cond_16c
    const/high16 v1, 0x3f00

    goto :goto_10d
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 217
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 218
    const-string v0, "ENABLED"

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->g:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 219
    const-string v0, "USE_3G"

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->f:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 220
    const-string v0, "UPLOAD_EXISTING"

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 221
    return-void
.end method
