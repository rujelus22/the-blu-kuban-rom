.class final Lcom/dropbox/android/activity/dialog/d;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/app/AlertDialog;

.field final synthetic d:Landroid/widget/ProgressBar;

.field final synthetic e:Landroid/widget/TextView;

.field final synthetic f:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Landroid/widget/EditText;Ljava/lang/String;Landroid/app/AlertDialog;Landroid/widget/ProgressBar;Landroid/widget/TextView;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/d;->f:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    iput-object p2, p0, Lcom/dropbox/android/activity/dialog/d;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/dropbox/android/activity/dialog/d;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/dropbox/android/activity/dialog/d;->c:Landroid/app/AlertDialog;

    iput-object p5, p0, Lcom/dropbox/android/activity/dialog/d;->d:Landroid/widget/ProgressBar;

    iput-object p6, p0, Lcom/dropbox/android/activity/dialog/d;->e:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/d;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 158
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/d;->c:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 160
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 161
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/d;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 162
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/d;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/d;->f:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/bc;->a(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 163
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/d;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/d;->e:Landroid/widget/TextView;

    const v2, 0x7f0b009d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 166
    new-instance v1, Ldbxyzptlk/g/v;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/d;->f:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/dialog/d;->f:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    iget-object v3, v3, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Ldbxyzptlk/g/x;

    invoke-direct {v1, v2, v0, v3}, Ldbxyzptlk/g/v;-><init>(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/g/x;)V

    .line 167
    invoke-virtual {v1}, Ldbxyzptlk/g/v;->f()V

    .line 168
    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Ldbxyzptlk/g/v;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 169
    return-void
.end method
