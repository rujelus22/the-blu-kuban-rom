.class final Lcom/dropbox/android/activity/cy;
.super Landroid/os/AsyncTask;
.source "panda.py"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/dropbox/android/activity/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 419
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 420
    iput-object p2, p0, Lcom/dropbox/android/activity/cy;->a:Ljava/lang/String;

    .line 421
    iput-object p1, p0, Lcom/dropbox/android/activity/cy;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    .line 422
    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ldbxyzptlk/r/D;
    .registers 5
    .parameter

    .prologue
    .line 435
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/activity/VideoPlayerActivity;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fetching "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/cy;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v1, p0, Lcom/dropbox/android/activity/cy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/r/i;->d(Ljava/lang/String;)Ldbxyzptlk/r/D;
    :try_end_27
    .catch Ldbxyzptlk/o/a; {:try_start_0 .. :try_end_27} :catch_29

    move-result-object v0

    .line 441
    :goto_28
    return-object v0

    .line 438
    :catch_29
    move-exception v0

    .line 439
    invoke-static {}, Lcom/dropbox/android/activity/VideoPlayerActivity;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AdjustMetricsAsyncTask"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 441
    const/4 v0, 0x0

    goto :goto_28
.end method

.method public final a()V
    .registers 2

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/cy;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    .line 430
    return-void
.end method

.method protected final a(Ldbxyzptlk/r/D;)V
    .registers 7
    .parameter

    .prologue
    .line 446
    if-nez p1, :cond_3

    .line 461
    :cond_2
    :goto_2
    return-void

    .line 449
    :cond_3
    iget-object v0, p0, Lcom/dropbox/android/activity/cy;->b:Lcom/dropbox/android/activity/VideoPlayerActivity;

    .line 450
    if-eqz v0, :cond_2

    .line 451
    const-wide v1, 0x408f400000000000L

    iget-wide v3, p1, Ldbxyzptlk/r/D;->f:D

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;I)V

    .line 452
    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;

    move-result-object v1

    iget-wide v2, p1, Ldbxyzptlk/r/D;->d:J

    long-to-int v2, v2

    iget-wide v3, p1, Ldbxyzptlk/r/D;->e:J

    long-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/widget/DbxVideoView;->a(II)V

    .line 457
    iget-wide v1, p1, Ldbxyzptlk/r/D;->a:D

    iget-wide v3, p1, Ldbxyzptlk/r/D;->f:D

    sub-double/2addr v1, v3

    const-wide/high16 v3, 0x4014

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_2

    .line 458
    const v1, 0x7f0b01cc

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 413
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/cy;->a([Ljava/lang/Void;)Ldbxyzptlk/r/D;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 413
    check-cast p1, Ldbxyzptlk/r/D;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/cy;->a(Ldbxyzptlk/r/D;)V

    return-void
.end method
