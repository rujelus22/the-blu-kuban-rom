.class final enum Lcom/dropbox/android/activity/bM;
.super Lcom/dropbox/android/activity/bI;
.source "panda.py"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/activity/bI;-><init>(Ljava/lang/String;ILcom/dropbox/android/activity/bF;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 121
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 122
    const v1, 0x7f0b0180

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 123
    const v1, 0x7f0b0181

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 125
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 127
    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 128
    const v2, 0x7f0b0182

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 129
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setLines(I)V

    .line 130
    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 132
    new-instance v2, Lcom/dropbox/android/activity/bN;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bN;-><init>(Lcom/dropbox/android/activity/bM;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 153
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 155
    new-instance v2, Lcom/dropbox/android/activity/bO;

    invoke-direct {v2, p0, v1, p1}, Lcom/dropbox/android/activity/bO;-><init>(Lcom/dropbox/android/activity/bM;Landroid/widget/EditText;Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 196
    const v1, 0x7f0b0019

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 197
    const v1, 0x7f0b0018

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 199
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
