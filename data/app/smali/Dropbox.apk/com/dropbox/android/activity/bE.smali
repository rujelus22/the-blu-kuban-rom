.class final Lcom/dropbox/android/activity/bE;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/SendToFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/SendToFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 125
    iput-object p1, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 135
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 137
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_43

    .line 138
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_24

    .line 140
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_24
    :goto_24
    if-eqz v2, :cond_42

    .line 173
    iget-object v0, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/e;

    iget-object v1, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    iget-object v4, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v4}, Lcom/dropbox/android/activity/SendToFragment;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Landroid/content/Context;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Z

    .line 177
    :cond_42
    :goto_42
    return-void

    .line 142
    :cond_43
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7d

    .line 143
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 144
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_65

    .line 145
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 146
    if-eqz v0, :cond_24

    .line 147
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_24

    .line 149
    :cond_65
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 150
    iget-object v0, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a(Lcom/dropbox/android/activity/SendToFragment;)Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/SendToFragment;->getFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a(Landroid/support/v4/app/j;)V

    goto :goto_42

    .line 153
    :cond_7d
    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b1

    .line 154
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 155
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 156
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 157
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9b
    :goto_9b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 158
    instance-of v3, v0, Landroid/net/Uri;

    if-eqz v3, :cond_9b

    .line 159
    check-cast v0, Landroid/net/Uri;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_9b

    .line 163
    :cond_b1
    const-string v0, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 164
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 165
    iget-object v1, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/SendToFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 166
    iget-object v1, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 167
    iget-object v0, p0, Lcom/dropbox/android/activity/bE;->a:Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_42
.end method
