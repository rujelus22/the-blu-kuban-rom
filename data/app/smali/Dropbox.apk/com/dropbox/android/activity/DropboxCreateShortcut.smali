.class public Lcom/dropbox/android/activity/DropboxCreateShortcut;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 17
    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->requestWindowFeature(I)Z

    .line 18
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->setContentView(I)V

    .line 23
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-nez v0, :cond_25

    .line 25
    const v0, 0x7f0b004b

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 27
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->finish()V

    .line 38
    :cond_24
    :goto_24
    return-void

    .line 31
    :cond_25
    if-nez p1, :cond_24

    .line 33
    new-instance v0, Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/CreateShortcutFragment;-><init>()V

    .line 34
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxCreateShortcut;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v1

    .line 35
    const v2, 0x7f06007f

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 36
    invoke-virtual {v1}, Landroid/support/v4/app/u;->b()I

    goto :goto_24
.end method
