.class public Lcom/dropbox/android/activity/MainBrowserFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/quickactions/e;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    .line 28
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/MainBrowserFragment;->setHasOptionsMenu(Z)V

    .line 29
    return-void
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    const/4 v2, 0x0

    const-class v3, Lcom/dropbox/android/activity/TextEditActivity;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/dropbox/android/util/af;->j()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1c

    .line 87
    invoke-static {v1}, Lcom/dropbox/android/util/af;->b(Ljava/io/File;)Z

    .line 89
    :cond_1c
    const-string v2, "output"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 91
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 115
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 116
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Z

    move-result v1

    if-nez v1, :cond_18

    .line 117
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    .line 119
    :goto_17
    return-object v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 101
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 110
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/widget/X;
    .registers 2

    .prologue
    .line 95
    sget-object v0, Lcom/dropbox/android/widget/X;->a:Lcom/dropbox/android/widget/X;

    return-object v0
.end method

.method protected final d()I
    .registers 2

    .prologue
    .line 105
    const v0, 0x7f0b003e

    return v0
.end method

.method protected final e()I
    .registers 2

    .prologue
    .line 125
    const v0, 0x7f03002a

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 132
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;Lcom/actionbarsherlock/view/MenuInflater;)V

    .line 40
    const/16 v0, 0xc9

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0089

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v4, v0, v4, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x1080055

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 42
    const/16 v0, 0xcd

    const/high16 v1, 0x1

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0086

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v4, v0, v1, v2}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02017a

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setShowAsAction(I)V

    .line 45
    const/16 v0, 0xca

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0087

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v4, v0, v4, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200d6

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 47
    const/16 v0, 0xcb

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0088

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v4, v0, v4, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200d5

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 49
    const/16 v0, 0xcc

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0085

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v4, v0, v4, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200d7

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 51
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 55
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_68

    .line 79
    :goto_8
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_c
    return v0

    .line 58
    :pswitch_d
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/UploadDialog;->a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/activity/dialog/UploadDialog;

    move-result-object v1

    .line 59
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getFragmentManager()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/UploadDialog;->a(Landroid/support/v4/app/j;)V

    goto :goto_c

    .line 64
    :pswitch_22
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v1

    .line 65
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 66
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getFragmentManager()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Landroid/support/v4/app/j;)V

    goto :goto_c

    .line 70
    :pswitch_3f
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/activity/MainBrowserFragment;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_c

    .line 73
    :pswitch_54
    invoke-static {}, Lcom/dropbox/android/util/i;->ah()Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    .line 74
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->g()V

    goto :goto_c

    .line 77
    :pswitch_5f
    invoke-virtual {p0}, Lcom/dropbox/android/activity/MainBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onSearchRequested()Z

    goto :goto_8

    .line 55
    nop

    :pswitch_data_68
    .packed-switch 0xc9
        :pswitch_d
        :pswitch_22
        :pswitch_3f
        :pswitch_54
        :pswitch_5f
    .end packed-switch
.end method
