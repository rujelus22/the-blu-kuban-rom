.class public Lcom/dropbox/android/activity/PrefsActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterPreferenceActivity;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Landroid/preference/Preference;

.field private d:Landroid/preference/Preference;

.field private e:Landroid/preference/PreferenceCategory;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/preference/ListPreference;

.field private h:Landroid/preference/ListPreference;

.field private i:Landroid/preference/CheckBoxPreference;

.field private j:Landroid/preference/Preference;

.field private k:Landroid/preference/Preference;

.field private l:Landroid/preference/Preference;

.field private m:Landroid/preference/Preference;

.field private n:Landroid/preference/Preference;

.field private o:Landroid/preference/Preference;

.field private p:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 60
    const-class v0, Lcom/dropbox/android/activity/PrefsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/PrefsActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterPreferenceActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .registers 8
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 506
    const-string v0, ""

    .line 508
    :try_start_3
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 509
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_12
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_12} :catch_c6

    .line 513
    :goto_12
    const v1, 0x7f0b000f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 515
    const v2, 0x7f0b0143

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 516
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 518
    const v0, 0x7f0b0144

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 519
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v3

    .line 520
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n\n\n\nInfo: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v3, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v3, Ldbxyzptlk/j/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v3, Ldbxyzptlk/j/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " UID: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v3, Ldbxyzptlk/j/g;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 523
    const/16 v0, 0x3e8

    invoke-static {v0}, Ldbxyzptlk/j/f;->a(I)Ljava/util/List;

    move-result-object v0

    .line 524
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 525
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ad

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 526
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v6, 0xa

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_8a

    .line 529
    :cond_ad
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 531
    invoke-static {p0, v1, v2, v0}, Lcom/dropbox/android/activity/PrefsActivity;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    return-void

    .line 510
    :catch_c6
    move-exception v1

    goto/16 :goto_12
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-static {p0, p1, p2, p3}, Lcom/dropbox/android/activity/PrefsActivity;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/l/m;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/PrefsActivity;->a(Ldbxyzptlk/l/m;)V

    return-void
.end method

.method private a(Ldbxyzptlk/l/m;)V
    .registers 4
    .parameter

    .prologue
    .line 316
    invoke-virtual {p1}, Ldbxyzptlk/l/m;->g()Z

    move-result v0

    if-eqz v0, :cond_76

    .line 317
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/Preference;

    const v1, 0x7f0b00fb

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 319
    invoke-static {}, Lcom/dropbox/android/util/aa;->b()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 320
    invoke-virtual {p1}, Ldbxyzptlk/l/m;->q()Z

    move-result v0

    if-eqz v0, :cond_59

    const-string v0, "3g"

    .line 321
    :goto_1c
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 325
    invoke-virtual {p1}, Ldbxyzptlk/l/m;->q()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 326
    invoke-virtual {p1}, Ldbxyzptlk/l/m;->r()Z

    move-result v0

    if-eqz v0, :cond_5c

    const-string v0, "limit"

    .line 327
    :goto_41
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 328
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 343
    :goto_58
    return-void

    .line 320
    :cond_59
    const-string v0, "wifi"

    goto :goto_1c

    .line 326
    :cond_5c
    const-string v0, "nolimit"

    goto :goto_41

    .line 331
    :cond_5f
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_58

    .line 335
    :cond_67
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 336
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_58

    .line 339
    :cond_76
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/Preference;

    const v1, 0x7f0b00fa

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 340
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 341
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_58
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 347
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    .line 348
    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 349
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    if-eqz p1, :cond_17

    .line 352
    const-string v2, "android.intent.extra.EMAIL"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    :cond_17
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    const-string v1, "message/rfc822"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    :try_start_26
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_29
    .catch Landroid/content/ActivityNotFoundException; {:try_start_26 .. :try_end_29} :catch_2a

    .line 363
    :goto_29
    return-void

    .line 360
    :catch_2a
    move-exception v0

    .line 361
    sget-object v0, Lcom/dropbox/android/activity/PrefsActivity;->b:Ljava/lang/String;

    const-string v1, "Mail app not found."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_29
.end method


# virtual methods
.method public final a(J)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 310
    invoke-static {p0, p1, p2, v2}, Lcom/dropbox/android/util/aj;->a(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 311
    const v1, 0x7f0b00f6

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 312
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->o:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 313
    return-void
.end method

.method public final a(Ldbxyzptlk/r/w;J)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 281
    if-eqz p1, :cond_5b

    .line 282
    iget-wide v0, p1, Ldbxyzptlk/r/w;->c:J

    .line 283
    iget-wide v2, p1, Ldbxyzptlk/r/w;->d:J

    .line 284
    iget-wide v4, p1, Ldbxyzptlk/r/w;->e:J

    .line 286
    add-long/2addr v2, v4

    invoke-static {p0, v2, v3, v0, v1}, Lcom/dropbox/android/util/aj;->a(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v2

    .line 288
    const/4 v3, -0x1

    invoke-static {p0, v0, v1, v3}, Lcom/dropbox/android/util/aj;->a(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 290
    const v1, 0x7f0b00e9

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v7

    aput-object v0, v3, v6

    invoke-static {v1, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->d:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v0, p1, Ldbxyzptlk/r/w;->h:Ljava/lang/String;

    if-eqz v0, :cond_4f

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Landroid/preference/Preference;

    iget-object v1, p1, Ldbxyzptlk/r/w;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 304
    :cond_36
    :goto_36
    invoke-static {p0, p2, p3, v6}, Lcom/dropbox/android/util/aj;->a(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 305
    const v1, 0x7f0b00f6

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lcom/dropbox/android/activity/PrefsActivity;->o:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 307
    return-void

    .line 296
    :cond_4f
    iget-object v0, p1, Ldbxyzptlk/r/w;->b:Ljava/lang/String;

    if-eqz v0, :cond_36

    .line 297
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Landroid/preference/Preference;

    iget-object v1, p1, Ldbxyzptlk/r/w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_36

    .line 301
    :cond_5b
    sget-object v0, Lcom/dropbox/android/activity/PrefsActivity;->b:Ljava/lang/String;

    const-string v1, "Null returned from account info call!"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_36
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 87
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->requestWindowFeature(I)Z

    .line 89
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v1

    .line 92
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v2

    .line 95
    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->addPreferencesFromResource(I)V

    .line 97
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 99
    const-string v0, "settings_name"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->c:Landroid/preference/Preference;

    .line 100
    const-string v0, "settings_space"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->d:Landroid/preference/Preference;

    .line 102
    const-string v0, "camera_upload_category"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    .line 104
    const-string v0, "camera_upload_on_off"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/Preference;

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->f:Landroid/preference/Preference;

    new-instance v3, Lcom/dropbox/android/activity/bm;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/bm;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/l/m;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 124
    const-string v0, "camera_upload_connection"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    .line 125
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->g:Landroid/preference/ListPreference;

    new-instance v3, Lcom/dropbox/android/activity/bt;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/bt;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/l/m;)V

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 136
    const-string v0, "camera_upload_3g_limit"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    .line 137
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->h:Landroid/preference/ListPreference;

    new-instance v3, Lcom/dropbox/android/activity/bu;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/bu;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/l/m;)V

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 151
    invoke-static {}, Lcom/dropbox/android/util/aa;->a()Z

    move-result v0

    if-nez v0, :cond_81

    .line 152
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v3, p0, Lcom/dropbox/android/activity/PrefsActivity;->e:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 155
    :cond_81
    const-string v0, "settings_lock_code"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/CheckBoxPreference;

    .line 156
    invoke-virtual {v2}, Ldbxyzptlk/l/a;->d()Z

    move-result v0

    .line 157
    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/dropbox/android/activity/bv;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bv;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 168
    const-string v0, "settings_unlink"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/Preference;

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->j:Landroid/preference/Preference;

    new-instance v2, Lcom/dropbox/android/activity/bw;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bw;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 177
    const-string v0, "settings_version"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->k:Landroid/preference/Preference;

    .line 178
    const-string v0, ""

    .line 180
    :try_start_ba
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 181
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 182
    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->k:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_ce
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_ba .. :try_end_ce} :catch_157

    .line 187
    :goto_ce
    const-string v0, "settings_watch_video"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->l:Landroid/preference/Preference;

    .line 188
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->l:Landroid/preference/Preference;

    new-instance v2, Lcom/dropbox/android/activity/bx;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bx;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 196
    const-string v0, "settings_tell_friends"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->m:Landroid/preference/Preference;

    .line 197
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->m:Landroid/preference/Preference;

    new-instance v2, Lcom/dropbox/android/activity/by;

    invoke-direct {v2, p0, v1}, Lcom/dropbox/android/activity/by;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/l/m;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 216
    const-string v0, "settings_legal"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v2, Lcom/dropbox/android/activity/bz;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bz;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 240
    const-string v0, "settings_send_feedback"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    .line 241
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->n:Landroid/preference/Preference;

    new-instance v2, Lcom/dropbox/android/activity/bB;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bB;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 250
    const-string v0, "settings_cache"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->o:Landroid/preference/Preference;

    .line 252
    const-string v0, "settings_clear_cache"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->p:Landroid/preference/Preference;

    .line 253
    iget-object v0, p0, Lcom/dropbox/android/activity/PrefsActivity;->p:Landroid/preference/Preference;

    new-instance v2, Lcom/dropbox/android/activity/bn;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bn;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 266
    invoke-virtual {v1}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_14f

    .line 268
    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/activity/PrefsActivity;->a(Ldbxyzptlk/r/w;J)V

    .line 274
    :goto_137
    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/PrefsActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 275
    new-instance v0, Ldbxyzptlk/g/O;

    invoke-direct {v0, p0}, Ldbxyzptlk/g/O;-><init>(Landroid/content/Context;)V

    .line 276
    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/O;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 277
    const v0, 0x7f0b00e5

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 278
    return-void

    .line 270
    :cond_14f
    sget-object v0, Lcom/dropbox/android/activity/PrefsActivity;->b:Ljava/lang/String;

    const-string v1, "onCreate(): Account info empty, should be set"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_137

    .line 183
    :catch_157
    move-exception v0

    goto/16 :goto_ce
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 367
    packed-switch p1, :pswitch_data_7e

    .line 412
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected dialog id in PrefsActivity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :pswitch_1e
    new-instance v0, Lcom/dropbox/android/activity/bo;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bo;-><init>(Lcom/dropbox/android/activity/PrefsActivity;)V

    .line 386
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 387
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 388
    const v2, 0x7f0b0147

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 389
    const v0, 0x7f0b0148

    invoke-virtual {v1, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 391
    const v0, 0x7f0b0145

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 392
    const v0, 0x7f0b0146

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 393
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 409
    :goto_47
    return-object v0

    .line 396
    :pswitch_48
    invoke-static {p0}, Lcom/dropbox/android/util/a;->a(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_47

    .line 401
    :pswitch_4d
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0c0080

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 402
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 403
    const v2, 0x7f0b013e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 404
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 405
    const v2, 0x7f030058

    invoke-static {v0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 406
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 407
    const v0, 0x7f0b013f

    invoke-virtual {v1, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 408
    const v0, 0x7f0b0018

    invoke-virtual {v1, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 409
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_47

    .line 367
    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_48
        :pswitch_4d
    .end packed-switch
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 536
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_e

    .line 537
    invoke-virtual {p0}, Lcom/dropbox/android/activity/PrefsActivity;->finish()V

    .line 538
    const/4 v0, 0x1

    .line 540
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterPreferenceActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    goto :goto_d
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 418
    packed-switch p1, :pswitch_data_50

    .line 494
    :goto_3
    return-void

    .line 420
    :pswitch_4
    const v0, 0x7f0600dc

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    move-object v0, p2

    .line 421
    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 422
    const v0, 0x7f0600f5

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 423
    const v0, 0x7f0600f7

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 425
    const-string v0, ""

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 426
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 427
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 428
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 431
    invoke-virtual {p2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 462
    new-instance v0, Lcom/dropbox/android/activity/bq;

    invoke-direct {v0, p0, v2}, Lcom/dropbox/android/activity/bq;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Landroid/widget/Button;)V

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 478
    new-instance v0, Lcom/dropbox/android/activity/br;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/br;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Landroid/widget/Button;Landroid/view/View;Landroid/view/View;Landroid/widget/EditText;)V

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 418
    :pswitch_data_50
    .packed-switch 0x3
        :pswitch_4
    .end packed-switch
.end method

.method protected onResume()V
    .registers 4

    .prologue
    .line 498
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    .line 499
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v1

    .line 500
    iget-object v2, p0, Lcom/dropbox/android/activity/PrefsActivity;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Ldbxyzptlk/l/a;->d()Z

    move-result v1

    invoke-virtual {v2, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 501
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/PrefsActivity;->a(Ldbxyzptlk/l/m;)V

    .line 502
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterPreferenceActivity;->onResume()V

    .line 503
    return-void
.end method
