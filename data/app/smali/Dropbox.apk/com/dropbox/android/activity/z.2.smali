.class final Lcom/dropbox/android/activity/z;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/CreateShortcutFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/CreateShortcutFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    iget-object v1, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/CreateShortcutFragment;->f()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/CreateShortcutFragment;->f()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v2}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200c5

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/activity/CreateShortcutFragment;->a(Lcom/dropbox/android/activity/CreateShortcutFragment;Landroid/net/Uri;Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/activity/z;->a:Lcom/dropbox/android/activity/CreateShortcutFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 83
    :cond_3c
    return-void
.end method
