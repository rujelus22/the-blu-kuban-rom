.class public Lcom/dropbox/android/activity/dialog/NoSdCardDialog;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    .line 19
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 21
    const v1, 0x7f0b0036

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 22
    const v1, 0x7f0b0019

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 23
    const v1, 0x7f0b0044

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 24
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 35
    sget-object v0, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    sget-object v0, Lcom/dropbox/android/activity/dialog/NoSdCardDialog;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 31
    return-void
.end method
