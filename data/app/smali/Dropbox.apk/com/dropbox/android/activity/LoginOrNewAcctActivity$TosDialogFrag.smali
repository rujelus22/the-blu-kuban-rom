.class public Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBaseWCallback;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;-><init>()V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/bd;)Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;
    .registers 4
    .parameter

    .prologue
    .line 122
    new-instance v0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;-><init>()V

    .line 123
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 124
    const-string v2, "account_details"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 126
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    .line 131
    new-instance v0, Lcom/dropbox/android/activity/be;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/be;-><init>(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)V

    .line 140
    new-instance v1, Lcom/dropbox/android/activity/bf;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/bf;-><init>(Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;)V

    .line 151
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginOrNewAcctActivity$TosDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 152
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 153
    const v3, 0x7f0b0031

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 154
    const v0, 0x7f0b0032

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    const v0, 0x7f0b002f

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 157
    const v0, 0x7f0b0030

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 158
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
