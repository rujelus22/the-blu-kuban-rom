.class public Lcom/dropbox/android/activity/CreateShortcutFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"


# instance fields
.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.BROWSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 95
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 99
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 100
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 101
    const-string v0, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v0

    .line 103
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/CreateShortcutFragment;Landroid/net/Uri;Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/activity/CreateShortcutFragment;->a(Landroid/net/Uri;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 27
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 28
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Z

    move-result v1

    if-nez v1, :cond_18

    .line 29
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    .line 31
    :goto_17
    return-object v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method protected final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 37
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CreateShortcutFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/widget/X;
    .registers 2

    .prologue
    .line 42
    sget-object v0, Lcom/dropbox/android/widget/X;->b:Lcom/dropbox/android/widget/X;

    return-object v0
.end method

.method protected final d()I
    .registers 2

    .prologue
    .line 47
    const v0, 0x7f0b003e

    return v0
.end method

.method protected final e()I
    .registers 2

    .prologue
    .line 52
    const v0, 0x7f030028

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 58
    const v0, 0x7f060030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->j:Landroid/widget/Button;

    .line 59
    iget-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->j:Landroid/widget/Button;

    const v2, 0x7f0b0018

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 60
    new-instance v0, Lcom/dropbox/android/activity/y;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/y;-><init>(Lcom/dropbox/android/activity/CreateShortcutFragment;)V

    .line 66
    iget-object v2, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->j:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    const v0, 0x7f060031

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->k:Landroid/widget/Button;

    .line 69
    iget-object v0, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->k:Landroid/widget/Button;

    const v2, 0x7f0b0164

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 70
    new-instance v0, Lcom/dropbox/android/activity/z;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/z;-><init>(Lcom/dropbox/android/activity/CreateShortcutFragment;)V

    .line 85
    iget-object v2, p0, Lcom/dropbox/android/activity/CreateShortcutFragment;->k:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v0, 0x7f060075

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 88
    const v2, 0x7f0b0163

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 89
    return-object v1
.end method
