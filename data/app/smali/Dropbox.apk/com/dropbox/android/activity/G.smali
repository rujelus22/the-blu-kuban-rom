.class abstract enum Lcom/dropbox/android/activity/G;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/G;

.field public static final enum b:Lcom/dropbox/android/activity/G;

.field public static final enum c:Lcom/dropbox/android/activity/G;

.field private static final synthetic e:[Lcom/dropbox/android/activity/G;


# instance fields
.field private d:Landroid/support/v4/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lcom/dropbox/android/activity/H;

    const-string v1, "BROWSER"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/H;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    .line 60
    new-instance v0, Lcom/dropbox/android/activity/I;

    const-string v1, "FAVORITES"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/I;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/G;->b:Lcom/dropbox/android/activity/G;

    .line 66
    new-instance v0, Lcom/dropbox/android/activity/J;

    const-string v1, "PHOTOS"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/J;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/G;->c:Lcom/dropbox/android/activity/G;

    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dropbox/android/activity/G;

    sget-object v1, Lcom/dropbox/android/activity/G;->a:Lcom/dropbox/android/activity/G;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/G;->b:Lcom/dropbox/android/activity/G;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/G;->c:Lcom/dropbox/android/activity/G;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dropbox/android/activity/G;->e:[Lcom/dropbox/android/activity/G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/D;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/G;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .registers 7
    .parameter

    .prologue
    .line 95
    invoke-static {}, Lcom/dropbox/android/activity/G;->values()[Lcom/dropbox/android/activity/G;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_1b

    aget-object v3, v1, v0

    .line 96
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/activity/G;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    iput-object v4, v3, Lcom/dropbox/android/activity/G;->d:Landroid/support/v4/app/Fragment;

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 98
    :cond_1b
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/G;
    .registers 2
    .parameter

    .prologue
    .line 53
    const-class v0, Lcom/dropbox/android/activity/G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/G;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/G;
    .registers 1

    .prologue
    .line 53
    sget-object v0, Lcom/dropbox/android/activity/G;->e:[Lcom/dropbox/android/activity/G;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/G;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/G;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/Fragment;
    .registers 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/dropbox/android/activity/G;->c()Z

    move-result v0

    if-nez v0, :cond_c

    .line 78
    invoke-virtual {p0}, Lcom/dropbox/android/activity/G;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/G;->d:Landroid/support/v4/app/Fragment;

    .line 80
    :cond_c
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KEY_FRAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/G;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dropbox/android/activity/G;->d:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected abstract d()Landroid/support/v4/app/Fragment;
.end method
