.class public Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/content/DialogInterface$OnClickListener;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    .line 77
    new-instance v0, Lcom/dropbox/android/activity/dialog/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/a;-><init>(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->d:Landroid/text/TextWatcher;

    .line 99
    new-instance v0, Lcom/dropbox/android/activity/dialog/b;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/b;-><init>(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;)Landroid/widget/EditText;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method public static a(Lcom/dropbox/android/activity/SendToFragment;)Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;
    .registers 3
    .parameter

    .prologue
    .line 27
    new-instance v0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;-><init>()V

    .line 28
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 29
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 70
    sget-object v0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 36
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 37
    const v1, 0x7f0b0180

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 38
    const v1, 0x7f0b0181

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 39
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 41
    new-instance v1, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    .line 42
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    const v2, 0x1020003

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setId(I)V

    .line 43
    if-nez p1, :cond_40

    .line 44
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    const v2, 0x7f0b0182

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 45
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setLines(I)V

    .line 46
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 48
    :cond_40
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->d:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 49
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 51
    const v1, 0x7f0b0019

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->b:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 52
    const v1, 0x7f0b0018

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 54
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 65
    sget-object v0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 59
    sget-object v0, Lcom/dropbox/android/activity/dialog/NewFileNameDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 61
    return-void
.end method
