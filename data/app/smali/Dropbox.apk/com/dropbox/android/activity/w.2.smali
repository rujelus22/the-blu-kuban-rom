.class final Lcom/dropbox/android/activity/w;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ldbxyzptlk/l/m;

.field final synthetic b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;Ldbxyzptlk/l/m;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 184
    iput-object p1, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/w;->a:Ldbxyzptlk/l/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 188
    iget-object v2, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v2}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CompoundButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_90

    .line 189
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Enabled."

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Use 3g: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v4}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/RadioButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Upload existing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v4}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iget-object v2, p0, Lcom/dropbox/android/activity/w;->a:Ldbxyzptlk/l/m;

    iget-object v3, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v3}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->a(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/RadioButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ldbxyzptlk/l/m;->f(Z)V

    .line 193
    iget-object v2, p0, Lcom/dropbox/android/activity/w;->a:Ldbxyzptlk/l/m;

    iget-object v3, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v3}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->c(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_7d

    move v0, v1

    :cond_7d
    invoke-virtual {v2, v0}, Ldbxyzptlk/l/m;->h(Z)V

    .line 194
    iget-object v0, p0, Lcom/dropbox/android/activity/w;->a:Ldbxyzptlk/l/m;

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->g(Z)V

    .line 195
    iget-object v0, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v1, v0}, Lcom/dropbox/android/service/CameraUploadService;->a(ZLandroid/content/Context;)V

    .line 200
    :goto_8a
    iget-object v0, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->finish()V

    .line 201
    return-void

    .line 197
    :cond_90
    invoke-static {}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Disabled."

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v1, p0, Lcom/dropbox/android/activity/w;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/CameraUploadService;->a(ZLandroid/content/Context;)V

    goto :goto_8a
.end method
