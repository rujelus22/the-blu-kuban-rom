.class public abstract Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/y;
.implements Lcom/dropbox/android/activity/K;
.implements Lcom/dropbox/android/activity/ax;
.implements Lcom/dropbox/android/widget/c;
.implements Lcom/dropbox/android/widget/quickactions/f;


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field protected a:Lcom/dropbox/android/activity/delegate/k;

.field protected b:Lcom/dropbox/android/util/bg;

.field protected c:I

.field protected d:Lcom/dropbox/android/util/aq;

.field protected e:Lcom/dropbox/android/widget/DropboxItemListView;

.field protected f:Landroid/widget/TextView;

.field protected g:Landroid/view/View;

.field protected h:Landroid/view/View;

.field protected i:Landroid/view/View;

.field private k:Lcom/dropbox/android/taskqueue/p;

.field private l:Ldbxyzptlk/a/d;

.field private m:Landroid/widget/TextView;

.field private n:Lcom/dropbox/android/widget/S;

.field private o:I

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 61
    const-class v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 63
    new-instance v0, Lcom/dropbox/android/activity/delegate/k;

    invoke-direct {v0}, Lcom/dropbox/android/activity/delegate/k;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a:Lcom/dropbox/android/activity/delegate/k;

    .line 64
    new-instance v0, Lcom/dropbox/android/util/bg;

    invoke-direct {v0}, Lcom/dropbox/android/util/bg;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b:Lcom/dropbox/android/util/bg;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:I

    .line 273
    new-instance v0, Lcom/dropbox/android/activity/as;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/as;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->n:Lcom/dropbox/android/widget/S;

    .line 625
    return-void
.end method

.method private a(III)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 573
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k:Lcom/dropbox/android/taskqueue/p;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/p;->c()V

    .line 575
    iget v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:I

    iget v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    add-int/2addr v2, v0

    .line 576
    add-int v3, p1, p2

    .line 578
    iget v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 579
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 580
    invoke-static {v2, p3}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v2, v0

    .line 583
    :goto_1c
    if-ge v2, v4, :cond_7b

    .line 584
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, v2}, Lcom/dropbox/android/widget/DropboxItemListView;->a(I)Landroid/database/Cursor;

    move-result-object v5

    .line 585
    if-nez v5, :cond_2a

    .line 583
    :cond_26
    :goto_26
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    .line 589
    :cond_2a
    invoke-static {v5}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v0

    .line 590
    sget-object v6, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    if-ne v0, v6, :cond_26

    .line 594
    const/4 v0, 0x5

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 595
    const/4 v0, 0x7

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_79

    const/4 v0, 0x1

    .line 598
    :goto_3f
    if-eqz v0, :cond_26

    if-lt v2, p1, :cond_26

    if-ge v2, v3, :cond_26

    .line 599
    const/16 v0, 0xd

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 600
    if-nez v0, :cond_26

    .line 603
    new-instance v0, Lcom/dropbox/android/taskqueue/MetadataTask;

    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v5

    new-instance v7, Lcom/dropbox/android/util/DropboxPath;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v0, v5, v6, v7, v1}, Lcom/dropbox/android/taskqueue/MetadataTask;-><init>(Lcom/dropbox/android/provider/h;Landroid/net/Uri;Lcom/dropbox/android/provider/A;I)V

    .line 605
    iget-object v5, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k:Lcom/dropbox/android/taskqueue/p;

    invoke-virtual {v5, v0}, Lcom/dropbox/android/taskqueue/p;->b(Lcom/dropbox/android/taskqueue/k;)V

    goto :goto_26

    :cond_79
    move v0, v1

    .line 595
    goto :goto_3f

    .line 610
    :cond_7b
    iput p1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:I

    .line 611
    iput p2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    .line 612
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;III)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(III)V

    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/database/Cursor;)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 476
    invoke-static {p1}, Lcom/dropbox/android/util/DropboxPath;->a(Landroid/net/Uri;)Z

    move-result v6

    .line 477
    if-eqz v6, :cond_45

    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_15
    move v1, v2

    .line 479
    :goto_16
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_44

    .line 480
    invoke-interface {p2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 481
    invoke-static {p2}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v4

    .line 482
    const/4 v3, 0x0

    .line 483
    if-eqz v6, :cond_55

    .line 485
    sget-object v7, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    if-ne v4, v7, :cond_4a

    .line 486
    const-string v4, "path"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 490
    :goto_30
    if-eq v4, v5, :cond_3a

    .line 491
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 498
    :cond_3a
    :goto_3a
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_64

    .line 499
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b(I)V

    .line 500
    const/4 v2, 0x1

    .line 504
    :cond_44
    return v2

    .line 477
    :cond_45
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    .line 487
    :cond_4a
    sget-object v7, Lcom/dropbox/android/provider/P;->c:Lcom/dropbox/android/provider/P;

    if-ne v4, v7, :cond_67

    .line 488
    const-string v4, "intended_db_path"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    goto :goto_30

    .line 494
    :cond_55
    sget-object v7, Lcom/dropbox/android/provider/P;->c:Lcom/dropbox/android/provider/P;

    if-ne v4, v7, :cond_3a

    .line 495
    const-string v3, "local_uri"

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3a

    .line 479
    :cond_64
    add-int/lit8 v1, v1, 0x1

    goto :goto_16

    :cond_67
    move v4, v5

    goto :goto_30
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 508
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/T;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/T;->a(I)V

    .line 513
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    new-instance v1, Lcom/dropbox/android/activity/at;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/activity/at;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;I)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->post(Ljava/lang/Runnable;)Z

    .line 519
    return-void
.end method

.method private b(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 202
    invoke-virtual {p0, v2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Z)V

    .line 203
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 204
    const-string v1, "network_refresh"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/x;

    move-result-object v1

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/x;->b(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;

    .line 208
    return-void
.end method

.method private j()Z
    .registers 5

    .prologue
    .line 456
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/e/a;

    .line 457
    invoke-virtual {v0}, Ldbxyzptlk/e/a;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3b

    .line 458
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 459
    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 460
    const-string v1, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 461
    instance-of v3, v1, Landroid/net/Uri;

    if-eqz v3, :cond_3b

    .line 462
    check-cast v1, Landroid/net/Uri;

    .line 463
    invoke-virtual {v0}, Ldbxyzptlk/e/a;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/net/Uri;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 464
    const-string v0, "EXTRA_FILE_SCROLL_TO"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 465
    const/4 v0, 0x1

    .line 471
    :goto_3a
    return v0

    :cond_3b
    const/4 v0, 0x0

    goto :goto_3a
.end method

.method private k()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 531
    iput v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->o:I

    .line 532
    iput v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->p:I

    .line 533
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Ldbxyzptlk/a/d;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 357
    const-string v0, "network_refresh"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 358
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v1

    .line 359
    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a:Lcom/dropbox/android/activity/delegate/k;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-eqz v0, :cond_4b

    move-object v0, p0

    :goto_14
    invoke-virtual {v2, v3, v0, v1}, Lcom/dropbox/android/activity/delegate/k;->a(Landroid/content/Context;Lcom/dropbox/android/activity/K;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 361
    new-instance v0, Lcom/dropbox/android/filemanager/F;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/dropbox/android/provider/K;->a:[Ljava/lang/String;

    const-string v7, "is_dir DESC, _display_name COLLATE NOCASE"

    move-object v6, v5

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/filemanager/F;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-class v1, Lcom/dropbox/android/filemanager/F;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "creating in SimpleDropboxBrowser: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ldbxyzptlk/a/d;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    return-object v0

    :cond_4b
    move-object v0, v5

    .line 359
    goto :goto_14
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 241
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 242
    return-void
.end method

.method public final a(ILandroid/net/Uri;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 523
    iput p1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:I

    .line 524
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b:Lcom/dropbox/android/util/bg;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/dropbox/android/util/bg;->a(Lcom/dropbox/android/activity/ax;ILandroid/net/Uri;Ljava/lang/String;)V

    .line 525
    return-void
.end method

.method public final a(Landroid/net/Uri;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 78
    invoke-static {p1}, Lcom/dropbox/android/util/DropboxPath;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 81
    sget-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid uri passed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "invalid uri passed"

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 82
    sget-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calling activity w/ invalid uri was: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_49
    :goto_49
    return-void

    .line 88
    :cond_4a
    new-instance v0, Lcom/dropbox/android/util/aq;

    invoke-direct {v0}, Lcom/dropbox/android/util/aq;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    new-instance v1, Lcom/dropbox/android/activity/av;

    invoke-direct {v1, p1}, Lcom/dropbox/android/activity/av;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;

    .line 92
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 93
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g()V

    goto :goto_49
.end method

.method protected final a(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 639
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    if-nez v0, :cond_18

    .line 641
    if-eqz p1, :cond_19

    const-string v0, "key_history_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 642
    const-string v0, "key_history_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/aq;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    .line 649
    :cond_18
    :goto_18
    return-void

    .line 645
    :cond_19
    new-instance v0, Lcom/dropbox/android/util/aq;

    invoke-direct {v0}, Lcom/dropbox/android/util/aq;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    .line 646
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    new-instance v1, Lcom/dropbox/android/activity/av;

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    const-string v3, "/"

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/activity/av;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;

    goto :goto_18
.end method

.method protected a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 5
    .parameter

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2}, Lcom/dropbox/android/util/bd;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/net/Uri;Z)V

    .line 271
    return-void
.end method

.method public final a(Ldbxyzptlk/a/d;)V
    .registers 4
    .parameter

    .prologue
    .line 433
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 434
    return-void
.end method

.method public final a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v4, -0x3e7

    .line 378
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    .line 382
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 387
    invoke-static {p2}, Lcom/dropbox/android/widget/T;->d(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_57

    const/4 v0, 0x1

    .line 388
    :goto_14
    iget-object v3, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->h:Landroid/view/View;

    if-eqz v0, :cond_59

    move v2, v1

    :goto_19
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 391
    if-eqz v0, :cond_2f

    iget v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c:I

    const/16 v2, 0xff

    if-ne v0, v2, :cond_2f

    .line 392
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d(Ljava/lang/String;)V

    .line 395
    :cond_2f
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Ljava/lang/String;)V

    .line 404
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v0

    .line 405
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j()Z

    move-result v2

    if-eqz v2, :cond_44

    .line 406
    iput v4, v0, Lcom/dropbox/android/util/ap;->b:I

    .line 409
    :cond_44
    iget v2, v0, Lcom/dropbox/android/util/ap;->b:I

    if-ltz v2, :cond_5c

    .line 411
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    iget v2, v0, Lcom/dropbox/android/util/ap;->b:I

    iget v3, v0, Lcom/dropbox/android/util/ap;->c:I

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/widget/DropboxItemListView;->setListViewScrollState(II)V

    .line 417
    iput v4, v0, Lcom/dropbox/android/util/ap;->b:I

    .line 425
    :cond_53
    :goto_53
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k()V

    .line 426
    return-void

    :cond_57
    move v0, v1

    .line 387
    goto :goto_14

    .line 388
    :cond_59
    const/16 v2, 0x8

    goto :goto_19

    .line 418
    :cond_5c
    iget v2, v0, Lcom/dropbox/android/util/ap;->b:I

    if-eq v2, v4, :cond_53

    .line 421
    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v2, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setSelection(I)V

    .line 422
    iput v4, v0, Lcom/dropbox/android/util/ap;->b:I

    goto :goto_53
.end method

.method public final bridge synthetic a(Ldbxyzptlk/a/d;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 49
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V

    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 219
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    return-void
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 216
    return-void
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public final b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    return-void
.end method

.method public abstract c()Lcom/dropbox/android/widget/X;
.end method

.method public final c(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k()V

    .line 254
    sget-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Browsing directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/widget/DropboxItemListView;)V

    .line 258
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 262
    :cond_3d
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    .line 264
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    .line 265
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    new-instance v2, Lcom/dropbox/android/activity/av;

    invoke-direct {v2, v0}, Lcom/dropbox/android/activity/av;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;

    .line 266
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g()V

    .line 267
    return-void
.end method

.method public final c_()V
    .registers 2

    .prologue
    .line 439
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l:Ldbxyzptlk/a/d;

    if-eqz v0, :cond_9

    .line 442
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l:Ldbxyzptlk/a/d;

    invoke-virtual {v0}, Ldbxyzptlk/a/d;->p()V

    .line 444
    :cond_9
    return-void
.end method

.method protected abstract d()I
.end method

.method public final d(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b(Ljava/lang/String;)V

    .line 333
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(I)V

    .line 334
    return-void
.end method

.method public final d_()V
    .registers 2

    .prologue
    .line 449
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l:Ldbxyzptlk/a/d;

    if-eqz v0, :cond_9

    .line 451
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->l:Ldbxyzptlk/a/d;

    invoke-virtual {v0}, Ldbxyzptlk/a/d;->n()V

    .line 453
    :cond_9
    return-void
.end method

.method protected abstract e()I
.end method

.method public final e(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 338
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b(Ljava/lang/String;)V

    .line 339
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(I)V

    .line 340
    return-void
.end method

.method public final f()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 185
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;
    :try_end_8
    .catch Ljava/util/EmptyStackException; {:try_start_0 .. :try_end_8} :catch_9

    .line 187
    :goto_8
    return-object v0

    .line 186
    :catch_9
    move-exception v0

    .line 187
    sget-object v0, Lcom/dropbox/android/d;->b:Landroid/net/Uri;

    goto :goto_8
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 198
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b(Z)V

    .line 199
    return-void
.end method

.method protected h()V
    .registers 4

    .prologue
    .line 305
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 306
    sget-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Ljava/lang/String;

    const-string v1, "Tried to browseParent with an empty history stack."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :goto_f
    return-void

    .line 310
    :cond_10
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 312
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 313
    sget-object v0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->j:Ljava/lang/String;

    const-string v1, "Tried to browse parent from root."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 317
    :cond_27
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    .line 318
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v1}, Lcom/dropbox/android/util/aq;->d()I

    move-result v1

    .line 320
    const/4 v2, 0x2

    if-lt v1, v2, :cond_51

    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v2, v1}, Lcom/dropbox/android/util/aq;->a(I)Lcom/dropbox/android/util/ap;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_51

    .line 321
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->c()Lcom/dropbox/android/util/ap;

    .line 327
    :goto_4d
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g()V

    goto :goto_f

    .line 323
    :cond_51
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    iget-object v2, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/widget/DropboxItemListView;)V

    .line 324
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    new-instance v2, Lcom/dropbox/android/activity/av;

    invoke-direct {v2, v0}, Lcom/dropbox/android/activity/av;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;

    goto :goto_4d
.end method

.method public final i()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 343
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 350
    :goto_d
    return v0

    .line 345
    :cond_e
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v1}, Lcom/dropbox/android/util/aq;->d()I

    move-result v1

    if-le v1, v0, :cond_1f

    .line 346
    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {v1}, Lcom/dropbox/android/util/aq;->c()Lcom/dropbox/android/util/ap;

    .line 347
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g()V

    goto :goto_d

    .line 350
    :cond_1f
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 124
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 127
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 128
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_HIDE_QUICKACTIONS"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    move v1, v0

    .line 131
    :goto_15
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    if-nez v1, :cond_4a

    const/4 v1, 0x1

    :goto_1a
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c()Lcom/dropbox/android/widget/X;

    move-result-object v5

    move-object v2, p0

    move-object v3, p0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/DropboxItemListView;->a(ZLandroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/c;Lcom/dropbox/android/widget/quickactions/f;Lcom/dropbox/android/widget/X;)V

    .line 132
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->n:Lcom/dropbox/android/widget/S;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setItemClickListener(Lcom/dropbox/android/widget/S;)V

    .line 133
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    new-instance v1, Lcom/dropbox/android/activity/aw;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/dropbox/android/activity/aw;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Lcom/dropbox/android/activity/as;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->b()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 136
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g()V

    .line 138
    return-void

    :cond_4a
    move v1, v2

    .line 131
    goto :goto_1a

    :cond_4c
    move v1, v2

    goto :goto_15
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    .line 179
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 180
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 102
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Landroid/os/Bundle;)V

    .line 103
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->g()Lcom/dropbox/android/taskqueue/i;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->k:Lcom/dropbox/android/taskqueue/p;

    .line 105
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 113
    const v0, 0x7f06006f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/DropboxItemListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    .line 114
    const v0, 0x7f060077

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->m:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f06007b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->f:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f06007a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->g:Landroid/view/View;

    .line 117
    const v0, 0x7f060072

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->h:Landroid/view/View;

    .line 118
    const v0, 0x7f060079

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->i:Landroid/view/View;

    .line 119
    return-object v1
.end method

.method public onDetach()V
    .registers 3

    .prologue
    .line 142
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 143
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    if-eqz v0, :cond_17

    invoke-virtual {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 144
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 146
    :cond_17
    return-void
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 152
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onPause()V

    .line 154
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DropboxItemListView;->a()Z

    .line 162
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setInForeground(Z)V

    .line 165
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->g()Lcom/dropbox/android/taskqueue/i;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/p;->c()V

    .line 169
    return-void
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 246
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onResume()V

    .line 247
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DropboxItemListView;->setInForeground(Z)V

    .line 248
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 616
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 619
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    if-eqz v0, :cond_e

    .line 620
    iget-object v0, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->e:Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/widget/DropboxItemListView;)V

    .line 622
    :cond_e
    const-string v0, "key_history_stack"

    iget-object v1, p0, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->d:Lcom/dropbox/android/util/aq;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 623
    return-void
.end method
