.class public Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    return-void
.end method

.method public static a()Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;
    .registers 1

    .prologue
    .line 18
    new-instance v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;-><init>()V

    return-object v0
.end method

.method public static b(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 47
    sget-object v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_12

    .line 49
    invoke-virtual {p0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v1

    .line 50
    invoke-virtual {v1, v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 51
    invoke-virtual {v1}, Landroid/support/v4/app/u;->c()I

    .line 53
    :cond_12
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 34
    sget-object v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/a;->a(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    sget-object v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 23
    sget-object v0, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 25
    return-void
.end method
