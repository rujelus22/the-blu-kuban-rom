.class public Lcom/dropbox/android/activity/GalleryPickerFragment;
.super Lcom/dropbox/android/activity/lock/SweetListFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/y;


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field protected final a:Ljava/util/HashSet;

.field b:I

.field c:I

.field private j:Lcom/dropbox/android/activity/bl;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/TextView;

.field private m:Z

.field private n:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 43
    const-class v0, Lcom/dropbox/android/activity/GalleryPickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/GalleryPickerFragment;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    .line 183
    iput v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->b:I

    .line 184
    iput v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->c:I

    .line 203
    iput-boolean v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->m:Z

    .line 204
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->n:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/GalleryPickerFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->d()V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/GalleryPickerFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->n:Landroid/os/Handler;

    return-object v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 145
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Landroid/widget/Button;

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_33

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 147
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 149
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-static {v1, v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;I)Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    return-void

    .line 145
    :cond_33
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private d()V
    .registers 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->j:Lcom/dropbox/android/activity/bl;

    if-eqz v0, :cond_9

    .line 155
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->j:Lcom/dropbox/android/activity/bl;

    invoke-interface {v0}, Lcom/dropbox/android/activity/bl;->c()V

    .line 157
    :cond_9
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Ldbxyzptlk/a/d;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 164
    sget-object v2, Lcom/dropbox/android/provider/ZipperedMediaProvider;->b:Landroid/net/Uri;

    .line 166
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_24

    move v5, v6

    .line 172
    :goto_f
    new-instance v0, Ldbxyzptlk/a/c;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    if-ne v5, v6, :cond_27

    const-string v5, "mini_thumb_path"

    :goto_1c
    aput-object v5, v3, v7

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/a/c;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 166
    :cond_24
    const/4 v0, 0x3

    move v5, v0

    goto :goto_f

    .line 172
    :cond_27
    const-string v5, "micro_thumb_path"

    goto :goto_1c
.end method

.method protected final a()V
    .registers 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->j:Lcom/dropbox/android/activity/bl;

    if-eqz v0, :cond_b

    .line 122
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->j:Lcom/dropbox/android/activity/bl;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/bl;->a(Ljava/util/Set;)V

    .line 124
    :cond_b
    return-void
.end method

.method protected final a(Landroid/database/Cursor;)V
    .registers 7
    .parameter

    .prologue
    .line 209
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 210
    :cond_5
    :goto_5
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 211
    const-string v1, "_cursor_type_tag"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_tag_video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 212
    const-string v1, "vid_duration"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    .line 213
    const-string v1, "_data"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 217
    :cond_3b
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 219
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_67

    iget-boolean v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->m:Z

    if-nez v1, :cond_67

    .line 220
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->m:Z

    .line 221
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-lt v1, v2, :cond_67

    .line 226
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Lcom/dropbox/android/activity/ap;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/ap;-><init>(Lcom/dropbox/android/activity/GalleryPickerFragment;)V

    invoke-static {v1, v0, v2, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 243
    :cond_67
    return-void
.end method

.method public final a(Lcom/dropbox/android/activity/bl;)V
    .registers 2
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->j:Lcom/dropbox/android/activity/bl;

    .line 56
    return-void
.end method

.method public final a(Ldbxyzptlk/a/d;)V
    .registers 4
    .parameter

    .prologue
    .line 250
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/Y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/Y;->a(Landroid/database/Cursor;)V

    .line 251
    return-void
.end method

.method public final a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 188
    invoke-virtual {p0, p2}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Landroid/database/Cursor;)V

    .line 192
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/Y;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/Y;->a(Landroid/database/Cursor;)V

    .line 196
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Lcom/dropbox/android/util/aM;

    if-eqz v0, :cond_22

    .line 198
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aM;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/SweetListView;->setDelayedPositionFromTop(I)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->f:Lcom/dropbox/android/util/aM;

    .line 201
    :cond_22
    return-void
.end method

.method public final bridge synthetic a(Ldbxyzptlk/a/d;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 61
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 63
    new-instance v0, Lcom/dropbox/android/widget/Y;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/widget/Y;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/x;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Landroid/support/v4/app/x;->a(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setSweetAdapter(Lcom/dropbox/android/widget/az;)V

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->g:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f060030

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 74
    const v1, 0x7f0b0018

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 75
    new-instance v1, Lcom/dropbox/android/activity/an;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/an;-><init>(Lcom/dropbox/android/activity/GalleryPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f060031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Landroid/widget/Button;

    .line 85
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 87
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0600de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Landroid/widget/Button;

    const v1, 0x7f0b00d0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 91
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->k:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/ao;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/ao;-><init>(Lcom/dropbox/android/activity/GalleryPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->c()V

    .line 99
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-virtual {p0, p3}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Landroid/os/Bundle;)V

    .line 114
    const v0, 0x7f03002e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 115
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    .line 116
    return-object v1
.end method

.method public onDetach()V
    .registers 3

    .prologue
    .line 103
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onDetach()V

    .line 104
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/Y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/Y;->a(Landroid/database/Cursor;)V

    .line 107
    :cond_19
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/Y;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/Y;->c()Landroid/database/Cursor;

    move-result-object v0

    .line 130
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 132
    const-string v1, "content_uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 135
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 140
    :goto_26
    iget-object v0, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/Y;

    invoke-virtual {v0, p3, p2}, Lcom/dropbox/android/widget/Y;->a(ILandroid/view/View;)V

    .line 141
    invoke-direct {p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->c()V

    .line 142
    return-void

    .line 137
    :cond_31
    iget-object v1, p0, Lcom/dropbox/android/activity/GalleryPickerFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_26
.end method
