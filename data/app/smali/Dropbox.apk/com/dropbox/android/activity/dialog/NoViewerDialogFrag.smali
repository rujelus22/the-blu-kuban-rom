.class public Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBaseWCallback;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;
    .registers 4
    .parameter

    .prologue
    .line 28
    new-instance v0, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;-><init>()V

    .line 29
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 30
    const-string v2, "filename"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 32
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    sget-object v0, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 37
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "filename"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0b00b1

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 44
    const v0, 0x7f0b00b2

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 45
    const v0, 0x7f0b0019

    new-instance v2, Lcom/dropbox/android/activity/dialog/h;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dialog/h;-><init>(Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 49
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 61
    sget-object v0, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 55
    sget-object v0, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 57
    return-void
.end method
