.class final enum Lcom/dropbox/android/activity/s;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/s;

.field public static final enum b:Lcom/dropbox/android/activity/s;

.field public static final enum c:Lcom/dropbox/android/activity/s;

.field public static final enum d:Lcom/dropbox/android/activity/s;

.field private static final synthetic e:[Lcom/dropbox/android/activity/s;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/dropbox/android/activity/s;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/activity/s;

    new-instance v0, Lcom/dropbox/android/activity/s;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/s;

    new-instance v0, Lcom/dropbox/android/activity/s;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/s;->c:Lcom/dropbox/android/activity/s;

    new-instance v0, Lcom/dropbox/android/activity/s;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/s;->d:Lcom/dropbox/android/activity/s;

    .line 38
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/activity/s;

    sget-object v1, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/activity/s;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/s;->b:Lcom/dropbox/android/activity/s;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/s;->c:Lcom/dropbox/android/activity/s;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/s;->d:Lcom/dropbox/android/activity/s;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/activity/s;->e:[Lcom/dropbox/android/activity/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/s;
    .registers 2
    .parameter

    .prologue
    .line 38
    const-class v0, Lcom/dropbox/android/activity/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/s;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/s;
    .registers 1

    .prologue
    .line 38
    sget-object v0, Lcom/dropbox/android/activity/s;->e:[Lcom/dropbox/android/activity/s;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/s;

    return-object v0
.end method
