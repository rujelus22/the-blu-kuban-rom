.class final Lcom/dropbox/android/activity/j;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/R;


# instance fields
.field final a:I

.field final b:I

.field final synthetic c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

.field private final d:Ljava/util/Random;

.field private final e:Lcom/dropbox/android/filemanager/I;

.field private final f:Ljava/util/ArrayList;

.field private final g:Ljava/util/Map;

.field private h:I

.field private i:Z

.field private j:Lcom/dropbox/android/taskqueue/Q;

.field private k:[F

.field private l:[I

.field private final m:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;Landroid/os/Bundle;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 562
    iput-object p1, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 516
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->d:Ljava/util/Random;

    .line 517
    invoke-static {}, Lcom/dropbox/android/filemanager/I;->a()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->e:Lcom/dropbox/android/filemanager/I;

    .line 518
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    .line 519
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->g:Ljava/util/Map;

    .line 520
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/activity/j;->h:I

    .line 521
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/j;->i:Z

    .line 525
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->m:Landroid/os/Handler;

    .line 527
    const/4 v0, 0x7

    iput v0, p0, Lcom/dropbox/android/activity/j;->a:I

    .line 528
    const/high16 v0, 0x4100

    iget-object v1, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/dropbox/android/activity/j;->b:I

    .line 563
    if-eqz p2, :cond_68

    const-string v0, "saved_rotations"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 564
    const-string v0, "saved_rotations"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->k:[F

    .line 572
    :cond_55
    :goto_55
    if-eqz p2, :cond_7f

    const-string v0, "saved_padding_offsets"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 573
    const-string v0, "saved_padding_offsets"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->l:[I

    .line 580
    :cond_67
    :goto_67
    return-void

    .line 566
    :cond_68
    invoke-virtual {p1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 567
    if-eqz v0, :cond_55

    const-string v1, "saved_rotations"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_55

    .line 568
    const-string v1, "saved_rotations"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->k:[F

    goto :goto_55

    .line 575
    :cond_7f
    invoke-virtual {p1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 576
    if-eqz v0, :cond_67

    const-string v1, "saved_padding_offsets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_67

    .line 577
    const-string v1, "saved_padding_offsets"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/j;->l:[I

    goto :goto_67
.end method

.method static synthetic a(Lcom/dropbox/android/activity/j;I)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 514
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/j;->b(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/j;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 514
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->g:Ljava/util/Map;

    return-object v0
.end method

.method private a()V
    .registers 4

    .prologue
    .line 691
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_33

    .line 693
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 695
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f80

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 696
    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 697
    new-instance v1, Lcom/dropbox/android/activity/l;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/l;-><init>(Lcom/dropbox/android/activity/j;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 717
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 719
    :cond_33
    return-void
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 869
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/activity/j;->a(ZI)V

    .line 870
    return-void
.end method

.method private a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 812
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 813
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    .line 814
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 816
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 817
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/j;->a(Z)V

    .line 820
    :cond_27
    invoke-direct {p0}, Lcom/dropbox/android/activity/j;->b()V

    .line 821
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/j;II)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 514
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/j;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/j;Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 514
    invoke-direct/range {p0 .. p7}, Lcom/dropbox/android/activity/j;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/j;Ljava/lang/String;Lcom/dropbox/android/filemanager/T;IZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 514
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/dropbox/android/activity/j;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/T;IZ)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 778
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 779
    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/j;->b(I)I

    move-result v2

    .line 781
    iget-object v3, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v3}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 782
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_1f

    .line 783
    invoke-direct {p0, v1, v2}, Lcom/dropbox/android/activity/j;->a(II)V

    .line 809
    :goto_1e
    return-void

    .line 785
    :cond_1f
    if-nez v1, :cond_32

    iget-object v1, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v0, :cond_32

    :goto_29
    new-instance v1, Lcom/dropbox/android/activity/n;

    invoke-direct {v1, p0, v3, p1}, Lcom/dropbox/android/activity/n;-><init>(Lcom/dropbox/android/activity/j;Landroid/view/View;Ljava/lang/String;)V

    invoke-static {v3, v0, v1}, Lcom/dropbox/android/widget/e;->a(Landroid/view/View;ZLandroid/view/animation/Animation$AnimationListener;)V

    goto :goto_1e

    :cond_32
    const/4 v0, 0x0

    goto :goto_29
.end method

.method private a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 740
    invoke-direct {p0}, Lcom/dropbox/android/activity/j;->a()V

    .line 742
    iget-boolean v0, p0, Lcom/dropbox/android/activity/j;->i:Z

    if-nez v0, :cond_24

    const/4 v5, 0x1

    .line 743
    :goto_9
    iput-boolean v6, p0, Lcom/dropbox/android/activity/j;->i:Z

    .line 745
    if-eqz p7, :cond_26

    .line 746
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, p6}, Lcom/dropbox/android/activity/j;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v1, p2

    move v2, p3

    move-wide v3, p4

    .line 747
    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/widget/e;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;ZJZ)V

    .line 774
    :goto_21
    iput-boolean v6, p0, Lcom/dropbox/android/activity/j;->i:Z

    .line 775
    return-void

    :cond_24
    move v5, v6

    .line 742
    goto :goto_9

    .line 749
    :cond_26
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v2}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v2

    const/4 v3, 0x7

    iget v4, p0, Lcom/dropbox/android/activity/j;->b:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dropbox/android/widget/e;->a(Landroid/view/LayoutInflater;Landroid/content/Context;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 754
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->k:[F

    if-eqz v1, :cond_7c

    .line 755
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->k:[F

    aget v1, v1, p6

    .line 760
    :goto_4b
    iget-object v2, p0, Lcom/dropbox/android/activity/j;->l:[I

    if-eqz v2, :cond_92

    .line 761
    iget-object v2, p0, Lcom/dropbox/android/activity/j;->l:[I

    mul-int/lit8 v3, p6, 0x2

    aget v2, v2, v3

    .line 762
    iget-object v3, p0, Lcom/dropbox/android/activity/j;->l:[I

    mul-int/lit8 v4, p6, 0x2

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    .line 767
    :goto_5d
    invoke-static {v0, v1}, Lcom/dropbox/android/widget/e;->a(Landroid/view/View;F)V

    .line 768
    invoke-static {v0, v2, v3}, Lcom/dropbox/android/widget/e;->a(Landroid/view/View;II)V

    move-object v1, p2

    move v2, p3

    move-wide v3, p4

    .line 769
    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/widget/e;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;ZJZ)V

    .line 771
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p6, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 772
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-direct {p0, p6}, Lcom/dropbox/android/activity/j;->b(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    goto :goto_21

    .line 757
    :cond_7c
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_86

    const/4 v1, 0x0

    goto :goto_4b

    :cond_86
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->d:Ljava/util/Random;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x7

    int-to-float v1, v1

    goto :goto_4b

    .line 764
    :cond_92
    iget-object v2, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_ab

    iget v2, p0, Lcom/dropbox/android/activity/j;->b:I

    div-int/lit8 v2, v2, 0x2

    .line 765
    :goto_9e
    iget-object v3, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_b9

    iget v3, p0, Lcom/dropbox/android/activity/j;->b:I

    div-int/lit8 v3, v3, 0x2

    goto :goto_5d

    .line 764
    :cond_ab
    iget v2, p0, Lcom/dropbox/android/activity/j;->b:I

    iget-object v3, p0, Lcom/dropbox/android/activity/j;->d:Ljava/util/Random;

    iget v4, p0, Lcom/dropbox/android/activity/j;->b:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_9e

    .line 765
    :cond_b9
    iget v3, p0, Lcom/dropbox/android/activity/j;->b:I

    iget-object v4, p0, Lcom/dropbox/android/activity/j;->d:Ljava/util/Random;

    iget v7, p0, Lcom/dropbox/android/activity/j;->b:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v4, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    sub-int/2addr v3, v4

    goto :goto_5d
.end method

.method private a(Ljava/lang/String;Lcom/dropbox/android/filemanager/T;IZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 722
    if-eqz p2, :cond_1f

    .line 723
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p2, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-boolean v3, p2, Lcom/dropbox/android/filemanager/T;->b:Z

    iget-wide v4, p2, Lcom/dropbox/android/filemanager/T;->c:J

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/activity/j;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    .line 737
    :goto_1e
    return-void

    .line 730
    :cond_1f
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/dropbox/android/activity/j;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    goto :goto_1e
.end method

.method private a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 674
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->h()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 675
    const v0, 0x7f020058

    .line 679
    :goto_d
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 680
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 681
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 683
    if-eqz p1, :cond_41

    .line 684
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f80

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 685
    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 686
    iget-object v1, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v1}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->d(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 688
    :cond_41
    return-void

    .line 677
    :cond_42
    const v0, 0x7f0200ef

    goto :goto_d
.end method

.method private a(ZI)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 852
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz p1, :cond_3c

    move v0, v1

    :goto_a
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 853
    if-eqz p1, :cond_18

    .line 854
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->e(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 856
    :cond_18
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 857
    :goto_20
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_3f

    .line 858
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/e;->a(Landroid/view/View;Z)V

    .line 857
    add-int/lit8 v1, v1, 0x1

    goto :goto_20

    .line 852
    :cond_3c
    const/16 v0, 0x8

    goto :goto_a

    .line 862
    :cond_3f
    return-void
.end method

.method private b(I)I
    .registers 3
    .parameter

    .prologue
    .line 873
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/j;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 514
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const v2, 0x7f0200e2

    .line 824
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 825
    invoke-direct {p0}, Lcom/dropbox/android/activity/j;->c()V

    .line 849
    :goto_e
    return-void

    .line 829
    :cond_f
    sget-object v0, Lcom/dropbox/android/activity/b;->a:[I

    iget-object v1, p0, Lcom/dropbox/android/activity/j;->j:Lcom/dropbox/android/taskqueue/Q;

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/Q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3e

    .line 846
    :pswitch_1c
    invoke-direct {p0}, Lcom/dropbox/android/activity/j;->c()V

    goto :goto_e

    .line 831
    :pswitch_20
    const v0, 0x7f020160

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/j;->a(I)V

    goto :goto_e

    .line 834
    :pswitch_27
    const v0, 0x7f020196

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/j;->a(I)V

    goto :goto_e

    .line 837
    :pswitch_2e
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/j;->a(I)V

    goto :goto_e

    .line 840
    :pswitch_32
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/j;->a(I)V

    goto :goto_e

    .line 843
    :pswitch_36
    const v0, 0x7f02005d

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/j;->a(I)V

    goto :goto_e

    .line 829
    nop

    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_2e
        :pswitch_27
        :pswitch_32
        :pswitch_36
        :pswitch_1c
        :pswitch_1c
        :pswitch_20
    .end packed-switch
.end method

.method private c()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 865
    invoke-direct {p0, v0, v0}, Lcom/dropbox/android/activity/j;->a(ZI)V

    .line 866
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/activity/j;)V
    .registers 1
    .parameter

    .prologue
    .line 514
    invoke-direct {p0}, Lcom/dropbox/android/activity/j;->b()V

    return-void
.end method


# virtual methods
.method public final a(ILcom/dropbox/android/filemanager/T;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 532
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->m:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/activity/k;

    invoke-direct {v1, p0, p2, p1}, Lcom/dropbox/android/activity/k;-><init>(Lcom/dropbox/android/activity/j;Lcom/dropbox/android/filemanager/T;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 560
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x5

    const/4 v2, 0x0

    .line 583
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_52

    .line 588
    new-array v3, v7, [F

    .line 589
    const/16 v0, 0xa

    new-array v4, v0, [I

    move v1, v2

    .line 590
    :goto_11
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_48

    if-ge v1, v7, :cond_48

    .line 591
    iget-object v0, p0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-static {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->c(Lcom/dropbox/android/activity/CameraUploadDetailsFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/dropbox/android/activity/j;->b(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/RotatableFrameLayout;

    .line 592
    invoke-virtual {v0}, Lcom/dropbox/android/widget/RotatableFrameLayout;->a()F

    move-result v5

    aput v5, v3, v1

    .line 593
    invoke-static {v0}, Lcom/dropbox/android/widget/e;->a(Landroid/view/View;)[I

    move-result-object v0

    .line 594
    mul-int/lit8 v5, v1, 0x2

    aget v6, v0, v2

    aput v6, v4, v5

    .line 595
    mul-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x1

    aget v0, v0, v6

    aput v0, v4, v5

    .line 590
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    .line 597
    :cond_48
    const-string v0, "saved_rotations"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 598
    const-string v0, "saved_padding_offsets"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 600
    :cond_52
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONArray;Lcom/dropbox/android/taskqueue/Q;)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 603
    if-nez p1, :cond_13

    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_13

    .line 604
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not expecting upcoming camera uploads when there is no current one."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 607
    :cond_13
    iput-object p3, p0, Lcom/dropbox/android/activity/j;->j:Lcom/dropbox/android/taskqueue/Q;

    .line 610
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 611
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 612
    if-eqz p1, :cond_24

    .line 613
    invoke-virtual {v3, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_24
    move v0, v2

    .line 616
    :goto_25
    :try_start_25
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_4b

    .line 617
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->size()I

    move-result v1

    const/4 v5, 0x5

    if-ge v1, v5, :cond_3c

    .line 618
    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 616
    :goto_39
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 620
    :cond_3c
    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_43
    .catch Lorg/json/JSONException; {:try_start_25 .. :try_end_43} :catch_44

    goto :goto_39

    .line 623
    :catch_44
    move-exception v0

    .line 624
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 629
    :cond_4b
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_56
    :goto_56
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 630
    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_56

    .line 631
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/j;->a(Ljava/lang/String;)V

    goto :goto_56

    .line 637
    :cond_6c
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_71
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_af

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 640
    iget-object v6, p0, Lcom/dropbox/android/activity/j;->f:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 641
    if-ltz v6, :cond_89

    .line 642
    add-int/lit8 v0, v6, 0x1

    move v1, v0

    .line 643
    goto :goto_71

    .line 646
    :cond_89
    iget v6, p0, Lcom/dropbox/android/activity/j;->h:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/dropbox/android/activity/j;->h:I

    .line 647
    iget-object v6, p0, Lcom/dropbox/android/activity/j;->g:Ljava/util/Map;

    iget v7, p0, Lcom/dropbox/android/activity/j;->h:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 648
    iget-object v6, p0, Lcom/dropbox/android/activity/j;->e:Lcom/dropbox/android/filemanager/I;

    iget v7, p0, Lcom/dropbox/android/activity/j;->h:I

    invoke-virtual {v6, v0, v7, v9, p0}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;

    move-result-object v6

    .line 650
    if-eqz v6, :cond_ab

    .line 651
    invoke-direct {p0, v0, v6, v1, v2}, Lcom/dropbox/android/activity/j;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/T;IZ)V

    .line 655
    :goto_a7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 656
    goto :goto_71

    .line 653
    :cond_ab
    invoke-direct {p0, v0, v8, v1, v2}, Lcom/dropbox/android/activity/j;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/T;IZ)V

    goto :goto_a7

    .line 658
    :cond_af
    iget-boolean v0, p0, Lcom/dropbox/android/activity/j;->i:Z

    if-eqz v0, :cond_bc

    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    if-nez v0, :cond_bc

    .line 659
    invoke-direct {p0, v2}, Lcom/dropbox/android/activity/j;->a(Z)V

    .line 662
    :cond_bc
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 663
    iget-object v3, p0, Lcom/dropbox/android/activity/j;->e:Lcom/dropbox/android/filemanager/I;

    invoke-virtual {v3, v0, v9}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;I)V

    goto :goto_c0

    .line 666
    :cond_d2
    invoke-direct {p0}, Lcom/dropbox/android/activity/j;->b()V

    .line 667
    iput-boolean v2, p0, Lcom/dropbox/android/activity/j;->i:Z

    .line 668
    iput-object v8, p0, Lcom/dropbox/android/activity/j;->k:[F

    .line 669
    iput-object v8, p0, Lcom/dropbox/android/activity/j;->l:[I

    .line 670
    return-void
.end method
