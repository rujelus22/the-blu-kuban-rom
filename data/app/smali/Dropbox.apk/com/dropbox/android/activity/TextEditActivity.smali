.class public Lcom/dropbox/android/activity/TextEditActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/net/Uri;

.field private c:Landroid/widget/EditText;

.field private d:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private final j:Lcom/dropbox/android/util/g;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 60
    const-class v0, Lcom/dropbox/android/activity/TextEditActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 64
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->d:Z

    .line 65
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->f:Z

    .line 66
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    .line 67
    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    .line 69
    new-instance v0, Lcom/dropbox/android/util/g;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/g;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->j:Lcom/dropbox/android/util/g;

    .line 70
    iput-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    .line 73
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    .line 75
    const-string v0, "\r\n"

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    .line 748
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    return-object p1
.end method

.method public static a(Ljava/io/InputStream;)Lcom/dropbox/android/activity/bQ;
    .registers 18
    .parameter

    .prologue
    .line 518
    new-instance v8, Lcom/dropbox/android/activity/bQ;

    invoke-direct {v8}, Lcom/dropbox/android/activity/bQ;-><init>()V

    .line 521
    const/4 v6, 0x0

    .line 522
    const/4 v5, 0x0

    .line 524
    new-instance v9, Ldbxyzptlk/G/h;

    const/4 v1, 0x0

    invoke-direct {v9, v1}, Ldbxyzptlk/G/h;-><init>(I)V

    .line 527
    new-instance v1, Lcom/dropbox/android/activity/bH;

    invoke-direct {v1, v8}, Lcom/dropbox/android/activity/bH;-><init>(Lcom/dropbox/android/activity/bQ;)V

    invoke-virtual {v9, v1}, Ldbxyzptlk/G/h;->a(Ldbxyzptlk/G/q;)V

    .line 537
    const/4 v4, 0x1

    .line 538
    const/4 v3, 0x0

    .line 540
    const/4 v1, 0x1

    .line 541
    const/4 v2, 0x0

    .line 543
    const/16 v7, 0x400

    new-array v10, v7, [B

    .line 545
    :cond_1d
    :goto_1d
    const/4 v7, 0x0

    array-length v11, v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v7, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v11

    const/4 v7, -0x1

    if-eq v11, v7, :cond_8c

    if-eqz v3, :cond_2c

    if-nez v6, :cond_8c

    .line 547
    :cond_2c
    const/4 v7, 0x0

    :goto_2d
    if-nez v6, :cond_4f

    if-ge v7, v11, :cond_4f

    .line 548
    aget-byte v12, v10, v7

    packed-switch v12, :pswitch_data_c4

    .line 561
    :pswitch_36
    if-eqz v5, :cond_3d

    .line 562
    const-string v6, "\r"

    iput-object v6, v8, Lcom/dropbox/android/activity/bQ;->b:Ljava/lang/String;

    .line 563
    const/4 v6, 0x1

    .line 547
    :cond_3d
    :goto_3d
    add-int/lit8 v7, v7, 0x1

    goto :goto_2d

    .line 550
    :pswitch_40
    if-eqz v5, :cond_48

    .line 551
    const-string v6, "\r\n"

    iput-object v6, v8, Lcom/dropbox/android/activity/bQ;->b:Ljava/lang/String;

    .line 555
    :goto_46
    const/4 v6, 0x1

    .line 556
    goto :goto_3d

    .line 553
    :cond_48
    const-string v6, "\n"

    iput-object v6, v8, Lcom/dropbox/android/activity/bQ;->b:Ljava/lang/String;

    goto :goto_46

    .line 558
    :pswitch_4d
    const/4 v5, 0x1

    .line 559
    goto :goto_3d

    .line 570
    :cond_4f
    if-eqz v1, :cond_78

    .line 572
    const/4 v1, 0x2

    new-array v12, v1, [Ldbxyzptlk/B/a;

    const/4 v1, 0x0

    sget-object v7, Ldbxyzptlk/B/a;->b:Ldbxyzptlk/B/a;

    aput-object v7, v12, v1

    const/4 v1, 0x1

    sget-object v7, Ldbxyzptlk/B/a;->c:Ldbxyzptlk/B/a;

    aput-object v7, v12, v1

    .line 573
    array-length v13, v12

    const/4 v1, 0x0

    move v7, v1

    :goto_61
    if-ge v7, v13, :cond_c1

    aget-object v1, v12, v7

    .line 574
    invoke-virtual {v1}, Ldbxyzptlk/B/a;->c()[B

    move-result-object v14

    .line 575
    array-length v15, v14

    if-lt v11, v15, :cond_88

    invoke-static {v10, v14}, Lcom/dropbox/android/activity/TextEditActivity;->a([B[B)Z

    move-result v14

    if-eqz v14, :cond_88

    .line 580
    :goto_72
    const/4 v2, 0x0

    move-object/from16 v16, v1

    move v1, v2

    move-object/from16 v2, v16

    .line 584
    :cond_78
    if-eqz v4, :cond_7e

    .line 585
    invoke-virtual {v9, v10, v11}, Ldbxyzptlk/G/h;->a([BI)Z

    move-result v4

    .line 589
    :cond_7e
    if-nez v4, :cond_1d

    if-nez v3, :cond_1d

    .line 590
    const/4 v3, 0x0

    invoke-virtual {v9, v10, v11, v3}, Ldbxyzptlk/G/h;->a([BIZ)Z

    move-result v3

    goto :goto_1d

    .line 573
    :cond_88
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_61

    .line 593
    :cond_8c
    invoke-virtual {v9}, Ldbxyzptlk/G/h;->b()V

    .line 598
    invoke-virtual {v9}, Ldbxyzptlk/G/h;->c()[Ljava/lang/String;

    move-result-object v3

    .line 599
    if-eqz v2, :cond_a7

    .line 600
    array-length v5, v3

    const/4 v1, 0x0

    :goto_97
    if-ge v1, v5, :cond_a7

    aget-object v6, v3, v1

    .line 601
    invoke-virtual {v2}, Ldbxyzptlk/B/a;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b9

    .line 602
    iput-object v6, v8, Lcom/dropbox/android/activity/bQ;->a:Ljava/lang/String;

    .line 608
    :cond_a7
    iget-object v1, v8, Lcom/dropbox/android/activity/bQ;->a:Ljava/lang/String;

    if-nez v1, :cond_b8

    .line 609
    if-eqz v4, :cond_bc

    .line 610
    sget-object v1, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    const-string v2, "CHARSET = ASCII"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    const-string v1, "us-ascii"

    iput-object v1, v8, Lcom/dropbox/android/activity/bQ;->a:Ljava/lang/String;

    .line 617
    :cond_b8
    :goto_b8
    return-object v8

    .line 600
    :cond_b9
    add-int/lit8 v1, v1, 0x1

    goto :goto_97

    .line 613
    :cond_bc
    const-string v1, "UTF-8"

    iput-object v1, v8, Lcom/dropbox/android/activity/bQ;->a:Ljava/lang/String;

    goto :goto_b8

    :cond_c1
    move-object v1, v2

    goto :goto_72

    .line 548
    nop

    :pswitch_data_c4
    .packed-switch 0xa
        :pswitch_40
        :pswitch_36
        :pswitch_36
        :pswitch_4d
    .end packed-switch
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/activity/bR;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 371
    :try_start_1
    const-string v1, "rw"

    .line 372
    new-instance v0, Lcom/dropbox/android/activity/bR;

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v2, v1, v3}, Lcom/dropbox/android/activity/bR;-><init>(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Z)V
    :try_end_d
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_d} :catch_e
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_d} :catch_1b

    .line 384
    :goto_d
    return-object v0

    .line 373
    :catch_e
    move-exception v0

    .line 376
    const-string v1, "r"

    .line 377
    new-instance v0, Lcom/dropbox/android/activity/bR;

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    invoke-direct {v0, v2, v1, v4}, Lcom/dropbox/android/activity/bR;-><init>(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Z)V

    goto :goto_d

    .line 378
    :catch_1b
    move-exception v0

    .line 383
    const-string v1, "r"

    .line 384
    new-instance v0, Lcom/dropbox/android/activity/bR;

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    invoke-direct {v0, v2, v1, v4}, Lcom/dropbox/android/activity/bR;-><init>(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Z)V

    goto :goto_d
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->e()V

    return-void
.end method

.method private a(Landroid/net/Uri;Z)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 623
    .line 627
    :try_start_3
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_da
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_6} :catch_100

    move-result-object v4

    .line 628
    if-nez p2, :cond_117

    .line 630
    :try_start_9
    const-string v2, "r"

    invoke-virtual {v4, p1, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_da
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_e} :catch_26
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_e} :catch_100

    move-result-object v2

    .line 633
    const v5, 0x7f0b0186

    const/4 v6, 0x1

    :try_start_13
    invoke-static {p0, v5, v6}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    .line 636
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_f0
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_1a} :catch_114
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_1a} :catch_b5

    .line 691
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/Writer;)V

    .line 692
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    .line 694
    if-eqz v2, :cond_25

    .line 695
    :try_start_22
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_25} :catch_e9

    .line 697
    :cond_25
    :goto_25
    return v0

    .line 638
    :catch_26
    move-exception v2

    move-object v2, v3

    .line 643
    :goto_28
    :try_start_28
    iget-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    if-nez v5, :cond_99

    .line 644
    const-string v5, "UTF-8"

    iput-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    .line 652
    :cond_30
    :goto_30
    const-string v5, "rwt"

    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_35
    .catchall {:try_start_28 .. :try_end_35} :catchall_f0
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_35} :catch_b5

    move-result-object v5

    .line 653
    :try_start_36
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->createOutputStream()Ljava/io/FileOutputStream;
    :try_end_39
    .catchall {:try_start_36 .. :try_end_39} :catchall_f4
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_39} :catch_104

    move-result-object v4

    .line 654
    :try_start_3a
    new-instance v2, Ljava/io/OutputStreamWriter;

    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    invoke-direct {v2, v4, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_41
    .catchall {:try_start_3a .. :try_end_41} :catchall_f7
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_41} :catch_108

    .line 656
    :try_start_41
    iget-object v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 657
    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5d

    .line 658
    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 660
    :cond_5d
    invoke-virtual {v2, v3}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 662
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_63
    .catchall {:try_start_41 .. :try_end_63} :catchall_f9
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_63} :catch_10c

    .line 665
    :try_start_63
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
    :try_end_6a
    .catchall {:try_start_63 .. :try_end_6a} :catchall_f9
    .catch Ljava/io/SyncFailedException; {:try_start_63 .. :try_end_6a} :catch_111
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_6a} :catch_10c

    .line 669
    :goto_6a
    :try_start_6a
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->close()V

    .line 670
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 672
    invoke-static {}, Lcom/dropbox/android/util/i;->o()Lcom/dropbox/android/util/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/s;->c()V

    .line 674
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->d:Z

    .line 675
    iget-boolean v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->f:Z

    if-eqz v3, :cond_81

    .line 676
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    .line 678
    :cond_81
    const v3, 0x7f0b017a

    const/4 v6, 0x1

    invoke-static {p0, v3, v6}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    .line 681
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_8c
    .catchall {:try_start_6a .. :try_end_8c} :catchall_f9
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_8c} :catch_10c

    .line 691
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/Writer;)V

    .line 692
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    .line 694
    if-eqz v5, :cond_97

    .line 695
    :try_start_94
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_97
    .catch Ljava/io/IOException; {:try_start_94 .. :try_end_97} :catch_ec

    :cond_97
    :goto_97
    move v0, v1

    .line 697
    goto :goto_25

    .line 646
    :cond_99
    :try_start_99
    iget-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 647
    const-string v6, "ascii"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_af

    const-string v6, "us-ascii"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_30

    .line 648
    :cond_af
    const-string v5, "UTF-8"

    iput-object v5, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;
    :try_end_b3
    .catchall {:try_start_99 .. :try_end_b3} :catchall_f0
    .catch Ljava/io/IOException; {:try_start_99 .. :try_end_b3} :catch_b5

    goto/16 :goto_30

    .line 683
    :catch_b5
    move-exception v1

    move-object v4, v2

    move-object v2, v3

    .line 684
    :goto_b8
    const v5, 0x7f0b0185

    const/4 v6, 0x1

    :try_start_bc
    invoke-static {p0, v5, v6}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    .line 687
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 688
    sget-object v5, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    const-string v6, "Problem saving file"

    invoke-static {v5, v6, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ca
    .catchall {:try_start_bc .. :try_end_ca} :catchall_fc

    .line 691
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/Writer;)V

    .line 692
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    .line 694
    if-eqz v4, :cond_25

    .line 695
    :try_start_d2
    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_d5
    .catch Ljava/io/IOException; {:try_start_d2 .. :try_end_d5} :catch_d7

    goto/16 :goto_25

    .line 697
    :catch_d7
    move-exception v1

    goto/16 :goto_25

    .line 691
    :catchall_da
    move-exception v0

    move-object v4, v3

    move-object v5, v3

    :goto_dd
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/Writer;)V

    .line 692
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    .line 694
    if-eqz v5, :cond_e8

    .line 695
    :try_start_e5
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_e8
    .catch Ljava/io/IOException; {:try_start_e5 .. :try_end_e8} :catch_ee

    .line 697
    :cond_e8
    :goto_e8
    throw v0

    :catch_e9
    move-exception v1

    goto/16 :goto_25

    :catch_ec
    move-exception v0

    goto :goto_97

    :catch_ee
    move-exception v1

    goto :goto_e8

    .line 691
    :catchall_f0
    move-exception v0

    move-object v4, v3

    move-object v5, v2

    goto :goto_dd

    :catchall_f4
    move-exception v0

    move-object v4, v3

    goto :goto_dd

    :catchall_f7
    move-exception v0

    goto :goto_dd

    :catchall_f9
    move-exception v0

    move-object v3, v2

    goto :goto_dd

    :catchall_fc
    move-exception v0

    move-object v5, v4

    move-object v4, v2

    goto :goto_dd

    .line 683
    :catch_100
    move-exception v1

    move-object v2, v3

    move-object v4, v3

    goto :goto_b8

    :catch_104
    move-exception v1

    move-object v2, v3

    move-object v4, v5

    goto :goto_b8

    :catch_108
    move-exception v1

    move-object v2, v4

    move-object v4, v5

    goto :goto_b8

    :catch_10c
    move-exception v1

    move-object v3, v2

    move-object v2, v4

    move-object v4, v5

    goto :goto_b8

    .line 666
    :catch_111
    move-exception v3

    goto/16 :goto_6a

    .line 638
    :catch_114
    move-exception v5

    goto/16 :goto_28

    :cond_117
    move-object v2, v3

    goto/16 :goto_28
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;Z)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/TextEditActivity;->a(Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TextEditActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    return p1
.end method

.method private static a([B[B)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 505
    array-length v0, p0

    array-length v2, p1

    if-lt v0, v2, :cond_f

    move v0, v1

    .line 506
    :goto_6
    array-length v2, p1

    if-ge v0, v2, :cond_13

    .line 507
    aget-byte v2, p0, v0

    aget-byte v3, p1, v0

    if-eq v2, v3, :cond_10

    .line 513
    :cond_f
    :goto_f
    return v1

    .line 506
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 511
    :cond_13
    const/4 v1, 0x1

    goto :goto_f
.end method

.method static synthetic b(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/net/Uri;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/TextEditActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/dropbox/android/activity/TextEditActivity;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/activity/TextEditActivity;)Lcom/dropbox/android/util/g;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->j:Lcom/dropbox/android/util/g;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/TextEditActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .registers 11

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 389
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 398
    :try_start_6
    iget-object v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v4, v2}, Lcom/dropbox/android/activity/TextEditActivity;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/activity/bR;
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_1db
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_b} :catch_243
    .catch Lcom/dropbox/android/activity/bP; {:try_start_6 .. :try_end_b} :catch_22c
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_b} :catch_151
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_b} :catch_196

    move-result-object v5

    .line 399
    :try_start_c
    iget-boolean v2, v5, Lcom/dropbox/android/activity/bR;->c:Z

    iput-boolean v2, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    .line 401
    iget-object v2, v5, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    .line 402
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1b
    .catchall {:try_start_c .. :try_end_1b} :catchall_1f2
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_1b} :catch_249
    .catch Lcom/dropbox/android/activity/bP; {:try_start_c .. :try_end_1b} :catch_232
    .catch Ljava/lang/SecurityException; {:try_start_c .. :try_end_1b} :catch_217
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_1b} :catch_207

    .line 403
    :try_start_1b
    new-instance v3, Ljava/io/BufferedInputStream;

    const/16 v6, 0x2000

    invoke-direct {v3, v2, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_22
    .catchall {:try_start_1b .. :try_end_22} :catchall_1f6
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_22} :catch_24f
    .catch Lcom/dropbox/android/activity/bP; {:try_start_1b .. :try_end_22} :catch_237
    .catch Ljava/lang/SecurityException; {:try_start_1b .. :try_end_22} :catch_21c
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_22} :catch_20b

    .line 406
    :try_start_22
    invoke-static {v3}, Lcom/dropbox/android/activity/TextEditActivity;->a(Ljava/io/InputStream;)Lcom/dropbox/android/activity/bQ;

    move-result-object v6

    .line 407
    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    if-nez v7, :cond_2e

    .line 408
    iget-object v7, v6, Lcom/dropbox/android/activity/bQ;->a:Ljava/lang/String;

    iput-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    .line 410
    :cond_2e
    iget-object v7, v6, Lcom/dropbox/android/activity/bQ;->b:Ljava/lang/String;

    if-eqz v7, :cond_36

    .line 411
    iget-object v6, v6, Lcom/dropbox/android/activity/bQ;->b:Ljava/lang/String;

    iput-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    .line 416
    :cond_36
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 417
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 418
    iget-object v6, v5, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 420
    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    iget-object v7, v5, Lcom/dropbox/android/activity/bR;->b:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 421
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    .line 422
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_52
    .catchall {:try_start_22 .. :try_end_52} :catchall_1fa
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_52} :catch_255
    .catch Lcom/dropbox/android/activity/bP; {:try_start_22 .. :try_end_52} :catch_23c
    .catch Ljava/lang/SecurityException; {:try_start_22 .. :try_end_52} :catch_221
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_52} :catch_20f

    .line 423
    :try_start_52
    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    invoke-direct {v2, v4, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_59
    .catchall {:try_start_52 .. :try_end_59} :catchall_1fd
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_59} :catch_25c
    .catch Lcom/dropbox/android/activity/bP; {:try_start_52 .. :try_end_59} :catch_240
    .catch Ljava/lang/SecurityException; {:try_start_52 .. :try_end_59} :catch_225
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_59} :catch_212

    .line 425
    const/16 v1, 0x400

    :try_start_5b
    new-array v1, v1, [C

    .line 426
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 428
    :goto_62
    invoke-virtual {v2, v1}, Ljava/io/Reader;->read([C)I

    move-result v7

    .line 429
    if-gez v7, :cond_b7

    .line 439
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 441
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7e

    .line 442
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    :cond_7e
    invoke-static {}, Lcom/dropbox/android/util/i;->n()Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v6, "mode"

    iget-object v7, v5, Lcom/dropbox/android/activity/bR;->b:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    .line 447
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 448
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    if-nez v0, :cond_a6

    .line 449
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->c:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v6, 0x0

    new-instance v7, Lcom/dropbox/android/activity/bG;

    invoke-direct {v7, p0}, Lcom/dropbox/android/activity/bG;-><init>(Lcom/dropbox/android/activity/TextEditActivity;)V

    aput-object v7, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V
    :try_end_a6
    .catchall {:try_start_5b .. :try_end_a6} :catchall_1ff
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_a6} :catch_c4
    .catch Lcom/dropbox/android/activity/bP; {:try_start_5b .. :try_end_a6} :catch_10e
    .catch Ljava/lang/SecurityException; {:try_start_5b .. :try_end_a6} :catch_228
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_a6} :catch_214

    .line 488
    :cond_a6
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    .line 489
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 490
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 491
    if-eqz v5, :cond_b6

    .line 493
    :try_start_b1
    iget-object v0, v5, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_b1 .. :try_end_b6} :catch_262

    .line 497
    :cond_b6
    :goto_b6
    return-void

    .line 432
    :cond_b7
    add-int/2addr v0, v7

    .line 433
    const v8, 0x3e800

    if-le v0, v8, :cond_108

    .line 434
    :try_start_bd
    new-instance v0, Lcom/dropbox/android/activity/bP;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/bP;-><init>(Lcom/dropbox/android/activity/bF;)V

    throw v0
    :try_end_c4
    .catchall {:try_start_bd .. :try_end_c4} :catchall_1ff
    .catch Ljava/io/IOException; {:try_start_bd .. :try_end_c4} :catch_c4
    .catch Lcom/dropbox/android/activity/bP; {:try_start_bd .. :try_end_c4} :catch_10e
    .catch Ljava/lang/SecurityException; {:try_start_bd .. :try_end_c4} :catch_228
    .catch Ljava/lang/Exception; {:try_start_bd .. :try_end_c4} :catch_214

    .line 459
    :catch_c4
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    .line 460
    :goto_c9
    const v5, 0x7f0b0183

    const/4 v6, 0x1

    :try_start_cd
    invoke-static {p0, v5, v6}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    .line 463
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 464
    sget-object v5, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Problem opening file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v7}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 465
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V
    :try_end_f5
    .catchall {:try_start_cd .. :try_end_f5} :catchall_202

    .line 488
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    .line 489
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 490
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 491
    if-eqz v4, :cond_b6

    .line 493
    :try_start_100
    iget-object v0, v4, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_105
    .catch Ljava/io/IOException; {:try_start_100 .. :try_end_105} :catch_106

    goto :goto_b6

    .line 494
    :catch_106
    move-exception v0

    goto :goto_b6

    .line 436
    :cond_108
    const/4 v8, 0x0

    :try_start_109
    invoke-virtual {v6, v1, v8, v7}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_10c
    .catchall {:try_start_109 .. :try_end_10c} :catchall_1ff
    .catch Ljava/io/IOException; {:try_start_109 .. :try_end_10c} :catch_c4
    .catch Lcom/dropbox/android/activity/bP; {:try_start_109 .. :try_end_10c} :catch_10e
    .catch Ljava/lang/SecurityException; {:try_start_109 .. :try_end_10c} :catch_228
    .catch Ljava/lang/Exception; {:try_start_109 .. :try_end_10c} :catch_214

    goto/16 :goto_62

    .line 466
    :catch_10e
    move-exception v0

    move-object v1, v2

    .line 467
    :goto_110
    const v0, 0x7f0b0184

    const/4 v2, 0x1

    :try_start_114
    invoke-static {p0, v0, v2}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 470
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 471
    sget-object v0, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Too large of a file: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v6}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V
    :try_end_13c
    .catchall {:try_start_114 .. :try_end_13c} :catchall_1fd

    .line 488
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    .line 489
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 490
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 491
    if-eqz v5, :cond_b6

    .line 493
    :try_start_147
    iget-object v0, v5, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_14c
    .catch Ljava/io/IOException; {:try_start_147 .. :try_end_14c} :catch_14e

    goto/16 :goto_b6

    .line 494
    :catch_14e
    move-exception v0

    goto/16 :goto_b6

    .line 473
    :catch_151
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    .line 474
    :goto_155
    const v2, 0x7f0b0187

    const/4 v6, 0x1

    :try_start_159
    invoke-static {p0, v2, v6}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 477
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 478
    sget-object v2, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Security exception opening: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v7}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 479
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V
    :try_end_181
    .catchall {:try_start_159 .. :try_end_181} :catchall_1fd

    .line 488
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    .line 489
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 490
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 491
    if-eqz v5, :cond_b6

    .line 493
    :try_start_18c
    iget-object v0, v5, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_191
    .catch Ljava/io/IOException; {:try_start_18c .. :try_end_191} :catch_193

    goto/16 :goto_b6

    .line 494
    :catch_193
    move-exception v0

    goto/16 :goto_b6

    .line 480
    :catch_196
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    .line 481
    :goto_19a
    const v2, 0x7f0b0183

    const/4 v6, 0x1

    :try_start_19e
    invoke-static {p0, v2, v6}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 484
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 485
    sget-object v2, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Problem opening file:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v7}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 486
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V
    :try_end_1c6
    .catchall {:try_start_19e .. :try_end_1c6} :catchall_1fd

    .line 488
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    .line 489
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 490
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 491
    if-eqz v5, :cond_b6

    .line 493
    :try_start_1d1
    iget-object v0, v5, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1d6
    .catch Ljava/io/IOException; {:try_start_1d1 .. :try_end_1d6} :catch_1d8

    goto/16 :goto_b6

    .line 494
    :catch_1d8
    move-exception v0

    goto/16 :goto_b6

    .line 488
    :catchall_1db
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    :goto_1df
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    .line 489
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 490
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 491
    if-eqz v5, :cond_1ef

    .line 493
    :try_start_1ea
    iget-object v1, v5, Lcom/dropbox/android/activity/bR;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1ef
    .catch Ljava/io/IOException; {:try_start_1ea .. :try_end_1ef} :catch_1f0

    .line 494
    :cond_1ef
    :goto_1ef
    throw v0

    :catch_1f0
    move-exception v1

    goto :goto_1ef

    .line 488
    :catchall_1f2
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    goto :goto_1df

    :catchall_1f6
    move-exception v0

    move-object v3, v1

    move-object v4, v2

    goto :goto_1df

    :catchall_1fa
    move-exception v0

    move-object v4, v2

    goto :goto_1df

    :catchall_1fd
    move-exception v0

    goto :goto_1df

    :catchall_1ff
    move-exception v0

    move-object v1, v2

    goto :goto_1df

    :catchall_202
    move-exception v0

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    goto :goto_1df

    .line 480
    :catch_207
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    goto :goto_19a

    :catch_20b
    move-exception v0

    move-object v3, v1

    move-object v4, v2

    goto :goto_19a

    :catch_20f
    move-exception v0

    move-object v4, v2

    goto :goto_19a

    :catch_212
    move-exception v0

    goto :goto_19a

    :catch_214
    move-exception v0

    move-object v1, v2

    goto :goto_19a

    .line 473
    :catch_217
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    goto/16 :goto_155

    :catch_21c
    move-exception v0

    move-object v3, v1

    move-object v4, v2

    goto/16 :goto_155

    :catch_221
    move-exception v0

    move-object v4, v2

    goto/16 :goto_155

    :catch_225
    move-exception v0

    goto/16 :goto_155

    :catch_228
    move-exception v0

    move-object v1, v2

    goto/16 :goto_155

    .line 466
    :catch_22c
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    goto/16 :goto_110

    :catch_232
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    goto/16 :goto_110

    :catch_237
    move-exception v0

    move-object v3, v1

    move-object v4, v2

    goto/16 :goto_110

    :catch_23c
    move-exception v0

    move-object v4, v2

    goto/16 :goto_110

    :catch_240
    move-exception v0

    goto/16 :goto_110

    .line 459
    :catch_243
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    goto/16 :goto_c9

    :catch_249
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v5

    goto/16 :goto_c9

    :catch_24f
    move-exception v0

    move-object v3, v2

    move-object v4, v5

    move-object v2, v1

    goto/16 :goto_c9

    :catch_255
    move-exception v0

    move-object v4, v5

    move-object v9, v2

    move-object v2, v3

    move-object v3, v9

    goto/16 :goto_c9

    :catch_25c
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_c9

    .line 494
    :catch_262
    move-exception v0

    goto/16 :goto_b6
.end method

.method private e()V
    .registers 3

    .prologue
    .line 702
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->f:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->g:Z

    if-eqz v0, :cond_16

    .line 703
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 704
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 705
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/activity/TextEditActivity;->setResult(ILandroid/content/Intent;)V

    .line 707
    :cond_16
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    .line 708
    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/TextEditActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->h:Z

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/TextEditActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->d:Z

    return v0
.end method

.method static synthetic g(Lcom/dropbox/android/activity/TextEditActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    return v0
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 238
    const/4 v0, 0x1

    return v0
.end method

.method protected final f_()Z
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 741
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->d:Z

    if-eqz v0, :cond_e

    .line 742
    sget-object v0, Lcom/dropbox/android/activity/bI;->a:Lcom/dropbox/android/activity/bI;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bI;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->showDialog(I)V

    .line 746
    :goto_d
    return-void

    .line 744
    :cond_e
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->e()V

    goto :goto_d
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 243
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 245
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 248
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 249
    const-string v2, "android.intent.action.EDIT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_94

    .line 251
    :cond_1c
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    .line 253
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    if-nez v0, :cond_50

    .line 254
    sget-object v0, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t edit this type of file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const v0, 0x7f0b0183

    invoke-static {p0, v0, v4}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 259
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    .line 327
    :goto_4f
    return-void

    .line 262
    :cond_50
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Ljava/lang/String;

    .line 281
    :goto_58
    const-string v0, "CHARACTER_SET"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_62

    .line 283
    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->l:Ljava/lang/String;

    .line 288
    :cond_62
    const v0, 0x7f030061

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->setContentView(I)V

    .line 291
    const v0, 0x7f0600fa

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->c:Landroid/widget/EditText;

    .line 293
    new-instance v0, Lcom/dropbox/android/activity/bF;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bF;-><init>(Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 319
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 321
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->f:Z

    if-nez v0, :cond_84

    .line 322
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->d()V

    .line 324
    :cond_84
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->d:Z

    .line 326
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->j:Lcom/dropbox/android/util/g;

    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/g;->a(Landroid/net/Uri;)V

    goto :goto_4f

    .line 263
    :cond_94
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e6

    .line 265
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_b0

    .line 267
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "output"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    .line 269
    :cond_b0
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    if-nez v0, :cond_d9

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/dropbox/android/util/af;->g()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    .line 272
    :cond_d9
    const v0, 0x7f0b017f

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->k:Ljava/lang/String;

    .line 273
    iput-boolean v4, p0, Lcom/dropbox/android/activity/TextEditActivity;->f:Z

    goto/16 :goto_58

    .line 276
    :cond_e6
    sget-object v1, Lcom/dropbox/android/activity/TextEditActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exiting, unknown action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TextEditActivity;->finish()V

    goto/16 :goto_4f
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 735
    invoke-static {p1}, Lcom/dropbox/android/activity/bI;->a(I)Lcom/dropbox/android/activity/bI;

    move-result-object v0

    .line 736
    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/bI;->a(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 338
    iget-boolean v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->i:Z

    if-eqz v0, :cond_10

    .line 339
    const v0, 0x7f0b0178

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v1, v1, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    .line 341
    :cond_10
    const v0, 0x7f0b0179

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TextEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v1, v0}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    .line 343
    return v2
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 331
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onDestroy()V

    .line 332
    iget-object v0, p0, Lcom/dropbox/android/activity/TextEditActivity;->j:Lcom/dropbox/android/util/g;

    invoke-virtual {v0}, Lcom/dropbox/android/util/g;->a()V

    .line 333
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 713
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_42

    .line 730
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 715
    :pswitch_a
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 716
    sget-object v1, Lcom/dropbox/android/activity/bI;->b:Lcom/dropbox/android/activity/bI;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bI;->a()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->showDialog(I)V

    goto :goto_9

    .line 718
    :cond_22
    iget-object v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->b:Landroid/net/Uri;

    invoke-direct {p0, v1, v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Landroid/net/Uri;Z)Z

    .line 719
    invoke-static {}, Lcom/dropbox/android/util/i;->ag()Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    goto :goto_9

    .line 723
    :pswitch_2f
    iget-boolean v1, p0, Lcom/dropbox/android/activity/TextEditActivity;->d:Z

    if-nez v1, :cond_37

    .line 724
    invoke-direct {p0}, Lcom/dropbox/android/activity/TextEditActivity;->e()V

    goto :goto_9

    .line 726
    :cond_37
    sget-object v1, Lcom/dropbox/android/activity/bI;->a:Lcom/dropbox/android/activity/bI;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/bI;->a()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->showDialog(I)V

    goto :goto_9

    .line 713
    nop

    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_a
        :pswitch_2f
    .end packed-switch
.end method
