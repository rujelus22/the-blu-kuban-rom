.class public Lcom/dropbox/android/activity/SendToFragment;
.super Lcom/dropbox/android/activity/HierarchicalBrowserFragment;
.source "panda.py"


# static fields
.field private static final n:Ljava/lang/String;


# instance fields
.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/ImageButton;

.field private m:Landroid/widget/TextView;

.field private final o:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 47
    const-class v0, Lcom/dropbox/android/activity/SendToFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/SendToFragment;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;-><init>()V

    .line 125
    new-instance v0, Lcom/dropbox/android/activity/bE;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bE;-><init>(Lcom/dropbox/android/activity/SendToFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->o:Landroid/view/View$OnClickListener;

    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/SendToFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_HIDE_QUICKACTIONS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 53
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 112
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 113
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->a()Z

    move-result v1

    if-nez v1, :cond_18

    .line 114
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    .line 116
    :goto_17
    return-object v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method protected final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 102
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/widget/X;
    .registers 2

    .prologue
    .line 107
    sget-object v0, Lcom/dropbox/android/widget/X;->b:Lcom/dropbox/android/widget/X;

    return-object v0
.end method

.method protected final d()I
    .registers 2

    .prologue
    .line 122
    const v0, 0x7f0b003e

    return v0
.end method

.method protected final e()I
    .registers 2

    .prologue
    .line 67
    const v0, 0x7f030028

    return v0
.end method

.method public final f(Ljava/lang/String;)V
    .registers 10
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 182
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 185
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 186
    if-nez v0, :cond_18

    .line 189
    const-string v0, ""

    .line 191
    :cond_18
    invoke-static {}, Lcom/dropbox/android/util/af;->b()Ljava/io/File;

    move-result-object v1

    .line 196
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 200
    :try_start_21
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_26
    .catchall {:try_start_21 .. :try_end_26} :catchall_9a
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_26} :catch_71

    .line 201
    :try_start_26
    new-instance v1, Ljava/io/OutputStreamWriter;

    const-string v5, "UTF-8"

    invoke-direct {v1, v3, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_26 .. :try_end_2d} :catchall_b2
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_2d} :catch_ba

    .line 203
    :try_start_2d
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_b5
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_37} :catch_be

    .line 208
    :try_start_37
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_3e
    .catchall {:try_start_37 .. :try_end_3e} :catchall_b5
    .catch Ljava/io/SyncFailedException; {:try_start_37 .. :try_end_3e} :catch_c1
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_3e} :catch_be

    .line 212
    :goto_3e
    :try_start_3e
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V

    .line 213
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_44
    .catchall {:try_start_3e .. :try_end_44} :catchall_b5
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_44} :catch_be

    .line 223
    if-eqz v1, :cond_49

    .line 224
    :try_start_46
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_49} :catch_a8

    .line 228
    :cond_49
    :goto_49
    if-eqz v3, :cond_4e

    .line 229
    :try_start_4b
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_aa

    .line 233
    :cond_4e
    :goto_4e
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v7}, Ljava/util/HashSet;-><init>(I)V

    .line 234
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 235
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/e;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {v0, v2, v1, v3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Landroid/content/Context;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Z

    .line 238
    return-void

    .line 214
    :catch_71
    move-exception v0

    move-object v1, v2

    .line 215
    :goto_73
    :try_start_73
    invoke-virtual {p0}, Lcom/dropbox/android/activity/SendToFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v5, 0x7f0b004a

    invoke-virtual {p0, v5}, Lcom/dropbox/android/activity/SendToFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 219
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 220
    sget-object v3, Lcom/dropbox/android/activity/SendToFragment;->n:Ljava/lang/String;

    const-string v5, "IOException creating tmp file for upload"

    invoke-static {v3, v5, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8d
    .catchall {:try_start_73 .. :try_end_8d} :catchall_b7

    .line 223
    if-eqz v1, :cond_92

    .line 224
    :try_start_8f
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_92
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_92} :catch_ac

    .line 228
    :cond_92
    :goto_92
    if-eqz v2, :cond_4e

    .line 229
    :try_start_94
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_97
    .catch Ljava/io/IOException; {:try_start_94 .. :try_end_97} :catch_98

    goto :goto_4e

    .line 231
    :catch_98
    move-exception v0

    goto :goto_4e

    .line 222
    :catchall_9a
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 223
    :goto_9d
    if-eqz v1, :cond_a2

    .line 224
    :try_start_9f
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_a2
    .catch Ljava/io/IOException; {:try_start_9f .. :try_end_a2} :catch_ae

    .line 228
    :cond_a2
    :goto_a2
    if-eqz v3, :cond_a7

    .line 229
    :try_start_a4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_a7
    .catch Ljava/io/IOException; {:try_start_a4 .. :try_end_a7} :catch_b0

    .line 231
    :cond_a7
    :goto_a7
    throw v0

    .line 226
    :catch_a8
    move-exception v0

    goto :goto_49

    .line 231
    :catch_aa
    move-exception v0

    goto :goto_4e

    .line 226
    :catch_ac
    move-exception v0

    goto :goto_92

    :catch_ae
    move-exception v1

    goto :goto_a2

    .line 231
    :catch_b0
    move-exception v1

    goto :goto_a7

    .line 222
    :catchall_b2
    move-exception v0

    move-object v1, v2

    goto :goto_9d

    :catchall_b5
    move-exception v0

    goto :goto_9d

    :catchall_b7
    move-exception v0

    move-object v3, v2

    goto :goto_9d

    .line 214
    :catch_ba
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_73

    :catch_be
    move-exception v0

    move-object v2, v3

    goto :goto_73

    .line 209
    :catch_c1
    move-exception v0

    goto/16 :goto_3e
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 74
    new-instance v0, Lcom/dropbox/android/activity/bC;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bC;-><init>(Lcom/dropbox/android/activity/SendToFragment;)V

    .line 80
    iget-object v1, p0, Lcom/dropbox/android/activity/SendToFragment;->k:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->k:Landroid/widget/Button;

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 83
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->j:Landroid/widget/Button;

    const v1, 0x7f0b015f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 84
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->j:Landroid/widget/Button;

    iget-object v1, p0, Lcom/dropbox/android/activity/SendToFragment;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->l:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 87
    new-instance v0, Lcom/dropbox/android/activity/bD;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/bD;-><init>(Lcom/dropbox/android/activity/SendToFragment;)V

    .line 95
    iget-object v1, p0, Lcom/dropbox/android/activity/SendToFragment;->l:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->m:Landroid/widget/TextView;

    const v1, 0x7f0b015e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 98
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 58
    const v0, 0x7f060030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->k:Landroid/widget/Button;

    .line 59
    const v0, 0x7f060031

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->j:Landroid/widget/Button;

    .line 60
    const v0, 0x7f060078

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->l:Landroid/widget/ImageButton;

    .line 61
    const v0, 0x7f060075

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/SendToFragment;->m:Landroid/widget/TextView;

    .line 62
    return-object v1
.end method
