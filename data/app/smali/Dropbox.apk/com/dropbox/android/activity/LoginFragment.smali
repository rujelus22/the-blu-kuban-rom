.class public Lcom/dropbox/android/activity/LoginFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field protected b:Lcom/dropbox/android/activity/aU;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/LoginFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LoginFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 55
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LoginFragment;->setArguments(Landroid/os/Bundle;)V

    .line 56
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/LoginFragment;
    .registers 4
    .parameter

    .prologue
    .line 48
    new-instance v0, Lcom/dropbox/android/activity/LoginFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/LoginFragment;-><init>()V

    .line 49
    invoke-virtual {v0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_EMAIL_PREFILL"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LoginFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dropbox/android/activity/LoginFragment;->b()V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LoginFragment;)Landroid/widget/EditText;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    .line 184
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Lcom/dropbox/android/activity/aU;

    iget-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/dropbox/android/activity/aU;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 69
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 73
    :goto_d
    return-void

    .line 71
    :cond_e
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_CLEAR_PASSWORD"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_d
.end method

.method public final b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 60
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :goto_12
    return-void

    .line 63
    :cond_13
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_12
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 6
    .parameter

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 79
    :try_start_3
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/activity/aU;

    move-object v1, v0

    iput-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Lcom/dropbox/android/activity/aU;
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_9} :catch_a

    .line 83
    return-void

    .line 80
    :catch_a
    move-exception v1

    .line 81
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement LoginFragmentCallback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    const v0, 0x7f030046

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 113
    const v0, 0x7f0600bc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Landroid/widget/EditText;

    .line 115
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_26

    .line 117
    const-string v2, "ARG_EMAIL_PREFILL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_26

    .line 119
    iget-object v2, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 123
    :cond_26
    const v0, 0x7f0600be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/EditText;

    .line 124
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/aQ;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aQ;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 149
    const v0, 0x7f0600bf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/Button;

    .line 150
    iget-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->e:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/aR;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aR;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v0, 0x7f0600c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 158
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v2

    if-nez v2, :cond_7a

    .line 159
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    :goto_68
    const v0, 0x7f0600c1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 173
    new-instance v2, Lcom/dropbox/android/activity/aT;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aT;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    return-object v1

    .line 161
    :cond_7a
    new-instance v2, Lcom/dropbox/android/activity/aS;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aS;-><init>(Lcom/dropbox/android/activity/LoginFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_68
.end method

.method public onDetach()V
    .registers 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginFragment;->b:Lcom/dropbox/android/activity/aU;

    .line 89
    return-void
.end method

.method public onResume()V
    .registers 4

    .prologue
    .line 93
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onResume()V

    .line 94
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 96
    const-string v1, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 97
    const-string v1, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    const-string v2, "ARG_RESET_EMAIL_PREFILL"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Lcom/dropbox/android/activity/LoginFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :cond_26
    const-string v1, "ARG_CLEAR_PASSWORD"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 103
    iget-object v1, p0, Lcom/dropbox/android/activity/LoginFragment;->d:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 104
    const-string v1, "ARG_CLEAR_PASSWORD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 106
    :cond_3b
    return-void
.end method
