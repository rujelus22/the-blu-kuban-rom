.class final Lcom/dropbox/android/activity/bO;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/dropbox/android/activity/TextEditActivity;

.field final synthetic c:Lcom/dropbox/android/activity/bM;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/bM;Landroid/widget/EditText;Lcom/dropbox/android/activity/TextEditActivity;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 155
    iput-object p1, p0, Lcom/dropbox/android/activity/bO;->c:Lcom/dropbox/android/activity/bM;

    iput-object p2, p0, Lcom/dropbox/android/activity/bO;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/dropbox/android/activity/bO;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_21

    .line 160
    iget-object v0, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    const v1, 0x7f0b0182

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    :cond_21
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3c

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 165
    :cond_3c
    invoke-static {v0}, Lcom/dropbox/android/util/DropboxPath;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167
    :try_start_40
    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 168
    const/16 v2, 0x2b

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
    :try_end_4d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_40 .. :try_end_4d} :catch_fa

    move-result-object v1

    .line 171
    :goto_4e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v3}, Lcom/dropbox/android/activity/TextEditActivity;->b(Lcom/dropbox/android/activity/TextEditActivity;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 172
    iget-object v2, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;Z)Z

    move-result v2

    if-eqz v2, :cond_f9

    .line 173
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/dropbox/android/util/af;->j()Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c7

    .line 175
    invoke-static {v1}, Lcom/dropbox/android/util/ae;->d(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v2

    .line 176
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v3

    new-instance v4, Lcom/dropbox/android/util/at;

    invoke-direct {v4, v2}, Lcom/dropbox/android/util/at;-><init>(Ljava/io/File;)V

    invoke-virtual {v4}, Lcom/dropbox/android/util/at;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v1, v5}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;Z)V

    .line 180
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/x;->m()Lcom/dropbox/android/util/ah;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/dropbox/android/util/ah;->b(Ljava/lang/String;)V

    .line 183
    :cond_c7
    invoke-static {}, Lcom/dropbox/android/util/i;->ae()Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V

    .line 184
    iget-object v2, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v2, v1}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Landroid/net/Uri;)Landroid/net/Uri;

    .line 185
    iget-object v2, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v2, v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->c(Lcom/dropbox/android/activity/TextEditActivity;)Lcom/dropbox/android/util/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/g;->a(Landroid/net/Uri;)V

    .line 187
    iget-object v0, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/TextEditActivity;->d(Lcom/dropbox/android/activity/TextEditActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/TextEditActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->e(Lcom/dropbox/android/activity/TextEditActivity;)Z

    move-result v0

    if-eqz v0, :cond_f9

    .line 190
    iget-object v0, p0, Lcom/dropbox/android/activity/bO;->b:Lcom/dropbox/android/activity/TextEditActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/TextEditActivity;->a(Lcom/dropbox/android/activity/TextEditActivity;)V

    .line 193
    :cond_f9
    return-void

    .line 169
    :catch_fa
    move-exception v2

    goto/16 :goto_4e
.end method
