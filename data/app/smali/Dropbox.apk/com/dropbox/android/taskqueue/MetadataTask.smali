.class public Lcom/dropbox/android/taskqueue/MetadataTask;
.super Lcom/dropbox/android/taskqueue/k;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/dropbox/android/provider/h;

.field private final d:Landroid/net/Uri;

.field private final h:Lcom/dropbox/android/provider/A;

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 26
    const-class v0, Lcom/dropbox/android/taskqueue/MetadataTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/MetadataTask;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/provider/h;Landroid/net/Uri;Lcom/dropbox/android/provider/A;I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/k;-><init>()V

    .line 35
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->b:Landroid/content/Context;

    .line 36
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->c:Lcom/dropbox/android/provider/h;

    .line 37
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->d:Landroid/net/Uri;

    .line 38
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->h:Lcom/dropbox/android/provider/A;

    .line 39
    iput p4, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->i:I

    .line 40
    return-void
.end method


# virtual methods
.method public final c()Lcom/dropbox/android/taskqueue/m;
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 44
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/k;->c()Lcom/dropbox/android/taskqueue/m;

    .line 46
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->c:Lcom/dropbox/android/provider/h;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 48
    new-instance v6, Lcom/dropbox/android/util/DropboxPath;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->d:Landroid/net/Uri;

    invoke-direct {v6, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 51
    invoke-static {v5, v6}, Lcom/dropbox/android/filemanager/u;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_9d

    .line 54
    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    .line 58
    :goto_1a
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->h:Lcom/dropbox/android/provider/A;

    invoke-static {v2, v6, v0, v4}, Lcom/dropbox/android/filemanager/u;->a(Landroid/content/Context;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Lcom/dropbox/android/provider/A;)Ldbxyzptlk/n/k;

    move-result-object v7

    .line 62
    if-nez v7, :cond_88

    sget-object v0, Lcom/dropbox/android/filemanager/W;->a:Lcom/dropbox/android/filemanager/W;

    move-object v4, v0

    .line 64
    :goto_27
    if-eqz v7, :cond_90

    move v2, v3

    .line 67
    :goto_2a
    if-eqz v2, :cond_52

    .line 69
    sget-object v0, Lcom/dropbox/android/taskqueue/MetadataTask;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Directory changed, going through line-by-line: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->d:Landroid/net/Uri;

    invoke-static {v9}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, v7, Ldbxyzptlk/n/k;->u:Ljava/util/List;

    .line 73
    iput-object v1, v7, Ldbxyzptlk/n/k;->u:Ljava/util/List;

    .line 74
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 78
    :cond_52
    invoke-static {v5, v6, v3}, Lcom/dropbox/android/filemanager/u;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;Z)Ljava/util/List;

    move-result-object v0

    .line 80
    new-instance v3, Lcom/dropbox/android/filemanager/t;

    iget-object v5, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->b:Landroid/content/Context;

    iget-object v6, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->c:Lcom/dropbox/android/provider/h;

    invoke-direct {v3, v5, v6}, Lcom/dropbox/android/filemanager/t;-><init>(Landroid/content/Context;Lcom/dropbox/android/provider/h;)V

    .line 81
    invoke-virtual {v3, v0, v1, v4}, Lcom/dropbox/android/filemanager/t;->a(Ljava/util/List;Ljava/util/List;Lcom/dropbox/android/filemanager/W;)Z

    .line 83
    if-eqz v2, :cond_6f

    .line 84
    iget v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->i:I

    if-lez v0, :cond_6f

    .line 86
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->d:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 90
    :cond_6f
    if-eqz v7, :cond_82

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->h:Lcom/dropbox/android/provider/A;

    if-eqz v0, :cond_82

    .line 91
    iget-boolean v0, v7, Ldbxyzptlk/n/k;->t:Z

    if-eqz v0, :cond_93

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->h:Lcom/dropbox/android/provider/A;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->b:Landroid/content/Context;

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    .line 98
    :cond_82
    :goto_82
    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/t;->a()V

    .line 100
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->b:Lcom/dropbox/android/taskqueue/m;

    return-object v0

    .line 62
    :cond_88
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/filemanager/V;->a(Landroid/content/Context;)Lcom/dropbox/android/filemanager/W;

    move-result-object v0

    move-object v4, v0

    goto :goto_27

    .line 64
    :cond_90
    const/4 v0, 0x0

    move v2, v0

    goto :goto_2a

    .line 94
    :cond_93
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->h:Lcom/dropbox/android/provider/A;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->b:Landroid/content/Context;

    const/16 v2, 0xff

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    goto :goto_82

    :cond_9d
    move-object v0, v1

    goto/16 :goto_1a
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/MetadataTask;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ldbxyzptlk/k/i;
    .registers 2

    .prologue
    .line 116
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/MetadataTask;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
