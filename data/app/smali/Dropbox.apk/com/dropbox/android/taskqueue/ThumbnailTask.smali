.class public Lcom/dropbox/android/taskqueue/ThumbnailTask;
.super Lcom/dropbox/android/taskqueue/k;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final k:Lcom/dropbox/android/taskqueue/F;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/dropbox/android/util/DropboxPath;

.field private final d:Ldbxyzptlk/n/o;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/dropbox/android/taskqueue/p;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 30
    const-class v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a:Ljava/lang/String;

    .line 96
    new-instance v0, Lcom/dropbox/android/taskqueue/F;

    const-wide/32 v1, 0x5265c00

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/F;-><init>(J)V

    sput-object v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->k:Lcom/dropbox/android/taskqueue/F;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ldbxyzptlk/n/o;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/p;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/k;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ljava/lang/String;

    .line 111
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->c:Lcom/dropbox/android/util/DropboxPath;

    .line 112
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->d:Ldbxyzptlk/n/o;

    .line 113
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->h:Ljava/lang/String;

    .line 114
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->i:Ljava/lang/String;

    .line 115
    iput-object p5, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->j:Lcom/dropbox/android/taskqueue/p;

    .line 116
    return-void
.end method

.method public static a(Ljava/lang/String;Ldbxyzptlk/n/o;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/p;)Lcom/dropbox/android/taskqueue/ThumbnailTask;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    sget-object v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->k:Lcom/dropbox/android/taskqueue/F;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/taskqueue/F;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 104
    const/4 v0, 0x0

    .line 106
    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/ThumbnailTask;-><init>(Ljava/lang/String;Ldbxyzptlk/n/o;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/taskqueue/p;)V

    goto :goto_9
.end method

.method public static a(Ljava/lang/String;Ldbxyzptlk/n/o;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b()V
    .registers 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->r()Lcom/dropbox/android/taskqueue/m;

    .line 138
    return-void
.end method

.method public final c()Lcom/dropbox/android/taskqueue/m;
    .registers 7

    .prologue
    .line 142
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/k;->c()Lcom/dropbox/android/taskqueue/m;

    .line 144
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->q()V

    .line 147
    :try_start_6
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 150
    iget-object v1, v0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->d:Ldbxyzptlk/n/o;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->k(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    sget-object v0, Ldbxyzptlk/n/n;->a:Ldbxyzptlk/n/n;

    :goto_1e
    invoke-virtual {v1, v2, v3, v0}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ldbxyzptlk/n/o;Ldbxyzptlk/n/n;)Ldbxyzptlk/n/i;
    :try_end_21
    .catch Ldbxyzptlk/o/i; {:try_start_6 .. :try_end_21} :catch_32
    .catch Ldbxyzptlk/o/a; {:try_start_6 .. :try_end_21} :catch_64

    move-result-object v3

    .line 165
    invoke-static {}, Lcom/dropbox/android/util/af;->a()Ljava/io/File;

    move-result-object v0

    .line 167
    if-nez v0, :cond_6c

    .line 168
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    .line 223
    :cond_2e
    :goto_2e
    return-object v0

    .line 150
    :cond_2f
    :try_start_2f
    sget-object v0, Ldbxyzptlk/n/n;->b:Ldbxyzptlk/n/n;
    :try_end_31
    .catch Ldbxyzptlk/o/i; {:try_start_2f .. :try_end_31} :catch_32
    .catch Ldbxyzptlk/o/a; {:try_start_2f .. :try_end_31} :catch_64

    goto :goto_1e

    .line 151
    :catch_32
    move-exception v0

    .line 152
    iget v1, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v2, 0x194

    if-ne v1, v2, :cond_49

    .line 153
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 154
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    goto :goto_2e

    .line 155
    :cond_49
    iget v0, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v1, 0x19f

    if-ne v0, v1, :cond_5d

    .line 156
    sget-object v0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->k:Lcom/dropbox/android/taskqueue/F;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/F;->a(Ljava/lang/Object;)V

    .line 157
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    goto :goto_2e

    .line 159
    :cond_5d
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    goto :goto_2e

    .line 161
    :catch_64
    move-exception v0

    .line 162
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    goto :goto_2e

    .line 171
    :cond_6c
    const/4 v2, 0x0

    .line 174
    :try_start_6d
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_72
    .catchall {:try_start_6d .. :try_end_72} :catchall_116
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6d .. :try_end_72} :catch_137
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_72} :catch_ff

    .line 175
    const/16 v2, 0x1000

    :try_start_74
    new-array v2, v2, [B

    .line 178
    :goto_76
    invoke-virtual {v3, v2}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 179
    if-gez v4, :cond_b1

    .line 186
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_7f
    .catchall {:try_start_74 .. :try_end_7f} :catchall_133
    .catch Ljava/lang/OutOfMemoryError; {:try_start_74 .. :try_end_7f} :catch_b6
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_7f} :catch_135

    .line 188
    :try_start_7f
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->sync()V
    :try_end_86
    .catchall {:try_start_7f .. :try_end_86} :catchall_133
    .catch Ljava/io/SyncFailedException; {:try_start_7f .. :try_end_86} :catch_13b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7f .. :try_end_86} :catch_b6
    .catch Ljava/io/IOException; {:try_start_7f .. :try_end_86} :catch_135

    .line 191
    :goto_86
    :try_start_86
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->h:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 193
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    .line 194
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_d3

    .line 195
    invoke-static {v4}, Lcom/dropbox/android/util/af;->b(Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_d3

    .line 196
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_a2
    .catchall {:try_start_86 .. :try_end_a2} :catchall_133
    .catch Ljava/lang/OutOfMemoryError; {:try_start_86 .. :try_end_a2} :catch_b6
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_a2} :catch_135

    move-result-object v0

    .line 209
    if-eqz v1, :cond_a8

    .line 211
    :try_start_a5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_a8
    .catch Ljava/io/IOException; {:try_start_a5 .. :try_end_a8} :catch_123

    .line 215
    :cond_a8
    :goto_a8
    if-eqz v3, :cond_2e

    .line 217
    :try_start_aa
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_ad
    .catch Ljava/io/IOException; {:try_start_aa .. :try_end_ad} :catch_ae

    goto :goto_2e

    .line 218
    :catch_ae
    move-exception v1

    goto/16 :goto_2e

    .line 182
    :cond_b1
    const/4 v5, 0x0

    :try_start_b2
    invoke-virtual {v1, v2, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_b5
    .catchall {:try_start_b2 .. :try_end_b5} :catchall_133
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b2 .. :try_end_b5} :catch_b6
    .catch Ljava/io/IOException; {:try_start_b2 .. :try_end_b5} :catch_135

    goto :goto_76

    .line 203
    :catch_b6
    move-exception v0

    .line 204
    :goto_b7
    :try_start_b7
    sget-object v2, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a:Ljava/lang/String;

    const-string v4, "Dropbox Gallery low on memory."

    invoke-static {v2, v4, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 205
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->g:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_c3
    .catchall {:try_start_b7 .. :try_end_c3} :catchall_133

    move-result-object v0

    .line 209
    if-eqz v1, :cond_c9

    .line 211
    :try_start_c6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_c9
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_c9} :catch_12b

    .line 215
    :cond_c9
    :goto_c9
    if-eqz v3, :cond_2e

    .line 217
    :try_start_cb
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_ce
    .catch Ljava/io/IOException; {:try_start_cb .. :try_end_ce} :catch_d0

    goto/16 :goto_2e

    .line 218
    :catch_d0
    move-exception v1

    goto/16 :goto_2e

    .line 200
    :cond_d3
    :try_start_d3
    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_ee

    .line 201
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_de
    .catchall {:try_start_d3 .. :try_end_de} :catchall_133
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d3 .. :try_end_de} :catch_b6
    .catch Ljava/io/IOException; {:try_start_d3 .. :try_end_de} :catch_135

    move-result-object v0

    .line 209
    if-eqz v1, :cond_e4

    .line 211
    :try_start_e1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_e4
    .catch Ljava/io/IOException; {:try_start_e1 .. :try_end_e4} :catch_125

    .line 215
    :cond_e4
    :goto_e4
    if-eqz v3, :cond_2e

    .line 217
    :try_start_e6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_e9
    .catch Ljava/io/IOException; {:try_start_e6 .. :try_end_e9} :catch_eb

    goto/16 :goto_2e

    .line 218
    :catch_eb
    move-exception v1

    goto/16 :goto_2e

    .line 209
    :cond_ee
    if-eqz v1, :cond_f3

    .line 211
    :try_start_f0
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_f3
    .catch Ljava/io/IOException; {:try_start_f0 .. :try_end_f3} :catch_127

    .line 215
    :cond_f3
    :goto_f3
    if-eqz v3, :cond_f8

    .line 217
    :try_start_f5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_f8
    .catch Ljava/io/IOException; {:try_start_f5 .. :try_end_f8} :catch_129

    .line 222
    :cond_f8
    :goto_f8
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->g()Lcom/dropbox/android/taskqueue/m;

    .line 223
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->b:Lcom/dropbox/android/taskqueue/m;

    goto/16 :goto_2e

    .line 206
    :catch_ff
    move-exception v0

    move-object v1, v2

    .line 207
    :goto_101
    :try_start_101
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_106
    .catchall {:try_start_101 .. :try_end_106} :catchall_133

    move-result-object v0

    .line 209
    if-eqz v1, :cond_10c

    .line 211
    :try_start_109
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_10c
    .catch Ljava/io/IOException; {:try_start_109 .. :try_end_10c} :catch_12d

    .line 215
    :cond_10c
    :goto_10c
    if-eqz v3, :cond_2e

    .line 217
    :try_start_10e
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_111
    .catch Ljava/io/IOException; {:try_start_10e .. :try_end_111} :catch_113

    goto/16 :goto_2e

    .line 218
    :catch_113
    move-exception v1

    goto/16 :goto_2e

    .line 209
    :catchall_116
    move-exception v0

    move-object v1, v2

    :goto_118
    if-eqz v1, :cond_11d

    .line 211
    :try_start_11a
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_11d
    .catch Ljava/io/IOException; {:try_start_11a .. :try_end_11d} :catch_12f

    .line 215
    :cond_11d
    :goto_11d
    if-eqz v3, :cond_122

    .line 217
    :try_start_11f
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_122
    .catch Ljava/io/IOException; {:try_start_11f .. :try_end_122} :catch_131

    .line 218
    :cond_122
    :goto_122
    throw v0

    .line 212
    :catch_123
    move-exception v1

    goto :goto_a8

    :catch_125
    move-exception v1

    goto :goto_e4

    :catch_127
    move-exception v0

    goto :goto_f3

    .line 218
    :catch_129
    move-exception v0

    goto :goto_f8

    .line 212
    :catch_12b
    move-exception v1

    goto :goto_c9

    :catch_12d
    move-exception v1

    goto :goto_10c

    :catch_12f
    move-exception v1

    goto :goto_11d

    .line 218
    :catch_131
    move-exception v1

    goto :goto_122

    .line 209
    :catchall_133
    move-exception v0

    goto :goto_118

    .line 206
    :catch_135
    move-exception v0

    goto :goto_101

    .line 203
    :catch_137
    move-exception v0

    move-object v1, v2

    goto/16 :goto_b7

    .line 189
    :catch_13b
    move-exception v2

    goto/16 :goto_86
.end method

.method public final d()Ljava/lang/String;
    .registers 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->d:Ldbxyzptlk/n/o;

    invoke-static {v0, v1}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->a(Ljava/lang/String;Ldbxyzptlk/n/o;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ldbxyzptlk/k/i;
    .registers 3

    .prologue
    .line 233
    new-instance v0, Ldbxyzptlk/k/i;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->c:Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, v1}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ldbxyzptlk/n/o;
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->d:Ldbxyzptlk/n/o;

    return-object v0
.end method

.method public final j()Lcom/dropbox/android/taskqueue/p;
    .registers 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/ThumbnailTask;->j:Lcom/dropbox/android/taskqueue/p;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ThumbnailTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/ThumbnailTask;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
