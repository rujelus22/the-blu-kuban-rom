.class public Lcom/dropbox/android/taskqueue/CameraUploadTask;
.super Lcom/dropbox/android/taskqueue/DbTask;
.source "panda.py"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:J

.field private final c:Landroid/content/Context;

.field private final d:Ljava/io/File;

.field private h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:J

.field private final n:I

.field private final o:J

.field private final p:Ljava/lang/String;

.field private q:Z

.field private r:J

.field private s:Ldbxyzptlk/r/C;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 62
    const-class v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {}, Lcom/dropbox/android/util/aj;->a()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-wide/from16 v5, p5

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v12}, Lcom/dropbox/android/taskqueue/CameraUploadTask;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;Z)V

    .line 91
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;Z)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/DbTask;-><init>()V

    .line 82
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    .line 83
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/r/C;

    .line 96
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    .line 97
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    .line 98
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    .line 99
    iput-object p7, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:Ljava/lang/String;

    .line 100
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    .line 101
    iput-object p3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    .line 102
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    .line 103
    iput-wide p5, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:J

    .line 104
    iput p8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:I

    .line 105
    iput-wide p9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:J

    .line 106
    iput-object p11, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Ljava/lang/String;

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:Z

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/CameraUploadTask;Ldbxyzptlk/l/m;Lcom/dropbox/android/service/x;)Lcom/dropbox/android/taskqueue/o;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ldbxyzptlk/l/m;Lcom/dropbox/android/service/x;)Lcom/dropbox/android/taskqueue/o;

    move-result-object v0

    return-object v0
.end method

.method private a(Ldbxyzptlk/l/m;Lcom/dropbox/android/service/x;)Lcom/dropbox/android/taskqueue/o;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 143
    invoke-virtual {p2}, Lcom/dropbox/android/service/x;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 144
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->a:Lcom/dropbox/android/taskqueue/o;

    .line 156
    :goto_8
    return-object v0

    .line 145
    :cond_9
    invoke-virtual {p1}, Ldbxyzptlk/l/m;->q()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p1}, Ldbxyzptlk/l/m;->r()Z

    move-result v0

    if-eqz v0, :cond_21

    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    const-wide/32 v2, 0x1900000

    cmp-long v0, v0, v2

    if-ltz v0, :cond_21

    .line 147
    :cond_1e
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->b:Lcom/dropbox/android/taskqueue/o;

    goto :goto_8

    .line 148
    :cond_21
    invoke-virtual {p2}, Lcom/dropbox/android/service/x;->a()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 149
    invoke-virtual {p2}, Lcom/dropbox/android/service/x;->c()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-virtual {p2}, Lcom/dropbox/android/service/x;->d()Z

    move-result v0

    if-nez v0, :cond_36

    .line 150
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->a:Lcom/dropbox/android/taskqueue/o;

    goto :goto_8

    .line 152
    :cond_36
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->d:Lcom/dropbox/android/taskqueue/o;

    goto :goto_8

    .line 156
    :cond_39
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->c:Lcom/dropbox/android/taskqueue/o;

    goto :goto_8
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/CameraUploadTask;)Ldbxyzptlk/r/C;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/r/C;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 243
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4c

    .line 247
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    .line 248
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    .line 249
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 250
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 251
    const-string v5, "server_hash"

    iget-object v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v5, "camera_upload"

    const-string v6, "_id = ?"

    new-array v7, v0, [Ljava/lang/String;

    iget-wide v8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 254
    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:Z

    .line 255
    const-string v1, "hash_change"

    invoke-static {v1, p0}, Lcom/dropbox/android/util/i;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v3, "oldid"

    int-to-long v4, v2

    invoke-virtual {v1, v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    .line 258
    :goto_4b
    return v0

    :cond_4c
    move v0, v1

    goto :goto_4b
.end method

.method public static restore(Landroid/content/Context;JLjava/lang/String;)Lcom/dropbox/android/taskqueue/CameraUploadTask;
    .registers 17
    .parameter
    .parameter
    .parameter

    .prologue
    .line 621
    invoke-static/range {p3 .. p3}, Ldbxyzptlk/D/d;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ldbxyzptlk/D/c;

    .line 625
    const-string v0, "mBatchFileNumber"

    invoke-virtual {v1, v0}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 626
    if-eqz v0, :cond_71

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v8, v2

    .line 628
    :goto_16
    new-instance v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    new-instance v2, Ljava/io/File;

    const-string v3, "mFilePath"

    invoke-virtual {v1, v3}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v3, "mServerHash"

    invoke-virtual {v1, v3}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "mMimeType"

    invoke-virtual {v1, v4}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "mDbRowId"

    invoke-virtual {v1, v5}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-string v7, "mContentUri"

    invoke-virtual {v1, v7}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v9, "mImportTime"

    invoke-virtual {v1, v9}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const-string v11, "mImportTimeoffset"

    invoke-virtual {v1, v11}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-string v12, "mRehashed"

    invoke-virtual {v1, v12}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    move-object v1, p0

    invoke-direct/range {v0 .. v12}, Lcom/dropbox/android/taskqueue/CameraUploadTask;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;IJLjava/lang/String;Z)V

    .line 638
    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b(J)V

    .line 639
    return-object v0

    .line 626
    :cond_71
    const/4 v8, 0x0

    goto :goto_16
.end method

.method private s()Lcom/dropbox/android/service/w;
    .registers 2

    .prologue
    .line 503
    new-instance v0, Lcom/dropbox/android/taskqueue/d;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/d;-><init>(Lcom/dropbox/android/taskqueue/CameraUploadTask;)V

    return-object v0
.end method

.method private t()V
    .registers 9

    .prologue
    const/4 v4, 0x1

    .line 644
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 645
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 646
    const-string v2, "uploaded"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 647
    const-string v2, "camera_upload"

    const-string v3, "_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 651
    return-void
.end method

.method private u()V
    .registers 8

    .prologue
    .line 657
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 658
    const-string v1, "camera_upload"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 659
    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/taskqueue/DbTask;)I
    .registers 13
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 164
    instance-of v0, p1, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    if-eqz v0, :cond_6a

    move-object v0, p1

    .line 165
    check-cast v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    .line 168
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v4

    .line 169
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v5

    .line 170
    if-eqz v4, :cond_1f

    if-nez v5, :cond_1f

    .line 196
    :cond_1e
    :goto_1e
    return v1

    .line 172
    :cond_1f
    if-nez v4, :cond_25

    if-eqz v5, :cond_25

    move v1, v2

    .line 173
    goto :goto_1e

    .line 176
    :cond_25
    iget-wide v5, v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    .line 178
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/l/m;->z()J

    move-result-wide v7

    .line 179
    iget-wide v9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    cmp-long v4, v9, v7

    if-gez v4, :cond_45

    move v4, v1

    .line 180
    :goto_36
    cmp-long v7, v5, v7

    if-gez v7, :cond_3b

    move v3, v1

    .line 181
    :cond_3b
    if-eqz v4, :cond_3f

    if-eqz v3, :cond_1e

    .line 183
    :cond_3f
    if-nez v4, :cond_47

    if-eqz v3, :cond_47

    move v1, v2

    .line 184
    goto :goto_1e

    :cond_45
    move v4, v3

    .line 179
    goto :goto_36

    .line 187
    :cond_47
    iget-wide v7, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_65

    .line 188
    if-eqz v4, :cond_63

    if-eqz v3, :cond_63

    .line 189
    :goto_51
    iget-wide v2, v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-wide v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    mul-int/2addr v1, v0

    goto :goto_1e

    :cond_63
    move v1, v2

    .line 188
    goto :goto_51

    .line 192
    :cond_65
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v1

    goto :goto_1e

    .line 196
    :cond_6a
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v1

    goto :goto_1e
.end method

.method public final a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    .registers 3
    .parameter

    .prologue
    .line 556
    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/m;->a()Z

    move-result v0

    if-nez v0, :cond_15

    .line 557
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    if-ne p1, v0, :cond_12

    .line 558
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->u()V

    .line 563
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->g()Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    .line 569
    :goto_11
    return-object v0

    .line 566
    :cond_12
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->t()V

    .line 569
    :cond_15
    invoke-super {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    goto :goto_11
.end method

.method public final a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 574
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/taskqueue/DbTask;->a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V

    .line 575
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/m;->b()Lcom/dropbox/android/taskqueue/n;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/n;->b:Lcom/dropbox/android/taskqueue/n;

    if-ne v0, v1, :cond_32

    sget-object v0, Lcom/dropbox/android/taskqueue/m;->j:Lcom/dropbox/android/taskqueue/m;

    if-eq p2, v0, :cond_32

    .line 576
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 577
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 578
    const-string v2, "ARG_FILENAME"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    const-string v0, "ARG_STATUS"

    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/m;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    sget-object v0, Lcom/dropbox/android/util/aA;->a:Lcom/dropbox/android/util/aA;

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aA;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 582
    :cond_32
    return-void
.end method

.method protected final a(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 221
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    invoke-static {v1}, Ldbxyzptlk/l/m;->b(Landroid/content/Context;)Ldbxyzptlk/l/m;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;

    move-result-object v1

    .line 222
    if-nez v1, :cond_e

    .line 227
    :cond_d
    :goto_d
    return v0

    :cond_e
    iget-wide v2, v1, Ldbxyzptlk/n/c;->e:J

    add-long/2addr v2, p1

    iget-wide v4, v1, Ldbxyzptlk/n/c;->d:J

    add-long/2addr v2, v4

    iget-wide v4, v1, Ldbxyzptlk/n/c;->c:J

    const-wide/32 v6, 0x3200000

    sub-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_d

    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 212
    monitor-enter p0

    .line 213
    :try_start_1
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    .line 214
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/r/C;

    if-eqz v0, :cond_d

    .line 215
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/r/C;

    invoke-interface {v0}, Ldbxyzptlk/r/C;->a()V

    .line 217
    :cond_d
    monitor-exit p0

    .line 218
    return-void

    .line 217
    :catchall_f
    move-exception v0

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/m;
    .registers 16

    .prologue
    const-wide/16 v2, 0x0

    .line 263
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->c()Lcom/dropbox/android/taskqueue/m;

    .line 265
    new-instance v14, Lcom/dropbox/android/taskqueue/G;

    invoke-direct {v14}, Lcom/dropbox/android/taskqueue/G;-><init>()V

    .line 267
    :try_start_a
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->a()V
    :try_end_d
    .catchall {:try_start_a .. :try_end_d} :catchall_2f9

    .line 271
    :try_start_d
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 272
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_1a
    .catchall {:try_start_d .. :try_end_1a} :catchall_2f9
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_1a} :catch_58
    .catch Ljava/lang/SecurityException; {:try_start_d .. :try_end_1a} :catch_63

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    :goto_1e
    return-object v0

    .line 275
    :cond_1f
    :try_start_1f
    new-instance v10, Ljava/io/FileInputStream;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 276
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    .line 277
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    .line 279
    if-eqz v10, :cond_3e

    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_48

    .line 280
    :cond_3e
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_43
    .catchall {:try_start_1f .. :try_end_43} :catchall_2f9
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_43} :catch_58
    .catch Ljava/lang/SecurityException; {:try_start_1f .. :try_end_43} :catch_63

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto :goto_1e

    .line 281
    :cond_48
    :try_start_48
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_6e

    .line 287
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_53
    .catchall {:try_start_48 .. :try_end_53} :catchall_2f9
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_53} :catch_58
    .catch Ljava/lang/SecurityException; {:try_start_48 .. :try_end_53} :catch_63

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto :goto_1e

    .line 289
    :catch_58
    move-exception v0

    .line 290
    :try_start_59
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_5e
    .catchall {:try_start_59 .. :try_end_5e} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto :goto_1e

    .line 291
    :catch_63
    move-exception v0

    .line 292
    :try_start_64
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->f:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_69
    .catchall {:try_start_64 .. :try_end_69} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto :goto_1e

    .line 296
    :cond_6e
    :try_start_6e
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(J)Z
    :try_end_73
    .catchall {:try_start_6e .. :try_end_73} :catchall_2f9

    move-result v0

    if-nez v0, :cond_c7

    .line 299
    :try_start_76
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;

    .line 300
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(J)Z

    move-result v0

    if-nez v0, :cond_c7

    .line 301
    iget v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:I

    .line 302
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    sget-object v1, Lcom/dropbox/android/util/aC;->b:Lcom/dropbox/android/util/aC;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z

    .line 304
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->l:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_9a
    .catchall {:try_start_76 .. :try_end_9a} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_76 .. :try_end_9a} :catch_9f
    .catch Ldbxyzptlk/o/a; {:try_start_76 .. :try_end_9a} :catch_ab

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto :goto_1e

    .line 307
    :catch_9f
    move-exception v0

    .line 308
    :try_start_a0
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_a5
    .catchall {:try_start_a0 .. :try_end_a5} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 309
    :catch_ab
    move-exception v0

    .line 310
    :try_start_ac
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    const-string v2, "Error getting account info for out of quota task."

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 312
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_c1
    .catchall {:try_start_ac .. :try_end_c1} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 319
    :cond_c7
    :try_start_c7
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    sget-object v1, Lcom/dropbox/android/util/aC;->b:Lcom/dropbox/android/util/aC;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/NotificationService;->b(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 321
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q()V

    .line 323
    new-instance v13, Lcom/dropbox/android/taskqueue/b;

    invoke-direct {v13, p0}, Lcom/dropbox/android/taskqueue/b;-><init>(Lcom/dropbox/android/taskqueue/CameraUploadTask;)V

    .line 330
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;
    :try_end_dc
    .catchall {:try_start_c7 .. :try_end_dc} :catchall_2f9

    .line 332
    :try_start_dc
    monitor-enter p0
    :try_end_dd
    .catchall {:try_start_dc .. :try_end_dd} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_dc .. :try_end_dd} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_dc .. :try_end_dd} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_dc .. :try_end_dd} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_dc .. :try_end_dd} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_dc .. :try_end_dd} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_dc .. :try_end_dd} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_dc .. :try_end_dd} :catch_381

    .line 333
    :try_start_dd
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n()Z

    move-result v1

    if-eqz v1, :cond_ed

    .line 334
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r()Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    monitor-exit p0
    :try_end_e8
    .catchall {:try_start_dd .. :try_end_e8} :catchall_125

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 336
    :cond_ed
    :try_start_ed
    monitor-exit p0
    :try_end_ee
    .catchall {:try_start_ed .. :try_end_ee} :catchall_125

    .line 340
    :try_start_ee
    iget-wide v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J
    :try_end_f0
    .catchall {:try_start_ee .. :try_end_f0} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_ee .. :try_end_f0} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_ee .. :try_end_f0} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_ee .. :try_end_f0} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_ee .. :try_end_f0} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_ee .. :try_end_f0} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_ee .. :try_end_f0} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_ee .. :try_end_f0} :catch_381

    const-wide/32 v3, 0x800000

    cmp-long v1, v1, v3

    if-gez v1, :cond_15e

    .line 343
    :try_start_f7
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-static {v1}, Lcom/dropbox/android/service/CameraUploadService;->a(Ljava/io/File;)Ljava/lang/String;
    :try_end_fc
    .catchall {:try_start_f7 .. :try_end_fc} :catchall_2f9
    .catch Ljava/io/IOException; {:try_start_f7 .. :try_end_fc} :catch_152
    .catch Ldbxyzptlk/o/d; {:try_start_f7 .. :try_end_fc} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_f7 .. :try_end_fc} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_f7 .. :try_end_fc} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_f7 .. :try_end_fc} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_f7 .. :try_end_fc} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_f7 .. :try_end_fc} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_f7 .. :try_end_fc} :catch_381

    move-result-object v1

    .line 347
    :try_start_fd
    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ljava/lang/String;)Z

    .line 348
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    iget-wide v4, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:J

    iget-object v8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Ljava/lang/String;

    iget v9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:I

    iget-wide v11, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    invoke-virtual/range {v0 .. v13}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/InputStream;JLdbxyzptlk/n/t;)Ldbxyzptlk/r/C;

    move-result-object v0

    .line 372
    :goto_114
    monitor-enter p0
    :try_end_115
    .catchall {:try_start_fd .. :try_end_115} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_fd .. :try_end_115} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_fd .. :try_end_115} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_fd .. :try_end_115} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_fd .. :try_end_115} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_fd .. :try_end_115} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_fd .. :try_end_115} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_fd .. :try_end_115} :catch_381

    .line 373
    :try_start_115
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n()Z

    move-result v1

    if-eqz v1, :cond_194

    .line 374
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r()Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    monitor-exit p0
    :try_end_120
    .catchall {:try_start_115 .. :try_end_120} :catchall_21f

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 336
    :catchall_125
    move-exception v0

    :try_start_126
    monitor-exit p0
    :try_end_127
    .catchall {:try_start_126 .. :try_end_127} :catchall_125

    :try_start_127
    throw v0
    :try_end_128
    .catchall {:try_start_127 .. :try_end_128} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_127 .. :try_end_128} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_127 .. :try_end_128} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_127 .. :try_end_128} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_127 .. :try_end_128} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_127 .. :try_end_128} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_127 .. :try_end_128} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_127 .. :try_end_128} :catch_381

    .line 404
    :catch_128
    move-exception v0

    .line 405
    :try_start_129
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IO Exception uploading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 406
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_14c
    .catchall {:try_start_129 .. :try_end_14c} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 344
    :catch_152
    move-exception v0

    .line 345
    :try_start_153
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_158
    .catchall {:try_start_153 .. :try_end_158} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_153 .. :try_end_158} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_153 .. :try_end_158} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_153 .. :try_end_158} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_153 .. :try_end_158} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_153 .. :try_end_158} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_153 .. :try_end_158} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_153 .. :try_end_158} :catch_381

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 353
    :cond_15e
    :try_start_15e
    iget-wide v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    invoke-static {v10, v1, v2, v13}, Ldbxyzptlk/r/a;->b(Ljava/io/FileInputStream;JLdbxyzptlk/n/t;)Ldbxyzptlk/r/g;
    :try_end_163
    .catchall {:try_start_15e .. :try_end_163} :catchall_2f9
    .catch Ljava/io/IOException; {:try_start_15e .. :try_end_163} :catch_181
    .catch Ldbxyzptlk/o/d; {:try_start_15e .. :try_end_163} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_15e .. :try_end_163} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_15e .. :try_end_163} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_15e .. :try_end_163} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_15e .. :try_end_163} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_15e .. :try_end_163} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_15e .. :try_end_163} :catch_381

    move-result-object v11

    .line 358
    :try_start_164
    new-instance v12, Lcom/dropbox/android/taskqueue/c;

    invoke-direct {v12, p0}, Lcom/dropbox/android/taskqueue/c;-><init>(Lcom/dropbox/android/taskqueue/CameraUploadTask;)V

    .line 367
    iget-object v1, v11, Ldbxyzptlk/r/g;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ljava/lang/String;)Z

    .line 368
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    iget-wide v4, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a:J

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:J

    iget-object v8, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Ljava/lang/String;

    iget v9, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:I

    invoke-virtual/range {v0 .. v12}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;ILjava/io/FileInputStream;Ldbxyzptlk/r/g;Ldbxyzptlk/r/l;)Ldbxyzptlk/r/C;

    move-result-object v0

    goto :goto_114

    .line 354
    :catch_181
    move-exception v0

    .line 355
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    const-string v2, "Error while scanning file"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 356
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_18e
    .catchall {:try_start_164 .. :try_end_18e} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_164 .. :try_end_18e} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_164 .. :try_end_18e} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_164 .. :try_end_18e} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_164 .. :try_end_18e} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_164 .. :try_end_18e} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_164 .. :try_end_18e} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_164 .. :try_end_18e} :catch_381

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 376
    :cond_194
    :try_start_194
    iput-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/r/C;

    .line 377
    monitor-exit p0
    :try_end_197
    .catchall {:try_start_194 .. :try_end_197} :catchall_21f

    .line 379
    :try_start_197
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s()Lcom/dropbox/android/service/w;

    move-result-object v1

    .line 380
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/u;->a(Lcom/dropbox/android/service/w;)V
    :try_end_1a2
    .catchall {:try_start_197 .. :try_end_1a2} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_197 .. :try_end_1a2} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_197 .. :try_end_1a2} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_197 .. :try_end_1a2} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_197 .. :try_end_1a2} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_197 .. :try_end_1a2} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_197 .. :try_end_1a2} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_197 .. :try_end_1a2} :catch_381

    .line 384
    :try_start_1a2
    const-string v0, "net.start"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/i;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 385
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->s:Ldbxyzptlk/r/C;

    invoke-interface {v0}, Ldbxyzptlk/r/C;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/r/t;

    .line 386
    const-string v2, "net.end"

    invoke-static {v2, p0}, Lcom/dropbox/android/util/i;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V
    :try_end_1bc
    .catchall {:try_start_1a2 .. :try_end_1bc} :catchall_239

    .line 388
    :try_start_1bc
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/dropbox/android/service/u;->b(Lcom/dropbox/android/service/w;)V

    .line 391
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v1

    .line 392
    invoke-virtual {v0}, Ldbxyzptlk/r/t;->a()Ldbxyzptlk/n/k;

    move-result-object v2

    iget-object v2, v2, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    .line 393
    invoke-virtual {v0}, Ldbxyzptlk/r/t;->a()Ldbxyzptlk/n/k;

    move-result-object v3

    iget-object v3, v3, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    .line 394
    sget-object v4, Lcom/dropbox/android/taskqueue/v;->b:Lcom/dropbox/android/taskqueue/v;

    invoke-static {}, Lcom/dropbox/android/util/bc;->d()Ldbxyzptlk/n/o;

    move-result-object v5

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V

    .line 395
    sget-object v4, Lcom/dropbox/android/taskqueue/v;->b:Lcom/dropbox/android/taskqueue/v;

    invoke-static {}, Lcom/dropbox/android/util/bc;->g()Ldbxyzptlk/n/o;

    move-result-object v5

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V

    .line 396
    sget-object v4, Lcom/dropbox/android/taskqueue/v;->b:Lcom/dropbox/android/taskqueue/v;

    invoke-static {}, Lcom/dropbox/android/util/bc;->e()Ldbxyzptlk/n/o;

    move-result-object v5

    invoke-virtual {v1, v4, v2, v3, v5}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V
    :try_end_1f2
    .catchall {:try_start_1bc .. :try_end_1f2} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_1bc .. :try_end_1f2} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_1bc .. :try_end_1f2} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_1bc .. :try_end_1f2} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_1bc .. :try_end_1f2} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_1bc .. :try_end_1f2} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_1bc .. :try_end_1f2} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_1bc .. :try_end_1f2} :catch_381

    .line 400
    :try_start_1f2
    invoke-virtual {v0}, Ldbxyzptlk/r/t;->b()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_206

    .line 401
    invoke-virtual {v0}, Ldbxyzptlk/r/t;->b()F

    move-result v0

    const/high16 v1, 0x447a

    mul-float/2addr v0, v1

    float-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_206
    .catchall {:try_start_1f2 .. :try_end_206} :catchall_2f9
    .catch Ljava/lang/InterruptedException; {:try_start_1f2 .. :try_end_206} :catch_3b7
    .catch Ldbxyzptlk/o/d; {:try_start_1f2 .. :try_end_206} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_1f2 .. :try_end_206} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_1f2 .. :try_end_206} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_1f2 .. :try_end_206} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_1f2 .. :try_end_206} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_1f2 .. :try_end_206} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_1f2 .. :try_end_206} :catch_381

    .line 489
    :cond_206
    :goto_206
    :try_start_206
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.dropbox.android.taskqueue.CameraUploadTask.ACTION_CAMERA_UPLOAD_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 490
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    invoke-static {v1}, Ldbxyzptlk/a/g;->a(Landroid/content/Context;)Ldbxyzptlk/a/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/a/g;->a(Landroid/content/Intent;)Z

    .line 492
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->g()Lcom/dropbox/android/taskqueue/m;
    :try_end_219
    .catchall {:try_start_206 .. :try_end_219} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 377
    :catchall_21f
    move-exception v0

    :try_start_220
    monitor-exit p0
    :try_end_221
    .catchall {:try_start_220 .. :try_end_221} :catchall_21f

    :try_start_221
    throw v0
    :try_end_222
    .catchall {:try_start_221 .. :try_end_222} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_221 .. :try_end_222} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_221 .. :try_end_222} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_221 .. :try_end_222} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_221 .. :try_end_222} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_221 .. :try_end_222} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_221 .. :try_end_222} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_221 .. :try_end_222} :catch_381

    .line 407
    :catch_222
    move-exception v0

    .line 408
    :try_start_223
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n()Z

    move-result v0

    if-eqz v0, :cond_251

    .line 409
    sget-object v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    const-string v1, "Upload canceled"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r()Lcom/dropbox/android/taskqueue/m;
    :try_end_233
    .catchall {:try_start_223 .. :try_end_233} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 388
    :catchall_239
    move-exception v0

    :try_start_23a
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/dropbox/android/service/u;->b(Lcom/dropbox/android/service/w;)V

    throw v0
    :try_end_242
    .catchall {:try_start_23a .. :try_end_242} :catchall_2f9
    .catch Ldbxyzptlk/o/d; {:try_start_23a .. :try_end_242} :catch_128
    .catch Ldbxyzptlk/o/g; {:try_start_23a .. :try_end_242} :catch_222
    .catch Ldbxyzptlk/o/j; {:try_start_23a .. :try_end_242} :catch_242
    .catch Ldbxyzptlk/r/v; {:try_start_23a .. :try_end_242} :catch_25c
    .catch Ldbxyzptlk/o/i; {:try_start_23a .. :try_end_242} :catch_268
    .catch Ldbxyzptlk/o/f; {:try_start_23a .. :try_end_242} :catch_337
    .catch Ldbxyzptlk/o/a; {:try_start_23a .. :try_end_242} :catch_381

    .line 418
    :catch_242
    move-exception v0

    .line 419
    :try_start_243
    invoke-static {}, Lcom/dropbox/android/util/a;->a()V

    .line 420
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_24b
    .catchall {:try_start_243 .. :try_end_24b} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 416
    :cond_251
    :try_start_251
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_256
    .catchall {:try_start_251 .. :try_end_256} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 421
    :catch_25c
    move-exception v0

    .line 422
    :try_start_25d
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->i:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_262
    .catchall {:try_start_25d .. :try_end_262} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 423
    :catch_268
    move-exception v0

    .line 424
    :try_start_269
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    const-string v2, "Server exception uploading."

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget v1, v0, Ldbxyzptlk/o/i;->b:I

    sparse-switch v1, :sswitch_data_3ba

    .line 472
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 473
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_283
    .catchall {:try_start_269 .. :try_end_283} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 430
    :sswitch_289
    :try_start_289
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sent bad camera upload hash for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    iget-boolean v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:Z

    if-nez v1, :cond_2d0

    .line 432
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    const-string v2, "Rehashing."

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b2
    .catchall {:try_start_289 .. :try_end_2b2} :catchall_2f9

    .line 434
    :try_start_2b2
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d:Ljava/io/File;

    invoke-static {v1}, Lcom/dropbox/android/service/CameraUploadService;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2c7

    .line 437
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c()Lcom/dropbox/android/taskqueue/m;
    :try_end_2c1
    .catchall {:try_start_2b2 .. :try_end_2c1} :catchall_2f9
    .catch Ljava/io/IOException; {:try_start_2b2 .. :try_end_2c1} :catch_3b4

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 439
    :cond_2c7
    :try_start_2c7
    const-string v1, "rehashed_nochange"

    invoke-static {v1, p0}, Lcom/dropbox/android/util/i;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V
    :try_end_2d0
    .catchall {:try_start_2c7 .. :try_end_2d0} :catchall_2f9
    .catch Ljava/io/IOException; {:try_start_2c7 .. :try_end_2d0} :catch_3b4

    .line 448
    :cond_2d0
    :goto_2d0
    :try_start_2d0
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    const-string v2, "Failing due to bad hash."

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 450
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_2e5
    .catchall {:try_start_2d0 .. :try_end_2e5} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 453
    :sswitch_2eb
    :try_start_2eb
    sget-object v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    const-string v1, "Got a camera upload conflict."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/service/CameraUploadService;->b(Landroid/content/Context;)V
    :try_end_2f7
    .catchall {:try_start_2eb .. :try_end_2f7} :catchall_2f9

    goto/16 :goto_206

    .line 494
    :catchall_2f9
    move-exception v0

    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    throw v0

    .line 457
    :sswitch_2fe
    :try_start_2fe
    iget v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->e:I
    :try_end_304
    .catchall {:try_start_2fe .. :try_end_304} :catchall_2f9

    .line 459
    :try_start_304
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;
    :try_end_30b
    .catchall {:try_start_304 .. :try_end_30b} :catchall_2f9
    .catch Ldbxyzptlk/o/a; {:try_start_304 .. :try_end_30b} :catch_320

    .line 465
    :try_start_30b
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    sget-object v1, Lcom/dropbox/android/util/aC;->b:Lcom/dropbox/android/util/aC;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z

    .line 467
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->k:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_31a
    .catchall {:try_start_30b .. :try_end_31a} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 460
    :catch_320
    move-exception v0

    .line 463
    :try_start_321
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_326
    .catchall {:try_start_321 .. :try_end_326} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 470
    :sswitch_32c
    :try_start_32c
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_331
    .catchall {:try_start_32c .. :try_end_331} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 475
    :catch_337
    move-exception v0

    .line 476
    :try_start_338
    invoke-virtual {v0}, Ldbxyzptlk/o/f;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "5xx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_34f

    .line 477
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_349
    .catchall {:try_start_338 .. :try_end_349} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 479
    :cond_34f
    :try_start_34f
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception uploading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 481
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->d:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_37b
    .catchall {:try_start_34f .. :try_end_37b} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 483
    :catch_381
    move-exception v0

    .line 484
    :try_start_382
    sget-object v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception uploading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 486
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_3ae
    .catchall {:try_start_382 .. :try_end_3ae} :catchall_2f9

    move-result-object v0

    .line 494
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_1e

    .line 440
    :catch_3b4
    move-exception v1

    goto/16 :goto_2d0

    .line 403
    :catch_3b7
    move-exception v0

    goto/16 :goto_206

    .line 425
    :sswitch_data_3ba
    .sparse-switch
        0x199 -> :sswitch_2eb
        0x19c -> :sswitch_289
        0x1f6 -> :sswitch_32c
        0x1f7 -> :sswitch_32c
        0x1fb -> :sswitch_2fe
    .end sparse-switch
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 60
    check-cast p1, Lcom/dropbox/android/taskqueue/DbTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .registers 3

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cameraupload~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ldbxyzptlk/k/i;
    .registers 4

    .prologue
    .line 595
    new-instance v0, Ldbxyzptlk/k/i;

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/k/i;-><init>(J)V

    return-object v0
.end method

.method protected final f()J
    .registers 3

    .prologue
    .line 233
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->r:J

    return-wide v0
.end method

.method public final g()Lcom/dropbox/android/taskqueue/m;
    .registers 2

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->t()V

    .line 551
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->g()Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 586
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 590
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j_()Lcom/dropbox/android/taskqueue/o;
    .registers 6

    .prologue
    .line 116
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Ldbxyzptlk/l/m;->g()Z

    move-result v1

    if-nez v1, :cond_d

    .line 118
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->f:Lcom/dropbox/android/taskqueue/o;

    .line 135
    :cond_c
    :goto_c
    return-object v0

    .line 121
    :cond_d
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->a(Ldbxyzptlk/l/m;Lcom/dropbox/android/service/x;)Lcom/dropbox/android/taskqueue/o;

    move-result-object v0

    .line 123
    sget-object v1, Lcom/dropbox/android/taskqueue/o;->a:Lcom/dropbox/android/taskqueue/o;

    if-ne v0, v1, :cond_c

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/util/z;->a(Landroid/content/Context;)Lcom/dropbox/android/util/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/D;->a()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 128
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    sget-object v1, Lcom/dropbox/android/util/aC;->d:Lcom/dropbox/android/util/aC;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z

    .line 130
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->e:Lcom/dropbox/android/taskqueue/o;

    goto :goto_c

    .line 132
    :cond_36
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->c:Landroid/content/Context;

    sget-object v1, Lcom/dropbox/android/util/aC;->d:Lcom/dropbox/android/util/aC;

    invoke-static {v0, v1}, Lcom/dropbox/android/service/NotificationService;->b(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 135
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->a:Lcom/dropbox/android/taskqueue/o;

    goto :goto_c
.end method

.method public final k()Ljava/lang/String;
    .registers 5

    .prologue
    .line 607
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 608
    const-string v1, "mFilePath"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    const-string v1, "mServerHash"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    const-string v1, "mMimeType"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    const-string v1, "mDbRowId"

    iget-wide v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    const-string v1, "mBatchFileNumber"

    iget v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 613
    const-string v1, "mContentUri"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->l:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    const-string v1, "mImportTime"

    iget-wide v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->o:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    const-string v1, "mImportTimeoffset"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->p:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 616
    const-string v1, "mRehashed"

    iget-boolean v2, p0, Lcom/dropbox/android/taskqueue/CameraUploadTask;->q:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    invoke-static {v0}, Ldbxyzptlk/D/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CameraUploadTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
