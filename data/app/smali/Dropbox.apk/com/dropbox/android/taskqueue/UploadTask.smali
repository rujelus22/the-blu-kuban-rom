.class public Lcom/dropbox/android/taskqueue/UploadTask;
.super Lcom/dropbox/android/taskqueue/DbTask;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private d:Lcom/dropbox/android/util/DropboxPath;

.field private final h:Landroid/net/Uri;

.field private final i:Ljava/lang/String;

.field private j:J

.field private final k:Z

.field private l:Ldbxyzptlk/r/C;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 67
    const-class v0, Lcom/dropbox/android/taskqueue/UploadTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 88
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/DbTask;-><init>()V

    .line 74
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    .line 77
    iput-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/r/C;

    .line 538
    iput-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ljava/lang/String;

    .line 91
    invoke-static {p3}, Lcom/dropbox/android/util/ae;->r(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    .line 92
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    .line 94
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    .line 95
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->g:Landroid/net/Uri;

    .line 97
    iput-boolean p5, p0, Lcom/dropbox/android/taskqueue/UploadTask;->k:Z

    .line 98
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/FileInputStream;JZLjava/lang/String;)Ldbxyzptlk/r/C;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 162
    new-instance v8, Lcom/dropbox/android/taskqueue/X;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/dropbox/android/taskqueue/X;-><init>(Lcom/dropbox/android/taskqueue/UploadTask;)V

    .line 168
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v3

    iget-object v3, v3, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    .line 169
    const-wide/32 v4, 0x800000

    cmp-long v4, p3, v4

    if-lez v4, :cond_29

    .line 170
    move-object/from16 v0, p2

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2, v8}, Ldbxyzptlk/r/a;->a(Ljava/io/FileInputStream;JLdbxyzptlk/n/t;)Ldbxyzptlk/r/g;

    move-result-object v8

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-virtual/range {v3 .. v8}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/io/FileInputStream;ZLjava/lang/String;Ldbxyzptlk/r/g;)Ldbxyzptlk/r/C;

    move-result-object v3

    .line 175
    :goto_28
    return-object v3

    .line 172
    :cond_29
    if-eqz p5, :cond_3c

    .line 173
    new-instance v9, Ldbxyzptlk/r/z;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/io/InputStream;JLdbxyzptlk/n/t;)Ldbxyzptlk/n/p;

    move-result-object v3

    invoke-direct {v9, v3}, Ldbxyzptlk/r/z;-><init>(Ldbxyzptlk/n/p;)V

    move-object v3, v9

    goto :goto_28

    .line 175
    :cond_3c
    new-instance v4, Ldbxyzptlk/r/z;

    move-object v9, v3

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-wide/from16 v12, p3

    move-object/from16 v14, p6

    move-object v15, v8

    invoke-virtual/range {v9 .. v15}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/n/p;

    move-result-object v3

    invoke-direct {v4, v3}, Ldbxyzptlk/r/z;-><init>(Ldbxyzptlk/n/p;)V

    move-object v3, v4

    goto :goto_28
.end method

.method private a(J)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 145
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    invoke-static {v1}, Ldbxyzptlk/l/m;->b(Landroid/content/Context;)Ldbxyzptlk/l/m;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;

    move-result-object v1

    .line 146
    if-nez v1, :cond_e

    .line 151
    :cond_d
    :goto_d
    return v0

    :cond_e
    iget-wide v2, v1, Ldbxyzptlk/n/c;->e:J

    add-long/2addr v2, p1

    iget-wide v4, v1, Ldbxyzptlk/n/c;->d:J

    add-long/2addr v2, v4

    iget-wide v4, v1, Ldbxyzptlk/n/c;->c:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_d

    const/4 v0, 0x0

    goto :goto_d
.end method

.method public static restore(Landroid/content/Context;JLjava/lang/String;)Lcom/dropbox/android/taskqueue/UploadTask;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 499
    new-instance v1, Ldbxyzptlk/E/b;

    invoke-direct {v1}, Ldbxyzptlk/E/b;-><init>()V

    .line 502
    :try_start_5
    invoke-virtual {v1, p3}, Ldbxyzptlk/E/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ldbxyzptlk/D/c;

    move-object v2, v0

    .line 504
    const-string v1, "mLocalUri"

    invoke-virtual {v2, v1}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 505
    const-string v1, "mDestinationFilename"

    invoke-virtual {v2, v1}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 513
    if-nez v1, :cond_4c

    .line 514
    invoke-static {v4}, Lcom/dropbox/android/util/ae;->r(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 516
    :goto_27
    new-instance v1, Lcom/dropbox/android/taskqueue/UploadTask;

    const-string v3, "mDropboxDir"

    invoke-virtual {v2, v3}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v6, "mOverwrite"

    invoke-virtual {v2, v6}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/dropbox/android/taskqueue/UploadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 522
    invoke-virtual {v1, p1, p2}, Lcom/dropbox/android/taskqueue/UploadTask;->b(J)V
    :try_end_44
    .catch Ldbxyzptlk/E/c; {:try_start_5 .. :try_end_44} :catch_45

    .line 523
    return-object v1

    .line 524
    :catch_45
    move-exception v1

    .line 525
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_4c
    move-object v5, v1

    goto :goto_27
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 477
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/taskqueue/DbTask;->a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V

    .line 478
    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/m;->b()Lcom/dropbox/android/taskqueue/n;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/n;->b:Lcom/dropbox/android/taskqueue/n;

    if-ne v0, v1, :cond_33

    sget-object v0, Lcom/dropbox/android/taskqueue/m;->j:Lcom/dropbox/android/taskqueue/m;

    if-eq p2, v0, :cond_33

    .line 479
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 480
    const-string v1, "ARG_FILENAME"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const-string v1, "ARG_STATUS"

    invoke-virtual {p2}, Lcom/dropbox/android/taskqueue/m;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string v1, "ARG_INTENDED_FOLDER"

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->i()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 483
    sget-object v1, Lcom/dropbox/android/util/aA;->a:Lcom/dropbox/android/util/aA;

    const/4 v2, 0x0

    invoke-static {p1, v1, v2, v0}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aA;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 485
    :cond_33
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 136
    monitor-enter p0

    .line 137
    :try_start_1
    invoke-super {p0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    .line 138
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/r/C;

    if-eqz v0, :cond_d

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/r/C;

    invoke-interface {v0}, Ldbxyzptlk/r/C;->a()V

    .line 141
    :cond_d
    monitor-exit p0

    .line 142
    return-void

    .line 141
    :catchall_f
    move-exception v0

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final c()Lcom/dropbox/android/taskqueue/m;
    .registers 18

    .prologue
    .line 180
    invoke-super/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/DbTask;->c()Lcom/dropbox/android/taskqueue/m;

    .line 182
    new-instance v14, Lcom/dropbox/android/taskqueue/G;

    invoke-direct {v14}, Lcom/dropbox/android/taskqueue/G;-><init>()V

    .line 184
    :try_start_8
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->a()V

    .line 186
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uploading file from URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->s()Ljava/lang/String;

    move-result-object v15

    .line 190
    const/4 v3, 0x0

    .line 193
    const/4 v11, 0x0

    .line 194
    const/4 v2, 0x0

    .line 195
    const/4 v4, 0x0

    .line 196
    const/4 v1, 0x0

    .line 199
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    .line 200
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    .line 201
    if-eqz v7, :cond_188

    if-eqz v5, :cond_188

    const-string v6, "content"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_188

    const-string v6, "com.dropbox.android.Dropbox"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_188

    const/4 v5, 0x1

    move v6, v5

    .line 203
    :goto_56
    if-eqz v7, :cond_18c

    const-string v5, "file"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/dropbox/android/util/ae;->s(Ljava/lang/String;)Z
    :try_end_6b
    .catchall {:try_start_8 .. :try_end_6b} :catchall_556

    move-result v5

    if-eqz v5, :cond_18c

    const/4 v5, 0x1

    .line 205
    :goto_6f
    if-nez v6, :cond_73

    if-eqz v5, :cond_5de

    .line 207
    :cond_73
    :try_start_73
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v5}, Lcom/dropbox/android/util/ae;->d(Landroid/net/Uri;)Ljava/io/File;
    :try_end_7a
    .catchall {:try_start_73 .. :try_end_7a} :catchall_556
    .catch Ljava/io/FileNotFoundException; {:try_start_73 .. :try_end_7a} :catch_192
    .catch Ljava/lang/SecurityException; {:try_start_73 .. :try_end_7a} :catch_19e

    move-result-object v13

    .line 208
    if-eqz v13, :cond_5da

    .line 209
    :try_start_7d
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_82
    .catchall {:try_start_7d .. :try_end_82} :catchall_556
    .catch Ljava/io/FileNotFoundException; {:try_start_7d .. :try_end_82} :catch_5a2
    .catch Ljava/lang/SecurityException; {:try_start_7d .. :try_end_82} :catch_19e

    .line 210
    :try_start_82
    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    .line 211
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_18f

    invoke-static {v13}, Lcom/dropbox/android/util/ae;->a(Ljava/io/File;)Z
    :try_end_93
    .catchall {:try_start_82 .. :try_end_93} :catchall_556
    .catch Ljava/io/FileNotFoundException; {:try_start_82 .. :try_end_93} :catch_5a9
    .catch Ljava/lang/SecurityException; {:try_start_82 .. :try_end_93} :catch_19e

    move-result v1

    if-eqz v1, :cond_18f

    const/4 v9, 0x1

    .line 212
    :goto_97
    if-eqz v9, :cond_5d5

    .line 213
    :try_start_99
    invoke-static {v15}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_9c
    .catchall {:try_start_99 .. :try_end_9c} :catchall_556
    .catch Ljava/io/FileNotFoundException; {:try_start_99 .. :try_end_9c} :catch_5af
    .catch Ljava/lang/SecurityException; {:try_start_99 .. :try_end_9c} :catch_19e

    move-result-object v10

    .line 214
    :try_start_9d
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 215
    if-nez v11, :cond_5cf

    .line 216
    const-string v2, "dropbox"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "local_revision"

    aput-object v5, v3, v4

    const-string v4, "canon_path = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v10, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 221
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5cc

    .line 222
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_ca
    .catchall {:try_start_9d .. :try_end_ca} :catchall_556
    .catch Ljava/io/FileNotFoundException; {:try_start_9d .. :try_end_ca} :catch_5b4
    .catch Ljava/lang/SecurityException; {:try_start_9d .. :try_end_ca} :catch_19e

    move-result-object v1

    .line 224
    :goto_cb
    :try_start_cb
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_ce
    .catchall {:try_start_cb .. :try_end_ce} :catchall_556
    .catch Ljava/io/FileNotFoundException; {:try_start_cb .. :try_end_ce} :catch_5b8
    .catch Ljava/lang/SecurityException; {:try_start_cb .. :try_end_ce} :catch_19e

    move-object v2, v10

    move-object v3, v1

    move-object v4, v12

    move v1, v9

    :goto_d2
    move v10, v1

    move-object v8, v13

    move-object v11, v2

    move-object v7, v3

    move-object v2, v4

    .line 235
    :goto_d7
    const/4 v1, 0x0

    .line 236
    if-nez v2, :cond_5c9

    :try_start_da
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/dropbox/android/util/ae;->a(Landroid/net/Uri;Landroid/content/Context;)Z
    :try_end_e5
    .catchall {:try_start_da .. :try_end_e5} :catchall_556

    move-result v3

    if-nez v3, :cond_5c9

    .line 238
    :try_start_e8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    const-string v5, "r"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v4

    .line 239
    if-eqz v4, :cond_163

    .line 240
    sget-object v3, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AssetFileDescriptor: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", length = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v12

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_129
    .catchall {:try_start_e8 .. :try_end_129} :catchall_556
    .catch Ljava/io/IOException; {:try_start_e8 .. :try_end_129} :catch_57b
    .catch Ljava/lang/NullPointerException; {:try_start_e8 .. :try_end_129} :catch_1db
    .catch Ljava/lang/SecurityException; {:try_start_e8 .. :try_end_129} :catch_1de

    move-result-object v3

    .line 242
    :try_start_12a
    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    .line 243
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const-wide/16 v12, -0x1

    cmp-long v2, v4, v12

    if-nez v2, :cond_5c6

    .line 246
    invoke-static {}, Lcom/dropbox/android/util/af;->a()Ljava/io/File;
    :try_end_13f
    .catchall {:try_start_12a .. :try_end_13f} :catchall_556
    .catch Ljava/io/IOException; {:try_start_12a .. :try_end_13f} :catch_57e
    .catch Ljava/lang/NullPointerException; {:try_start_12a .. :try_end_13f} :catch_562
    .catch Ljava/lang/SecurityException; {:try_start_12a .. :try_end_13f} :catch_1de

    move-result-object v2

    .line 247
    if-eqz v2, :cond_1ce

    .line 248
    const/4 v1, 0x0

    .line 250
    :try_start_143
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_148
    .catchall {:try_start_143 .. :try_end_148} :catchall_1c0
    .catch Ljava/io/IOException; {:try_start_143 .. :try_end_148} :catch_1ab

    .line 251
    :try_start_148
    invoke-static {v3, v4}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 252
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 253
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_153
    .catchall {:try_start_148 .. :try_end_153} :catchall_58a
    .catch Ljava/io/IOException; {:try_start_148 .. :try_end_153} :catch_59e

    .line 254
    :try_start_153
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_15b
    .catchall {:try_start_153 .. :try_end_15b} :catchall_58d
    .catch Ljava/io/IOException; {:try_start_153 .. :try_end_15b} :catch_59e

    .line 259
    :try_start_15b
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V
    :try_end_15e
    .catchall {:try_start_15b .. :try_end_15e} :catchall_556
    .catch Ljava/io/IOException; {:try_start_15b .. :try_end_15e} :catch_582
    .catch Ljava/lang/NullPointerException; {:try_start_15b .. :try_end_15e} :catch_566
    .catch Ljava/lang/SecurityException; {:try_start_15b .. :try_end_15e} :catch_1de

    :goto_15e
    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    :cond_163
    :goto_163
    move-object v9, v1

    .line 277
    :goto_164
    if-nez v2, :cond_5c3

    .line 279
    :try_start_166
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->d(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 280
    if-eqz v3, :cond_5c0

    .line 281
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_17c

    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_1eb

    .line 282
    :cond_17c
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_183
    .catchall {:try_start_166 .. :try_end_183} :catchall_556
    .catch Ljava/io/IOException; {:try_start_166 .. :try_end_183} :catch_212
    .catch Ljava/lang/SecurityException; {:try_start_166 .. :try_end_183} :catch_215

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    :goto_187
    return-object v1

    .line 201
    :cond_188
    const/4 v5, 0x0

    move v6, v5

    goto/16 :goto_56

    .line 203
    :cond_18c
    const/4 v5, 0x0

    goto/16 :goto_6f

    .line 211
    :cond_18f
    const/4 v9, 0x0

    goto/16 :goto_97

    .line 228
    :catch_192
    move-exception v5

    move v9, v1

    move-object v10, v2

    move-object v12, v3

    move-object v1, v4

    :goto_197
    move-object v8, v1

    move-object v7, v11

    move-object v2, v12

    move-object v11, v10

    move v10, v9

    .line 231
    goto/16 :goto_d7

    .line 229
    :catch_19e
    move-exception v1

    .line 230
    :try_start_19f
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->f:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_1a6
    .catchall {:try_start_19f .. :try_end_1a6} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto :goto_187

    .line 255
    :catch_1ab
    move-exception v3

    move-object v3, v1

    .line 256
    :goto_1ad
    const/4 v1, 0x0

    .line 257
    const-wide/16 v4, -0x1

    :try_start_1b0
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_1b4
    .catchall {:try_start_1b0 .. :try_end_1b4} :catchall_595

    .line 259
    :try_start_1b4
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V
    :try_end_1b7
    .catchall {:try_start_1b4 .. :try_end_1b7} :catchall_556
    .catch Ljava/io/IOException; {:try_start_1b4 .. :try_end_1b7} :catch_1b8
    .catch Ljava/lang/NullPointerException; {:try_start_1b4 .. :try_end_1b7} :catch_56e
    .catch Ljava/lang/SecurityException; {:try_start_1b4 .. :try_end_1b7} :catch_1de

    goto :goto_15e

    .line 267
    :catch_1b8
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    :goto_1be
    move-object v9, v1

    .line 273
    goto :goto_164

    .line 259
    :catchall_1c0
    move-exception v4

    move-object/from16 v16, v4

    move-object v4, v1

    move-object/from16 v1, v16

    :goto_1c6
    :try_start_1c6
    invoke-static {v4}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    throw v1
    :try_end_1ca
    .catchall {:try_start_1c6 .. :try_end_1ca} :catchall_556
    .catch Ljava/io/IOException; {:try_start_1c6 .. :try_end_1ca} :catch_1ca
    .catch Ljava/lang/NullPointerException; {:try_start_1c6 .. :try_end_1ca} :catch_576
    .catch Ljava/lang/SecurityException; {:try_start_1c6 .. :try_end_1ca} :catch_1de

    .line 267
    :catch_1ca
    move-exception v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1be

    .line 262
    :cond_1ce
    const/4 v1, 0x0

    .line 263
    const-wide/16 v3, -0x1

    :try_start_1d1
    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_1d5
    .catchall {:try_start_1d1 .. :try_end_1d5} :catchall_556
    .catch Ljava/io/IOException; {:try_start_1d1 .. :try_end_1d5} :catch_1b8
    .catch Ljava/lang/NullPointerException; {:try_start_1d1 .. :try_end_1d5} :catch_56e
    .catch Ljava/lang/SecurityException; {:try_start_1d1 .. :try_end_1d5} :catch_1de

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto :goto_163

    .line 268
    :catch_1db
    move-exception v3

    :goto_1dc
    move-object v9, v1

    .line 273
    goto :goto_164

    .line 271
    :catch_1de
    move-exception v1

    .line 272
    :try_start_1df
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->f:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_1e6
    .catchall {:try_start_1df .. :try_end_1e6} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto :goto_187

    .line 285
    :cond_1eb
    :try_start_1eb
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1f0
    .catchall {:try_start_1eb .. :try_end_1f0} :catchall_556
    .catch Ljava/io/IOException; {:try_start_1eb .. :try_end_1f0} :catch_212
    .catch Ljava/lang/SecurityException; {:try_start_1eb .. :try_end_1f0} :catch_215

    .line 286
    :try_start_1f0
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J
    :try_end_1f8
    .catchall {:try_start_1f0 .. :try_end_1f8} :catchall_556
    .catch Ljava/io/IOException; {:try_start_1f0 .. :try_end_1f8} :catch_55e
    .catch Ljava/lang/SecurityException; {:try_start_1f0 .. :try_end_1f8} :catch_215

    :goto_1f8
    move-object v3, v1

    .line 295
    :goto_1f9
    if-eqz v3, :cond_205

    :try_start_1fb
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-gez v1, :cond_223

    .line 296
    :cond_205
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_20c
    .catchall {:try_start_1fb .. :try_end_20c} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 288
    :catch_212
    move-exception v1

    :goto_213
    move-object v3, v2

    .line 291
    goto :goto_1f9

    .line 289
    :catch_215
    move-exception v1

    .line 290
    :try_start_216
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->f:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_21d
    .catchall {:try_start_216 .. :try_end_21d} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 300
    :cond_223
    :try_start_223
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/UploadTask;->a(J)Z
    :try_end_22c
    .catchall {:try_start_223 .. :try_end_22c} :catchall_556

    move-result v1

    if-nez v1, :cond_27c

    .line 303
    :try_start_22f
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;

    .line 304
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/taskqueue/UploadTask;->a(J)Z

    move-result v1

    if-nez v1, :cond_27c

    .line 305
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:I

    .line 306
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    sget-object v2, Lcom/dropbox/android/util/aC;->g:Lcom/dropbox/android/util/aC;

    invoke-static {v1, v2}, Lcom/dropbox/android/service/NotificationService;->b(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    sget-object v2, Lcom/dropbox/android/util/aC;->c:Lcom/dropbox/android/util/aC;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z

    .line 309
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->k:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_268
    .catchall {:try_start_22f .. :try_end_268} :catchall_556
    .catch Ldbxyzptlk/o/a; {:try_start_22f .. :try_end_268} :catch_26e

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 312
    :catch_26e
    move-exception v1

    .line 314
    :try_start_26f
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_276
    .catchall {:try_start_26f .. :try_end_276} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 317
    :cond_27c
    :try_start_27c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    sget-object v2, Lcom/dropbox/android/util/aC;->c:Lcom/dropbox/android/util/aC;

    invoke-static {v1, v2}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;)V

    .line 319
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->n()Z

    move-result v1

    if-eqz v1, :cond_294

    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->r()Lcom/dropbox/android/taskqueue/m;
    :try_end_28e
    .catchall {:try_start_27c .. :try_end_28e} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 323
    :cond_294
    :try_start_294
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->q()V

    .line 325
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uploading uri "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2c1
    .catchall {:try_start_294 .. :try_end_2c1} :catchall_556

    .line 329
    :try_start_2c1
    monitor-enter p0
    :try_end_2c2
    .catchall {:try_start_2c1 .. :try_end_2c2} :catchall_556
    .catch Ljava/io/IOException; {:try_start_2c1 .. :try_end_2c2} :catch_302
    .catch Ldbxyzptlk/o/d; {:try_start_2c1 .. :try_end_2c2} :catch_431
    .catch Ldbxyzptlk/o/g; {:try_start_2c1 .. :try_end_2c2} :catch_45f
    .catch Ldbxyzptlk/o/j; {:try_start_2c1 .. :try_end_2c2} :catch_470
    .catch Ldbxyzptlk/o/i; {:try_start_2c1 .. :try_end_2c2} :catch_481
    .catch Ldbxyzptlk/o/f; {:try_start_2c1 .. :try_end_2c2} :catch_4e9
    .catch Ldbxyzptlk/o/a; {:try_start_2c1 .. :try_end_2c2} :catch_519

    .line 330
    :try_start_2c2
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->n()Z

    move-result v1

    if-eqz v1, :cond_2d2

    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->r()Lcom/dropbox/android/taskqueue/m;

    move-result-object v1

    monitor-exit p0
    :try_end_2cd
    .catchall {:try_start_2c2 .. :try_end_2cd} :catchall_2ff

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 333
    :cond_2d2
    :try_start_2d2
    monitor-exit p0
    :try_end_2d3
    .catchall {:try_start_2d2 .. :try_end_2d3} :catchall_2ff

    .line 337
    :try_start_2d3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->k:Z

    if-nez v1, :cond_2e1

    if-eqz v10, :cond_317

    invoke-static {v7}, Lcom/dropbox/android/util/aT;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_317

    .line 342
    :cond_2e1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v1, p0

    move-object v2, v15

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Ljava/lang/String;Ljava/io/FileInputStream;JZLjava/lang/String;)Ldbxyzptlk/r/C;

    move-result-object v1

    .line 350
    :goto_2ee
    monitor-enter p0
    :try_end_2ef
    .catchall {:try_start_2d3 .. :try_end_2ef} :catchall_556
    .catch Ljava/io/IOException; {:try_start_2d3 .. :try_end_2ef} :catch_302
    .catch Ldbxyzptlk/o/d; {:try_start_2d3 .. :try_end_2ef} :catch_431
    .catch Ldbxyzptlk/o/g; {:try_start_2d3 .. :try_end_2ef} :catch_45f
    .catch Ldbxyzptlk/o/j; {:try_start_2d3 .. :try_end_2ef} :catch_470
    .catch Ldbxyzptlk/o/i; {:try_start_2d3 .. :try_end_2ef} :catch_481
    .catch Ldbxyzptlk/o/f; {:try_start_2d3 .. :try_end_2ef} :catch_4e9
    .catch Ldbxyzptlk/o/a; {:try_start_2d3 .. :try_end_2ef} :catch_519

    .line 351
    :try_start_2ef
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->n()Z

    move-result v2

    if-eqz v2, :cond_324

    .line 352
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->r()Lcom/dropbox/android/taskqueue/m;

    move-result-object v1

    monitor-exit p0
    :try_end_2fa
    .catchall {:try_start_2ef .. :try_end_2fa} :catchall_42e

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 333
    :catchall_2ff
    move-exception v1

    :try_start_300
    monitor-exit p0
    :try_end_301
    .catchall {:try_start_300 .. :try_end_301} :catchall_2ff

    :try_start_301
    throw v1
    :try_end_302
    .catchall {:try_start_301 .. :try_end_302} :catchall_556
    .catch Ljava/io/IOException; {:try_start_301 .. :try_end_302} :catch_302
    .catch Ldbxyzptlk/o/d; {:try_start_301 .. :try_end_302} :catch_431
    .catch Ldbxyzptlk/o/g; {:try_start_301 .. :try_end_302} :catch_45f
    .catch Ldbxyzptlk/o/j; {:try_start_301 .. :try_end_302} :catch_470
    .catch Ldbxyzptlk/o/i; {:try_start_301 .. :try_end_302} :catch_481
    .catch Ldbxyzptlk/o/f; {:try_start_301 .. :try_end_302} :catch_4e9
    .catch Ldbxyzptlk/o/a; {:try_start_301 .. :try_end_302} :catch_519

    .line 360
    :catch_302
    move-exception v1

    .line 361
    :try_start_303
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    const-string v3, "IO exception while starting upload"

    invoke-static {v2, v3, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_311
    .catchall {:try_start_303 .. :try_end_311} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 347
    :cond_317
    :try_start_317
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object v2, v15

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Ljava/lang/String;Ljava/io/FileInputStream;JZLjava/lang/String;)Ldbxyzptlk/r/C;
    :try_end_322
    .catchall {:try_start_317 .. :try_end_322} :catchall_556
    .catch Ljava/io/IOException; {:try_start_317 .. :try_end_322} :catch_302
    .catch Ldbxyzptlk/o/d; {:try_start_317 .. :try_end_322} :catch_431
    .catch Ldbxyzptlk/o/g; {:try_start_317 .. :try_end_322} :catch_45f
    .catch Ldbxyzptlk/o/j; {:try_start_317 .. :try_end_322} :catch_470
    .catch Ldbxyzptlk/o/i; {:try_start_317 .. :try_end_322} :catch_481
    .catch Ldbxyzptlk/o/f; {:try_start_317 .. :try_end_322} :catch_4e9
    .catch Ldbxyzptlk/o/a; {:try_start_317 .. :try_end_322} :catch_519

    move-result-object v1

    goto :goto_2ee

    .line 354
    :cond_324
    :try_start_324
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/r/C;

    .line 355
    monitor-exit p0
    :try_end_329
    .catchall {:try_start_324 .. :try_end_329} :catchall_42e

    .line 357
    :try_start_329
    const-string v1, "net.start"

    move-object/from16 v0, p0

    invoke-static {v1, v0}, Lcom/dropbox/android/util/i;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    .line 358
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->l:Ldbxyzptlk/r/C;

    invoke-interface {v1}, Ldbxyzptlk/r/C;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldbxyzptlk/n/k;

    .line 359
    const-string v2, "net.end"

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/dropbox/android/util/i;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V
    :try_end_349
    .catchall {:try_start_329 .. :try_end_349} :catchall_556
    .catch Ljava/io/IOException; {:try_start_329 .. :try_end_349} :catch_302
    .catch Ldbxyzptlk/o/d; {:try_start_329 .. :try_end_349} :catch_431
    .catch Ldbxyzptlk/o/g; {:try_start_329 .. :try_end_349} :catch_45f
    .catch Ldbxyzptlk/o/j; {:try_start_329 .. :try_end_349} :catch_470
    .catch Ldbxyzptlk/o/i; {:try_start_329 .. :try_end_349} :catch_481
    .catch Ldbxyzptlk/o/f; {:try_start_329 .. :try_end_349} :catch_4e9
    .catch Ldbxyzptlk/o/a; {:try_start_329 .. :try_end_349} :catch_519

    .line 408
    :try_start_349
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_34c
    .catchall {:try_start_349 .. :try_end_34c} :catchall_556
    .catch Ljava/io/IOException; {:try_start_349 .. :try_end_34c} :catch_55b

    .line 411
    :goto_34c
    if-eqz v9, :cond_351

    .line 412
    :try_start_34e
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 418
    :cond_351
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, v1, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->d:Lcom/dropbox/android/util/DropboxPath;

    .line 419
    if-eqz v10, :cond_5bd

    .line 420
    new-instance v2, Lcom/dropbox/android/util/at;

    invoke-direct {v2, v8}, Lcom/dropbox/android/util/at;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/at;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    .line 421
    iget-object v3, v1, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5bd

    .line 423
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->d:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/at;->a()Ljava/io/File;

    move-result-object v2

    .line 424
    invoke-virtual {v8, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_550

    .line 431
    :goto_385
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 432
    const-string v4, "_data"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 433
    const-string v4, "local_hash"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 434
    const-string v4, "local_modified"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 435
    const-string v4, "local_revision"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 436
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 437
    const-string v5, "path"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3b9

    .line 438
    const-string v5, "Entries.PATH"

    const-string v6, "path"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_3b9
    const-string v5, "dropbox"

    const-string v6, "canon_path = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v11, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 448
    :goto_3c6
    invoke-static {v1}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ldbxyzptlk/n/k;)Landroid/content/ContentValues;

    move-result-object v3

    .line 449
    if-eqz v10, :cond_410

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_410

    .line 450
    const-string v4, "local_revision"

    iget-object v1, v1, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    const-string v1, "_data"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 453
    const-string v1, "local_modified"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 454
    const-string v1, "local_hash"

    invoke-static {v2}, Lcom/dropbox/android/util/af;->c(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Uploaded file modified at: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :cond_410
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->d:Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    .line 459
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 461
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->g()Lcom/dropbox/android/taskqueue/m;
    :try_end_428
    .catchall {:try_start_34e .. :try_end_428} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 355
    :catchall_42e
    move-exception v1

    :try_start_42f
    monitor-exit p0
    :try_end_430
    .catchall {:try_start_42f .. :try_end_430} :catchall_42e

    :try_start_430
    throw v1
    :try_end_431
    .catchall {:try_start_430 .. :try_end_431} :catchall_556
    .catch Ljava/io/IOException; {:try_start_430 .. :try_end_431} :catch_302
    .catch Ldbxyzptlk/o/d; {:try_start_430 .. :try_end_431} :catch_431
    .catch Ldbxyzptlk/o/g; {:try_start_430 .. :try_end_431} :catch_45f
    .catch Ldbxyzptlk/o/j; {:try_start_430 .. :try_end_431} :catch_470
    .catch Ldbxyzptlk/o/i; {:try_start_430 .. :try_end_431} :catch_481
    .catch Ldbxyzptlk/o/f; {:try_start_430 .. :try_end_431} :catch_4e9
    .catch Ldbxyzptlk/o/a; {:try_start_430 .. :try_end_431} :catch_519

    .line 363
    :catch_431
    move-exception v1

    .line 364
    :try_start_432
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IO Exception uploading: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v4}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 365
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_459
    .catchall {:try_start_432 .. :try_end_459} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 366
    :catch_45f
    move-exception v1

    .line 367
    :try_start_460
    sget-object v1, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    const-string v2, "Upload canceled"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/taskqueue/UploadTask;->r()Lcom/dropbox/android/taskqueue/m;
    :try_end_46a
    .catchall {:try_start_460 .. :try_end_46a} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 369
    :catch_470
    move-exception v1

    .line 370
    :try_start_471
    invoke-static {}, Lcom/dropbox/android/util/a;->a()V

    .line 371
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_47b
    .catchall {:try_start_471 .. :try_end_47b} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 372
    :catch_481
    move-exception v1

    .line 373
    :try_start_482
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    const-string v3, "Server exception uploading."

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    iget v2, v1, Ldbxyzptlk/o/i;->b:I

    packed-switch v2, :pswitch_data_5e6

    .line 391
    :pswitch_48e
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v1, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 392
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->d:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_49e
    .catchall {:try_start_482 .. :try_end_49e} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 376
    :pswitch_4a4
    :try_start_4a4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->e:I
    :try_end_4ae
    .catchall {:try_start_4a4 .. :try_end_4ae} :catchall_556

    .line 378
    :try_start_4ae
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;
    :try_end_4b5
    .catchall {:try_start_4ae .. :try_end_4b5} :catchall_556
    .catch Ldbxyzptlk/o/a; {:try_start_4ae .. :try_end_4b5} :catch_4ce

    .line 384
    :try_start_4b5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    sget-object v2, Lcom/dropbox/android/util/aC;->c:Lcom/dropbox/android/util/aC;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z

    .line 386
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->k:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_4c8
    .catchall {:try_start_4b5 .. :try_end_4c8} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 379
    :catch_4ce
    move-exception v1

    .line 382
    :try_start_4cf
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_4d6
    .catchall {:try_start_4cf .. :try_end_4d6} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 389
    :pswitch_4dc
    :try_start_4dc
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_4e3
    .catchall {:try_start_4dc .. :try_end_4e3} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 394
    :catch_4e9
    move-exception v1

    .line 395
    :try_start_4ea
    invoke-virtual {v1}, Ldbxyzptlk/o/f;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "5xx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_503

    .line 396
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_4fd
    .catchall {:try_start_4ea .. :try_end_4fd} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 398
    :cond_503
    :try_start_503
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v1, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 399
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->d:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_513
    .catchall {:try_start_503 .. :try_end_513} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 401
    :catch_519
    move-exception v1

    .line 402
    :try_start_51a
    sget-object v2, Lcom/dropbox/android/taskqueue/UploadTask;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception uploading: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-static {v4}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v1, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 404
    sget-object v1, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/UploadTask;->a(Lcom/dropbox/android/taskqueue/m;)Lcom/dropbox/android/taskqueue/m;
    :try_end_54a
    .catchall {:try_start_51a .. :try_end_54a} :catchall_556

    move-result-object v1

    .line 463
    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    goto/16 :goto_187

    .line 427
    :cond_550
    :try_start_550
    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_553
    .catchall {:try_start_550 .. :try_end_553} :catchall_556

    move-object v2, v8

    goto/16 :goto_385

    .line 463
    :catchall_556
    move-exception v1

    invoke-virtual {v14}, Lcom/dropbox/android/taskqueue/G;->b()V

    throw v1

    .line 409
    :catch_55b
    move-exception v2

    goto/16 :goto_34c

    .line 288
    :catch_55e
    move-exception v2

    move-object v2, v1

    goto/16 :goto_213

    .line 268
    :catch_562
    move-exception v2

    move-object v2, v3

    goto/16 :goto_1dc

    :catch_566
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto/16 :goto_1dc

    :catch_56e
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto/16 :goto_1dc

    :catch_576
    move-exception v1

    move-object v1, v2

    move-object v2, v3

    goto/16 :goto_1dc

    .line 267
    :catch_57b
    move-exception v3

    goto/16 :goto_1be

    :catch_57e
    move-exception v2

    move-object v2, v3

    goto/16 :goto_1be

    :catch_582
    move-exception v3

    move-object/from16 v16, v2

    move-object v2, v1

    move-object/from16 v1, v16

    goto/16 :goto_1be

    .line 259
    :catchall_58a
    move-exception v1

    goto/16 :goto_1c6

    :catchall_58d
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v1

    move-object/from16 v1, v16

    goto/16 :goto_1c6

    :catchall_595
    move-exception v4

    move-object/from16 v16, v4

    move-object v4, v3

    move-object v3, v1

    move-object/from16 v1, v16

    goto/16 :goto_1c6

    .line 255
    :catch_59e
    move-exception v1

    move-object v3, v4

    goto/16 :goto_1ad

    .line 228
    :catch_5a2
    move-exception v4

    move v9, v1

    move-object v10, v2

    move-object v12, v3

    move-object v1, v13

    goto/16 :goto_197

    :catch_5a9
    move-exception v3

    move v9, v1

    move-object v10, v2

    move-object v1, v13

    goto/16 :goto_197

    :catch_5af
    move-exception v1

    move-object v1, v13

    move-object v10, v2

    goto/16 :goto_197

    :catch_5b4
    move-exception v1

    move-object v1, v13

    goto/16 :goto_197

    :catch_5b8
    move-exception v2

    move-object v11, v1

    move-object v1, v13

    goto/16 :goto_197

    :cond_5bd
    move-object v2, v8

    goto/16 :goto_3c6

    :cond_5c0
    move-object v1, v2

    goto/16 :goto_1f8

    :cond_5c3
    move-object v3, v2

    goto/16 :goto_1f9

    :cond_5c6
    move-object v2, v3

    goto/16 :goto_163

    :cond_5c9
    move-object v9, v1

    goto/16 :goto_164

    :cond_5cc
    move-object v1, v11

    goto/16 :goto_cb

    :cond_5cf
    move v1, v9

    move-object v2, v10

    move-object v3, v11

    move-object v4, v12

    goto/16 :goto_d2

    :cond_5d5
    move v1, v9

    move-object v3, v11

    move-object v4, v12

    goto/16 :goto_d2

    :cond_5da
    move-object v4, v3

    move-object v3, v11

    goto/16 :goto_d2

    :cond_5de
    move v10, v1

    move-object v8, v4

    move-object v7, v11

    move-object v11, v2

    move-object v2, v3

    goto/16 :goto_d7

    .line 374
    nop

    :pswitch_data_5e6
    .packed-switch 0x1f6
        :pswitch_4dc
        :pswitch_4dc
        :pswitch_48e
        :pswitch_48e
        :pswitch_48e
        :pswitch_4a4
    .end packed-switch
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ldbxyzptlk/k/i;
    .registers 4

    .prologue
    .line 563
    new-instance v0, Ldbxyzptlk/k/i;

    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->s()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    return-object v0
.end method

.method protected final f()J
    .registers 3

    .prologue
    .line 157
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->j:J

    return-wide v0
.end method

.method public final h()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    return-object v0
.end method

.method public final i()Lcom/dropbox/android/util/DropboxPath;
    .registers 3

    .prologue
    .line 126
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .registers 4

    .prologue
    .line 548
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;

    if-nez v0, :cond_12

    .line 550
    :try_start_4
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;
    :try_end_12
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_12} :catch_15

    .line 558
    :cond_12
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->m:Ljava/lang/String;

    :goto_14
    return-object v0

    .line 551
    :catch_15
    move-exception v0

    .line 554
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 555
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public final j_()Lcom/dropbox/android/taskqueue/o;
    .registers 2

    .prologue
    .line 117
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/dropbox/android/service/x;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 119
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->a:Lcom/dropbox/android/taskqueue/o;

    .line 121
    :goto_10
    return-object v0

    :cond_11
    sget-object v0, Lcom/dropbox/android/taskqueue/o;->c:Lcom/dropbox/android/taskqueue/o;

    goto :goto_10
.end method

.method public final k()Ljava/lang/String;
    .registers 4

    .prologue
    .line 489
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 490
    const-string v1, "mDropboxDir"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    const-string v1, "mLocalUri"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->h:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    const-string v1, "mDestinationFilename"

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    const-string v1, "mOverwrite"

    iget-boolean v2, p0, Lcom/dropbox/android/taskqueue/UploadTask;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    invoke-static {v0}, Ldbxyzptlk/D/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .registers 3

    .prologue
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/dropbox/android/util/DropboxPath;
    .registers 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->d:Lcom/dropbox/android/util/DropboxPath;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UploadTask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UploadTask;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .registers 2

    .prologue
    .line 542
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/UploadTask;->i:Ljava/lang/String;

    return-object v0
.end method
