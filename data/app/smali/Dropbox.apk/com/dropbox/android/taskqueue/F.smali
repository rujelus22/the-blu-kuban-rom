.class public final Lcom/dropbox/android/taskqueue/F;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/util/HashMap;

.field private final b:J


# direct methods
.method public constructor <init>(J)V
    .registers 4
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/F;->a:Ljava/util/HashMap;

    .line 57
    iput-wide p1, p0, Lcom/dropbox/android/taskqueue/F;->b:J

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .registers 6
    .parameter

    .prologue
    .line 61
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/F;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 62
    :try_start_3
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/F;->a:Ljava/util/HashMap;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    monitor-exit v1

    .line 64
    return-void

    .line 63
    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    iget-object v3, p0, Lcom/dropbox/android/taskqueue/F;->a:Ljava/util/HashMap;

    monitor-enter v3

    .line 74
    :try_start_5
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/F;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 75
    if-nez v0, :cond_12

    .line 76
    monitor-exit v3

    move v0, v1

    .line 83
    :goto_11
    return v0

    .line 79
    :cond_12
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/dropbox/android/taskqueue/F;->b:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_31

    move v0, v2

    .line 80
    :goto_22
    if-eqz v0, :cond_29

    .line 81
    iget-object v4, p0, Lcom/dropbox/android/taskqueue/F;->a:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_29
    if-nez v0, :cond_33

    move v0, v2

    :goto_2c
    monitor-exit v3

    goto :goto_11

    .line 84
    :catchall_2e
    move-exception v0

    monitor-exit v3
    :try_end_30
    .catchall {:try_start_5 .. :try_end_30} :catchall_2e

    throw v0

    :cond_31
    move v0, v1

    .line 79
    goto :goto_22

    :cond_33
    move v0, v1

    .line 83
    goto :goto_2c
.end method
