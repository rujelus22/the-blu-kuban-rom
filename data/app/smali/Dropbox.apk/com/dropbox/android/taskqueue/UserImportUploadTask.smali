.class public Lcom/dropbox/android/taskqueue/UserImportUploadTask;
.super Lcom/dropbox/android/taskqueue/UploadTask;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct/range {p0 .. p5}, Lcom/dropbox/android/taskqueue/UploadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 16
    return-void
.end method

.method public static restore(Landroid/content/Context;JLjava/lang/String;)Lcom/dropbox/android/taskqueue/UserImportUploadTask;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 19
    new-instance v1, Ldbxyzptlk/E/b;

    invoke-direct {v1}, Ldbxyzptlk/E/b;-><init>()V

    .line 22
    :try_start_5
    invoke-virtual {v1, p3}, Ldbxyzptlk/E/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ldbxyzptlk/D/c;

    move-object v2, v0

    .line 24
    const-string v1, "mLocalUri"

    invoke-virtual {v2, v1}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 25
    const-string v1, "mDestinationFilename"

    invoke-virtual {v2, v1}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 33
    if-nez v1, :cond_4c

    .line 34
    invoke-static {v4}, Lcom/dropbox/android/util/ae;->r(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 36
    :goto_27
    new-instance v1, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    const-string v3, "mDropboxDir"

    invoke-virtual {v2, v3}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v6, "mOverwrite"

    invoke-virtual {v2, v6}, Ldbxyzptlk/D/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 42
    invoke-virtual {v1, p1, p2}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->b(J)V
    :try_end_44
    .catch Ldbxyzptlk/E/c; {:try_start_5 .. :try_end_44} :catch_45

    .line 43
    return-object v1

    .line 44
    :catch_45
    move-exception v1

    .line 45
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_4c
    move-object v5, v1

    goto :goto_27
.end method


# virtual methods
.method public final e()Ldbxyzptlk/k/i;
    .registers 4

    .prologue
    .line 51
    new-instance v0, Ldbxyzptlk/k/i;

    invoke-virtual {p0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->l()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/k/i;-><init>(J)V

    return-object v0
.end method
