.class public Lcom/dropbox/android/provider/DropboxProvider;
.super Landroid/content/ContentProvider;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/provider/F;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 35
    const-class v0, Lcom/dropbox/android/provider/DropboxProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 45
    return-void
.end method

.method public static a(Landroid/net/Uri;)Lcom/dropbox/android/provider/m;
    .registers 3
    .parameter

    .prologue
    .line 62
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 63
    const-string v1, "/metadata"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 64
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 65
    sget-object v0, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    .line 80
    :goto_1a
    return-object v0

    .line 67
    :cond_1b
    sget-object v0, Lcom/dropbox/android/provider/m;->a:Lcom/dropbox/android/provider/m;

    goto :goto_1a

    .line 69
    :cond_1e
    const-string v1, "/favorites"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 70
    sget-object v0, Lcom/dropbox/android/provider/m;->c:Lcom/dropbox/android/provider/m;

    goto :goto_1a

    .line 71
    :cond_29
    const-string v1, "/query_status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 72
    sget-object v0, Lcom/dropbox/android/provider/m;->f:Lcom/dropbox/android/provider/m;

    goto :goto_1a

    .line 73
    :cond_34
    const-string v1, "/search_suggest_query"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 74
    sget-object v0, Lcom/dropbox/android/provider/m;->e:Lcom/dropbox/android/provider/m;

    goto :goto_1a

    .line 75
    :cond_3f
    const-string v1, "/search"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 76
    sget-object v0, Lcom/dropbox/android/provider/m;->d:Lcom/dropbox/android/provider/m;

    goto :goto_1a

    .line 77
    :cond_4a
    const-string v1, "/clear_db"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 78
    sget-object v0, Lcom/dropbox/android/provider/m;->g:Lcom/dropbox/android/provider/m;

    goto :goto_1a

    .line 80
    :cond_55
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private static a(Landroid/content/ContentValues;)V
    .registers 3
    .parameter

    .prologue
    .line 462
    const-string v0, "path"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 463
    const-string v0, "path"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 464
    const-string v1, "canon_path"

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_17
    const-string v0, "parent_path"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 468
    const-string v0, "parent_path"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 469
    const-string v1, "canon_parent_path"

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    :cond_2e
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 355
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 357
    if-nez p2, :cond_9

    if-eqz p3, :cond_2f

    .line 358
    :cond_9
    sget-object v1, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected where clause: \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' with args: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    :cond_2f
    invoke-static {p1}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/m;

    move-result-object v1

    .line 362
    if-nez v1, :cond_52

    .line 363
    sget-object v1, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown type of URI in delete(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :goto_51
    return v0

    .line 367
    :cond_52
    sget-object v2, Lcom/dropbox/android/provider/l;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/provider/m;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_90

    .line 375
    sget-object v1, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown type of URI in delete(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_51

    .line 369
    :pswitch_7a
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 370
    const-string v1, "dropbox"

    invoke-virtual {v0, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 371
    invoke-virtual {p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_51

    .line 367
    :pswitch_data_90
    .packed-switch 0x7
        :pswitch_7a
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 229
    invoke-static {p1}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/m;

    move-result-object v0

    .line 230
    if-nez v0, :cond_9

    .line 231
    const-string v0, ""

    .line 240
    :goto_8
    return-object v0

    .line 233
    :cond_9
    sget-object v1, Lcom/dropbox/android/provider/l;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/m;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1e

    .line 240
    :pswitch_14
    const-string v0, ""

    goto :goto_8

    .line 236
    :pswitch_17
    const-string v0, "vnd.android.cursor.dir/vnd.dropbox.entry"

    goto :goto_8

    .line 238
    :pswitch_1a
    const-string v0, "vnd.android.cursor.item/vnd.dropbox.entry"

    goto :goto_8

    .line 233
    nop

    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_17
        :pswitch_1a
        :pswitch_14
        :pswitch_17
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 246
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 251
    invoke-static {p1}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/m;

    move-result-object v4

    .line 252
    sget-object v0, Lcom/dropbox/android/provider/m;->a:Lcom/dropbox/android/provider/m;

    if-eq v4, v0, :cond_2a

    sget-object v0, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    if-eq v4, v0, :cond_2a

    .line 253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Insert: Illegal URI for insert: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_2a
    if-eqz p2, :cond_170

    .line 263
    invoke-static {p2}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/content/ContentValues;)V

    .line 265
    const-string v0, "_upload"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16d

    .line 266
    const-string v0, "_upload"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 268
    :goto_3d
    const-string v2, "_upload_force"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_169

    .line 269
    const-string v2, "_upload_force"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object v3, v0

    move v0, v2

    .line 272
    :goto_51
    sget-object v2, Lcom/dropbox/android/provider/m;->a:Lcom/dropbox/android/provider/m;

    if-eq v4, v2, :cond_5b

    sget-object v2, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    if-ne v4, v2, :cond_158

    if-eqz v3, :cond_158

    .line 275
    :cond_5b
    if-eqz p2, :cond_72

    .line 276
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 277
    const-string v4, "path"

    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8b

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Entries.PATH missiong in initialvalues"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 279
    :cond_72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Insert: Can\'t insert a null entry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_8b
    if-eqz v3, :cond_d2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_d2

    .line 284
    sget-object v0, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding new file (from import, probably): "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-static {v3}, Lcom/dropbox/android/util/ae;->r(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 288
    new-instance v0, Lcom/dropbox/android/taskqueue/UploadTask;

    invoke-virtual {p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v6, "parent_path"

    invoke-virtual {v2, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/UploadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 296
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    .line 297
    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v1

    .line 298
    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/H;->a(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 349
    :goto_d1
    return-object p1

    .line 303
    :cond_d2
    const-string v3, "_upload"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_df

    .line 304
    const-string v3, "_upload"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 306
    :cond_df
    const-string v3, "_upload_force"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_ec

    .line 307
    const-string v3, "_upload_force"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 310
    :cond_ec
    const-string v3, "_data"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_166

    .line 311
    const-string v3, "Entries.PATH"

    const-string v4, "path"

    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 314
    :try_start_107
    const-string v4, "dropbox"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_10d
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_107 .. :try_end_10d} :catch_151

    .line 322
    :cond_10d
    :goto_10d
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 323
    const-string v1, "hash"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-static {p1}, Lcom/dropbox/android/util/ae;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 326
    const-string v4, "canon_parent_path = ?"

    .line 327
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    new-instance v7, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v7, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v7}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v5

    .line 329
    const-string v1, "path"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_144

    .line 330
    const-string v1, "Entries.PATH"

    const-string v5, "path"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_144
    const-string v1, "dropbox"

    invoke-virtual {v3, v1, v0, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 334
    invoke-virtual {p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_d1

    .line 315
    :catch_151
    move-exception v4

    .line 316
    if-eqz v0, :cond_10d

    .line 317
    invoke-virtual {p0, p1, v2, v1, v1}, Lcom/dropbox/android/provider/DropboxProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_10d

    .line 337
    :cond_158
    sget-object v0, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    if-ne v4, v0, :cond_166

    .line 339
    new-instance v0, Lcom/dropbox/android/provider/k;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/provider/k;-><init>(Lcom/dropbox/android/provider/DropboxProvider;Landroid/net/Uri;)V

    .line 346
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_d1

    :cond_166
    move-object p1, v1

    .line 349
    goto/16 :goto_d1

    :cond_169
    move-object v3, v0

    move v0, v5

    goto/16 :goto_51

    :cond_16d
    move-object v0, v1

    goto/16 :goto_3d

    :cond_170
    move v0, v5

    move-object v3, v1

    goto/16 :goto_51
.end method

.method public onCreate()Z
    .registers 2

    .prologue
    .line 56
    new-instance v0, Lcom/dropbox/android/provider/F;

    invoke-direct {v0}, Lcom/dropbox/android/provider/F;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/DropboxProvider;->b:Lcom/dropbox/android/provider/F;

    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 458
    const/4 v0, 0x0

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 90
    :try_start_3
    const-string v1, "dbonly"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6d

    const/4 v1, 0x1

    move v5, v1

    .line 93
    :goto_f
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    .line 94
    if-eqz v1, :cond_35

    .line 95
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dbonly=true"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "&&"

    const-string v3, "&"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 99
    :cond_35
    new-instance v7, Lcom/dropbox/android/provider/A;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Lcom/dropbox/android/provider/A;-><init>(Landroid/net/Uri;)V

    .line 100
    invoke-virtual {v7}, Lcom/dropbox/android/provider/A;->d()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_70

    const/4 v1, 0x1

    move v3, v1

    .line 101
    :goto_45
    invoke-virtual {v7}, Lcom/dropbox/android/provider/A;->b()Landroid/net/Uri;

    move-result-object v4

    .line 103
    invoke-static {v4}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/m;

    move-result-object v6

    .line 104
    if-nez v6, :cond_73

    .line 105
    sget-object v1, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad uri in query: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v1, 0x0

    .line 222
    :goto_6c
    return-object v1

    .line 90
    :cond_6d
    const/4 v1, 0x0

    move v5, v1

    goto :goto_f

    .line 100
    :cond_70
    const/4 v1, 0x0

    move v3, v1

    goto :goto_45

    .line 108
    :cond_73
    sget-object v1, Lcom/dropbox/android/provider/l;->a:[I

    invoke-virtual {v6}, Lcom/dropbox/android/provider/m;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_210

    .line 143
    sget-object v1, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad uri in query: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v1, 0x0

    goto :goto_6c

    .line 110
    :pswitch_9c
    if-nez p3, :cond_209

    .line 111
    const-string p3, "canon_parent_path = ?"

    .line 112
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 p4, v0

    const/4 v1, 0x0

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v2, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p4, v1

    move-object/from16 v12, p4

    move-object/from16 v11, p3

    .line 150
    :goto_b9
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_144

    .line 151
    const-string v15, "is_dir DESC, _display_name COLLATE NOCASE"

    .line 157
    :goto_c1
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 158
    const-string v9, "dropbox"

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v10, p2

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 161
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v2, v1, v4}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 163
    if-eqz v5, :cond_148

    move-object v1, v2

    .line 164
    goto :goto_6c

    .line 116
    :pswitch_e2
    if-nez p3, :cond_209

    .line 117
    const-string p3, "canon_path = ?"

    .line 118
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 p4, v0

    const/4 v1, 0x0

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v2, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p4, v1

    move-object/from16 v12, p4

    move-object/from16 v11, p3

    goto :goto_b9

    .line 122
    :pswitch_100
    invoke-static {v4}, Lcom/dropbox/android/provider/A;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/A;

    move-result-object v1

    .line 123
    invoke-virtual {v1}, Lcom/dropbox/android/provider/A;->c()Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_6c

    .line 126
    :pswitch_10a
    const-string p3, "is_favorite=1"

    move-object/from16 v12, p4

    move-object/from16 v11, p3

    .line 127
    goto :goto_b9

    .line 130
    :pswitch_111
    const-string v1, "query"

    invoke-virtual {v4, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 131
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/provider/DropboxProvider;->b:Lcom/dropbox/android/provider/F;

    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v3

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v7}, Lcom/dropbox/android/provider/F;->a(Landroid/content/Context;Lcom/dropbox/android/provider/h;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/dropbox/android/provider/A;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_6c

    .line 135
    :pswitch_12b
    move-object/from16 v0, p4

    array-length v1, v0

    if-lez v1, :cond_141

    .line 136
    const/4 v1, 0x0

    aget-object v1, p4, v1

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/provider/DropboxProvider;->b:Lcom/dropbox/android/provider/F;

    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/dropbox/android/provider/F;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_6c

    .line 139
    :cond_141
    const/4 v1, 0x0

    goto/16 :goto_6c

    :cond_144
    move-object/from16 v15, p5

    .line 153
    goto/16 :goto_c1

    .line 169
    :cond_148
    if-eqz v2, :cond_150

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1de

    .line 170
    :cond_150
    const/4 v1, 0x0

    .line 172
    sget-object v5, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    if-ne v6, v5, :cond_1a6

    .line 174
    new-instance v5, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v5, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v5}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v5

    .line 175
    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 177
    const-string v9, "dropbox"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "hash"

    aput-object v12, v10, v11

    const-string v11, "canon_path = ?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {v5}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v12, v13

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 184
    if-eqz v5, :cond_1a6

    .line 185
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_1a3

    .line 186
    const-string v8, "hash"

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 187
    if-eqz v8, :cond_1a3

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    const/16 v9, 0x20

    if-ne v8, v9, :cond_1a3

    .line 191
    const/4 v1, 0x1

    .line 194
    :cond_1a3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 201
    :cond_1a6
    :goto_1a6
    if-eqz v3, :cond_1db

    .line 202
    sget-object v3, Lcom/dropbox/android/provider/m;->c:Lcom/dropbox/android/provider/m;

    if-eq v6, v3, :cond_1b3

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v7, v3, v1}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    .line 207
    :cond_1b3
    sget-object v1, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    if-ne v6, v1, :cond_1db

    .line 208
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    .line 209
    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/x;->g()Lcom/dropbox/android/taskqueue/i;

    move-result-object v1

    .line 210
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/i;->d()V

    .line 211
    new-instance v3, Lcom/dropbox/android/taskqueue/MetadataTask;

    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v3, v5, v4, v7, v6}, Lcom/dropbox/android/taskqueue/MetadataTask;-><init>(Lcom/dropbox/android/provider/h;Landroid/net/Uri;Lcom/dropbox/android/provider/A;I)V

    .line 212
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/dropbox/android/taskqueue/i;->a(Lcom/dropbox/android/taskqueue/k;Z)Z

    move-result v1

    if-eqz v1, :cond_1db

    .line 213
    new-instance v1, Lcom/dropbox/android/provider/x;

    invoke-direct {v1, v3}, Lcom/dropbox/android/provider/x;-><init>(Lcom/dropbox/android/taskqueue/MetadataTask;)V

    .line 214
    invoke-virtual {v1}, Lcom/dropbox/android/provider/x;->start()V
    :try_end_1db
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_1db} :catch_1e0

    :cond_1db
    move-object v1, v2

    .line 218
    goto/16 :goto_6c

    .line 198
    :cond_1de
    const/4 v1, 0x2

    goto :goto_1a6

    .line 219
    :catch_1e0
    move-exception v1

    .line 220
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v1, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 221
    sget-object v2, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in query() with uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 222
    const/4 v1, 0x0

    goto/16 :goto_6c

    :cond_209
    move-object/from16 v12, p4

    move-object/from16 v11, p3

    goto/16 :goto_b9

    .line 108
    nop

    :pswitch_data_210
    .packed-switch 0x1
        :pswitch_9c
        :pswitch_e2
        :pswitch_100
        :pswitch_10a
        :pswitch_111
        :pswitch_12b
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 382
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 386
    const-string v3, "canon_path = ?"

    .line 387
    new-array v4, v10, [Ljava/lang/String;

    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 389
    if-eqz p2, :cond_1e

    .line 390
    invoke-static {p2}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/content/ContentValues;)V

    .line 394
    :cond_1e
    invoke-static {p1}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/m;

    move-result-object v0

    .line 395
    sget-object v1, Lcom/dropbox/android/provider/m;->a:Lcom/dropbox/android/provider/m;

    if-ne v0, v1, :cond_a3

    .line 398
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 402
    :try_start_2e
    const-string v1, "dropbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_data"

    aput-object v6, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3e
    .catchall {:try_start_2e .. :try_end_3e} :catchall_9c

    move-result-object v1

    .line 407
    if-eqz v1, :cond_108

    :try_start_41
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_102

    move-result v0

    if-eqz v0, :cond_108

    move v0, v10

    .line 411
    :goto_48
    if-eqz v1, :cond_4d

    .line 412
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 422
    :cond_4d
    :goto_4d
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 423
    if-eqz p2, :cond_9b

    .line 424
    if-eqz v0, :cond_e9

    .line 427
    if-nez p4, :cond_c4

    .line 433
    :goto_5b
    const-string v0, "path"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 434
    const-string v0, "Entries.PATH"

    const-string v2, "path"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_6e
    const-string v0, "dropbox"

    invoke-virtual {v1, v0, p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 437
    if-eq v8, v10, :cond_94

    .line 438
    sget-object v0, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rows modified in update operation!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    :cond_94
    :goto_94
    invoke-virtual {p0}, Lcom/dropbox/android/provider/DropboxProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 451
    :cond_9b
    :goto_9b
    return v8

    .line 411
    :catchall_9c
    move-exception v0

    :goto_9d
    if-eqz v9, :cond_a2

    .line 412
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_a2
    throw v0

    .line 415
    :cond_a3
    sget-object v1, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    if-eq v0, v1, :cond_105

    .line 416
    sget-object v0, Lcom/dropbox/android/provider/DropboxProvider;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type of URI in update(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9b

    .line 430
    :cond_c4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 431
    array-length v0, v4

    array-length v2, p4

    invoke-static {p4, v8, v4, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v4, v9

    goto/16 :goto_5b

    .line 441
    :cond_e9
    const-string v0, "Entries.PATH"

    const-string v2, "path"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dropbox/android/util/T;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    const-string v0, "dropbox"

    invoke-virtual {v1, v0, v9, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 443
    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_94

    move v8, v10

    .line 444
    goto :goto_94

    .line 411
    :catchall_102
    move-exception v0

    move-object v9, v1

    goto :goto_9d

    :cond_105
    move v0, v8

    goto/16 :goto_4d

    :cond_108
    move v0, v8

    goto/16 :goto_48
.end method
