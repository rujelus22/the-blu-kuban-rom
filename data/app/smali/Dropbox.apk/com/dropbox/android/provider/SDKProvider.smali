.class public Lcom/dropbox/android/provider/SDKProvider;
.super Landroid/content/ContentProvider;
.source "panda.py"


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/dropbox/android/provider/SDKProvider;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 32
    return-void
.end method

.method private static a(Landroid/net/Uri;)Lcom/dropbox/android/provider/C;
    .registers 7
    .parameter

    .prologue
    .line 133
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 135
    invoke-static {}, Lcom/dropbox/android/provider/C;->values()[Lcom/dropbox/android/provider/C;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v4, :cond_1e

    aget-object v0, v3, v1

    .line 136
    invoke-virtual {v0}, Lcom/dropbox/android/provider/C;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 140
    :goto_19
    return-object v0

    .line 135
    :cond_1a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 140
    :cond_1e
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public static a(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 223
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/C;->a:Lcom/dropbox/android/provider/C;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/C;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 224
    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 217
    if-eqz p0, :cond_6

    .line 218
    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :cond_6
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 227
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/C;->b:Lcom/dropbox/android/provider/C;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/C;->b()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 228
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 145
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 146
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 151
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/C;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_b

    .line 153
    invoke-virtual {v0}, Lcom/dropbox/android/provider/C;->c()Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 192
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 193
    invoke-virtual {p0}, Lcom/dropbox/android/provider/SDKProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 194
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/C;

    move-result-object v1

    .line 195
    invoke-static {v0}, Lcom/dropbox/android/filemanager/a;->b(Landroid/content/Context;)Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v2

    if-eqz v2, :cond_17

    if-nez v1, :cond_19

    .line 196
    :cond_17
    const/4 v0, 0x0

    .line 200
    :goto_18
    return-object v0

    .line 199
    :cond_19
    invoke-virtual {v1}, Lcom/dropbox/android/provider/C;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/dropbox/android/provider/SDKProvider;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 200
    invoke-virtual {v1, p1, p2, v0}, Lcom/dropbox/android/provider/C;->a(Landroid/net/Uri;Landroid/content/ContentValues;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_18
.end method

.method public onCreate()Z
    .registers 2

    .prologue
    .line 163
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 170
    invoke-virtual {p0}, Lcom/dropbox/android/provider/SDKProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/provider/SDKProvider;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 177
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 178
    invoke-static {}, Lcom/dropbox/android/util/i;->aj()Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "calling.pkgs"

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 181
    :cond_38
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/C;

    move-result-object v0

    .line 182
    invoke-static {v6}, Lcom/dropbox/android/filemanager/a;->b(Landroid/content/Context;)Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v1

    if-eqz v1, :cond_48

    if-nez v0, :cond_4a

    .line 183
    :cond_48
    const/4 v0, 0x0

    .line 187
    :goto_49
    return-object v0

    .line 186
    :cond_4a
    invoke-virtual {v0}, Lcom/dropbox/android/provider/C;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/dropbox/android/provider/SDKProvider;->a(Ljava/lang/String;Landroid/content/Context;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 187
    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/android/provider/C;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_49
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 205
    invoke-static {}, Lcom/dropbox/android/DropboxApplication;->b()V

    .line 206
    invoke-virtual {p0}, Lcom/dropbox/android/provider/SDKProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 207
    invoke-static {p1}, Lcom/dropbox/android/provider/SDKProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/C;

    move-result-object v0

    .line 208
    invoke-static {v5}, Lcom/dropbox/android/filemanager/a;->b(Landroid/content/Context;)Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v1

    if-eqz v1, :cond_17

    if-nez v0, :cond_19

    .line 209
    :cond_17
    const/4 v0, 0x0

    .line 213
    :goto_18
    return v0

    .line 212
    :cond_19
    invoke-virtual {v0}, Lcom/dropbox/android/provider/C;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/dropbox/android/provider/SDKProvider;->a(Ljava/lang/String;Landroid/content/Context;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 213
    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/provider/C;->a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    goto :goto_18
.end method
