.class public final Lcom/dropbox/android/provider/M;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/dropbox/android/provider/b;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 14
    iget-object v0, p0, Lcom/dropbox/android/provider/b;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/dropbox/android/provider/b;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_25

    .line 15
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected to be able to move to position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/provider/b;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_25
    iget-object v0, p0, Lcom/dropbox/android/provider/b;->a:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/provider/b;->a:Landroid/database/Cursor;

    const-string v2, "local_uri"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 18
    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/provider/b;Lcom/dropbox/android/provider/b;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-static {p1}, Lcom/dropbox/android/provider/M;->a(Lcom/dropbox/android/provider/b;)Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-static {p2}, Lcom/dropbox/android/provider/M;->a(Lcom/dropbox/android/provider/b;)Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {v0, v1}, Lcom/dropbox/android/util/aT;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 11
    check-cast p1, Lcom/dropbox/android/provider/b;

    check-cast p2, Lcom/dropbox/android/provider/b;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/provider/M;->a(Lcom/dropbox/android/provider/b;Lcom/dropbox/android/provider/b;)I

    move-result v0

    return v0
.end method
