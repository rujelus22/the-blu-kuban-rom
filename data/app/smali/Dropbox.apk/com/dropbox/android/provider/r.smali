.class public final Lcom/dropbox/android/provider/r;
.super Landroid/database/AbstractCursor;
.source "panda.py"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Landroid/database/Cursor;

.field private c:Landroid/database/Cursor;

.field private d:Ljava/util/List;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "HEADER_LABEL"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/provider/r;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 167
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 33
    iput-boolean v7, p0, Lcom/dropbox/android/provider/r;->e:Z

    .line 168
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/dropbox/android/provider/r;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 170
    new-instance v1, Lcom/dropbox/android/provider/L;

    const-string v2, "DropboxEntry"

    invoke-direct {v1, p1, v2}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    .line 171
    new-instance v1, Lcom/dropbox/android/provider/L;

    const-string v2, "_separator"

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    .line 172
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/dropbox/android/provider/r;->d:Ljava/util/List;

    .line 174
    sget-object v1, Lcom/dropbox/android/provider/e;->c:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 175
    new-instance v2, Lcom/dropbox/android/provider/s;

    invoke-direct {v2}, Lcom/dropbox/android/provider/s;-><init>()V

    .line 176
    invoke-interface {p1, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 177
    :goto_37
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_81

    .line 178
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 179
    invoke-static {v2, v3, v4}, Lcom/dropbox/android/provider/s;->a(Lcom/dropbox/android/provider/s;J)Ljava/lang/String;

    move-result-object v3

    .line 180
    if-eqz v3, :cond_6d

    .line 182
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 183
    iget-object v3, p0, Lcom/dropbox/android/provider/r;->d:Ljava/util/List;

    iget-object v4, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    iget-object v5, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    :cond_6d
    iget-object v3, p0, Lcom/dropbox/android/provider/r;->d:Ljava/util/List;

    iget-object v4, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_37

    .line 187
    :cond_81
    return-void
.end method

.method static synthetic a()Ljava/util/Calendar;
    .registers 1

    .prologue
    .line 26
    invoke-static {}, Lcom/dropbox/android/provider/r;->b()Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/util/Date;)Ljava/util/Date;
    .registers 2
    .parameter

    .prologue
    .line 26
    invoke-static {p0}, Lcom/dropbox/android/provider/r;->b(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Calendar;)V
    .registers 3
    .parameter

    .prologue
    .line 50
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 51
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 52
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->clear(I)V

    .line 53
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 54
    return-void
.end method

.method private static b()Ljava/util/Calendar;
    .registers 1

    .prologue
    .line 44
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lcom/dropbox/android/provider/r;->a(Ljava/util/Calendar;)V

    .line 46
    return-object v0
.end method

.method private static b(Ljava/util/Date;)Ljava/util/Date;
    .registers 4
    .parameter

    .prologue
    .line 36
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 38
    invoke-static {v0}, Lcom/dropbox/android/provider/r;->a(Ljava/util/Calendar;)V

    .line 39
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 40
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method private c()Landroid/database/Cursor;
    .registers 4

    .prologue
    .line 240
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->d:Ljava/util/List;

    iget v1, p0, Lcom/dropbox/android/provider/r;->mPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 241
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/database/Cursor;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 242
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/provider/r;->e:Z

    .line 320
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 321
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 322
    return-void
.end method

.method public final copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 258
    return-void
.end method

.method public final deactivate()V
    .registers 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 305
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 306
    return-void
.end method

.method public final getBlob(I)[B
    .registers 3
    .parameter

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getColumnCount()I
    .registers 2

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    return v0
.end method

.method public final getColumnIndex(Ljava/lang/String;)I
    .registers 3
    .parameter

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getColumnIndexOrThrow(Ljava/lang/String;)I
    .registers 3
    .parameter

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getColumnName(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getDouble(I)D
    .registers 4
    .parameter

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 365
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getFloat(I)F
    .registers 3
    .parameter

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final getInt(I)I
    .registers 4
    .parameter

    .prologue
    .line 267
    if-nez p1, :cond_a

    .line 268
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Asked for _id as int instead of long."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_a
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getLong(I)J
    .registers 4
    .parameter

    .prologue
    .line 275
    if-nez p1, :cond_6

    .line 276
    iget v0, p0, Lcom/dropbox/android/provider/r;->mPos:I

    int-to-long v0, v0

    .line 278
    :goto_5
    return-wide v0

    :cond_6
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_5
.end method

.method public final getShort(I)S
    .registers 3
    .parameter

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getType(I)I
    .registers 3
    .parameter

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public final getWantsAllOnMoveCalls()Z
    .registers 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final isClosed()Z
    .registers 3

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/dropbox/android/provider/r;->e:Z

    if-nez v0, :cond_1c

    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 327
    :cond_14
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "HeaderWrappedCursor is not closed, but one of its wrapped cursors is"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_1c
    iget-boolean v0, p0, Lcom/dropbox/android/provider/r;->e:Z

    return v0
.end method

.method public final isNull(I)Z
    .registers 3
    .parameter

    .prologue
    .line 299
    invoke-direct {p0}, Lcom/dropbox/android/provider/r;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 334
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 335
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 344
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 345
    return-void
.end method

.method public final requery()Z
    .registers 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/dropbox/android/provider/r;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    .line 311
    :goto_11
    if-nez v0, :cond_16

    .line 312
    invoke-virtual {p0}, Lcom/dropbox/android/provider/r;->close()V

    .line 314
    :cond_16
    return v0

    .line 310
    :cond_17
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 4
    .parameter

    .prologue
    .line 370
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 354
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 355
    return-void
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 339
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 340
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 349
    iget-object v0, p0, Lcom/dropbox/android/provider/r;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 350
    return-void
.end method
