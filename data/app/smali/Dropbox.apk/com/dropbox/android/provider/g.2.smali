.class public final Lcom/dropbox/android/provider/g;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Lcom/dropbox/android/provider/c;

.field public static final b:Lcom/dropbox/android/provider/c;

.field public static final c:Lcom/dropbox/android/provider/c;

.field public static final d:Lcom/dropbox/android/provider/c;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 8
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "thumbnail_info"

    const-string v2, "_id"

    sget-object v3, Lcom/dropbox/android/provider/d;->a:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/g;->a:Lcom/dropbox/android/provider/c;

    .line 9
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "thumbnail_info"

    const-string v2, "dropbox_canon_path"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/g;->b:Lcom/dropbox/android/provider/c;

    .line 10
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "thumbnail_info"

    const-string v2, "thumb_size"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/g;->c:Lcom/dropbox/android/provider/c;

    .line 11
    new-instance v0, Lcom/dropbox/android/provider/c;

    const-string v1, "thumbnail_info"

    const-string v2, "revision"

    sget-object v3, Lcom/dropbox/android/provider/d;->b:Lcom/dropbox/android/provider/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/android/provider/d;)V

    sput-object v0, Lcom/dropbox/android/provider/g;->d:Lcom/dropbox/android/provider/c;

    return-void
.end method
