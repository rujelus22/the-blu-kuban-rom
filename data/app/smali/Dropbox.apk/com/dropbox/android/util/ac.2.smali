.class public Lcom/dropbox/android/util/ac;
.super Lcom/dropbox/android/util/s;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/n/r;


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:J

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;

.field private g:Ljava/lang/Long;

.field private h:J

.field private i:J

.field private j:Z

.field private k:Lcom/dropbox/android/service/x;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 62
    const-class v0, Lcom/dropbox/android/util/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/ac;->b:Ljava/lang/String;

    .line 65
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "event"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "request.method"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "request.startTime"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "response.wallTime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "logging.ready"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/ac;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 124
    const-string v0, "network.log"

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/ac;->e:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/ac;->f:Ljava/util/ArrayList;

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/ac;->g:Ljava/lang/Long;

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/util/ac;->j:Z

    .line 126
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v0

    .line 127
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/util/ac;->k:Lcom/dropbox/android/service/x;

    .line 129
    const-string v1, "system.carrier"

    iget-object v0, v0, Ldbxyzptlk/j/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 130
    const-string v0, "system.manufacturer"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 131
    const-string v0, "system.locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 133
    const-string v0, "network.connected"

    iget-object v1, p0, Lcom/dropbox/android/util/ac;->k:Lcom/dropbox/android/service/x;

    invoke-virtual {v1}, Lcom/dropbox/android/service/x;->a()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 134
    const-string v0, "network.wifi"

    iget-object v1, p0, Lcom/dropbox/android/util/ac;->k:Lcom/dropbox/android/service/x;

    invoke-virtual {v1}, Lcom/dropbox/android/service/x;->b()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 135
    const-string v0, "network.roaming"

    iget-object v1, p0, Lcom/dropbox/android/util/ac;->k:Lcom/dropbox/android/service/x;

    invoke-virtual {v1}, Lcom/dropbox/android/service/x;->d()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 136
    const-string v0, "network.3g"

    iget-object v1, p0, Lcom/dropbox/android/util/ac;->k:Lcom/dropbox/android/service/x;

    invoke-virtual {v1}, Lcom/dropbox/android/service/x;->c()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 139
    return-void
.end method

.method private a(JJ)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 232
    iput-wide p3, p0, Lcom/dropbox/android/util/ac;->i:J

    .line 233
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/util/ac;->h:J

    .line 234
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->f:Ljava/util/ArrayList;

    iget-wide v1, p0, Lcom/dropbox/android/util/ac;->h:J

    iget-wide v3, p0, Lcom/dropbox/android/util/ac;->i:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xf4240

    div-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->e:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    return-void
.end method

.method public static d()Z
    .registers 4

    .prologue
    .line 148
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    const-wide v2, 0x3fa999999999999aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/util/ac;->c:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lorg/apache/http/HttpResponse;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 185
    const-string v0, "response.wallTime"

    invoke-virtual {p0}, Lcom/dropbox/android/util/ac;->f()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 186
    const-string v0, "response.status"

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 187
    const-string v0, "response.size"

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 189
    if-nez p2, :cond_2c

    .line 190
    const-string v0, "logging.ready"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 192
    :cond_2c
    return-void
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .registers 4
    .parameter

    .prologue
    .line 158
    const-string v0, "request.method"

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 159
    const-string v0, "request.path"

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 160
    return-void
.end method

.method public final b(J)V
    .registers 6
    .parameter

    .prologue
    .line 169
    iput-wide p1, p0, Lcom/dropbox/android/util/ac;->d:J

    .line 170
    const-string v0, "request.startTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/dropbox/android/util/ac;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 171
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/util/ac;->g:Ljava/lang/Long;

    .line 172
    return-void
.end method

.method public final c()V
    .registers 6

    .prologue
    .line 281
    const-string v0, "run a dev build for more debugging info"

    .line 286
    iget-boolean v1, p0, Lcom/dropbox/android/util/ac;->j:Z

    if-eqz v1, :cond_1f

    .line 287
    sget-object v1, Lcom/dropbox/android/util/ac;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not logged; event was invalid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :goto_1e
    return-void

    .line 290
    :cond_1f
    invoke-direct {p0}, Lcom/dropbox/android/util/ac;->h()Z

    move-result v1

    if-nez v1, :cond_3e

    .line 291
    sget-object v1, Lcom/dropbox/android/util/ac;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not logged; missing keys: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e

    .line 294
    :cond_3e
    invoke-virtual {p0}, Lcom/dropbox/android/util/ac;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-long v1, v1

    const-wide/32 v3, 0x380000

    cmp-long v1, v1, v3

    if-ltz v1, :cond_67

    .line 295
    sget-object v1, Lcom/dropbox/android/util/ac;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not logged; too large: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e

    .line 298
    :cond_67
    sget-object v0, Lcom/dropbox/android/util/ac;->b:Ljava/lang/String;

    const-string v1, "Writing event to log."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-super {p0}, Lcom/dropbox/android/util/s;->c()V

    goto :goto_1e
.end method

.method public final declared-synchronized c(J)V
    .registers 13
    .parameter

    .prologue
    const-wide/16 v8, 0x1

    .line 210
    monitor-enter p0

    :try_start_3
    iget-wide v0, p0, Lcom/dropbox/android/util/ac;->d:J
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_24

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_d

    .line 229
    :goto_b
    monitor-exit p0

    return-void

    .line 214
    :cond_d
    :try_start_d
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 215
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/dropbox/android/util/ac;->a(JJ)V

    .line 216
    iget-wide v0, p0, Lcom/dropbox/android/util/ac;->d:J

    sub-long/2addr v0, v8

    iput-wide v0, p0, Lcom/dropbox/android/util/ac;->d:J
    :try_end_23
    .catchall {:try_start_d .. :try_end_23} :catchall_24

    goto :goto_b

    .line 210
    :catchall_24
    move-exception v0

    monitor-exit p0

    throw v0

    .line 218
    :cond_27
    :try_start_27
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 219
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 220
    add-long v4, v2, p1

    const-wide/16 v6, 0x4000

    cmp-long v0, v4, v6

    if-gtz v0, :cond_55

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 223
    add-long v0, v2, p1

    iget-wide v2, p0, Lcom/dropbox/android/util/ac;->i:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/dropbox/android/util/ac;->a(JJ)V

    goto :goto_b

    .line 225
    :cond_55
    iget-wide v0, p0, Lcom/dropbox/android/util/ac;->h:J

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/dropbox/android/util/ac;->a(JJ)V

    .line 226
    iget-wide v0, p0, Lcom/dropbox/android/util/ac;->d:J

    sub-long/2addr v0, v8

    iput-wide v0, p0, Lcom/dropbox/android/util/ac;->d:J
    :try_end_5f
    .catchall {:try_start_27 .. :try_end_5f} :catchall_24

    goto :goto_b
.end method

.method public final d(J)V
    .registers 6
    .parameter

    .prologue
    .line 244
    const-string v0, "stream.updates.bytes"

    iget-object v1, p0, Lcom/dropbox/android/util/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/s;

    .line 245
    const-string v0, "stream.updates.time"

    iget-object v1, p0, Lcom/dropbox/android/util/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/android/util/s;

    .line 247
    const-string v0, "stream.total.bytes"

    invoke-virtual {p0, v0, p1, p2}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 248
    const-string v0, "stream.total.time"

    invoke-virtual {p0}, Lcom/dropbox/android/util/ac;->f()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 256
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/dropbox/android/util/ac;->k:Lcom/dropbox/android/service/x;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/x;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_30

    .line 258
    invoke-virtual {p0}, Lcom/dropbox/android/util/ac;->g()V

    .line 263
    :goto_2f
    return-void

    .line 262
    :cond_30
    const-string v0, "logging.ready"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    goto :goto_2f
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 175
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/ac;->b(J)V

    .line 176
    return-void
.end method

.method public final f()J
    .registers 5

    .prologue
    .line 267
    iget-object v0, p0, Lcom/dropbox/android/util/ac;->g:Ljava/lang/Long;

    if-nez v0, :cond_a

    .line 268
    invoke-virtual {p0}, Lcom/dropbox/android/util/ac;->g()V

    .line 269
    const-wide/16 v0, -0x1

    .line 271
    :goto_9
    return-wide v0

    :cond_a
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/dropbox/android/util/ac;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    goto :goto_9
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/util/ac;->j:Z

    .line 277
    return-void
.end method
