.class final enum Lcom/dropbox/android/util/aE;
.super Lcom/dropbox/android/util/aC;
.source "panda.py"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/util/aC;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/az;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
    .registers 7
    .parameter
    .parameter

    .prologue
    const v3, 0x7f0b01b2

    .line 51
    const-string v0, "ACTION_CAMERA_UPLOAD_DETAILS"

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/DropboxBrowser;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 52
    invoke-static {p1, v0}, Lcom/dropbox/android/util/ay;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 53
    new-instance v1, Landroid/support/v4/app/D;

    invoke-direct {v1, p1}, Landroid/support/v4/app/D;-><init>(Landroid/content/Context;)V

    .line 55
    const v2, 0x7f0200f2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->a(I)Landroid/support/v4/app/D;

    move-result-object v1

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    const v2, 0x7f0b01b3

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/D;->a(J)Landroid/support/v4/app/D;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/D;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/D;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/D;->b(I)Landroid/support/v4/app/D;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/D;->b(Z)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/D;->a()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method
