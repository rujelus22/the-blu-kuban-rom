.class final Lcom/dropbox/android/util/d;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/aj;


# instance fields
.field final synthetic a:Landroid/support/v4/app/FragmentActivity;

.field final synthetic b:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 386
    iput-object p1, p0, Lcom/dropbox/android/util/d;->a:Landroid/support/v4/app/FragmentActivity;

    iput-object p2, p0, Lcom/dropbox/android/util/d;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 1

    .prologue
    .line 403
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    .line 389
    iget-object v0, p0, Lcom/dropbox/android/util/d;->a:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_1d

    .line 390
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/dropbox/android/activity/LocalFileBrowserActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 391
    iget-object v0, p0, Lcom/dropbox/android/util/d;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 400
    :cond_1d
    :goto_1d
    return-void

    .line 393
    :cond_1e
    new-instance v0, Ldbxyzptlk/g/d;

    iget-object v1, p0, Lcom/dropbox/android/util/d;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/dropbox/android/util/d;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v0, v1, v2, p1}, Ldbxyzptlk/g/d;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V

    .line 394
    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Ldbxyzptlk/g/h;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    move-result-object v1

    .line 395
    iget-object v2, p0, Lcom/dropbox/android/util/d;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v2

    sget-object v3, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 396
    invoke-virtual {v0}, Ldbxyzptlk/g/d;->f()V

    .line 397
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1d
.end method
