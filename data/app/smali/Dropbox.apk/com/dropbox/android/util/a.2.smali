.class public Lcom/dropbox/android/util/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 51
    const-class v0, Lcom/dropbox/android/util/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/a;->a:Ljava/lang/String;

    .line 91
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.dropbox.android.activity.DropboxSendTo"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/a;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    const/16 v4, 0xa

    .line 295
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-static {}, Lcom/dropbox/android/util/bc;->c()I

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 296
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 297
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 298
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 299
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-direct {v2, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 300
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x106000c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 301
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 302
    return-object v1
.end method

.method protected static a(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 277
    new-instance v0, Lcom/dropbox/android/util/b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/dropbox/android/util/b;-><init>(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    return-object v0
.end method

.method public static a()V
    .registers 2

    .prologue
    .line 116
    sget-object v0, Lcom/dropbox/android/util/a;->a:Ljava/lang/String;

    const-string v1, "Unauthorized token, unlinking account"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->e()Z

    .line 118
    invoke-static {}, Lcom/dropbox/android/util/bi;->a()V

    .line 119
    invoke-static {}, Lcom/dropbox/android/util/a;->b()V

    .line 120
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .registers 4
    .parameter

    .prologue
    .line 60
    const v0, 0x7f0b000c

    .line 68
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 69
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 70
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v2, 0x35c

    if-le v1, v2, :cond_1c

    .line 71
    const v0, 0x7f0b000d

    .line 74
    :cond_1c
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 76
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 77
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v2, "video/mp4"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    :try_start_33
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 81
    invoke-static {}, Lcom/dropbox/android/util/i;->af()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V
    :try_end_3d
    .catch Landroid/content/ActivityNotFoundException; {:try_start_33 .. :try_end_3d} :catch_3e

    .line 89
    :goto_3d
    return-void

    .line 82
    :catch_3e
    move-exception v0

    .line 83
    sget-object v1, Lcom/dropbox/android/util/a;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 84
    const v0, 0x7f0b0077

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3d
.end method

.method public static a(Landroid/app/Activity;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 314
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 315
    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    const/high16 v0, 0x1000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 318
    new-array v2, v4, [Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/dropbox/android/activity/CopyLinkToClipboardActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    aput-object v0, v2, v5

    .line 319
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_3e

    const v0, 0x7f0b008e

    .line 320
    :goto_25
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    new-instance v3, Lcom/dropbox/android/widget/ag;

    new-array v4, v4, [Landroid/content/Intent;

    aput-object v1, v4, v5

    invoke-direct {v3, p0, v0, v4, v2}, Lcom/dropbox/android/widget/ag;-><init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;)V

    .line 324
    new-instance v0, Lcom/dropbox/android/util/c;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/util/c;-><init>(Landroid/app/Activity;Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v3, v0}, Lcom/dropbox/android/widget/ag;->a(Lcom/dropbox/android/widget/aj;)V

    .line 334
    invoke-virtual {v3}, Lcom/dropbox/android/widget/ag;->a()V

    .line 335
    return-void

    .line 319
    :cond_3e
    const v0, 0x7f0b008d

    goto :goto_25
.end method

.method public static a(Landroid/content/Context;IZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 413
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/DropboxWebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 414
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 415
    const-string v1, "TITLE"

    const v2, 0x7f0b015d

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    const-string v1, "EXTRA_REQUIRES_AUTH"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 417
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 418
    return-void
.end method

.method public static a(Landroid/content/Intent;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 342
    const-string v0, "com.dropbox.android.intent.extra.DROPBOX_PATH"

    iget-object v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const v5, 0x10000003

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 346
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_11

    .line 347
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot export a directory!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_11
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 353
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/at;->d()Landroid/net/Uri;

    move-result-object v2

    .line 354
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 355
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 356
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 357
    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 363
    invoke-static {p1}, Lcom/dropbox/android/util/ae;->a(Ldbxyzptlk/n/k;)Z

    move-result v3

    if-nez v3, :cond_48

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a4

    .line 364
    :cond_48
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 365
    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 370
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    aput-object v1, v0, v6

    aput-object v3, v0, v7

    .line 376
    :goto_5c
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    const/4 v4, 0x0

    const-class v5, Lcom/dropbox/android/activity/LocalFileBrowserActivity;

    invoke-direct {v1, v3, v4, p0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 378
    const-string v2, "EXPORT_DB_PROVIDER_URI"

    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 379
    new-instance v2, Landroid/content/pm/LabeledIntent;

    const-string v3, "com.dropbox.android"

    const v4, 0x7f0b0092

    const v5, 0x7f02015c

    invoke-direct {v2, v1, v3, v4, v5}, Landroid/content/pm/LabeledIntent;-><init>(Landroid/content/Intent;Ljava/lang/String;II)V

    .line 380
    new-array v1, v7, [Landroid/content/Intent;

    aput-object v2, v1, v6

    .line 382
    const v2, 0x7f0b008c

    invoke-virtual {p0, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 384
    new-instance v3, Lcom/dropbox/android/widget/ag;

    invoke-direct {v3, p0, v2, v0, v1}, Lcom/dropbox/android/widget/ag;-><init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;)V

    .line 386
    new-instance v0, Lcom/dropbox/android/util/d;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/util/d;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v3, v0}, Lcom/dropbox/android/widget/ag;->a(Lcom/dropbox/android/widget/aj;)V

    .line 406
    invoke-virtual {v3}, Lcom/dropbox/android/widget/ag;->a()V

    .line 407
    return-void

    .line 372
    :cond_a4
    new-array v0, v7, [Landroid/content/Intent;

    aput-object v1, v0, v6

    goto :goto_5c
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 287
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 289
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public static a(Lcom/dropbox/android/util/e;Landroid/content/Context;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Z
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v9, 0x7f0b0018

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 155
    new-instance v0, Lcom/dropbox/android/activity/delegate/k;

    invoke-direct {v0}, Lcom/dropbox/android/activity/delegate/k;-><init>()V

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, p1, v3, v1}, Lcom/dropbox/android/activity/delegate/k;->a(Landroid/content/Context;Lcom/dropbox/android/activity/K;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/dropbox/android/provider/K;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 161
    :try_start_20
    invoke-static {p2}, Lcom/dropbox/android/util/ae;->a(Ljava/util/Collection;)Ljava/util/HashMap;
    :try_end_23
    .catch Ljava/lang/SecurityException; {:try_start_20 .. :try_end_23} :catch_49

    move-result-object v1

    .line 171
    invoke-static {v0}, Lcom/dropbox/android/util/ae;->a(Landroid/database/Cursor;)Ljava/util/Set;

    move-result-object v0

    .line 172
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/dropbox/android/util/aS;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/HashSet;

    move-result-object v2

    .line 173
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/dropbox/android/util/aS;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/HashSet;

    move-result-object v4

    .line 175
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_88

    .line 176
    sget-object v0, Lcom/dropbox/android/util/f;->a:Lcom/dropbox/android/util/f;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {p0, v0, v1, p3}, Lcom/dropbox/android/util/e;->a(Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    :goto_47
    move v0, v6

    .line 259
    :goto_48
    return v0

    .line 162
    :catch_49
    move-exception v0

    .line 163
    sget-object v0, Lcom/dropbox/android/util/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Security Exception trying to import "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " files to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const v0, 0x7f0b0048

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v6}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v7

    .line 168
    goto :goto_48

    .line 181
    :cond_88
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 183
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_11f

    .line 185
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v6, :cond_db

    .line 186
    const v0, 0x7f0b00d7

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 187
    const v0, 0x7f0b00d8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v7

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-static {v2, v4}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 190
    const v0, 0x7f0b00d9

    sget-object v2, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {p0, v2, v1, p3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 197
    sget-object v0, Lcom/dropbox/android/util/f;->d:Lcom/dropbox/android/util/f;

    invoke-static {p0, v0, v3, p3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v5, v9, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 257
    :goto_d6
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_47

    .line 205
    :cond_db
    const v0, 0x7f0b00e2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v7

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-static {v0, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 208
    const v0, 0x7f0b00e3

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 209
    const v0, 0x7f0b00e4

    sget-object v2, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {p0, v2, v1, p3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 216
    sget-object v0, Lcom/dropbox/android/util/f;->d:Lcom/dropbox/android/util/f;

    invoke-static {p0, v0, v3, p3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v5, v9, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_d6

    .line 227
    :cond_11f
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v6, :cond_187

    const v0, 0x7f0b00da

    :goto_128
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v8, [Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v7

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v0, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 233
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v6, :cond_18b

    const v0, 0x7f0b00db

    :goto_152
    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 237
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v6, :cond_18f

    const v0, 0x7f0b00dc

    :goto_15e
    sget-object v3, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-static {p0, v3, v7, p3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 246
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v6, :cond_193

    const v0, 0x7f0b00dd

    :goto_174
    sget-object v3, Lcom/dropbox/android/util/f;->c:Lcom/dropbox/android/util/f;

    invoke-static {v2, v1}, Lcom/dropbox/android/util/aS;->a(Ljava/util/Set;Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {p0, v3, v1, p3}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_d6

    .line 227
    :cond_187
    const v0, 0x7f0b00de

    goto :goto_128

    .line 233
    :cond_18b
    const v0, 0x7f0b00df

    goto :goto_152

    .line 237
    :cond_18f
    const v0, 0x7f0b00e0

    goto :goto_15e

    .line 246
    :cond_193
    const v0, 0x7f0b00e1

    goto :goto_174
.end method

.method public static a(Ljava/lang/String;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 96
    sget-object v2, Lcom/dropbox/android/util/a;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 97
    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 98
    const/4 v0, 0x1

    .line 101
    :cond_10
    return v0

    .line 96
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_5
.end method

.method public static b()V
    .registers 3

    .prologue
    .line 306
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    .line 307
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/DropboxBrowser;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 308
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 309
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 310
    return-void
.end method
