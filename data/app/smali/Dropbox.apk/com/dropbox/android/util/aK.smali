.class final enum Lcom/dropbox/android/util/aK;
.super Lcom/dropbox/android/util/aC;
.source "panda.py"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 178
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/util/aC;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/az;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 181
    if-eqz p2, :cond_b

    const-string v0, "ARG_NUM_UPLOADS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 182
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Upload done notification needs args."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_13
    const-string v0, "ACTION_MOST_RECENT_UPLOAD"

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/DropboxBrowser;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 186
    invoke-static {p1, v0}, Lcom/dropbox/android/util/ay;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 189
    const-string v0, "ARG_NUM_UPLOADS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 190
    if-ne v0, v5, :cond_60

    .line 191
    const v0, 0x7f0b01c3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_2c
    new-instance v2, Landroid/support/v4/app/D;

    invoke-direct {v2, p1}, Landroid/support/v4/app/D;-><init>(Landroid/content/Context;)V

    .line 199
    const v3, 0x7f0200f2

    invoke-virtual {v2, v3}, Landroid/support/v4/app/D;->a(I)Landroid/support/v4/app/D;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/D;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v2

    const v3, 0x7f0b01c2

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/D;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/D;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/D;->a(J)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/D;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/D;->b(Z)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/D;->a()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 193
    :cond_60
    const v2, 0x7f0b01c4

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2c
.end method
