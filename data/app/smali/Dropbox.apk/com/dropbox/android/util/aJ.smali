.class final enum Lcom/dropbox/android/util/aJ;
.super Lcom/dropbox/android/util/aC;
.source "panda.py"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/util/aC;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/az;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
    .registers 9
    .parameter
    .parameter

    .prologue
    const v5, 0x7f0b01bf

    const/4 v3, 0x1

    .line 149
    if-eqz p2, :cond_e

    const-string v0, "ARG_NUM_UPLOADS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 150
    :cond_e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "In progress upload notification needs args."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_16
    const-string v0, "ACTION_MOST_RECENT_UPLOAD"

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/DropboxBrowser;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 154
    invoke-static {p1, v0}, Lcom/dropbox/android/util/ay;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 157
    const-string v0, "ARG_NUM_UPLOADS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 158
    if-ne v0, v3, :cond_5e

    .line 159
    const v0, 0x7f0b01c0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 165
    :goto_2f
    new-instance v2, Landroid/support/v4/app/D;

    invoke-direct {v2, p1}, Landroid/support/v4/app/D;-><init>(Landroid/content/Context;)V

    .line 168
    const v3, 0x7f0200f2

    invoke-virtual {v2, v3}, Landroid/support/v4/app/D;->a(I)Landroid/support/v4/app/D;

    move-result-object v2

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/D;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v2

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/D;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/D;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/D;->a(J)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/D;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/D;->a()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 161
    :cond_5e
    const v2, 0x7f0b01c1

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2f
.end method
