.class public final Lcom/dropbox/android/util/L;
.super Ljava/lang/Object;
.source "panda.py"


# direct methods
.method public static a(Landroid/content/Context;)Lcom/dropbox/android/util/M;
    .registers 5
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    .line 35
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v1

    if-nez v1, :cond_3d

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->t()Z

    move-result v1

    if-nez v1, :cond_3d

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->w()Z

    move-result v1

    if-nez v1, :cond_3d

    invoke-static {p0}, Lcom/dropbox/android/util/u;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3d

    invoke-static {p0}, Lcom/dropbox/android/util/au;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 41
    invoke-static {p0}, Lcom/dropbox/android/util/L;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 42
    sget-object v1, Lcom/dropbox/android/util/aC;->a:Lcom/dropbox/android/util/aC;

    const/4 v2, 0x0

    invoke-static {p0, v1, v2, v3, v3}, Lcom/dropbox/android/service/NotificationService;->a(Landroid/content/Context;Lcom/dropbox/android/util/aC;Landroid/os/Bundle;ZZ)Z

    .line 43
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->j(Z)V

    .line 44
    sget-object v0, Lcom/dropbox/android/util/M;->c:Lcom/dropbox/android/util/M;

    .line 49
    :goto_39
    return-object v0

    .line 47
    :cond_3a
    sget-object v0, Lcom/dropbox/android/util/M;->b:Lcom/dropbox/android/util/M;

    goto :goto_39

    .line 49
    :cond_3d
    sget-object v0, Lcom/dropbox/android/util/M;->a:Lcom/dropbox/android/util/M;

    goto :goto_39
.end method

.method private static b(Landroid/content/Context;)Z
    .registers 18
    .parameter

    .prologue
    .line 54
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v3

    .line 56
    invoke-virtual {v3}, Ldbxyzptlk/l/m;->v()I

    move-result v1

    .line 57
    invoke-virtual {v3}, Ldbxyzptlk/l/m;->u()Ljava/util/Map;

    move-result-object v4

    .line 59
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 61
    invoke-static {}, Lcom/dropbox/android/filemanager/O;->a()[Lcom/dropbox/android/filemanager/O;

    move-result-object v6

    array-length v7, v6

    const/4 v0, 0x0

    move v2, v0

    :goto_17
    if-ge v2, v7, :cond_82

    aget-object v0, v6, v2

    .line 62
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/O;->b()Landroid/net/Uri;

    move-result-object v8

    .line 64
    invoke-static {v5, v8}, Lcom/dropbox/android/util/aw;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/util/aw;

    move-result-object v9

    .line 65
    if-nez v9, :cond_29

    .line 61
    :goto_25
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_17

    .line 70
    :cond_29
    :try_start_29
    const-string v0, "_id"

    invoke-virtual {v9, v0}, Lcom/dropbox/android/util/aw;->a(Ljava/lang/String;)I

    move-result v10

    .line 71
    const-string v0, "_data"

    invoke-virtual {v9, v0}, Lcom/dropbox/android/util/aw;->a(Ljava/lang/String;)I

    move-result v11

    .line 72
    :goto_35
    invoke-virtual {v9}, Lcom/dropbox/android/util/aw;->a()Z

    move-result v0

    if-eqz v0, :cond_56

    .line 73
    invoke-virtual {v9, v10}, Lcom/dropbox/android/util/aw;->a(I)J

    move-result-wide v12

    .line 74
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 75
    if-nez v0, :cond_5a

    .line 76
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 78
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_56
    .catchall {:try_start_29 .. :try_end_56} :catchall_7d

    .line 94
    :cond_56
    invoke-virtual {v9}, Lcom/dropbox/android/util/aw;->b()V

    goto :goto_25

    .line 82
    :cond_5a
    :try_start_5a
    invoke-virtual {v9, v11}, Lcom/dropbox/android/util/aw;->b(I)Ljava/lang/String;

    move-result-object v14

    .line 83
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    cmp-long v0, v12, v15

    if-lez v0, :cond_77

    invoke-static {v14}, Lcom/dropbox/android/util/av;->b(Ljava/lang/String;)Z
    :try_end_69
    .catchall {:try_start_5a .. :try_end_69} :catchall_7d

    move-result v0

    if-eqz v0, :cond_77

    .line 84
    add-int/lit8 v0, v1, 0x1

    .line 85
    const/16 v1, 0xa

    if-lt v0, v1, :cond_7b

    .line 86
    const/4 v0, 0x1

    .line 94
    invoke-virtual {v9}, Lcom/dropbox/android/util/aw;->b()V

    .line 102
    :goto_76
    return v0

    .line 90
    :cond_77
    :try_start_77
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7a
    .catchall {:try_start_77 .. :try_end_7a} :catchall_7d

    move v0, v1

    :cond_7b
    move v1, v0

    .line 92
    goto :goto_35

    .line 94
    :catchall_7d
    move-exception v0

    invoke-virtual {v9}, Lcom/dropbox/android/util/aw;->b()V

    throw v0

    .line 100
    :cond_82
    invoke-virtual {v3, v1}, Ldbxyzptlk/l/m;->a(I)V

    .line 101
    invoke-virtual {v3, v4}, Ldbxyzptlk/l/m;->a(Ljava/util/Map;)V

    .line 102
    const/4 v0, 0x0

    goto :goto_76
.end method
