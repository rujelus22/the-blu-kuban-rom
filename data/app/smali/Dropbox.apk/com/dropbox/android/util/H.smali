.class public Lcom/dropbox/android/util/H;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field protected e:[Ljava/lang/Object;

.field protected f:Ljava/util/TreeSet;

.field protected g:Ljava/util/TreeSet;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    const-class v0, Lcom/dropbox/android/util/H;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/H;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v0, p0, Lcom/dropbox/android/util/H;->a:I

    .line 17
    iput v0, p0, Lcom/dropbox/android/util/H;->b:I

    .line 19
    iput v0, p0, Lcom/dropbox/android/util/H;->c:I

    .line 20
    iput v0, p0, Lcom/dropbox/android/util/H;->d:I

    .line 23
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iput-object v1, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    .line 24
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iput-object v1, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    .line 30
    iput p1, p0, Lcom/dropbox/android/util/H;->a:I

    .line 31
    iget v1, p0, Lcom/dropbox/android/util/H;->a:I

    new-array v1, v1, [Ljava/lang/Object;

    iput-object v1, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    .line 32
    iput p2, p0, Lcom/dropbox/android/util/H;->b:I

    .line 35
    if-eqz p1, :cond_2a

    .line 36
    iput v0, p0, Lcom/dropbox/android/util/H;->c:I

    .line 37
    iput v0, p0, Lcom/dropbox/android/util/H;->d:I

    .line 40
    :cond_2a
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    iput-object v1, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    .line 41
    :goto_31
    if-ge v0, p1, :cond_3f

    .line 42
    iget-object v1, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_31

    .line 44
    :cond_3f
    return-void
.end method

.method private a(Ljava/util/TreeSet;I)Ljava/lang/Integer;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 147
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_11

    .line 148
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 152
    :goto_10
    return-object v0

    .line 151
    :cond_11
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/TreeSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_21

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_23

    :cond_21
    const/4 v0, 0x0

    goto :goto_10

    :cond_23
    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_10
.end method

.method private b(Ljava/util/TreeSet;I)Ljava/lang/Integer;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 158
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_11

    .line 159
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 163
    :goto_10
    return-object v0

    .line 162
    :cond_11
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/TreeSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_21

    invoke-interface {v0}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_23

    :cond_21
    const/4 v0, 0x0

    goto :goto_10

    :cond_23
    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_10
.end method

.method private d()V
    .registers 4

    .prologue
    .line 172
    :goto_0
    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    iget v1, p0, Lcom/dropbox/android/util/H;->b:I

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_7d

    .line 174
    invoke-direct {p0}, Lcom/dropbox/android/util/H;->e()Lcom/dropbox/android/util/J;

    move-result-object v1

    .line 175
    iget v2, v1, Lcom/dropbox/android/util/J;->a:I

    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v1, v1, Lcom/dropbox/android/util/J;->b:I

    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lt v2, v0, :cond_72

    const/4 v0, 0x1

    .line 176
    :goto_3b
    if-eqz v0, :cond_74

    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_45
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, v1

    instance-of v0, v0, Lcom/dropbox/android/util/E;

    if-eqz v0, :cond_5a

    .line 179
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Lcom/dropbox/android/util/E;

    invoke-virtual {v0}, Lcom/dropbox/android/util/E;->b()V

    .line 181
    :cond_5a
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 182
    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    :cond_72
    const/4 v0, 0x0

    goto :goto_3b

    .line 176
    :cond_74
    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_45

    .line 185
    :cond_7d
    return-void
.end method

.method private e()Lcom/dropbox/android/util/J;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 242
    iget v0, p0, Lcom/dropbox/android/util/H;->c:I

    iget v1, p0, Lcom/dropbox/android/util/H;->b:I

    iget v2, p0, Lcom/dropbox/android/util/H;->d:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 243
    iget v1, p0, Lcom/dropbox/android/util/H;->b:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v1, v0

    iget v2, p0, Lcom/dropbox/android/util/H;->a:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 244
    iget v2, p0, Lcom/dropbox/android/util/H;->a:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2c

    .line 248
    iget v0, p0, Lcom/dropbox/android/util/H;->b:I

    add-int/lit8 v0, v0, -0x1

    sub-int v0, v1, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 251
    :cond_2c
    new-instance v2, Lcom/dropbox/android/util/J;

    invoke-direct {v2, v0, v1}, Lcom/dropbox/android/util/J;-><init>(II)V

    return-object v2
.end method


# virtual methods
.method public final a()V
    .registers 5

    .prologue
    .line 62
    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 63
    iget-object v1, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, v1, v3

    instance-of v1, v1, Lcom/dropbox/android/util/E;

    if-eqz v1, :cond_2b

    .line 64
    iget-object v1, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, v1, v3

    check-cast v1, Lcom/dropbox/android/util/E;

    invoke-virtual {v1}, Lcom/dropbox/android/util/E;->b()V

    .line 66
    :cond_2b
    iget-object v1, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x0

    aput-object v3, v1, v0

    goto :goto_6

    .line 68
    :cond_35
    iget-object v0, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    iget-object v1, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 69
    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 70
    return-void
.end method

.method public final a(I)V
    .registers 2
    .parameter

    .prologue
    .line 55
    iput p1, p0, Lcom/dropbox/android/util/H;->b:I

    .line 56
    invoke-direct {p0}, Lcom/dropbox/android/util/H;->d()V

    .line 57
    return-void
.end method

.method public final a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 73
    iget v0, p0, Lcom/dropbox/android/util/H;->b:I

    if-le p2, v0, :cond_6

    .line 77
    iput p2, p0, Lcom/dropbox/android/util/H;->b:I

    .line 79
    :cond_6
    iput p1, p0, Lcom/dropbox/android/util/H;->c:I

    .line 80
    iput p2, p0, Lcom/dropbox/android/util/H;->d:I

    .line 81
    return-void
.end method

.method public final a(ILcom/dropbox/android/util/E;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-virtual {p2}, Lcom/dropbox/android/util/E;->a()V

    .line 89
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/util/H;->a(ILjava/lang/Object;)V

    .line 90
    return-void
.end method

.method protected final a(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, p1

    if-nez v0, :cond_20

    .line 94
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 108
    :goto_1c
    invoke-direct {p0}, Lcom/dropbox/android/util/H;->d()V

    .line 109
    return-void

    .line 97
    :cond_20
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, p1

    instance-of v0, v0, Lcom/dropbox/android/util/E;

    if-eqz v0, :cond_38

    .line 99
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Lcom/dropbox/android/util/E;

    check-cast v0, Lcom/dropbox/android/util/E;

    invoke-virtual {v0}, Lcom/dropbox/android/util/E;->b()V

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aput-object p2, v0, p1

    goto :goto_1c

    .line 101
    :cond_38
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, p1

    instance-of v0, v0, Lcom/dropbox/android/util/K;

    if-eqz v0, :cond_45

    .line 103
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aput-object p2, v0, p1

    goto :goto_1c

    .line 105
    :cond_45
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "how did a non node or bitmap get in here!?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()I
    .registers 7

    .prologue
    const/4 v2, -0x1

    .line 189
    iget-object v0, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    iget v1, p0, Lcom/dropbox/android/util/H;->c:I

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/util/H;->a(Ljava/util/TreeSet;I)Ljava/lang/Integer;

    move-result-object v0

    .line 190
    iget v1, p0, Lcom/dropbox/android/util/H;->c:I

    iget v3, p0, Lcom/dropbox/android/util/H;->d:I

    add-int/2addr v3, v1

    .line 192
    if-eqz v0, :cond_1b

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v1, v3, :cond_1b

    .line 193
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 226
    :cond_1a
    :goto_1a
    return v2

    .line 197
    :cond_1b
    iget-object v0, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    iget v1, p0, Lcom/dropbox/android/util/H;->c:I

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/util/H;->b(Ljava/util/TreeSet;I)Ljava/lang/Integer;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/dropbox/android/util/H;->g:Ljava/util/TreeSet;

    invoke-direct {p0, v1, v3}, Lcom/dropbox/android/util/H;->a(Ljava/util/TreeSet;I)Ljava/lang/Integer;

    move-result-object v1

    .line 199
    iget-object v4, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v4}, Ljava/util/TreeSet;->size()I

    move-result v4

    iget v5, p0, Lcom/dropbox/android/util/H;->b:I

    if-ge v4, v5, :cond_5b

    .line 200
    if-eqz v0, :cond_4d

    if-eqz v1, :cond_4d

    .line 201
    iget v2, p0, Lcom/dropbox/android/util/H;->c:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int v3, v4, v3

    if-ge v2, v3, :cond_4b

    :goto_46
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1a

    :cond_4b
    move-object v0, v1

    goto :goto_46

    .line 202
    :cond_4d
    if-eqz v0, :cond_54

    .line 203
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1a

    .line 204
    :cond_54
    if-eqz v1, :cond_1a

    .line 205
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1a

    .line 211
    :cond_5b
    if-eqz v0, :cond_85

    if-eqz v1, :cond_85

    .line 212
    iget v4, p0, Lcom/dropbox/android/util/H;->c:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int v3, v5, v3

    if-ge v4, v3, :cond_83

    :goto_6e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 220
    :goto_72
    if-eq v0, v2, :cond_81

    .line 221
    invoke-direct {p0}, Lcom/dropbox/android/util/H;->e()Lcom/dropbox/android/util/J;

    move-result-object v1

    .line 222
    iget v3, v1, Lcom/dropbox/android/util/J;->a:I

    if-lt v0, v3, :cond_80

    iget v1, v1, Lcom/dropbox/android/util/J;->b:I

    if-le v0, v1, :cond_81

    :cond_80
    move v0, v2

    :cond_81
    move v2, v0

    .line 226
    goto :goto_1a

    :cond_83
    move-object v0, v1

    .line 212
    goto :goto_6e

    .line 213
    :cond_85
    if-eqz v0, :cond_8c

    .line 214
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_72

    .line 215
    :cond_8c
    if-eqz v1, :cond_93

    .line 216
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_72

    :cond_93
    move v0, v2

    .line 218
    goto :goto_72
.end method

.method public final b(I)V
    .registers 4
    .parameter

    .prologue
    .line 84
    new-instance v0, Lcom/dropbox/android/util/K;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/K;-><init>(Lcom/dropbox/android/util/I;)V

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/util/H;->a(ILjava/lang/Object;)V

    .line 85
    return-void
.end method

.method public final c(I)Lcom/dropbox/android/util/E;
    .registers 3
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, p1

    instance-of v0, v0, Lcom/dropbox/android/util/E;

    if-eqz v0, :cond_12

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Lcom/dropbox/android/util/E;

    .line 114
    invoke-virtual {v0}, Lcom/dropbox/android/util/E;->a()V

    .line 117
    :goto_11
    return-object v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final c()Ljava/util/HashMap;
    .registers 6

    .prologue
    .line 259
    new-instance v1, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 260
    iget-object v0, p0, Lcom/dropbox/android/util/H;->f:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 261
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, v3

    instance-of v0, v0, Lcom/dropbox/android/util/E;

    if-eqz v0, :cond_11

    .line 262
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, Lcom/dropbox/android/util/E;

    invoke-virtual {v0}, Lcom/dropbox/android/util/E;->a()V

    .line 263
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, Lcom/dropbox/android/util/E;

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_11

    .line 266
    :cond_40
    return-object v1
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v0, v0, p1

    instance-of v0, v0, Lcom/dropbox/android/util/E;

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 127
    const-string v1, "["

    .line 128
    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_6
    iget-object v2, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v1, v2, :cond_a4

    .line 129
    if-lez v1, :cond_20

    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    :cond_20
    iget-object v2, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v2, v2, v1

    if-nez v2, :cond_40

    .line 134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": null"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    :goto_3d
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 135
    :cond_40
    iget-object v2, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v2, v2, v1

    instance-of v2, v2, Lcom/dropbox/android/util/E;

    if-eqz v2, :cond_80

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x40

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3d

    .line 138
    :cond_80
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/util/H;->e:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3d

    .line 141
    :cond_a4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    return-object v0
.end method
