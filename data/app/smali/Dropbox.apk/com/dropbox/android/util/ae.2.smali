.class public final Lcom/dropbox/android/util/ae;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/util/HashMap;

.field private static final d:Ljava/util/HashMap;

.field private static final e:Ljava/util/HashMap;

.field private static final f:Ljava/util/HashMap;

.field private static final g:Ljava/util/HashMap;

.field private static final h:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 32
    const-class v0, Lcom/dropbox/android/util/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_527

    move v0, v1

    :goto_a
    sput-boolean v0, Lcom/dropbox/android/util/ae;->a:Z

    .line 36
    const-class v0, Lcom/dropbox/android/util/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ae;->c:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ae;->d:Ljava/util/HashMap;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ae;->e:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ae;->f:Ljava/util/HashMap;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ae;->g:Ljava/util/HashMap;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    .line 47
    const-string v0, "htm"

    const-string v2, "text/html"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v0, "html"

    const-string v2, "text/html"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v0, "asc"

    const-string v2, "text/plain"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v0, "txt"

    const-string v2, "text/plain"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v0, "xsl"

    const-string v2, "text/xml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v0, "xml"

    const-string v2, "text/xml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "css"

    const-string v2, "text/css"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "rtx"

    const-string v2, "text/richtext"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "rtf"

    const-string v2, "text/rtf"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "sgm"

    const-string v2, "text/sgml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v0, "sgml"

    const-string v2, "text/sgml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v0, "tsv"

    const-string v2, "text/tab-separated-values"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v0, "csv"

    const-string v2, "text/comma-separated-values"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v0, "vnt"

    const-string v2, "text/x-vnote"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v0, "doc"

    const-string v2, "application/msword"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v0, "dot"

    const-string v2, "application/msword"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v0, "docx"

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v0, "dotx"

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v0, "docm"

    const-string v2, "application/vnd.ms-word.document.macroEnabled.12"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v0, "dotm"

    const-string v2, "application/vnd.ms-word.template.macroEnabled.12"

    const-string v3, "page_white_word"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v0, "xls"

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v0, "xlt"

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v0, "xla"

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v0, "xlsx"

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v0, "xltx"

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v0, "xlsm"

    const-string v2, "application/vnd.ms-excel.sheet.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v0, "xltm"

    const-string v2, "application/vnd.ms-excel.template.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v0, "xlam"

    const-string v2, "application/vnd.ms-excel.addin.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v0, "xlsb"

    const-string v2, "application/vnd.ms-excel.sheet.binary.macroEnabled.12"

    const-string v3, "page_white_excel"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v0, "ppt"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v0, "pot"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, "pps"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "ppa"

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "pptx"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v0, "potx"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v0, "ppsx"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v0, "ppam"

    const-string v2, "application/vnd.ms-powerpoint.addin.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v0, "pptm"

    const-string v2, "application/vnd.ms-powerpoint.presentation.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v0, "potm"

    const-string v2, "application/vnd.ms-powerpoint.template.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v0, "ppsm"

    const-string v2, "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"

    const-string v3, "page_white_powerpoint"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "pdf"

    const-string v2, "application/pdf"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v0, "ps"

    const-string v2, "application/postscript"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v0, "ai"

    const-string v2, "application/postscript"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v0, "eps"

    const-string v2, "application/postscript"

    const-string v3, "page_white_acrobat"

    invoke-static {v0, v2, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v0, "xml"

    const-string v2, "application/xml"

    const-string v3, "page_white_text"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 101
    const-string v0, "js"

    const-string v2, "application/javascript"

    const-string v3, "page_white_js"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 102
    const-string v0, "js"

    const-string v2, "application/x-javascript"

    const-string v3, "page_white_js"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 103
    const-string v0, "latex"

    const-string v2, "application/x-latex"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 104
    const-string v0, "sh"

    const-string v2, "application/x-sh"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 105
    const-string v0, "tex"

    const-string v2, "application/x-tex"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 106
    const-string v0, "texi"

    const-string v2, "application/x-texinfo"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 107
    const-string v0, "texinfo"

    const-string v2, "application/x-texinfo"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 108
    const-string v0, "tcl"

    const-string v2, "application/x-tcl"

    const-string v3, "page_white_code"

    invoke-static {v0, v2, v3, v1}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 111
    const-string v0, "tar"

    const-string v1, "application/x-tar"

    const-string v2, "page_white_compressed"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "zip"

    const-string v1, "application/zip"

    const-string v2, "page_white_compressed"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "tar"

    const-string v1, "application/x-tar"

    const-string v2, "page_white_compressed"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v0, "tar"

    const-string v1, "application/x-tar"

    const-string v2, "page_white_compressed"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v0, "tar"

    const-string v1, "application/x-tar"

    const-string v2, "page_white_compressed"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v0, "swf"

    const-string v1, "application/x-shockwave-flash"

    const-string v2, "page_white_flash"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v0, "bmp"

    const-string v1, "image/bmp"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v0, "gif"

    const-string v1, "image/gif"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v0, "jpg"

    const-string v1, "image/jpeg"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "jpe"

    const-string v1, "image/jpeg"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v0, "jpeg"

    const-string v1, "image/jpeg"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v0, "png"

    const-string v1, "image/png"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "tif"

    const-string v1, "image/tiff"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v0, "tiff"

    const-string v1, "image/tiff"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v0, "pnm"

    const-string v1, "image/x-portable-anymap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v0, "pbm"

    const-string v1, "image/x-portable-bitmap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v0, "pgm"

    const-string v1, "image/x-portable-graymap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "xbm"

    const-string v1, "image/x-xbitmap"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v0, "xwd"

    const-string v1, "image/x-xwindowdump"

    const-string v2, "page_white_picture"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v0, "wrl"

    const-string v1, "model/vrml"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v0, "vrml"

    const-string v1, "model/vrml"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "mpg"

    const-string v1, "video/m3peg"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "mpe"

    const-string v1, "video/mpeg"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "mpeg"

    const-string v1, "video/mpeg"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v0, "mov"

    const-string v1, "video/quicktime"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v0, "qt"

    const-string v1, "video/quicktime"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v0, "mxu"

    const-string v1, "video/vnd.mpegurl"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v0, "avi"

    const-string v1, "video/x-msvideo"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v0, "movie"

    const-string v1, "video/x-sgi-movie"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v0, "3gp"

    const-string v1, "video/3gpp"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "3gpp"

    const-string v1, "video/3gpp"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "mkv"

    const-string v1, "video/x-matroska"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "au"

    const-string v1, "audio/basic"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v0, "snd"

    const-string v1, "audio/basic"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v0, "mid"

    const-string v1, "audio/midi"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v0, "midi"

    const-string v1, "audio/midi"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "kar"

    const-string v1, "audio/midi"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v0, "aif"

    const-string v1, "audio/x-aiff"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v0, "aiff"

    const-string v1, "audio/x-aiff"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v0, "aifc"

    const-string v1, "audio/x-aiff"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v0, "mpga"

    const-string v1, "audio/mpeg"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v0, "mp2"

    const-string v1, "audio/mpeg"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v0, "mp3"

    const-string v1, "audio/mpeg"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v0, "mp4"

    const-string v1, "audio/mp4"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v0, "m3u"

    const-string v1, "audio/x-mpegurl"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v0, "ram"

    const-string v1, "audio/x-pn-realaudio"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v0, "rm"

    const-string v1, "audio/x-pn-realaudio"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v0, "ra"

    const-string v1, "audio/x-realaudio"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v0, "wav"

    const-string v1, "audio/x-wav"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "amr"

    const-string v1, "audio/3gpp"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v0, "mka"

    const-string v1, "audio/x-matroska"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v0, "3ga"

    const-string v1, "audio/3ga"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v0, "wma"

    const-string v1, "audio/x-ms-wma"

    const-string v2, "page_white_sound"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "wmv"

    const-string v1, "audio/x-ms-wmv"

    const-string v2, "film"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v0, "url"

    const-string v1, "text/url"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v0, "apk"

    const-string v1, "application/vnd.android.package-archive"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "epub"

    const-string v1, "application/epub+zip"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "prc"

    const-string v1, "application/x-pilot-prc"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "mobi"

    const-string v1, "application/x-mobipocket-ebook"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v0, "cbr"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v0, "cbz"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v0, "cbt"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v0, "cba"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v0, "cb7"

    const-string v1, "application/x-cbr"

    const-string v2, "page_white"

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/bmp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/gif"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/png"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/tiff"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 198
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/x-portable-anymap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 199
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/x-portable-bitmap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 200
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/x-portable-graymap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 201
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/x-xbitmap"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 202
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    const-string v1, "image/x-xwindowdump"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v0, "apk"

    const-string v1, "application/vnd.android.package-archive"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v0, "wma"

    const-string v1, "audio/x-ms-wma"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string v0, "url"

    const-string v1, "application/x-url"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v0, "epub"

    const-string v1, "application/epub+zip"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v0, "prc"

    const-string v1, "application/x-pilot-prc"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v0, "mobi"

    const-string v1, "application/x-mobipocket-ebook"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v0, "psafe3"

    const-string v1, "application/application/x-psafe"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "cbr"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v0, "cbz"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v0, "cbt"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v0, "cba"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v0, "cb7"

    const-string v1, "application/x-cbr"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v0, "mmap"

    const-string v1, "application/vnd.mindjet.mindmanager"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v0, "xmmap"

    const-string v1, "application/vnd.mindjet.mindmanager"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v0, "mm"

    const-string v1, "application/x-freemind"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v0, "rmvb"

    const-string v1, "video/x-rmvb"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "aa"

    const-string v1, "audio/audible"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "aax"

    const-string v1, "audio/vnd.audible.aax"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v0, "bmml"

    const-string v1, "text/vnd.balsamiq.bmml"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v0, "vnt"

    const-string v1, "text/x-vnote"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    return-void

    .line 32
    :cond_527
    const/4 v0, 0x0

    goto/16 :goto_a
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 248
    sget-object v0, Lcom/dropbox/android/util/ae;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 371
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 372
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 374
    sget-object v1, Lcom/dropbox/android/util/ae;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 375
    if-nez v0, :cond_1b

    .line 378
    :goto_1a
    return-object p1

    :cond_1b
    move-object p1, v0

    goto :goto_1a
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/HashMap;
    .registers 5
    .parameter

    .prologue
    .line 682
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 683
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 684
    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    .line 686
    :cond_25
    return-object v1
.end method

.method public static a(Landroid/database/Cursor;)Ljava/util/Set;
    .registers 4
    .parameter

    .prologue
    .line 691
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 693
    if-eqz p0, :cond_34

    :try_start_7
    invoke-interface {p0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_34

    .line 694
    const/4 v1, -0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 695
    :cond_11
    :goto_11
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 696
    invoke-static {p0}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v1

    .line 697
    sget-object v2, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    if-ne v1, v2, :cond_11

    .line 698
    const-string v1, "_display_name"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 699
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 700
    if-eqz v1, :cond_11

    .line 701
    invoke-static {v1}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_32
    .catch Landroid/database/StaleDataException; {:try_start_7 .. :try_end_32} :catch_33

    goto :goto_11

    .line 706
    :catch_33
    move-exception v1

    .line 710
    :cond_34
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    const-string v1, "text/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 230
    const/4 v0, 0x1

    .line 232
    :cond_a
    invoke-static {p0, p1, p2, v0}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 233
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 236
    sget-object v0, Lcom/dropbox/android/util/ae;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/dropbox/android/util/ae;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/dropbox/android/util/ae;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/dropbox/android/util/ae;->g:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    return-void
.end method

.method public static a(Landroid/net/Uri;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 382
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    .line 386
    :cond_7
    :goto_7
    return v0

    .line 385
    :cond_8
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 386
    const-string v2, "https"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    array-length v2, v1

    const/4 v3, 0x2

    if-lt v2, v3, :cond_7

    const-string v2, "dropbox"

    array-length v3, v1

    add-int/lit8 v3, v3, -0x2

    aget-object v3, v1, v3

    invoke-static {v2, v3}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "com"

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    invoke-static {v2, v1}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method public static a(Landroid/net/Uri;Landroid/content/Context;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 605
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 606
    if-eqz v2, :cond_10

    const-string v3, "file"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 608
    :cond_10
    :try_start_10
    invoke-static {p0}, Lcom/dropbox/android/util/ae;->d(Landroid/net/Uri;)Ljava/io/File;
    :try_end_13
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_13} :catch_1b

    move-result-object v2

    if-nez v2, :cond_17

    .line 615
    :goto_16
    return v0

    :cond_17
    move v0, v1

    .line 608
    goto :goto_16

    :cond_19
    move v0, v1

    .line 615
    goto :goto_16

    .line 609
    :catch_1b
    move-exception v1

    goto :goto_16
.end method

.method public static a(Ldbxyzptlk/n/k;)Z
    .registers 3
    .parameter

    .prologue
    .line 285
    iget-boolean v0, p0, Ldbxyzptlk/n/k;->s:Z

    if-eqz v0, :cond_1e

    iget-object v0, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    iget-object v0, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    const-string v1, "video/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    :cond_1c
    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public static a(Ljava/io/File;)Z
    .registers 2
    .parameter

    .prologue
    .line 632
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->s(Ljava/lang/String;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_7} :catch_9

    move-result v0

    .line 634
    :goto_8
    return v0

    .line 633
    :catch_9
    move-exception v0

    .line 634
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->s(Ljava/lang/String;)Z

    move-result v0

    goto :goto_8
.end method

.method public static b(Landroid/net/Uri;)Landroid/net/Uri;
    .registers 4
    .parameter

    .prologue
    .line 408
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 409
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 410
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 411
    return-object v0
.end method

.method private static b(Ljava/io/File;)Ljava/io/File;
    .registers 3
    .parameter

    .prologue
    .line 639
    invoke-static {p0}, Lcom/dropbox/android/util/ae;->c(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Security exception, a filename we don\'t allow was attempting to be uploaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 641
    sget-object v1, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 645
    :cond_2c
    return-object p0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 243
    sget-object v0, Lcom/dropbox/android/util/ae;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 252
    sget-object v0, Lcom/dropbox/android/util/ae;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 253
    sget-object v0, Lcom/dropbox/android/util/ae;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 255
    :goto_14
    return v0

    :cond_15
    const-string v0, "text/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_14
.end method

.method public static c(Landroid/net/Uri;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 455
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 456
    if-nez v0, :cond_15

    .line 458
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 535
    :cond_14
    :goto_14
    return-object v0

    .line 459
    :cond_15
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 461
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_14

    .line 462
    :cond_2b
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.dropbox.android.Dropbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 464
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/at;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_14

    .line 466
    :cond_4d
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 468
    if-eqz v0, :cond_63

    .line 469
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 470
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_63

    .line 471
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_14

    .line 485
    :cond_63
    :try_start_63
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 486
    if-eqz v1, :cond_11f

    .line 487
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_11d

    .line 488
    const-string v0, "_display_name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 489
    if-ltz v0, :cond_88

    .line 490
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_87
    .catch Ljava/lang/SecurityException; {:try_start_63 .. :try_end_87} :catch_c8
    .catch Ljava/lang/RuntimeException; {:try_start_63 .. :try_end_87} :catch_e7

    move-result-object v6

    .line 493
    :cond_88
    :try_start_88
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 494
    if-ltz v0, :cond_11b

    .line 503
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 504
    if-eqz v0, :cond_11b

    .line 505
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 506
    if-eqz v6, :cond_b1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_11b

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_ae
    .catch Ljava/lang/SecurityException; {:try_start_88 .. :try_end_ae} :catch_116
    .catch Ljava/lang/RuntimeException; {:try_start_88 .. :try_end_ae} :catch_110

    move-result v2

    if-eqz v2, :cond_11b

    .line 514
    :cond_b1
    :goto_b1
    :try_start_b1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_b4
    .catch Ljava/lang/SecurityException; {:try_start_b1 .. :try_end_b4} :catch_119
    .catch Ljava/lang/RuntimeException; {:try_start_b1 .. :try_end_b4} :catch_114

    .line 527
    :goto_b4
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_be

    .line 528
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 531
    :cond_be
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 532
    const-string v0, "unknown"

    goto/16 :goto_14

    .line 517
    :catch_c8
    move-exception v0

    move-object v0, v6

    .line 519
    :goto_ca
    sget-object v1, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception, couldn\'t get filename from URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b4

    .line 520
    :catch_e7
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    .line 522
    :goto_ea
    sget-object v2, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get filename from URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v1, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    goto :goto_b4

    .line 520
    :catch_110
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    goto :goto_ea

    :catch_114
    move-exception v1

    goto :goto_ea

    .line 517
    :catch_116
    move-exception v0

    move-object v0, v6

    goto :goto_ca

    :catch_119
    move-exception v1

    goto :goto_ca

    :cond_11b
    move-object v0, v6

    goto :goto_b1

    :cond_11d
    move-object v0, v6

    goto :goto_b1

    :cond_11f
    move-object v0, v6

    goto :goto_b4
.end method

.method private static c(Ljava/io/File;)Z
    .registers 3
    .parameter

    .prologue
    .line 654
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 656
    :try_start_c
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_13} :catch_15

    move-result v0

    .line 659
    :goto_14
    return v0

    .line 657
    :catch_15
    move-exception v0

    .line 659
    const/4 v0, 0x1

    goto :goto_14
.end method

.method public static c(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 260
    const-string v0, "text/html"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static d(Landroid/net/Uri;)Ljava/io/File;
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 548
    .line 550
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 551
    if-nez v0, :cond_2a

    .line 553
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 588
    :goto_b
    if-nez v0, :cond_df

    .line 589
    sget-object v0, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to determine source File from uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :goto_29
    return-object v6

    .line 554
    :cond_2a
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_37

    .line 555
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 556
    :cond_37
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.dropbox.android.Dropbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 558
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 559
    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/at;->b()Ljava/io/File;

    move-result-object v6

    goto :goto_29

    .line 562
    :cond_59
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 563
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 564
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 565
    invoke-static {v1}, Lcom/dropbox/android/util/ae;->b(Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    goto :goto_29

    .line 569
    :cond_6d
    const/4 v0, 0x1

    :try_start_6e
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 570
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 571
    if-eqz v1, :cond_116

    .line 572
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_113

    .line 573
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_91
    .catch Ljava/lang/SecurityException; {:try_start_6e .. :try_end_91} :catch_97
    .catch Ljava/lang/RuntimeException; {:try_start_6e .. :try_end_91} :catch_b5

    move-result-object v0

    .line 575
    :goto_92
    :try_start_92
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_95
    .catch Ljava/lang/SecurityException; {:try_start_92 .. :try_end_95} :catch_97
    .catch Ljava/lang/RuntimeException; {:try_start_92 .. :try_end_95} :catch_10e

    goto/16 :goto_b

    .line 577
    :catch_97
    move-exception v0

    .line 579
    sget-object v1, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception, couldn\'t get filename from URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    throw v0

    .line 581
    :catch_b5
    move-exception v0

    move-object v1, v6

    .line 582
    :goto_b7
    sget-object v2, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get filename from content provider for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 583
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v0, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    move-object v0, v1

    goto/16 :goto_b

    .line 591
    :cond_df
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 592
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_f0

    .line 593
    invoke-static {v1}, Lcom/dropbox/android/util/ae;->b(Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    goto/16 :goto_29

    .line 595
    :cond_f0
    sget-object v1, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File doesn\'t exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_29

    .line 581
    :catch_10e
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_b7

    :cond_113
    move-object v0, v6

    goto/16 :goto_92

    :cond_116
    move-object v0, v6

    goto/16 :goto_b
.end method

.method public static d(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 264
    const-string v0, "application/x-url"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/net/Uri;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 754
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 755
    if-nez v0, :cond_51

    .line 756
    const-string v0, ""

    .line 760
    :goto_8
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 761
    if-nez v1, :cond_10

    .line 762
    const-string v1, ""

    .line 764
    :cond_10
    const-string v2, ""

    .line 765
    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    .line 766
    if-eqz v3, :cond_2f

    .line 767
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 769
    :cond_2f
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 758
    :cond_51
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method public static e(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 268
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 269
    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    .line 270
    const/4 v0, 0x0

    .line 273
    :goto_a
    return v0

    :cond_b
    const-string v1, "webloc"

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_a
.end method

.method public static f(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 277
    if-nez p0, :cond_4

    .line 281
    :cond_3
    :goto_3
    return v0

    :cond_4
    const-string v1, "audio/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, "video/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_14
    const/4 v0, 0x1

    goto :goto_3
.end method

.method public static g(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 292
    .line 293
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 294
    const/4 v0, -0x1

    if-le v1, v0, :cond_13

    .line 295
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 297
    :cond_13
    sget-object v0, Lcom/dropbox/android/util/ae;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 298
    if-nez v0, :cond_2c

    if-lez v1, :cond_2c

    .line 299
    invoke-static {}, Lcom/dropbox/android/util/i;->I()Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "ext"

    invoke-virtual {v1, v2, p0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    .line 301
    :cond_2c
    return-object v0
.end method

.method public static h(Ljava/lang/String;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 305
    if-nez p0, :cond_4

    .line 309
    :cond_3
    :goto_3
    return v0

    .line 308
    :cond_4
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 309
    aget-object v2, v1, v0

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    aget-object v1, v1, v0

    const-string v2, "image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1e
    const/4 v0, 0x1

    goto :goto_3
.end method

.method public static i(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 314
    if-eqz p0, :cond_c

    const-string v0, "video/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static j(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 318
    if-eqz p0, :cond_c

    const-string v0, "image/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static k(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 322
    sget-object v0, Lcom/dropbox/android/util/ae;->h:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static l(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 326
    if-eqz p0, :cond_c

    const-string v0, "audio/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static m(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 330
    invoke-static {p0}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public static n(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 344
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 345
    if-eqz v1, :cond_13

    .line 346
    sget-object v1, Lcom/dropbox/android/util/ae;->b:Ljava/lang/String;

    const-string v2, "Transcoding disabled; HTC."

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :cond_12
    return v0

    .line 352
    :cond_13
    const/16 v1, 0xe

    invoke-static {v1}, Lcom/dropbox/android/util/bj;->a(I)Z

    move-result v1

    if-eqz v1, :cond_22

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    const/4 v0, 0x1

    .line 353
    :cond_22
    if-eqz v0, :cond_12

    .line 354
    sget-boolean v1, Lcom/dropbox/android/util/ae;->a:Z

    if-nez v1, :cond_12

    invoke-static {p0}, Lcom/dropbox/android/util/ae;->m(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static o(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 362
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 363
    const/4 v1, -0x1

    if-le v0, v1, :cond_f

    .line 364
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 366
    :cond_f
    sget-object v0, Lcom/dropbox/android/util/ae;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static p(Ljava/lang/String;)Landroid/util/Pair;
    .registers 5
    .parameter

    .prologue
    .line 395
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 396
    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public static q(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 402
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 403
    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static r(Ljava/lang/String;)Landroid/net/Uri;
    .registers 2
    .parameter

    .prologue
    .line 435
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 437
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 439
    :goto_11
    return-object v0

    :cond_12
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_11
.end method

.method public static s(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 619
    invoke-static {}, Lcom/dropbox/android/util/af;->j()Ljava/io/File;

    move-result-object v0

    .line 622
    :try_start_4
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_7} :catch_d

    move-result-object v0

    .line 627
    :goto_8
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 623
    :catch_d
    move-exception v1

    .line 624
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method public static t(Ljava/lang/String;)Landroid/util/Pair;
    .registers 4
    .parameter

    .prologue
    .line 672
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 673
    if-gtz v1, :cond_10

    .line 674
    new-instance v0, Landroid/util/Pair;

    const-string v1, ""

    invoke-direct {v0, p0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 676
    :goto_f
    return-object v0

    :cond_10
    new-instance v0, Landroid/util/Pair;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_f
.end method

.method public static u(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 714
    const-string v0, "[\\000-\\037]|\\\\|/|:|\\?|\\*|<|>|\"|\\|"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static v(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 725
    const-string v0, "/"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 727
    const/4 v0, 0x0

    :goto_7
    array-length v2, v1

    if-ge v0, v2, :cond_1d

    .line 728
    aget-object v2, v1, v0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 729
    aget-object v2, v1, v0

    invoke-static {v2}, Lcom/dropbox/android/util/ae;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 727
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 733
    :cond_1d
    const-string v0, "/"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static w(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 742
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%x"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
