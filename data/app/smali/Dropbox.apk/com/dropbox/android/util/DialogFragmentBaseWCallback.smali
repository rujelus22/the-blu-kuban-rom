.class public Lcom/dropbox/android/util/DialogFragmentBaseWCallback;
.super Lcom/actionbarsherlock/app/SherlockDialogFragment;
.source "panda.py"


# instance fields
.field protected b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .registers 5
    .parameter

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 14
    :try_start_3
    iput-object p1, p0, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;->b:Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_5} :catch_6

    .line 18
    return-void

    .line 15
    :catch_6
    move-exception v0

    .line 16
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement correct DialogCallback"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onDestroyView()V
    .registers 3

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 30
    invoke-virtual {p0}, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 32
    :cond_14
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onDestroyView()V

    .line 33
    return-void
.end method

.method public onDetach()V
    .registers 2

    .prologue
    .line 22
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onDetach()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/DialogFragmentBaseWCallback;->b:Ljava/lang/Object;

    .line 24
    return-void
.end method
