.class Lcom/dropbox/android/service/p;
.super Lcom/dropbox/android/service/h;
.source "panda.py"


# instance fields
.field final synthetic b:Lcom/dropbox/android/service/CameraUploadService;

.field private final c:Ljava/util/HashSet;

.field private d:J


# direct methods
.method public constructor <init>(Lcom/dropbox/android/service/CameraUploadService;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 521
    iput-object p1, p0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    .line 522
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/service/h;-><init>(Lcom/dropbox/android/service/CameraUploadService;I)V

    .line 515
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/p;->c:Ljava/util/HashSet;

    .line 516
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/service/p;->d:J

    .line 523
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashMap;
    .registers 10
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 867
    const-string v1, "camera_upload"

    sget-object v2, Lcom/dropbox/android/service/k;->a:[Ljava/lang/String;

    const-string v3, "local_hash IS NOT NULL"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 870
    :try_start_f
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 871
    :goto_18
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_30

    .line 872
    new-instance v2, Lcom/dropbox/android/service/k;

    invoke-direct {v2, v1}, Lcom/dropbox/android/service/k;-><init>(Landroid/database/Cursor;)V

    .line 873
    invoke-static {v2}, Lcom/dropbox/android/service/k;->e(Lcom/dropbox/android/service/k;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2a
    .catchall {:try_start_f .. :try_end_2a} :catchall_2b

    goto :goto_18

    .line 876
    :catchall_2b
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_30
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 878
    return-object v0
.end method


# virtual methods
.method protected final a()V
    .registers 42

    .prologue
    .line 532
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setPriority(I)V

    .line 533
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Starting camera upload scan..."

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    const-class v17, Lcom/dropbox/android/service/p;

    monitor-enter v17

    .line 536
    :try_start_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->c(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6
    :try_end_1d
    .catchall {:try_start_14 .. :try_end_1d} :catchall_63

    .line 537
    const-wide/16 v4, 0x0

    .line 538
    :try_start_1f
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/dropbox/android/service/p;->d:J

    const-wide/16 v7, 0xbb8

    add-long/2addr v2, v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_29
    .catchall {:try_start_1f .. :try_end_29} :catchall_86

    move-result-wide v7

    sub-long/2addr v2, v7

    .line 539
    :goto_2b
    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-lez v7, :cond_5a

    const-wide/32 v7, 0xea60

    cmp-long v7, v4, v7

    if-gez v7, :cond_5a

    .line 545
    :try_start_38
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v7}, Lcom/dropbox/android/service/CameraUploadService;->c(Lcom/dropbox/android/service/CameraUploadService;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_43
    .catchall {:try_start_38 .. :try_end_43} :catchall_86
    .catch Ljava/lang/InterruptedException; {:try_start_38 .. :try_end_43} :catch_38b

    .line 551
    :goto_43
    :try_start_43
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/service/p;->b()Z

    move-result v7

    if-eqz v7, :cond_4c

    .line 552
    monitor-exit v6
    :try_end_4a
    .catchall {:try_start_43 .. :try_end_4a} :catchall_86

    :try_start_4a
    monitor-exit v17
    :try_end_4b
    .catchall {:try_start_4a .. :try_end_4b} :catchall_63

    .line 863
    :goto_4b
    return-void

    .line 555
    :cond_4c
    add-long/2addr v4, v2

    .line 556
    :try_start_4d
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/dropbox/android/service/p;->d:J

    const-wide/16 v7, 0xbb8

    add-long/2addr v2, v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v2, v7

    goto :goto_2b

    .line 561
    :cond_5a
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/service/p;->b()Z

    move-result v2

    if-eqz v2, :cond_66

    .line 562
    monitor-exit v6
    :try_end_61
    .catchall {:try_start_4d .. :try_end_61} :catchall_86

    :try_start_61
    monitor-exit v17

    goto :goto_4b

    .line 862
    :catchall_63
    move-exception v2

    monitor-exit v17
    :try_end_65
    .catchall {:try_start_61 .. :try_end_65} :catchall_63

    throw v2

    .line 565
    :cond_66
    :try_start_66
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/p;)Lcom/dropbox/android/service/p;

    .line 566
    monitor-exit v6
    :try_end_6f
    .catchall {:try_start_66 .. :try_end_6f} :catchall_86

    .line 568
    :try_start_6f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 570
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v20

    .line 573
    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->g()Z

    move-result v2

    if-nez v2, :cond_89

    .line 574
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-virtual {v2}, Lcom/dropbox/android/service/CameraUploadService;->stopSelf()V

    .line 575
    monitor-exit v17
    :try_end_85
    .catchall {:try_start_6f .. :try_end_85} :catchall_63

    goto :goto_4b

    .line 566
    :catchall_86
    move-exception v2

    :try_start_87
    monitor-exit v6
    :try_end_88
    .catchall {:try_start_87 .. :try_end_88} :catchall_86

    :try_start_88
    throw v2

    .line 579
    :cond_89
    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->s()Z

    move-result v21

    .line 580
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 584
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    .line 585
    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->j()Ldbxyzptlk/l/q;

    move-result-object v3

    sget-object v4, Ldbxyzptlk/l/q;->c:Ldbxyzptlk/l/q;

    if-eq v3, v4, :cond_bd

    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->p()Z

    move-result v3

    if-nez v3, :cond_bd

    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->i()Z

    move-result v3

    if-eqz v3, :cond_397

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->d(Lcom/dropbox/android/service/CameraUploadService;)Z

    move-result v3

    if-nez v3, :cond_397

    .line 590
    :cond_bd
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/x;->n()V

    .line 591
    const/4 v2, 0x1

    move/from16 v16, v2

    .line 595
    :goto_c7
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v2

    .line 596
    invoke-virtual {v2}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 605
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v3

    .line 606
    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->j()Ldbxyzptlk/l/q;

    move-result-object v4

    sget-object v5, Ldbxyzptlk/l/q;->a:Ldbxyzptlk/l/q;

    if-eq v4, v5, :cond_113

    .line 607
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/H;->c()V

    .line 609
    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v4

    if-eqz v4, :cond_1f6

    .line 610
    sget-object v4, Ldbxyzptlk/l/q;->a:Ldbxyzptlk/l/q;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ldbxyzptlk/l/m;->a(Ldbxyzptlk/l/q;)V

    .line 611
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/H;->c()V

    .line 613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->e(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/w;

    move-result-object v3

    if-eqz v3, :cond_113

    .line 614
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v4}, Lcom/dropbox/android/service/CameraUploadService;->e(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/w;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dropbox/android/service/u;->b(Lcom/dropbox/android/service/w;)V

    .line 615
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/w;)Lcom/dropbox/android/service/w;

    .line 637
    :cond_113
    :goto_113
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-virtual {v3}, Lcom/dropbox/android/service/CameraUploadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    .line 642
    const/4 v3, 0x0

    .line 643
    if-eqz v16, :cond_394

    .line 644
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/dropbox/android/service/p;->a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashMap;

    move-result-object v3

    move-object v15, v3

    .line 647
    :goto_125
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    .line 648
    invoke-static {}, Lcom/dropbox/android/service/r;->a()Lcom/dropbox/android/service/r;

    move-result-object v25

    .line 649
    invoke-static {}, Lcom/dropbox/android/service/r;->b()Lcom/dropbox/android/service/r;

    move-result-object v26

    .line 652
    new-instance v27, Ljava/util/LinkedList;

    invoke-direct/range {v27 .. v27}, Ljava/util/LinkedList;-><init>()V

    .line 653
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/p;->c:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :cond_13f
    :goto_13f
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_320

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/net/Uri;

    .line 654
    move-object/from16 v0, v23

    invoke-static {v0, v12}, Lcom/dropbox/android/util/aw;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/dropbox/android/util/aw;
    :try_end_150
    .catchall {:try_start_88 .. :try_end_150} :catchall_63

    move-result-object v29

    .line 655
    if-eqz v29, :cond_13f

    .line 660
    :try_start_153
    const-string v3, "_id"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/aw;->a(Ljava/lang/String;)I

    move-result v30

    .line 661
    const-string v3, "_data"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/aw;->a(Ljava/lang/String;)I

    move-result v31

    .line 662
    const-string v3, "_size"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/aw;->a(Ljava/lang/String;)I

    move-result v32

    .line 663
    const-string v3, "mime_type"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/aw;->a(Ljava/lang/String;)I

    move-result v33

    .line 664
    const-string v3, "date_modified"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/dropbox/android/util/aw;->a(Ljava/lang/String;)I

    move-result v34

    .line 666
    :cond_17b
    :goto_17b
    invoke-virtual/range {v29 .. v29}, Lcom/dropbox/android/util/aw;->a()Z

    move-result v3

    if-eqz v3, :cond_1f1

    .line 667
    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aw;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 670
    invoke-static {v3}, Lcom/dropbox/android/util/av;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_195

    invoke-static {v3}, Lcom/dropbox/android/util/av;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17b

    .line 674
    :cond_195
    new-instance v35, Ljava/io/File;

    move-object/from16 v0, v35

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 676
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_17b

    .line 680
    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aw;->a(I)J

    move-result-wide v3

    .line 681
    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-nez v5, :cond_391

    .line 685
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->length()J

    move-result-wide v3

    move-wide v13, v3

    .line 688
    :goto_1b5
    const-wide/16 v3, 0x0

    cmp-long v3, v13, v3

    if-lez v3, :cond_17b

    .line 692
    move-object/from16 v0, v35

    invoke-static {v0, v13, v14}, Lcom/dropbox/android/service/CameraUploadService;->a(Ljava/io/File;J)Ljava/lang/String;

    move-result-object v36

    .line 693
    move-object/from16 v0, v29

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aw;->b(I)Ljava/lang/String;

    move-result-object v37

    .line 695
    invoke-virtual/range {v29 .. v30}, Lcom/dropbox/android/util/aw;->a(I)J

    move-result-wide v3

    invoke-static {v12, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v38

    .line 696
    move-object/from16 v0, v29

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aw;->a(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v39, v3, v5

    .line 699
    const/4 v10, 0x0

    .line 700
    if-eqz v15, :cond_237

    .line 701
    move-object/from16 v0, v36

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dropbox/android/service/k;
    :try_end_1ec
    .catchall {:try_start_153 .. :try_end_1ec} :catchall_25f

    move-object v4, v3

    .line 717
    :goto_1ed
    if-eqz v4, :cond_2c6

    .line 721
    if-nez v16, :cond_264

    .line 786
    :cond_1f1
    :try_start_1f1
    invoke-virtual/range {v29 .. v29}, Lcom/dropbox/android/util/aw;->b()V

    goto/16 :goto_13f

    .line 618
    :cond_1f6
    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->j()Ldbxyzptlk/l/q;

    move-result-object v4

    sget-object v5, Ldbxyzptlk/l/q;->c:Ldbxyzptlk/l/q;

    if-ne v4, v5, :cond_22b

    .line 623
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v2}, Lcom/dropbox/android/service/CameraUploadService;->e(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/w;

    move-result-object v2

    if-nez v2, :cond_228

    .line 624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    new-instance v3, Lcom/dropbox/android/service/l;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/dropbox/android/service/l;-><init>(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/c;)V

    invoke-static {v2, v3}, Lcom/dropbox/android/service/CameraUploadService;->a(Lcom/dropbox/android/service/CameraUploadService;Lcom/dropbox/android/service/w;)Lcom/dropbox/android/service/w;

    .line 625
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/dropbox/android/service/p;->b:Lcom/dropbox/android/service/CameraUploadService;

    invoke-static {v3}, Lcom/dropbox/android/service/CameraUploadService;->e(Lcom/dropbox/android/service/CameraUploadService;)Lcom/dropbox/android/service/w;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/dropbox/android/service/u;->a(Lcom/dropbox/android/service/w;)V

    .line 627
    :cond_228
    monitor-exit v17

    goto/16 :goto_4b

    .line 631
    :cond_22b
    sget-object v4, Ldbxyzptlk/l/q;->a:Ldbxyzptlk/l/q;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ldbxyzptlk/l/m;->a(Ldbxyzptlk/l/q;)V

    .line 632
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/H;->c()V
    :try_end_235
    .catchall {:try_start_1f1 .. :try_end_235} :catchall_63

    goto/16 :goto_113

    .line 703
    :cond_237
    :try_start_237
    const-string v3, "camera_upload"

    sget-object v4, Lcom/dropbox/android/service/k;->a:[Ljava/lang/String;

    const-string v5, "local_hash = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v36, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_249
    .catchall {:try_start_237 .. :try_end_249} :catchall_25f

    move-result-object v4

    .line 709
    :try_start_24a
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_38e

    .line 710
    new-instance v3, Lcom/dropbox/android/service/k;

    invoke-direct {v3, v4}, Lcom/dropbox/android/service/k;-><init>(Landroid/database/Cursor;)V
    :try_end_255
    .catchall {:try_start_24a .. :try_end_255} :catchall_25a

    .line 713
    :goto_255
    :try_start_255
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    move-object v4, v3

    .line 714
    goto :goto_1ed

    .line 713
    :catchall_25a
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_25f
    .catchall {:try_start_255 .. :try_end_25f} :catchall_25f

    .line 786
    :catchall_25f
    move-exception v2

    :try_start_260
    invoke-virtual/range {v29 .. v29}, Lcom/dropbox/android/util/aw;->b()V

    throw v2
    :try_end_264
    .catchall {:try_start_260 .. :try_end_264} :catchall_63

    .line 725
    :cond_264
    :try_start_264
    invoke-static {v4}, Lcom/dropbox/android/service/k;->a(Lcom/dropbox/android/service/k;)Z

    move-result v3

    if-nez v3, :cond_17b

    .line 732
    if-eqz v21, :cond_298

    .line 733
    invoke-static {v4}, Lcom/dropbox/android/service/k;->b(Lcom/dropbox/android/service/k;)Z

    move-result v3

    if-nez v3, :cond_17b

    .line 735
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 736
    const-string v5, "ignored"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 737
    const-string v5, "camera_upload"

    const-string v6, "_id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v4}, Lcom/dropbox/android/service/k;->c(Lcom/dropbox/android/service/k;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-virtual {v2, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_17b

    .line 755
    :cond_298
    invoke-static {v4}, Lcom/dropbox/android/service/k;->b(Lcom/dropbox/android/service/k;)Z

    move-result v3

    if-eqz v3, :cond_2a4

    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->p()Z

    move-result v3

    if-eqz v3, :cond_17b

    .line 756
    :cond_2a4
    new-instance v3, Lcom/dropbox/android/service/i;

    invoke-static {v4}, Lcom/dropbox/android/service/k;->c(Lcom/dropbox/android/service/k;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-static {v4}, Lcom/dropbox/android/service/k;->d(Lcom/dropbox/android/service/k;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v4, v35

    move-object/from16 v5, v38

    move-object/from16 v6, v36

    move-object/from16 v7, v37

    move-wide/from16 v8, v39

    invoke-direct/range {v3 .. v12}, Lcom/dropbox/android/service/i;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_17b

    .line 764
    :cond_2c6
    move-object/from16 v0, v24

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 765
    if-nez v3, :cond_2d7

    .line 766
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 768
    :cond_2d7
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v24

    move-object/from16 v1, v37

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 770
    invoke-static/range {v37 .. v37}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2fe

    .line 771
    move-object/from16 v0, v25

    invoke-virtual {v0, v13, v14}, Lcom/dropbox/android/service/r;->a(J)V

    .line 777
    :cond_2f3
    :goto_2f3
    if-eqz v21, :cond_30a

    .line 778
    move-object/from16 v0, v22

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_17b

    .line 772
    :cond_2fe
    invoke-static/range {v37 .. v37}, Lcom/dropbox/android/util/ae;->j(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2f3

    .line 773
    move-object/from16 v0, v26

    invoke-virtual {v0, v13, v14}, Lcom/dropbox/android/service/r;->a(J)V

    goto :goto_2f3

    .line 780
    :cond_30a
    new-instance v5, Lcom/dropbox/android/service/i;

    move-object/from16 v6, v35

    move-object/from16 v7, v38

    move-object/from16 v8, v36

    move-object/from16 v9, v37

    move-wide/from16 v10, v39

    invoke-direct/range {v5 .. v12}, Lcom/dropbox/android/service/i;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_31e
    .catchall {:try_start_264 .. :try_end_31e} :catchall_25f

    goto/16 :goto_17b

    .line 790
    :cond_320
    :try_start_320
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v3, v3, v18

    .line 792
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Finished querying providers."

    invoke-static {v5, v6}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    invoke-static {}, Lcom/dropbox/android/util/i;->h()Lcom/dropbox/android/util/s;

    move-result-object v5

    const-string v6, "mimes"

    move-object/from16 v0, v24

    invoke-virtual {v5, v6, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/dropbox/android/util/s;

    move-result-object v5

    const-string v6, "vidsizes"

    invoke-virtual/range {v25 .. v25}, Lcom/dropbox/android/service/r;->c()Ljava/util/LinkedHashMap;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/dropbox/android/util/s;

    move-result-object v5

    const-string v6, "imgsizes"

    invoke-virtual/range {v26 .. v26}, Lcom/dropbox/android/service/r;->c()Ljava/util/LinkedHashMap;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/dropbox/android/util/s;

    move-result-object v5

    const-string v6, "duration"

    invoke-virtual {v5, v6, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v3

    const-string v4, "ignore.existing"

    move/from16 v0, v21

    invoke-virtual {v3, v4, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    move-result-object v3

    const-string v4, "initial"

    invoke-virtual/range {v20 .. v20}, Ldbxyzptlk/l/m;->p()Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/s;->c()V

    .line 799
    new-instance v3, Lcom/dropbox/android/service/q;

    move-object/from16 v4, p0

    move-object/from16 v5, v27

    move-object/from16 v6, v20

    move/from16 v7, v21

    move-object v8, v2

    move-object/from16 v9, v22

    invoke-direct/range {v3 .. v9}, Lcom/dropbox/android/service/q;-><init>(Lcom/dropbox/android/service/p;Ljava/util/LinkedList;Ldbxyzptlk/l/m;ZLandroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/dropbox/android/service/p;->a(Ljava/lang/Runnable;)V

    .line 861
    invoke-static {}, Lcom/dropbox/android/service/CameraUploadService;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Finished camera upload scan."

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    monitor-exit v17
    :try_end_389
    .catchall {:try_start_320 .. :try_end_389} :catchall_63

    goto/16 :goto_4b

    .line 546
    :catch_38b
    move-exception v7

    goto/16 :goto_43

    :cond_38e
    move-object v3, v10

    goto/16 :goto_255

    :cond_391
    move-wide v13, v3

    goto/16 :goto_1b5

    :cond_394
    move-object v15, v3

    goto/16 :goto_125

    :cond_397
    move/from16 v16, v2

    goto/16 :goto_c7
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 526
    iget-object v0, p0, Lcom/dropbox/android/service/p;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 527
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/android/service/p;->d:J

    .line 528
    return-void
.end method
