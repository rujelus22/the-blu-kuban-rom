.class final Landroid/support/v4/app/z;
.super Landroid/support/v4/app/x;
.source "panda.py"


# static fields
.field static a:Z


# instance fields
.field final b:Ldbxyzptlk/c/d;

.field final c:Ldbxyzptlk/c/d;

.field d:Landroid/support/v4/app/FragmentActivity;

.field e:Z

.field f:Z

.field g:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 189
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v4/app/z;->a:Z

    return-void
.end method

.method constructor <init>(Landroid/support/v4/app/FragmentActivity;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 475
    invoke-direct {p0}, Landroid/support/v4/app/x;-><init>()V

    .line 194
    new-instance v0, Ldbxyzptlk/c/d;

    invoke-direct {v0}, Ldbxyzptlk/c/d;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    .line 200
    new-instance v0, Ldbxyzptlk/c/d;

    invoke-direct {v0}, Ldbxyzptlk/c/d;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    .line 476
    iput-object p1, p0, Landroid/support/v4/app/z;->d:Landroid/support/v4/app/FragmentActivity;

    .line 477
    iput-boolean p2, p0, Landroid/support/v4/app/z;->e:Z

    .line 478
    return-void
.end method

.method private c(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Landroid/support/v4/app/A;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 486
    new-instance v0, Landroid/support/v4/app/A;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/support/v4/app/A;-><init>(Landroid/support/v4/app/z;ILandroid/os/Bundle;Landroid/support/v4/app/y;)V

    .line 487
    invoke-interface {p3, p1, p2}, Landroid/support/v4/app/y;->a(ILandroid/os/Bundle;)Ldbxyzptlk/a/d;

    move-result-object v1

    .line 488
    iput-object v1, v0, Landroid/support/v4/app/A;->d:Ldbxyzptlk/a/d;

    .line 489
    return-object v0
.end method

.method private d(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Landroid/support/v4/app/A;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 495
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Landroid/support/v4/app/z;->g:Z

    .line 496
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/z;->c(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Landroid/support/v4/app/A;

    move-result-object v0

    .line 497
    invoke-virtual {p0, v0}, Landroid/support/v4/app/z;->a(Landroid/support/v4/app/A;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_e

    .line 500
    iput-boolean v1, p0, Landroid/support/v4/app/z;->g:Z

    return-object v0

    :catchall_e
    move-exception v0

    iput-boolean v1, p0, Landroid/support/v4/app/z;->g:Z

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 540
    iget-boolean v0, p0, Landroid/support/v4/app/z;->g:Z

    if-eqz v0, :cond_c

    .line 541
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 544
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, p1}, Ldbxyzptlk/c/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    .line 546
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_3a

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initLoader in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": args="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    :cond_3a
    if-nez v0, :cond_6e

    .line 550
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/z;->d(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Landroid/support/v4/app/A;

    move-result-object v0

    .line 551
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_5c

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Created new loader "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :cond_5c
    :goto_5c
    iget-boolean v1, v0, Landroid/support/v4/app/A;->e:Z

    if-eqz v1, :cond_6b

    iget-boolean v1, p0, Landroid/support/v4/app/z;->e:Z

    if-eqz v1, :cond_6b

    .line 559
    iget-object v1, v0, Landroid/support/v4/app/A;->d:Ldbxyzptlk/a/d;

    iget-object v2, v0, Landroid/support/v4/app/A;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/A;->b(Ldbxyzptlk/a/d;Ljava/lang/Object;)V

    .line 562
    :cond_6b
    iget-object v0, v0, Landroid/support/v4/app/A;->d:Ldbxyzptlk/a/d;

    return-object v0

    .line 553
    :cond_6e
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_8a

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Re-using existing loader "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    :cond_8a
    iput-object p3, v0, Landroid/support/v4/app/A;->c:Landroid/support/v4/app/y;

    goto :goto_5c
.end method

.method final a(Landroid/support/v4/app/A;)V
    .registers 4
    .parameter

    .prologue
    .line 505
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    iget v1, p1, Landroid/support/v4/app/A;->a:I

    invoke-virtual {v0, v1, p1}, Ldbxyzptlk/c/d;->b(ILjava/lang/Object;)V

    .line 506
    iget-boolean v0, p0, Landroid/support/v4/app/z;->e:Z

    if-eqz v0, :cond_e

    .line 510
    invoke-virtual {p1}, Landroid/support/v4/app/A;->a()V

    .line 512
    :cond_e
    return-void
.end method

.method final a(Landroid/support/v4/app/FragmentActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 481
    iput-object p1, p0, Landroid/support/v4/app/z;->d:Landroid/support/v4/app/FragmentActivity;

    .line 482
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 797
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    if-lez v0, :cond_59

    .line 798
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 799
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move v1, v2

    .line 800
    :goto_25
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    if-ge v1, v0, :cond_59

    .line 801
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    .line 802
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v4, v1}, Ldbxyzptlk/c/d;->e(I)I

    move-result v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(I)V

    .line 803
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/A;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 804
    invoke-virtual {v0, v3, p2, p3, p4}, Landroid/support/v4/app/A;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 800
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_25

    .line 807
    :cond_59
    iget-object v0, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    if-lez v0, :cond_af

    .line 808
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Inactive Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 810
    :goto_7c
    iget-object v0, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    if-ge v2, v0, :cond_af

    .line 811
    iget-object v0, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v2}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    .line 812
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v3, v2}, Ldbxyzptlk/c/d;->e(I)I

    move-result v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    .line 813
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/A;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 814
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/A;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 810
    add-int/lit8 v2, v2, 0x1

    goto :goto_7c

    .line 817
    :cond_af
    return-void
.end method

.method public final a()Z
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 821
    .line 822
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v4

    move v2, v1

    move v3, v1

    .line 823
    :goto_9
    if-ge v2, v4, :cond_23

    .line 824
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v2}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    .line 825
    iget-boolean v5, v0, Landroid/support/v4/app/A;->h:Z

    if-eqz v5, :cond_21

    iget-boolean v0, v0, Landroid/support/v4/app/A;->f:Z

    if-nez v0, :cond_21

    const/4 v0, 0x1

    :goto_1c
    or-int/2addr v3, v0

    .line 823
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    :cond_21
    move v0, v1

    .line 825
    goto :goto_1c

    .line 827
    :cond_23
    return v3
.end method

.method public final b(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 590
    iget-boolean v0, p0, Landroid/support/v4/app/z;->g:Z

    if-eqz v0, :cond_d

    .line 591
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_d
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, p1}, Ldbxyzptlk/c/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    .line 595
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_3b

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "restartLoader in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": args="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_3b
    if-eqz v0, :cond_77

    .line 597
    iget-object v1, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v1, p1}, Ldbxyzptlk/c/d;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/A;

    .line 598
    if-eqz v1, :cond_d5

    .line 599
    iget-boolean v2, v0, Landroid/support/v4/app/A;->e:Z

    if-eqz v2, :cond_7e

    .line 604
    sget-boolean v2, Landroid/support/v4/app/z;->a:Z

    if-eqz v2, :cond_67

    const-string v2, "LoaderManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Removing last inactive loader: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :cond_67
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/support/v4/app/A;->f:Z

    .line 606
    invoke-virtual {v1}, Landroid/support/v4/app/A;->f()V

    .line 607
    iget-object v1, v0, Landroid/support/v4/app/A;->d:Ldbxyzptlk/a/d;

    invoke-virtual {v1}, Ldbxyzptlk/a/d;->q()V

    .line 608
    iget-object v1, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v1, p1, v0}, Ldbxyzptlk/c/d;->b(ILjava/lang/Object;)V

    .line 643
    :cond_77
    :goto_77
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/z;->d(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Landroid/support/v4/app/A;

    move-result-object v0

    .line 644
    iget-object v0, v0, Landroid/support/v4/app/A;->d:Ldbxyzptlk/a/d;

    :goto_7d
    return-object v0

    .line 612
    :cond_7e
    iget-boolean v1, v0, Landroid/support/v4/app/A;->h:Z

    if-nez v1, :cond_96

    .line 616
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_8d

    const-string v1, "LoaderManager"

    const-string v2, "  Current loader is stopped; replacing"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :cond_8d
    iget-object v1, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v1, p1, v4}, Ldbxyzptlk/c/d;->b(ILjava/lang/Object;)V

    .line 618
    invoke-virtual {v0}, Landroid/support/v4/app/A;->f()V

    goto :goto_77

    .line 623
    :cond_96
    iget-object v1, v0, Landroid/support/v4/app/A;->n:Landroid/support/v4/app/A;

    if-eqz v1, :cond_bf

    .line 624
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_b8

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Removing pending loader: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/support/v4/app/A;->n:Landroid/support/v4/app/A;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    :cond_b8
    iget-object v1, v0, Landroid/support/v4/app/A;->n:Landroid/support/v4/app/A;

    invoke-virtual {v1}, Landroid/support/v4/app/A;->f()V

    .line 626
    iput-object v4, v0, Landroid/support/v4/app/A;->n:Landroid/support/v4/app/A;

    .line 628
    :cond_bf
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_ca

    const-string v1, "LoaderManager"

    const-string v2, "  Enqueuing as new pending loader"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    :cond_ca
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/z;->c(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Landroid/support/v4/app/A;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/A;->n:Landroid/support/v4/app/A;

    .line 631
    iget-object v0, v0, Landroid/support/v4/app/A;->n:Landroid/support/v4/app/A;

    iget-object v0, v0, Landroid/support/v4/app/A;->d:Ldbxyzptlk/a/d;

    goto :goto_7d

    .line 637
    :cond_d5
    sget-boolean v1, Landroid/support/v4/app/z;->a:Z

    if-eqz v1, :cond_f1

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Making last loader inactive: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    :cond_f1
    iget-object v1, v0, Landroid/support/v4/app/A;->d:Ldbxyzptlk/a/d;

    invoke-virtual {v1}, Ldbxyzptlk/a/d;->q()V

    .line 639
    iget-object v1, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v1, p1, v0}, Ldbxyzptlk/c/d;->b(ILjava/lang/Object;)V

    goto/16 :goto_77
.end method

.method final b()V
    .registers 5

    .prologue
    .line 698
    sget-boolean v0, Landroid/support/v4/app/z;->a:Z

    if-eqz v0, :cond_1c

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    :cond_1c
    iget-boolean v0, p0, Landroid/support/v4/app/z;->e:Z

    if-eqz v0, :cond_43

    .line 700
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 701
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 702
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Called doStart when already started: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 713
    :cond_42
    return-void

    .line 706
    :cond_43
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/z;->e:Z

    .line 710
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4f
    if-ltz v1, :cond_42

    .line 711
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    invoke-virtual {v0}, Landroid/support/v4/app/A;->a()V

    .line 710
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4f
.end method

.method final c()V
    .registers 5

    .prologue
    .line 716
    sget-boolean v0, Landroid/support/v4/app/z;->a:Z

    if-eqz v0, :cond_1c

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopping in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_1c
    iget-boolean v0, p0, Landroid/support/v4/app/z;->e:Z

    if-nez v0, :cond_43

    .line 718
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 719
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 720
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Called doStop when not started: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 728
    :goto_42
    return-void

    .line 724
    :cond_43
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4c
    if-ltz v1, :cond_5d

    .line 725
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    invoke-virtual {v0}, Landroid/support/v4/app/A;->e()V

    .line 724
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4c

    .line 727
    :cond_5d
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/z;->e:Z

    goto :goto_42
.end method

.method final d()V
    .registers 5

    .prologue
    .line 731
    sget-boolean v0, Landroid/support/v4/app/z;->a:Z

    if-eqz v0, :cond_1c

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retaining in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    :cond_1c
    iget-boolean v0, p0, Landroid/support/v4/app/z;->e:Z

    if-nez v0, :cond_43

    .line 733
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 734
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 735
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Called doRetain when not started: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 744
    :cond_42
    return-void

    .line 739
    :cond_43
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/z;->f:Z

    .line 740
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/z;->e:Z

    .line 741
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_52
    if-ltz v1, :cond_42

    .line 742
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    invoke-virtual {v0}, Landroid/support/v4/app/A;->b()V

    .line 741
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_52
.end method

.method final e()V
    .registers 4

    .prologue
    .line 747
    iget-boolean v0, p0, Landroid/support/v4/app/z;->f:Z

    if-eqz v0, :cond_3d

    .line 748
    sget-boolean v0, Landroid/support/v4/app/z;->a:Z

    if-eqz v0, :cond_20

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Finished Retaining in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    :cond_20
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/z;->f:Z

    .line 751
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2c
    if-ltz v1, :cond_3d

    .line 752
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    invoke-virtual {v0}, Landroid/support/v4/app/A;->c()V

    .line 751
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2c

    .line 755
    :cond_3d
    return-void
.end method

.method final f()V
    .registers 4

    .prologue
    .line 758
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 759
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/app/A;->k:Z

    .line 758
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 761
    :cond_1a
    return-void
.end method

.method final g()V
    .registers 3

    .prologue
    .line 764
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 765
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    invoke-virtual {v0}, Landroid/support/v4/app/A;->d()V

    .line 764
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 767
    :cond_1a
    return-void
.end method

.method final h()V
    .registers 4

    .prologue
    .line 770
    iget-boolean v0, p0, Landroid/support/v4/app/z;->f:Z

    if-nez v0, :cond_3a

    .line 771
    sget-boolean v0, Landroid/support/v4/app/z;->a:Z

    if-eqz v0, :cond_20

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Destroying Active in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    :cond_20
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_29
    if-ltz v1, :cond_3a

    .line 773
    iget-object v0, p0, Landroid/support/v4/app/z;->b:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    invoke-virtual {v0}, Landroid/support/v4/app/A;->f()V

    .line 772
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_29

    .line 777
    :cond_3a
    sget-boolean v0, Landroid/support/v4/app/z;->a:Z

    if-eqz v0, :cond_56

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Destroying Inactive in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    :cond_56
    iget-object v0, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_5f
    if-ltz v1, :cond_70

    .line 779
    iget-object v0, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v0, v1}, Ldbxyzptlk/c/d;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/A;

    invoke-virtual {v0}, Landroid/support/v4/app/A;->f()V

    .line 778
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_5f

    .line 781
    :cond_70
    iget-object v0, p0, Landroid/support/v4/app/z;->c:Ldbxyzptlk/c/d;

    invoke-virtual {v0}, Ldbxyzptlk/c/d;->b()V

    .line 782
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 786
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 787
    const-string v1, "LoaderManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 788
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 790
    iget-object v1, p0, Landroid/support/v4/app/z;->d:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1, v0}, Ldbxyzptlk/c/a;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 791
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
