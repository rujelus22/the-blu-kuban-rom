.class public final Landroid/support/v4/app/D;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Landroid/app/PendingIntent;

.field e:Landroid/app/PendingIntent;

.field f:Landroid/widget/RemoteViews;

.field g:Landroid/graphics/Bitmap;

.field h:Ljava/lang/CharSequence;

.field i:I

.field j:Landroid/app/Notification;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    .line 101
    iput-object p1, p0, Landroid/support/v4/app/D;->a:Landroid/content/Context;

    .line 104
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    .line 105
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 106
    return-void
.end method

.method private a(IZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 373
    if-eqz p2, :cond_a

    .line 374
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 378
    :goto_9
    return-void

    .line 376
    :cond_a
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_9
.end method


# virtual methods
.method public final a()Landroid/app/Notification;
    .registers 2

    .prologue
    .line 385
    invoke-static {}, Landroid/support/v4/app/C;->a()Landroid/support/v4/app/E;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/support/v4/app/E;->a(Landroid/support/v4/app/D;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/D;
    .registers 3
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 126
    return-object p0
.end method

.method public final a(J)Landroid/support/v4/app/D;
    .registers 4
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 114
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Landroid/support/v4/app/D;
    .registers 2
    .parameter

    .prologue
    .line 208
    iput-object p1, p0, Landroid/support/v4/app/D;->d:Landroid/app/PendingIntent;

    .line 209
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;
    .registers 2
    .parameter

    .prologue
    .line 149
    iput-object p1, p0, Landroid/support/v4/app/D;->b:Ljava/lang/CharSequence;

    .line 150
    return-object p0
.end method

.method public final a(Z)Landroid/support/v4/app/D;
    .registers 3
    .parameter

    .prologue
    .line 330
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/D;->a(IZ)V

    .line 331
    return-object p0
.end method

.method public final b(I)Landroid/support/v4/app/D;
    .registers 4
    .parameter

    .prologue
    .line 365
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->defaults:I

    .line 366
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_10

    .line 367
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 369
    :cond_10
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;
    .registers 2
    .parameter

    .prologue
    .line 157
    iput-object p1, p0, Landroid/support/v4/app/D;->c:Ljava/lang/CharSequence;

    .line 158
    return-object p0
.end method

.method public final b(Z)Landroid/support/v4/app/D;
    .registers 3
    .parameter

    .prologue
    .line 350
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/D;->a(IZ)V

    .line 351
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;
    .registers 3
    .parameter

    .prologue
    .line 248
    iget-object v0, p0, Landroid/support/v4/app/D;->j:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 249
    return-object p0
.end method
