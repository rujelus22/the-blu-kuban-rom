.class Lcom/google/android/accounts/CupcakeContentSyncer;
.super Lcom/google/android/accounts/ContentSyncer;
.source "CupcakeContentSyncer.java"


# static fields
.field private static final NAMESPACE:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"


# instance fields
.field private mAuthenticators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/accounts/DatabaseAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/accounts/ContentSyncer;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/accounts/CupcakeContentSyncer;->mContext:Landroid/content/Context;

    .line 74
    invoke-static {p1}, Lcom/google/android/accounts/DatabaseAuthenticator;->createDatabaseAuthenticators(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/accounts/CupcakeContentSyncer;->mAuthenticators:Ljava/util/Map;

    .line 75
    return-void
.end method

.method private static final beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .registers 6
    .parameter "parser"
    .parameter "firstElementName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 55
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .local v0, type:I
    if-eq v0, v2, :cond_a

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 58
    :cond_a
    if-eq v0, v2, :cond_14

    .line 59
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "No start tag found"

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 62
    :cond_14
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_45

    .line 63
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected start tag: found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_45
    return-void
.end method

.method private createOperation(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/PendingIntent;
    .registers 5
    .parameter "account"
    .parameter "authority"
    .parameter "extras"

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/accounts/CupcakeContentSyncer;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/accounts/PeriodicSyncReceiver;->createPendingIntent(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getAlarmManager()Landroid/app/AlarmManager;
    .registers 3

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/accounts/CupcakeContentSyncer;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    return-object v0
.end method

.method private getAuthenticator(Ljava/lang/String;)Lcom/google/android/accounts/DatabaseAuthenticator;
    .registers 3
    .parameter "type"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/accounts/CupcakeContentSyncer;->mAuthenticators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/accounts/DatabaseAuthenticator;

    return-object v0
.end method

.method private querySyncAdapterServices()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v3, p0, Lcom/google/android/accounts/CupcakeContentSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 124
    .local v2, pm:Landroid/content/pm/PackageManager;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.content.SyncAdapter"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 125
    .local v1, intent:Landroid/content/Intent;
    const/16 v0, 0x80

    .line 126
    .local v0, flags:I
    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public addPeriodicSync(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V
    .registers 17
    .parameter "account"
    .parameter "authority"
    .parameter "extras"
    .parameter "pollFrequency"

    .prologue
    .line 189
    const-wide/16 v9, 0x3e8

    mul-long v7, p4, v9

    .line 190
    .local v7, pollFrequencyMsec:J
    invoke-direct {p0}, Lcom/google/android/accounts/CupcakeContentSyncer;->getAlarmManager()Landroid/app/AlarmManager;

    move-result-object v0

    .line 191
    .local v0, manager:Landroid/app/AlarmManager;
    const/4 v1, 0x2

    .line 192
    .local v1, type:I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    add-long v2, v9, v7

    .line 193
    .local v2, triggerAtTime:J
    move-wide v4, v7

    .line 194
    .local v4, interval:J
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/accounts/CupcakeContentSyncer;->createOperation(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v6

    .line 195
    .local v6, operation:Landroid/app/PendingIntent;
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 196
    return-void
.end method

.method public getIsSyncable(Lcom/google/android/accounts/Account;Ljava/lang/String;)I
    .registers 4
    .parameter "account"
    .parameter "authority"

    .prologue
    .line 91
    if-eqz p1, :cond_4

    if-nez p2, :cond_a

    .line 92
    :cond_4
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 95
    :cond_a
    const/4 v0, 0x1

    return v0
.end method

.method public getSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 5
    .parameter "account"
    .parameter "authority"

    .prologue
    .line 111
    if-eqz p1, :cond_4

    if-nez p2, :cond_a

    .line 112
    :cond_4
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 114
    :cond_a
    iget-object v1, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/accounts/CupcakeContentSyncer;->getAuthenticator(Ljava/lang/String;)Lcom/google/android/accounts/DatabaseAuthenticator;

    move-result-object v0

    .line 115
    .local v0, authenticator:Lcom/google/android/accounts/DatabaseAuthenticator;
    if-eqz v0, :cond_17

    .line 116
    invoke-virtual {v0, p1, p2}, Lcom/google/android/accounts/DatabaseAuthenticator;->getSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    .line 118
    :goto_16
    return v1

    :cond_17
    const/4 v1, 0x0

    goto :goto_16
.end method

.method public removePeriodicSync(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 6
    .parameter "account"
    .parameter "authority"
    .parameter "extras"

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/google/android/accounts/CupcakeContentSyncer;->getAlarmManager()Landroid/app/AlarmManager;

    move-result-object v0

    .line 201
    .local v0, manager:Landroid/app/AlarmManager;
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/accounts/CupcakeContentSyncer;->createOperation(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 202
    .local v1, operation:Landroid/app/PendingIntent;
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 203
    return-void
.end method

.method public requestSync(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 21
    .parameter "account"
    .parameter "authority"
    .parameter "extras"

    .prologue
    .line 131
    if-eqz p1, :cond_4

    if-nez p2, :cond_a

    .line 132
    :cond_4
    new-instance v15, Ljava/lang/NullPointerException;

    invoke-direct {v15}, Ljava/lang/NullPointerException;-><init>()V

    throw v15

    .line 134
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/accounts/CupcakeContentSyncer;->querySyncAdapterServices()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_cb

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    .line 136
    .local v10, service:Landroid/content/pm/ResolveInfo;
    :try_start_1e
    iget-object v5, v10, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 138
    .local v5, info:Landroid/content/pm/ServiceInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/CupcakeContentSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 139
    .local v7, pm:Landroid/content/pm/PackageManager;
    iget-object v15, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v15}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v9

    .line 141
    .local v9, resources:Landroid/content/res/Resources;
    iget-object v6, v5, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    .line 142
    .local v6, metaData:Landroid/os/Bundle;
    const-string v15, "android.content.SyncAdapter"

    invoke-virtual {v6, v15}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 144
    .local v8, resId:I
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_39} :catch_ba

    move-result-object v14

    .line 146
    .local v14, xml:Landroid/content/res/XmlResourceParser;
    :try_start_3a
    const-string v15, "sync-adapter"

    invoke-static {v14, v15}, Lcom/google/android/accounts/CupcakeContentSyncer;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 147
    const-string v15, "http://schemas.android.com/apk/res/android"

    const-string v16, "contentAuthority"

    invoke-interface/range {v14 .. v16}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 148
    .local v2, contentAuthority:Ljava/lang/String;
    const-string v15, "http://schemas.android.com/apk/res/android"

    const-string v16, "accountType"

    invoke-interface/range {v14 .. v16}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, accountType:Ljava/lang/String;
    const-string v15, "http://schemas.android.com/apk/res/android"

    const-string v16, "supportsUploading"

    invoke-interface/range {v14 .. v16}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 151
    .local v13, supportsUploadingValue:Ljava/lang/String;
    const-string v15, "true"

    invoke-virtual {v15, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 152
    .local v12, supportsUploading:Z
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b5

    .line 153
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b5

    .line 154
    new-instance v11, Landroid/content/Intent;

    const-string v15, "android.content.SyncAdapter"

    invoke-direct {v11, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    .local v11, serviceIntent:Landroid/content/Intent;
    iget-object v15, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v0, v5, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    const-string v15, "com.google.android.accounts.intent.extra.ACCOUNT_NAME"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v15, "com.google.android.accounts.intent.extra.ACCOUNT_TYPE"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    const-string v15, "com.google.android.accounts.intent.extra.AUTHORITY"

    move-object/from16 v0, p2

    invoke-virtual {v11, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string v15, "com.google.android.accounts.intent.extra.BUNDLE"

    move-object/from16 v0, p3

    invoke-virtual {v11, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 163
    const-string v15, "com.google.android.accounts.intent.extra.SUPPORTS_UPLOADING"

    invoke-virtual {v11, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 165
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/CupcakeContentSyncer;->mContext:Landroid/content/Context;

    invoke-virtual {v15, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_b5
    .catchall {:try_start_3a .. :try_end_b5} :catchall_c6

    .line 169
    .end local v11           #serviceIntent:Landroid/content/Intent;
    :cond_b5
    :try_start_b5
    invoke-interface {v14}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_b8
    .catch Ljava/lang/Exception; {:try_start_b5 .. :try_end_b8} :catch_ba

    goto/16 :goto_12

    .line 171
    .end local v1           #accountType:Ljava/lang/String;
    .end local v2           #contentAuthority:Ljava/lang/String;
    .end local v5           #info:Landroid/content/pm/ServiceInfo;
    .end local v6           #metaData:Landroid/os/Bundle;
    .end local v7           #pm:Landroid/content/pm/PackageManager;
    .end local v8           #resId:I
    .end local v9           #resources:Landroid/content/res/Resources;
    .end local v12           #supportsUploading:Z
    .end local v13           #supportsUploadingValue:Ljava/lang/String;
    .end local v14           #xml:Landroid/content/res/XmlResourceParser;
    :catch_ba
    move-exception v3

    .line 172
    .local v3, e:Ljava/lang/Exception;
    const-string v15, "ContentSyncer"

    const-string v16, "Could not read SyncAdapter meta-data"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_12

    .line 169
    .end local v3           #e:Ljava/lang/Exception;
    .restart local v5       #info:Landroid/content/pm/ServiceInfo;
    .restart local v6       #metaData:Landroid/os/Bundle;
    .restart local v7       #pm:Landroid/content/pm/PackageManager;
    .restart local v8       #resId:I
    .restart local v9       #resources:Landroid/content/res/Resources;
    .restart local v14       #xml:Landroid/content/res/XmlResourceParser;
    :catchall_c6
    move-exception v15

    :try_start_c7
    invoke-interface {v14}, Landroid/content/res/XmlResourceParser;->close()V

    throw v15
    :try_end_cb
    .catch Ljava/lang/Exception; {:try_start_c7 .. :try_end_cb} :catch_ba

    .line 175
    .end local v5           #info:Landroid/content/pm/ServiceInfo;
    .end local v6           #metaData:Landroid/os/Bundle;
    .end local v7           #pm:Landroid/content/pm/PackageManager;
    .end local v8           #resId:I
    .end local v9           #resources:Landroid/content/res/Resources;
    .end local v10           #service:Landroid/content/pm/ResolveInfo;
    .end local v14           #xml:Landroid/content/res/XmlResourceParser;
    :cond_cb
    return-void
.end method

.method public setIsSyncable(Lcom/google/android/accounts/Account;Ljava/lang/String;I)V
    .registers 5
    .parameter "account"
    .parameter "authority"
    .parameter "syncable"

    .prologue
    .line 83
    if-eqz p1, :cond_4

    if-nez p2, :cond_a

    .line 84
    :cond_4
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 87
    :cond_a
    return-void
.end method

.method public setSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;Z)V
    .registers 6
    .parameter "account"
    .parameter "authority"
    .parameter "sync"

    .prologue
    .line 100
    if-eqz p1, :cond_4

    if-nez p2, :cond_a

    .line 101
    :cond_4
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 103
    :cond_a
    iget-object v1, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/accounts/CupcakeContentSyncer;->getAuthenticator(Ljava/lang/String;)Lcom/google/android/accounts/DatabaseAuthenticator;

    move-result-object v0

    .line 104
    .local v0, authenticator:Lcom/google/android/accounts/DatabaseAuthenticator;
    if-eqz v0, :cond_15

    .line 105
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/accounts/DatabaseAuthenticator;->setSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;Z)V

    .line 107
    :cond_15
    return-void
.end method
