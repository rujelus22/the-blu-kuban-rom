.class Lcom/google/android/accounts/FroyoContentSyncer;
.super Lcom/google/android/accounts/EclairContentSyncer;
.source "FroyoContentSyncer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/accounts/EclairContentSyncer;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method private static getPeriodicSyncs(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/util/List;
    .registers 3
    .parameter "account"
    .parameter "authority"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/accounts/Account;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/PeriodicSync;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {p0}, Lcom/google/android/accounts/FroyoContentSyncer;->convertAccount(Lcom/google/android/accounts/Account;)Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static length(Ljava/util/List;)Ljava/lang/Integer;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, list:Ljava/util/List;,"Ljava/util/List<*>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public removeAllPeriodicSyncs(Lcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 10
    .parameter "account"
    .parameter "authority"

    .prologue
    .line 45
    invoke-static {p1, p2}, Lcom/google/android/accounts/FroyoContentSyncer;->getPeriodicSyncs(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 46
    .local v1, periodicSyncs:Ljava/util/List;,"Ljava/util/List<Landroid/content/PeriodicSync;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_33

    .line 47
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_24

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/PeriodicSync;

    .line 48
    .local v2, sync:Landroid/content/PeriodicSync;
    iget-object v3, v2, Landroid/content/PeriodicSync;->account:Landroid/accounts/Account;

    iget-object v4, v2, Landroid/content/PeriodicSync;->authority:Ljava/lang/String;

    iget-object v5, v2, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {v3, v4, v5}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_e

    .line 50
    .end local v2           #sync:Landroid/content/PeriodicSync;
    :cond_24
    const-string v3, "Removed %d periodic syncs"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Lcom/google/android/accounts/FroyoContentSyncer;->length(Ljava/util/List;)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_33
    return-void
.end method
