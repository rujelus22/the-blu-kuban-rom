.class Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;
.super Ljava/lang/Object;
.source "ItemContextMenu.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/widget/ItemContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnchorListener"
.end annotation


# static fields
.field private static final HREF:I = 0x1


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;)V
    .registers 4
    .parameter "webView"

    .prologue
    .line 421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->mWebView:Landroid/webkit/WebView;

    .line 424
    move-object v0, p0

    .line 425
    .local v0, callback:Landroid/os/Handler$Callback;
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->mHandler:Landroid/os/Handler;

    .line 426
    return-void
.end method

.method private copy(Ljava/lang/String;)V
    .registers 4
    .parameter "value"

    .prologue
    .line 481
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 482
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/reader/util/SystemService;->getClipboardManager(Landroid/content/Context;)Landroid/text/ClipboardManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 483
    return-void
.end method

.method private open(Ljava/lang/String;)V
    .registers 4
    .parameter "url"

    .prologue
    .line 465
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 466
    .local v0, context:Landroid/content/Context;
    invoke-static {v0, p1}, Lcom/google/android/apps/reader/widget/ItemViewUri;->open(Landroid/content/Context;Ljava/lang/String;)Z

    .line 467
    return-void
.end method

.method private share(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "url"
    .parameter "title"

    .prologue
    .line 470
    iget-object v2, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 471
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 472
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 473
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 474
    const-string v2, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 476
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 477
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .registers 7
    .parameter "msg"

    .prologue
    const/4 v2, 0x0

    .line 440
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_3a

    .line 460
    :cond_6
    :goto_6
    return v2

    .line 442
    :pswitch_7
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "url"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 443
    .local v1, url:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 444
    .local v0, title:Ljava/lang/String;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    .line 447
    iget v2, p1, Landroid/os/Message;->arg1:I

    packed-switch v2, :pswitch_data_40

    .line 458
    :goto_2c
    const/4 v2, 0x1

    goto :goto_6

    .line 449
    :pswitch_2e
    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->open(Ljava/lang/String;)V

    goto :goto_2c

    .line 452
    :pswitch_32
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->share(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2c

    .line 455
    :pswitch_36
    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->copy(Ljava/lang/String;)V

    goto :goto_2c

    .line 440
    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch

    .line 447
    :pswitch_data_40
    .packed-switch 0x7f0b007c
        :pswitch_2e
        :pswitch_32
        :pswitch_36
    .end packed-switch
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v5, 0x1

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;->mHandler:Landroid/os/Handler;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->requestFocusNodeHref(Landroid/os/Message;)V

    .line 433
    return v5
.end method
