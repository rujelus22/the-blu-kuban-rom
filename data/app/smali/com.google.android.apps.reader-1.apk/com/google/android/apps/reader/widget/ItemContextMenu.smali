.class Lcom/google/android/apps/reader/widget/ItemContextMenu;
.super Ljava/lang/Object;
.source "ItemContextMenu.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;,
        Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;,
        Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;,
        Lcom/google/android/apps/reader/widget/ItemContextMenu$CopyListener;,
        Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;,
        Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewImageListener;
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x2000

.field private static final TAG:Ljava/lang/String; = "WebViewContextMenu"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    return-void
.end method

.method private bindImageContextMenuGroup(Landroid/view/ContextMenu;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 16
    .parameter "menu"
    .parameter "context"
    .parameter "imageUrl"
    .parameter "title"
    .parameter "alt"

    .prologue
    .line 181
    const v8, 0x7f0b007f

    const/4 v9, 0x1

    invoke-interface {p1, v8, v9}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    .line 182
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_69

    const/4 v4, 0x1

    .line 183
    .local v4, hasTitle:Z
    :goto_e
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6b

    const/4 v1, 0x1

    .line 184
    .local v1, hasAlt:Z
    :goto_15
    if-eqz v1, :cond_6d

    invoke-virtual {p5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6d

    const/4 v3, 0x1

    .line 185
    .local v3, hasShortAlt:Z
    :goto_1e
    if-nez v4, :cond_22

    if-eqz v1, :cond_6f

    :cond_22
    const/4 v2, 0x1

    .line 186
    .local v2, hasCaption:Z
    :goto_23
    if-eqz v3, :cond_71

    move-object v8, p5

    :goto_26
    invoke-interface {p1, v8}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 187
    const v8, 0x7f0b0080

    invoke-interface {p1, v8}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 188
    .local v7, viewImage:Landroid/view/MenuItem;
    const v8, 0x7f0b0081

    invoke-interface {p1, v8}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 189
    .local v6, viewCaption:Landroid/view/MenuItem;
    const v8, 0x7f0b0082

    invoke-interface {p1, v8}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 190
    .local v0, copyImageUrl:Landroid/view/MenuItem;
    const v8, 0x7f0b0083

    invoke-interface {p1, v8}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 191
    .local v5, setWallpaper:Landroid/view/MenuItem;
    new-instance v8, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewImageListener;

    invoke-direct {v8, p2, p3}, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewImageListener;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 192
    new-instance v8, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;

    invoke-direct {v8, p2, p3, p4, p5}, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 194
    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 195
    new-instance v8, Lcom/google/android/apps/reader/widget/ItemContextMenu$CopyListener;

    invoke-direct {v8, p2, p3}, Lcom/google/android/apps/reader/widget/ItemContextMenu$CopyListener;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 196
    new-instance v8, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;

    invoke-direct {v8, p2, p3}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 197
    return-void

    .line 182
    .end local v0           #copyImageUrl:Landroid/view/MenuItem;
    .end local v1           #hasAlt:Z
    .end local v2           #hasCaption:Z
    .end local v3           #hasShortAlt:Z
    .end local v4           #hasTitle:Z
    .end local v5           #setWallpaper:Landroid/view/MenuItem;
    .end local v6           #viewCaption:Landroid/view/MenuItem;
    .end local v7           #viewImage:Landroid/view/MenuItem;
    :cond_69
    const/4 v4, 0x0

    goto :goto_e

    .line 183
    .restart local v4       #hasTitle:Z
    :cond_6b
    const/4 v1, 0x0

    goto :goto_15

    .line 184
    .restart local v1       #hasAlt:Z
    :cond_6d
    const/4 v3, 0x0

    goto :goto_1e

    .line 185
    .restart local v3       #hasShortAlt:Z
    :cond_6f
    const/4 v2, 0x0

    goto :goto_23

    .restart local v2       #hasCaption:Z
    :cond_71
    move-object v8, p3

    .line 186
    goto :goto_26
.end method

.method private bindLinkContextMenuGroup(Landroid/view/ContextMenu;Landroid/webkit/WebView;)V
    .registers 9
    .parameter "menu"
    .parameter "webView"

    .prologue
    .line 169
    const v4, 0x7f0b007b

    const/4 v5, 0x1

    invoke-interface {p1, v4, v5}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    .line 170
    const v4, 0x7f0b007c

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 171
    .local v2, open:Landroid/view/MenuItem;
    const v4, 0x7f0b007e

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 172
    .local v0, copy:Landroid/view/MenuItem;
    const v4, 0x7f0b007d

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 173
    .local v3, share:Landroid/view/MenuItem;
    new-instance v1, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;

    invoke-direct {v1, p2}, Lcom/google/android/apps/reader/widget/ItemContextMenu$AnchorListener;-><init>(Landroid/webkit/WebView;)V

    .line 174
    .local v1, listener:Landroid/view/MenuItem$OnMenuItemClickListener;
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 175
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 176
    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 177
    return-void
.end method

.method private static findImageAttributes(Landroid/webkit/WebView;Ljava/lang/String;)Lorg/xml/sax/Attributes;
    .registers 12
    .parameter "webView"
    .parameter "imageUrl"

    .prologue
    .line 77
    :try_start_0
    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 78
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 79
    .local v5, resolver:Landroid/content/ContentResolver;
    invoke-virtual {p0}, Landroid/webkit/WebView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 80
    .local v4, map:Ljava/util/Map;,"Ljava/util/Map<**>;"
    const-string v7, "htmlUri"

    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 81
    .local v6, uri:Landroid/net/Uri;
    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_19} :catch_34

    move-result-object v2

    .line 83
    .local v2, input:Ljava/io/InputStream;
    :try_start_1a
    new-instance v3, Ljava/io/BufferedInputStream;

    const/16 v7, 0x2000

    invoke-direct {v3, v2, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_21
    .catchall {:try_start_1a .. :try_end_21} :catchall_2f

    .line 84
    .end local v2           #input:Ljava/io/InputStream;
    .local v3, input:Ljava/io/InputStream;
    :try_start_21
    const-string v7, "UTF-8"

    const-string v8, "img"

    const-string v9, "src"

    invoke-static {v3, v7, v8, v9, p1}, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->findAttributes(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xml/sax/Attributes;
    :try_end_2a
    .catchall {:try_start_21 .. :try_end_2a} :catchall_3e

    move-result-object v7

    .line 87
    :try_start_2b
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 91
    .end local v0           #context:Landroid/content/Context;
    .end local v3           #input:Ljava/io/InputStream;
    .end local v4           #map:Ljava/util/Map;,"Ljava/util/Map<**>;"
    .end local v5           #resolver:Landroid/content/ContentResolver;
    .end local v6           #uri:Landroid/net/Uri;
    :goto_2e
    return-object v7

    .line 87
    .restart local v0       #context:Landroid/content/Context;
    .restart local v2       #input:Ljava/io/InputStream;
    .restart local v4       #map:Ljava/util/Map;,"Ljava/util/Map<**>;"
    .restart local v5       #resolver:Landroid/content/ContentResolver;
    .restart local v6       #uri:Landroid/net/Uri;
    :catchall_2f
    move-exception v7

    :goto_30
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v7
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_34} :catch_34

    .line 89
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #input:Ljava/io/InputStream;
    .end local v4           #map:Ljava/util/Map;,"Ljava/util/Map<**>;"
    .end local v5           #resolver:Landroid/content/ContentResolver;
    .end local v6           #uri:Landroid/net/Uri;
    :catch_34
    move-exception v1

    .line 90
    .local v1, e:Ljava/io/IOException;
    const-string v7, "WebViewContextMenu"

    const-string v8, "onCreateContextMenu: Failed to read HTML"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 91
    const/4 v7, 0x0

    goto :goto_2e

    .line 87
    .end local v1           #e:Ljava/io/IOException;
    .restart local v0       #context:Landroid/content/Context;
    .restart local v3       #input:Ljava/io/InputStream;
    .restart local v4       #map:Ljava/util/Map;,"Ljava/util/Map<**>;"
    .restart local v5       #resolver:Landroid/content/ContentResolver;
    .restart local v6       #uri:Landroid/net/Uri;
    :catchall_3e
    move-exception v7

    move-object v2, v3

    .end local v3           #input:Ljava/io/InputStream;
    .restart local v2       #input:Ljava/io/InputStream;
    goto :goto_30
.end method


# virtual methods
.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 16
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 123
    instance-of v0, p2, Landroid/webkit/WebView;

    if-eqz v0, :cond_d

    move-object v11, p2

    .line 124
    check-cast v11, Landroid/webkit/WebView;

    .line 125
    .local v11, webView:Landroid/webkit/WebView;
    invoke-virtual {v11}, Landroid/webkit/WebView;->getHitTestResult()Landroid/webkit/WebView$HitTestResult;

    move-result-object v7

    .line 126
    .local v7, hitTestResult:Landroid/webkit/WebView$HitTestResult;
    if-nez v7, :cond_e

    .line 166
    .end local v7           #hitTestResult:Landroid/webkit/WebView$HitTestResult;
    .end local v11           #webView:Landroid/webkit/WebView;
    :cond_d
    :goto_d
    return-void

    .line 129
    .restart local v7       #hitTestResult:Landroid/webkit/WebView$HitTestResult;
    .restart local v11       #webView:Landroid/webkit/WebView;
    :cond_e
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 130
    .local v2, context:Landroid/content/Context;
    new-instance v8, Landroid/view/MenuInflater;

    invoke-direct {v8, v2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 131
    .local v8, inflater:Landroid/view/MenuInflater;
    const v0, 0x7f100007

    invoke-virtual {v8, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 132
    invoke-virtual {v7}, Landroid/webkit/WebView$HitTestResult;->getType()I

    move-result v9

    .line 133
    .local v9, type:I
    packed-switch v9, :pswitch_data_68

    .line 144
    :cond_24
    :goto_24
    packed-switch v9, :pswitch_data_70

    :pswitch_27
    goto :goto_d

    .line 147
    :pswitch_28
    invoke-virtual {v7}, Landroid/webkit/WebView$HitTestResult;->getExtra()Ljava/lang/String;

    move-result-object v3

    .line 148
    .local v3, imageUrl:Ljava/lang/String;
    if-nez v3, :cond_48

    .line 149
    const-string v0, "WebViewContextMenu"

    const-string v1, "onCreateContextMenu: HitTestResult extra is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 136
    .end local v3           #imageUrl:Ljava/lang/String;
    :pswitch_36
    invoke-virtual {v7}, Landroid/webkit/WebView$HitTestResult;->getExtra()Ljava/lang/String;

    move-result-object v10

    .line 137
    .local v10, url:Ljava/lang/String;
    if-eqz v10, :cond_44

    const-string v0, "content:"

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 141
    :cond_44
    invoke-direct {p0, p1, v11}, Lcom/google/android/apps/reader/widget/ItemContextMenu;->bindLinkContextMenuGroup(Landroid/view/ContextMenu;Landroid/webkit/WebView;)V

    goto :goto_24

    .line 152
    .end local v10           #url:Ljava/lang/String;
    .restart local v3       #imageUrl:Ljava/lang/String;
    :cond_48
    invoke-static {v11, v3}, Lcom/google/android/apps/reader/widget/ItemContextMenu;->findImageAttributes(Landroid/webkit/WebView;Ljava/lang/String;)Lorg/xml/sax/Attributes;

    move-result-object v6

    .line 155
    .local v6, attributes:Lorg/xml/sax/Attributes;
    if-eqz v6, :cond_60

    .line 156
    const-string v0, "title"

    invoke-interface {v6, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 157
    .local v4, title:Ljava/lang/String;
    const-string v0, "alt"

    invoke-interface {v6, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .local v5, alt:Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    .line 162
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/reader/widget/ItemContextMenu;->bindImageContextMenuGroup(Landroid/view/ContextMenu;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_d

    .line 159
    .end local v4           #title:Ljava/lang/String;
    .end local v5           #alt:Ljava/lang/String;
    :cond_60
    const-string v0, "WebViewContextMenu"

    const-string v1, "onCreateContextMenu: Attributes not found"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 133
    :pswitch_data_68
    .packed-switch 0x7
        :pswitch_36
        :pswitch_36
    .end packed-switch

    .line 144
    :pswitch_data_70
    .packed-switch 0x5
        :pswitch_28
        :pswitch_27
        :pswitch_27
        :pswitch_28
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 6
    .parameter "v"

    .prologue
    const/4 v2, 0x0

    .line 99
    instance-of v3, p1, Landroid/webkit/WebView;

    if-eqz v3, :cond_13

    move-object v1, p1

    .line 100
    check-cast v1, Landroid/webkit/WebView;

    .line 101
    .local v1, webView:Landroid/webkit/WebView;
    invoke-virtual {v1}, Landroid/webkit/WebView;->getHitTestResult()Landroid/webkit/WebView$HitTestResult;

    move-result-object v0

    .line 102
    .local v0, result:Landroid/webkit/WebView$HitTestResult;
    invoke-virtual {v0}, Landroid/webkit/WebView$HitTestResult;->getType()I

    move-result v3

    packed-switch v3, :pswitch_data_20

    .line 115
    .end local v0           #result:Landroid/webkit/WebView$HitTestResult;
    .end local v1           #webView:Landroid/webkit/WebView;
    :cond_13
    :goto_13
    :pswitch_13
    return v2

    .line 105
    .restart local v0       #result:Landroid/webkit/WebView$HitTestResult;
    .restart local v1       #webView:Landroid/webkit/WebView;
    :pswitch_14
    invoke-virtual {v0}, Landroid/webkit/WebView$HitTestResult;->getExtra()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_13

    .line 106
    invoke-virtual {p1}, Landroid/view/View;->showContextMenu()Z

    .line 107
    const/4 v2, 0x1

    goto :goto_13

    .line 102
    nop

    :pswitch_data_20
    .packed-switch 0x5
        :pswitch_14
        :pswitch_13
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method
