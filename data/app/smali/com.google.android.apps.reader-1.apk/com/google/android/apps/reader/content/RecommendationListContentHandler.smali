.class final Lcom/google/android/apps/reader/content/RecommendationListContentHandler;
.super Ljava/net/ContentHandler;
.source "RecommendationListContentHandler.java"


# instance fields
.field private final mAccount:Lcom/google/android/accounts/Account;

.field private final mContext:Landroid/content/Context;

.field private final mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mPosition:I

.field private final mValues:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "database"

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/net/ContentHandler;-><init>()V

    .line 55
    if-nez p2, :cond_b

    .line 56
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58
    :cond_b
    if-nez p1, :cond_13

    .line 59
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61
    :cond_13
    if-nez p3, :cond_1b

    .line 62
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64
    :cond_1b
    iput-object p1, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 67
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    .line 68
    return-void
.end method

.method private clearRecommendations()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 97
    const-string v1, "account_name = ?"

    .line 98
    .local v1, whereClause:Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v2, v2, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v0, v4

    .line 101
    .local v0, whereArgs:[Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "recommendations"

    invoke-virtual {v2, v3, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 103
    iput v4, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mPosition:I

    .line 104
    return-void
.end method

.method private insertRecommendation(Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;)V
    .registers 9
    .parameter "recommendation"

    .prologue
    .line 107
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 109
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    const-string v4, "account_name"

    iget-object v5, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v5, v5, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    const-string v4, "account_type"

    iget-object v5, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v5, v5, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;->getStreamId()Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, streamId:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-long v0, v3

    .line 114
    .local v0, baseId:J
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    const-string v4, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 115
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    const-string v4, "position"

    iget v5, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mPosition:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mPosition:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 116
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    const-string v4, "id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    const-string v4, "title"

    invoke-virtual {p1}, Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p1}, Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;->hasSnippet()Z

    move-result v3

    if-eqz v3, :cond_67

    .line 119
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    const-string v4, "snippet"

    invoke-virtual {p1}, Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;->getSnippet()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_67
    iget-object v3, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "recommendations"

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 123
    return-void
.end method

.method private notifyChange()V
    .registers 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->notifyChange(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 127
    return-void
.end method


# virtual methods
.method public getContent(Ljava/net/URLConnection;)Ljava/lang/Object;
    .registers 8
    .parameter "connection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {p1}, Lcom/google/android/feeds/ContentHandlerUtils;->getUncompressedInputStream(Ljava/net/URLConnection;)Ljava/io/InputStream;

    move-result-object v1

    .line 75
    .local v1, input:Ljava/io/InputStream;
    :try_start_4
    invoke-static {v1}, Lcom/google/feedreader/extrpc/Client$RecommendationList;->parseFrom(Ljava/io/InputStream;)Lcom/google/feedreader/extrpc/Client$RecommendationList;
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_32

    move-result-object v3

    .line 77
    .local v3, recommendations:Lcom/google/feedreader/extrpc/Client$RecommendationList;
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 80
    iget-object v4, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 82
    :try_start_10
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->clearRecommendations()V

    .line 83
    invoke-virtual {v3}, Lcom/google/feedreader/extrpc/Client$RecommendationList;->getRecsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_37

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;

    .line 84
    .local v2, r:Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;
    invoke-direct {p0, v2}, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->insertRecommendation(Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;)V
    :try_end_2a
    .catchall {:try_start_10 .. :try_end_2a} :catchall_2b

    goto :goto_1b

    .line 88
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/google/feedreader/extrpc/Client$RecommendationList$Recommendation;
    :catchall_2b
    move-exception v4

    iget-object v5, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    .line 77
    .end local v3           #recommendations:Lcom/google/feedreader/extrpc/Client$RecommendationList;
    :catchall_32
    move-exception v4

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v4

    .line 86
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v3       #recommendations:Lcom/google/feedreader/extrpc/Client$RecommendationList;
    :cond_37
    :try_start_37
    iget-object v4, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3c
    .catchall {:try_start_37 .. :try_end_3c} :catchall_2b

    .line 88
    iget-object v4, p0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;->notifyChange()V

    .line 93
    invoke-virtual {v3}, Lcom/google/feedreader/extrpc/Client$RecommendationList;->getRecsCount()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/feeds/FeedLoader;->documentInfo(I)Ljava/lang/Object;

    move-result-object v4

    return-object v4
.end method
