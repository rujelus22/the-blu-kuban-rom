.class public Lcom/google/android/apps/reader/content/StreamContentHandler;
.super Lcom/google/android/apps/reader/content/ItemsContentHandler;
.source "StreamContentHandler.java"


# instance fields
.field private mConnection:Ljava/net/URLConnection;

.field private final mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private final mExtras:Landroid/os/Bundle;

.field private final mItemList:Lcom/google/android/apps/reader/content/ItemList;

.field private mTimestamp:J

.field private final mValues:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/reader/content/ItemList;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ReaderFileCache;Landroid/os/Bundle;)V
    .registers 12
    .parameter "context"
    .parameter "itemList"
    .parameter "database"
    .parameter "fileCache"
    .parameter "extras"

    .prologue
    .line 79
    invoke-virtual {p2}, Lcom/google/android/apps/reader/content/ItemList;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/reader/content/ItemsContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ReaderFileCache;Landroid/os/Bundle;)V

    .line 80
    iput-object p2, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mItemList:Lcom/google/android/apps/reader/content/ItemList;

    .line 81
    iput-object p3, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 82
    iput-object p5, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mExtras:Landroid/os/Bundle;

    .line 83
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mValues:Landroid/content/ContentValues;

    .line 84
    return-void
.end method

.method private deleteItemPositions()V
    .registers 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mItemList:Lcom/google/android/apps/reader/content/ItemList;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/ItemList;->deleteItemPositions(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 95
    return-void
.end method

.method private getMaxPosition()I
    .registers 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mItemList:Lcom/google/android/apps/reader/content/ItemList;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/ItemList;->getMaxPosition(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    return v0
.end method

.method private static isContinuation(Ljava/net/URLConnection;)Z
    .registers 5
    .parameter "connection"

    .prologue
    .line 50
    invoke-virtual {p0}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object v2

    .line 51
    .local v2, url:Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, spec:Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 53
    .local v1, uri:Landroid/net/Uri;
    const-string v3, "c"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_16

    const/4 v3, 0x1

    :goto_15
    return v3

    :cond_16
    const/4 v3, 0x0

    goto :goto_15
.end method

.method private replaceItemList(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 16
    .parameter "date"
    .parameter "continuation"
    .parameter "title"
    .parameter "description"

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mItemList:Lcom/google/android/apps/reader/content/ItemList;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v2, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mValues:Landroid/content/ContentValues;

    iget-wide v3, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mTimestamp:J

    move-wide v5, p1

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/reader/content/ItemList;->replaceItemList(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method private setItemPosition(Lorg/json/JSONObject;I)V
    .registers 10
    .parameter "item"
    .parameter "position"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 98
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 99
    .local v6, externalId:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/apps/reader/provider/ReaderItem;->getItemId(Ljava/lang/String;)J

    move-result-wide v3

    .line 100
    .local v3, itemId:J
    iget-object v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mItemList:Lcom/google/android/apps/reader/content/ItemList;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v2, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mValues:Landroid/content/ContentValues;

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/reader/content/ItemList;->setItemPosition(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;JI)V

    .line 101
    return-void
.end method


# virtual methods
.method public getContent(Ljava/net/URLConnection;)Ljava/lang/Object;
    .registers 4
    .parameter "connection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mTimestamp:J

    .line 111
    iput-object p1, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mConnection:Ljava/net/URLConnection;

    .line 112
    invoke-super {p0, p1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->getContent(Ljava/net/URLConnection;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected handleFeed(Lorg/json/JSONObject;)Ljava/lang/Object;
    .registers 15
    .parameter "feed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v6, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mExtras:Landroid/os/Bundle;

    .line 118
    .local v6, extras:Landroid/os/Bundle;
    const/4 v11, 0x0

    .line 120
    .local v11, position:I
    iget-object v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mConnection:Ljava/net/URLConnection;

    invoke-virtual {v0}, Ljava/net/URLConnection;->getDate()J

    move-result-wide v1

    .line 121
    .local v1, date:J
    const-string v0, "continuation"

    const/4 v12, 0x0

    invoke-virtual {p1, v0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 122
    .local v3, continuation:Ljava/lang/String;
    const-string v0, "title"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/reader/content/PlainText;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 123
    .local v4, title:Ljava/lang/String;
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/reader/content/PlainText;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .local v5, description:Ljava/lang/String;
    move-object v0, p0

    .line 124
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/reader/content/StreamContentHandler;->replaceItemList(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/reader/content/StreamContentHandler;->mConnection:Ljava/net/URLConnection;

    invoke-static {v0}, Lcom/google/android/apps/reader/content/StreamContentHandler;->isContinuation(Ljava/net/URLConnection;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 126
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/StreamContentHandler;->getMaxPosition()I

    move-result v0

    add-int/lit8 v11, v0, 0x1

    .line 130
    :goto_36
    const-string v0, "items"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 131
    .local v10, items:Lorg/json/JSONArray;
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v9

    .line 132
    .local v9, itemCount:I
    const/4 v7, 0x0

    .local v7, i:I
    :goto_41
    if-ge v7, v9, :cond_53

    .line 133
    invoke-virtual {v10, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 134
    .local v8, item:Lorg/json/JSONObject;
    invoke-direct {p0, v8, v11}, Lcom/google/android/apps/reader/content/StreamContentHandler;->setItemPosition(Lorg/json/JSONObject;I)V

    .line 135
    add-int/lit8 v11, v11, 0x1

    .line 132
    add-int/lit8 v7, v7, 0x1

    goto :goto_41

    .line 128
    .end local v7           #i:I
    .end local v8           #item:Lorg/json/JSONObject;
    .end local v9           #itemCount:I
    .end local v10           #items:Lorg/json/JSONArray;
    :cond_4f
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/StreamContentHandler;->deleteItemPositions()V

    goto :goto_36

    .line 138
    .restart local v7       #i:I
    .restart local v9       #itemCount:I
    .restart local v10       #items:Lorg/json/JSONArray;
    :cond_53
    invoke-super {p0, p1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->handleFeed(Lorg/json/JSONObject;)Ljava/lang/Object;

    .line 141
    const-string v0, "android.intent.extra.TITLE"

    invoke-virtual {v6, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v0, "com.google.reader.cursor.extra.DESCRIPTION"

    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-static {v9, v3}, Lcom/google/android/feeds/FeedLoader;->documentInfo(ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
