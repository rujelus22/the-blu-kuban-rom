.class public Lcom/google/android/apps/reader/app/StreamActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "StreamActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;


# static fields
.field private static final TAG:Ljava/lang/String; = "StreamActivity"


# instance fields
.field private mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

.field private mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private changeIntent(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeIntent(Landroid/content/Intent;)V

    .line 173
    return-void
.end method

.method private refresh()V
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->refresh()V

    .line 137
    return-void
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"

    .prologue
    .line 187
    const-string v0, "reader_window"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    .line 190
    :goto_a
    return-object v0

    :cond_b
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_a
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 177
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 178
    invoke-static {p3}, Lcom/google/android/apps/reader/app/ItemActivity;->hasStream(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->finish()V

    .line 183
    :cond_c
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_14

    .line 82
    :goto_7
    return-void

    .line 76
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->showMainScreen()V

    goto :goto_7

    .line 79
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->refresh()V

    goto :goto_7

    .line 74
    :pswitch_data_14
    .packed-switch 0x7f0b0003
        :pswitch_8
        :pswitch_e
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    new-instance v1, Lcom/google/android/apps/reader/widget/ReaderWindow;

    invoke-direct {v1, p0}, Lcom/google/android/apps/reader/widget/ReaderWindow;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    .line 50
    iget-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->requestCustomTitle()V

    .line 51
    iget-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setHomeButtonEnabled(Z)V

    .line 52
    const v1, 0x7f03002e

    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/app/StreamActivity;->setContentView(I)V

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    const v2, 0x7f03001d

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setCustomTitleLayout(I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 56
    .local v0, fragments:Landroid/support/v4/app/FragmentManager;
    const v1, 0x7f0b0032

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/reader/fragment/StreamFragment;

    iput-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->setObserver(Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;)V

    .line 59
    if-nez p1, :cond_40

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/app/StreamActivity;->changeIntent(Landroid/content/Intent;)V

    .line 62
    :cond_40
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 162
    packed-switch p1, :pswitch_data_1a

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_12

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_17

    :cond_12
    :goto_12
    return v0

    .line 164
    :pswitch_13
    invoke-direct {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->refresh()V

    goto :goto_12

    .line 167
    :cond_17
    const/4 v0, 0x0

    goto :goto_12

    .line 162
    nop

    :pswitch_data_1a
    .packed-switch 0x2e
        :pswitch_13
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/app/StreamActivity;->changeIntent(Landroid/content/Intent;)V

    .line 68
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 120
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_1c

    .line 131
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_c
    return v0

    .line 122
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->showMainScreen()V

    goto :goto_c

    .line 125
    :sswitch_13
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->onSearchRequested()Z

    goto :goto_c

    .line 128
    :sswitch_17
    invoke-direct {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->refresh()V

    goto :goto_c

    .line 120
    nop

    :sswitch_data_1c
    .sparse-switch
        0x102002c -> :sswitch_d
        0x7f0b0096 -> :sswitch_17
        0x7f0b0097 -> :sswitch_13
    .end sparse-switch
.end method

.method public onSearchRequested()Z
    .registers 10

    .prologue
    .line 141
    iget-object v7, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 142
    .local v0, account:Lcom/google/android/accounts/Account;
    if-nez v0, :cond_11

    .line 143
    const-string v7, "StreamActivity"

    const-string v8, "Cannot search without an account"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v7, 0x0

    .line 157
    :goto_10
    return v7

    .line 146
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/StreamActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 147
    .local v4, intent:Landroid/content/Intent;
    const-string v7, "query"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 148
    .local v3, initialQuery:Ljava/lang/String;
    const/4 v5, 0x1

    .line 149
    .local v5, selectInitialQuery:Z
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 150
    .local v1, appSearchData:Landroid/os/Bundle;
    const-string v7, "authAccount"

    iget-object v8, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v7, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getStreamId()Ljava/lang/String;

    move-result-object v6

    .line 152
    .local v6, streamId:Ljava/lang/String;
    if-eqz v6, :cond_35

    .line 153
    const-string v7, "stream_id"

    invoke-virtual {v1, v7, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_35
    const/4 v2, 0x0

    .line 156
    .local v2, globalSearch:Z
    invoke-virtual {p0, v3, v5, v1, v2}, Lcom/google/android/apps/reader/app/StreamActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 157
    const/4 v7, 0x1

    goto :goto_10
.end method

.method public onStreamChanged()V
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getStreamTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/app/StreamActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 89
    return-void
.end method

.method public onStreamLoaded()V
    .registers 1

    .prologue
    .line 95
    return-void
.end method

.method public onStreamUnloaded()V
    .registers 1

    .prologue
    .line 101
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter "title"
    .parameter "color"

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/reader/app/StreamActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method
