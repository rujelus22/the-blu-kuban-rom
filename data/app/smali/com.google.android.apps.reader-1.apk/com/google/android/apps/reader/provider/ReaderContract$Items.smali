.class public Lcom/google/android/apps/reader/provider/ReaderContract$Items;
.super Ljava/lang/Object;
.source "ReaderContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/google/android/apps/reader/provider/ReaderContract$FriendsColumns;
.implements Lcom/google/android/apps/reader/provider/ReaderContract$ItemsColumns;
.implements Lcom/google/android/apps/reader/provider/ReaderContract$SyncColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/provider/ReaderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Items"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = null

.field public static final CONTENT_TYPE:Ljava/lang/String; = null

.field public static final HTML_CHARSET:Ljava/lang/String; = "UTF-8"

.field public static final HTML_MARKER_END:Ljava/lang/String; = "<!--[/HTML]-->"

.field public static final HTML_MARKER_START:Ljava/lang/String; = "<!--[HTML]-->"

.field public static final RANKING_AUTO:Ljava/lang/String; = "auto"

.field public static final RANKING_DATE:Ljava/lang/String; = "date"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RANKING_HYBRID:Ljava/lang/String; = "hybrid"

.field public static final RANKING_MAGIC:Ljava/lang/String; = "magic"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RANKING_NEWEST_FIRST:Ljava/lang/String; = "newest"

.field public static final RANKING_OLDEST_FIRST:Ljava/lang/String; = "oldest"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1740
    const-string v0, "item"

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract;->contentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_TYPE:Ljava/lang/String;

    .line 1742
    const-string v0, "item"

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract;->contentItemType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 2065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066
    return-void
.end method

.method public static getAnnotation(Landroid/net/Uri;)Ljava/lang/String;
    .registers 2
    .parameter "uri"

    .prologue
    .line 2037
    const-string v0, "annotation"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getExcludeTarget(Landroid/net/Uri;)Ljava/lang/String;
    .registers 2
    .parameter "uri"

    .prologue
    .line 2014
    const-string v0, "xt"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHtmlAsString(Landroid/content/ContentResolver;Lcom/google/android/accounts/Account;J)Ljava/lang/String;
    .registers 18
    .parameter "resolver"
    .parameter "account"
    .parameter "itemId"

    .prologue
    .line 1849
    invoke-static/range {p1 .. p3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->htmlUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;

    move-result-object v11

    .line 1851
    .local v11, uri:Landroid/net/Uri;
    :try_start_4
    const-string v12, "r"

    invoke-virtual {p0, v11, v12}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v3

    .line 1852
    .local v3, fd:Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v6

    .line 1853
    .local v6, length:J
    const-wide/16 v12, -0x1

    cmp-long v12, v6, v12

    if-nez v12, :cond_16

    .line 1854
    const-wide/16 v6, 0x2000

    .line 1856
    :cond_16
    new-instance v1, Ljava/lang/StringBuilder;

    long-to-int v12, v6

    invoke-direct {v1, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1857
    .local v1, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v5

    .line 1858
    .local v5, in:Ljava/io/InputStream;
    new-instance v9, Ljava/io/InputStreamReader;

    const-string v12, "UTF-8"

    invoke-direct {v9, v5, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_27} :catch_65

    .line 1860
    .local v9, reader:Ljava/io/InputStreamReader;
    const/16 v12, 0x400

    :try_start_29
    new-array v0, v12, [C

    .line 1861
    .local v0, buffer:[C
    invoke-virtual {v9, v0}, Ljava/io/InputStreamReader;->read([C)I

    move-result v8

    .local v8, n:I
    :goto_2f
    const/4 v12, -0x1

    if-eq v8, v12, :cond_3b

    .line 1862
    const/4 v12, 0x0

    invoke-virtual {v1, v0, v12, v8}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1861
    invoke-virtual {v9, v0}, Ljava/io/InputStreamReader;->read([C)I
    :try_end_39
    .catchall {:try_start_29 .. :try_end_39} :catchall_60

    move-result v8

    goto :goto_2f

    .line 1865
    :cond_3b
    :try_start_3b
    invoke-virtual {v9}, Ljava/io/InputStreamReader;->close()V

    .line 1867
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1868
    .local v4, html:Ljava/lang/String;
    const-string v12, "<!--[HTML]-->"

    invoke-virtual {v4, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 1869
    .local v10, start:I
    const-string v12, "<!--[/HTML]-->"

    invoke-virtual {v4, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 1870
    .local v2, end:I
    const/4 v12, -0x1

    if-eq v10, v12, :cond_5f

    const/4 v12, -0x1

    if-eq v2, v12, :cond_5f

    .line 1871
    const-string v12, "<!--[HTML]-->"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v10, v12

    .line 1872
    invoke-virtual {v4, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1877
    .end local v0           #buffer:[C
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v2           #end:I
    .end local v3           #fd:Landroid/content/res/AssetFileDescriptor;
    .end local v4           #html:Ljava/lang/String;
    .end local v5           #in:Ljava/io/InputStream;
    .end local v6           #length:J
    .end local v8           #n:I
    .end local v9           #reader:Ljava/io/InputStreamReader;
    .end local v10           #start:I
    :cond_5f
    :goto_5f
    return-object v4

    .line 1865
    .restart local v1       #builder:Ljava/lang/StringBuilder;
    .restart local v3       #fd:Landroid/content/res/AssetFileDescriptor;
    .restart local v5       #in:Ljava/io/InputStream;
    .restart local v6       #length:J
    .restart local v9       #reader:Ljava/io/InputStreamReader;
    :catchall_60
    move-exception v12

    invoke-virtual {v9}, Ljava/io/InputStreamReader;->close()V

    throw v12
    :try_end_65
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_65} :catch_65

    .line 1877
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v3           #fd:Landroid/content/res/AssetFileDescriptor;
    .end local v5           #in:Ljava/io/InputStream;
    .end local v6           #length:J
    .end local v9           #reader:Ljava/io/InputStreamReader;
    :catch_65
    move-exception v12

    const/4 v4, 0x0

    goto :goto_5f
.end method

.method public static getQuery(Landroid/net/Uri;)Ljava/lang/String;
    .registers 2
    .parameter "uri"

    .prologue
    .line 2029
    const-string v0, "q"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRanking(Landroid/net/Uri;)Ljava/lang/String;
    .registers 2
    .parameter "uri"

    .prologue
    .line 2022
    const-string v0, "r"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getShare(Landroid/net/Uri;)Z
    .registers 3
    .parameter "uri"

    .prologue
    .line 2053
    const-string v1, "share"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2054
    .local v0, share:Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static getSnippet(Landroid/net/Uri;)Ljava/lang/String;
    .registers 2
    .parameter "uri"

    .prologue
    .line 2045
    const-string v0, "snippet"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getStreamId(Landroid/net/Uri;)Ljava/lang/String;
    .registers 2
    .parameter "uri"

    .prologue
    .line 2006
    const-string v0, "stream_id"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTags(Landroid/net/Uri;)Ljava/util/List;
    .registers 2
    .parameter "uri"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2062
    const-string v0, "tag"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static htmlUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;
    .registers 6
    .parameter "account"
    .parameter "itemId"

    .prologue
    .line 1832
    if-nez p0, :cond_a

    .line 1833
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Account is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1835
    :cond_a
    sget-object v1, Lcom/google/android/apps/reader/provider/ReaderContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1836
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "item_html"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1837
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1838
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1839
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static insertUri(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)Landroid/net/Uri;
    .registers 10
    .parameter "account"
    .parameter "snippet"
    .parameter "annotation"
    .parameter "share"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .line 1892
    .local p4, tags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p0, :cond_a

    .line 1893
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Account is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1895
    :cond_a
    sget-object v3, Lcom/google/android/apps/reader/provider/ReaderContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1896
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v3, "items"

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1897
    const-string v3, "account_name"

    iget-object v4, p0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1898
    if-eqz p1, :cond_23

    .line 1899
    const-string v3, "snippet"

    invoke-virtual {v0, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1901
    :cond_23
    if-eqz p2, :cond_2a

    .line 1902
    const-string v3, "annotation"

    invoke-virtual {v0, v3, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1904
    :cond_2a
    const-string v3, "share"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1905
    if-eqz p4, :cond_4b

    .line 1906
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_39
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1907
    .local v2, tag:Ljava/lang/String;
    const-string v3, "tag"

    invoke-virtual {v0, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_39

    .line 1910
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #tag:Ljava/lang/String;
    :cond_4b
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method public static itemUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;
    .registers 6
    .parameter "account"
    .parameter "itemId"

    .prologue
    .line 1818
    if-nez p0, :cond_a

    .line 1819
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Account is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1821
    :cond_a
    sget-object v1, Lcom/google/android/apps/reader/provider/ReaderContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1822
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1823
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1824
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1825
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static markAllAsRead(Landroid/content/AsyncQueryHandler;ILjava/lang/Object;Landroid/net/Uri;)V
    .registers 10
    .parameter "handler"
    .parameter "token"
    .parameter "cookie"
    .parameter "uri"

    .prologue
    .line 1938
    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->markAllAsRead(Landroid/content/AsyncQueryHandler;ILjava/lang/Object;Landroid/net/Uri;J)V

    .line 1939
    return-void
.end method

.method public static markAllAsRead(Landroid/content/AsyncQueryHandler;ILjava/lang/Object;Landroid/net/Uri;J)V
    .registers 13
    .parameter "handler"
    .parameter "token"
    .parameter "cookie"
    .parameter "uri"
    .parameter "offset"

    .prologue
    const-wide/16 v2, 0x0

    .line 1923
    cmp-long v0, p4, v2

    if-lez v0, :cond_e

    .line 1924
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Positive offsets are not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1926
    :cond_e
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1927
    .local v4, values:Landroid/content/ContentValues;
    const-string v0, "read"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1928
    cmp-long v0, p4, v2

    if-eqz v0, :cond_3a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "crawl_time < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1929
    .local v5, selection:Ljava/lang/String;
    :goto_31
    const/4 v6, 0x0

    .local v6, selectionArgs:[Ljava/lang/String;
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 1930
    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1931
    return-void

    .line 1928
    .end local v5           #selection:Ljava/lang/String;
    .end local v6           #selectionArgs:[Ljava/lang/String;
    :cond_3a
    const/4 v5, 0x0

    goto :goto_31
.end method

.method public static searchUri(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 6
    .parameter "account"
    .parameter "query"
    .parameter "streamId"

    .prologue
    .line 1801
    if-nez p0, :cond_a

    .line 1802
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Account is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1804
    :cond_a
    if-nez p1, :cond_14

    .line 1805
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Query is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1807
    :cond_14
    sget-object v1, Lcom/google/android/apps/reader/provider/ReaderContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1808
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1809
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1810
    const-string v1, "q"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1811
    if-eqz p2, :cond_32

    .line 1812
    const-string v1, "stream_id"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1814
    :cond_32
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static setAccount(Landroid/net/Uri;Lcom/google/android/accounts/Account;)Landroid/net/Uri;
    .registers 7
    .parameter "uri"
    .parameter "account"

    .prologue
    .line 1989
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1990
    .local v2, streamId:Ljava/lang/String;
    const-string v3, "user/-/state/com.google/read"

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getExcludeTarget(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1991
    .local v0, excludeRead:Z
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getRanking(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1992
    .local v1, ranking:Ljava/lang/String;
    if-nez v2, :cond_1c

    .line 1993
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Stream ID not set"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1995
    :cond_1c
    if-nez v1, :cond_26

    .line 1996
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Ranking not set"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1998
    :cond_26
    invoke-static {p1, v2, v0, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method public static setExcludeRead(Landroid/net/Uri;Z)Landroid/net/Uri;
    .registers 6
    .parameter "uri"
    .parameter "excludeRead"

    .prologue
    .line 1951
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 1952
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1953
    .local v2, streamId:Ljava/lang/String;
    if-nez v2, :cond_10

    .line 1954
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    .line 1956
    :cond_10
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getRanking(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1957
    .local v1, ranking:Ljava/lang/String;
    invoke-static {v0, v2, p1, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method public static setRanking(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .registers 7
    .parameter "uri"
    .parameter "ranking"

    .prologue
    .line 1971
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 1972
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1973
    .local v2, streamId:Ljava/lang/String;
    if-nez v2, :cond_12

    .line 1974
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Stream ID not set"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1976
    :cond_12
    const-string v3, "user/-/state/com.google/read"

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getExcludeTarget(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1977
    .local v1, excludeRead:Z
    invoke-static {v0, v2, v1, p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method public static streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;
    .registers 7
    .parameter "account"
    .parameter "streamId"
    .parameter "excludeRead"
    .parameter "ranking"

    .prologue
    .line 1771
    if-nez p0, :cond_a

    .line 1772
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Account is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1774
    :cond_a
    if-nez p1, :cond_14

    .line 1775
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Stream ID is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1777
    :cond_14
    if-nez p3, :cond_1e

    .line 1778
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Ranking is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1780
    :cond_1e
    sget-object v1, Lcom/google/android/apps/reader/provider/ReaderContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1781
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1782
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1783
    const-string v1, "stream_id"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1784
    if-eqz p2, :cond_3e

    .line 1785
    const-string v1, "xt"

    const-string v2, "user/-/state/com.google/read"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1787
    :cond_3e
    const-string v1, "r"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1788
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method
