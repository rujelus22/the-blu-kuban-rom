.class public final Lcom/google/android/apps/reader/widget/ItemViewFragment;
.super Landroid/support/v4/app/Fragment;
.source "ItemViewFragment.java"


# static fields
#the value of this static final field might be set in the static constructor
.field private static final PAUSE_WEBVIEW:Z = false

.field private static final STATE_VALUES:Ljava/lang/String; = "reader:values"


# instance fields
.field private mContentValues:Landroid/content/ContentValues;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_7
    sput-boolean v0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->PAUSE_WEBVIEW:Z

    return-void

    :cond_a
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/ContentValues;)V
    .registers 2
    .parameter "values"

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mContentValues:Landroid/content/ContentValues;

    .line 51
    return-void
.end method

.method private static contentValuesToCursorRow([Ljava/lang/String;Landroid/content/ContentValues;)Landroid/database/Cursor;
    .registers 9
    .parameter "columns"
    .parameter "values"

    .prologue
    .line 126
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, p0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 127
    .local v2, cursor:Landroid/database/MatrixCursor;
    invoke-virtual {v2}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    .line 128
    .local v5, row:Landroid/database/MatrixCursor$RowBuilder;
    move-object v0, p0

    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_c
    if-ge v3, v4, :cond_1a

    aget-object v1, v0, v3

    .line 129
    .local v1, columnName:Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 128
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 131
    .end local v1           #columnName:Ljava/lang/String;
    :cond_1a
    invoke-virtual {v2}, Landroid/database/MatrixCursor;->moveToFirst()Z

    .line 132
    return-object v2
.end method

.method private static invoke(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 6
    .parameter "obj"
    .parameter "method"

    .prologue
    .line 110
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 111
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, p1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 112
    .local v2, m:Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_11} :catch_12

    .line 116
    .end local v0           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #m:Ljava/lang/reflect/Method;
    :goto_11
    return-void

    .line 113
    :catch_12
    move-exception v1

    .line 114
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_11
.end method


# virtual methods
.method public getItemId()J
    .registers 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mContentValues:Landroid/content/ContentValues;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getProgress()I
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getProgress()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 9
    .parameter "inflater"
    .parameter "root"
    .parameter "savedInstanceState"

    .prologue
    .line 62
    if-eqz p3, :cond_c

    .line 63
    const-string v3, "reader:values"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    iput-object v3, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mContentValues:Landroid/content/ContentValues;

    .line 65
    :cond_c
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 66
    .local v0, context:Landroid/content/Context;
    invoke-static {p2}, Lcom/google/android/apps/reader/widget/ItemViewFactory;->newView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 67
    .local v2, view:Landroid/view/View;
    const v3, 0x7f0b003b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/webkit/WebView;

    iput-object v3, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    .line 68
    sget-object v3, Lcom/google/android/apps/reader/widget/ItemQuery;->PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mContentValues:Landroid/content/ContentValues;

    invoke-static {v3, v4}, Lcom/google/android/apps/reader/widget/ItemViewFragment;->contentValuesToCursorRow([Ljava/lang/String;Landroid/content/ContentValues;)Landroid/database/Cursor;

    move-result-object v1

    .line 69
    .local v1, cursor:Landroid/database/Cursor;
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 70
    return-object v2
.end method

.method public onDestroyView()V
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    .line 99
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 100
    return-void
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 89
    sget-boolean v0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->PAUSE_WEBVIEW:Z

    if-eqz v0, :cond_b

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/widget/ItemViewFragment;->invoke(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    :cond_b
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 93
    return-void
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 81
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 82
    sget-boolean v0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->PAUSE_WEBVIEW:Z

    if-eqz v0, :cond_e

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/widget/ItemViewFragment;->invoke(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    :cond_e
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 76
    const-string v0, "reader:values"

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mContentValues:Landroid/content/ContentValues;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 77
    return-void
.end method

.method public scrollToTop()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_a

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, v1}, Landroid/webkit/WebView;->scrollTo(II)V

    .line 143
    :cond_a
    return-void
.end method
