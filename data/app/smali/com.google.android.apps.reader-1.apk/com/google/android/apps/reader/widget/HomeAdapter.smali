.class public Lcom/google/android/apps/reader/widget/HomeAdapter;
.super Lcom/google/android/apps/reader/widget/StreamListAdapter;
.source "HomeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/widget/HomeAdapter$1;,
        Lcom/google/android/apps/reader/widget/HomeAdapter$UnreadCountsDataSetObserver;
    }
.end annotation


# static fields
.field private static final GROUP_COLLAPSED_STATE_SET:[I = null

.field private static final GROUP_EXPANDED_STATE_SET:[I = null

.field public static final ITEM_EXPLORE:Ljava/lang/Object; = null

.field public static final ITEM_HOME:Ljava/lang/Object; = null

.field public static final ITEM_SUBSCRIPTIONS:Ljava/lang/Object; = null

.field public static final PARAM_FILTERED:Ljava/lang/String; = "filtered"

.field private static final VIEW_TYPE_COUNT:I = 0x3

.field private static final VIEW_TYPE_EMPTY:I = 0x2

.field private static final VIEW_TYPE_HEADER:I = 0x0

.field private static final VIEW_TYPE_ITEM:I = 0x1


# instance fields
.field private mExpanded:Z

.field private final mKeysCollapsed:[Ljava/lang/String;

.field private final mKeysExpanded:[Ljava/lang/String;

.field private final mLabelsCollapsed:[Ljava/lang/String;

.field private final mLabelsExpanded:[Ljava/lang/String;

.field private final mLayoutsCollapsed:[I

.field private final mLayoutsExpanded:[I

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private mShowAllSubscriptions:Z

.field private final mUnreadCounts:Lcom/google/android/apps/reader/widget/UnreadCountsQuery;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 60
    const-string v0, "home"

    sput-object v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->ITEM_HOME:Ljava/lang/Object;

    .line 62
    const-string v0, "explore"

    sput-object v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->ITEM_EXPLORE:Ljava/lang/Object;

    .line 64
    const-string v0, "subscriptions"

    sput-object v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->ITEM_SUBSCRIPTIONS:Ljava/lang/Object;

    .line 82
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->GROUP_COLLAPSED_STATE_SET:[I

    .line 85
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100a8

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->GROUP_EXPANDED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/reader/widget/UnreadCountsQuery;Landroid/view/View$OnClickListener;)V
    .registers 8
    .parameter "context"
    .parameter "unreadCounts"
    .parameter "onClickListener"

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/StreamListAdapter;-><init>(Landroid/content/Context;)V

    .line 124
    iput-object p2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mUnreadCounts:Lcom/google/android/apps/reader/widget/UnreadCountsQuery;

    .line 125
    iput-object p3, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mUnreadCounts:Lcom/google/android/apps/reader/widget/UnreadCountsQuery;

    new-instance v2, Lcom/google/android/apps/reader/widget/HomeAdapter$UnreadCountsDataSetObserver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/reader/widget/HomeAdapter$UnreadCountsDataSetObserver;-><init>(Lcom/google/android/apps/reader/widget/HomeAdapter;Lcom/google/android/apps/reader/widget/HomeAdapter$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/widget/UnreadCountsQuery;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 127
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 128
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x7f070003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 129
    const/high16 v1, 0x7f07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    .line 130
    const v1, 0x7f070004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLabelsExpanded:[Ljava/lang/String;

    .line 131
    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLabelsCollapsed:[Ljava/lang/String;

    .line 132
    const v1, 0x7f070005

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getResourceIdArray(Landroid/content/res/Resources;I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLayoutsExpanded:[I

    .line 133
    const v1, 0x7f070002

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getResourceIdArray(Landroid/content/res/Resources;I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLayoutsCollapsed:[I

    .line 134
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/reader/widget/HomeAdapter;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mDataValid:Z

    return v0
.end method

.method private createExploreStreamId()Ljava/lang/String;
    .registers 4

    .prologue
    .line 478
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, language:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getUserId()Ljava/lang/String;

    move-result-object v1

    .line 480
    .local v1, userId:Ljava/lang/String;
    if-eqz v1, :cond_f

    .line 481
    invoke-static {v1, v0}, Lcom/google/android/apps/reader/provider/ReaderStream;->createSplicedRecommendationsStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 483
    :goto_e
    return-object v2

    :cond_f
    const-string v2, "top"

    invoke-static {v2, v0}, Lcom/google/android/apps/reader/provider/ReaderStream;->createPopularStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_e
.end method

.method private findLabel(Ljava/lang/Object;)Ljava/lang/String;
    .registers 5
    .parameter "item"

    .prologue
    .line 488
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCount()I

    move-result v0

    .line 489
    .local v0, count:I
    const/4 v1, 0x0

    .local v1, position:I
    :goto_5
    if-ge v1, v0, :cond_19

    .line 490
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 491
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getLabel(I)Ljava/lang/String;

    move-result-object v2

    .line 494
    :goto_15
    return-object v2

    .line 489
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 494
    :cond_19
    const/4 v2, 0x0

    goto :goto_15
.end method

.method private getAccount()Lcom/google/android/accounts/Account;
    .registers 5

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursorExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 180
    .local v0, extras:Landroid/os/Bundle;
    const-string v3, "com.google.reader.cursor.extra.ACCOUNT_NAME"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 181
    .local v1, name:Ljava/lang/String;
    const-string v3, "com.google.reader.cursor.extra.ACCOUNT_TYPE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 182
    .local v2, type:Ljava/lang/String;
    if-eqz v1, :cond_1a

    if-eqz v2, :cond_1a

    new-instance v3, Lcom/google/android/accounts/Account;

    invoke-direct {v3, v1, v2}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_19
    return-object v3

    :cond_1a
    const/4 v3, 0x0

    goto :goto_19
.end method

.method private getCursorExtras()Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 171
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_b

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    :goto_a
    return-object v1

    :cond_b
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_a
.end method

.method private getKey(I)Ljava/lang/String;
    .registers 5
    .parameter "position"

    .prologue
    const/4 v1, 0x0

    .line 380
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 387
    :cond_7
    :goto_7
    return-object v1

    .line 383
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v2, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 384
    .local v0, keys:[Ljava/lang/String;
    :goto_e
    if-ltz p1, :cond_7

    array-length v2, v0

    if-ge p1, v2, :cond_7

    .line 385
    aget-object v1, v0, p1

    goto :goto_7

    .line 383
    .end local v0           #keys:[Ljava/lang/String;
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    goto :goto_e
.end method

.method private getLanguage()Ljava/lang/String;
    .registers 5

    .prologue
    .line 466
    iget-object v3, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 467
    .local v2, resources:Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 468
    .local v0, configuration:Landroid/content/res/Configuration;
    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 469
    .local v1, language:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 472
    .end local v1           #language:Ljava/lang/String;
    :goto_16
    return-object v1

    .restart local v1       #language:Ljava/lang/String;
    :cond_17
    const-string v1, "en"

    goto :goto_16
.end method

.method private getLayout(I)I
    .registers 5
    .parameter "position"

    .prologue
    const/4 v1, 0x0

    .line 407
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 414
    :cond_7
    :goto_7
    return v1

    .line 410
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v2, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLayoutsExpanded:[I

    .line 411
    .local v0, layouts:[I
    :goto_e
    if-ltz p1, :cond_7

    array-length v2, v0

    if-ge p1, v2, :cond_7

    .line 412
    aget v1, v0, p1

    goto :goto_7

    .line 410
    .end local v0           #layouts:[I
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLayoutsCollapsed:[I

    goto :goto_e
.end method

.method private static getResourceIdArray(Landroid/content/res/Resources;I)[I
    .registers 7
    .parameter "resources"
    .parameter "resourceId"

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 90
    .local v3, typedArray:Landroid/content/res/TypedArray;
    :try_start_4
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->length()I

    move-result v1

    .line 91
    .local v1, length:I
    new-array v2, v1, [I

    .line 92
    .local v2, resourceIds:[I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_b
    if-ge v0, v1, :cond_17

    .line 93
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    aput v4, v2, v0
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_1b

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 97
    :cond_17
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    return-object v2

    .end local v0           #i:I
    .end local v1           #length:I
    .end local v2           #resourceIds:[I
    :catchall_1b
    move-exception v4

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v4
.end method

.method private getUri()Landroid/net/Uri;
    .registers 3

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursorExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.reader.cursor.extra.URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method private hasAccount()Z
    .registers 3

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 187
    .local v0, account:Lcom/google/android/accounts/Account;
    if-eqz v0, :cond_8

    const/4 v1, 0x1

    :goto_7
    return v1

    :cond_8
    const/4 v1, 0x0

    goto :goto_7
.end method

.method private hasError()Z
    .registers 3

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursorExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.feeds.cursor.extra.ERROR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isFiltered()Z
    .registers 3

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getUri()Landroid/net/Uri;

    move-result-object v0

    .line 196
    .local v0, uri:Landroid/net/Uri;
    if-eqz v0, :cond_10

    const-string v1, "filtered"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method private newErrorView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .parameter "inflater"
    .parameter "parent"

    .prologue
    .line 341
    const v1, 0x7f030010

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 342
    .local v0, view:Landroid/view/View;
    const v1, 0x7f0b002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    return-object v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method public createLoader(Lcom/google/android/accounts/Account;ZZLjava/lang/CharSequence;)Landroid/support/v4/content/Loader;
    .registers 16
    .parameter "account"
    .parameter "sortSubscriptionsAlpha"
    .parameter "showAllSubscriptions"
    .parameter "constraint"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/accounts/Account;",
            "ZZ",
            "Ljava/lang/CharSequence;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;->ROOT:Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;

    invoke-static {p1, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->contentUri(Lcom/google/android/accounts/Account;Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;)Landroid/net/Uri;

    move-result-object v2

    .line 139
    .local v2, uri:Landroid/net/Uri;
    sget-object v3, Lcom/google/android/apps/reader/widget/HomeAdapter;->PROJECTION:[Ljava/lang/String;

    .line 140
    .local v3, projection:[Ljava/lang/String;
    const-string v4, "id LIKE \'feed/%\' OR id LIKE \'webfeed/%\' OR id LIKE \'%/label/%\'"

    .line 141
    .local v4, selection:Ljava/lang/String;
    sget-object v5, Lcom/google/android/apps/reader/widget/HomeAdapter;->SELECTION_ARGS:[Ljava/lang/String;

    .line 142
    .local v5, selectionArgs:[Ljava/lang/String;
    if-eqz p2, :cond_75

    const-string v6, "sort_key_alpha"

    .line 143
    .local v6, orderBy:Ljava/lang/String;
    :goto_10
    if-nez p3, :cond_37

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "unread_count"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 146
    :cond_37
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6d

    .line 147
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "filtered"

    const-string v10, "true"

    invoke-virtual {v0, v1, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 159
    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 160
    .local v7, escapedConstraint:Ljava/lang/String;
    const-string v8, "(^1) AND (^2 LIKE (^3 || \'%\') OR ^2 LIKE (\'% \' || ^3 || \'%\'))"

    .line 161
    .local v8, template:Ljava/lang/String;
    const/4 v0, 0x3

    new-array v9, v0, [Ljava/lang/CharSequence;

    const/4 v0, 0x0

    aput-object v4, v9, v0

    const/4 v0, 0x1

    const-string v1, "label"

    aput-object v1, v9, v0

    const/4 v0, 0x2

    aput-object v7, v9, v0

    .line 164
    .local v9, values:[Ljava/lang/CharSequence;
    invoke-static {v8, v9}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 166
    .end local v7           #escapedConstraint:Ljava/lang/String;
    .end local v8           #template:Ljava/lang/String;
    .end local v9           #values:[Ljava/lang/CharSequence;
    :cond_6d
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mContext:Landroid/content/Context;

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 142
    .end local v6           #orderBy:Ljava/lang/String;
    :cond_75
    const-string v6, "sort_key_manual"

    goto :goto_10
.end method

.method public getCount()I
    .registers 4

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 202
    invoke-super {p0}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->getCount()I

    move-result v0

    .line 214
    :cond_a
    :goto_a
    return v0

    .line 204
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mDataValid:Z

    if-eqz v2, :cond_24

    .line 205
    iget-boolean v2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v2, :cond_21

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 206
    .local v1, keys:[Ljava/lang/String;
    :goto_15
    array-length v0, v1

    .line 207
    .local v0, count:I
    invoke-super {p0}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->getCount()I

    move-result v2

    add-int/2addr v0, v2

    .line 208
    array-length v2, v1

    if-ne v0, v2, :cond_a

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 205
    .end local v0           #count:I
    .end local v1           #keys:[Ljava/lang/String;
    :cond_21
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    goto :goto_15

    .line 214
    :cond_24
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 5
    .parameter "position"

    .prologue
    .line 348
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 349
    invoke-super {p0, p1}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 360
    :cond_a
    :goto_a
    return-object v0

    .line 351
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v2, :cond_19

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 352
    .local v1, keys:[Ljava/lang/String;
    :goto_11
    array-length v2, v1

    if-ge p1, v2, :cond_1c

    .line 353
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getKey(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a

    .line 351
    .end local v1           #keys:[Ljava/lang/String;
    :cond_19
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    goto :goto_11

    .line 355
    .restart local v1       #keys:[Ljava/lang/String;
    :cond_1c
    array-length v2, v1

    sub-int/2addr p1, v2

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 357
    .local v0, cursor:Landroid/database/Cursor;
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_a

    .line 360
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getItemId(I)J
    .registers 5
    .parameter "position"

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 368
    invoke-super {p0, p1}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->getItemId(I)J

    move-result-wide v1

    .line 375
    :goto_a
    return-wide v1

    .line 370
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v1, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 371
    .local v0, keys:[Ljava/lang/String;
    :goto_11
    array-length v1, v0

    if-ge p1, v1, :cond_23

    .line 372
    aget-object v1, v0, p1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-long v1, v1

    goto :goto_a

    .line 370
    .end local v0           #keys:[Ljava/lang/String;
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    goto :goto_11

    .line 374
    .restart local v0       #keys:[Ljava/lang/String;
    :cond_23
    array-length v1, v0

    sub-int/2addr p1, v1

    .line 375
    invoke-super {p0, p1}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->getItemId(I)J

    move-result-wide v1

    goto :goto_a
.end method

.method public getItemViewType(I)I
    .registers 6
    .parameter "position"

    .prologue
    const/4 v2, 0x1

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 243
    :cond_7
    :goto_7
    return v2

    .line 228
    :cond_8
    iget-boolean v3, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v3, :cond_1f

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 229
    .local v1, keys:[Ljava/lang/String;
    :goto_e
    array-length v3, v1

    if-ge p1, v3, :cond_28

    .line 230
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isStream(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 232
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isHeader(I)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 233
    const/4 v2, 0x0

    goto :goto_7

    .line 228
    .end local v1           #keys:[Ljava/lang/String;
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    goto :goto_e

    .line 235
    .restart local v1       #keys:[Ljava/lang/String;
    :cond_22
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 238
    :cond_28
    array-length v3, v1

    sub-int/2addr p1, v3

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 240
    .local v0, cursor:Landroid/database/Cursor;
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 243
    const/4 v2, 0x2

    goto :goto_7
.end method

.method public getLabel(I)Ljava/lang/String;
    .registers 5
    .parameter "position"

    .prologue
    const/4 v1, 0x0

    .line 395
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 402
    :cond_7
    :goto_7
    return-object v1

    .line 398
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v2, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLabelsExpanded:[Ljava/lang/String;

    .line 399
    .local v0, labels:[Ljava/lang/String;
    :goto_e
    if-ltz p1, :cond_7

    array-length v2, v0

    if-ge p1, v2, :cond_7

    .line 400
    aget-object v1, v0, p1

    goto :goto_7

    .line 398
    .end local v0           #labels:[Ljava/lang/String;
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mLabelsCollapsed:[Ljava/lang/String;

    goto :goto_e
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 21
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 280
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 281
    invoke-super/range {p0 .. p3}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 328
    :cond_a
    :goto_a
    return-object v3

    .line 283
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v2, :cond_5a

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 284
    .local v11, keys:[Ljava/lang/String;
    :goto_15
    array-length v2, v11

    move/from16 v0, p1

    if-ge v0, v2, :cond_97

    .line 285
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getLayout(I)I

    move-result v12

    .line 286
    .local v12, layout:I
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getLabel(I)Ljava/lang/String;

    move-result-object v5

    .line 287
    .local v5, label:Ljava/lang/String;
    move-object/from16 v3, p2

    .line 288
    .local v3, view:Landroid/view/View;
    if-nez v3, :cond_33

    .line 289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    .line 290
    .local v10, inflater:Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    invoke-virtual {v10, v12, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 292
    .end local v10           #inflater:Landroid/view/LayoutInflater;
    :cond_33
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isStream(I)Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 293
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getKey(I)Ljava/lang/String;

    move-result-object v4

    .line 294
    .local v4, streamId:Ljava/lang/String;
    const/4 v14, -0x1

    .line 295
    .local v14, subscriptionCount:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mUnreadCounts:Lcom/google/android/apps/reader/widget/UnreadCountsQuery;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/reader/widget/UnreadCountsQuery;->getUnreadCount(Ljava/lang/String;)I

    move-result v6

    .line 296
    .local v6, unreadCount:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mUnreadCounts:Lcom/google/android/apps/reader/widget/UnreadCountsQuery;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/reader/widget/UnreadCountsQuery;->getMaxUnreadCount(Ljava/lang/String;)I

    move-result v7

    .line 297
    .local v7, maxUnreadCount:I
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2, v14}, Lcom/google/android/apps/reader/widget/HomeAdapter;->bindImageView(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v2, p0

    .line 298
    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/reader/widget/HomeAdapter;->bindTextViews(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_a

    .line 283
    .end local v3           #view:Landroid/view/View;
    .end local v4           #streamId:Ljava/lang/String;
    .end local v5           #label:Ljava/lang/String;
    .end local v6           #unreadCount:I
    .end local v7           #maxUnreadCount:I
    .end local v11           #keys:[Ljava/lang/String;
    .end local v12           #layout:I
    .end local v14           #subscriptionCount:I
    :cond_5a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    goto :goto_15

    .line 299
    .restart local v3       #view:Landroid/view/View;
    .restart local v5       #label:Ljava/lang/String;
    .restart local v11       #keys:[Ljava/lang/String;
    .restart local v12       #layout:I
    :cond_5f
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isHeader(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 300
    const v2, 0x1020014

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 301
    .local v15, text1:Landroid/widget/TextView;
    const v2, 0x1020006

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 302
    .local v9, icon:Landroid/widget/ImageView;
    invoke-virtual {v15, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    if-nez p1, :cond_91

    .line 304
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v2, :cond_8e

    sget-object v13, Lcom/google/android/apps/reader/widget/HomeAdapter;->GROUP_EXPANDED_STATE_SET:[I

    .line 305
    .local v13, state:[I
    :goto_84
    const/4 v2, 0x1

    invoke-virtual {v9, v13, v2}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 306
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    .line 304
    .end local v13           #state:[I
    :cond_8e
    sget-object v13, Lcom/google/android/apps/reader/widget/HomeAdapter;->GROUP_COLLAPSED_STATE_SET:[I

    goto :goto_84

    .line 308
    :cond_91
    const/4 v2, 0x4

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    .line 313
    .end local v3           #view:Landroid/view/View;
    .end local v5           #label:Ljava/lang/String;
    .end local v9           #icon:Landroid/widget/ImageView;
    .end local v12           #layout:I
    .end local v15           #text1:Landroid/widget/TextView;
    :cond_97
    array-length v2, v11

    sub-int p1, p1, v2

    .line 314
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 315
    .local v8, cursor:Landroid/database/Cursor;
    move/from16 v0, p1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_ac

    .line 316
    invoke-super/range {p0 .. p3}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_a

    .line 317
    :cond_ac
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->hasError()Z

    move-result v2

    if-eqz v2, :cond_c4

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    .line 319
    .restart local v10       #inflater:Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v10, v1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->newErrorView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_a

    .end local v10           #inflater:Landroid/view/LayoutInflater;
    :cond_c4
    move-object/from16 v3, p2

    .line 321
    check-cast v3, Landroid/widget/TextView;

    .line 322
    .local v3, view:Landroid/widget/TextView;
    if-nez v3, :cond_e1

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    .line 324
    .restart local v10       #inflater:Landroid/view/LayoutInflater;
    const v2, 0x7f030032

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v10, v2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .end local v3           #view:Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 326
    .end local v10           #inflater:Landroid/view/LayoutInflater;
    .restart local v3       #view:Landroid/widget/TextView;
    :cond_e1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mShowAllSubscriptions:Z

    if-eqz v2, :cond_f5

    const v2, 0x7f0d0051

    :goto_ea
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v3, v2, v0}, Lcom/google/android/apps/reader/widget/ReaderLinks;->linkify(Landroid/widget/TextView;ILcom/google/android/accounts/Account;)V

    goto/16 :goto_a

    :cond_f5
    const v2, 0x7f0d0055

    goto :goto_ea
.end method

.method public getViewTypeCount()I
    .registers 2

    .prologue
    .line 220
    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .registers 6
    .parameter "position"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 255
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFiltered()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 273
    :cond_8
    :goto_8
    return v1

    .line 258
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    if-eqz v3, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysExpanded:[Ljava/lang/String;

    .line 259
    .local v0, keys:[Ljava/lang/String;
    :goto_f
    array-length v3, v0

    if-ge p1, v3, :cond_2d

    .line 260
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isHeader(I)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 261
    if-eqz p1, :cond_8

    move v1, v2

    goto :goto_8

    .line 258
    .end local v0           #keys:[Ljava/lang/String;
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mKeysCollapsed:[Ljava/lang/String;

    goto :goto_f

    .line 262
    .restart local v0       #keys:[Ljava/lang/String;
    :cond_1f
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isStream(I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 265
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected item type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_2d
    invoke-super {p0}, Lcom/google/android/apps/reader/widget/StreamListAdapter;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    move v1, v2

    .line 270
    goto :goto_8
.end method

.method public isExpanded()Z
    .registers 2

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    return v0
.end method

.method public isHeader(I)Z
    .registers 4
    .parameter "position"

    .prologue
    .line 428
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getLayout(I)I

    move-result v0

    .line 429
    .local v0, layout:I
    const v1, 0x7f030033

    if-ne v0, v1, :cond_b

    const/4 v1, 0x1

    :goto_a
    return v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public isStream(I)Z
    .registers 4
    .parameter "position"

    .prologue
    .line 423
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getLayout(I)I

    move-result v0

    .line 424
    .local v0, layout:I
    const v1, 0x7f030034

    if-ne v0, v1, :cond_b

    const/4 v1, 0x1

    :goto_a
    return v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public newViewIntent(Ljava/lang/Object;)Landroid/content/Intent;
    .registers 16
    .parameter "item"

    .prologue
    const/4 v13, 0x1

    const/4 v11, 0x0

    .line 499
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 500
    .local v1, cursor:Landroid/database/Cursor;
    instance-of v12, p1, Ljava/lang/String;

    if-eqz v12, :cond_79

    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->hasAccount()Z

    move-result v12

    if-eqz v12, :cond_79

    .line 501
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .local v0, account:Lcom/google/android/accounts/Account;
    move-object v7, p1

    .line 502
    check-cast v7, Ljava/lang/String;

    .line 503
    .local v7, streamId:Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->findLabel(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 504
    .local v5, label:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/apps/reader/provider/ReaderStream;->isFollowing(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_43

    .line 505
    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Friends;->followingUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    .line 506
    .local v2, friendsUri:Landroid/net/Uri;
    const-string v12, "user/-/state/com.google/broadcast-friends"

    invoke-virtual {p0, v0, v12}, Lcom/google/android/apps/reader/widget/HomeAdapter;->createItemsUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 507
    .local v8, streamUri:Landroid/net/Uri;
    if-eqz v8, :cond_41

    .line 508
    new-instance v3, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    invoke-direct {v3, v11, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 509
    .local v3, intent:Landroid/content/Intent;
    const-string v11, "android.intent.extra.STREAM"

    invoke-virtual {v3, v11, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 510
    if-eqz v5, :cond_40

    .line 511
    const-string v11, "android.intent.extra.TITLE"

    invoke-virtual {v3, v11, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 571
    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v2           #friendsUri:Landroid/net/Uri;
    .end local v3           #intent:Landroid/content/Intent;
    .end local v5           #label:Ljava/lang/String;
    .end local v7           #streamId:Ljava/lang/String;
    .end local v8           #streamUri:Landroid/net/Uri;
    :cond_40
    :goto_40
    return-object v3

    .restart local v0       #account:Lcom/google/android/accounts/Account;
    .restart local v2       #friendsUri:Landroid/net/Uri;
    .restart local v5       #label:Ljava/lang/String;
    .restart local v7       #streamId:Ljava/lang/String;
    .restart local v8       #streamUri:Landroid/net/Uri;
    :cond_41
    move-object v3, v11

    .line 515
    goto :goto_40

    .line 517
    .end local v2           #friendsUri:Landroid/net/Uri;
    .end local v8           #streamUri:Landroid/net/Uri;
    :cond_43
    invoke-static {v7}, Lcom/google/android/apps/reader/provider/ReaderStream;->isRecommendation(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_62

    .line 518
    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Recommendations;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v6

    .line 519
    .local v6, recommendationsUri:Landroid/net/Uri;
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->createExploreStreamId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v0, v11}, Lcom/google/android/apps/reader/widget/HomeAdapter;->createItemsUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 520
    .restart local v8       #streamUri:Landroid/net/Uri;
    new-instance v3, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    invoke-direct {v3, v11, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 521
    .restart local v3       #intent:Landroid/content/Intent;
    const-string v11, "android.intent.extra.STREAM"

    invoke-virtual {v3, v11, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_40

    .line 524
    .end local v3           #intent:Landroid/content/Intent;
    .end local v6           #recommendationsUri:Landroid/net/Uri;
    .end local v8           #streamUri:Landroid/net/Uri;
    :cond_62
    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/reader/widget/HomeAdapter;->createItemsUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 525
    .local v4, itemsUri:Landroid/net/Uri;
    if-eqz v4, :cond_77

    .line 526
    new-instance v3, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    invoke-direct {v3, v11, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 527
    .restart local v3       #intent:Landroid/content/Intent;
    if-eqz v5, :cond_40

    .line 528
    const-string v11, "android.intent.extra.TITLE"

    invoke-virtual {v3, v11, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_40

    .end local v3           #intent:Landroid/content/Intent;
    :cond_77
    move-object v3, v11

    .line 532
    goto :goto_40

    .line 535
    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v4           #itemsUri:Landroid/net/Uri;
    .end local v5           #label:Ljava/lang/String;
    .end local v7           #streamId:Ljava/lang/String;
    :cond_79
    if-eqz p1, :cond_ee

    if-ne p1, v1, :cond_ee

    .line 536
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->getAccount(Landroid/database/Cursor;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 537
    .restart local v0       #account:Lcom/google/android/accounts/Account;
    invoke-interface {v1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 538
    .restart local v7       #streamId:Ljava/lang/String;
    const/4 v12, 0x2

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 540
    .local v10, title:Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v12, "android.intent.action.VIEW"

    invoke-direct {v3, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 541
    .restart local v3       #intent:Landroid/content/Intent;
    invoke-static {v7}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isSubscriptionOrWebFeed(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_aa

    .line 543
    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/reader/widget/HomeAdapter;->createItemsUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 544
    .restart local v4       #itemsUri:Landroid/net/Uri;
    if-nez v4, :cond_9f

    move-object v3, v11

    .line 545
    goto :goto_40

    .line 547
    :cond_9f
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 566
    .end local v4           #itemsUri:Landroid/net/Uri;
    :goto_a2
    if-eqz v10, :cond_40

    .line 567
    const-string v11, "android.intent.extra.TITLE"

    invoke-virtual {v3, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_40

    .line 548
    :cond_aa
    invoke-static {v7}, Lcom/google/android/apps/reader/provider/ReaderStream;->isLabel(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_d5

    .line 549
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isFolder(Landroid/database/Cursor;)Z

    move-result v12

    if-eqz v12, :cond_c8

    .line 551
    invoke-static {v0, v7}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 552
    .local v9, tagUri:Landroid/net/Uri;
    invoke-virtual {v3, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 553
    const-string v11, "android.intent.extra.TITLE"

    invoke-virtual {v3, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 554
    const-string v11, "folder"

    invoke-virtual {v3, v11, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_a2

    .line 557
    .end local v9           #tagUri:Landroid/net/Uri;
    :cond_c8
    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/reader/widget/HomeAdapter;->createItemsUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 558
    .restart local v4       #itemsUri:Landroid/net/Uri;
    if-nez v4, :cond_d1

    move-object v3, v11

    .line 559
    goto/16 :goto_40

    .line 561
    :cond_d1
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_a2

    .line 564
    .end local v4           #itemsUri:Landroid/net/Uri;
    :cond_d5
    new-instance v11, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unexpected stream ID: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11

    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v3           #intent:Landroid/content/Intent;
    .end local v7           #streamId:Ljava/lang/String;
    .end local v10           #title:Ljava/lang/String;
    :cond_ee
    move-object v3, v11

    .line 571
    goto/16 :goto_40
.end method

.method public setExpanded(Z)V
    .registers 4
    .parameter "expanded"

    .prologue
    .line 450
    iput-boolean p1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mExpanded:Z

    .line 452
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/reader/preference/LocalPreferences;->edit(Landroid/content/Context;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 453
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v1, "home_expanded"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 454
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 456
    iget-boolean v1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mDataValid:Z

    if-eqz v1, :cond_17

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->notifyDataSetChanged()V

    .line 459
    :cond_17
    return-void
.end method

.method public setShowAllSubscriptions(Z)V
    .registers 3
    .parameter "showAllSubscriptions"

    .prologue
    .line 334
    iput-boolean p1, p0, Lcom/google/android/apps/reader/widget/HomeAdapter;->mShowAllSubscriptions:Z

    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->notifyDataSetChanged()V

    .line 338
    :cond_b
    return-void
.end method

.method public toggleExpanded()V
    .registers 2

    .prologue
    .line 462
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_7
    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/widget/HomeAdapter;->setExpanded(Z)V

    .line 463
    return-void

    .line 462
    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method
