.class public Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "TagAutoCompleteAdapter.java"


# static fields
.field private static final FROM:[Ljava/lang/String; = null

.field private static final LAYOUT:I = 0x109000a

.field private static final TO:[I


# instance fields
.field private mAccount:Lcom/google/android/accounts/Account;

.field private final mActivity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "label"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->FROM:[Ljava/lang/String;

    .line 40
    new-array v0, v3, [I

    const v1, 0x1020014

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->TO:[I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .registers 9
    .parameter "activity"

    .prologue
    .line 49
    const v2, 0x109000a

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->FROM:[Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->TO:[I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->mActivity:Landroid/app/Activity;

    .line 51
    return-void
.end method

.method private managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->mActivity:Landroid/app/Activity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public changeAccount(Lcom/google/android/accounts/Account;)V
    .registers 2
    .parameter "account"

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->mAccount:Lcom/google/android/accounts/Account;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->notifyDataSetChanged()V

    .line 56
    return-void
.end method

.method public convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .registers 4
    .parameter "cursor"

    .prologue
    .line 60
    if-eqz p1, :cond_d

    .line 61
    const-string v1, "label"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 62
    .local v0, columnIndex:I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 64
    .end local v0           #columnIndex:I
    :goto_c
    return-object v1

    :cond_d
    const-string v1, ""

    goto :goto_c
.end method

.method public runQueryOnBackgroundThread(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .registers 12
    .parameter "constraint"

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 70
    iget-object v6, p0, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->mAccount:Lcom/google/android/accounts/Account;

    if-nez v6, :cond_9

    .line 87
    :cond_8
    :goto_8
    return-object v0

    .line 73
    :cond_9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 79
    .local v1, uri:Landroid/net/Uri;
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v7

    const-string v0, "label"

    aput-object v0, v2, v8

    .line 82
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "label LIKE ? AND id LIKE ?"

    .line 83
    .local v3, selection:Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "%"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v0, "%/label/%"

    aput-object v0, v4, v8

    .line 86
    .local v4, selectionArgs:[Ljava/lang/String;
    const-string v5, "label COLLATE NOCASE ASC"

    .local v5, sortOrder:Ljava/lang/String;
    move-object v0, p0

    .line 87
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/reader/widget/TagAutoCompleteAdapter;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_8
.end method
