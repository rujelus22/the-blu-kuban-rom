.class public final Lcom/google/android/apps/reader/widget/ItemViewFactory;
.super Ljava/lang/Object;
.source "ItemViewFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/widget/ItemViewFactory$1;,
        Lcom/google/android/apps/reader/widget/ItemViewFactory$ItemViewClient;,
        Lcom/google/android/apps/reader/widget/ItemViewFactory$ItemChromeClient;,
        Lcom/google/android/apps/reader/widget/ItemViewFactory$DataProxy;,
        Lcom/google/android/apps/reader/widget/ItemViewFactory$SettingsProxy;,
        Lcom/google/android/apps/reader/widget/ItemViewFactory$OnProgressChangeListener;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final API_LEVEL:I = 0x0

.field private static final HONEYCOMB:I = 0xb

.field private static final TAG:Ljava/lang/String; = "ItemView"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 52
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/reader/widget/ItemViewFactory;->API_LEVEL:I

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method private static enableSmoothTransition(Landroid/webkit/WebSettings;)V
    .registers 8
    .parameter "webSettings"

    .prologue
    .line 159
    sget v2, Lcom/google/android/apps/reader/widget/ItemViewFactory;->API_LEVEL:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_21

    .line 161
    :try_start_6
    const-class v2, Landroid/webkit/WebSettings;

    const-string v3, "setEnableSmoothTransition"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 162
    .local v0, m:Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object v4, v2, v3

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_21} :catch_22

    .line 167
    .end local v0           #m:Ljava/lang/reflect/Method;
    :cond_21
    :goto_21
    return-void

    .line 163
    :catch_22
    move-exception v1

    .line 164
    .local v1, t:Ljava/lang/Throwable;
    const-string v2, "ItemView"

    const-string v3, "enableSmoothTransition failed"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21
.end method

.method public static final getPluginsSupported()Z
    .registers 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public static newView(Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 15
    .parameter "parent"

    .prologue
    const/4 v13, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 80
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 81
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 83
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v12, 0x7f03001a

    invoke-virtual {v4, v12, p0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 84
    .local v7, view:Landroid/view/View;
    const v12, 0x7f0b003b

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/webkit/WebView;

    .line 86
    .local v9, webView:Landroid/webkit/WebView;
    new-instance v12, Lcom/google/android/apps/reader/widget/ItemViewFactory$ItemChromeClient;

    invoke-direct {v12, v13}, Lcom/google/android/apps/reader/widget/ItemViewFactory$ItemChromeClient;-><init>(Lcom/google/android/apps/reader/widget/ItemViewFactory$1;)V

    invoke-virtual {v9, v12}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 87
    new-instance v12, Lcom/google/android/apps/reader/widget/ItemViewFactory$ItemViewClient;

    invoke-direct {v12, v13}, Lcom/google/android/apps/reader/widget/ItemViewFactory$ItemViewClient;-><init>(Lcom/google/android/apps/reader/widget/ItemViewFactory$1;)V

    invoke-virtual {v9, v12}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 89
    new-instance v1, Lcom/google/android/apps/reader/widget/ItemContextMenu;

    invoke-direct {v1}, Lcom/google/android/apps/reader/widget/ItemContextMenu;-><init>()V

    .line 90
    .local v1, contextMenu:Lcom/google/android/apps/reader/widget/ItemContextMenu;
    invoke-virtual {v9, v1}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 91
    invoke-virtual {v9, v1}, Landroid/webkit/WebView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 93
    const/high16 v12, 0x200

    invoke-virtual {v9, v12}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    .line 94
    invoke-virtual {v9, v10}, Landroid/webkit/WebView;->setHorizontalScrollbarOverlay(Z)V

    .line 95
    invoke-virtual {v9, v10}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 102
    sget v12, Lcom/google/android/apps/reader/widget/ItemViewFactory;->API_LEVEL:I

    const/16 v13, 0xb

    if-lt v12, v13, :cond_b3

    move v3, v10

    .line 106
    .local v3, hardwareAccelerated:Z
    :goto_48
    invoke-virtual {v9}, Landroid/webkit/WebView;->getPaddingLeft()I

    move-result v12

    if-gtz v12, :cond_60

    invoke-virtual {v9}, Landroid/webkit/WebView;->getPaddingTop()I

    move-result v12

    if-gtz v12, :cond_60

    invoke-virtual {v9}, Landroid/webkit/WebView;->getPaddingRight()I

    move-result v12

    if-gtz v12, :cond_60

    invoke-virtual {v9}, Landroid/webkit/WebView;->getPaddingBottom()I

    move-result v12

    if-lez v12, :cond_b5

    :cond_60
    move v6, v10

    .line 109
    .local v6, padded:Z
    :goto_61
    if-nez v6, :cond_b7

    if-nez v3, :cond_b7

    move v2, v10

    .line 110
    .local v2, fadingEdge:Z
    :goto_66
    invoke-virtual {v9, v2}, Landroid/webkit/WebView;->setVerticalFadingEdgeEnabled(Z)V

    .line 111
    invoke-virtual {v9, v2}, Landroid/webkit/WebView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 113
    invoke-virtual {v9}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v8

    .line 116
    .local v8, webSettings:Landroid/webkit/WebSettings;
    invoke-virtual {v8, v11}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    .line 125
    invoke-virtual {v8, v10}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 127
    invoke-static {}, Lcom/google/android/apps/reader/widget/ItemViewFactory;->getPluginsSupported()Z

    move-result v11

    if-eqz v11, :cond_85

    invoke-static {v0}, Lcom/google/android/apps/reader/preference/LocalPreferences;->getPluginsEnabled(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_85

    .line 128
    invoke-virtual {v8, v10}, Landroid/webkit/WebSettings;->setPluginsEnabled(Z)V

    .line 134
    :cond_85
    invoke-static {v0}, Lcom/google/android/apps/reader/preference/LocalPreferences;->getTextSize(Landroid/content/Context;)Landroid/webkit/WebSettings$TextSize;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    .line 136
    invoke-virtual {v8, v10}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 138
    invoke-static {v8}, Lcom/google/android/apps/reader/widget/ItemViewFactory;->enableSmoothTransition(Landroid/webkit/WebSettings;)V

    .line 140
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v10}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    .line 143
    .local v5, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v10, Lcom/google/android/apps/reader/widget/ItemViewFactory$SettingsProxy;

    invoke-direct {v10, v8}, Lcom/google/android/apps/reader/widget/ItemViewFactory$SettingsProxy;-><init>(Landroid/webkit/WebSettings;)V

    const-string v11, "settings"

    invoke-virtual {v9, v10, v11}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    new-instance v10, Lcom/google/android/apps/reader/widget/ItemViewFactory$DataProxy;

    invoke-direct {v10, v5}, Lcom/google/android/apps/reader/widget/ItemViewFactory$DataProxy;-><init>(Ljava/util/Map;)V

    const-string v11, "data"

    invoke-virtual {v9, v10, v11}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {v9, v5}, Landroid/webkit/WebView;->setTag(Ljava/lang/Object;)V

    .line 151
    return-object v7

    .end local v2           #fadingEdge:Z
    .end local v3           #hardwareAccelerated:Z
    .end local v5           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6           #padded:Z
    .end local v8           #webSettings:Landroid/webkit/WebSettings;
    :cond_b3
    move v3, v11

    .line 102
    goto :goto_48

    .restart local v3       #hardwareAccelerated:Z
    :cond_b5
    move v6, v11

    .line 106
    goto :goto_61

    .restart local v6       #padded:Z
    :cond_b7
    move v2, v11

    .line 109
    goto :goto_66
.end method
