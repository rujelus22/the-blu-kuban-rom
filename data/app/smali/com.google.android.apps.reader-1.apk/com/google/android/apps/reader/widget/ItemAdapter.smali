.class public final Lcom/google/android/apps/reader/widget/ItemAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "ItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;
    }
.end annotation


# instance fields
.field private mCurrentPrimaryItem:Lcom/google/android/apps/reader/widget/ItemViewFragment;

.field private final mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

.field private mLastPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V
    .registers 4
    .parameter "context"
    .parameter "fragmentManager"

    .prologue
    .line 58
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 59
    new-instance v0, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;-><init>(Lcom/google/android/apps/reader/widget/ItemAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    .line 60
    return-void
.end method

.method private static cursorRowToContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .registers 2
    .parameter "cursor"

    .prologue
    .line 224
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 225
    .local v0, values:Landroid/content/ContentValues;
    invoke-static {p0, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 226
    return-object v0
.end method

.method private getCursorExtras()Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 101
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 102
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_d

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    :goto_c
    return-object v1

    :cond_d
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_c
.end method


# virtual methods
.method public createLoader(Landroid/net/Uri;)Landroid/support/v4/content/Loader;
    .registers 3
    .parameter "uri"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->createLoader(Landroid/net/Uri;)Landroid/support/v4/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method

.method public findItem(J)Landroid/database/Cursor;
    .registers 6
    .parameter "itemId"

    .prologue
    .line 207
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    iget v2, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mLastPosition:I

    invoke-virtual {v1, p1, p2, v2}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->findItem(JI)Landroid/database/Cursor;

    move-result-object v0

    .line 208
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_10

    .line 210
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mLastPosition:I

    .line 212
    :cond_10
    return-object v0
.end method

.method public findItemPosition(J)I
    .registers 5
    .parameter "itemId"

    .prologue
    .line 216
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 217
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_b

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    :goto_a
    return v1

    :cond_b
    const/4 v1, -0x2

    goto :goto_a
.end method

.method public getAccount()Lcom/google/android/accounts/Account;
    .registers 5

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getCursorExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 107
    .local v0, extras:Landroid/os/Bundle;
    const-string v3, "com.google.reader.cursor.extra.ACCOUNT_NAME"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, name:Ljava/lang/String;
    const-string v3, "com.google.reader.cursor.extra.ACCOUNT_TYPE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, type:Ljava/lang/String;
    if-eqz v1, :cond_1a

    if-eqz v2, :cond_1a

    new-instance v3, Lcom/google/android/accounts/Account;

    invoke-direct {v3, v1, v2}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_19
    return-object v3

    :cond_1a
    const/4 v3, 0x0

    goto :goto_19
.end method

.method public getCount()I
    .registers 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->getCount()I

    move-result v0

    return v0
.end method

.method public getCursor(I)Landroid/database/Cursor;
    .registers 3
    .parameter "position"

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .registers 5
    .parameter "position"

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v0

    .line 65
    .local v0, cursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->cursorRowToContentValues(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v1

    .line 66
    .local v1, values:Landroid/content/ContentValues;
    new-instance v2, Lcom/google/android/apps/reader/widget/ItemViewFragment;

    invoke-direct {v2, v1}, Lcom/google/android/apps/reader/widget/ItemViewFragment;-><init>(Landroid/content/ContentValues;)V

    return-object v2
.end method

.method public getItemId(I)J
    .registers 5
    .parameter "position"

    .prologue
    .line 137
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 138
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_19

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_19

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 139
    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getItemId(Landroid/database/Cursor;)J

    move-result-wide v1

    .line 141
    :goto_18
    return-wide v1

    :cond_19
    const-wide/high16 v1, -0x8000

    goto :goto_18
.end method

.method public getItemId(Landroid/database/Cursor;)J
    .registers 4
    .parameter "cursor"

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->getItemId(Landroid/database/Cursor;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .registers 6
    .parameter "object"

    .prologue
    .line 71
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/reader/widget/ItemViewFragment;

    .line 72
    .local v0, fragment:Lcom/google/android/apps/reader/widget/ItemViewFragment;
    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ItemViewFragment;->getItemId()J

    move-result-wide v1

    .line 73
    .local v1, itemId:J
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItemPosition(J)I

    move-result v3

    return v3
.end method

.method public getProgress()I
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mCurrentPrimaryItem:Lcom/google/android/apps/reader/widget/ItemViewFragment;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mCurrentPrimaryItem:Lcom/google/android/apps/reader/widget/ItemViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ItemViewFragment;->getProgress()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getUri()Landroid/net/Uri;
    .registers 3

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getCursorExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.reader.cursor.extra.URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method public isStarred(Landroid/database/Cursor;)Z
    .registers 3
    .parameter "cursor"

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->isStarred(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public isUnread(Landroid/database/Cursor;)Z
    .registers 3
    .parameter "cursor"

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->isUnread(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public itemUri(Landroid/database/Cursor;)Landroid/net/Uri;
    .registers 3
    .parameter "cursor"

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->itemUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public markRead(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->markRead(Landroid/database/Cursor;)V

    .line 179
    return-void
.end method

.method public notifyDataSetChanged()V
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->notifyDataSetChanged()V

    .line 97
    invoke-super {p0}, Landroid/support/v4/app/FragmentStatePagerAdapter;->notifyDataSetChanged()V

    .line 98
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 147
    return-void
.end method

.method public onKeyDown(Landroid/database/Cursor;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "cursor"
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->onKeyDown(Landroid/database/Cursor;ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;Landroid/database/Cursor;)Z
    .registers 4
    .parameter "item"
    .parameter "cursor"

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->onOptionsItemSelected(Landroid/view/MenuItem;Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;Landroid/database/Cursor;)V
    .registers 4
    .parameter "menu"
    .parameter "cursor"

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Landroid/database/Cursor;)V

    .line 151
    return-void
.end method

.method public registerObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 83
    return-void
.end method

.method public sendItem(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->sendItem(Landroid/database/Cursor;)Z

    .line 159
    return-void
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 5
    .parameter "container"
    .parameter "position"
    .parameter "object"

    .prologue
    .line 191
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentStatePagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mCurrentPrimaryItem:Lcom/google/android/apps/reader/widget/ItemViewFragment;

    if-eq p3, v0, :cond_14

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mCurrentPrimaryItem:Lcom/google/android/apps/reader/widget/ItemViewFragment;

    if-eqz v0, :cond_10

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mCurrentPrimaryItem:Lcom/google/android/apps/reader/widget/ItemViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ItemViewFragment;->scrollToTop()V

    .line 198
    :cond_10
    check-cast p3, Lcom/google/android/apps/reader/widget/ItemViewFragment;

    .end local p3
    iput-object p3, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mCurrentPrimaryItem:Lcom/google/android/apps/reader/widget/ItemViewFragment;

    .line 200
    :cond_14
    return-void
.end method

.method public setSilent(Z)V
    .registers 3
    .parameter "silent"

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->setSilent(Z)V

    .line 175
    return-void
.end method

.method public setStarred(Landroid/database/Cursor;Z)V
    .registers 4
    .parameter "cursor"
    .parameter "starred"

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->setStarred(Landroid/database/Cursor;Z)V

    .line 163
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 91
    invoke-super {p0}, Landroid/support/v4/app/FragmentStatePagerAdapter;->notifyDataSetChanged()V

    .line 92
    return-void
.end method

.method public syncChanges()V
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->syncChanges()Z

    .line 126
    return-void
.end method

.method public unregisterObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemAdapter;->mHelper:Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter$ItemsHelper;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 87
    return-void
.end method
