.class public final Lcom/google/android/apps/reader/fragment/Loadable;
.super Ljava/lang/Object;
.source "Loadable.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final ALMOST_MAX_LOADING_VALUE:J = 0x7ffffffffffffffeL

.field private static final TAG:Ljava/lang/String; = "ReaderLoadable"


# instance fields
.field private final mCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mHasError:Z

.field private mHasMore:Z

.field private final mLoaderId:I

.field private mLoading:Z

.field private final mManager:Landroid/support/v4/app/LoaderManager;

.field private mMaxAgeAfterLoading:J

.field private mMaxAgeBeforeLoading:J

.field private mMinCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;ILandroid/support/v4/app/LoaderManager$LoaderCallbacks;)V
    .registers 7
    .parameter "context"
    .parameter "loaderManager"
    .parameter "loaderId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/support/v4/app/LoaderManager;",
            "I",
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, callbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;,"Landroid/support/v4/app/LoaderManager$LoaderCallbacks<Landroid/database/Cursor;>;"
    const-wide v0, 0x7fffffffffffffffL

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-wide v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    .line 93
    iput-wide v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeAfterLoading:J

    .line 120
    iput-object p1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mContext:Landroid/content/Context;

    .line 121
    iput-object p2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mManager:Landroid/support/v4/app/LoaderManager;

    .line 122
    iput p3, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoaderId:I

    .line 123
    iput-object p4, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 124
    return-void
.end method

.method private autoRefresh(J)V
    .registers 11
    .parameter "date"

    .prologue
    const-wide/16 v6, 0x0

    .line 248
    cmp-long v2, p1, v6

    if-gez v2, :cond_7

    .line 273
    :cond_6
    :goto_6
    return-void

    .line 250
    :cond_7
    sget-object v2, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1a

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeAfterLoading:J

    const-wide v4, 0x7ffffffffffffffeL

    cmp-long v2, v2, v4

    if-gez v2, :cond_6

    .line 253
    :cond_1a
    sget-object v2, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/util/Experiment;->isDisabled()Z

    move-result v2

    if-eqz v2, :cond_2d

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeAfterLoading:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 256
    :cond_2d
    cmp-long v2, p1, v6

    if-nez v2, :cond_3c

    .line 258
    const-string v2, "ReaderLoadable"

    const-string v3, "Refreshing because cached content is invalid"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->refresh()V

    goto :goto_6

    .line 262
    :cond_3c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, p1

    .line 263
    .local v0, age:J
    cmp-long v2, v0, v6

    if-gez v2, :cond_51

    .line 266
    const-string v2, "ReaderLoadable"

    const-string v3, "Refreshing because system clock changed and age is unknown"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->refresh()V

    goto :goto_6

    .line 268
    :cond_51
    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeAfterLoading:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 269
    const-string v2, "ReaderLoadable"

    const-string v3, "Refreshing because cached content is stale"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->refresh()V

    goto :goto_6
.end method

.method private getDefaultMaxAge()J
    .registers 6

    .prologue
    .line 412
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/reader/util/SystemService;->getConnectivityManager(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    .line 413
    .local v0, manager:Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 414
    .local v1, network:Landroid/net/NetworkInfo;
    if-eqz v1, :cond_20

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_20

    .line 415
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    .line 416
    .local v2, type:I
    if-nez v2, :cond_1c

    .line 419
    const-wide/32 v3, 0xdbba00

    .line 429
    .end local v2           #type:I
    :goto_1b
    return-wide v3

    .line 424
    .restart local v2       #type:I
    :cond_1c
    const-wide/32 v3, 0x6ddd00

    goto :goto_1b

    .line 426
    .end local v2           #type:I
    :cond_20
    sget-object v3, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 427
    const-wide v3, 0x7ffffffffffffffeL

    goto :goto_1b

    .line 429
    :cond_2e
    const-wide v3, 0x7fffffffffffffffL

    goto :goto_1b
.end method

.method private startSolution(Landroid/content/Intent;)V
    .registers 5
    .parameter "solution"

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/high16 v2, 0x2000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 230
    return-void
.end method


# virtual methods
.method public destroyLoader()V
    .registers 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoaderId:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 329
    return-void
.end method

.method public destroyLoaderIf(Z)V
    .registers 4
    .parameter "condition"

    .prologue
    .line 335
    if-eqz p1, :cond_9

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoaderId:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 338
    :cond_9
    return-void
.end method

.method public filterable(Landroid/widget/ListAdapter;)Landroid/widget/ListAdapter;
    .registers 5
    .parameter "adapter"

    .prologue
    .line 295
    new-instance v0, Lcom/google/android/apps/reader/fragment/LoaderFilterable;

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoaderId:I

    invoke-direct {v0, p1, v1, v2, p0}, Lcom/google/android/apps/reader/fragment/LoaderFilterable;-><init>(Landroid/widget/ListAdapter;Landroid/support/v4/app/LoaderManager;ILandroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    return-object v0
.end method

.method public initLoader()V
    .registers 4

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoaderId:I

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 277
    return-void
.end method

.method public initLoaderIf(Z)V
    .registers 2
    .parameter "condition"

    .prologue
    .line 372
    if-eqz p1, :cond_6

    .line 373
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->initLoader()V

    .line 377
    :goto_5
    return-void

    .line 375
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->destroyLoader()V

    goto :goto_5
.end method

.method public isReadyToLoadMore()Z
    .registers 2

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasMore:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoading:Z

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public loadMore(I)Z
    .registers 4
    .parameter "amount"

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->isReadyToLoadMore()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 396
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    .line 397
    iget v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMinCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMinCount:I

    .line 398
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->restartLoader()V

    .line 399
    const/4 v0, 0x1

    .line 401
    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoading:Z

    .line 160
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-interface {v2, p1, p2}, Landroid/support/v4/app/LoaderManager$LoaderCallbacks;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;

    move-result-object v0

    check-cast v0, Landroid/support/v4/content/CursorLoader;

    .line 163
    .local v0, loader:Landroid/support/v4/content/CursorLoader;
    invoke-virtual {v0}, Landroid/support/v4/content/CursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 164
    .local v1, uri:Landroid/net/Uri;
    iget v2, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMinCount:I

    iget-wide v3, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/reader/provider/ReaderContract;->withQueryParameters(Landroid/net/Uri;IJ)Landroid/net/Uri;

    move-result-object v1

    .line 165
    invoke-virtual {v0, v1}, Landroid/support/v4/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 167
    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 15
    .parameter
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, l:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const-wide v10, 0x7fffffffffffffffL

    const/4 v5, 0x0

    .line 174
    move-object v3, p1

    check-cast v3, Landroid/support/v4/content/CursorLoader;

    .line 177
    .local v3, loader:Landroid/support/v4/content/CursorLoader;
    iget-object v6, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-interface {v6, v3, p2}, Landroid/support/v4/app/LoaderManager$LoaderCallbacks;->onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V

    .line 180
    if-eqz p2, :cond_60

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 181
    .local v2, extras:Landroid/os/Bundle;
    :goto_14
    iput-boolean v5, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoading:Z

    .line 182
    const-string v6, "com.google.feeds.cursor.extra.ERROR"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasError:Z

    .line 183
    const-string v6, "com.google.feeds.cursor.extra.MORE"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasMore:Z

    .line 186
    iget-boolean v6, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasError:Z

    if-eqz v6, :cond_63

    .line 187
    const-string v6, "com.google.feeds.cursor.extra.SOLUTION"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/content/Intent;

    .line 188
    .local v4, solution:Landroid/content/Intent;
    sget-object v6, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_5a

    iget-wide v6, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    const-wide v8, 0x7ffffffffffffffeL

    cmp-long v6, v6, v8

    if-nez v6, :cond_5a

    .line 193
    invoke-virtual {v3}, Landroid/support/v4/content/CursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/reader/provider/ReaderContract;->requery(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/support/v4/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 194
    iput-wide v10, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    .line 196
    if-eqz p2, :cond_58

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    :cond_58
    iput v5, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMinCount:I

    .line 198
    :cond_5a
    if-eqz v4, :cond_5f

    .line 199
    invoke-direct {p0, v4}, Lcom/google/android/apps/reader/fragment/Loadable;->startSolution(Landroid/content/Intent;)V

    .line 214
    .end local v4           #solution:Landroid/content/Intent;
    :cond_5f
    :goto_5f
    return-void

    .line 180
    .end local v2           #extras:Landroid/os/Bundle;
    :cond_60
    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_14

    .line 204
    .restart local v2       #extras:Landroid/os/Bundle;
    :cond_63
    invoke-virtual {v3}, Landroid/support/v4/content/CursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/reader/provider/ReaderContract;->requery(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/support/v4/content/CursorLoader;->setUri(Landroid/net/Uri;)V

    .line 205
    iput-wide v10, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    .line 208
    if-eqz p2, :cond_76

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    :cond_76
    iput v5, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMinCount:I

    .line 211
    const-string v5, "com.google.feeds.cursor.extra.TIMESTAMP"

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 212
    .local v0, date:J
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/reader/fragment/Loadable;->autoRefresh(J)V

    goto :goto_5f
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 60
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/reader/fragment/Loadable;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v1, 0x0

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-interface {v0, p1}, Landroid/support/v4/app/LoaderManager$LoaderCallbacks;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    .line 237
    iput-boolean v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoading:Z

    .line 238
    iput-boolean v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasError:Z

    .line 239
    iput-boolean v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasMore:Z

    .line 240
    return-void
.end method

.method public refresh()V
    .registers 2

    .prologue
    .line 314
    iget v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMinCount:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/fragment/Loadable;->refresh(I)V

    .line 315
    return-void
.end method

.method public refresh(I)V
    .registers 4
    .parameter "n"

    .prologue
    .line 304
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    .line 305
    iput p1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMinCount:I

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->restartLoader()V

    .line 307
    return-void
.end method

.method public refreshAfterLoading()V
    .registers 3

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->getDefaultMaxAge()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeAfterLoading:J

    .line 150
    return-void
.end method

.method public refreshBeforeLoading()V
    .registers 3

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->getDefaultMaxAge()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mMaxAgeBeforeLoading:J

    .line 138
    return-void
.end method

.method public restartLoader()V
    .registers 4

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoaderId:I

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 322
    return-void
.end method

.method public restartLoaderIf(Z)V
    .registers 2
    .parameter "condition"

    .prologue
    .line 348
    if-eqz p1, :cond_6

    .line 349
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->restartLoader()V

    .line 353
    :goto_5
    return-void

    .line 351
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/Loadable;->destroyLoader()V

    goto :goto_5
.end method

.method public restartLoaderIfHasError()V
    .registers 2

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasError:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/fragment/Loadable;->restartLoaderIf(Z)V

    .line 363
    return-void
.end method

.method public retry()V
    .registers 4

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mHasError:Z

    if-eqz v0, :cond_d

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/reader/fragment/Loadable;->mLoaderId:I

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 283
    :cond_d
    return-void
.end method
