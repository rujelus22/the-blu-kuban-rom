.class public Lcom/google/android/apps/reader/app/GoogleAuthenticator;
.super Lcom/google/android/accounts/DatabaseAuthenticator;
.source "GoogleAuthenticator.java"


# static fields
.field private static final AUTHENTICATOR_ACTIVITY:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATABASE_NAME:Ljava/lang/String; = "accounts.db"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 35
    const-class v0, Landroid/app/Activity;

    sput-object v0, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->AUTHENTICATOR_ACTIVITY:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 40
    const-string v0, "accounts.db"

    sget-object v1, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->AUTHENTICATOR_ACTIVITY:Ljava/lang/Class;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/accounts/DatabaseAuthenticator;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method private static asList([Ljava/lang/Object;)Ljava/util/List;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, array:[Ljava/lang/Object;,"[TT;"
    if-eqz p0, :cond_7

    .line 25
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 27
    :goto_6
    return-object v0

    :cond_7
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_6
.end method


# virtual methods
.method public getAccountsByType(Ljava/lang/String;)[Lcom/google/android/accounts/Account;
    .registers 3
    .parameter "type"

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/google/android/accounts/DatabaseAuthenticator;->getAccountsByType(Ljava/lang/String;)[Lcom/google/android/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;)[Lcom/google/android/accounts/Account;
    .registers 12
    .parameter "type"
    .parameter "features"

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 97
    const-string v8, "com.google"

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_45

    .line 99
    :try_start_a
    new-instance v3, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;

    iget-object v8, p0, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->mContext:Landroid/content/Context;

    invoke-direct {v3, v8}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;-><init>(Landroid/content/Context;)V
    :try_end_11
    .catch Lcom/google/android/googlelogindist/GoogleLoginServiceNotFoundException; {:try_start_a .. :try_end_11} :catch_4f

    .line 102
    .local v3, helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    :try_start_11
    invoke-static {p2}, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 103
    .local v2, featureList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v8, "google"

    invoke-interface {v2, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_25

    const-string v8, "reader"

    invoke-interface {v2, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_26

    :cond_25
    move v5, v7

    .line 106
    .local v5, requiresGoogle:Z
    :cond_26
    invoke-virtual {v3, v5}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->getAccount(Z)Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, accountName:Ljava/lang/String;
    if-eqz v1, :cond_42

    .line 108
    new-instance v0, Lcom/google/android/accounts/Account;

    const-string v7, "com.google"

    invoke-direct {v0, v1, v7}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .local v0, account:Lcom/google/android/accounts/Account;
    const/4 v4, 0x0

    .line 110
    .local v4, password:Ljava/lang/String;
    const/4 v6, 0x0

    .line 111
    .local v6, userdata:Landroid/os/Bundle;
    invoke-virtual {p0, v0, v4, v6}, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->addAccountExplicitly(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 112
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/android/accounts/Account;

    const/4 v8, 0x0

    aput-object v0, v7, v8
    :try_end_3e
    .catchall {:try_start_11 .. :try_end_3e} :catchall_4a

    .line 115
    :try_start_3e
    invoke-virtual {v3}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->close()V

    .line 122
    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v1           #accountName:Ljava/lang/String;
    .end local v2           #featureList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v3           #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    .end local v4           #password:Ljava/lang/String;
    .end local v5           #requiresGoogle:Z
    .end local v6           #userdata:Landroid/os/Bundle;
    :goto_41
    return-object v7

    .line 115
    .restart local v1       #accountName:Ljava/lang/String;
    .restart local v2       #featureList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3       #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    .restart local v5       #requiresGoogle:Z
    :cond_42
    invoke-virtual {v3}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->close()V
    :try_end_45
    .catch Lcom/google/android/googlelogindist/GoogleLoginServiceNotFoundException; {:try_start_3e .. :try_end_45} :catch_4f

    .line 122
    .end local v1           #accountName:Ljava/lang/String;
    .end local v2           #featureList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v3           #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    .end local v5           #requiresGoogle:Z
    :cond_45
    :goto_45
    invoke-super {p0, p1, p2}, Lcom/google/android/accounts/DatabaseAuthenticator;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;)[Lcom/google/android/accounts/Account;

    move-result-object v7

    goto :goto_41

    .line 115
    .restart local v3       #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    :catchall_4a
    move-exception v7

    :try_start_4b
    invoke-virtual {v3}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->close()V

    throw v7
    :try_end_4f
    .catch Lcom/google/android/googlelogindist/GoogleLoginServiceNotFoundException; {:try_start_4b .. :try_end_4f} :catch_4f

    .end local v3           #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    :catch_4f
    move-exception v7

    goto :goto_45
.end method

.method public getAuthToken(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 11
    .parameter "account"
    .parameter "authTokenType"
    .parameter "loginOptions"

    .prologue
    .line 61
    const-string v5, "com.google"

    iget-object v6, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_54

    .line 63
    :try_start_a
    new-instance v1, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;

    iget-object v5, p0, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;-><init>(Landroid/content/Context;)V
    :try_end_11
    .catch Lcom/google/android/googlelogindist/GoogleLoginServiceNotFoundException; {:try_start_a .. :try_end_11} :catch_53

    .line 66
    .local v1, helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    :try_start_11
    const-string v5, "SID"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4c

    const/4 v4, 0x0

    .line 67
    .local v4, service:Ljava/lang/String;
    :goto_1a
    const/4 v2, 0x0

    .line 68
    .local v2, notifyAuthFailure:Z
    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v5, v4, v2}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->getCredentials(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/googleapps/GoogleLoginCredentialsResult;

    move-result-object v3

    .line 70
    .local v3, result:Lcom/google/android/googleapps/GoogleLoginCredentialsResult;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 71
    .local v0, bundle:Landroid/os/Bundle;
    const-string v5, "authAccount"

    invoke-virtual {v3}, Lcom/google/android/googleapps/GoogleLoginCredentialsResult;->getAccount()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v5, "accountType"

    const-string v6, "com.google"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v5, "authtoken"

    invoke-virtual {v3}, Lcom/google/android/googleapps/GoogleLoginCredentialsResult;->getCredentialsString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v5, "intent"

    invoke-virtual {v3}, Lcom/google/android/googleapps/GoogleLoginCredentialsResult;->getCredentialsIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_48
    .catchall {:try_start_11 .. :try_end_48} :catchall_4e

    .line 77
    :try_start_48
    invoke-virtual {v1}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->close()V

    .line 84
    .end local v0           #bundle:Landroid/os/Bundle;
    .end local v1           #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    .end local v2           #notifyAuthFailure:Z
    .end local v3           #result:Lcom/google/android/googleapps/GoogleLoginCredentialsResult;
    .end local v4           #service:Ljava/lang/String;
    :goto_4b
    return-object v0

    .restart local v1       #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    :cond_4c
    move-object v4, p2

    .line 66
    goto :goto_1a

    .line 77
    :catchall_4e
    move-exception v5

    invoke-virtual {v1}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->close()V

    throw v5
    :try_end_53
    .catch Lcom/google/android/googlelogindist/GoogleLoginServiceNotFoundException; {:try_start_48 .. :try_end_53} :catch_53

    .end local v1           #helper:Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;
    :catch_53
    move-exception v5

    .line 84
    :cond_54
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/accounts/DatabaseAuthenticator;->getAuthToken(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_4b
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "authTokenType"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->mContext:Landroid/content/Context;

    const/high16 v1, 0x7f0d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "accountType"
    .parameter "authToken"

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/google/android/accounts/DatabaseAuthenticator;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/reader/app/GoogleAuthenticator;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/googlelogindist/GoogleLoginServiceBlockingHelper;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/google/android/googlelogindist/GoogleLoginServiceNotFoundException; {:try_start_3 .. :try_end_8} :catch_9

    .line 57
    :goto_8
    return-void

    :catch_9
    move-exception v0

    goto :goto_8
.end method
