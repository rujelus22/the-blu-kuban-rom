.class final Lcom/google/android/apps/reader/widget/ItemViewBinder;
.super Ljava/lang/Object;
.source "ItemViewBinder.java"

# interfaces
.implements Lcom/google/android/apps/reader/widget/ItemQuery;


# static fields
.field private static final ICON_LIKED:Ljava/lang/String; = "/android_res/drawable/emo_im_happy"

.field private static final ICON_SHARED:Ljava/lang/String; = "/android_res/drawable/ic_contact_picture"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public static bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 10
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 216
    const v5, 0x7f0b003b

    invoke-virtual {p0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/webkit/WebView;

    .line 219
    .local v4, webView:Landroid/webkit/WebView;
    invoke-virtual {v4}, Landroid/webkit/WebView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 220
    .local v0, data:Ljava/util/Map;
    invoke-static {p2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->htmlUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v1

    .line 221
    .local v1, htmlUri:Landroid/net/Uri;
    const-string v5, "htmlUri"

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_66

    const/4 v2, 0x1

    .line 224
    .local v2, itemChanged:Z
    :goto_20
    const-string v5, "likedByHtml"

    invoke-static {p1, p2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->createLikedByHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    const-string v5, "sharedByHtml"

    invoke-static {p1, p2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->createSharedByHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    const-string v5, "annotationHtml"

    invoke-static {p1, p2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->createAnnotationHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    if-eqz v2, :cond_65

    .line 230
    const-string v5, "titleHtml"

    invoke-static {p1, p2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->createTitleHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    const-string v5, "subtitleHtml"

    invoke-static {p1, p2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->createSubtitleHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "?text/html"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 234
    .local v3, url:Ljava/lang/String;
    invoke-virtual {v4, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 239
    .end local v3           #url:Ljava/lang/String;
    :cond_65
    return-void

    .line 221
    .end local v2           #itemChanged:Z
    :cond_66
    const/4 v2, 0x0

    goto :goto_20
.end method

.method private static createAnchor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "text"
    .parameter "href"

    .prologue
    .line 100
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2f

    .line 101
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2e

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "<a href=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    invoke-static {p1}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string v1, "</a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 113
    .end local v0           #builder:Ljava/lang/StringBuilder;
    .end local p0
    :cond_2e
    :goto_2e
    return-object p0

    .restart local p0
    :cond_2f
    const/4 p0, 0x0

    goto :goto_2e
.end method

.method private static createAnnotationHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 203
    sget-object v1, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 204
    const-string v0, ""

    .line 210
    :cond_a
    :goto_a
    return-object v0

    .line 206
    :cond_b
    const/16 v1, 0x19

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, annotation:Ljava/lang/String;
    if-nez v0, :cond_a

    .line 210
    const-string v0, ""

    goto :goto_a
.end method

.method private static createLikedByHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .registers 5
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 162
    sget-object v2, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 163
    const-string v2, ""

    .line 172
    :goto_a
    return-object v2

    .line 165
    :cond_b
    const/16 v2, 0x13

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 166
    .local v1, likeCount:I
    if-eqz v1, :cond_2d

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v2, "<img src=\"/android_res/drawable/emo_im_happy\" /> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    invoke-static {p0, v1}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->getLikeCountText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_a

    .line 172
    .end local v0           #builder:Ljava/lang/StringBuilder;
    :cond_2d
    const-string v2, ""

    goto :goto_a
.end method

.method private static createSharedByHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .registers 8
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 177
    sget-object v3, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 178
    const-string v3, ""

    .line 198
    :goto_a
    return-object v3

    .line 180
    :cond_b
    const/16 v3, 0xe

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 181
    .local v1, displayName:Ljava/lang/String;
    const/16 v3, 0xd

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 182
    .local v2, photoUri:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7a

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v3, "<img src=\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_63

    .line 186
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?image/png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :goto_47
    const-string v3, "\" /> "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const/16 v3, 0xf

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_69

    .line 192
    const v3, 0x7f0d00b1

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :goto_5e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_a

    .line 188
    :cond_63
    const-string v3, "/android_res/drawable/ic_contact_picture"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_47

    .line 194
    :cond_69
    const v3, 0x7f0d00b0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5e

    .line 198
    .end local v0           #builder:Ljava/lang/StringBuilder;
    :cond_7a
    const-string v3, ""

    goto :goto_a
.end method

.method private static createSubtitleHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .registers 22
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 139
    const/16 v18, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 140
    .local v15, title:Ljava/lang/String;
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 141
    .local v12, streamId:Ljava/lang/String;
    const/4 v9, 0x0

    .line 142
    .local v9, href:Ljava/lang/String;
    if-eqz v12, :cond_36

    .line 143
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->getAccount(Landroid/database/Cursor;)Lcom/google/android/accounts/Account;

    move-result-object v2

    .line 144
    .local v2, account:Lcom/google/android/accounts/Account;
    sget-object v18, Lcom/google/android/apps/reader/preference/ReaderPreference;->READ_ITEMS_VISIBLE:Lcom/google/android/apps/reader/preference/ReaderPreference;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/reader/preference/ReaderPreference;->getBoolean(Landroid/content/Context;Lcom/google/android/accounts/Account;)Z

    move-result v18

    if-nez v18, :cond_b3

    const/4 v5, 0x1

    .line 145
    .local v5, excludeRead:Z
    :goto_28
    move-object/from16 v0, p0

    invoke-static {v0, v2, v12}, Lcom/google/android/apps/reader/preference/StreamPreferences;->getRanking(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 146
    .local v10, ranking:Ljava/lang/String;
    invoke-static {v2, v12, v5, v10}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 147
    .local v17, uri:Landroid/net/Uri;
    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 149
    .end local v2           #account:Lcom/google/android/accounts/Account;
    .end local v5           #excludeRead:Z
    .end local v10           #ranking:Ljava/lang/String;
    .end local v17           #uri:Landroid/net/Uri;
    :cond_36
    const/16 v18, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 150
    .local v3, author:Ljava/lang/String;
    invoke-static {v15, v9}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->createAnchor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 151
    .local v11, source:Ljava/lang/String;
    const/16 v18, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 152
    .local v16, updated:Ljava/lang/Long;
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 153
    .local v4, date:Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 154
    .local v14, time:Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_b6

    const/4 v8, 0x1

    .line 155
    .local v8, hasSource:Z
    :goto_7d
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_b8

    const/4 v7, 0x1

    .line 156
    .local v7, hasAuthor:Z
    :goto_84
    invoke-static {v8, v7}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->getSubtitleTemplate(ZZ)I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v13

    .line 157
    .local v13, template:Ljava/lang/CharSequence;
    const/16 v18, 0x4

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/CharSequence;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v11, v18, v19

    const/16 v19, 0x1

    aput-object v3, v18, v19

    const/16 v19, 0x2

    aput-object v4, v18, v19

    const/16 v19, 0x3

    aput-object v14, v18, v19

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 158
    .local v6, expanded:Ljava/lang/CharSequence;
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    return-object v18

    .line 144
    .end local v3           #author:Ljava/lang/String;
    .end local v4           #date:Ljava/lang/String;
    .end local v6           #expanded:Ljava/lang/CharSequence;
    .end local v7           #hasAuthor:Z
    .end local v8           #hasSource:Z
    .end local v11           #source:Ljava/lang/String;
    .end local v13           #template:Ljava/lang/CharSequence;
    .end local v14           #time:Ljava/lang/String;
    .end local v16           #updated:Ljava/lang/Long;
    .restart local v2       #account:Lcom/google/android/accounts/Account;
    :cond_b3
    const/4 v5, 0x0

    goto/16 :goto_28

    .line 154
    .end local v2           #account:Lcom/google/android/accounts/Account;
    .restart local v3       #author:Ljava/lang/String;
    .restart local v4       #date:Ljava/lang/String;
    .restart local v11       #source:Ljava/lang/String;
    .restart local v14       #time:Ljava/lang/String;
    .restart local v16       #updated:Ljava/lang/Long;
    :cond_b6
    const/4 v8, 0x0

    goto :goto_7d

    .line 155
    .restart local v8       #hasSource:Z
    :cond_b8
    const/4 v7, 0x0

    goto :goto_84
.end method

.method private static createTitleHtml(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .registers 5
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 130
    const/16 v2, 0x16

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 131
    .local v1, title:Ljava/lang/String;
    const/16 v2, 0xc

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, href:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 133
    const v2, 0x7f0d012e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    :cond_1d
    invoke-static {v1, v0}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->createAnchor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static final escape(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "input"

    .prologue
    .line 51
    if-eqz p0, :cond_22

    .line 52
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 53
    const-string v0, "<"

    const-string v1, "&lt;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 54
    const-string v0, ">"

    const-string v1, "&gt;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 55
    const-string v0, "\""

    const-string v1, "&quot;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 57
    :cond_22
    return-object p0
.end method

.method private static getAccount(Landroid/database/Cursor;)Lcom/google/android/accounts/Account;
    .registers 4
    .parameter "cursor"

    .prologue
    .line 118
    const/16 v2, 0x14

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, accountName:Ljava/lang/String;
    const/16 v2, 0x15

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, accountType:Ljava/lang/String;
    new-instance v2, Lcom/google/android/accounts/Account;

    invoke-direct {v2, v0, v1}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private static getLikeCountText(Landroid/content/Context;I)Ljava/lang/String;
    .registers 7
    .parameter "context"
    .parameter "likeCount"

    .prologue
    const/16 v4, 0x64

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 62
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 63
    .local v1, resources:Landroid/content/res/Resources;
    if-ge p1, v4, :cond_1a

    .line 64
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    .line 67
    .local v0, formatArgs:[Ljava/lang/Object;
    const v2, 0x7f0e0001

    invoke-virtual {v1, v2, p1, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 72
    :goto_19
    return-object v2

    .line 69
    .end local v0           #formatArgs:[Ljava/lang/Object;
    :cond_1a
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    .line 72
    .restart local v0       #formatArgs:[Ljava/lang/Object;
    const v2, 0x7f0d0126

    invoke-virtual {v1, v2, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_19
.end method

.method private static getSubtitleTemplate(ZZ)I
    .registers 3
    .parameter "hasSource"
    .parameter "hasAuthor"

    .prologue
    .line 81
    if-eqz p0, :cond_c

    .line 82
    if-eqz p1, :cond_8

    .line 83
    const v0, 0x7f0d008c

    .line 88
    :goto_7
    return v0

    .line 85
    :cond_8
    const v0, 0x7f0d008d

    goto :goto_7

    .line 88
    :cond_c
    const v0, 0x7f0d008f

    goto :goto_7
.end method

.method private static htmlUri(Landroid/database/Cursor;)Landroid/net/Uri;
    .registers 5
    .parameter "cursor"

    .prologue
    .line 124
    invoke-static {p0}, Lcom/google/android/apps/reader/widget/ItemViewBinder;->getAccount(Landroid/database/Cursor;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 125
    .local v0, account:Lcom/google/android/accounts/Account;
    const/4 v3, 0x1

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 126
    .local v1, itemId:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->htmlUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method public static unbindView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 242
    const v2, 0x7f0b003b

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    .line 245
    .local v1, webView:Landroid/webkit/WebView;
    invoke-virtual {v1}, Landroid/webkit/WebView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 246
    .local v0, data:Ljava/util/Map;
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 248
    invoke-virtual {v1}, Landroid/webkit/WebView;->clearView()V

    .line 249
    return-void
.end method
