.class public final Lcom/google/android/apps/reader/fragment/StreamFragment;
.super Landroid/support/v4/app/ListFragment;
.source "StreamFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/fragment/StreamFragment$1;,
        Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;,
        Lcom/google/android/apps/reader/fragment/StreamFragment$SubscribedDataSetObserver;,
        Lcom/google/android/apps/reader/fragment/StreamFragment$StreamObserver;,
        Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/ListFragment;",
        "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final DIALOG_DELETE_CONFIRM:Ljava/lang/String; = "reader:delete_confirm"

.field private static final DIALOG_MARK_ALL_AS_READ_CONFIRM:Ljava/lang/String; = "reader:dialog_mark_read_confirm"

.field private static final DIALOG_RANKING:Ljava/lang/String; = "reader:dialog_ranking"

.field private static final DIALOG_RENAME_CONFIRM:Ljava/lang/String; = "reader:rename_confirm"

.field private static final DIALOG_UNSUBSCRIBE_CONFIRM:Ljava/lang/String; = "reader:unsubscribe_confirm"

.field private static final LOADER_ITEMS:I = 0x1

.field private static final REQUEST_DELETE_TAG:I = 0x2

.field private static final REQUEST_MARK_ALL_AS_READ:I = 0x4

.field private static final REQUEST_RENAME_TAG:I = 0x3

.field private static final REQUEST_VIEW_ITEM:I = 0x1

.field private static final STATE_TITLE:Ljava/lang/String; = "reader:title"

.field private static final STATE_URI:Ljava/lang/String; = "reader:uri"

.field public static final SUPPORTED_CONTENT_TYPES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "StreamFragment"


# instance fields
.field private mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

.field private mDisplayName:Ljava/lang/CharSequence;

.field private mEmpty:Landroid/widget/TextView;

.field private mFolder:Z

.field private mItemSelector:Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;

.field private mItems:Lcom/google/android/apps/reader/fragment/Loadable;

.field private mListView:Landroid/widget/ListView;

.field private mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

.field private mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

.field private mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

.field private mRefreshBeforeLoading:Z

.field private mStreamPreferences:Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;

.field private mTitle:Ljava/lang/CharSequence;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    const-class v0, Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_24

    move v0, v1

    :goto_b
    sput-boolean v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->$assertionsDisabled:Z

    .line 95
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    aput-object v3, v0, v2

    sget-object v2, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->SUPPORTED_CONTENT_TYPES:Ljava/util/List;

    return-void

    :cond_24
    move v0, v2

    .line 80
    goto :goto_b
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 80
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mRefreshBeforeLoading:Z

    .line 919
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/reader/fragment/StreamFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->updateEmptyText()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/reader/fragment/StreamFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->updateActionBar()V

    return-void
.end method

.method private callbacks(Landroid/view/View;)Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .registers 4
    .parameter "view"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/reader/widget/ReaderWindow;->from(Landroid/content/Context;)Lcom/google/android/apps/reader/widget/ReaderWindow;

    move-result-object v0

    invoke-static {p0, p1, p0, p0}, Lcom/google/android/apps/reader/fragment/ListStateObserver;->forFragment(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;Landroid/view/View$OnClickListener;)Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->observe(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    move-result-object v0

    return-object v0
.end method

.method private changeFolders()V
    .registers 4

    .prologue
    .line 573
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getSubscriptionUri()Landroid/net/Uri;

    move-result-object v1

    .line 574
    .local v1, uri:Landroid/net/Uri;
    if-eqz v1, :cond_10

    .line 575
    new-instance v0, Landroid/content/Intent;

    const-string v2, "org.openintents.action.TAG"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 576
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->startActivity(Landroid/content/Intent;)V

    .line 578
    .end local v0           #intent:Landroid/content/Intent;
    :cond_10
    return-void
.end method

.method private changeUri(Landroid/net/Uri;)V
    .registers 4
    .parameter "uri"

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/Loadable;->destroyLoader()V

    .line 347
    iput-object p1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    .line 348
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->handleUriChanged()V

    .line 349
    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mRefreshBeforeLoading:Z

    if-eqz v0, :cond_13

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/Loadable;->refreshBeforeLoading()V

    .line 352
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    :goto_1a
    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/fragment/Loadable;->restartLoaderIf(Z)V

    .line 353
    return-void

    .line 352
    :cond_1e
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private confirmMarkAllAsRead()V
    .registers 5

    .prologue
    .line 599
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getItemsUri()Landroid/net/Uri;

    move-result-object v1

    .line 600
    .local v1, uri:Landroid/net/Uri;
    if-eqz v1, :cond_1c

    .line 601
    new-instance v0, Lcom/google/android/apps/reader/dialog/MarkAllReadConfirmationDialog;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/reader/dialog/MarkAllReadConfirmationDialog;-><init>(Landroid/net/Uri;Ljava/lang/CharSequence;)V

    .line 602
    .local v0, dialog:Landroid/support/v4/app/DialogFragment;
    const/4 v2, 0x4

    invoke-virtual {v0, p0, v2}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 603
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "reader:dialog_mark_read_confirm"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 605
    .end local v0           #dialog:Landroid/support/v4/app/DialogFragment;
    :cond_1c
    return-void
.end method

.method private findFragmentById(I)Landroid/support/v4/app/Fragment;
    .registers 4
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/support/v4/app/Fragment;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 191
    .local v0, fragments:Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    return-object v1
.end method

.method private getEmptyText()Ljava/lang/CharSequence;
    .registers 10

    .prologue
    .line 832
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getItemsUri()Landroid/net/Uri;

    move-result-object v5

    .line 833
    .local v5, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 834
    .local v0, context:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 835
    .local v4, streamId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 836
    .local v3, query:Ljava/lang/String;
    if-eqz v4, :cond_27

    .line 837
    invoke-static {v5}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getExcludeTarget(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 838
    .local v2, excludeTarget:Ljava/lang/String;
    if-eqz v2, :cond_1c

    .line 839
    invoke-static {v2}, Lcom/google/android/apps/reader/provider/ReaderStream;->unsetUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 841
    :cond_1c
    const-string v6, "user/-/state/com.google/read"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 842
    .local v1, excludeRead:Z
    invoke-direct {p0, v4, v1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getEmptyTextForStream(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v6

    .line 846
    .end local v1           #excludeRead:Z
    .end local v2           #excludeTarget:Ljava/lang/String;
    :goto_26
    return-object v6

    .line 843
    :cond_27
    if-eqz v3, :cond_37

    .line 844
    const v6, 0x7f0d00af

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_26

    .line 846
    :cond_37
    const v6, 0x7f0d00a6

    invoke-virtual {v0, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_26
.end method

.method private getEmptyTextForFollowing(Z)Ljava/lang/CharSequence;
    .registers 4
    .parameter "excludeRead"

    .prologue
    .line 769
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 770
    .local v0, context:Landroid/content/Context;
    if-eqz p1, :cond_e

    .line 771
    const v1, 0x7f0d00ac

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 773
    :goto_d
    return-object v1

    :cond_e
    const v1, 0x7f0d00ab

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_d
.end method

.method private getEmptyTextForShared(Ljava/lang/String;Z)Ljava/lang/CharSequence;
    .registers 7
    .parameter "streamId"
    .parameter "excludeRead"

    .prologue
    .line 800
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 801
    .local v0, context:Landroid/content/Context;
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/widget/StreamAdapter;->getUserId()Ljava/lang/String;

    move-result-object v2

    .line 802
    .local v2, userId:Ljava/lang/String;
    if-nez v2, :cond_14

    .line 803
    const v3, 0x7f0d00a6

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 809
    :goto_13
    return-object v3

    .line 805
    :cond_14
    invoke-static {v2}, Lcom/google/android/apps/reader/provider/ReaderStream;->createBroadcastId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 806
    .local v1, sharedByMe:Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 807
    invoke-direct {p0, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getEmptyTextForSharedByMe(Z)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_13

    .line 809
    :cond_23
    invoke-direct {p0, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getEmptyTextForSharedByFriend(Z)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_13
.end method

.method private getEmptyTextForSharedByFriend(Z)Ljava/lang/CharSequence;
    .registers 7
    .parameter "excludeRead"

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 787
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 788
    .local v0, context:Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mDisplayName:Ljava/lang/CharSequence;

    if-eqz v1, :cond_28

    .line 789
    if-eqz p1, :cond_1a

    .line 790
    const v1, 0x7f0d00ae

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mDisplayName:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 795
    :goto_19
    return-object v1

    .line 792
    :cond_1a
    const v1, 0x7f0d00ad

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mDisplayName:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_19

    .line 795
    :cond_28
    const v1, 0x7f0d00a6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_19
.end method

.method private getEmptyTextForSharedByMe(Z)Ljava/lang/CharSequence;
    .registers 4
    .parameter "excludeRead"

    .prologue
    .line 778
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 779
    .local v0, context:Landroid/content/Context;
    if-eqz p1, :cond_e

    .line 780
    const v1, 0x7f0d00a6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 782
    :goto_d
    return-object v1

    :cond_e
    const v1, 0x7f0d00a9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_d
.end method

.method private getEmptyTextForStream(Ljava/lang/String;Z)Ljava/lang/CharSequence;
    .registers 5
    .parameter "streamId"
    .parameter "excludeRead"

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 816
    .local v0, context:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isMyStuff(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    if-nez p2, :cond_14

    .line 817
    const v1, 0x7f0d00a8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 827
    :goto_13
    return-object v1

    .line 818
    :cond_14
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isBroadcast(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 819
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getEmptyTextForShared(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_13

    .line 820
    :cond_1f
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isNotes(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2f

    if-nez p2, :cond_2f

    .line 821
    const v1, 0x7f0d00aa

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_13

    .line 822
    :cond_2f
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isStarred(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3f

    if-nez p2, :cond_3f

    .line 823
    const v1, 0x7f0d00a7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_13

    .line 824
    :cond_3f
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isFollowing(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 825
    invoke-direct {p0, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getEmptyTextForFollowing(Z)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_13

    .line 827
    :cond_4a
    const v1, 0x7f0d00a6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_13
.end method

.method private getSubscriptionUri()Landroid/net/Uri;
    .registers 4

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 722
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getStreamId()Ljava/lang/String;

    move-result-object v1

    .line 723
    .local v1, streamId:Ljava/lang/String;
    if-eqz v0, :cond_1d

    if-eqz v1, :cond_1d

    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isSubscription(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isWebFeed(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 725
    :cond_18
    invoke-static {v0, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 727
    :goto_1c
    return-object v2

    :cond_1d
    const/4 v2, 0x0

    goto :goto_1c
.end method

.method private getTagUri()Landroid/net/Uri;
    .registers 4

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 733
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getStreamId()Ljava/lang/String;

    move-result-object v1

    .line 734
    .local v1, streamId:Ljava/lang/String;
    if-eqz v0, :cond_17

    if-eqz v1, :cond_17

    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isLabel(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 735
    invoke-static {v0, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 737
    :goto_16
    return-object v2

    :cond_17
    const/4 v2, 0x0

    goto :goto_16
.end method

.method private handleUriChanged()V
    .registers 1

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->updatePreferences()V

    .line 357
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->updateNotSubscribedFragment()V

    .line 358
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->updateActionBar()V

    .line 359
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->notifyObserver()V

    .line 360
    return-void
.end method

.method private hasItemFragment()Z
    .registers 4

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0b0038

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 406
    .local v0, fragment:Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isInLayout()Z

    move-result v1

    if-eqz v1, :cond_15

    const/4 v1, 0x1

    :goto_14
    return v1

    :cond_15
    const/4 v1, 0x0

    goto :goto_14
.end method

.method private isSearch()Z
    .registers 3

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getItemsUri()Landroid/net/Uri;

    move-result-object v0

    .line 648
    .local v0, uri:Landroid/net/Uri;
    if-eqz v0, :cond_e

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    const/4 v1, 0x1

    :goto_d
    return v1

    :cond_e
    const/4 v1, 0x0

    goto :goto_d
.end method

.method private notifyObserver()V
    .registers 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

    if-eqz v0, :cond_9

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

    invoke-interface {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;->onStreamChanged()V

    .line 686
    :cond_9
    return-void
.end method

.method private renameSubscription()V
    .registers 5

    .prologue
    .line 557
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getSubscriptionUri()Landroid/net/Uri;

    move-result-object v1

    .line 558
    .local v1, uri:Landroid/net/Uri;
    if-eqz v1, :cond_16

    .line 559
    new-instance v0, Lcom/google/android/apps/reader/dialog/RenameSubscriptionDialog;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/reader/dialog/RenameSubscriptionDialog;-><init>(Landroid/net/Uri;Ljava/lang/CharSequence;)V

    .line 560
    .local v0, dialog:Landroid/support/v4/app/DialogFragment;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "reader:rename_confirm"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 562
    .end local v0           #dialog:Landroid/support/v4/app/DialogFragment;
    :cond_16
    return-void
.end method

.method private renameTag()V
    .registers 5

    .prologue
    .line 590
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getTagUri()Landroid/net/Uri;

    move-result-object v1

    .line 591
    .local v1, uri:Landroid/net/Uri;
    if-eqz v1, :cond_1c

    .line 592
    new-instance v0, Lcom/google/android/apps/reader/dialog/RenameLabelDialog;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    iget-boolean v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mFolder:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/reader/dialog/RenameLabelDialog;-><init>(Landroid/net/Uri;Ljava/lang/CharSequence;Z)V

    .line 593
    .local v0, dialog:Landroid/support/v4/app/DialogFragment;
    const/4 v2, 0x3

    invoke-virtual {v0, p0, v2}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 594
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "reader:rename_confirm"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 596
    .end local v0           #dialog:Landroid/support/v4/app/DialogFragment;
    :cond_1c
    return-void
.end method

.method private showDeleteConfirmation()V
    .registers 5

    .prologue
    .line 581
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getTagUri()Landroid/net/Uri;

    move-result-object v1

    .line 582
    .local v1, uri:Landroid/net/Uri;
    if-eqz v1, :cond_1a

    .line 583
    new-instance v0, Lcom/google/android/apps/reader/dialog/DeleteConfirmationDialog;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/reader/dialog/DeleteConfirmationDialog;-><init>(Landroid/net/Uri;Ljava/lang/CharSequence;)V

    .line 584
    .local v0, dialog:Landroid/support/v4/app/DialogFragment;
    const/4 v2, 0x2

    invoke-virtual {v0, p0, v2}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 585
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "reader:delete_confirm"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 587
    .end local v0           #dialog:Landroid/support/v4/app/DialogFragment;
    :cond_1a
    return-void
.end method

.method private showRankingChooser()V
    .registers 4

    .prologue
    .line 619
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_16

    .line 620
    new-instance v0, Lcom/google/android/apps/reader/dialog/RankingDialog;

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/reader/dialog/RankingDialog;-><init>(Landroid/net/Uri;Ljava/lang/CharSequence;)V

    .line 621
    .local v0, dialog:Lcom/google/android/apps/reader/dialog/RankingDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "reader:dialog_ranking"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/reader/dialog/RankingDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 623
    .end local v0           #dialog:Lcom/google/android/apps/reader/dialog/RankingDialog;
    :cond_16
    return-void
.end method

.method private simulateKeyDown(I)Z
    .registers 4
    .parameter "keyCode"

    .prologue
    .line 457
    new-instance v0, Landroid/view/KeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Landroid/view/KeyEvent;-><init>(II)V

    .line 458
    .local v0, event:Landroid/view/KeyEvent;
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p1, v0}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method private unsubscribe()V
    .registers 5

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getSubscriptionUri()Landroid/net/Uri;

    move-result-object v1

    .line 566
    .local v1, uri:Landroid/net/Uri;
    if-eqz v1, :cond_16

    .line 567
    new-instance v0, Lcom/google/android/apps/reader/dialog/UnsubscribeConfirmationDialog;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/reader/dialog/UnsubscribeConfirmationDialog;-><init>(Landroid/net/Uri;Ljava/lang/CharSequence;)V

    .line 568
    .local v0, dialog:Landroid/support/v4/app/DialogFragment;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "reader:unsubscribe_confirm"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 570
    .end local v0           #dialog:Landroid/support/v4/app/DialogFragment;
    :cond_16
    return-void
.end method

.method private updateActionBar()V
    .registers 2

    .prologue
    .line 667
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/reader/widget/ReaderWindow;->invalidateOptionsMenu(Landroid/support/v4/app/FragmentActivity;)V

    .line 668
    return-void
.end method

.method private updateEmptyText()V
    .registers 3

    .prologue
    .line 851
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getEmptyText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 852
    .local v0, text:Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mEmpty:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 853
    return-void
.end method

.method private updateNotSubscribedFragment()V
    .registers 5

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 657
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getStreamId()Ljava/lang/String;

    move-result-object v1

    .line 658
    .local v1, streamId:Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    if-eqz v3, :cond_1c

    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 659
    .local v2, title:Ljava/lang/String;
    :goto_12
    if-eqz v0, :cond_1e

    if-eqz v1, :cond_1e

    .line 660
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;->changeStreamId(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    :goto_1b
    return-void

    .line 658
    .end local v2           #title:Ljava/lang/String;
    :cond_1c
    const/4 v2, 0x0

    goto :goto_12

    .line 662
    .restart local v2       #title:Ljava/lang/String;
    :cond_1e
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;->clear()V

    goto :goto_1b
.end method

.method private updatePreferences()V
    .registers 4

    .prologue
    .line 671
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_1b

    .line 672
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 673
    .local v0, account:Lcom/google/android/accounts/Account;
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 674
    .local v1, streamId:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->setAccount(Lcom/google/android/accounts/Account;)V

    .line 675
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mStreamPreferences:Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;->setStream(Lcom/google/android/accounts/Account;Ljava/lang/String;)V

    .line 680
    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v1           #streamId:Ljava/lang/String;
    :goto_1a
    return-void

    .line 677
    :cond_1b
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->clear()V

    .line 678
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mStreamPreferences:Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;->clear()V

    goto :goto_1a
.end method

.method private workAroundLoaderBug()V
    .registers 4

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    .line 207
    .local v0, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    if-eqz v0, :cond_e

    .line 208
    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->onContentChanged()V

    .line 210
    :cond_e
    return-void
.end method


# virtual methods
.method public changeIntent(Landroid/content/Intent;)V
    .registers 19
    .parameter "intent"

    .prologue
    .line 249
    if-nez p1, :cond_8

    .line 250
    new-instance v14, Ljava/lang/NullPointerException;

    invoke-direct {v14}, Ljava/lang/NullPointerException;-><init>()V

    throw v14

    .line 253
    :cond_8
    const-string v14, "android.intent.extra.TITLE"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    .line 254
    const-string v14, "android.intent.extra.TITLE"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mDisplayName:Ljava/lang/CharSequence;

    .line 255
    const-string v14, "folder"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mFolder:Z

    .line 256
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    invoke-virtual {v14}, Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;->reset()V

    .line 257
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->resetList()V

    .line 259
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 260
    .local v5, context:Landroid/content/Context;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 261
    .local v3, action:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v13

    .line 262
    .local v13, uri:Landroid/net/Uri;
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 263
    .local v12, type:Ljava/lang/String;
    if-eqz v13, :cond_71

    invoke-virtual {v13}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    .line 264
    .local v9, scheme:Ljava/lang/String;
    :goto_4f
    const-string v14, "android.intent.action.SEARCH"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d5

    .line 265
    const-string v14, "query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 266
    .local v7, query:Ljava/lang/String;
    const-string v14, "app_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 267
    .local v4, appData:Landroid/os/Bundle;
    if-nez v7, :cond_73

    .line 268
    new-instance v14, Ljava/lang/IllegalArgumentException;

    const-string v15, "query was not specified"

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 263
    .end local v4           #appData:Landroid/os/Bundle;
    .end local v7           #query:Ljava/lang/String;
    .end local v9           #scheme:Ljava/lang/String;
    :cond_71
    const/4 v9, 0x0

    goto :goto_4f

    .line 270
    .restart local v4       #appData:Landroid/os/Bundle;
    .restart local v7       #query:Ljava/lang/String;
    .restart local v9       #scheme:Ljava/lang/String;
    :cond_73
    if-nez v4, :cond_8e

    .line 272
    const-string v14, "StreamFragment"

    const-string v15, "app_data was not specified"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    new-instance v4, Landroid/os/Bundle;

    .end local v4           #appData:Landroid/os/Bundle;
    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 276
    .restart local v4       #appData:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/google/android/apps/reader/preference/LocalPreferences;->getAccount(Landroid/content/Context;)Lcom/google/android/accounts/Account;

    move-result-object v1

    .line 277
    .local v1, account:Lcom/google/android/accounts/Account;
    if-eqz v1, :cond_8e

    .line 278
    const-string v14, "authAccount"

    iget-object v15, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    .end local v1           #account:Lcom/google/android/accounts/Account;
    :cond_8e
    const v14, 0x7f0d008b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    .line 282
    .local v11, template:Ljava/lang/CharSequence;
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/CharSequence;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    invoke-static {v11, v14}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    .line 284
    const-string v14, "authAccount"

    invoke-virtual {v4, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, accountName:Ljava/lang/String;
    const-string v14, "stream_id"

    invoke-virtual {v4, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 286
    .local v10, streamId:Ljava/lang/String;
    if-eqz v2, :cond_c4

    .line 287
    new-instance v1, Lcom/google/android/accounts/Account;

    const-string v14, "com.google"

    invoke-direct {v1, v2, v14}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    .restart local v1       #account:Lcom/google/android/accounts/Account;
    invoke-static {v1, v7, v10}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->searchUri(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    .line 337
    .end local v1           #account:Lcom/google/android/accounts/Account;
    .end local v2           #accountName:Ljava/lang/String;
    .end local v4           #appData:Landroid/os/Bundle;
    .end local v7           #query:Ljava/lang/String;
    .end local v10           #streamId:Ljava/lang/String;
    .end local v11           #template:Ljava/lang/CharSequence;
    :goto_c3
    return-void

    .line 290
    .restart local v2       #accountName:Ljava/lang/String;
    .restart local v4       #appData:Landroid/os/Bundle;
    .restart local v7       #query:Ljava/lang/String;
    .restart local v10       #streamId:Ljava/lang/String;
    .restart local v11       #template:Ljava/lang/CharSequence;
    :cond_c4
    const-string v14, "StreamFragment"

    const-string v15, "Cannot search without account"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto :goto_c3

    .line 293
    .end local v2           #accountName:Ljava/lang/String;
    .end local v4           #appData:Landroid/os/Bundle;
    .end local v7           #query:Ljava/lang/String;
    .end local v10           #streamId:Ljava/lang/String;
    .end local v11           #template:Ljava/lang/CharSequence;
    :cond_d5
    const-string v14, "android.intent.action.VIEW"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f6

    sget-object v14, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f6

    .line 298
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 299
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->startActivity(Landroid/content/Intent;)V

    .line 300
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_c3

    .line 301
    :cond_f6
    const-string v14, "content"

    invoke-virtual {v14, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_17d

    .line 302
    sget-object v14, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10c

    .line 303
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto :goto_c3

    .line 304
    :cond_10c
    sget-object v14, Lcom/google/android/apps/reader/fragment/StreamFragment;->SUPPORTED_CONTENT_TYPES:Ljava/util/List;

    invoke-interface {v14, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_15d

    .line 306
    invoke-static {v13}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v1

    .line 307
    .restart local v1       #account:Lcom/google/android/accounts/Account;
    if-eqz v1, :cond_13d

    .line 308
    invoke-virtual {v13}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    .line 309
    .restart local v10       #streamId:Ljava/lang/String;
    sget-object v14, Lcom/google/android/apps/reader/preference/ReaderPreference;->READ_ITEMS_VISIBLE:Lcom/google/android/apps/reader/preference/ReaderPreference;

    invoke-virtual {v14, v5, v1}, Lcom/google/android/apps/reader/preference/ReaderPreference;->getBoolean(Landroid/content/Context;Lcom/google/android/accounts/Account;)Z

    move-result v14

    if-nez v14, :cond_13b

    invoke-static {v10}, Lcom/google/android/apps/reader/provider/ReaderStream;->getReadItemsVisible(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_13b

    const/4 v6, 0x1

    .line 311
    .local v6, excludeRead:Z
    :goto_12d
    invoke-static {v5, v1, v10}, Lcom/google/android/apps/reader/preference/StreamPreferences;->getRanking(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 312
    .local v8, ranking:Ljava/lang/String;
    invoke-static {v1, v10, v6, v8}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto :goto_c3

    .line 309
    .end local v6           #excludeRead:Z
    .end local v8           #ranking:Ljava/lang/String;
    :cond_13b
    const/4 v6, 0x0

    goto :goto_12d

    .line 314
    .end local v10           #streamId:Ljava/lang/String;
    :cond_13d
    const-string v14, "StreamFragment"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Account was not specified: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto/16 :goto_c3

    .line 318
    .end local v1           #account:Lcom/google/android/accounts/Account;
    :cond_15d
    const-string v14, "StreamFragment"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Unsupported content type: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto/16 :goto_c3

    .line 321
    :cond_17d
    const-string v14, "http"

    invoke-virtual {v14, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1ac

    .line 322
    invoke-static {v5}, Lcom/google/android/apps/reader/preference/LocalPreferences;->getAccount(Landroid/content/Context;)Lcom/google/android/accounts/Account;

    move-result-object v1

    .line 323
    .restart local v1       #account:Lcom/google/android/accounts/Account;
    if-eqz v1, :cond_19d

    .line 324
    invoke-static {v13}, Lcom/google/android/apps/reader/provider/ReaderStream;->createSubscriptionId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    .line 325
    .restart local v10       #streamId:Ljava/lang/String;
    const/4 v6, 0x0

    .line 326
    .restart local v6       #excludeRead:Z
    const-string v8, "newest"

    .line 327
    .restart local v8       #ranking:Ljava/lang/String;
    invoke-static {v1, v10, v6, v8}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto/16 :goto_c3

    .line 330
    .end local v6           #excludeRead:Z
    .end local v8           #ranking:Ljava/lang/String;
    .end local v10           #streamId:Ljava/lang/String;
    :cond_19d
    const-string v14, "StreamFragment"

    const-string v15, "Account not selected"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto/16 :goto_c3

    .line 334
    .end local v1           #account:Lcom/google/android/accounts/Account;
    :cond_1ac
    const-string v14, "StreamFragment"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Unsupported intent data: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto/16 :goto_c3
.end method

.method public getAccount()Lcom/google/android/accounts/Account;
    .registers 2

    .prologue
    .line 712
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getItemsUri()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getStreamId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 716
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getItemsUri()Landroid/net/Uri;

    move-result-object v0

    .line 717
    .local v0, uri:Landroid/net/Uri;
    if-eqz v0, :cond_b

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    :goto_a
    return-object v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public getStreamTitle()Ljava/lang/CharSequence;
    .registers 6

    .prologue
    const/high16 v4, 0x7f0d

    .line 689
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/widget/StreamAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 690
    .local v0, cursor:Landroid/database/Cursor;
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    if-eqz v3, :cond_f

    .line 691
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    .line 703
    :cond_e
    :goto_e
    return-object v2

    .line 692
    :cond_f
    if-eqz v0, :cond_22

    .line 695
    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 696
    .local v1, extras:Landroid/os/Bundle;
    const-string v3, "android.intent.extra.TITLE"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 697
    .local v2, title:Ljava/lang/CharSequence;
    if-nez v2, :cond_e

    .line 700
    invoke-virtual {p0, v4}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_e

    .line 703
    .end local v1           #extras:Landroid/os/Bundle;
    .end local v2           #title:Ljava/lang/CharSequence;
    :cond_22
    invoke-virtual {p0, v4}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_e
.end method

.method public highlight(J)V
    .registers 9
    .parameter "item"

    .prologue
    const/4 v5, 0x1

    .line 742
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v0

    .line 743
    .local v0, choiceMode:I
    if-eq v0, v5, :cond_e

    .line 744
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 746
    :cond_e
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v3

    .line 747
    .local v3, start:I
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v4, p1, p2, v3}, Lcom/google/android/apps/reader/widget/StreamAdapter;->findItem(JI)Landroid/database/Cursor;

    move-result-object v1

    .line 748
    .local v1, cursor:Landroid/database/Cursor;
    if-eqz v1, :cond_2e

    .line 749
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 750
    .local v2, position:I
    if-ne v2, v3, :cond_23

    .line 762
    .end local v2           #position:I
    :goto_22
    return-void

    .line 757
    .restart local v2       #position:I
    :cond_23
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v2, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 761
    .end local v2           #position:I
    :goto_28
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItemSelector:Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;->setSelectedItemId(J)V

    goto :goto_22

    .line 759
    :cond_2e
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->clearChoices()V

    goto :goto_28
.end method

.method public isLoaded()Z
    .registers 2

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/StreamAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 10
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v3, -0x1

    .line 411
    packed-switch p1, :pswitch_data_62

    .line 448
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 450
    :cond_7
    :goto_7
    return-void

    .line 413
    :pswitch_8
    if-ne p2, v3, :cond_7

    .line 417
    if-nez p3, :cond_14

    .line 418
    const-string v3, "StreamFragment"

    const-string v4, "Result data was null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 421
    :cond_14
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 422
    .local v0, contentUri:Landroid/net/Uri;
    if-nez v0, :cond_22

    .line 423
    const-string v3, "StreamFragment"

    const-string v4, "Result had no content URI"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 426
    :cond_22
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v1

    .line 427
    .local v1, itemId:J
    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_45

    .line 428
    const-string v3, "StreamFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected content URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 432
    :cond_45
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItemSelector:Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;->setSelectedItemId(J)V

    goto :goto_7

    .line 436
    .end local v0           #contentUri:Landroid/net/Uri;
    .end local v1           #itemId:J
    :pswitch_4b
    if-ne p2, v3, :cond_7

    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/reader/fragment/MainScreen;->show(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    goto :goto_7

    .line 443
    :pswitch_59
    if-ne p2, v3, :cond_7

    .line 444
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/widget/StreamAdapter;->showAllAsRead()V

    goto :goto_7

    .line 411
    nop

    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_8
        :pswitch_4b
        :pswitch_4b
        :pswitch_59
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    .line 889
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_e

    .line 894
    :goto_7
    return-void

    .line 891
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/Loadable;->retry()V

    goto :goto_7

    .line 889
    :pswitch_data_e
    .packed-switch 0x7f0b002f
        :pswitch_8
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/StreamAdapter;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 5
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 609
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/reader/widget/StreamAdapter;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 611
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 5
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 859
    sget-boolean v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    if-eq v0, p1, :cond_d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 860
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/widget/StreamAdapter;->createLoader(Landroid/net/Uri;)Landroid/support/v4/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 15
    .parameter "menu"
    .parameter "inflater"

    .prologue
    const v11, 0x7f0b0092

    const v10, 0x7f0b0072

    const v9, 0x7f0b0071

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 481
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 482
    const v5, 0x7f100009

    invoke-virtual {p2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 483
    const v5, 0x7f10000f

    invoke-virtual {p2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 484
    iget-boolean v5, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mFolder:Z

    if-eqz v5, :cond_b2

    const v5, 0x7f100004

    :goto_21
    invoke-virtual {p2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 485
    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v5, :cond_b1

    .line 486
    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 487
    .local v1, prefs:Landroid/content/SharedPreferences;
    if-eqz v1, :cond_bd

    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->isSearch()Z

    move-result v5

    if-nez v5, :cond_bd

    .line 488
    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v5}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 489
    .local v3, streamId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderStream;->isSubscription(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_48

    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderStream;->isWebFeed(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b7

    :cond_48
    move v4, v7

    .line 491
    .local v4, subscription:Z
    :goto_49
    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderStream;->isLabel(Ljava/lang/String;)Z

    move-result v0

    .line 492
    .local v0, label:Z
    sget-object v5, Lcom/google/android/apps/reader/preference/ReaderPreference;->READ_ITEMS_VISIBLE:Lcom/google/android/apps/reader/preference/ReaderPreference;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/reader/preference/ReaderPreference;->getBoolean(Landroid/content/SharedPreferences;)Z

    move-result v2

    .line 493
    .local v2, readItemsVisible:Z
    const v5, 0x7f0b0094

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 494
    const v5, 0x7f0b0093

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    if-nez v2, :cond_b9

    move v5, v7

    :goto_67
    invoke-interface {v8, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 495
    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 496
    const v5, 0x7f0b0095

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 497
    const v5, 0x7f0b009c

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v4, :cond_bb

    iget-object v8, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    invoke-virtual {v8}, Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;->isPanelVisible()Z

    move-result v8

    if-nez v8, :cond_bb

    :goto_8c
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 499
    const v5, 0x7f0b009d

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 500
    const v5, 0x7f0b009e

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 501
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 502
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 515
    .end local v0           #label:Z
    .end local v1           #prefs:Landroid/content/SharedPreferences;
    .end local v2           #readItemsVisible:Z
    .end local v3           #streamId:Ljava/lang/String;
    .end local v4           #subscription:Z
    :cond_b1
    :goto_b1
    return-void

    .line 484
    :cond_b2
    const v5, 0x7f100011

    goto/16 :goto_21

    .restart local v1       #prefs:Landroid/content/SharedPreferences;
    .restart local v3       #streamId:Ljava/lang/String;
    :cond_b7
    move v4, v6

    .line 489
    goto :goto_49

    .restart local v0       #label:Z
    .restart local v2       #readItemsVisible:Z
    .restart local v4       #subscription:Z
    :cond_b9
    move v5, v6

    .line 494
    goto :goto_67

    :cond_bb
    move v7, v6

    .line 497
    goto :goto_8c

    .line 504
    .end local v0           #label:Z
    .end local v2           #readItemsVisible:Z
    .end local v3           #streamId:Ljava/lang/String;
    .end local v4           #subscription:Z
    :cond_bd
    const v5, 0x7f0b0094

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 505
    const v5, 0x7f0b0093

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 506
    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 507
    const v5, 0x7f0b0095

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 508
    const v5, 0x7f0b009c

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 509
    const v5, 0x7f0b009d

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 510
    const v5, 0x7f0b009e

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 511
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 512
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_b1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .parameter "inflater"
    .parameter "root"
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 140
    const v2, 0x7f03001c

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 142
    .local v1, view:Landroid/view/View;
    const v2, 0x7f0b0030

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mEmpty:Landroid/widget/TextView;

    .line 143
    const v2, 0x7f0b003c

    invoke-direct {p0, v2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    .line 144
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    new-instance v5, Lcom/google/android/apps/reader/fragment/StreamFragment$SubscribedDataSetObserver;

    invoke-direct {v5, p0, v6}, Lcom/google/android/apps/reader/fragment/StreamFragment$SubscribedDataSetObserver;-><init>(Lcom/google/android/apps/reader/fragment/StreamFragment;Lcom/google/android/apps/reader/fragment/StreamFragment$1;)V

    invoke-virtual {v2, v5}, Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 145
    const v2, 0x7f0b003e

    invoke-direct {p0, v2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    .line 146
    const v2, 0x7f0b003f

    invoke-direct {p0, v2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mStreamPreferences:Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;

    .line 147
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->setOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mStreamPreferences:Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;->setOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 150
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 151
    .local v0, context:Landroid/content/Context;
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    .line 152
    new-instance v2, Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-direct {v2, v0}, Lcom/google/android/apps/reader/widget/StreamAdapter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    new-instance v5, Lcom/google/android/apps/reader/fragment/StreamFragment$StreamObserver;

    invoke-direct {v5, p0, v6}, Lcom/google/android/apps/reader/fragment/StreamFragment$StreamObserver;-><init>(Lcom/google/android/apps/reader/fragment/StreamFragment;Lcom/google/android/apps/reader/fragment/StreamFragment$1;)V

    invoke-virtual {v2, v5}, Lcom/google/android/apps/reader/widget/StreamAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 154
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    new-instance v5, Lcom/google/android/apps/reader/fragment/ListDecorator;

    iget-object v6, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-direct {v5, v6, p0}, Lcom/google/android/apps/reader/fragment/ListDecorator;-><init>(Landroid/widget/ListAdapter;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 156
    new-instance v2, Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->callbacks(Landroid/view/View;)Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    move-result-object v6

    invoke-direct {v2, v0, v5, v3, v6}, Lcom/google/android/apps/reader/fragment/Loadable;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;ILandroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    new-instance v5, Lcom/google/android/apps/reader/fragment/ListScrollObserver;

    iget-object v6, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-direct {v5, v6}, Lcom/google/android/apps/reader/fragment/ListScrollObserver;-><init>(Lcom/google/android/apps/reader/fragment/Loadable;)V

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 161
    new-instance v2, Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;

    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-direct {v2, v5, v6}, Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;-><init>(Landroid/widget/ListView;Lcom/google/android/apps/reader/widget/StreamAdapter;)V

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItemSelector:Lcom/google/android/apps/reader/fragment/StreamFragment$ItemSelector;

    .line 163
    invoke-virtual {p0, v3}, Lcom/google/android/apps/reader/fragment/StreamFragment;->setHasOptionsMenu(Z)V

    .line 165
    if-eqz p3, :cond_bb

    .line 166
    const-string v2, "reader:uri"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    .line 167
    const-string v2, "reader:title"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    .line 168
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->handleUriChanged()V

    .line 170
    :cond_bb
    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_c6

    move v2, v3

    :goto_c2
    invoke-virtual {v5, v2}, Lcom/google/android/apps/reader/fragment/Loadable;->initLoaderIf(Z)V

    .line 172
    return-object v1

    :cond_c6
    move v2, v4

    .line 170
    goto :goto_c2
.end method

.method public onDestroyView()V
    .registers 3

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/widget/StreamAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 180
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroyView()V

    .line 181
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 462
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    .line 463
    .local v1, item:Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/widget/StreamAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 464
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v1, :cond_1a

    if-ne v1, v0, :cond_1a

    .line 465
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v2, v0, p1, p2}, Lcom/google/android/apps/reader/widget/StreamAdapter;->onKeyDown(Landroid/database/Cursor;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 466
    const/4 v2, 0x1

    .line 475
    :goto_19
    return v2

    .line 469
    :cond_1a
    packed-switch p1, :pswitch_data_2e

    .line 475
    const/4 v2, 0x0

    goto :goto_19

    .line 471
    :pswitch_1f
    const/16 v2, 0x14

    invoke-direct {p0, v2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->simulateKeyDown(I)Z

    move-result v2

    goto :goto_19

    .line 473
    :pswitch_26
    const/16 v2, 0x13

    invoke-direct {p0, v2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->simulateKeyDown(I)Z

    move-result v2

    goto :goto_19

    .line 469
    nop

    :pswitch_data_2e
    .packed-switch 0x26
        :pswitch_1f
        :pswitch_26
    .end packed-switch
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 11
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    .prologue
    .line 376
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    .line 377
    .local v2, item:Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/widget/StreamAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 378
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v2, :cond_37

    if-ne v2, v0, :cond_37

    .line 379
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/reader/widget/StreamAdapter;->newViewItemInStreamIntent(Landroid/database/Cursor;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 380
    .local v1, intent:Landroid/content/Intent;
    if-eqz v1, :cond_37

    .line 381
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    if-eqz v3, :cond_23

    .line 382
    const-string v3, "android.intent.extra.TITLE"

    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 384
    :cond_23
    const-string v3, "com.google.reader.intent.extra.SUBSCRIBE_DECLINED"

    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mNotSubscribed:Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/fragment/NotSubscribedFragment;->getDeclined()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 385
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->hasItemFragment()Z

    move-result v3

    if-eqz v3, :cond_38

    .line 392
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->startActivity(Landroid/content/Intent;)V

    .line 402
    .end local v1           #intent:Landroid/content/Intent;
    :cond_37
    :goto_37
    return-void

    .line 398
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_38
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/reader/fragment/StreamFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_37
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 5
    .parameter
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 867
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-boolean v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_11

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    if-eq v0, v1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 868
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/reader/widget/StreamAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 869
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

    if-eqz v0, :cond_1f

    .line 870
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

    invoke-interface {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;->onStreamLoaded()V

    .line 872
    :cond_1f
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 80
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 878
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-boolean v0, Lcom/google/android/apps/reader/fragment/StreamFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_11

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    if-eq v0, v1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 879
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/widget/StreamAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 880
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

    if-eqz v0, :cond_20

    .line 881
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

    invoke-interface {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;->onStreamUnloaded()V

    .line 883
    :cond_20
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 519
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_44

    .line 552
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_c
    return v0

    .line 521
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->getConfirmMarkAsRead()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 522
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->confirmMarkAllAsRead()V

    goto :goto_c

    .line 524
    :cond_19
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/widget/StreamAdapter;->markAllAsRead()V

    goto :goto_c

    .line 528
    :sswitch_1f
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->setReadItemsVisiblePreference(Z)Z

    goto :goto_c

    .line 531
    :sswitch_26
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->setReadItemsVisiblePreference(Z)Z

    goto :goto_c

    .line 534
    :sswitch_2c
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->showRankingChooser()V

    goto :goto_c

    .line 537
    :sswitch_30
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->showDeleteConfirmation()V

    goto :goto_c

    .line 540
    :sswitch_34
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->renameTag()V

    goto :goto_c

    .line 543
    :sswitch_38
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeFolders()V

    goto :goto_c

    .line 546
    :sswitch_3c
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->unsubscribe()V

    goto :goto_c

    .line 549
    :sswitch_40
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->renameSubscription()V

    goto :goto_c

    .line 519
    :sswitch_data_44
    .sparse-switch
        0x7f0b0071 -> :sswitch_34
        0x7f0b0072 -> :sswitch_30
        0x7f0b0092 -> :sswitch_d
        0x7f0b0093 -> :sswitch_26
        0x7f0b0094 -> :sswitch_1f
        0x7f0b0095 -> :sswitch_2c
        0x7f0b009c -> :sswitch_3c
        0x7f0b009d -> :sswitch_40
        0x7f0b009e -> :sswitch_38
    .end sparse-switch
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/StreamAdapter;->syncChanges()Z

    .line 216
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 217
    return-void
.end method

.method public onResume()V
    .registers 1

    .prologue
    .line 196
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->workAroundLoaderBug()V

    .line 198
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 221
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 222
    const-string v0, "reader:uri"

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 223
    const-string v0, "reader:title"

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 224
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 7
    .parameter "prefs"
    .parameter "key"

    .prologue
    .line 230
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mReaderPreferences:Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/fragment/ReaderPreferencesFragment;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    if-ne p1, v3, :cond_31

    .line 231
    sget-object v3, Lcom/google/android/apps/reader/preference/ReaderPreference;->READ_ITEMS_VISIBLE:Lcom/google/android/apps/reader/preference/ReaderPreference;

    invoke-virtual {v3, p2}, Lcom/google/android/apps/reader/preference/ReaderPreference;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 232
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v3, :cond_2e

    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2e

    .line 233
    sget-object v3, Lcom/google/android/apps/reader/preference/ReaderPreference;->READ_ITEMS_VISIBLE:Lcom/google/android/apps/reader/preference/ReaderPreference;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/reader/preference/ReaderPreference;->getBoolean(Landroid/content/SharedPreferences;)Z

    move-result v3

    if-nez v3, :cond_2f

    const/4 v0, 0x1

    .line 234
    .local v0, excludeRead:Z
    :goto_25
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v3, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->setExcludeRead(Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    .line 246
    .end local v0           #excludeRead:Z
    :cond_2e
    :goto_2e
    return-void

    .line 233
    :cond_2f
    const/4 v0, 0x0

    goto :goto_25

    .line 237
    :cond_31
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mStreamPreferences:Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/fragment/StreamPreferencesFragment;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    if-ne p1, v3, :cond_2e

    .line 238
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v3, :cond_2e

    .line 239
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 240
    .local v2, streamId:Ljava/lang/String;
    if-eqz v2, :cond_2e

    .line 241
    invoke-static {p1, v2}, Lcom/google/android/apps/reader/preference/StreamPreferences;->getRanking(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, ranking:Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v3, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->setRanking(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeUri(Landroid/net/Uri;)V

    goto :goto_2e
.end method

.method public refresh()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 630
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_15

    .line 631
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getExcludeTarget(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_16

    const/4 v0, 0x1

    .line 632
    .local v0, showAll:Z
    :goto_e
    if-eqz v0, :cond_18

    .line 634
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/Loadable;->refresh()V

    .line 641
    .end local v0           #showAll:Z
    :cond_15
    :goto_15
    return-void

    :cond_16
    move v0, v1

    .line 631
    goto :goto_e

    .line 638
    .restart local v0       #showAll:Z
    :cond_18
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/reader/fragment/Loadable;->refresh(I)V

    goto :goto_15
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/StreamAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 368
    return-void
.end method

.method public resetList()V
    .registers 3

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setStackFromBottom(Z)V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setStackFromBottom(Z)V

    .line 343
    return-void
.end method

.method public setObserver(Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;)V
    .registers 2
    .parameter "observer"

    .prologue
    .line 363
    iput-object p1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mObserver:Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;

    .line 364
    return-void
.end method

.method public setRefreshBeforeLoading(Z)V
    .registers 2
    .parameter "refreshBeforeLoading"

    .prologue
    .line 626
    iput-boolean p1, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mRefreshBeforeLoading:Z

    .line 627
    return-void
.end method

.method public showMainScreen()V
    .registers 3

    .prologue
    .line 765
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/fragment/MainScreen;->show(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 766
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/StreamFragment;->mAdapter:Lcom/google/android/apps/reader/widget/StreamAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/StreamAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 372
    return-void
.end method
