.class public Lcom/google/android/apps/reader/widget/ReaderWindow;
.super Ljava/lang/Object;
.source "ReaderWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/widget/ReaderWindow$LoaderCallbacksObserver;
    }
.end annotation


# static fields
.field public static final ANDROID_R_ID_HOME:I = 0x102002c

#the value of this static final field might be set in the static constructor
.field private static final HAS_ACTION_BAR:Z = false

.field private static final HONEYCOMB:I = 0xb

.field private static final ICE_CREAM_SANDWICH:I = 0xe

#the value of this static final field might be set in the static constructor
.field private static final SDK:I = 0x0

.field public static final SERVICE_NAME:Ljava/lang/String; = "reader_window"


# instance fields
.field private final mActiveLoaders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mActivity:Landroid/app/Activity;

.field private final mWindow:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/google/android/apps/reader/widget/ReaderWindow;->SDK:I

    .line 58
    sget v0, Lcom/google/android/apps/reader/widget/ReaderWindow;->SDK:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_b
    sput-boolean v0, Lcom/google/android/apps/reader/widget/ReaderWindow;->HAS_ACTION_BAR:Z

    return-void

    :cond_e
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .registers 3
    .parameter "activity"

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mActiveLoaders:Ljava/util/Set;

    .line 73
    if-nez p1, :cond_12

    .line 74
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 76
    :cond_12
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mActivity:Landroid/app/Activity;

    .line 77
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mWindow:Landroid/view/Window;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/reader/widget/ReaderWindow;)Ljava/util/Set;
    .registers 2
    .parameter "x0"

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mActiveLoaders:Ljava/util/Set;

    return-object v0
.end method

.method private static clearAnimation(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 190
    return-void
.end method

.method public static from(Landroid/content/Context;)Lcom/google/android/apps/reader/widget/ReaderWindow;
    .registers 2
    .parameter "context"

    .prologue
    .line 89
    const-string v0, "reader_window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/reader/widget/ReaderWindow;

    return-object v0
.end method

.method public static invalidateOptionsMenu(Landroid/support/v4/app/FragmentActivity;)V
    .registers 6
    .parameter "activity"

    .prologue
    .line 194
    :try_start_0
    const-class v2, Landroid/support/v4/app/FragmentActivity;

    const-string v3, "supportInvalidateOptionsMenu"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 195
    .local v0, m:Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 196
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_15} :catch_16

    .line 200
    .end local v0           #m:Ljava/lang/reflect/Method;
    :goto_15
    return-void

    .line 197
    :catch_16
    move-exception v1

    .line 198
    .local v1, t:Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_15
.end method

.method public static setDisplayHomeAsUpEnabled(Landroid/app/Activity;Z)V
    .registers 11
    .parameter "activity"
    .parameter "showHomeAsUp"

    .prologue
    .line 206
    sget v5, Lcom/google/android/apps/reader/widget/ReaderWindow;->SDK:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_37

    .line 208
    :try_start_6
    const-class v5, Landroid/app/Activity;

    const-string v6, "getActionBar"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 209
    .local v2, getActionBar:Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 210
    .local v0, actionBar:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v1

    .line 211
    .local v1, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-string v5, "setDisplayHomeAsUpEnabled"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 212
    .local v3, set:Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_37} :catch_38

    .line 220
    .end local v0           #actionBar:Ljava/lang/Object;
    .end local v1           #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #getActionBar:Ljava/lang/reflect/Method;
    .end local v3           #set:Ljava/lang/reflect/Method;
    :cond_37
    :goto_37
    return-void

    .line 213
    :catch_38
    move-exception v4

    .line 215
    .local v4, t:Ljava/lang/Throwable;
    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_37
.end method

.method public static setHomeButtonEnabled(Landroid/app/Activity;Z)V
    .registers 11
    .parameter "activity"
    .parameter "enabled"

    .prologue
    .line 226
    sget v5, Lcom/google/android/apps/reader/widget/ReaderWindow;->SDK:I

    const/16 v6, 0xe

    if-lt v5, v6, :cond_3d

    .line 228
    :try_start_6
    const-class v5, Landroid/app/Activity;

    const-string v6, "getActionBar"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 229
    .local v2, getActionBar:Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 230
    .local v0, actionBar:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v1

    .line 231
    .local v1, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-string v5, "setHomeButtonEnabled"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 232
    .local v3, set:Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_37} :catch_38

    .line 242
    .end local v0           #actionBar:Ljava/lang/Object;
    .end local v1           #cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #getActionBar:Ljava/lang/reflect/Method;
    .end local v3           #set:Ljava/lang/reflect/Method;
    :goto_37
    return-void

    .line 233
    :catch_38
    move-exception v4

    .line 235
    .local v4, t:Ljava/lang/Throwable;
    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_37

    .line 237
    .end local v4           #t:Ljava/lang/Throwable;
    :cond_3d
    sget v5, Lcom/google/android/apps/reader/widget/ReaderWindow;->SDK:I

    goto :goto_37
.end method

.method public static setProgress(Landroid/app/Activity;I)V
    .registers 6
    .parameter "activity"
    .parameter "value"

    .prologue
    .line 133
    sget v2, Lcom/google/android/apps/reader/widget/ReaderWindow;->SDK:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_15

    .line 134
    const v2, 0x7f0b0056

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/reader/widget/ProgressView;

    .line 135
    .local v1, progressView:Lcom/google/android/apps/reader/widget/ProgressView;
    if-eqz v1, :cond_14

    .line 136
    invoke-static {v1, p1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setProgress(Lcom/google/android/apps/reader/widget/ProgressView;I)V

    .line 147
    .end local v1           #progressView:Lcom/google/android/apps/reader/widget/ProgressView;
    :cond_14
    :goto_14
    return-void

    .line 139
    :cond_15
    const v2, 0x7f0b0002

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 141
    .local v0, progressBar:Landroid/widget/ProgressBar;
    if-eqz v0, :cond_24

    .line 142
    invoke-static {v0, p1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setProgress(Landroid/widget/ProgressBar;I)V

    goto :goto_14

    .line 144
    :cond_24
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3, p1}, Landroid/view/Window;->setFeatureInt(II)V

    goto :goto_14
.end method

.method private static setProgress(Landroid/widget/ProgressBar;I)V
    .registers 5
    .parameter "progressBar"
    .parameter "value"

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v2

    if-eq p1, v2, :cond_13

    const/4 v0, 0x1

    .line 168
    .local v0, changed:Z
    :goto_8
    invoke-virtual {p0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 170
    const/16 v2, 0x2710

    if-ge p1, v2, :cond_15

    .line 171
    invoke-virtual {p0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 180
    :goto_12
    return-void

    .end local v0           #changed:Z
    :cond_13
    move v0, v1

    .line 167
    goto :goto_8

    .line 173
    .restart local v0       #changed:Z
    :cond_15
    if-eqz v0, :cond_23

    .line 174
    const v1, 0x10a0001

    invoke-static {p0, v1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->startAnimation(Landroid/view/View;I)V

    .line 178
    :goto_1d
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_12

    .line 176
    :cond_23
    invoke-static {p0}, Lcom/google/android/apps/reader/widget/ReaderWindow;->clearAnimation(Landroid/view/View;)V

    goto :goto_1d
.end method

.method private static setProgress(Lcom/google/android/apps/reader/widget/ProgressView;I)V
    .registers 5
    .parameter "progressView"
    .parameter "value"

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ProgressView;->getProgress()I

    move-result v2

    if-eq p1, v2, :cond_16

    const/4 v0, 0x1

    .line 151
    .local v0, changed:Z
    :goto_8
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/ProgressView;->setProgress(I)V

    .line 153
    const/16 v2, 0x2710

    if-ge p1, v2, :cond_18

    .line 154
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/ProgressView;->setVisibility(I)V

    .line 163
    :goto_12
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/ProgressView;->setProgress(I)V

    .line 164
    return-void

    .end local v0           #changed:Z
    :cond_16
    move v0, v1

    .line 150
    goto :goto_8

    .line 156
    .restart local v0       #changed:Z
    :cond_18
    if-eqz v0, :cond_26

    .line 157
    const v1, 0x10a0001

    invoke-static {p0, v1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->startAnimation(Landroid/view/View;I)V

    .line 161
    :goto_20
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/ProgressView;->setVisibility(I)V

    goto :goto_12

    .line 159
    :cond_26
    invoke-static {p0}, Lcom/google/android/apps/reader/widget/ReaderWindow;->clearAnimation(Landroid/view/View;)V

    goto :goto_20
.end method

.method private static startAnimation(Landroid/view/View;I)V
    .registers 4
    .parameter "view"
    .parameter "resId"

    .prologue
    .line 183
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 184
    .local v0, context:Landroid/content/Context;
    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 185
    .local v1, fadeOut:Landroid/view/animation/Animation;
    invoke-virtual {p0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 186
    return-void
.end method


# virtual methods
.method public observe(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .registers 4
    .parameter "fragment"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;)",
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    .local p2, callbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;,"Landroid/support/v4/app/LoaderManager$LoaderCallbacks<Landroid/database/Cursor;>;"
    new-instance v0, Lcom/google/android/apps/reader/widget/ReaderWindow$LoaderCallbacksObserver;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/apps/reader/widget/ReaderWindow$LoaderCallbacksObserver;-><init>(Lcom/google/android/apps/reader/widget/ReaderWindow;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method

.method public requestCustomTitle()V
    .registers 3

    .prologue
    .line 97
    sget-boolean v0, Lcom/google/android/apps/reader/widget/ReaderWindow;->HAS_ACTION_BAR:Z

    if-nez v0, :cond_a

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mWindow:Landroid/view/Window;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 100
    :cond_a
    return-void
.end method

.method public setCustomTitleLayout(I)V
    .registers 4
    .parameter "layout"

    .prologue
    .line 106
    sget-boolean v0, Lcom/google/android/apps/reader/widget/ReaderWindow;->HAS_ACTION_BAR:Z

    if-nez v0, :cond_a

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mWindow:Landroid/view/Window;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Landroid/view/Window;->setFeatureInt(II)V

    .line 109
    :cond_a
    return-void
.end method

.method public setHomeButtonEnabled(Z)V
    .registers 3
    .parameter "enabled"

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setHomeButtonEnabled(Landroid/app/Activity;Z)V

    .line 249
    return-void
.end method

.method public setProgress(I)V
    .registers 3
    .parameter "value"

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setProgress(Landroid/app/Activity;I)V

    .line 130
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "title"

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mWindow:Landroid/view/Window;

    const v2, 0x1020016

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 121
    .local v0, titleView:Landroid/widget/TextView;
    if-eqz v0, :cond_11

    .line 122
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_10
    return-void

    .line 124
    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mWindow:Landroid/view/Window;

    invoke-virtual {v1, p1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_10
.end method

.method updateIndeterminateProgress()V
    .registers 8

    .prologue
    const/4 v4, 0x0

    .line 253
    iget-object v5, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mActiveLoaders:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    if-eqz v5, :cond_1b

    const/4 v3, 0x1

    .line 257
    .local v3, visible:Z
    :goto_a
    iget-object v5, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mWindow:Landroid/view/Window;

    const v6, 0x7f0b0001

    invoke-virtual {v5, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 258
    .local v1, progress:Landroid/view/View;
    if-eqz v1, :cond_20

    .line 259
    if-eqz v3, :cond_1d

    :goto_17
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 265
    :goto_1a
    return-void

    .end local v1           #progress:Landroid/view/View;
    .end local v3           #visible:Z
    :cond_1b
    move v3, v4

    .line 253
    goto :goto_a

    .line 259
    .restart local v1       #progress:Landroid/view/View;
    .restart local v3       #visible:Z
    :cond_1d
    const/16 v4, 0x8

    goto :goto_17

    .line 261
    :cond_20
    const/4 v0, 0x5

    .line 262
    .local v0, featureId:I
    if-eqz v3, :cond_2a

    const/4 v2, -0x1

    .line 263
    .local v2, value:I
    :goto_24
    iget-object v4, p0, Lcom/google/android/apps/reader/widget/ReaderWindow;->mWindow:Landroid/view/Window;

    invoke-virtual {v4, v0, v2}, Landroid/view/Window;->setFeatureInt(II)V

    goto :goto_1a

    .line 262
    .end local v2           #value:I
    :cond_2a
    const/4 v2, -0x2

    goto :goto_24
.end method
