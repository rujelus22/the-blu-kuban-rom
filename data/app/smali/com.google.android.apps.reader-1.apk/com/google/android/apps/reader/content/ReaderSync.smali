.class public Lcom/google/android/apps/reader/content/ReaderSync;
.super Ljava/lang/Object;
.source "ReaderSync.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/content/ReaderSync$PendingActionsQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION_SYNC_LOCK:Ljava/lang/Object; = null

.field private static KEEP_EDITS:Z = false

.field public static final MODE_DEFAULT:I = 0x0

.field public static final MODE_PREFERENCES_ONLY:I = 0x4

.field public static final MODE_UPLOAD_ONLY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ReaderSync"


# instance fields
.field protected final mAccount:Lcom/google/android/accounts/Account;

.field protected final mConfig:Lcom/google/android/apps/reader/util/Config;

.field protected final mContentLoader:Lcom/google/android/apps/reader/content/ContentLoader;

.field protected final mContext:Landroid/content/Context;

.field protected final mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mDownloadPreferences:Z

.field private mDownloadRecentStreams:Z

.field private mDownloadSubscriptions:Z

.field private mDownloadTags:Z

.field private mDownloadUnreadCounts:Z

.field private final mPostHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;

.field private final mPreferencesHandler:Ljava/net/ContentHandler;

.field protected final mReaderPreferences:Landroid/content/SharedPreferences;

.field private final mStreamPreferencesHandler:Ljava/net/ContentHandler;

.field private mSyncStart:J

.field private mToken:Ljava/lang/String;

.field private final mTokenHandler:Ljava/net/ContentHandler;

.field private mUploadPendingActions:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 86
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/reader/content/ReaderSync;->KEEP_EDITS:Z

    .line 94
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/reader/content/ReaderSync;->ACTION_SYNC_LOCK:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Lcom/google/android/apps/reader/content/ContentLoader;Ljava/net/ContentHandler;Ljava/net/ContentHandler;Ljava/net/ContentHandler;Lcom/google/android/apps/reader/net/HttpContentHandler;)V
    .registers 11
    .parameter "context"
    .parameter "db"
    .parameter "account"
    .parameter "contentLoader"
    .parameter "tokenHandler"
    .parameter "preferencesHandler"
    .parameter "streamPreferencesHandler"
    .parameter "httpPostHandler"

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    if-nez p1, :cond_d

    .line 167
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_d
    if-nez p2, :cond_17

    .line 170
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Database is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_17
    if-nez p3, :cond_21

    .line 173
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Account is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_21
    if-nez p4, :cond_2b

    .line 176
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Feed resolver factory is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_2b
    if-nez p5, :cond_35

    .line 179
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Token handler is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_35
    if-nez p6, :cond_3f

    .line 182
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Preferences handler is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_3f
    if-nez p7, :cond_49

    .line 185
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream preferences handler is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_49
    if-nez p8, :cond_53

    .line 188
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "HTTP POST handler is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_53
    iput-object p1, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mContext:Landroid/content/Context;

    .line 191
    invoke-static {p1}, Lcom/google/android/apps/reader/util/Config;->get(Landroid/content/Context;)Lcom/google/android/apps/reader/util/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mConfig:Lcom/google/android/apps/reader/util/Config;

    .line 192
    iput-object p2, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 193
    iput-object p3, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    .line 194
    iput-object p4, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mContentLoader:Lcom/google/android/apps/reader/content/ContentLoader;

    .line 195
    invoke-static {p1, p3}, Lcom/google/android/apps/reader/preference/ReaderPreference;->getSharedPreferences(Landroid/content/Context;Lcom/google/android/accounts/Account;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mReaderPreferences:Landroid/content/SharedPreferences;

    .line 196
    iput-object p5, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mTokenHandler:Ljava/net/ContentHandler;

    .line 197
    iput-object p6, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mPreferencesHandler:Ljava/net/ContentHandler;

    .line 198
    iput-object p7, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mStreamPreferencesHandler:Ljava/net/ContentHandler;

    .line 199
    iput-object p8, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mPostHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;

    .line 200
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/content/ReaderSync;->setMode(I)V

    .line 201
    return-void
.end method

.method private commitPendingAction(J)V
    .registers 10
    .parameter "id"

    .prologue
    .line 297
    sget-boolean v4, Lcom/google/android/apps/reader/content/ReaderSync;->KEEP_EDITS:Z

    if-eqz v4, :cond_2c

    .line 298
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 299
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 300
    .local v1, values:Landroid/content/ContentValues;
    const-string v4, "committed"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 301
    const-string v3, "_id = ?"

    .line 302
    .local v3, whereClause:Ljava/lang/String;
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 303
    .local v2, whereArgs:[Ljava/lang/String;
    const-string v4, "pending_actions"

    invoke-virtual {v0, v4, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 307
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v1           #values:Landroid/content/ContentValues;
    .end local v2           #whereArgs:[Ljava/lang/String;
    .end local v3           #whereClause:Ljava/lang/String;
    :goto_2b
    return-void

    .line 305
    :cond_2c
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/content/ReaderSync;->deletePendingAction(J)V

    goto :goto_2b
.end method

.method private deletePendingAction(J)V
    .registers 8
    .parameter "id"

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 316
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "_id = ?"

    .line 317
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 318
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "pending_actions"

    invoke-virtual {v0, v3, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 319
    return-void
.end method

.method private getAccount()Lcom/google/android/accounts/Account;
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    return-object v0
.end method

.method private getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method private getToken()Ljava/lang/String;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mToken:Ljava/lang/String;

    if-nez v0, :cond_12

    .line 233
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->token()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mTokenHandler:Ljava/net/ContentHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/net/HttpContentHandler;->getContent(Landroid/net/Uri;Ljava/net/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mToken:Ljava/lang/String;

    .line 235
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method private getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method private static isExpired(J)Z
    .registers 6
    .parameter "timestamp"

    .prologue
    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, p0

    .line 110
    .local v0, age:J
    const-wide/32 v2, 0x5265c00

    cmp-long v2, v0, v2

    if-lez v2, :cond_f

    const/4 v2, 0x1

    :goto_e
    return v2

    :cond_f
    const/4 v2, 0x0

    goto :goto_e
.end method

.method private logHttpNotFound(Ljava/lang/String;)V
    .registers 5
    .parameter "streamId"

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mConfig:Lcom/google/android/apps/reader/util/Config;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/util/Config;->logd()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 120
    const-string v0, "ReaderSync"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stream not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :goto_20
    return-void

    .line 122
    :cond_21
    const-string v0, "ReaderSync"

    const-string v1, "Stream not found"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_20
.end method

.method private static now()J
    .registers 2

    .prologue
    .line 114
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private queryPendingActions()Landroid/database/Cursor;
    .registers 12

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v8

    .line 280
    .local v8, account:Lcom/google/android/accounts/Account;
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 281
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "pending_actions"

    .line 282
    .local v1, table:Ljava/lang/String;
    sget-object v2, Lcom/google/android/apps/reader/content/ReaderSync$PendingActionsQuery;->PROJECTION:[Ljava/lang/String;

    .line 283
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "account_name = ? AND committed IS NULL"

    .line 284
    .local v3, selection:Ljava/lang/String;
    const/4 v9, 0x1

    new-array v4, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, v8, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v10, v4, v9

    .line 285
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 286
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 287
    .local v6, having:Ljava/lang/String;
    const-string v7, "created ASC"

    .line 288
    .local v7, orderBy:Ljava/lang/String;
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    return-object v9
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 554
    invoke-static {}, Lcom/google/android/apps/reader/content/ReaderSync;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mSyncStart:J

    .line 555
    iget-boolean v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mUploadPendingActions:Z

    if-eqz v0, :cond_d

    .line 556
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->uploadPendingActions()V

    .line 558
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadPreferences:Z

    if-eqz v0, :cond_14

    .line 559
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadPreferences()V

    .line 561
    :cond_14
    iget-boolean v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadSubscriptions:Z

    if-eqz v0, :cond_1b

    .line 562
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadSubscriptions()V

    .line 564
    :cond_1b
    iget-boolean v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadTags:Z

    if-eqz v0, :cond_22

    .line 565
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadTags()V

    .line 567
    :cond_22
    iget-boolean v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadUnreadCounts:Z

    if-eqz v0, :cond_29

    .line 568
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadUnreadCounts()V

    .line 570
    :cond_29
    iget-boolean v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadRecentStreams:Z

    if-eqz v0, :cond_32

    .line 571
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadRecentStreams(I)V

    .line 573
    :cond_32
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final downloadContent(Landroid/net/Uri;)V
    .registers 7
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 393
    invoke-static {}, Lcom/google/android/apps/reader/content/ReaderSync;->now()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mSyncStart:J

    sub-long/2addr v1, v3

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/reader/provider/ReaderContract;->withMaxAge(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 394
    .local v0, contentUri:Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mContentLoader:Lcom/google/android/apps/reader/content/ContentLoader;

    invoke-interface {v1, v0}, Lcom/google/android/apps/reader/content/ContentLoader;->loadContent(Landroid/net/Uri;)V

    .line 395
    return-void
.end method

.method protected final downloadPreferences()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 374
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->listPreferences()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mPreferencesHandler:Ljava/net/ContentHandler;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/reader/content/ReaderSync;->get(Landroid/net/Uri;Ljava/net/ContentHandler;)V

    .line 375
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->listStreamPreferences()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mStreamPreferencesHandler:Ljava/net/ContentHandler;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/reader/content/ReaderSync;->get(Landroid/net/Uri;Ljava/net/ContentHandler;)V

    .line 376
    return-void
.end method

.method protected final downloadRecentStreams(I)V
    .registers 17
    .parameter "n"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 506
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 507
    .local v0, builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v1, "unread_counts LEFT JOIN usage ON ("

    invoke-direct {v13, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 508
    .local v13, tables:Ljava/lang/StringBuilder;
    const-string v1, "usage.account_name = unread_counts.account_name"

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    const-string v1, " AND "

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    const-string v1, "usage.stream_id = unread_counts.stream_id"

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    const-string v1, ")"

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 513
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 514
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v14, "unread_counts.stream_id"

    aput-object v14, v2, v1

    .line 515
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "unread_counts.account_name = ? AND unread_count != 0"

    .line 516
    .local v3, selection:Ljava/lang/String;
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v14, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v14, v14, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v14, v4, v1

    .line 519
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 520
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 525
    .local v6, having:Ljava/lang/String;
    const-string v7, "usage.timestamp DESC, unread_counts.stream_id LIKE \'user/%\' DESC"

    .line 527
    .local v7, sortOrder:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 528
    .local v8, limit:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 531
    .local v9, cursor:Landroid/database/Cursor;
    const/4 v11, 0x0

    .local v11, position:I
    :goto_4e
    :try_start_4e
    invoke-interface {v9, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_71

    .line 532
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_58
    .catchall {:try_start_4e .. :try_end_58} :catchall_68

    move-result-object v12

    .line 534
    .local v12, streamId:Ljava/lang/String;
    :try_start_59
    invoke-virtual {p0, v12}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadStream(Ljava/lang/String;)V
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_68
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_59 .. :try_end_5c} :catch_5f

    .line 531
    :goto_5c
    add-int/lit8 v11, v11, 0x1

    goto :goto_4e

    .line 535
    :catch_5f
    move-exception v10

    .line 536
    .local v10, e:Lorg/apache/http/client/HttpResponseException;
    :try_start_60
    invoke-virtual {v10}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    packed-switch v1, :pswitch_data_76

    .line 541
    throw v10
    :try_end_68
    .catchall {:try_start_60 .. :try_end_68} :catchall_68

    .line 546
    .end local v10           #e:Lorg/apache/http/client/HttpResponseException;
    .end local v12           #streamId:Ljava/lang/String;
    :catchall_68
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1

    .line 538
    .restart local v10       #e:Lorg/apache/http/client/HttpResponseException;
    .restart local v12       #streamId:Ljava/lang/String;
    :pswitch_6d
    :try_start_6d
    invoke-direct {p0, v12}, Lcom/google/android/apps/reader/content/ReaderSync;->logHttpNotFound(Ljava/lang/String;)V
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_68

    goto :goto_5c

    .line 546
    .end local v10           #e:Lorg/apache/http/client/HttpResponseException;
    .end local v12           #streamId:Ljava/lang/String;
    :cond_71
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 548
    return-void

    .line 536
    nop

    :pswitch_data_76
    .packed-switch 0x194
        :pswitch_6d
    .end packed-switch
.end method

.method protected final downloadStream(Ljava/lang/String;)V
    .registers 9
    .parameter "streamId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 402
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Downloading "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 403
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mContext:Landroid/content/Context;

    .line 404
    .local v1, context:Landroid/content/Context;
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    .line 405
    .local v0, account:Lcom/google/android/accounts/Account;
    sget-object v5, Lcom/google/android/apps/reader/preference/ReaderPreference;->READ_ITEMS_VISIBLE:Lcom/google/android/apps/reader/preference/ReaderPreference;

    iget-object v6, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mReaderPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/reader/preference/ReaderPreference;->getBoolean(Landroid/content/SharedPreferences;)Z

    move-result v5

    if-nez v5, :cond_2f

    const/4 v2, 0x1

    .line 406
    .local v2, excludeRead:Z
    :goto_21
    invoke-static {v1, v0, p1}, Lcom/google/android/apps/reader/preference/StreamPreferences;->getRanking(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 407
    .local v3, ranking:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {v5, p1, v2, v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 408
    .local v4, uri:Landroid/net/Uri;
    invoke-virtual {p0, v4}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadContent(Landroid/net/Uri;)V

    .line 409
    return-void

    .line 405
    .end local v2           #excludeRead:Z
    .end local v3           #ranking:Ljava/lang/String;
    .end local v4           #uri:Landroid/net/Uri;
    :cond_2f
    const/4 v2, 0x0

    goto :goto_21
.end method

.method protected final downloadSubscriptionStreams()V
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 447
    iget-object v6, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    .line 449
    .local v6, account:Lcom/google/android/accounts/Account;
    iget-object v10, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 450
    .local v0, cr:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 451
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadContent(Landroid/net/Uri;)V

    .line 452
    const/4 v10, 0x1

    new-array v2, v10, [Ljava/lang/String;

    const-string v10, "id"

    aput-object v10, v2, v11

    .line 453
    .local v2, projection:[Ljava/lang/String;
    const/4 v3, 0x0

    .line 454
    .local v3, selection:Ljava/lang/String;
    const/4 v4, 0x0

    .line 455
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 456
    .local v5, sortOrder:Ljava/lang/String;
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 458
    .local v7, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    .local v8, position:I
    :goto_1f
    :try_start_1f
    invoke-interface {v7, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v10

    if-eqz v10, :cond_30

    .line 459
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 460
    .local v9, streamId:Ljava/lang/String;
    invoke-virtual {p0, v9}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadStream(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_1f .. :try_end_2d} :catchall_34

    .line 458
    add-int/lit8 v8, v8, 0x1

    goto :goto_1f

    .line 463
    .end local v9           #streamId:Ljava/lang/String;
    :cond_30
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 465
    return-void

    .line 463
    :catchall_34
    move-exception v10

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v10
.end method

.method protected final downloadSubscriptions()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadContent(Landroid/net/Uri;)V

    .line 474
    return-void
.end method

.method protected final downloadTagStreams()V
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 419
    iget-object v6, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    .line 421
    .local v6, account:Lcom/google/android/accounts/Account;
    iget-object v10, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 422
    .local v0, cr:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 423
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadContent(Landroid/net/Uri;)V

    .line 424
    const/4 v10, 0x1

    new-array v2, v10, [Ljava/lang/String;

    const-string v10, "id"

    aput-object v10, v2, v11

    .line 425
    .local v2, projection:[Ljava/lang/String;
    const/4 v3, 0x0

    .line 426
    .local v3, selection:Ljava/lang/String;
    const/4 v4, 0x0

    .line 427
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 428
    .local v5, sortOrder:Ljava/lang/String;
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 430
    .local v7, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    .local v8, position:I
    :goto_1f
    :try_start_1f
    invoke-interface {v7, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v10

    if-eqz v10, :cond_30

    .line 431
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 432
    .local v9, streamId:Ljava/lang/String;
    invoke-virtual {p0, v9}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadStream(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_1f .. :try_end_2d} :catchall_34

    .line 430
    add-int/lit8 v8, v8, 0x1

    goto :goto_1f

    .line 435
    .end local v9           #streamId:Ljava/lang/String;
    :cond_30
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 437
    return-void

    .line 435
    :catchall_34
    move-exception v10

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v10
.end method

.method protected final downloadTags()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadContent(Landroid/net/Uri;)V

    .line 483
    return-void
.end method

.method protected final downloadUnreadCounts()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$UnreadCounts;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/content/ReaderSync;->downloadContent(Landroid/net/Uri;)V

    .line 490
    return-void
.end method

.method protected get(Landroid/net/Uri;Ljava/net/ContentHandler;)V
    .registers 3
    .parameter "uri"
    .parameter "handler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 270
    invoke-static {p1, p2}, Lcom/google/android/apps/reader/net/HttpContentHandler;->getContent(Landroid/net/Uri;Ljava/net/ContentHandler;)Ljava/lang/Object;

    .line 271
    return-void
.end method

.method protected post(Landroid/net/Uri;Lorg/apache/http/HttpEntity;)I
    .registers 6
    .parameter "uri"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 247
    if-nez p1, :cond_9

    .line 248
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 250
    :cond_9
    if-nez p2, :cond_11

    .line 251
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 253
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mPostHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;

    .line 255
    .local v0, handler:Lcom/google/android/apps/reader/net/HttpContentHandler;
    :try_start_13
    invoke-virtual {v0, p2}, Lcom/google/android/apps/reader/net/HttpContentHandler;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 256
    invoke-static {p1, v0}, Lcom/google/android/apps/reader/net/HttpContentHandler;->getResponseCode(Landroid/net/Uri;Lcom/google/android/apps/reader/net/HttpContentHandler;)I
    :try_end_19
    .catchall {:try_start_13 .. :try_end_19} :catchall_1e

    move-result v1

    .line 258
    invoke-virtual {v0, v2}, Lcom/google/android/apps/reader/net/HttpContentHandler;->setEntity(Lorg/apache/http/HttpEntity;)V

    return v1

    :catchall_1e
    move-exception v1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/reader/net/HttpContentHandler;->setEntity(Lorg/apache/http/HttpEntity;)V

    throw v1
.end method

.method public setMode(I)V
    .registers 7
    .parameter "mode"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 204
    and-int/lit8 v4, p1, 0x2

    if-eqz v4, :cond_31

    move v1, v2

    .line 205
    .local v1, uploadOnly:Z
    :goto_7
    and-int/lit8 v4, p1, 0x4

    if-eqz v4, :cond_33

    move v0, v2

    .line 206
    .local v0, preferencesOnly:Z
    :goto_c
    iput-boolean v2, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mUploadPendingActions:Z

    .line 207
    if-eqz v1, :cond_12

    if-eqz v0, :cond_35

    :cond_12
    move v4, v2

    :goto_13
    iput-boolean v4, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadPreferences:Z

    .line 208
    if-nez v1, :cond_37

    if-nez v0, :cond_37

    move v4, v2

    :goto_1a
    iput-boolean v4, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadSubscriptions:Z

    .line 209
    if-nez v1, :cond_39

    if-nez v0, :cond_39

    move v4, v2

    :goto_21
    iput-boolean v4, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadTags:Z

    .line 210
    if-nez v1, :cond_3b

    if-nez v0, :cond_3b

    move v4, v2

    :goto_28
    iput-boolean v4, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadUnreadCounts:Z

    .line 211
    if-nez v1, :cond_3d

    if-nez v0, :cond_3d

    :goto_2e
    iput-boolean v2, p0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadRecentStreams:Z

    .line 212
    return-void

    .end local v0           #preferencesOnly:Z
    .end local v1           #uploadOnly:Z
    :cond_31
    move v1, v3

    .line 204
    goto :goto_7

    .restart local v1       #uploadOnly:Z
    :cond_33
    move v0, v3

    .line 205
    goto :goto_c

    .restart local v0       #preferencesOnly:Z
    :cond_35
    move v4, v3

    .line 207
    goto :goto_13

    :cond_37
    move v4, v3

    .line 208
    goto :goto_1a

    :cond_39
    move v4, v3

    .line 209
    goto :goto_21

    :cond_3b
    move v4, v3

    .line 210
    goto :goto_28

    :cond_3d
    move v2, v3

    .line 211
    goto :goto_2e
.end method

.method protected final uploadPendingActions()V
    .registers 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    sget-object v14, Lcom/google/android/apps/reader/content/ReaderSync;->ACTION_SYNC_LOCK:Ljava/lang/Object;

    monitor-enter v14

    .line 324
    :try_start_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderSync;->queryPendingActions()Landroid/database/Cursor;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_c4

    move-result-object v4

    .line 326
    .local v4, cursor:Landroid/database/Cursor;
    :try_start_7
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 327
    .local v1, count:I
    if-eqz v1, :cond_1d

    .line 328
    const-string v13, "Uploading %d pending actions"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v13, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 330
    :cond_1d
    const/4 v9, 0x0

    .local v9, position:I
    :goto_1e
    invoke-interface {v4, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v13

    if-eqz v13, :cond_cd

    .line 331
    const/4 v13, 0x0

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 332
    .local v7, id:J
    const/4 v13, 0x1

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 333
    .local v12, url:Ljava/lang/String;
    const/4 v13, 0x2

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 334
    .local v6, dataString:Ljava/lang/String;
    const/4 v13, 0x3

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 335
    .local v2, created:J
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 338
    .local v11, uri:Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadSubscriptions:Z

    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->editSubscription()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v11, v15}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v15

    or-int/2addr v13, v15

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadSubscriptions:Z

    .line 341
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadUnreadCounts:Z

    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->markAllAsRead()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v11, v15}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v15

    or-int/2addr v13, v15

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/reader/content/ReaderSync;->mDownloadUnreadCounts:Z

    .line 343
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "&T="

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderSync;->getToken()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 344
    new-instance v5, Lorg/apache/http/entity/StringEntity;

    const-string v13, "UTF-8"

    invoke-direct {v5, v6, v13}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    .local v5, data:Lorg/apache/http/entity/StringEntity;
    const-string v13, "application/x-www-form-urlencoded"

    invoke-virtual {v5, v13}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 346
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v5}, Lcom/google/android/apps/reader/content/ReaderSync;->post(Landroid/net/Uri;Lorg/apache/http/HttpEntity;)I

    move-result v10

    .line 347
    .local v10, responseCode:I
    sparse-switch v10, :sswitch_data_d2

    .line 357
    const-string v13, "ReaderSync"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Unexpected response code: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-static {v2, v3}, Lcom/google/android/apps/reader/content/ReaderSync;->isExpired(J)Z

    move-result v13

    if-eqz v13, :cond_b5

    .line 359
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/google/android/apps/reader/content/ReaderSync;->deletePendingAction(J)V

    .line 330
    :cond_b5
    :goto_b5
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1e

    .line 349
    :sswitch_b9
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/google/android/apps/reader/content/ReaderSync;->commitPendingAction(J)V
    :try_end_be
    .catchall {:try_start_7 .. :try_end_be} :catchall_bf

    goto :goto_b5

    .line 368
    .end local v1           #count:I
    .end local v2           #created:J
    .end local v5           #data:Lorg/apache/http/entity/StringEntity;
    .end local v6           #dataString:Ljava/lang/String;
    .end local v7           #id:J
    .end local v9           #position:I
    .end local v10           #responseCode:I
    .end local v11           #uri:Landroid/net/Uri;
    .end local v12           #url:Ljava/lang/String;
    :catchall_bf
    move-exception v13

    :try_start_c0
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v13
    :try_end_c4
    .catchall {:try_start_c0 .. :try_end_c4} :catchall_c4

    .line 370
    .end local v4           #cursor:Landroid/database/Cursor;
    :catchall_c4
    move-exception v13

    monitor-exit v14

    throw v13

    .line 354
    .restart local v1       #count:I
    .restart local v2       #created:J
    .restart local v4       #cursor:Landroid/database/Cursor;
    .restart local v5       #data:Lorg/apache/http/entity/StringEntity;
    .restart local v6       #dataString:Ljava/lang/String;
    .restart local v7       #id:J
    .restart local v9       #position:I
    .restart local v10       #responseCode:I
    .restart local v11       #uri:Landroid/net/Uri;
    .restart local v12       #url:Ljava/lang/String;
    :sswitch_c7
    :try_start_c7
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/google/android/apps/reader/content/ReaderSync;->deletePendingAction(J)V
    :try_end_cc
    .catchall {:try_start_c7 .. :try_end_cc} :catchall_bf

    goto :goto_b5

    .line 368
    .end local v2           #created:J
    .end local v5           #data:Lorg/apache/http/entity/StringEntity;
    .end local v6           #dataString:Ljava/lang/String;
    .end local v7           #id:J
    .end local v10           #responseCode:I
    .end local v11           #uri:Landroid/net/Uri;
    .end local v12           #url:Ljava/lang/String;
    :cond_cd
    :try_start_cd
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 370
    monitor-exit v14
    :try_end_d1
    .catchall {:try_start_cd .. :try_end_d1} :catchall_c4

    .line 371
    return-void

    .line 347
    :sswitch_data_d2
    .sparse-switch
        0xc8 -> :sswitch_b9
        0x190 -> :sswitch_c7
        0x193 -> :sswitch_c7
        0x194 -> :sswitch_c7
    .end sparse-switch
.end method
