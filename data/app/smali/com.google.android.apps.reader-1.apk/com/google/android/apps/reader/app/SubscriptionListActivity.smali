.class public Lcom/google/android/apps/reader/app/SubscriptionListActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SubscriptionListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/app/SubscriptionListActivity$1;,
        Lcom/google/android/apps/reader/app/SubscriptionListActivity$SubscriptionListObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SubscriptionList"


# instance fields
.field private mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

.field private mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

.field private mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 234
    return-void
.end method

.method private changeIntent(Landroid/content/Intent;)V
    .registers 5
    .parameter "intent"

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 85
    .local v1, uri:Landroid/net/Uri;
    const-string v2, "android.intent.extra.TITLE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 86
    .local v0, title:Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->changeUri(Landroid/net/Uri;Ljava/lang/CharSequence;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->syncFragments()V

    .line 88
    return-void
.end method

.method private findFragmentById(I)Landroid/support/v4/app/Fragment;
    .registers 4
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/support/v4/app/Fragment;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 116
    .local v0, fragments:Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    return-object v1
.end method

.method private refresh()V
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->refresh()V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    if-eqz v0, :cond_e

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->refresh()V

    .line 159
    :cond_e
    return-void
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"

    .prologue
    .line 207
    const-string v0, "reader_window"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    .line 210
    :goto_a
    return-object v0

    :cond_b
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_a
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_12

    .line 152
    :goto_7
    return-void

    .line 146
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->showMainScreen()V

    goto :goto_7

    .line 149
    :pswitch_e
    invoke-direct {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->refresh()V

    goto :goto_7

    .line 144
    :pswitch_data_12
    .packed-switch 0x7f0b0003
        :pswitch_8
        :pswitch_e
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    new-instance v1, Lcom/google/android/apps/reader/widget/ReaderWindow;

    invoke-direct {v1, p0}, Lcom/google/android/apps/reader/widget/ReaderWindow;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    .line 58
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->requestCustomTitle()V

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setHomeButtonEnabled(Z)V

    .line 60
    const v1, 0x7f030039

    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->setContentView(I)V

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    const v2, 0x7f03003b

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setCustomTitleLayout(I)V

    .line 62
    const v1, 0x7f0b0063

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    iput-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    new-instance v2, Lcom/google/android/apps/reader/app/SubscriptionListActivity$SubscriptionListObserver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/reader/app/SubscriptionListActivity$SubscriptionListObserver;-><init>(Lcom/google/android/apps/reader/app/SubscriptionListActivity;Lcom/google/android/apps/reader/app/SubscriptionListActivity$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 64
    const v1, 0x7f0b0032

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/reader/fragment/StreamFragment;

    iput-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    if-eqz v1, :cond_55

    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->isInLayout()Z

    move-result v1

    if-eqz v1, :cond_55

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->setObserver(Lcom/google/android/apps/reader/fragment/StreamFragment$Observer;)V

    .line 68
    :cond_55
    if-nez p1, :cond_5e

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 70
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->changeIntent(Landroid/content/Intent;)V

    .line 72
    .end local v0           #intent:Landroid/content/Intent;
    :cond_5e
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 183
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 185
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 190
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_1c

    .line 201
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_c
    return v0

    .line 192
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->showMainScreen()V

    goto :goto_c

    .line 195
    :sswitch_13
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->onSearchRequested()Z

    goto :goto_c

    .line 198
    :sswitch_17
    invoke-direct {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->refresh()V

    goto :goto_c

    .line 190
    nop

    :sswitch_data_1c
    .sparse-switch
        0x102002c -> :sswitch_d
        0x7f0b0096 -> :sswitch_17
        0x7f0b0097 -> :sswitch_13
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 2
    .parameter "savedInstanceState"

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->syncFragments()V

    .line 81
    return-void
.end method

.method public onSearchRequested()Z
    .registers 9

    .prologue
    .line 163
    iget-object v6, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 164
    .local v0, account:Lcom/google/android/accounts/Account;
    if-nez v0, :cond_11

    .line 165
    const-string v6, "SubscriptionList"

    const-string v7, "Cannot search without an account"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const/4 v6, 0x0

    .line 178
    :goto_10
    return v6

    .line 168
    :cond_11
    const/4 v3, 0x0

    .line 169
    .local v3, initialQuery:Ljava/lang/String;
    const/4 v4, 0x1

    .line 170
    .local v4, selectInitialQuery:Z
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 171
    .local v1, appSearchData:Landroid/os/Bundle;
    const-string v6, "authAccount"

    iget-object v7, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v6, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->getTagId()Ljava/lang/String;

    move-result-object v5

    .line 173
    .local v5, tagId:Ljava/lang/String;
    if-eqz v5, :cond_2c

    .line 174
    const-string v6, "stream_id"

    invoke-virtual {v1, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_2c
    const/4 v2, 0x0

    .line 177
    .local v2, globalSearch:Z
    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 178
    const/4 v6, 0x1

    goto :goto_10
.end method

.method public onStreamChanged()V
    .registers 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->syncFragments()V

    .line 220
    return-void
.end method

.method public onStreamLoaded()V
    .registers 1

    .prologue
    .line 226
    return-void
.end method

.method public onStreamUnloaded()V
    .registers 1

    .prologue
    .line 232
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter "title"
    .parameter "color"

    .prologue
    .line 121
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mReaderWindow:Lcom/google/android/apps/reader/widget/ReaderWindow;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setTitle(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method

.method public startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .registers 7
    .parameter "fragment"
    .parameter "intent"
    .parameter "requestCode"

    .prologue
    .line 129
    invoke-virtual {p2, p0}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, type:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    if-eqz v1, :cond_2a

    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/StreamFragment;->isInLayout()Z

    move-result v1

    if-eqz v1, :cond_2a

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    sget-object v1, Lcom/google/android/apps/reader/fragment/StreamFragment;->SUPPORTED_CONTENT_TYPES:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeIntent(Landroid/content/Intent;)V

    .line 138
    :goto_29
    return-void

    .line 136
    :cond_2a
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    goto :goto_29
.end method

.method syncFragments()V
    .registers 6

    .prologue
    .line 91
    iget-object v4, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    if-eqz v4, :cond_19

    iget-object v4, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/fragment/StreamFragment;->isInLayout()Z

    move-result v4

    if-eqz v4, :cond_19

    .line 92
    iget-object v4, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/fragment/StreamFragment;->getStreamId()Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, streamId:Ljava/lang/String;
    if-eqz v1, :cond_1a

    .line 95
    iget-object v4, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->highlight(Ljava/lang/String;)V

    .line 111
    .end local v1           #streamId:Ljava/lang/String;
    :cond_19
    :goto_19
    return-void

    .line 98
    .restart local v1       #streamId:Ljava/lang/String;
    :cond_1a
    iget-object v4, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->getStreamUri()Landroid/net/Uri;

    move-result-object v2

    .line 99
    .local v2, streamUri:Landroid/net/Uri;
    if-eqz v2, :cond_19

    .line 100
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v0, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 101
    .local v0, intent:Landroid/content/Intent;
    iget-object v4, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mSubscriptionList:Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/fragment/SubscriptionListFragment;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    .line 102
    .local v3, title:Ljava/lang/CharSequence;
    if-eqz v3, :cond_36

    .line 103
    const-string v4, "android.intent.extra.TITLE"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 105
    :cond_36
    iget-object v4, p0, Lcom/google/android/apps/reader/app/SubscriptionListActivity;->mStream:Lcom/google/android/apps/reader/fragment/StreamFragment;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/reader/fragment/StreamFragment;->changeIntent(Landroid/content/Intent;)V

    goto :goto_19
.end method
