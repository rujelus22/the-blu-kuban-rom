.class public Lcom/google/android/apps/reader/app/ReaderCleanupService;
.super Landroid/app/IntentService;
.source "ReaderCleanupService.java"


# static fields
.field public static final EXTRA_PRIORITY:Ljava/lang/String; = "priority"

.field private static final TAG:Ljava/lang/String; = "ReaderCleanupService"


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 62
    const-string v0, "ReaderCleanupService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method static clearWebViewCache(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/apps/reader/app/ReaderCleanupService$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/reader/app/ReaderCleanupService$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 59
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .registers 12
    .parameter "intent"

    .prologue
    .line 67
    move-object v3, p0

    .line 68
    .local v3, context:Landroid/content/Context;
    const/4 v5, 0x5

    .line 69
    .local v5, defaultValue:I
    const-string v9, "priority"

    invoke-virtual {p1, v9, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 70
    .local v8, priority:I
    invoke-static {v3}, Lcom/google/android/accounts/AccountManager;->get(Landroid/content/Context;)Lcom/google/android/accounts/AccountManager;

    move-result-object v1

    .line 71
    .local v1, am:Lcom/google/android/accounts/AccountManager;
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 72
    .local v4, cr:Landroid/content/ContentResolver;
    const-string v9, "com.google"

    invoke-virtual {v1, v9}, Lcom/google/android/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Lcom/google/android/accounts/Account;

    move-result-object v2

    .local v2, arr$:[Lcom/google/android/accounts/Account;
    array-length v7, v2

    .local v7, len$:I
    const/4 v6, 0x0

    .local v6, i$:I
    :goto_18
    if-ge v6, v7, :cond_25

    aget-object v0, v2, v6

    .line 73
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-static {v4, v0, v8}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->cleanup(Landroid/content/ContentResolver;Lcom/google/android/accounts/Account;I)Z

    .line 74
    invoke-static {v3, v0}, Lcom/google/android/apps/reader/app/ReaderTimestamps;->updateCleanupTimestamp(Landroid/content/Context;Lcom/google/android/accounts/Account;)Z

    .line 72
    add-int/lit8 v6, v6, 0x1

    goto :goto_18

    .line 76
    .end local v0           #account:Lcom/google/android/accounts/Account;
    :cond_25
    invoke-static {v3}, Lcom/google/android/apps/reader/app/ReaderCleanupService;->clearWebViewCache(Landroid/content/Context;)V

    .line 78
    return-void
.end method
