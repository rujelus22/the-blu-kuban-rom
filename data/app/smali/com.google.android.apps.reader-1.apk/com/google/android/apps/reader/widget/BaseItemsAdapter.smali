.class abstract Lcom/google/android/apps/reader/widget/BaseItemsAdapter;
.super Landroid/support/v4/widget/CursorAdapter;
.source "BaseItemsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/reader/widget/ItemQuery;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/widget/BaseItemsAdapter$QueryHandler;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final EMAIL_SUPPORTED:Z = false

#the value of this static final field might be set in the static constructor
.field private static final SHARE_IS_SEND:Z = false

.field private static final TAG:Ljava/lang/String; = "BaseItemsAdapter"


# instance fields
.field private final mQueryHandler:Landroid/content/AsyncQueryHandler;

.field private mSilent:Z

.field private mUnsynchronizedAccount:Lcom/google/android/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 69
    const-class v0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->$assertionsDisabled:Z

    .line 76
    sget-object v0, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->SHARE_IS_SEND:Z

    return-void

    .line 69
    :cond_14
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 89
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 90
    new-instance v0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter$QueryHandler;

    invoke-direct {v0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter$QueryHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    .line 91
    return-void
.end method

.method private createSendIntent(Landroid/database/Cursor;)Landroid/content/Intent;
    .registers 6
    .parameter "cursor"

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 205
    .local v2, title:Ljava/lang/String;
    const/16 v3, 0xc

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, alternateHref:Ljava/lang/String;
    if-nez v0, :cond_e

    .line 207
    const/4 v1, 0x0

    .line 213
    :goto_d
    return-object v1

    .line 209
    :cond_e
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 210
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "text/plain"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_d
.end method

.method private getItemHtmlAsString(Landroid/database/Cursor;)Ljava/lang/String;
    .registers 7
    .parameter "cursor"

    .prologue
    .line 245
    iget-object v4, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 246
    .local v3, resolver:Landroid/content/ContentResolver;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getAccount(Landroid/database/Cursor;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 247
    .local v0, account:Lcom/google/android/accounts/Account;
    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 248
    .local v1, itemId:J
    invoke-static {v3, v0, v1, v2}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getHtmlAsString(Landroid/content/ContentResolver;Lcom/google/android/accounts/Account;J)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private varargs getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 4
    .parameter "resId"
    .parameter "formatArgs"

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getText(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "resId"

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static getTitle(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 806
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 807
    .local v0, title:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 808
    const v1, 0x7f0d012e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 810
    :cond_12
    return-object v0
.end method

.method private startActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 478
    return-void
.end method

.method private startActivitySafely(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    .prologue
    .line 482
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_3} :catch_5

    .line 483
    const/4 v0, 0x1

    .line 486
    :goto_4
    return v0

    .line 485
    :catch_5
    move-exception v0

    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startActivity(Landroid/content/Intent;)V

    .line 486
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private startDelete(Landroid/database/Cursor;Ljava/lang/CharSequence;)V
    .registers 9
    .parameter "cursor"
    .parameter "toast"

    .prologue
    .line 195
    const/4 v1, 0x2

    .line 196
    .local v1, token:I
    iget-boolean v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mSilent:Z

    if-eqz v0, :cond_16

    const/4 v2, 0x0

    .line 197
    .local v2, cookie:Ljava/lang/CharSequence;
    :goto_6
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->itemUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->syncLater(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 198
    .local v3, uri:Landroid/net/Uri;
    const/4 v4, 0x0

    .line 199
    .local v4, selection:Ljava/lang/String;
    const/4 v5, 0x0

    .line 200
    .local v5, selectionArgs:[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 201
    return-void

    .end local v2           #cookie:Ljava/lang/CharSequence;
    .end local v3           #uri:Landroid/net/Uri;
    .end local v4           #selection:Ljava/lang/String;
    .end local v5           #selectionArgs:[Ljava/lang/String;
    :cond_16
    move-object v2, p2

    .line 196
    goto :goto_6
.end method

.method private startUpdate(Landroid/database/Cursor;Ljava/lang/String;ZLjava/lang/CharSequence;)V
    .registers 12
    .parameter "cursor"
    .parameter "columnName"
    .parameter "value"
    .parameter "toast"

    .prologue
    .line 177
    const/4 v1, 0x1

    .line 178
    .local v1, token:I
    iget-boolean v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mSilent:Z

    if-eqz v0, :cond_27

    const/4 v2, 0x0

    .line 179
    .local v2, cookie:Ljava/lang/CharSequence;
    :goto_6
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->itemUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->syncLater(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 180
    .local v3, uri:Landroid/net/Uri;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 181
    .local v4, values:Landroid/content/ContentValues;
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v4, p2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 182
    const/4 v5, 0x0

    .line 183
    .local v5, selection:Ljava/lang/String;
    const/4 v6, 0x0

    .line 184
    .local v6, selectionArgs:[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 185
    check-cast p1, Lcom/google/android/apps/reader/widget/ModifiedCursor;

    .end local p1
    invoke-virtual {p1, v4, p0}, Lcom/google/android/apps/reader/widget/ModifiedCursor;->put(Landroid/content/ContentValues;Landroid/widget/BaseAdapter;)V

    .line 186
    return-void

    .end local v2           #cookie:Ljava/lang/CharSequence;
    .end local v3           #uri:Landroid/net/Uri;
    .end local v4           #values:Landroid/content/ContentValues;
    .end local v5           #selection:Ljava/lang/String;
    .end local v6           #selectionArgs:[Ljava/lang/String;
    .restart local p1
    :cond_27
    move-object v2, p4

    .line 178
    goto :goto_6
.end method

.method private syncLater(Landroid/net/Uri;)Landroid/net/Uri;
    .registers 4
    .parameter "uri"

    .prologue
    .line 163
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 166
    .local v0, account:Lcom/google/android/accounts/Account;
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mUnsynchronizedAccount:Lcom/google/android/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->syncChanges()Z

    .line 171
    :cond_f
    iput-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mUnsynchronizedAccount:Lcom/google/android/accounts/Account;

    .line 173
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->setSyncToNetwork(Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public addStar(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 308
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setStarred(Landroid/database/Cursor;Z)V

    .line 309
    return-void
.end method

.method public createLoader(Landroid/net/Uri;)Landroid/support/v4/content/CursorLoader;
    .registers 10
    .parameter "uri"

    .prologue
    const/4 v5, 0x0

    .line 94
    new-instance v0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter$1;

    iget-object v2, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v3, p1

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter$1;-><init>(Lcom/google/android/apps/reader/widget/BaseItemsAdapter;Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public deleteItem(Landroid/database/Cursor;)V
    .registers 7
    .parameter "cursor"

    .prologue
    .line 252
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, title:Ljava/lang/String;
    const v2, 0x7f0d0029

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 254
    .local v1, toast:Ljava/lang/CharSequence;
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startDelete(Landroid/database/Cursor;Ljava/lang/CharSequence;)V

    .line 255
    return-void
.end method

.method public emailItem(Landroid/database/Cursor;)V
    .registers 6
    .parameter "cursor"

    .prologue
    .line 361
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 362
    .local v2, title:Ljava/lang/String;
    const/16 v3, 0xc

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, alternateHref:Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 364
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "message/rfc822"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 365
    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 367
    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startActivitySafely(Landroid/content/Intent;)Z

    .line 368
    return-void
.end method

.method public findItem(JI)Landroid/database/Cursor;
    .registers 8
    .parameter "item"
    .parameter "start"

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 126
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_42

    .line 127
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_d

    .line 128
    const/4 p3, 0x0

    .line 130
    :cond_d
    const/4 v1, 0x0

    .line 131
    .local v1, offset:I
    :goto_e
    add-int v2, p3, v1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_1e

    sub-int v2, p3, v1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_42

    .line 132
    :cond_1e
    add-int v2, p3, v1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 133
    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getItemId(Landroid/database/Cursor;)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_2f

    .line 144
    .end local v0           #cursor:Landroid/database/Cursor;
    .end local v1           #offset:I
    :cond_2e
    :goto_2e
    return-object v0

    .line 137
    .restart local v0       #cursor:Landroid/database/Cursor;
    .restart local v1       #offset:I
    :cond_2f
    sub-int v2, p3, v1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_3f

    .line 138
    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getItemId(Landroid/database/Cursor;)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-eqz v2, :cond_2e

    .line 131
    :cond_3f
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 144
    .end local v1           #offset:I
    :cond_42
    const/4 v0, 0x0

    goto :goto_2e
.end method

.method protected getAccount(Landroid/database/Cursor;)Lcom/google/android/accounts/Account;
    .registers 5
    .parameter "cursor"

    .prologue
    .line 786
    const/16 v2, 0x14

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 787
    .local v0, accountName:Ljava/lang/String;
    const/16 v2, 0x15

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 788
    .local v1, accountType:Ljava/lang/String;
    new-instance v2, Lcom/google/android/accounts/Account;

    invoke-direct {v2, v0, v1}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getItemId(Landroid/database/Cursor;)J
    .registers 4
    .parameter "cursor"

    .prologue
    .line 798
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .registers 3
    .parameter "cursor"

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getTitle(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isStarred(Landroid/database/Cursor;)Z
    .registers 3
    .parameter "cursor"

    .prologue
    .line 814
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public isUnread(Landroid/database/Cursor;)Z
    .registers 3
    .parameter "cursor"

    .prologue
    .line 340
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public itemUri(Landroid/database/Cursor;)Landroid/net/Uri;
    .registers 6
    .parameter "cursor"

    .prologue
    .line 792
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getAccount(Landroid/database/Cursor;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 793
    .local v0, account:Lcom/google/android/accounts/Account;
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 794
    .local v1, itemId:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->itemUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method public keepUnread(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 336
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setRead(Landroid/database/Cursor;Z)V

    .line 337
    return-void
.end method

.method public likeItem(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 316
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setLiked(Landroid/database/Cursor;Z)V

    .line 317
    return-void
.end method

.method public markAllAsRead()V
    .registers 3

    .prologue
    .line 430
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->markAllAsRead(J)V

    .line 431
    return-void
.end method

.method public markAllAsRead(J)V
    .registers 12
    .parameter "offset"

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v7

    .line 435
    .local v7, cursor:Landroid/database/Cursor;
    if-nez v7, :cond_e

    .line 436
    const-string v0, "BaseItemsAdapter"

    const-string v4, "Cannot mark all as read because data is not loaded"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :goto_d
    return-void

    .line 439
    :cond_e
    invoke-interface {v7}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "com.google.reader.cursor.extra.URI"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 440
    .local v3, uri:Landroid/net/Uri;
    if-nez v3, :cond_24

    .line 442
    const-string v0, "BaseItemsAdapter"

    const-string v4, "Cannot mark all as read because stream is not set"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 445
    :cond_24
    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    .line 446
    .local v8, streamId:Ljava/lang/String;
    if-nez v8, :cond_32

    .line 449
    const-string v0, "BaseItemsAdapter"

    const-string v4, "Cannot mark all as read because stream has no ID"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 452
    :cond_32
    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v6

    .line 453
    .local v6, account:Lcom/google/android/accounts/Account;
    const/4 v1, 0x3

    .line 454
    .local v1, token:I
    move-object v2, v6

    .line 455
    .local v2, cookie:Lcom/google/android/accounts/Account;
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->markAllAsRead(Landroid/content/AsyncQueryHandler;ILjava/lang/Object;Landroid/net/Uri;J)V

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->showAllAsRead()V

    goto :goto_d
.end method

.method public markPreviousAsRead(Landroid/database/Cursor;)V
    .registers 14
    .parameter "cursor"

    .prologue
    const/16 v10, 0x18

    const/4 v11, 0x0

    .line 395
    invoke-interface {p1, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 400
    const-string v0, "BaseItemsAdapter"

    const-string v10, "Position is null"

    invoke-static {v0, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :goto_10
    return-void

    .line 403
    :cond_11
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 404
    .local v8, position:I
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v10, "com.google.reader.cursor.extra.URI"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 405
    .local v3, uri:Landroid/net/Uri;
    if-nez v3, :cond_2b

    .line 407
    const-string v0, "BaseItemsAdapter"

    const-string v10, "Cannot mark previous as read because stream is not set"

    invoke-static {v0, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_10

    .line 410
    :cond_2b
    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 411
    .local v9, streamId:Ljava/lang/String;
    if-nez v9, :cond_39

    .line 414
    const-string v0, "BaseItemsAdapter"

    const-string v10, "Cannot mark previous as read because stream has no ID"

    invoke-static {v0, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_10

    .line 417
    :cond_39
    invoke-static {v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v7

    .line 418
    .local v7, account:Lcom/google/android/accounts/Account;
    const/4 v1, 0x4

    .line 419
    .local v1, token:I
    move-object v2, v7

    .line 420
    .local v2, cookie:Lcom/google/android/accounts/Account;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 421
    .local v4, values:Landroid/content/ContentValues;
    const-string v0, "read"

    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 422
    const-string v5, "position <= ?"

    .line 423
    .local v5, selection:Ljava/lang/String;
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v11

    .line 424
    .local v6, selectionArgs:[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    move-object v0, p1

    .line 426
    check-cast v0, Lcom/google/android/apps/reader/widget/ModifiedCursor;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v0, v4, v11, v10, p0}, Lcom/google/android/apps/reader/widget/ModifiedCursor;->fill(Landroid/content/ContentValues;IILandroid/widget/BaseAdapter;)V

    goto :goto_10
.end method

.method public markRead(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 332
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setRead(Landroid/database/Cursor;Z)V

    .line 333
    return-void
.end method

.method public newViewItemInStreamIntent(Landroid/database/Cursor;Landroid/net/Uri;)Landroid/content/Intent;
    .registers 8
    .parameter "cursor"
    .parameter "streamUri"

    .prologue
    .line 151
    sget-boolean v3, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->$assertionsDisabled:Z

    if-nez v3, :cond_1c

    sget-object v3, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_TYPE:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 152
    :cond_1c
    if-eqz p2, :cond_30

    .line 153
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 154
    .local v1, itemId:J
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.reader.intent.action.VIEW_ITEM"

    invoke-direct {v0, v3, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 155
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "item_id"

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 158
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #itemId:J
    :goto_2f
    return-object v0

    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 10
    .parameter "menuItem"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 509
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v4

    .line 510
    .local v4, menuInfo:Landroid/view/ContextMenu$ContextMenuInfo;
    instance-of v7, v4, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz v7, :cond_66

    move-object v2, v4

    .line 511
    check-cast v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 512
    .local v2, info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget-object v7, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView;

    .line 513
    .local v0, adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget v7, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v0, v7}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    .line 514
    .local v3, item:Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 515
    .local v1, cursor:Landroid/database/Cursor;
    if-eqz v3, :cond_64

    if-ne v3, v1, :cond_64

    .line 516
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    packed-switch v7, :pswitch_data_68

    move v5, v6

    .line 564
    .end local v0           #adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    .end local v1           #cursor:Landroid/database/Cursor;
    .end local v2           #info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    .end local v3           #item:Ljava/lang/Object;
    :goto_2b
    return v5

    .line 518
    .restart local v0       #adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    .restart local v1       #cursor:Landroid/database/Cursor;
    .restart local v2       #info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    .restart local v3       #item:Ljava/lang/Object;
    :pswitch_2c
    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setStarred(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 521
    :pswitch_30
    invoke-virtual {p0, v1, v6}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setStarred(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 524
    :pswitch_34
    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setLiked(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 527
    :pswitch_38
    invoke-virtual {p0, v1, v6}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setLiked(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 530
    :pswitch_3c
    sget-boolean v6, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->SHARE_IS_SEND:Z

    if-eqz v6, :cond_44

    .line 531
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->sendItem(Landroid/database/Cursor;)Z

    goto :goto_2b

    .line 533
    :cond_44
    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setShared(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 537
    :pswitch_48
    invoke-virtual {p0, v1, v6}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setShared(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 540
    :pswitch_4c
    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setRead(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 543
    :pswitch_50
    invoke-virtual {p0, v1, v6}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setRead(Landroid/database/Cursor;Z)V

    goto :goto_2b

    .line 546
    :pswitch_54
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->shareWithNote(Landroid/database/Cursor;)Z

    goto :goto_2b

    .line 549
    :pswitch_58
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->deleteItem(Landroid/database/Cursor;)V

    goto :goto_2b

    .line 552
    :pswitch_5c
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->tagItem(Landroid/database/Cursor;)V

    goto :goto_2b

    .line 555
    :pswitch_60
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->markPreviousAsRead(Landroid/database/Cursor;)V

    goto :goto_2b

    :cond_64
    move v5, v6

    .line 561
    goto :goto_2b

    .end local v0           #adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    .end local v1           #cursor:Landroid/database/Cursor;
    .end local v2           #info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    .end local v3           #item:Ljava/lang/Object;
    :cond_66
    move v5, v6

    .line 564
    goto :goto_2b

    .line 516
    :pswitch_data_68
    .packed-switch 0x7f0b0084
        :pswitch_4c
        :pswitch_50
        :pswitch_60
        :pswitch_2c
        :pswitch_30
        :pswitch_34
        :pswitch_38
        :pswitch_3c
        :pswitch_48
        :pswitch_54
        :pswitch_58
        :pswitch_5c
    .end packed-switch
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 11
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 491
    instance-of v6, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz v6, :cond_2f

    move-object v4, p3

    .line 492
    check-cast v4, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 493
    .local v4, info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget-object v6, v4, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView;

    .line 494
    .local v0, adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget v6, v4, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v0, v6}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v5

    .line 495
    .local v5, item:Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 496
    .local v2, cursor:Landroid/database/Cursor;
    if-eqz v5, :cond_2f

    if-ne v5, v2, :cond_2f

    .line 497
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 498
    .local v1, context:Landroid/content/Context;
    new-instance v3, Landroid/view/MenuInflater;

    invoke-direct {v3, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 499
    .local v3, inflater:Landroid/view/MenuInflater;
    const v6, 0x7f100008

    invoke-virtual {v3, v6, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 503
    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->onPrepareOptionsMenu(Landroid/view/Menu;Landroid/database/Cursor;)V

    .line 506
    .end local v0           #adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    .end local v1           #context:Landroid/content/Context;
    .end local v2           #cursor:Landroid/database/Cursor;
    .end local v3           #inflater:Landroid/view/MenuInflater;
    .end local v4           #info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    .end local v5           #item:Ljava/lang/Object;
    :cond_2f
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 632
    new-instance v0, Landroid/view/MenuInflater;

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 633
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f10000a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 634
    const/4 v1, 0x1

    return v1
.end method

.method public onKeyDown(Landroid/database/Cursor;ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "cursor"
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 569
    sparse-switch p2, :sswitch_data_6c

    .line 627
    :cond_5
    :goto_5
    return v0

    .line 571
    :sswitch_6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 572
    sget-object v2, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 575
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->toggleShare(Landroid/database/Cursor;)V

    move v0, v1

    .line 576
    goto :goto_5

    .line 579
    :cond_19
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->toggleStar(Landroid/database/Cursor;)V

    move v0, v1

    .line 580
    goto :goto_5

    .line 583
    :sswitch_1e
    sget-object v2, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 586
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->toggleLike(Landroid/database/Cursor;)V

    move v0, v1

    .line 587
    goto :goto_5

    .line 590
    :sswitch_2b
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 591
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->sendItem(Landroid/database/Cursor;)Z

    :goto_34
    move v0, v1

    .line 595
    goto :goto_5

    .line 593
    :cond_36
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->tagItem(Landroid/database/Cursor;)V

    goto :goto_34

    .line 597
    :sswitch_3a
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->emailItem(Landroid/database/Cursor;)V

    move v0, v1

    .line 598
    goto :goto_5

    .line 600
    :sswitch_3f
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 601
    sget-object v2, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 604
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->shareWithNote(Landroid/database/Cursor;)Z

    move v0, v1

    .line 605
    goto :goto_5

    .line 611
    :sswitch_52
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->viewOriginal(Landroid/database/Cursor;)Z

    move v0, v1

    .line 612
    goto :goto_5

    .line 614
    :sswitch_57
    sget-object v1, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_5

    .line 616
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_5

    .line 624
    :sswitch_66
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->toggleRead(Landroid/database/Cursor;)V

    move v0, v1

    .line 625
    goto :goto_5

    .line 569
    nop

    :sswitch_data_6c
    .sparse-switch
        0x1f -> :sswitch_57
        0x20 -> :sswitch_3f
        0x21 -> :sswitch_3a
        0x28 -> :sswitch_1e
        0x29 -> :sswitch_66
        0x2f -> :sswitch_6
        0x30 -> :sswitch_2b
        0x32 -> :sswitch_52
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;Landroid/database/Cursor;)Z
    .registers 5
    .parameter "item"
    .parameter "cursor"

    .prologue
    const/4 v0, 0x1

    .line 715
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_46

    .line 760
    :pswitch_8
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 717
    :pswitch_a
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->addStar(Landroid/database/Cursor;)V

    goto :goto_9

    .line 720
    :pswitch_e
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->removeStar(Landroid/database/Cursor;)V

    goto :goto_9

    .line 723
    :pswitch_12
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->likeItem(Landroid/database/Cursor;)V

    goto :goto_9

    .line 726
    :pswitch_16
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->unlikeItem(Landroid/database/Cursor;)V

    goto :goto_9

    .line 729
    :pswitch_1a
    sget-boolean v1, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->SHARE_IS_SEND:Z

    if-eqz v1, :cond_22

    .line 730
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->sendItem(Landroid/database/Cursor;)Z

    goto :goto_9

    .line 732
    :cond_22
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->shareItem(Landroid/database/Cursor;)V

    goto :goto_9

    .line 736
    :pswitch_26
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->unshareItem(Landroid/database/Cursor;)V

    goto :goto_9

    .line 739
    :pswitch_2a
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->shareWithNote(Landroid/database/Cursor;)Z

    goto :goto_9

    .line 742
    :pswitch_2e
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->deleteItem(Landroid/database/Cursor;)V

    goto :goto_9

    .line 745
    :pswitch_32
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->keepUnread(Landroid/database/Cursor;)V

    goto :goto_9

    .line 748
    :pswitch_36
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->markRead(Landroid/database/Cursor;)V

    goto :goto_9

    .line 751
    :pswitch_3a
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->tagItem(Landroid/database/Cursor;)V

    goto :goto_9

    .line 754
    :pswitch_3e
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->sendItem(Landroid/database/Cursor;)Z

    goto :goto_9

    .line 757
    :pswitch_42
    invoke-virtual {p0, p2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->emailItem(Landroid/database/Cursor;)V

    goto :goto_9

    .line 715
    :pswitch_data_46
    .packed-switch 0x7f0b0084
        :pswitch_36
        :pswitch_32
        :pswitch_8
        :pswitch_a
        :pswitch_e
        :pswitch_12
        :pswitch_16
        :pswitch_1a
        :pswitch_26
        :pswitch_2a
        :pswitch_2e
        :pswitch_3a
        :pswitch_3e
        :pswitch_42
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;Landroid/database/Cursor;)V
    .registers 26
    .parameter "menu"
    .parameter "cursor"

    .prologue
    .line 638
    const v22, 0x7f0b0087

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 639
    .local v2, addStar:Landroid/view/MenuItem;
    const v22, 0x7f0b0088

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    .line 640
    .local v15, removeStar:Landroid/view/MenuItem;
    const v22, 0x7f0b0089

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v12

    .line 641
    .local v12, like:Landroid/view/MenuItem;
    const v22, 0x7f0b008a

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v20

    .line 642
    .local v20, unlike:Landroid/view/MenuItem;
    const v22, 0x7f0b008b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v17

    .line 643
    .local v17, share:Landroid/view/MenuItem;
    const v22, 0x7f0b008c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v21

    .line 644
    .local v21, unshare:Landroid/view/MenuItem;
    const v22, 0x7f0b008d

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v18

    .line 645
    .local v18, shareWithNote:Landroid/view/MenuItem;
    const v22, 0x7f0b008e

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 646
    .local v3, delete:Landroid/view/MenuItem;
    const v22, 0x7f0b0085

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v14

    .line 647
    .local v14, markUnread:Landroid/view/MenuItem;
    const v22, 0x7f0b0084

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 648
    .local v13, markRead:Landroid/view/MenuItem;
    const v22, 0x7f0b0090

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v16

    .line 649
    .local v16, send:Landroid/view/MenuItem;
    const v22, 0x7f0b0091

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 650
    .local v4, email:Landroid/view/MenuItem;
    const v22, 0x7f0b008f

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v19

    .line 652
    .local v19, tag:Landroid/view/MenuItem;
    if-eqz p2, :cond_1f1

    .line 653
    const/16 v22, 0x5

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-eqz v22, :cond_1c0

    const/4 v11, 0x1

    .line 654
    .local v11, isStarred:Z
    :goto_9e
    const/16 v22, 0x7

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-eqz v22, :cond_1c3

    const/4 v6, 0x1

    .line 655
    .local v6, isLiked:Z
    :goto_ab
    const/16 v22, 0x8

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-eqz v22, :cond_1c6

    const/4 v10, 0x1

    .line 656
    .local v10, isShared:Z
    :goto_b8
    const/16 v22, 0x6

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-eqz v22, :cond_1c9

    const/4 v7, 0x1

    .line 657
    .local v7, isLocked:Z
    :goto_c5
    const/16 v22, 0x9

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-nez v22, :cond_dd

    const/16 v22, 0xa

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-eqz v22, :cond_1cc

    :cond_dd
    const/4 v8, 0x1

    .line 659
    .local v8, isNote:Z
    :goto_de
    const/16 v22, 0x4

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    if-eqz v22, :cond_1cf

    const/4 v9, 0x1

    .line 660
    .local v9, isRead:Z
    :goto_eb
    const/16 v22, 0xc

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v22

    if-nez v22, :cond_1d2

    const/4 v5, 0x1

    .line 662
    .local v5, hasLink:Z
    :goto_f8
    if-nez v11, :cond_1d5

    const/16 v22, 0x1

    :goto_fc
    move/from16 v0, v22

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 663
    invoke-interface {v15, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 664
    if-nez v6, :cond_1d9

    const/16 v22, 0x1

    :goto_108
    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 665
    move-object/from16 v0, v20

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 666
    if-nez v10, :cond_1dd

    const/16 v22, 0x1

    :goto_116
    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 667
    move-object/from16 v0, v21

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 668
    if-eqz v5, :cond_1e1

    if-nez v8, :cond_1e1

    const/16 v22, 0x1

    :goto_128
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 669
    invoke-interface {v3, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 670
    invoke-interface {v14, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 671
    if-nez v9, :cond_1e5

    const/16 v22, 0x1

    :goto_139
    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 672
    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 673
    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 674
    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 676
    if-nez v7, :cond_1e9

    const/16 v22, 0x1

    :goto_153
    move/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 677
    if-nez v7, :cond_1ed

    const/16 v22, 0x1

    :goto_15c
    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 679
    sget-object v22, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v22

    if-eqz v22, :cond_190

    .line 680
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 681
    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 682
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 683
    const/16 v22, 0x0

    invoke-interface/range {v21 .. v22}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 684
    const/16 v22, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 687
    :cond_190
    sget-boolean v22, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->SHARE_IS_SEND:Z

    if-eqz v22, :cond_1b8

    .line 688
    invoke-interface/range {v16 .. v16}, Landroid/view/MenuItem;->isVisible()Z

    move-result v22

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 689
    invoke-interface/range {v16 .. v16}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 690
    const/16 v22, 0x0

    invoke-interface/range {v21 .. v22}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 691
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 695
    :cond_1b8
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 712
    .end local v5           #hasLink:Z
    .end local v6           #isLiked:Z
    .end local v7           #isLocked:Z
    .end local v8           #isNote:Z
    .end local v9           #isRead:Z
    .end local v10           #isShared:Z
    .end local v11           #isStarred:Z
    :goto_1bf
    return-void

    .line 653
    :cond_1c0
    const/4 v11, 0x0

    goto/16 :goto_9e

    .line 654
    .restart local v11       #isStarred:Z
    :cond_1c3
    const/4 v6, 0x0

    goto/16 :goto_ab

    .line 655
    .restart local v6       #isLiked:Z
    :cond_1c6
    const/4 v10, 0x0

    goto/16 :goto_b8

    .line 656
    .restart local v10       #isShared:Z
    :cond_1c9
    const/4 v7, 0x0

    goto/16 :goto_c5

    .line 657
    .restart local v7       #isLocked:Z
    :cond_1cc
    const/4 v8, 0x0

    goto/16 :goto_de

    .line 659
    .restart local v8       #isNote:Z
    :cond_1cf
    const/4 v9, 0x0

    goto/16 :goto_eb

    .line 660
    .restart local v9       #isRead:Z
    :cond_1d2
    const/4 v5, 0x0

    goto/16 :goto_f8

    .line 662
    .restart local v5       #hasLink:Z
    :cond_1d5
    const/16 v22, 0x0

    goto/16 :goto_fc

    .line 664
    :cond_1d9
    const/16 v22, 0x0

    goto/16 :goto_108

    .line 666
    :cond_1dd
    const/16 v22, 0x0

    goto/16 :goto_116

    .line 668
    :cond_1e1
    const/16 v22, 0x0

    goto/16 :goto_128

    .line 671
    :cond_1e5
    const/16 v22, 0x0

    goto/16 :goto_139

    .line 676
    :cond_1e9
    const/16 v22, 0x0

    goto/16 :goto_153

    .line 677
    :cond_1ed
    const/16 v22, 0x0

    goto/16 :goto_15c

    .line 698
    .end local v5           #hasLink:Z
    .end local v6           #isLiked:Z
    .end local v7           #isLocked:Z
    .end local v8           #isNote:Z
    .end local v9           #isRead:Z
    .end local v10           #isShared:Z
    .end local v11           #isStarred:Z
    :cond_1f1
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 699
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v15, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 700
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 701
    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 702
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 703
    const/16 v22, 0x0

    invoke-interface/range {v21 .. v22}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 704
    const/16 v22, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 705
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 706
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 707
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 708
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 709
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 710
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1bf
.end method

.method public removeStar(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 312
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setStarred(Landroid/database/Cursor;Z)V

    .line 313
    return-void
.end method

.method public sendItem(Landroid/database/Cursor;)Z
    .registers 8
    .parameter "cursor"

    .prologue
    .line 371
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 372
    .local v4, title:Ljava/lang/String;
    const/16 v5, 0xc

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, alternateHref:Ljava/lang/String;
    if-nez v0, :cond_e

    .line 374
    const/4 v5, 0x0

    .line 386
    :goto_d
    return v5

    .line 376
    :cond_e
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 377
    .local v3, intent:Landroid/content/Intent;
    const-string v5, "text/plain"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    const-string v5, "android.intent.extra.SUBJECT"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 382
    const/4 v2, 0x0

    .line 383
    .local v2, chooserTitle:Ljava/lang/String;
    invoke-static {v3, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 385
    .local v1, chooser:Landroid/content/Intent;
    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startActivity(Landroid/content/Intent;)V

    .line 386
    const/4 v5, 0x1

    goto :goto_d
.end method

.method public setLiked(Landroid/database/Cursor;Z)V
    .registers 8
    .parameter "cursor"
    .parameter "liked"

    .prologue
    .line 276
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 277
    .local v0, title:Ljava/lang/String;
    if-eqz p2, :cond_19

    const v2, 0x7f0d0025

    :goto_9
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 278
    .local v1, toast:Ljava/lang/CharSequence;
    const-string v2, "liked"

    invoke-direct {p0, p1, v2, p2, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startUpdate(Landroid/database/Cursor;Ljava/lang/String;ZLjava/lang/CharSequence;)V

    .line 279
    return-void

    .line 277
    .end local v1           #toast:Ljava/lang/CharSequence;
    :cond_19
    const v2, 0x7f0d0026

    goto :goto_9
.end method

.method public setRead(Landroid/database/Cursor;Z)V
    .registers 5
    .parameter "cursor"
    .parameter "read"

    .prologue
    .line 264
    if-eqz p2, :cond_f

    const v1, 0x7f0d002a

    :goto_5
    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 265
    .local v0, toast:Ljava/lang/CharSequence;
    const-string v1, "read"

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startUpdate(Landroid/database/Cursor;Ljava/lang/String;ZLjava/lang/CharSequence;)V

    .line 266
    return-void

    .line 264
    .end local v0           #toast:Ljava/lang/CharSequence;
    :cond_f
    const v1, 0x7f0d002b

    goto :goto_5
.end method

.method public setShared(Landroid/database/Cursor;Z)V
    .registers 8
    .parameter "cursor"
    .parameter "shared"

    .prologue
    .line 269
    if-eqz p2, :cond_19

    const v0, 0x7f0d0027

    .line 270
    .local v0, resId:I
    :goto_5
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, title:Ljava/lang/String;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 272
    .local v2, toast:Ljava/lang/CharSequence;
    const-string v3, "shared"

    invoke-direct {p0, p1, v3, p2, v2}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startUpdate(Landroid/database/Cursor;Ljava/lang/String;ZLjava/lang/CharSequence;)V

    .line 273
    return-void

    .line 269
    .end local v0           #resId:I
    .end local v1           #title:Ljava/lang/String;
    .end local v2           #toast:Ljava/lang/CharSequence;
    :cond_19
    const v0, 0x7f0d0028

    goto :goto_5
.end method

.method public setSilent(Z)V
    .registers 2
    .parameter "silent"

    .prologue
    .line 768
    iput-boolean p1, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mSilent:Z

    .line 769
    return-void
.end method

.method public setStarred(Landroid/database/Cursor;Z)V
    .registers 6
    .parameter "cursor"
    .parameter "starred"

    .prologue
    .line 282
    if-eqz p2, :cond_f

    const v0, 0x7f0d0023

    .line 283
    .local v0, resId:I
    :goto_5
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 284
    .local v1, toast:Ljava/lang/CharSequence;
    const-string v2, "starred"

    invoke-direct {p0, p1, v2, p2, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startUpdate(Landroid/database/Cursor;Ljava/lang/String;ZLjava/lang/CharSequence;)V

    .line 285
    return-void

    .line 282
    .end local v0           #resId:I
    .end local v1           #toast:Ljava/lang/CharSequence;
    :cond_f
    const v0, 0x7f0d0024

    goto :goto_5
.end method

.method public shareItem(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 324
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setShared(Landroid/database/Cursor;Z)V

    .line 325
    return-void
.end method

.method public shareWithNote(Landroid/database/Cursor;)Z
    .registers 9
    .parameter "cursor"

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->createSendIntent(Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v0

    .line 218
    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_41

    .line 219
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getItemHtmlAsString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, snippet:Ljava/lang/String;
    if-eqz v1, :cond_11

    .line 221
    const-string v5, "com.google.android.reader.intent.extra.SNIPPET"

    invoke-virtual {v0, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    :cond_11
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 225
    .local v4, template:Landroid/content/ContentValues;
    const/16 v5, 0xb

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 226
    .local v2, sourceTitle:Ljava/lang/String;
    if-eqz v2, :cond_23

    .line 227
    const-string v5, "source_title_plaintext"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_23
    const/16 v5, 0x1a

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 230
    .local v3, sourceUrl:Ljava/lang/String;
    if-eqz v3, :cond_30

    .line 231
    const-string v5, "source_alternate_href"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_30
    const-string v5, "android.intent.extra.TEMPLATE"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 235
    iget-object v5, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    const-class v6, Lcom/google/android/apps/reader/app/NoteActivity;

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 237
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startActivity(Landroid/content/Intent;)V

    .line 238
    const/4 v5, 0x1

    .line 240
    .end local v1           #snippet:Ljava/lang/String;
    .end local v2           #sourceTitle:Ljava/lang/String;
    .end local v3           #sourceUrl:Ljava/lang/String;
    .end local v4           #template:Landroid/content/ContentValues;
    :goto_40
    return v5

    :cond_41
    const/4 v5, 0x0

    goto :goto_40
.end method

.method public showAllAsRead()V
    .registers 5

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/reader/widget/ModifiedCursor;

    .line 468
    .local v0, cursor:Lcom/google/android/apps/reader/widget/ModifiedCursor;
    if-eqz v0, :cond_23

    .line 469
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 470
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "read"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 471
    const-string v2, "locked"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 472
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ModifiedCursor;->getCount()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/reader/widget/ModifiedCursor;->fill(Landroid/content/ContentValues;IILandroid/widget/BaseAdapter;)V

    .line 474
    .end local v1           #values:Landroid/content/ContentValues;
    :cond_23
    return-void
.end method

.method public syncChanges()Z
    .registers 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mUnsynchronizedAccount:Lcom/google/android/accounts/Account;

    if-eqz v0, :cond_10

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mUnsynchronizedAccount:Lcom/google/android/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->requestSyncUpload(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mUnsynchronizedAccount:Lcom/google/android/accounts/Account;

    .line 110
    const/4 v0, 0x1

    .line 112
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public tagItem(Landroid/database/Cursor;)V
    .registers 5
    .parameter "cursor"

    .prologue
    .line 258
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->itemUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v1

    .line 259
    .local v1, uri:Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "org.openintents.action.TAG"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 260
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startActivity(Landroid/content/Intent;)V

    .line 261
    return-void
.end method

.method public toggleLike(Landroid/database/Cursor;)V
    .registers 6
    .parameter "cursor"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 293
    const/4 v3, 0x7

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_10

    move v0, v1

    .line 294
    .local v0, liked:Z
    :goto_a
    if-nez v0, :cond_12

    :goto_c
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setLiked(Landroid/database/Cursor;Z)V

    .line 295
    return-void

    .end local v0           #liked:Z
    :cond_10
    move v0, v2

    .line 293
    goto :goto_a

    .restart local v0       #liked:Z
    :cond_12
    move v1, v2

    .line 294
    goto :goto_c
.end method

.method public toggleRead(Landroid/database/Cursor;)V
    .registers 6
    .parameter "cursor"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 298
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_10

    move v0, v1

    .line 299
    .local v0, read:Z
    :goto_a
    if-nez v0, :cond_12

    :goto_c
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setRead(Landroid/database/Cursor;Z)V

    .line 300
    return-void

    .end local v0           #read:Z
    :cond_10
    move v0, v2

    .line 298
    goto :goto_a

    .restart local v0       #read:Z
    :cond_12
    move v1, v2

    .line 299
    goto :goto_c
.end method

.method public toggleShare(Landroid/database/Cursor;)V
    .registers 6
    .parameter "cursor"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 288
    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_11

    move v0, v1

    .line 289
    .local v0, shared:Z
    :goto_b
    if-nez v0, :cond_13

    :goto_d
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setShared(Landroid/database/Cursor;Z)V

    .line 290
    return-void

    .end local v0           #shared:Z
    :cond_11
    move v0, v2

    .line 288
    goto :goto_b

    .restart local v0       #shared:Z
    :cond_13
    move v1, v2

    .line 289
    goto :goto_d
.end method

.method public toggleStar(Landroid/database/Cursor;)V
    .registers 6
    .parameter "cursor"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 303
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_10

    move v0, v1

    .line 304
    .local v0, starred:Z
    :goto_a
    if-nez v0, :cond_12

    :goto_c
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setStarred(Landroid/database/Cursor;Z)V

    .line 305
    return-void

    .end local v0           #starred:Z
    :cond_10
    move v0, v2

    .line 303
    goto :goto_a

    .restart local v0       #starred:Z
    :cond_12
    move v1, v2

    .line 304
    goto :goto_c
.end method

.method public unlikeItem(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 320
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setLiked(Landroid/database/Cursor;Z)V

    .line 321
    return-void
.end method

.method public unshareItem(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 328
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setShared(Landroid/database/Cursor;Z)V

    .line 329
    return-void
.end method

.method public viewOriginal(Landroid/database/Cursor;)Z
    .registers 8
    .parameter "cursor"

    .prologue
    const/4 v4, 0x1

    .line 344
    const/16 v5, 0xc

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, alternateHref:Ljava/lang/String;
    if-nez v0, :cond_b

    .line 346
    const/4 v4, 0x0

    .line 357
    :cond_a
    :goto_a
    return v4

    .line 348
    :cond_b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 349
    .local v3, uri:Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v1, v5, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 350
    .local v1, intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 351
    .local v2, packageName:Ljava/lang/String;
    const-string v5, "com.android.browser.application_id"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->startActivitySafely(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 353
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->isUnread(Landroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 354
    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/reader/widget/BaseItemsAdapter;->setRead(Landroid/database/Cursor;Z)V

    goto :goto_a
.end method
