.class public final Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "NewsTickerWidget.java"


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final REMOTE_VIEWS_SERVICE:Ljava/lang/String; = "com.google.android.apps.reader.appwidget.NewsTickerWidgetRemoteViewsService"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private static bindViews(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .registers 15
    .parameter "context"
    .parameter "views"
    .parameter "appWidgetId"
    .parameter "uri"
    .parameter "label"

    .prologue
    const v8, 0x7f0b0010

    .line 79
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v7

    packed-switch v7, :pswitch_data_ae

    .line 118
    :pswitch_a
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unexpected layout: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 81
    :pswitch_27
    invoke-static {p3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 82
    .local v5, streamId:Ljava/lang/String;
    const/4 v7, 0x0

    invoke-static {v5, v7}, Lcom/google/android/apps/reader/res/ReaderResources;->getShortcutIconResource(Ljava/lang/String;I)I

    move-result v0

    .line 83
    .local v0, icon:I
    const v7, 0x7f0b0017

    invoke-virtual {p1, v7, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 84
    const v7, 0x7f0b0018

    invoke-virtual {p1, v7, p4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 86
    invoke-static {p3, p4}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createViewIntent(Landroid/net/Uri;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 87
    .local v1, intent:Landroid/content/Intent;
    const v7, 0x7f0b0016

    invoke-static {p0, v1}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createPendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 90
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.google.reader.intent.action.BIND"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    .local v3, service:Landroid/content/Intent;
    const-class v7, Lcom/google/android/apps/reader/appwidget/NewsTickerWidgetUpdateService;

    invoke-virtual {v3, p0, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 92
    invoke-static {p0, p3}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->autoRefresh(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 93
    const-string v7, "appWidgetId"

    invoke-virtual {v3, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 94
    const-string v7, "com.google.reader.intent.extra.LABEL"

    invoke-virtual {v3, v7, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    const-string v7, "com.google.reader.intent.extra.REMOTE_VIEWS"

    invoke-static {p1}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->clone(Landroid/widget/RemoteViews;)Landroid/widget/RemoteViews;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v4, v3

    .line 115
    .end local v0           #icon:I
    .end local v3           #service:Landroid/content/Intent;
    .end local v5           #streamId:Ljava/lang/String;
    .local v4, service:Ljava/lang/Object;
    :goto_71
    return-object v4

    .line 100
    .end local v1           #intent:Landroid/content/Intent;
    .end local v4           #service:Ljava/lang/Object;
    :pswitch_72
    const v7, 0x7f0b0011

    invoke-static {p1, v8, v7}, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->setEmptyView(Landroid/widget/RemoteViews;II)V

    .line 101
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 102
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v7, "com.google.android.apps.reader.appwidget.NewsTickerWidgetRemoteViewsService"

    invoke-virtual {v1, p0, v7}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    const-string v7, "appWidgetId"

    invoke-virtual {v1, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 104
    invoke-virtual {v1, p3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 105
    invoke-static {p1, p2, v8, v1}, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->setRemoteAdapter(Landroid/widget/RemoteViews;IILandroid/content/Intent;)V

    .line 107
    invoke-static {p3, p4}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createViewItemIntentTemplate(Landroid/net/Uri;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v6

    .line 108
    .local v6, template:Landroid/content/Intent;
    invoke-static {p0, v6}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createPendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 110
    .local v2, pendingIntent:Landroid/app/PendingIntent;
    invoke-static {p1, v8, v2}, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->setPendingIntentTemplate(Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;)V

    .line 112
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.google.reader.intent.action.QUERY"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .restart local v3       #service:Landroid/content/Intent;
    const-class v7, Lcom/google/android/apps/reader/appwidget/NewsTickerWidgetUpdateService;

    invoke-virtual {v3, p0, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 114
    invoke-static {p0, p3}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->autoRefresh(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-object v4, v3

    .line 115
    .restart local v4       #service:Ljava/lang/Object;
    goto :goto_71

    .line 79
    nop

    :pswitch_data_ae
    .packed-switch 0x7f030003
        :pswitch_72
        :pswitch_a
        :pswitch_27
    .end packed-switch
.end method

.method private static setEmptyView(Landroid/widget/RemoteViews;II)V
    .registers 10
    .parameter "views"
    .parameter "viewId"
    .parameter "emptyViewId"

    .prologue
    .line 140
    sget-boolean v5, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->$assertionsDisabled:Z

    if-nez v5, :cond_10

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-ge v5, v6, :cond_10

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 142
    :cond_10
    :try_start_10
    const-string v2, "setEmptyView"

    .line 143
    .local v2, methodName:Ljava/lang/String;
    const/4 v5, 0x2

    new-array v3, v5, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    .line 146
    .local v3, parameters:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    const-class v5, Landroid/widget/RemoteViews;

    invoke-virtual {v5, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 147
    .local v4, setEmptyView:Ljava/lang/reflect/Method;
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v5

    .line 150
    .local v0, arguments:[Ljava/lang/Object;
    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_39
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_39} :catch_3a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_10 .. :try_end_39} :catch_3f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_39} :catch_44
    .catch Ljava/lang/IllegalAccessException; {:try_start_10 .. :try_end_39} :catch_49
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_10 .. :try_end_39} :catch_4e

    .line 162
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v2           #methodName:Ljava/lang/String;
    .end local v3           #parameters:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    .end local v4           #setEmptyView:Ljava/lang/reflect/Method;
    :goto_39
    return-void

    .line 151
    :catch_3a
    move-exception v1

    .line 152
    .local v1, e:Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_39

    .line 153
    .end local v1           #e:Ljava/lang/SecurityException;
    :catch_3f
    move-exception v1

    .line 154
    .local v1, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_39

    .line 155
    .end local v1           #e:Ljava/lang/NoSuchMethodException;
    :catch_44
    move-exception v1

    .line 156
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_39

    .line 157
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_49
    move-exception v1

    .line 158
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_39

    .line 159
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_4e
    move-exception v1

    .line 160
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_39
.end method

.method private static setPendingIntentTemplate(Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;)V
    .registers 10
    .parameter "views"
    .parameter "viewId"
    .parameter "pendingIntentTemplate"

    .prologue
    .line 201
    sget-boolean v5, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->$assertionsDisabled:Z

    if-nez v5, :cond_10

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-ge v5, v6, :cond_10

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 203
    :cond_10
    :try_start_10
    const-string v2, "setPendingIntentTemplate"

    .line 204
    .local v2, methodName:Ljava/lang/String;
    const/4 v5, 0x2

    new-array v3, v5, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v3, v5

    .line 207
    .local v3, parameters:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    const-class v5, Landroid/widget/RemoteViews;

    invoke-virtual {v5, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 208
    .local v4, setPendingIntentTemplate:Ljava/lang/reflect/Method;
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v5

    const/4 v5, 0x1

    aput-object p2, v0, v5

    .line 211
    .local v0, arguments:[Ljava/lang/Object;
    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_35
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_35} :catch_36
    .catch Ljava/lang/NoSuchMethodException; {:try_start_10 .. :try_end_35} :catch_3b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_35} :catch_40
    .catch Ljava/lang/IllegalAccessException; {:try_start_10 .. :try_end_35} :catch_45
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_10 .. :try_end_35} :catch_4a

    .line 223
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v2           #methodName:Ljava/lang/String;
    .end local v3           #parameters:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    .end local v4           #setPendingIntentTemplate:Ljava/lang/reflect/Method;
    :goto_35
    return-void

    .line 212
    :catch_36
    move-exception v1

    .line 213
    .local v1, e:Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_35

    .line 214
    .end local v1           #e:Ljava/lang/SecurityException;
    :catch_3b
    move-exception v1

    .line 215
    .local v1, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_35

    .line 216
    .end local v1           #e:Ljava/lang/NoSuchMethodException;
    :catch_40
    move-exception v1

    .line 217
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_35

    .line 218
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_45
    move-exception v1

    .line 219
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_35

    .line 220
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_4a
    move-exception v1

    .line 221
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_35
.end method

.method private static setRemoteAdapter(Landroid/widget/RemoteViews;IILandroid/content/Intent;)V
    .registers 11
    .parameter "views"
    .parameter "appWidgetId"
    .parameter "viewId"
    .parameter "intent"

    .prologue
    .line 171
    sget-boolean v5, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->$assertionsDisabled:Z

    if-nez v5, :cond_10

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-ge v5, v6, :cond_10

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 173
    :cond_10
    :try_start_10
    const-string v2, "setRemoteAdapter"

    .line 174
    .local v2, methodName:Ljava/lang/String;
    const/4 v5, 0x3

    new-array v3, v5, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-class v6, Landroid/content/Intent;

    aput-object v6, v3, v5

    .line 177
    .local v3, parameters:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    const-class v5, Landroid/widget/RemoteViews;

    invoke-virtual {v5, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 178
    .local v4, setRemoteAdapter:Ljava/lang/reflect/Method;
    const/4 v5, 0x3

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v5

    const/4 v5, 0x2

    aput-object p3, v0, v5

    .line 181
    .local v0, arguments:[Ljava/lang/Object;
    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_41
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_41} :catch_42
    .catch Ljava/lang/NoSuchMethodException; {:try_start_10 .. :try_end_41} :catch_47
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_41} :catch_4c
    .catch Ljava/lang/IllegalAccessException; {:try_start_10 .. :try_end_41} :catch_51
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_10 .. :try_end_41} :catch_56

    .line 193
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v2           #methodName:Ljava/lang/String;
    .end local v3           #parameters:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    .end local v4           #setRemoteAdapter:Ljava/lang/reflect/Method;
    :goto_41
    return-void

    .line 182
    :catch_42
    move-exception v1

    .line 183
    .local v1, e:Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_41

    .line 184
    .end local v1           #e:Ljava/lang/SecurityException;
    :catch_47
    move-exception v1

    .line 185
    .local v1, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_41

    .line 186
    .end local v1           #e:Ljava/lang/NoSuchMethodException;
    :catch_4c
    move-exception v1

    .line 187
    .local v1, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_41

    .line 188
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    :catch_51
    move-exception v1

    .line 189
    .local v1, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_41

    .line 190
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catch_56
    move-exception v1

    .line 191
    .local v1, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_41
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 125
    const-string v4, "com.google.reader.intent.action.CONTENT_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 126
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 127
    .local v1, appWidgetManager:Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v2, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 128
    .local v2, componentName:Landroid/content/ComponentName;
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 129
    .local v0, appWidgetIds:[I
    const v3, 0x7f0b0010

    .line 130
    .local v3, viewId:I
    invoke-static {v1, v0, v3}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->notifyAppWidgetViewDataChanged(Landroid/appwidget/AppWidgetManager;[II)V

    .line 134
    .end local v0           #appWidgetIds:[I
    .end local v1           #appWidgetManager:Landroid/appwidget/AppWidgetManager;
    .end local v2           #componentName:Landroid/content/ComponentName;
    .end local v3           #viewId:I
    :goto_23
    return-void

    .line 132
    :cond_24
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_23
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .registers 19
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetIds"

    .prologue
    .line 51
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 52
    .local v8, packageName:Ljava/lang/String;
    move-object/from16 v3, p3

    .local v3, arr$:[I
    array-length v7, v3

    .local v7, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_8
    if-ge v4, v7, :cond_4d

    aget v2, v3, v4

    .line 53
    .local v2, appWidgetId:I
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/reader/preference/AppWidgetPreferences;->getAccount(Landroid/content/Context;I)Lcom/google/android/accounts/Account;

    move-result-object v1

    .line 54
    .local v1, account:Lcom/google/android/accounts/Account;
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/reader/preference/AppWidgetPreferences;->getStreamId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 55
    .local v9, streamId:Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/reader/preference/AppWidgetPreferences;->getLabel(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, label:Ljava/lang/String;
    if-eqz v1, :cond_46

    if-eqz v9, :cond_46

    .line 58
    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v14, 0xb

    if-ge v13, v14, :cond_49

    const v6, 0x7f030005

    .line 60
    .local v6, layout:I
    :goto_2b
    new-instance v12, Landroid/widget/RemoteViews;

    invoke-direct {v12, v8, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 61
    .local v12, views:Landroid/widget/RemoteViews;
    move-object/from16 v0, p1

    invoke-static {v0, v1, v9}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createItemsUri(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 62
    .local v11, uri:Landroid/net/Uri;
    move-object/from16 v0, p1

    invoke-static {v0, v12, v2, v11, v5}, Lcom/google/android/apps/reader/appwidget/NewsTickerWidget;->bindViews(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 63
    .local v10, updateService:Landroid/content/Intent;
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v12}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 64
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 52
    .end local v6           #layout:I
    .end local v10           #updateService:Landroid/content/Intent;
    .end local v11           #uri:Landroid/net/Uri;
    .end local v12           #views:Landroid/widget/RemoteViews;
    :cond_46
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 58
    :cond_49
    const v6, 0x7f030003

    goto :goto_2b

    .line 69
    .end local v1           #account:Lcom/google/android/accounts/Account;
    .end local v2           #appWidgetId:I
    .end local v5           #label:Ljava/lang/String;
    .end local v9           #streamId:Ljava/lang/String;
    :cond_4d
    return-void
.end method
