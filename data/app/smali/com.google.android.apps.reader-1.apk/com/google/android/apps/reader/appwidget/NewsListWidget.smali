.class public final Lcom/google/android/apps/reader/appwidget/NewsListWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "NewsListWidget.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private bindListView(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "context"
    .parameter "views"
    .parameter "appWidgetId"
    .parameter "account"
    .parameter "streamId"
    .parameter "label"

    .prologue
    const v5, 0x7f0b006a

    .line 71
    invoke-static {p1, p4, p5}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createItemsUri(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 72
    .local v3, uri:Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/reader/appwidget/NewsListWidgetRemoteViewsService;

    invoke-direct {v0, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    .local v0, intent:Landroid/content/Intent;
    const-string v4, "appWidgetId"

    invoke-virtual {v0, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 74
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 75
    invoke-virtual {p2, p3, v5, v0}, Landroid/widget/RemoteViews;->setRemoteAdapter(IILandroid/content/Intent;)V

    .line 76
    const v4, 0x7f0b006b

    invoke-virtual {p2, v5, v4}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    .line 78
    invoke-static {v3, p6}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createViewItemIntentTemplate(Landroid/net/Uri;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 79
    .local v2, template:Landroid/content/Intent;
    invoke-static {p1, v2}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createPendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 80
    .local v1, pendingIntent:Landroid/app/PendingIntent;
    invoke-virtual {p2, v5, v1}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 81
    return-void
.end method

.method private bindLogo(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;)V
    .registers 7
    .parameter "context"
    .parameter "views"
    .parameter "account"

    .prologue
    .line 95
    invoke-static {p1, p3}, Lcom/google/android/apps/reader/fragment/MainScreen;->intent(Landroid/content/Context;Lcom/google/android/accounts/Account;)Landroid/content/Intent;

    move-result-object v0

    .line 96
    .local v0, intent:Landroid/content/Intent;
    invoke-static {p1, v0}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createPendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 97
    .local v1, pendingIntent:Landroid/app/PendingIntent;
    const v2, 0x7f0b0066

    invoke-virtual {p2, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 98
    return-void
.end method

.method private bindRefreshButton(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "views"
    .parameter "account"
    .parameter "streamId"

    .prologue
    const/4 v2, 0x0

    .line 111
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->createRefreshIntent(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 112
    .local v0, intent:Landroid/content/Intent;
    invoke-static {p1, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 113
    .local v1, refresh:Landroid/app/PendingIntent;
    const v2, 0x7f0b0069

    invoke-virtual {p2, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 114
    return-void
.end method

.method private bindTitle(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/CharSequence;)V
    .registers 10
    .parameter "context"
    .parameter "views"
    .parameter "account"
    .parameter "streamId"
    .parameter "label"

    .prologue
    const v3, 0x7f0b0067

    .line 102
    invoke-static {p1, p3, p4}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createItemsUri(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 103
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v2, p5}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createViewIntent(Landroid/net/Uri;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 104
    .local v0, intent:Landroid/content/Intent;
    invoke-static {p1, v0}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createPendingIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 105
    .local v1, pendingIntent:Landroid/app/PendingIntent;
    invoke-virtual {p2, v3, p5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 106
    invoke-virtual {p2, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 107
    return-void
.end method

.method private bindUnreadCount(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "views"
    .parameter "appWidgetId"
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 85
    invoke-static {p4, p5}, Lcom/google/android/apps/reader/provider/ReaderContract$UnreadCounts;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 86
    .local v1, uri:Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.reader.intent.action.BIND"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 87
    .local v0, intent:Landroid/content/Intent;
    const-class v2, Lcom/google/android/apps/reader/appwidget/NewsListWidgetUpdateService;

    invoke-virtual {v0, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 88
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 89
    const-string v2, "com.google.reader.intent.extra.REMOTE_VIEWS"

    invoke-static {p2}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->clone(Landroid/widget/RemoteViews;)Landroid/widget/RemoteViews;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 92
    return-void
.end method

.method private createAutoRefreshIntent(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;
    .registers 8
    .parameter "context"
    .parameter "views"
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 126
    invoke-static {p1, p3, p4}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createItemsUri(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 127
    .local v1, uri:Landroid/net/Uri;
    invoke-static {p1, v1}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->autoRefresh(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 128
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.reader.intent.action.QUERY"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 129
    .local v0, intent:Landroid/content/Intent;
    const-class v2, Lcom/google/android/apps/reader/appwidget/NewsListWidgetUpdateService;

    invoke-virtual {v0, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 130
    return-object v0
.end method

.method private createRefreshIntent(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 117
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->createItemsUri(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 118
    .local v1, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderContract;->refresh(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 119
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.reader.intent.action.QUERY"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 120
    .local v0, intent:Landroid/content/Intent;
    const-class v2, Lcom/google/android/apps/reader/appwidget/NewsListWidgetUpdateService;

    invoke-virtual {v0, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 121
    return-object v0
.end method

.method private updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .registers 19
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetId"

    .prologue
    .line 51
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/preference/AppWidgetPreferences;->getAccount(Landroid/content/Context;I)Lcom/google/android/accounts/Account;

    move-result-object v5

    .line 52
    .local v5, account:Lcom/google/android/accounts/Account;
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/preference/AppWidgetPreferences;->getStreamId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 53
    .local v6, streamId:Ljava/lang/String;
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/preference/AppWidgetPreferences;->getLabel(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    .line 54
    .local v7, label:Ljava/lang/String;
    if-eqz v5, :cond_61

    if-eqz v6, :cond_61

    .line 55
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030043

    invoke-direct {v4, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 56
    .local v4, views:Landroid/widget/RemoteViews;
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->bindLogo(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;)V

    move-object v2, p0

    move-object/from16 v3, p1

    .line 57
    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->bindTitle(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 58
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v4, v5, v6}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->bindRefreshButton(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;Ljava/lang/String;)V

    move-object v8, p0

    move-object/from16 v9, p1

    move-object v10, v4

    move/from16 v11, p3

    move-object v12, v5

    move-object v13, v6

    move-object v14, v7

    .line 59
    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->bindListView(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    move-object v8, p0

    move-object/from16 v9, p1

    move-object v10, v4

    move/from16 v11, p3

    move-object v12, v5

    move-object v13, v6

    .line 60
    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->bindUnreadCount(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/google/android/accounts/Account;Ljava/lang/String;)V

    .line 61
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 62
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v4, v5, v6}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->createAutoRefreshIntent(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 66
    .end local v4           #views:Landroid/widget/RemoteViews;
    :cond_61
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 135
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, action:Ljava/lang/String;
    const-string v5, "com.google.reader.intent.action.CONTENT_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 137
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 138
    .local v2, appWidgetManager:Landroid/appwidget/AppWidgetManager;
    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v3, p1, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .local v3, componentName:Landroid/content/ComponentName;
    invoke-virtual {v2, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 140
    .local v1, appWidgetIds:[I
    const v4, 0x7f0b006a

    .line 141
    .local v4, viewId:I
    invoke-static {v2, v1, v4}, Lcom/google/android/apps/reader/appwidget/ReaderAppWidget;->notifyAppWidgetViewDataChanged(Landroid/appwidget/AppWidgetManager;[II)V

    .line 145
    .end local v1           #appWidgetIds:[I
    .end local v2           #appWidgetManager:Landroid/appwidget/AppWidgetManager;
    .end local v3           #componentName:Landroid/content/ComponentName;
    .end local v4           #viewId:I
    :goto_23
    return-void

    .line 143
    :cond_24
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_23
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .registers 8
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetIds"

    .prologue
    .line 45
    move-object v1, p3

    .local v1, arr$:[I
    array-length v3, v1

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_3
    if-ge v2, v3, :cond_d

    aget v0, v1, v2

    .line 46
    .local v0, appWidgetId:I
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/reader/appwidget/NewsListWidget;->updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 45
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 48
    .end local v0           #appWidgetId:I
    :cond_d
    return-void
.end method
