.class Lcom/google/android/apps/reader/widget/QuickContactView$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "QuickContactView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/widget/QuickContactView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/reader/widget/QuickContactView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/reader/widget/QuickContactView;Landroid/content/ContentResolver;)V
    .registers 3
    .parameter
    .parameter "cr"

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/QuickContactView$QueryHandler;->this$0:Lcom/google/android/apps/reader/widget/QuickContactView;

    .line 100
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 101
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .registers 10
    .parameter "token"
    .parameter "cookie"
    .parameter "cursor"

    .prologue
    .line 105
    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    .line 107
    .local v2, email:Ljava/lang/String;
    if-nez p3, :cond_10

    .line 110
    :try_start_5
    iget-object v5, p0, Lcom/google/android/apps/reader/widget/QuickContactView$QueryHandler;->this$0:Lcom/google/android/apps/reader/widget/QuickContactView;

    #calls: Lcom/google/android/apps/reader/widget/QuickContactView;->showOrCreateContact(Ljava/lang/String;)V
    invoke-static {v5, v2}, Lcom/google/android/apps/reader/widget/QuickContactView;->access$000(Lcom/google/android/apps/reader/widget/QuickContactView;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_2a

    .line 121
    :goto_a
    if-eqz p3, :cond_f

    .line 122
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 125
    :cond_f
    return-void

    .line 111
    :cond_10
    :try_start_10
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_31

    .line 112
    const/4 v5, 0x0

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 113
    .local v0, contactId:J
    const/4 v5, 0x1

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, lookupKey:Ljava/lang/String;
    invoke-static {v0, v1, v3}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 115
    .local v4, lookupUri:Landroid/net/Uri;
    iget-object v5, p0, Lcom/google/android/apps/reader/widget/QuickContactView$QueryHandler;->this$0:Lcom/google/android/apps/reader/widget/QuickContactView;

    #calls: Lcom/google/android/apps/reader/widget/QuickContactView;->showQuickContact(Landroid/net/Uri;)V
    invoke-static {v5, v4}, Lcom/google/android/apps/reader/widget/QuickContactView;->access$100(Lcom/google/android/apps/reader/widget/QuickContactView;Landroid/net/Uri;)V
    :try_end_29
    .catchall {:try_start_10 .. :try_end_29} :catchall_2a

    goto :goto_a

    .line 121
    .end local v0           #contactId:J
    .end local v3           #lookupKey:Ljava/lang/String;
    .end local v4           #lookupUri:Landroid/net/Uri;
    :catchall_2a
    move-exception v5

    if-eqz p3, :cond_30

    .line 122
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_30
    throw v5

    .line 118
    :cond_31
    :try_start_31
    iget-object v5, p0, Lcom/google/android/apps/reader/widget/QuickContactView$QueryHandler;->this$0:Lcom/google/android/apps/reader/widget/QuickContactView;

    #calls: Lcom/google/android/apps/reader/widget/QuickContactView;->showOrCreateContact(Ljava/lang/String;)V
    invoke-static {v5, v2}, Lcom/google/android/apps/reader/widget/QuickContactView;->access$000(Lcom/google/android/apps/reader/widget/QuickContactView;Ljava/lang/String;)V
    :try_end_36
    .catchall {:try_start_31 .. :try_end_36} :catchall_2a

    goto :goto_a
.end method
