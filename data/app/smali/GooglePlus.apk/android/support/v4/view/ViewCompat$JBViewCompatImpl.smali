.class final Landroid/support/v4/view/ViewCompat$JBViewCompatImpl;
.super Landroid/support/v4/view/ViewCompat$ICSViewCompatImpl;
.source "ViewCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/ViewCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "JBViewCompatImpl"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 186
    invoke-direct {p0}, Landroid/support/v4/view/ViewCompat$ICSViewCompatImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;
    .registers 4
    .parameter "view"

    .prologue
    .line 221
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    .line 222
    .local v0, compat:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v0, :cond_c

    .line 223
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;-><init>(Ljava/lang/Object;)V

    .line 225
    :goto_b
    return-object v1

    :cond_c
    const/4 v1, 0x0

    goto :goto_b
.end method

.method public final getImportantForAccessibility(Landroid/view/View;)I
    .registers 3
    .parameter "view"

    .prologue
    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v0

    return v0
.end method

.method public final hasTransientState(Landroid/view/View;)Z
    .registers 3
    .parameter "view"

    .prologue
    .line 189
    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    move-result v0

    return v0
.end method

.method public final postInvalidateOnAnimation(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    .prologue
    .line 197
    invoke-virtual {p1}, Landroid/view/View;->postInvalidateOnAnimation()V

    .line 198
    return-void
.end method

.method public final setImportantForAccessibility(Landroid/view/View;I)V
    .registers 3
    .parameter "view"
    .parameter "mode"

    .prologue
    .line 217
    invoke-virtual {p1, p2}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 218
    return-void
.end method
