.class final Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoJellybeanImpl;
.super Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoIcsImpl;
.source "AccessibilityNodeInfoCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AccessibilityNodeInfoJellybeanImpl"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 636
    invoke-direct {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoIcsImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public final addChild(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4
    .parameter "info"
    .parameter "child"
    .parameter "virtualDescendantId"

    .prologue
    .line 654
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 655
    return-void
.end method

.method public final setSource(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4
    .parameter "info"
    .parameter "root"
    .parameter "virtualDescendantId"

    .prologue
    .line 659
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    .line 660
    return-void
.end method

.method public final setVisibleToUser(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "visibleToUser"

    .prologue
    .line 669
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    .line 670
    return-void
.end method
