.class Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoStubImpl;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoCompat.java"

# interfaces
.implements Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AccessibilityNodeInfoStubImpl"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addAction(Ljava/lang/Object;I)V
    .registers 3
    .parameter "info"
    .parameter "action"

    .prologue
    .line 121
    return-void
.end method

.method public addChild(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4
    .parameter "info"
    .parameter "child"
    .parameter "virtualDescendantId"

    .prologue
    .line 131
    return-void
.end method

.method public getBoundsInParent(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "outBounds"

    .prologue
    .line 146
    return-void
.end method

.method public getBoundsInScreen(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "outBounds"

    .prologue
    .line 151
    return-void
.end method

.method public getContentDescription(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    .line 170
    const/4 v0, 0x0

    return-object v0
.end method

.method public getText(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    .line 185
    const/4 v0, 0x0

    return-object v0
.end method

.method public obtain()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public obtain(Landroid/view/View;)Ljava/lang/Object;
    .registers 3
    .parameter "source"

    .prologue
    .line 105
    const/4 v0, 0x0

    return-object v0
.end method

.method public obtain(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "info"

    .prologue
    .line 115
    const/4 v0, 0x0

    return-object v0
.end method

.method public recycle(Ljava/lang/Object;)V
    .registers 2
    .parameter "info"

    .prologue
    .line 391
    return-void
.end method

.method public setBoundsInParent(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "bounds"

    .prologue
    .line 276
    return-void
.end method

.method public setBoundsInScreen(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "bounds"

    .prologue
    .line 281
    return-void
.end method

.method public setClassName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "className"

    .prologue
    .line 296
    return-void
.end method

.method public setEnabled(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "enabled"

    .prologue
    .line 311
    return-void
.end method

.method public setPackageName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "packageName"

    .prologue
    .line 341
    return-void
.end method

.method public setParent(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3
    .parameter "info"
    .parameter "parent"

    .prologue
    .line 346
    return-void
.end method

.method public setScrollable(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "scrollable"

    .prologue
    .line 356
    return-void
.end method

.method public setSource(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4
    .parameter "info"
    .parameter "root"
    .parameter "virtualDescendantId"

    .prologue
    .line 371
    return-void
.end method

.method public setText(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "text"

    .prologue
    .line 386
    return-void
.end method

.method public setVisibleToUser(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "visibleToUser"

    .prologue
    .line 326
    return-void
.end method
