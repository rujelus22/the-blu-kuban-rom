.class public final Lcom/google/api/services/plusi/model/DataItemJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataItemJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataItem;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataItemJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/DataItemJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataItemJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataItemJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/DataItem;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "actorOid"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/DataKvPairJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "opaqueClientFields"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "notificationType"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/DataActorJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "actor"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "isRead"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "timestamp"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "id"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataItemJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/DataItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataItemJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/DataItem;

    .end local p1
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataItem;->actorOid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataItem;->opaqueClientFields:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataItem;->notificationType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataItem;->isRead:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataItem;->timestamp:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataItem;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/DataItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataItem;-><init>()V

    return-object v0
.end method
