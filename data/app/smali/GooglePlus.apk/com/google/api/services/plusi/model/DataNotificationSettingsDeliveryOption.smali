.class public final Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataNotificationSettingsDeliveryOption.java"


# instance fields
.field public bucketId:Ljava/lang/String;

.field public category:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public enabled:Ljava/lang/Boolean;

.field public enabledForEmail:Ljava/lang/Boolean;

.field public enabledForPhone:Ljava/lang/Boolean;

.field public offnetworkBucketId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
