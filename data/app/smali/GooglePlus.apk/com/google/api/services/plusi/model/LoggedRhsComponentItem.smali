.class public final Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoggedRhsComponentItem.java"


# instance fields
.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public col:Ljava/lang/Integer;

.field public connectSiteId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public gamesLabelId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public person:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public row:Ljava/lang/Integer;

.field public square:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedSquare;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionInfo:Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 52
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
