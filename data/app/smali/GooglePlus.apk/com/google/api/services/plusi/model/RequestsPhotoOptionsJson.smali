.class public final Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "RequestsPhotoOptionsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/RequestsPhotoOptions;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;->INSTANCE:Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "returnDownloadability"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "returnVideoUrls"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "returnOwnerInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "returnComments"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "returnShapes"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "contentFormat"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "returnAsbeUpdates"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "returnPlusOnes"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "returnPhotos"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "returnAlbumInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;->INSTANCE:Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    .end local p1
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnDownloadability:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnVideoUrls:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnOwnerInfo:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnComments:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnShapes:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->contentFormat:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnAsbeUpdates:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPlusOnes:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPhotos:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnAlbumInfo:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;-><init>()V

    return-object v0
.end method
