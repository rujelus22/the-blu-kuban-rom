.class public final Lcom/google/api/services/plusi/model/Profile;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Profile.java"


# instance fields
.field public aboutMeHtml:Lcom/google/api/services/plusi/model/StringField;

.field public birthday:Lcom/google/api/services/plusi/model/BirthdayField;

.field public braggingRights:Lcom/google/api/services/plusi/model/StringField;

.field public canonicalProfileUrl:Ljava/lang/String;

.field public classification:Lcom/google/api/services/plusi/model/Classification;

.field public contactMeChat:Lcom/google/api/services/plusi/model/ContactMeField;

.field public contactMeEmail:Lcom/google/api/services/plusi/model/ContactMeField;

.field public contactMeHangout:Lcom/google/api/services/plusi/model/ContactMeField;

.field public contactMePhone:Lcom/google/api/services/plusi/model/ContactMeField;

.field public contactMeShare:Lcom/google/api/services/plusi/model/ContactMeField;

.field public contributorToLinks:Lcom/google/api/services/plusi/model/Links;

.field public educations:Lcom/google/api/services/plusi/model/Educations;

.field public employments:Lcom/google/api/services/plusi/model/Employments;

.field public enableWallRead:Lcom/google/api/services/plusi/model/BooleanField;

.field public enableWallWrite:Lcom/google/api/services/plusi/model/BooleanField;

.field public entityInfo:Lcom/google/api/services/plusi/model/EntityInfo;

.field public gender:Lcom/google/api/services/plusi/model/Gender;

.field public googleAnalyticsWebPropertyId:Lcom/google/api/services/plusi/model/StringField;

.field public googleMeEnabled:Ljava/lang/Boolean;

.field public homeContact:Lcom/google/api/services/plusi/model/ContactInfo;

.field public inAbuseiamQueue:Ljava/lang/Boolean;

.field public incomingConnections:Lcom/google/api/services/plusi/model/IntField;

.field public legacyPublicUsername:Ljava/lang/String;

.field public links:Lcom/google/api/services/plusi/model/Links;

.field public localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

.field public localUserActivity:Lcom/google/api/services/plusi/model/LocalUserActivity;

.field public locationMapUrl:Ljava/lang/String;

.field public locations:Lcom/google/api/services/plusi/model/Locations;

.field public meLinks:Lcom/google/api/services/plusi/model/Links;

.field public name:Lcom/google/api/services/plusi/model/Name;

.field public nameDisplayOptions:Lcom/google/api/services/plusi/model/NameDisplayOptions;

.field public nickName:Lcom/google/api/services/plusi/model/NickName;

.field public obfuscatedGaiaId:Ljava/lang/String;

.field public occupation:Lcom/google/api/services/plusi/model/StringField;

.field public optedIntoLocal:Ljava/lang/Boolean;

.field public otherLinks:Lcom/google/api/services/plusi/model/Links;

.field public otherNames:Lcom/google/api/services/plusi/model/OtherNames;

.field public outOfBoxDismissed:Ljava/lang/Boolean;

.field public photoIsSilhouette:Ljava/lang/Boolean;

.field public photoUrl:Ljava/lang/String;

.field public plusPageInfo:Lcom/google/api/services/plusi/model/PlusPageInfo;

.field public primaryLink:Lcom/google/api/services/plusi/model/ProfilesLink;

.field public profileBirthdayMissing:Ljava/lang/Boolean;

.field public profileCompletionStats:Lcom/google/api/services/plusi/model/ProfileCompletionStats;

.field public profilePageCrawlable:Ljava/lang/Boolean;

.field public profileState:Lcom/google/api/services/plusi/model/ProfileState;

.field public profileType:Ljava/lang/String;

.field public profileWasAgeRestricted:Ljava/lang/Boolean;

.field public publicUsername:Ljava/lang/String;

.field public relationshipInterests:Lcom/google/api/services/plusi/model/RelationshipInterests;

.field public relationshipStatus:Lcom/google/api/services/plusi/model/RelationshipStatus;

.field public scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

.field public segmentationInfo:Lcom/google/api/services/plusi/model/SegmentationInfo;

.field public showFollowerCounts:Ljava/lang/Boolean;

.field public tabVisibility:Lcom/google/api/services/plusi/model/TabVisibility;

.field public tagLine:Lcom/google/api/services/plusi/model/StringField;

.field public validAgeRestrictions:Ljava/lang/String;

.field public verifiedDomains:Lcom/google/api/services/plusi/model/VerifiedDomains;

.field public workContact:Lcom/google/api/services/plusi/model/ContactInfo;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
