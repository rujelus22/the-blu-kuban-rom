.class public final Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "NearbyStreamRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/NearbyStreamRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/NearbyStreamRequest;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ActivityFiltersJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "activityFilters"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "fieldRequestOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "contentFormat"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/UpdateFilterJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "updateFilter"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "maxResults"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7Json;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "latLongE7"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "continuationToken"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;

    .end local p1
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->contentFormat:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->maxResults:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->latLongE7:Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->continuationToken:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/NearbyStreamRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/NearbyStreamRequest;-><init>()V

    return-object v0
.end method
