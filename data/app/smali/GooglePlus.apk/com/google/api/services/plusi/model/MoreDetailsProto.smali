.class public final Lcom/google/api/services/plusi/model/MoreDetailsProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MoreDetailsProto.java"


# instance fields
.field public providerDetail:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MoreDetailsProtoProviderDetails;",
            ">;"
        }
    .end annotation
.end field

.field public summaryDetail:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DetailProto;",
            ">;"
        }
    .end annotation
.end field

.field public title:Lcom/google/api/services/plusi/model/StoryTitle;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 49
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
