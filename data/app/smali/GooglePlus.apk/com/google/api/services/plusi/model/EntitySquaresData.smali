.class public final Lcom/google/api/services/plusi/model/EntitySquaresData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EntitySquaresData.java"


# instance fields
.field public invite:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntitySquaresDataSquareInvite;",
            ">;"
        }
    .end annotation
.end field

.field public membershipApproved:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipApproved;",
            ">;"
        }
    .end annotation
.end field

.field public membershipRequest:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipRequest;",
            ">;"
        }
    .end annotation
.end field

.field public newModerator:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;",
            ">;"
        }
    .end annotation
.end field

.field public renderSquaresData:Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresData;

.field public subscription:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 47
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
