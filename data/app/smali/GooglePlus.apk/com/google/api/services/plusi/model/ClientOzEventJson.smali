.class public final Lcom/google/api/services/plusi/model/ClientOzEventJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientOzEventJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ClientOzEvent;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ClientOzEventJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ClientOzEventJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientOzEventJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ClientOzEventJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientOzEventJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ClientOzEvent;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/OzEventJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "ozEvent"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/ClientActionDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "actionData"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/google/api/services/plusi/model/ClientOzEventJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "clientTimeMsec"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/ClientOutputDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "startViewData"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/ClientOutputDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "endViewData"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/ClientViewerInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "viewerInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ClientOzEventJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientOzEventJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ClientOzEvent;

    .end local p1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOzEvent;->actionData:Lcom/google/api/services/plusi/model/ClientActionData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOzEvent;->clientTimeMsec:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOzEvent;->startViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOzEvent;->endViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOzEvent;->viewerInfo:Lcom/google/api/services/plusi/model/ClientViewerInfo;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ClientOzEvent;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientOzEvent;-><init>()V

    return-object v0
.end method
