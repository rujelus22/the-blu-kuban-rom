.class public final Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoadSocialNetworkRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "circlesOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "personListOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "systemGroupsOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;

    .end local p1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->circlesOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->personListOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->systemGroupsOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;-><init>()V

    return-object v0
.end method
