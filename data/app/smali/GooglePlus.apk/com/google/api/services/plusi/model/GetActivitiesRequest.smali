.class public final Lcom/google/api/services/plusi/model/GetActivitiesRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetActivitiesRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public continuesToken:Ljava/lang/String;

.field public enableTracing:Ljava/lang/Boolean;

.field public ownerId:Ljava/lang/String;

.field public perspectiveId:Ljava/lang/String;

.field public skipPopularMixin:Ljava/lang/Boolean;

.field public streamParams:Lcom/google/api/services/plusi/model/StreamParams;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
