.class public final Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PhotosOfUserResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosOfUserResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "suggestedQueryResumeToken"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "localplusViewerPhotosQueryResumeToken"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "localplusQueryResumeToken"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "approvedQueryResumeToken"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "errorCode"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "localplusPhotoOfViewer"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "unapprovedPhoto"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "suggestedPhoto"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "unapprovedQueryResumeToken"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "localplusPhoto"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "approvedPhoto"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 34
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;

    .end local p1
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->suggestedQueryResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->localplusViewerPhotosQueryResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->localplusQueryResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->approvedQueryResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->errorCode:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->localplusPhotoOfViewer:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->unapprovedPhoto:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->suggestedPhoto:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->unapprovedQueryResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->localplusPhoto:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->approvedPhoto:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;-><init>()V

    return-object v0
.end method
