.class public final Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosInAlbumResponse.java"


# instance fields
.field public album:Lcom/google/api/services/plusi/model/DataAlbum;

.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public featuredPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

.field public isDownloadable:Ljava/lang/Boolean;

.field public owner:Lcom/google/api/services/plusi/model/DataUser;

.field public photo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 63
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
