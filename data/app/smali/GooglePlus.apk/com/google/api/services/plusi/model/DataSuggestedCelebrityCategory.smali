.class public final Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataSuggestedCelebrityCategory.java"


# instance fields
.field public category:Ljava/lang/String;

.field public categoryName:Ljava/lang/String;

.field public celebrity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;"
        }
    .end annotation
.end field

.field public totalCelebrityCount:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 58
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
