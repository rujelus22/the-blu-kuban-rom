.class public final Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedSuggestionInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "friendSuggestionSummarizedInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "friendSuggestionSummarizedAdditionalInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "explanationType"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "experimentNames"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "numberOfCircleMembersAdded"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "score"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "queryId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/LoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "suggestedCircleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "deprecatedFriendSuggestionSummarizedInfoBitmask"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "placement"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "suggestionType"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "explanationsTypesBitmask"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/LoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "suggestedCircle"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "numberOfCircleMembersRemoved"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;

    .end local p1
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->explanationType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->experimentNames:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->numberOfCircleMembersAdded:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->score:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->queryId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->suggestedCircleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->deprecatedFriendSuggestionSummarizedInfoBitmask:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->placement:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->suggestionType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->explanationsTypesBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->suggestedCircle:Lcom/google/api/services/plusi/model/LoggedCircle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;->numberOfCircleMembersRemoved:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;-><init>()V

    return-object v0
.end method
