.class public final Lcom/google/api/services/plusi/model/FavaDiagnostics;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "FavaDiagnostics.java"


# instance fields
.field public actionNumber:Ljava/lang/Integer;

.field public actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

.field public endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

.field public errorCode:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public experimentIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public isCacheHit:Ljava/lang/Boolean;

.field public jsLoadTimeMs:Ljava/lang/Integer;

.field public jsVersion:Ljava/lang/String;

.field public mainPageId:Ljava/lang/Long;

.field public memoryStats:Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;

.field public numLogicalRequests:Ljava/lang/Integer;

.field public numRequests:Ljava/lang/Integer;

.field public requestId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public requestStats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;",
            ">;"
        }
    .end annotation
.end field

.field public requiredJsLoad:Ljava/lang/Boolean;

.field public screenHeight:Ljava/lang/Integer;

.field public screenWidth:Ljava/lang/Integer;

.field public startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

.field public status:Ljava/lang/String;

.field public timeMs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public timeUsecDelta:Ljava/lang/Long;

.field public totalTimeMs:Ljava/lang/Integer;

.field public tracers:Ljava/lang/String;

.field public viewportHeight:Ljava/lang/Integer;

.field public viewportWidth:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 156
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
