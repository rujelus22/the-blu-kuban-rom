.class public final Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoggedSuggestionInfo.java"


# instance fields
.field public deprecatedFriendSuggestionSummarizedInfoBitmask:Ljava/math/BigInteger;

.field public experimentNames:Ljava/lang/String;

.field public explanationType:Ljava/lang/String;

.field public explanationsTypesBitmask:Ljava/lang/Integer;

.field public friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

.field public friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

.field public numberOfCircleMembersAdded:Ljava/lang/Integer;

.field public numberOfCircleMembersRemoved:Ljava/lang/Integer;

.field public placement:Ljava/lang/Integer;

.field public queryId:Ljava/lang/String;

.field public score:Ljava/lang/Double;

.field public suggestedCircle:Lcom/google/api/services/plusi/model/LoggedCircle;

.field public suggestedCircleMember:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 118
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
