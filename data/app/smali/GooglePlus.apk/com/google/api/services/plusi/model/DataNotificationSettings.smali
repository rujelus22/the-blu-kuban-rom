.class public final Lcom/google/api/services/plusi/model/DataNotificationSettings;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataNotificationSettings.java"


# instance fields
.field public alternateEmail:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataNotificationSettingsEmail;",
            ">;"
        }
    .end annotation
.end field

.field public categoryInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataNotificationSettingsNotificationsSettingsCategoryInfo;",
            ">;"
        }
    .end annotation
.end field

.field public defaultDestinationId:Ljava/lang/String;

.field public deliveryOption:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;",
            ">;"
        }
    .end annotation
.end field

.field public emailAddress:Ljava/lang/String;

.field public emailDeliveryOption:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;",
            ">;"
        }
    .end annotation
.end field

.field public isEmailAddressEditable:Ljava/lang/Boolean;

.field public showEntitiesSettings:Ljava/lang/Boolean;

.field public smsDeliveryOption:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;",
            ">;"
        }
    .end annotation
.end field

.field public smsDestinationId:Ljava/lang/String;

.field public uncircledCanNotify:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 49
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
