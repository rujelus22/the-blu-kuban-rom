.class public final Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "FavaDiagnosticsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/FavaDiagnostics;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;->INSTANCE:Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/FavaDiagnostics;

    const/16 v1, 0x20

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "timeMs"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "errorCode"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedTypeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "actionType"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "screenWidth"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedTypeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "endView"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "actionNumber"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "numRequests"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStatsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "memoryStats"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "requestId"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "viewportWidth"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "mainPageId"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "requestStats"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "status"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "tracers"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "jsLoadTimeMs"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "jsVersion"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "viewportHeight"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "requiredJsLoad"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "numLogicalRequests"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "screenHeight"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    sget-object v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "timeUsecDelta"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "experimentIds"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "totalTimeMs"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "isCacheHit"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-class v3, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedTypeJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "startView"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 47
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;->INSTANCE:Lcom/google/api/services/plusi/model/FavaDiagnosticsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;

    .end local p1
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->timeMs:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->errorCode:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->screenWidth:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionNumber:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->numRequests:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->memoryStats:Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->requestId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->viewportWidth:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->mainPageId:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->requestStats:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->status:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->tracers:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->jsLoadTimeMs:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->jsVersion:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->viewportHeight:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->requiredJsLoad:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->numLogicalRequests:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->screenHeight:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->timeUsecDelta:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->experimentIds:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->totalTimeMs:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->isCacheHit:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnostics;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnostics;-><init>()V

    return-object v0
.end method
