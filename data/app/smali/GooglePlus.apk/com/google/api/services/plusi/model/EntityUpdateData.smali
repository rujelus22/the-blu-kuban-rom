.class public final Lcom/google/api/services/plusi/model/EntityUpdateData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EntityUpdateData.java"


# instance fields
.field public activity:Lcom/google/api/services/plusi/model/Update;

.field public commentId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public contextComment:Lcom/google/api/services/plusi/model/Comment;

.field public countData:Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;

.field public ownerOid:Ljava/lang/String;

.field public safeAnnotationHtml:Ljava/lang/String;

.field public safeTitleHtml:Ljava/lang/String;

.field public summary:Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;

.field public unreadCommentCount:Ljava/lang/Integer;

.field public unreadReshareCount:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
