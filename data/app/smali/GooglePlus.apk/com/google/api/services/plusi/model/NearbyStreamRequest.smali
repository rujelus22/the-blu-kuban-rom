.class public final Lcom/google/api/services/plusi/model/NearbyStreamRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "NearbyStreamRequest.java"


# instance fields
.field public activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public contentFormat:Ljava/lang/String;

.field public continuationToken:Ljava/lang/String;

.field public enableTracing:Ljava/lang/Boolean;

.field public fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

.field public latLongE7:Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;

.field public maxResults:Ljava/lang/Integer;

.field public updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
