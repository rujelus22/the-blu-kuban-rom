.class public final Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ReportAbusePhotoRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataAbuseReportJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "abuseReport"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "photoId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "signedClusterId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "ownerId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "reportToken"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;

    .end local p1
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->photoId:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->signedClusterId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->ownerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->reportToken:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;-><init>()V

    return-object v0
.end method
