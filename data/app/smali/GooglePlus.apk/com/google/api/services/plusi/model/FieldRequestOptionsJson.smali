.class public final Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "FieldRequestOptionsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/FieldRequestOptions;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;->INSTANCE:Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "includeSparksData"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "includeUrls"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "includeBuzzData"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "includeDebugData"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "includeEmbedsData"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "includeSharingData"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "includeLegacyMediaData"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "includePhotosData"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "includeDeprecatedFields"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;->INSTANCE:Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    .end local p1
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeSparksData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeUrls:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeBuzzData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeDebugData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeEmbedsData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeSharingData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeLegacyMediaData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includePhotosData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeDeprecatedFields:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FieldRequestOptions;-><init>()V

    return-object v0
.end method
