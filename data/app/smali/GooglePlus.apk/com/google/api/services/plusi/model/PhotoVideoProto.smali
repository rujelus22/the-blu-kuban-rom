.class public final Lcom/google/api/services/plusi/model/PhotoVideoProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotoVideoProto.java"


# instance fields
.field public media:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MediaProto;",
            ">;"
        }
    .end annotation
.end field

.field public morePhotos:Lcom/google/api/services/plusi/model/PlacePageLink;

.field public moreVideos:Lcom/google/api/services/plusi/model/PlacePageLink;

.field public navbar:Lcom/google/api/services/plusi/model/NavbarProto;

.field public photoSize:Ljava/lang/String;

.field public resultsRange:Lcom/google/api/services/plusi/model/ResultsRangeProto;

.field public totalNumPhotos:Ljava/lang/Integer;

.field public totalNumVideos:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 81
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
