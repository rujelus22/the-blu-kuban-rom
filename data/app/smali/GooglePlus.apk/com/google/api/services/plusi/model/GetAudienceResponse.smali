.class public final Lcom/google/api/services/plusi/model/GetAudienceResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetAudienceResponse.java"


# instance fields
.field public aclJson:Ljava/lang/String;

.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public gaiaAudienceCount:Ljava/lang/Integer;

.field public nonGaiaAudienceCount:Ljava/lang/Integer;

.field public person:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Person;",
            ">;"
        }
    .end annotation
.end field

.field public updateId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 92
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
