.class public final Lcom/google/api/services/plusi/model/GetNotificationsResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetNotificationsResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public notificationsData:Lcom/google/api/services/plusi/model/DataNotificationsData;

.field public obfuscatedGaiaId:Ljava/lang/String;

.field public refreshRequired:Ljava/lang/Boolean;

.field public viewerData:Lcom/google/api/services/plusi/model/ViewerData;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
