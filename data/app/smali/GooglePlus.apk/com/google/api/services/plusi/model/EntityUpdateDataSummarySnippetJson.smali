.class public final Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityUpdateDataSummarySnippetJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "actorOid"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "headerSanitizedHtml"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "resharedActorOid"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "entityActorOid"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "bodySanitizedHtml"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "activityContentSanitizedHtml"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;

    .end local p1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->actorOid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->headerSanitizedHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->resharedActorOid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->entityActorOid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->bodySanitizedHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->activityContentSanitizedHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;-><init>()V

    return-object v0
.end method
