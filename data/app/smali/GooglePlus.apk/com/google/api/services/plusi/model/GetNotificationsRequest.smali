.class public final Lcom/google/api/services/plusi/model/GetNotificationsRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetNotificationsRequest.java"


# instance fields
.field public clientVersion:Ljava/math/BigInteger;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public continuationToken:Ljava/lang/String;

.field public dontCoalesce:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public enableTracing:Ljava/lang/Boolean;

.field public featuredNotificationId:Ljava/lang/String;

.field public fetchUnreadCount:Ljava/lang/Boolean;

.field public filter:Ljava/lang/String;

.field public geographicLocation:Ljava/lang/String;

.field public loadViewerData:Ljava/lang/Boolean;

.field public maxResults:Ljava/lang/Long;

.field public notificationId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public notificationsResponseOptions:Lcom/google/api/services/plusi/model/NotificationsResponseOptions;

.field public oldestNotificationTimeUsec:Ljava/math/BigInteger;

.field public renderContextLocation:Ljava/lang/String;

.field public setPushEnabled:Ljava/lang/Boolean;

.field public summarySnippets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
