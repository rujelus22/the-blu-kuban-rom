.class public final Lcom/google/api/services/plusi/model/ActivityRequestData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ActivityRequestData.java"


# instance fields
.field public activityCountOnly:Ljava/lang/Boolean;

.field public activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

.field public fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

.field public maxResults:Ljava/lang/Integer;

.field public shownActivitiesBlob:Ljava/lang/String;

.field public updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
