.class public final Lcom/google/api/services/plusi/model/DataNotificationsData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataNotificationsData.java"


# instance fields
.field public actor:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;"
        }
    .end annotation
.end field

.field public coalescedItem:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
            ">;"
        }
    .end annotation
.end field

.field public continuationToken:Ljava/lang/String;

.field public featuredItem:Lcom/google/api/services/plusi/model/DataCoalescedItem;

.field public filteredId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public hasMoreNotifications:Ljava/lang/Boolean;

.field public hasMoreUnreadNotifications:Ljava/lang/Boolean;

.field public lastReadTime:Ljava/lang/Double;

.field public latestNotificationTime:Ljava/lang/Double;

.field public unreadCount:Ljava/lang/Integer;

.field public unreadCountData:Lcom/google/api/services/plusi/model/DataUnreadCountData;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 76
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
