.class public final Lcom/google/api/services/plusi/model/LoggedContactDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedContactDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoggedContactData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoggedContactDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/LoggedContactDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedContactDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoggedContactDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedContactDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/LoggedContactData;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "jobTitleCount"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "emailCount"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "phoneCount"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "hasPhoto"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "addressCount"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "hasName"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoggedContactDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/LoggedContactDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedContactDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/LoggedContactData;

    .end local p1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedContactData;->jobTitleCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedContactData;->emailCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedContactData;->phoneCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedContactData;->hasPhoto:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedContactData;->addressCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedContactData;->hasName:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/LoggedContactData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedContactData;-><init>()V

    return-object v0
.end method
