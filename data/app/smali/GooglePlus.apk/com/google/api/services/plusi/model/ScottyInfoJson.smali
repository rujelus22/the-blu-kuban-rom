.class public final Lcom/google/api/services/plusi/model/ScottyInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ScottyInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ScottyInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ScottyInfoJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ScottyInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ScottyInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ScottyInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ScottyInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ScottyInfo;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/CustomAgentDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "customAgentData"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ScottyInfoJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ScottyInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ScottyInfoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ScottyInfo;

    .end local p1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ScottyInfo;->customAgentData:Lcom/google/api/services/plusi/model/CustomAgentData;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ScottyInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ScottyInfo;-><init>()V

    return-object v0
.end method
