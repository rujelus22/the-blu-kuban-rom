.class public final Lcom/google/api/services/plusi/model/ActionTarget;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ActionTarget.java"


# instance fields
.field public actionSource:Ljava/lang/String;

.field public activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

.field public activityId:Ljava/lang/String;

.field public autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

.field public billboardImpression:Lcom/google/api/services/plusi/model/LoggedBillboardImpression;

.field public billboardPromoAction:Lcom/google/api/services/plusi/model/LoggedBillboardPromoAction;

.field public categoryId:Ljava/lang/Integer;

.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public circleMember:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public commentId:Ljava/lang/String;

.field public connectSiteId:Ljava/lang/Integer;

.field public deprecatedCircleId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public deprecatedSettingsNotificationType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public entityTypeId:Ljava/lang/Integer;

.field public externalUrl:Ljava/lang/String;

.field public frame:Lcom/google/api/services/plusi/model/LoggedFrame;

.field public gadgetId:Ljava/math/BigInteger;

.field public gadgetPlayId:Ljava/lang/String;

.field public gaiaId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public intrCelebsClick:Lcom/google/api/services/plusi/model/LoggedIntrCelebsClick;

.field public iphFlowId:Ljava/lang/Long;

.field public iphStepId:Ljava/lang/String;

.field public isUnreadNotification:Ljava/lang/Boolean;

.field public labelId:Ljava/math/BigInteger;

.field public notificationId:Ljava/lang/String;

.field public notificationSlot:Ljava/lang/Integer;

.field public notificationTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/NotificationTypes;",
            ">;"
        }
    .end annotation
.end field

.field public notificationWidgetPostReloadBuildLabel:Ljava/lang/String;

.field public notificationWidgetPreReloadBuildLabel:Ljava/lang/String;

.field public notificationWidgetUpTimeBeforeReload:Ljava/lang/Integer;

.field public numUnreadNotifications:Ljava/lang/Integer;

.field public page:Ljava/lang/Integer;

.field public photoAlbumId:Ljava/lang/String;

.field public photoAlbumIdDeprecated:Ljava/math/BigInteger;

.field public photoAlbumType:Ljava/lang/Integer;

.field public photoCount:Ljava/lang/Integer;

.field public photoId:Ljava/math/BigInteger;

.field public plusEventId:Ljava/lang/String;

.field public previousNumUnreadNotifications:Ljava/lang/Integer;

.field public profileData:Ljava/lang/String;

.field public promoType:Ljava/lang/Integer;

.field public questionsOneboxQuery:Ljava/lang/String;

.field public region:Ljava/lang/String;

.field public rhsComponent:Lcom/google/api/services/plusi/model/LoggedRhsComponent;

.field public ribbonClick:Lcom/google/api/services/plusi/model/LoggedRibbonClick;

.field public ribbonOrder:Lcom/google/api/services/plusi/model/LoggedRibbonOrder;

.field public settingsNotificationType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SettingsNotificationType;",
            ">;"
        }
    .end annotation
.end field

.field public shortcutTask:Ljava/lang/String;

.field public square:Lcom/google/api/services/plusi/model/LoggedSquare;

.field public suggestionInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionSummary:Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfo;

.field public tab:Ljava/lang/Integer;

.field public updateStreamPosition:Ljava/lang/Integer;

.field public volumeChange:Lcom/google/api/services/plusi/model/VolumeChange;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 51
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
