.class public final Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityGadgetDataGadgetMessageJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "authToken"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "body"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "hangoutId"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageGadgetJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "gadget"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "anchorText"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "navParam"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "notificationId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "imageUrl"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;

    .end local p1
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->authToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->body:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->hangoutId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->gadget:Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessageGadget;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->anchorText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->navParam:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->notificationId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;->imageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityGadgetDataGadgetMessage;-><init>()V

    return-object v0
.end method
