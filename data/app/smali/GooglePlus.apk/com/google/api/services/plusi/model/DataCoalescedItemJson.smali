.class public final Lcom/google/api/services/plusi/model/DataCoalescedItemJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataCoalescedItemJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataCoalescedItemJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/DataCoalescedItemJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataCoalescedItemJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataCoalescedItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataCoalescedItemJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    const/16 v1, 0x16

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "category"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "pushEnabled"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "entityReferenceType"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "isEntityDeleted"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/DataKvPairJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "opaqueClientFields"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/EntityEntityDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "entityData"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "filterType"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "isRead"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "entityReference"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "involvedActorOid"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "debug"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "isMuted"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/api/services/plusi/model/A2aDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "a2aData"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "timestamp"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-class v3, Lcom/google/api/services/plusi/model/DataActionJson;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "action"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "hashCode"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "coalescingCode"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 40
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataCoalescedItemJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/DataCoalescedItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataCoalescedItemJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    .end local p1
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->category:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->pushEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReferenceType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->isEntityDeleted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->opaqueClientFields:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->filterType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->isRead:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReference:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->involvedActorOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->debug:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->isMuted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->a2aData:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->hashCode:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->coalescingCode:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataCoalescedItem;-><init>()V

    return-object v0
.end method
