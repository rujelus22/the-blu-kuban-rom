.class public final Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "WritePlaceReviewRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;

    const/16 v1, 0x13

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "reviewText"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/ZagatAspectRatingProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "oldZagatAspectRatings"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/PriceProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "price"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "cid"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/PriceLevelsProtoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "priceLevel"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "source"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/ZagatAspectRatingProtoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "zagatAspectRatings"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "userCountryCode"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "includeSuggestedPlaces"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/AbuseSignalsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "abuseSignals"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "userLanguage"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;

    .end local p1
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->reviewText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->oldZagatAspectRatings:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->price:Lcom/google/api/services/plusi/model/PriceProto;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->cid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->source:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->zagatAspectRatings:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->userCountryCode:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->includeSuggestedPlaces:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->abuseSignals:Lcom/google/api/services/plusi/model/AbuseSignals;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->userLanguage:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;-><init>()V

    return-object v0
.end method
