.class public final Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataCircleMembershipModificationParams.java"


# instance fields
.field public addCircleMembersActionSource:Ljava/lang/String;

.field public includeIsFollowing:Ljava/lang/Boolean;

.field public person:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 49
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
