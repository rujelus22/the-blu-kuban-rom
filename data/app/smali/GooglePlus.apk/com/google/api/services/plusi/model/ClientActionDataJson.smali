.class public final Lcom/google/api/services/plusi/model/ClientActionDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientActionDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ClientActionData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ClientActionDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ClientActionDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientActionDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ClientActionDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientActionDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ClientActionData;

    const/16 v1, 0x1f

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionSummaryInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "suggestionSummaryInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedIntrCelebsClickJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "intrCelebsClick"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedRibbonClickJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "ribbonClick"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "photoId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "photoAlbumId"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "labelId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedBillboardPromoActionJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "billboardPromoAction"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "gadgetId"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "rhsComponent"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "autoComplete"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "autoCompleteQuery"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedSquareJson;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "square"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedBillboardImpressionJson;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "billboardImpression"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "obfuscatedGaiaId"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "plusEventId"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "circleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedRibbonOrderJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string v3, "ribbonOrder"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ClientActionDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ClientActionDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientActionDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ClientActionData;

    .end local p1
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->suggestionSummaryInfo:Lcom/google/api/services/plusi/model/ClientLoggedSuggestionSummaryInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->intrCelebsClick:Lcom/google/api/services/plusi/model/ClientLoggedIntrCelebsClick;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->ribbonClick:Lcom/google/api/services/plusi/model/ClientLoggedRibbonClick;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->photoId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->photoAlbumId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->labelId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->billboardPromoAction:Lcom/google/api/services/plusi/model/ClientLoggedBillboardPromoAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->gadgetId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->suggestionInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->rhsComponent:Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->autoComplete:Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->autoCompleteQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->square:Lcom/google/api/services/plusi/model/ClientLoggedSquare;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->billboardImpression:Lcom/google/api/services/plusi/model/ClientLoggedBillboardImpression;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->obfuscatedGaiaId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->plusEventId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->circleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientActionData;->ribbonOrder:Lcom/google/api/services/plusi/model/ClientLoggedRibbonOrder;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ClientActionData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientActionData;-><init>()V

    return-object v0
.end method
