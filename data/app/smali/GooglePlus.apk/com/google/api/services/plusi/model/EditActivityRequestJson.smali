.class public final Lcom/google/api/services/plusi/model/EditActivityRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EditActivityRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EditActivityRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EditActivityRequestJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EditActivityRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EditActivityRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EditActivityRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/EditActivityRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EditActivityRequest;

    const/16 v1, 0x10

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "mediaJson"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "renderContextLocation"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "contentFormat"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "annotation"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/EditSegmentsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "editSegments"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "externalId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "isReshare"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "preserveExistingAttachment"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "embed"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "updateText"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "deleteLocation"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EditActivityRequestJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EditActivityRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/EditActivityRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EditActivityRequest;

    .end local p1
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->mediaJson:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->renderContextLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->contentFormat:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->annotation:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->editSegments:Lcom/google/api/services/plusi/model/EditSegments;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->externalId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->isReshare:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->preserveExistingAttachment:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->embed:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->updateText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->deleteLocation:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EditActivityRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EditActivityRequest;-><init>()V

    return-object v0
.end method
