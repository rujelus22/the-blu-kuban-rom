.class public final Lcom/google/api/services/plusi/model/Magazine;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Magazine.java"


# instance fields
.field public about:Lcom/google/api/services/plusi/model/EmbedClientItem;

.field public buttonStyle:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public logoHrefUrl:Ljava/lang/String;

.field public logoImageUrl:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public offers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Offer;",
            ">;"
        }
    .end annotation
.end field

.field public text:Ljava/lang/String;

.field public thumbnailUrl:Ljava/lang/String;

.field public titleIconUrl:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 111
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
