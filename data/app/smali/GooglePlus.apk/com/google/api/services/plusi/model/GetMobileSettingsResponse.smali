.class public final Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetMobileSettingsResponse.java"


# instance fields
.field public application:Lcom/google/api/services/plusi/model/MobileSettingsApplication;

.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public preference:Lcom/google/api/services/plusi/model/MobilePreference;

.field public requestError:Ljava/lang/String;

.field public shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

.field public user:Lcom/google/api/services/plusi/model/MobileSettingsUser;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
