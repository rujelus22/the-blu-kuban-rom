.class public final Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ReportAbusePhotoRequest.java"


# instance fields
.field public abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public ownerId:Ljava/lang/String;

.field public photoId:Ljava/lang/Long;

.field public reportToken:Ljava/lang/String;

.field public signedClusterId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
