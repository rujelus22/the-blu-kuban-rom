.class public final Lcom/google/api/services/plusi/model/HangoutConsumer;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "HangoutConsumer.java"


# instance fields
.field public attendees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;"
        }
    .end annotation
.end field

.field public hashedRoomId:Ljava/lang/String;

.field public isBroadcastInvite:Ljava/lang/Boolean;

.field public isSelfPost:Ljava/lang/Boolean;

.field public minorsNotAllowed:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public preferredLanguage:Ljava/lang/String;

.field public region:Ljava/lang/String;

.field public startContext:Lcom/google/api/services/plusi/model/HangoutStartContext;

.field public status:Ljava/lang/String;

.field public topicMaybeNsfw:Ljava/lang/Boolean;

.field public totalAttendeeCount:Ljava/lang/Integer;

.field public url:Ljava/lang/String;

.field public youtubeLiveId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 57
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
