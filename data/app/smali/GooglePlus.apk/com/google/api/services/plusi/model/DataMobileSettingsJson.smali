.class public final Lcom/google/api/services/plusi/model/DataMobileSettingsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataMobileSettingsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataMobileSettings;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataMobileSettingsJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/DataMobileSettingsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataMobileSettingsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataMobileSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataMobileSettingsJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/DataMobileSettings;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataMobileSettingsCountryJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "selectedCountry"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "mobileNumber"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "pin"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "isNumberAdded"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/DataMobileSettingsCountryJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "country"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "hasPushTarget"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "emailFrequency"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "isSmsPostingDisabled"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/DataMobileSettingsDeliveryWindowJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "deliveryWindow"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "isInternationalSmsServiceNumber"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "isSmsEnabled"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "smsServiceNumber"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "userRegionCodeFromPhoneNumber"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "mobileNotificationType"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "smsFrequency"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataMobileSettingsJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/DataMobileSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataMobileSettingsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/DataMobileSettings;

    .end local p1
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->selectedCountry:Lcom/google/api/services/plusi/model/DataMobileSettingsCountry;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->mobileNumber:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->pin:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->isNumberAdded:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->country:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->hasPushTarget:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->emailFrequency:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->isSmsPostingDisabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->deliveryWindow:Lcom/google/api/services/plusi/model/DataMobileSettingsDeliveryWindow;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->isInternationalSmsServiceNumber:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->isSmsEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->smsServiceNumber:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->userRegionCodeFromPhoneNumber:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->mobileNotificationType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataMobileSettings;->smsFrequency:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/DataMobileSettings;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataMobileSettings;-><init>()V

    return-object v0
.end method
