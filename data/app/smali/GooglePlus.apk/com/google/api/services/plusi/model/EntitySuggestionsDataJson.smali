.class public final Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntitySuggestionsDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntitySuggestionsData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EntitySuggestionsData;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "suggestedObfuscatedGaiaId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EntitySuggestionsData;

    .end local p1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySuggestionsData;->suggestedObfuscatedGaiaId:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EntitySuggestionsData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntitySuggestionsData;-><init>()V

    return-object v0
.end method
