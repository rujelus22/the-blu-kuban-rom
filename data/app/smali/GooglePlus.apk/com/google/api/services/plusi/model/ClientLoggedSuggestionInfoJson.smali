.class public final Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedSuggestionInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;

    const/16 v1, 0x13

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "scoringExperiments"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "friendSuggestionSummarizedInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "friendSuggestionSummarizedAdditionalInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "explanationType"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "numberOfCircleMembersAdded"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "score"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "queryId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "suggestedCircleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "deprecatedFriendSuggestionSummarizedInfoBitmask"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "placement"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "suggestionType"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "explanationsTypesBitmask"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "seed"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "suggestedCircle"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "numberOfCircleMembersRemoved"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;

    .end local p1
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->scoringExperiments:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->explanationType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->numberOfCircleMembersAdded:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->score:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->queryId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->suggestedCircleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->deprecatedFriendSuggestionSummarizedInfoBitmask:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->placement:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->suggestionType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->explanationsTypesBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->seed:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->suggestedCircle:Lcom/google/api/services/plusi/model/ClientLoggedCircle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;->numberOfCircleMembersRemoved:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;-><init>()V

    return-object v0
.end method
