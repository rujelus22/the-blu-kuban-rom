.class public final Lcom/google/api/services/plusi/model/OutputData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "OutputData.java"


# instance fields
.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public circleMember:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public containerPropertyId:Ljava/lang/String;

.field public filterCircle:Lcom/google/api/services/plusi/model/LoggedCircle;

.field public filterType:Ljava/lang/Integer;

.field public getStartedStepIndex:Ljava/lang/Integer;

.field public interest:Ljava/lang/String;

.field public photo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public photoAlbumId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public profile:Lcom/google/api/services/plusi/model/LoggedProfile;

.field public streamSort:Ljava/lang/String;

.field public suggestionInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedSuggestionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public tab:Ljava/lang/Integer;

.field public update:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public userInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/UserInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 79
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
