.class public final Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MobileOutOfBoxRequest.java"


# instance fields
.field public action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

.field public clientType:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public continueUrl:Ljava/lang/String;

.field public enableTracing:Ljava/lang/Boolean;

.field public input:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxInputField;",
            ">;"
        }
    .end annotation
.end field

.field public integrated:Ljava/lang/Boolean;

.field public invitationToken:Ljava/lang/String;

.field public partnerId:Ljava/lang/String;

.field public postMessageTargetOrigin:Ljava/lang/String;

.field public upgradeOrigin:Ljava/lang/String;

.field public webClientPathAndQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 129
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
