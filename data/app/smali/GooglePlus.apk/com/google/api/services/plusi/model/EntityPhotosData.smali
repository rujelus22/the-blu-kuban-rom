.class public final Lcom/google/api/services/plusi/model/EntityPhotosData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EntityPhotosData.java"


# instance fields
.field public album:Lcom/google/api/services/plusi/model/EntityAlbumData;

.field public numPhotos:Ljava/lang/Integer;

.field public numPhotosDeleted:Ljava/lang/Integer;

.field public numTagged:Ljava/lang/Integer;

.field public numTagsRemoved:Ljava/lang/Integer;

.field public numVideos:Ljava/lang/Integer;

.field public photo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public photoCrop:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntityPhotoCrop;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 58
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
