.class public final Lcom/google/api/services/plusi/model/PostCommentRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PostCommentRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PostCommentRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PostCommentRequestJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/PostCommentRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PostCommentRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PostCommentRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PostCommentRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/PostCommentRequest;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "commentText"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "botGuardResponse"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "renderContextLocation"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/EditSegmentsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "commentSegments"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "contentFormat"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "clientId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "activityId"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/google/api/services/plusi/model/PostCommentRequestJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "creationTimeMs"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PostCommentRequestJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/PostCommentRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PostCommentRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/PostCommentRequest;

    .end local p1
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->commentText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->botGuardResponse:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->renderContextLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->commentSegments:Lcom/google/api/services/plusi/model/EditSegments;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->contentFormat:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->clientId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->activityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->creationTimeMs:Ljava/lang/Long;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/PostCommentRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PostCommentRequest;-><init>()V

    return-object v0
.end method
