.class public final Lcom/google/api/services/plusi/model/FindMorePeopleResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "FindMorePeopleResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public experimentNames:Ljava/lang/String;

.field public experimentThreshold:Ljava/lang/Double;

.field public imported:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataImportedPerson;",
            ">;"
        }
    .end annotation
.end field

.field public portraitVersion:Ljava/lang/String;

.field public queryId:Ljava/lang/String;

.field public suggestion:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 70
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
