.class public final Lcom/google/api/services/plusi/model/PhotosOfUserRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosOfUserRequest.java"


# instance fields
.field public approvedResumeToken:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public maxResults:Ljava/lang/Integer;

.field public ownerId:Ljava/lang/String;

.field public photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

.field public suggestedResumeToken:Ljava/lang/String;

.field public unapprovedResumeToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
