.class public final Lcom/google/api/services/plusi/model/EntityEntityData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EntityEntityData.java"


# instance fields
.field public birthdayData:Lcom/google/api/services/plusi/model/EntityBirthdayData;

.field public circles:Lcom/google/api/services/plusi/model/EntityCirclesData;

.field public gadget:Lcom/google/api/services/plusi/model/EntityGadgetData;

.field public hangout:Lcom/google/api/services/plusi/model/EntityHangoutData;

.field public legacy:Lcom/google/api/services/plusi/model/EntityLegacyClientData;

.field public photos:Lcom/google/api/services/plusi/model/EntityPhotosData;

.field public plusEvents:Lcom/google/api/services/plusi/model/EntityEventsData;

.field public recommendedPeopleData:Lcom/google/api/services/plusi/model/EntityRecommendedPeopleData;

.field public segmentedShareData:Lcom/google/api/services/plusi/model/EntitySegmentedShareData;

.field public squares:Lcom/google/api/services/plusi/model/EntitySquaresData;

.field public suggestions:Lcom/google/api/services/plusi/model/EntitySuggestionsData;

.field public summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

.field public targetShared:Lcom/google/api/services/plusi/model/EntityTargetSharedData;

.field public update:Lcom/google/api/services/plusi/model/EntityUpdateData;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
