.class public final Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosCreateCommentRequest.java"


# instance fields
.field public comment:Ljava/lang/String;

.field public commentSegments:Lcom/google/api/services/plusi/model/EditSegments;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public obfuscatedOwnerId:Ljava/lang/String;

.field public photoId:Ljava/lang/Long;

.field public returnAllComments:Ljava/lang/Boolean;

.field public timestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
