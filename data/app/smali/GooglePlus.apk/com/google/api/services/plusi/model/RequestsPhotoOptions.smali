.class public final Lcom/google/api/services/plusi/model/RequestsPhotoOptions;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "RequestsPhotoOptions.java"


# instance fields
.field public contentFormat:Ljava/lang/String;

.field public returnAlbumInfo:Ljava/lang/Boolean;

.field public returnAsbeUpdates:Ljava/lang/Boolean;

.field public returnComments:Ljava/lang/Boolean;

.field public returnDownloadability:Ljava/lang/Boolean;

.field public returnOwnerInfo:Ljava/lang/Boolean;

.field public returnPhotos:Ljava/lang/Boolean;

.field public returnPlusOnes:Ljava/lang/Boolean;

.field public returnShapes:Ljava/lang/Boolean;

.field public returnVideoUrls:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
