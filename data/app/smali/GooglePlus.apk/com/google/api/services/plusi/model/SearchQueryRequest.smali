.class public final Lcom/google/api/services/plusi/model/SearchQueryRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SearchQueryRequest.java"


# instance fields
.field public activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public contentFormat:Ljava/lang/String;

.field public enableTracing:Ljava/lang/Boolean;

.field public peopleRequestData:Lcom/google/api/services/plusi/model/PeopleRequestData;

.field public searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
