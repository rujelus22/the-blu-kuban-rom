.class public final Lcom/google/api/services/plusi/model/DataSuggestionAction;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataSuggestionAction.java"


# instance fields
.field public accepted:Ljava/lang/Boolean;

.field public actionType:Ljava/lang/String;

.field public circleId:Lcom/google/api/services/plusi/model/DataCircleId;

.field public suggestedEntityId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedEntityId;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCircleMemberId;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionUi:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 67
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
