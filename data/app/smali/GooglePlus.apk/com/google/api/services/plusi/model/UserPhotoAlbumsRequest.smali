.class public final Lcom/google/api/services/plusi/model/UserPhotoAlbumsRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "UserPhotoAlbumsRequest.java"


# instance fields
.field public albumTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public maxPreviewCount:Ljava/lang/Integer;

.field public maxResults:Ljava/lang/Integer;

.field public offset:Ljava/lang/Integer;

.field public ownerId:Ljava/lang/String;

.field public sharedAlbumsOnly:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
