.class public final Lcom/google/api/services/plusi/model/SearchResults;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SearchResults.java"


# instance fields
.field public activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

.field public debugInfoHtml:Ljava/lang/String;

.field public decorationMode:Ljava/lang/String;

.field public peopleResults:Lcom/google/api/services/plusi/model/PeopleResults;

.field public spellingSuggestions:Lcom/google/api/services/plusi/model/SpellingSuggestions;

.field public squareResults:Lcom/google/api/services/plusi/model/SquareResults;

.field public status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
