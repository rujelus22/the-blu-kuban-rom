.class public final Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ExternalEntityKey.java"

# interfaces
.implements Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKeyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;",
        "Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;",
        ">;",
        "Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKeyOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private domain_:Ljava/lang/Object;

.field private id_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 342
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->domain_:Ljava/lang/Object;

    .line 378
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->id_:Ljava/lang/Object;

    .line 225
    return-void
.end method

.method static synthetic access$100()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    .registers 1

    .prologue
    .line 219
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;-><init>()V

    return-object v0
.end method

.method private clear()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    .registers 2

    .prologue
    .line 235
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 236
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->domain_:Ljava/lang/Object;

    .line 237
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    .line 238
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->id_:Ljava/lang/Object;

    .line 239
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    .line 240
    return-object p0
.end method

.method private clone()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    .registers 3

    .prologue
    .line 244
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    .registers 5
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 313
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 314
    .local v0, tag:I
    sparse-switch v0, :sswitch_data_28

    .line 319
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 321
    :sswitch_d
    return-object p0

    .line 326
    :sswitch_e
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    .line 327
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->domain_:Ljava/lang/Object;

    goto :goto_0

    .line 331
    :sswitch_1b
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    .line 332
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->id_:Ljava/lang/Object;

    goto :goto_0

    .line 314
    :sswitch_data_28
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_1b
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 3

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_10
    return-object v0
.end method

.method public final buildPartial()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    .registers 6

    .prologue
    .line 270
    new-instance v1, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;-><init>(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;B)V

    .line 271
    .local v1, result:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    .line 272
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 273
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    .line 274
    const/4 v2, 0x1

    .line 276
    :cond_f
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->domain_:Ljava/lang/Object;

    #setter for: Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->domain_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->access$302(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1b

    .line 278
    or-int/lit8 v2, v2, 0x2

    .line 280
    :cond_1b
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->id_:Ljava/lang/Object;

    #setter for: Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->id_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->access$402(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    #setter for: Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->access$502(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;I)I

    .line 282
    return-object v1
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 219
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 219
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 297
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 305
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 297
    goto :goto_9

    .line 301
    :cond_e
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1a

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    move v0, v1

    .line 305
    goto :goto_b

    :cond_1a
    move v2, v0

    .line 301
    goto :goto_16
.end method

.method public final mergeFrom(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    .registers 4
    .parameter "other"

    .prologue
    .line 286
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 293
    :cond_6
    :goto_6
    return-object p0

    .line 287
    :cond_7
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->hasDomain()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 288
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDomain()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->domain_:Ljava/lang/Object;

    .line 290
    :cond_21
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->hasId()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 291
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_33

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_33
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->id_:Ljava/lang/Object;

    goto :goto_6
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 219
    check-cast p1, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method
