.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HangoutStartContext.java"

# interfaces
.implements Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarksOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatencyMarks"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private clientLaunch_:J

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private serverCreateRedirectEnd_:J

.field private serverCreateRoomEnd_:J

.field private serverCreateRoomStart_:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const-wide/16 v1, 0x0

    .line 1369
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;-><init>()V

    .line 1370
    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    .line 1371
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 948
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1006
    iput-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedIsInitialized:B

    .line 1032
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    .line 948
    return-void
.end method

.method private constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;)V
    .registers 4
    .parameter "builder"

    .prologue
    const/4 v1, -0x1

    .line 946
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    .line 1006
    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedIsInitialized:B

    .line 1032
    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    .line 947
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 941
    invoke-direct {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;)V

    return-void
.end method

.method static synthetic access$1302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 941
    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    return-wide p1
.end method

.method static synthetic access$1402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 941
    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    return-wide p1
.end method

.method static synthetic access$1502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 941
    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    return-wide p1
.end method

.method static synthetic access$1602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 941
    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    return-wide p1
.end method

.method static synthetic access$1702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 941
    iput p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .registers 1

    .prologue
    .line 952
    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .registers 1

    .prologue
    .line 1132
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->access$1100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .registers 2
    .parameter "prototype"

    .prologue
    .line 1135
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->access$1100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getClientLaunch()J
    .registers 3

    .prologue
    .line 967
    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    return-wide v0
.end method

.method public final getSerializedSize()I
    .registers 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1034
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    .line 1035
    .local v0, size:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_a

    move v1, v0

    .line 1055
    .end local v0           #size:I
    .local v1, size:I
    :goto_9
    return v1

    .line 1037
    .end local v1           #size:I
    .restart local v0       #size:I
    :cond_a
    const/4 v0, 0x0

    .line 1038
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_19

    .line 1039
    iget-wide v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    .line 1042
    :cond_19
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_26

    .line 1043
    iget-wide v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    invoke-static {v5, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1046
    :cond_26
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_34

    .line 1047
    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1050
    :cond_34
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_43

    .line 1051
    iget-wide v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    invoke-static {v6, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1054
    :cond_43
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    move v1, v0

    .line 1055
    .end local v0           #size:I
    .restart local v1       #size:I
    goto :goto_9
.end method

.method public final getServerCreateRedirectEnd()J
    .registers 3

    .prologue
    .line 997
    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    return-wide v0
.end method

.method public final getServerCreateRoomEnd()J
    .registers 3

    .prologue
    .line 987
    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    return-wide v0
.end method

.method public final getServerCreateRoomStart()J
    .registers 3

    .prologue
    .line 977
    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    return-wide v0
.end method

.method public final hasClientLaunch()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 964
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasServerCreateRedirectEnd()Z
    .registers 3

    .prologue
    .line 994
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasServerCreateRoomEnd()Z
    .registers 3

    .prologue
    .line 984
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasServerCreateRoomStart()Z
    .registers 3

    .prologue
    .line 974
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 1008
    iget-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedIsInitialized:B

    .line 1009
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_b

    if-ne v0, v1, :cond_9

    .line 1012
    :goto_8
    return v1

    .line 1009
    :cond_9
    const/4 v1, 0x0

    goto :goto_8

    .line 1011
    :cond_b
    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedIsInitialized:B

    goto :goto_8
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 941
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->access$1100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 941
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->access$1100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1062
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 7
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1017
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getSerializedSize()I

    .line 1018
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_11

    .line 1019
    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 1021
    :cond_11
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1c

    .line 1022
    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 1024
    :cond_1c
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_28

    .line 1025
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 1027
    :cond_28
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_35

    .line 1028
    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 1030
    :cond_35
    return-void
.end method
