.class public final Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "A2a.java"

# interfaces
.implements Lcom/google/apps/tacotown/proto/A2a$HangoutOccupantOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;",
        "Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;",
        ">;",
        "Lcom/google/apps/tacotown/proto/A2a$HangoutOccupantOrBuilder;"
    }
.end annotation


# instance fields
.field private avatarUrl_:Ljava/lang/Object;

.field private bitField0_:I

.field private name_:Ljava/lang/Object;

.field private obfuscatedGaiaId_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 3404
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 3536
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->name_:Ljava/lang/Object;

    .line 3572
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->avatarUrl_:Ljava/lang/Object;

    .line 3608
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->obfuscatedGaiaId_:Ljava/lang/Object;

    .line 3405
    return-void
.end method

.method static synthetic access$3900()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;
    .registers 1

    .prologue
    .line 3399
    new-instance v0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    invoke-direct {v0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;-><init>()V

    return-object v0
.end method

.method private clear()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;
    .registers 2

    .prologue
    .line 3415
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3416
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->name_:Ljava/lang/Object;

    .line 3417
    iget v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    .line 3418
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->avatarUrl_:Ljava/lang/Object;

    .line 3419
    iget v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    .line 3420
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->obfuscatedGaiaId_:Ljava/lang/Object;

    .line 3421
    iget v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    .line 3422
    return-object p0
.end method

.method private clone()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;
    .registers 3

    .prologue
    .line 3426
    new-instance v0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    invoke-direct {v0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->buildPartial()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->mergeFrom(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;)Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;
    .registers 5
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3502
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3503
    .local v0, tag:I
    sparse-switch v0, :sswitch_data_36

    .line 3508
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3510
    :sswitch_d
    return-object p0

    .line 3515
    :sswitch_e
    iget v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    .line 3516
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->name_:Ljava/lang/Object;

    goto :goto_0

    .line 3520
    :sswitch_1b
    iget v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    .line 3521
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->avatarUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 3525
    :sswitch_28
    iget v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    .line 3526
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->obfuscatedGaiaId_:Ljava/lang/Object;

    goto :goto_0

    .line 3503
    nop

    :sswitch_data_36
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_1b
        0x1a -> :sswitch_28
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 3

    .prologue
    .line 3399
    invoke-virtual {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->buildPartial()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_10
    return-object v0
.end method

.method public final buildPartial()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;
    .registers 6

    .prologue
    .line 3452
    new-instance v1, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;-><init>(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;B)V

    .line 3453
    .local v1, result:Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;
    iget v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    .line 3454
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 3455
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    .line 3456
    const/4 v2, 0x1

    .line 3458
    :cond_f
    iget-object v3, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->name_:Ljava/lang/Object;

    #setter for: Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->name_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->access$4102(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3459
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1b

    .line 3460
    or-int/lit8 v2, v2, 0x2

    .line 3462
    :cond_1b
    iget-object v3, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->avatarUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->avatarUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->access$4202(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3463
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_27

    .line 3464
    or-int/lit8 v2, v2, 0x4

    .line 3466
    :cond_27
    iget-object v3, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->obfuscatedGaiaId_:Ljava/lang/Object;

    #setter for: Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->obfuscatedGaiaId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->access$4302(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3467
    #setter for: Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->access$4402(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;I)I

    .line 3468
    return-object v1
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 3399
    invoke-virtual {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->buildPartial()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 3399
    invoke-direct {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->clear()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 3399
    invoke-direct {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->clear()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 3399
    invoke-direct {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->clone()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 3399
    invoke-direct {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->clone()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3399
    invoke-direct {p0}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->clone()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 3399
    invoke-static {}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->getDefaultInstance()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 3399
    invoke-static {}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->getDefaultInstance()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3486
    iget v2, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 3494
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 3486
    goto :goto_9

    .line 3490
    :cond_e
    iget v2, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1a

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    move v0, v1

    .line 3494
    goto :goto_b

    :cond_1a
    move v2, v0

    .line 3490
    goto :goto_16
.end method

.method public final mergeFrom(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;)Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;
    .registers 4
    .parameter "other"

    .prologue
    .line 3472
    invoke-static {}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->getDefaultInstance()Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 3482
    :cond_6
    :goto_6
    return-object p0

    .line 3473
    :cond_7
    invoke-virtual {p1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->hasName()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 3474
    invoke-virtual {p1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    iget v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->name_:Ljava/lang/Object;

    .line 3476
    :cond_21
    invoke-virtual {p1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->hasAvatarUrl()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 3477
    invoke-virtual {p1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->getAvatarUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_33

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_33
    iget v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->avatarUrl_:Ljava/lang/Object;

    .line 3479
    :cond_3b
    invoke-virtual {p1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->hasObfuscatedGaiaId()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3480
    invoke-virtual {p1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4d
    iget v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->obfuscatedGaiaId_:Ljava/lang/Object;

    goto :goto_6
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3399
    invoke-direct {p0, p1, p2}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 3399
    check-cast p1, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->mergeFrom(Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant;)Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3399
    invoke-direct {p0, p1, p2}, Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/tacotown/proto/A2a$HangoutOccupant$Builder;

    move-result-object v0

    return-object v0
.end method
