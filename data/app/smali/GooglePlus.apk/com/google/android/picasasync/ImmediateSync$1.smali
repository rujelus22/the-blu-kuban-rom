.class final Lcom/google/android/picasasync/ImmediateSync$1;
.super Lcom/google/android/picasasync/ImmediateSync$Task;
.source "ImmediateSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/picasasync/ImmediateSync;->requestSyncAlbumList(Ljava/lang/String;[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/picasasync/ImmediateSync;

.field final synthetic val$accountArgs:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/picasasync/ImmediateSync;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/picasasync/ImmediateSync$1;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    iput-object p3, p0, Lcom/google/android/picasasync/ImmediateSync$1;->val$accountArgs:[Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/picasasync/ImmediateSync$Task;-><init>(Lcom/google/android/picasasync/ImmediateSync;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final doSync()I
    .registers 25

    .prologue
    .line 164
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v21

    if-nez v21, :cond_10

    .line 165
    const-string v21, "ImmediateSync"

    const-string v22, "master auto sync is off"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const/16 v21, 0x0

    .line 236
    :goto_f
    return v21

    .line 169
    :cond_10
    const-string v21, "ImmediateSync.albums"

    invoke-static/range {v21 .. v21}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v16

    .line 171
    .local v16, statsId:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/picasasync/ImmediateSync$1;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    move-object/from16 v21, v0

    #getter for: Lcom/google/android/picasasync/ImmediateSync;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/google/android/picasasync/ImmediateSync;->access$100(Lcom/google/android/picasasync/ImmediateSync;)Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v17

    .line 173
    .local v17, sync:Lcom/google/android/picasasync/PicasaSyncHelper;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/picasasync/ImmediateSync$1;->val$accountArgs:[Ljava/lang/String;

    .line 174
    .local v5, accounts:[Ljava/lang/String;
    if-nez v5, :cond_83

    .line 177
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/picasasync/PicasaSyncHelper;->getUsers()Ljava/util/ArrayList;

    move-result-object v20

    .line 178
    .local v20, users:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/picasasync/UserEntry;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v4, accountList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/picasasync/ImmediateSync$1;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    move-object/from16 v21, v0

    #getter for: Lcom/google/android/picasasync/ImmediateSync;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/google/android/picasasync/ImmediateSync;->access$100(Lcom/google/android/picasasync/ImmediateSync;)Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/google/android/picasasync/PicasaFacade;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;

    move-result-object v10

    .line 180
    .local v10, facade:Lcom/google/android/picasasync/PicasaFacade;
    invoke-virtual {v10}, Lcom/google/android/picasasync/PicasaFacade;->getAuthority()Ljava/lang/String;

    move-result-object v7

    .line 181
    .local v7, authority:Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, i:I
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v15

    .local v15, n:I
    :goto_4a
    if-ge v11, v15, :cond_71

    .line 182
    move-object/from16 v0, v20

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/picasasync/UserEntry;

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    .line 183
    .local v3, account:Ljava/lang/String;
    new-instance v21, Landroid/accounts/Account;

    const-string v22, "com.google"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v3, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-static {v0, v7}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v8

    .line 185
    .local v8, autoSyncEnabled:Z
    if-eqz v8, :cond_6e

    .line 186
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_6e
    add-int/lit8 v11, v11, 0x1

    goto :goto_4a

    .line 189
    .end local v3           #account:Ljava/lang/String;
    .end local v8           #autoSyncEnabled:Z
    :cond_71
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    .end local v5           #accounts:[Ljava/lang/String;
    check-cast v5, [Ljava/lang/String;

    .line 193
    .end local v4           #accountList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7           #authority:Ljava/lang/String;
    .end local v10           #facade:Lcom/google/android/picasasync/PicasaFacade;
    .end local v11           #i:I
    .end local v15           #n:I
    .end local v20           #users:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/picasasync/UserEntry;>;"
    .restart local v5       #accounts:[Ljava/lang/String;
    :cond_83
    :try_start_83
    const-string v21, "ImmediateSync"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "sync album list:"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v5

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " account(s)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    new-instance v18, Landroid/content/SyncResult;

    invoke-direct/range {v18 .. v18}, Landroid/content/SyncResult;-><init>()V

    .line 195
    .local v18, syncResult:Landroid/content/SyncResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/picasasync/ImmediateSync$1;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    move-object/from16 v22, v0

    monitor-enter v22
    :try_end_ac
    .catchall {:try_start_83 .. :try_end_ac} :catchall_107

    .line 196
    :try_start_ac
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/picasasync/ImmediateSync$1;->syncInterrupted()Z

    move-result v21

    if-eqz v21, :cond_c0

    .line 197
    monitor-exit v22
    :try_end_b3
    .catchall {:try_start_ac .. :try_end_b3} :catchall_104

    .line 236
    const-string v21, "picasa.sync.metadata"

    move/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    const/16 v21, 0x1

    goto/16 :goto_f

    .line 199
    :cond_c0
    :try_start_c0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->createSyncContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/picasasync/ImmediateSync$1;->syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    .line 200
    monitor-exit v22
    :try_end_d5
    .catchall {:try_start_c0 .. :try_end_d5} :catchall_104

    .line 201
    move-object v6, v5

    .local v6, arr$:[Ljava/lang/String;
    :try_start_d6
    array-length v14, v5

    .local v14, len$:I
    const/4 v12, 0x0

    .local v12, i$:I
    :goto_d8
    if-ge v12, v14, :cond_15f

    aget-object v3, v6, v12

    .line 202
    .restart local v3       #account:Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/google/android/picasasync/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;
    :try_end_e1
    .catchall {:try_start_d6 .. :try_end_e1} :catchall_107

    move-result-object v19

    .line 204
    .local v19, userEntry:Lcom/google/android/picasasync/UserEntry;
    const/4 v13, 0x0

    .line 206
    .local v13, isPicasaAccount:Z
    :try_start_e3
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/google/android/picasasync/PicasaSyncHelper;->isPicasaAccount(Ljava/lang/String;)Z
    :try_end_e8
    .catchall {:try_start_e3 .. :try_end_e8} :catchall_107
    .catch Ljava/lang/Exception; {:try_start_e3 .. :try_end_e8} :catch_112

    move-result v13

    .line 210
    :goto_e9
    if-nez v13, :cond_11f

    .line 211
    :try_start_eb
    const-string v21, "ImmediateSync"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "not picasa account, ignore: "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_101
    add-int/lit8 v12, v12, 0x1

    goto :goto_d8

    .line 200
    .end local v3           #account:Ljava/lang/String;
    .end local v6           #arr$:[Ljava/lang/String;
    .end local v12           #i$:I
    .end local v13           #isPicasaAccount:Z
    .end local v14           #len$:I
    .end local v19           #userEntry:Lcom/google/android/picasasync/UserEntry;
    :catchall_104
    move-exception v21

    monitor-exit v22

    throw v21
    :try_end_107
    .catchall {:try_start_eb .. :try_end_107} :catchall_107

    .line 236
    .end local v18           #syncResult:Landroid/content/SyncResult;
    :catchall_107
    move-exception v21

    const-string v22, "picasa.sync.metadata"

    move/from16 v0, v16

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    throw v21

    .line 207
    .restart local v3       #account:Ljava/lang/String;
    .restart local v6       #arr$:[Ljava/lang/String;
    .restart local v12       #i$:I
    .restart local v13       #isPicasaAccount:Z
    .restart local v14       #len$:I
    .restart local v18       #syncResult:Landroid/content/SyncResult;
    .restart local v19       #userEntry:Lcom/google/android/picasasync/UserEntry;
    :catch_112
    move-exception v9

    .line 208
    .local v9, e:Ljava/lang/Exception;
    :try_start_113
    const-string v21, "ImmediateSync"

    const-string v22, "check picasa account failed"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_e9

    .line 215
    .end local v9           #e:Ljava/lang/Exception;
    :cond_11f
    if-eqz v19, :cond_178

    .line 216
    const-string v21, "ImmediateSync"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "sync albums for "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/picasasync/ImmediateSync$1;->syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->setAccount(Ljava/lang/String;)Z

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/picasasync/ImmediateSync$1;->syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->syncAlbumsForUser(Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;Lcom/google/android/picasasync/UserEntry;)V

    .line 225
    :goto_153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/picasasync/ImmediateSync$1;->syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->syncInterrupted()Z

    move-result v21

    if-eqz v21, :cond_101

    .line 229
    .end local v3           #account:Ljava/lang/String;
    .end local v13           #isPicasaAccount:Z
    .end local v19           #userEntry:Lcom/google/android/picasasync/UserEntry;
    :cond_15f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/picasasync/ImmediateSync$1;->syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->syncInterrupted()Z
    :try_end_168
    .catchall {:try_start_113 .. :try_end_168} :catchall_107

    move-result v21

    if-eqz v21, :cond_191

    const/16 v21, 0x1

    .line 236
    :goto_16d
    const-string v22, "picasa.sync.metadata"

    move/from16 v0, v16

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    goto/16 :goto_f

    .line 223
    .restart local v3       #account:Ljava/lang/String;
    .restart local v13       #isPicasaAccount:Z
    .restart local v19       #userEntry:Lcom/google/android/picasasync/UserEntry;
    :cond_178
    :try_start_178
    const-string v21, "ImmediateSync"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "no userEntry for "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_153

    .line 229
    .end local v3           #account:Ljava/lang/String;
    .end local v13           #isPicasaAccount:Z
    .end local v19           #userEntry:Lcom/google/android/picasasync/UserEntry;
    :cond_191
    invoke-virtual/range {v18 .. v18}, Landroid/content/SyncResult;->hasError()Z
    :try_end_194
    .catchall {:try_start_178 .. :try_end_194} :catchall_107

    move-result v21

    if-eqz v21, :cond_19a

    const/16 v21, 0x2

    goto :goto_16d

    :cond_19a
    const/16 v21, 0x0

    goto :goto_16d
.end method
