.class abstract Lcom/google/android/picasasync/ImmediateSync$Task;
.super Ljava/lang/Object;
.source "ImmediateSync.java"

# interfaces
.implements Lcom/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/ImmediateSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "Task"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public refCount:I

.field public syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

.field public syncResultCode:I

.field public final taskId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/picasasync/ImmediateSync;


# direct methods
.method constructor <init>(Lcom/google/android/picasasync/ImmediateSync;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "taskId"

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->refCount:I

    .line 50
    iput-object p2, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->taskId:Ljava/lang/String;

    .line 51
    return-void
.end method

.method private run$44b8b4b6()Ljava/lang/Void;
    .registers 5

    .prologue
    .line 63
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/picasasync/ImmediateSync$Task;->doSync()I

    move-result v0

    .line 65
    .local v0, result:I
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    monitor-enter v2
    :try_end_7
    .catchall {:try_start_0 .. :try_end_7} :catchall_19

    .line 66
    :try_start_7
    iget v1, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_e

    .line 67
    iput v0, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    .line 69
    :cond_e
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_7 .. :try_end_f} :catchall_16

    .line 71
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    #calls: Lcom/google/android/picasasync/ImmediateSync;->completeTask(Lcom/google/android/picasasync/ImmediateSync$Task;)V
    invoke-static {v1, p0}, Lcom/google/android/picasasync/ImmediateSync;->access$000(Lcom/google/android/picasasync/ImmediateSync;Lcom/google/android/picasasync/ImmediateSync$Task;)V

    .line 73
    const/4 v1, 0x0

    return-object v1

    .line 69
    :catchall_16
    move-exception v1

    :try_start_17
    monitor-exit v2

    throw v1
    :try_end_19
    .catchall {:try_start_17 .. :try_end_19} :catchall_19

    .line 71
    .end local v0           #result:I
    :catchall_19
    move-exception v1

    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    #calls: Lcom/google/android/picasasync/ImmediateSync;->completeTask(Lcom/google/android/picasasync/ImmediateSync$Task;)V
    invoke-static {v2, p0}, Lcom/google/android/picasasync/ImmediateSync;->access$000(Lcom/google/android/picasasync/ImmediateSync;Lcom/google/android/picasasync/ImmediateSync$Task;)V

    throw v1
.end method


# virtual methods
.method final addRequester()Z
    .registers 3

    .prologue
    .line 92
    iget v0, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    if-nez v0, :cond_11

    .line 94
    :cond_9
    iget v0, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->refCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->refCount:I

    .line 95
    const/4 v0, 0x1

    .line 97
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method protected abstract doSync()I
.end method

.method public final bridge synthetic run(Lcom/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/picasasync/ImmediateSync$Task;->run$44b8b4b6()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final syncInterrupted()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 77
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->this$0:Lcom/google/android/picasasync/ImmediateSync;

    monitor-enter v1

    .line 78
    :try_start_4
    iget v2, p0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    if-ne v2, v0, :cond_a

    :goto_8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_c

    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    .line 79
    :catchall_c
    move-exception v0

    monitor-exit v1

    throw v0
.end method
