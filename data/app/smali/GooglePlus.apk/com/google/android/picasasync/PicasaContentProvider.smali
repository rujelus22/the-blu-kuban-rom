.class public Lcom/google/android/picasasync/PicasaContentProvider;
.super Landroid/content/ContentProvider;
.source "PicasaContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/PicasaContentProvider$WhereEntry;
    }
.end annotation


# static fields
.field private static final ALBUM_TABLE_NAME:Ljava/lang/String;

.field private static final ALBUM_TYPE_WHERE:Ljava/lang/String;

.field private static final PHOTO_TABLE_NAME:Ljava/lang/String;

.field private static PROJECTION_CONTENT_URL:[Ljava/lang/String;

.field private static PROJECTION_SCREENNAIL_URL:[Ljava/lang/String;

.field private static PROJECTION_THUMBNAIL_URL:[Ljava/lang/String;

.field private static final SETTING_DEFAULTS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SETTING_DEPRECATED:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final USER_TABLE_NAME:Ljava/lang/String;


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

.field private mPicasaStoreFacade:Lcom/google/android/picasastore/PicasaStoreFacade;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 132
    sget-object v0, Lcom/google/android/picasasync/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->USER_TABLE_NAME:Ljava/lang/String;

    .line 133
    sget-object v0, Lcom/google/android/picasasync/AlbumEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TABLE_NAME:Ljava/lang/String;

    .line 134
    sget-object v0, Lcom/google/android/picasasync/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_id in (SELECT album_id FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/picasasync/PicasaContentProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE content_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TYPE_WHERE:Ljava/lang/String;

    .line 299
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    .line 300
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    .line 302
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_picasa_on_wifi_only"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_roaming"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_battery"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "sync_photo_on_mobile"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "auto_upload_enabled"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "auto_upload_account_name"

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "auto_upload_account_type"

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "sync_on_wifi_only"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "video_upload_wifi_only"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "thumbnail_url"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->PROJECTION_THUMBNAIL_URL:[Ljava/lang/String;

    .line 499
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "content_url"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->PROJECTION_CONTENT_URL:[Ljava/lang/String;

    .line 500
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "screennail_url"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->PROJECTION_SCREENNAIL_URL:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 129
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 179
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mPicasaStoreFacade:Lcom/google/android/picasastore/PicasaStoreFacade;

    .line 363
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    .prologue
    .line 129
    sget-object v0, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TABLE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private static getItemIdFromUri(Landroid/net/Uri;)J
    .registers 4
    .parameter "uri"

    .prologue
    .line 491
    :try_start_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_e
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_e} :catch_10

    move-result-wide v0

    .line 494
    :goto_f
    return-wide v0

    .line 493
    :catch_10
    move-exception v0

    const-string v0, "PicasaContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot get id from: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const-wide/16 v0, -0x1

    goto :goto_f
.end method

.method private static getLastSegmentAsLong(Landroid/net/Uri;J)J
    .registers 11
    .parameter "uri"
    .parameter "defaultValue"

    .prologue
    const-wide/16 v3, -0x1

    .line 608
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 609
    .local v2, segments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_21

    .line 610
    const-string v5, "PicasaContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "parse fail: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    :goto_20
    return-wide v3

    .line 613
    :cond_21
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 615
    .local v1, last:Ljava/lang/String;
    :try_start_2d
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_30
    .catch Ljava/lang/NumberFormatException; {:try_start_2d .. :try_end_30} :catch_32

    move-result-wide v3

    goto :goto_20

    .line 616
    :catch_32
    move-exception v0

    .line 617
    .local v0, e:Ljava/lang/NumberFormatException;
    const-string v5, "PicasaContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "pasre fail:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_20
.end method

.method private lookupAlbumCoverUrl(J)Ljava/lang/String;
    .registers 13
    .parameter "id"

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 503
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 504
    .local v8, context:Landroid/content/Context;
    invoke-static {v8}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 505
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v1, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->PROJECTION_THUMBNAIL_URL:[Ljava/lang/String;

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 509
    .local v9, cursor:Landroid/database/Cursor;
    if-eqz v9, :cond_32

    :try_start_25
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_32

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_2f
    .catchall {:try_start_25 .. :try_end_2f} :catchall_3f

    move-result v1

    if-eqz v1, :cond_36

    .line 512
    :cond_32
    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_35
    return-object v5

    .line 510
    :cond_36
    const/4 v1, 0x0

    :try_start_37
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3f

    move-result-object v5

    .line 512
    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_35

    :catchall_3f
    move-exception v1

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private lookupContentUrl(JLjava/lang/String;)Ljava/lang/String;
    .registers 14
    .parameter "id"
    .parameter "type"

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 518
    if-nez p3, :cond_6

    const-string p3, "full"

    .line 520
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 521
    .local v8, context:Landroid/content/Context;
    const-string v1, "full"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_42

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->PROJECTION_CONTENT_URL:[Ljava/lang/String;

    .line 524
    .local v2, projection:[Ljava/lang/String;
    :goto_14
    invoke-static {v8}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 525
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v1, Lcom/google/android/picasasync/PicasaContentProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 529
    .local v9, cursor:Landroid/database/Cursor;
    if-eqz v9, :cond_3e

    :try_start_31
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3e

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_3b
    .catchall {:try_start_31 .. :try_end_3b} :catchall_4e

    move-result v1

    if-eqz v1, :cond_45

    .line 532
    :cond_3e
    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_41
    return-object v5

    .line 521
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v2           #projection:[Ljava/lang/String;
    .end local v9           #cursor:Landroid/database/Cursor;
    :cond_42
    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->PROJECTION_SCREENNAIL_URL:[Ljava/lang/String;

    goto :goto_14

    .line 530
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2       #projection:[Ljava/lang/String;
    .restart local v9       #cursor:Landroid/database/Cursor;
    :cond_45
    const/4 v1, 0x0

    :try_start_46
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_4e

    move-result-object v5

    .line 532
    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_41

    :catchall_4e
    move-exception v1

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private declared-synchronized querySettings$7be7850c([Ljava/lang/String;)Landroid/database/Cursor;
    .registers 11
    .parameter "projection"

    .prologue
    .line 317
    monitor-enter p0

    :try_start_1
    new-instance v2, Lcom/google/android/picasastore/PicasaMatrixCursor;

    invoke-direct {v2, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 318
    .local v2, cursor:Lcom/google/android/picasastore/PicasaMatrixCursor;
    array-length v6, p1

    new-array v1, v6, [Ljava/lang/Object;

    .line 319
    .local v1, columnValues:[Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 320
    .local v5, resolver:Landroid/content/ContentResolver;
    const/4 v3, 0x0

    .local v3, i:I
    array-length v4, p1

    .local v4, n:I
    :goto_13
    if-ge v3, v4, :cond_6c

    .line 321
    aget-object v0, p1, v3

    .line 322
    .local v0, column:Ljava/lang/String;
    sget-object v6, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4a

    .line 323
    sget-object v6, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_32

    .line 324
    sget-object v6, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v1, v3

    .line 320
    :cond_2f
    :goto_2f
    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    .line 327
    :cond_32
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unknown column: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_47
    .catchall {:try_start_1 .. :try_end_47} :catchall_47

    .line 317
    .end local v0           #column:Ljava/lang/String;
    .end local v1           #columnValues:[Ljava/lang/Object;
    .end local v2           #cursor:Lcom/google/android/picasastore/PicasaMatrixCursor;
    .end local v3           #i:I
    .end local v4           #n:I
    .end local v5           #resolver:Landroid/content/ContentResolver;
    :catchall_47
    move-exception v6

    monitor-exit p0

    throw v6

    .line 330
    .restart local v0       #column:Ljava/lang/String;
    .restart local v1       #columnValues:[Ljava/lang/Object;
    .restart local v2       #cursor:Lcom/google/android/picasastore/PicasaMatrixCursor;
    .restart local v3       #i:I
    .restart local v4       #n:I
    .restart local v5       #resolver:Landroid/content/ContentResolver;
    :cond_4a
    :try_start_4a
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "com.google.android.picasasync."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v3

    .line 331
    aget-object v6, v1, v3

    if-nez v6, :cond_2f

    .line 332
    sget-object v6, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v1, v3

    goto :goto_2f

    .line 335
    .end local v0           #column:Ljava/lang/String;
    :cond_6c
    invoke-virtual {v2, v1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_6f
    .catchall {:try_start_4a .. :try_end_6f} :catchall_47

    .line 336
    monitor-exit p0

    return-object v2
.end method

.method private resetSettings()Z
    .registers 6

    .prologue
    .line 673
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 674
    .local v2, values:Landroid/content/ContentValues;
    sget-object v3, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 675
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_f

    .line 677
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2b
    invoke-direct {p0, v2}, Lcom/google/android/picasasync/PicasaContentProvider;->updateSettings(Landroid/content/ContentValues;)Z

    move-result v3

    return v3
.end method

.method private updateSettings(Landroid/content/ContentValues;)Z
    .registers 13
    .parameter "values"

    .prologue
    const/4 v8, 0x0

    .line 644
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 645
    .local v4, resolver:Landroid/content/ContentResolver;
    const/4 v0, 0x0

    .line 647
    .local v0, changed:Z
    monitor-enter p0

    .line 648
    :try_start_b
    invoke-virtual {p1}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_13
    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_89

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 649
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v7, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_55

    .line 650
    sget-object v7, Lcom/google/android/picasasync/PicasaContentProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_13

    .line 651
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v7, "unknown setting: "

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_52
    .catchall {:try_start_b .. :try_end_52} :catchall_52

    .line 663
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    :catchall_52
    move-exception v7

    monitor-exit p0

    throw v7

    .line 656
    .restart local v2       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_55
    :try_start_55
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v7, "com.google.android.picasasync."

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 657
    .local v5, systemKey:Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_80

    move-object v6, v8

    .line 658
    .local v6, value:Ljava/lang/String;
    :goto_71
    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_13

    .line 659
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 660
    const/4 v0, 0x1

    goto :goto_13

    .line 657
    .end local v6           #value:Ljava/lang/String;
    :cond_80
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_71

    .line 663
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5           #systemKey:Ljava/lang/String;
    :cond_89
    monitor-exit p0
    :try_end_8a
    .catchall {:try_start_55 .. :try_end_8a} :catchall_52

    .line 664
    if-eqz v0, :cond_a4

    .line 665
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 666
    .local v1, context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v7

    const-wide/16 v9, 0x0

    invoke-virtual {v7, v9, v10}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V

    .line 667
    invoke-static {v1}, Lcom/google/android/picasasync/PicasaFacade;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/picasasync/PicasaFacade;->getSettingsUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 669
    .end local v1           #context:Landroid/content/Context;
    :cond_a4
    return v0
.end method


# virtual methods
.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .registers 7
    .parameter "context"
    .parameter "info"

    .prologue
    .line 193
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 194
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    .line 195
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "photos"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "albums"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "posts"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "posts_album"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 199
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "users"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 200
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "photos/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 201
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "albums/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 202
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "users/#"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 203
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "settings"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "sync_request"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "sync_request/*"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 206
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "albumcovers/#"

    const/16 v3, 0xe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 207
    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 9
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 587
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_56

    .line 593
    :pswitch_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported uri:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 589
    :pswitch_20
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_33

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid URI: expect /sync_request/<task_ID>"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_33
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/ImmediateSync;->get(Landroid/content/Context;)Lcom/google/android/picasasync/ImmediateSync;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/picasasync/ImmediateSync;->cancelTask(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    move v0, v1

    .line 591
    :goto_48
    return v0

    :cond_49
    move v0, v2

    .line 589
    goto :goto_48

    .line 591
    :pswitch_4b
    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->resetSettings()Z

    move-result v0

    if-eqz v0, :cond_53

    move v0, v1

    goto :goto_48

    :cond_53
    move v0, v2

    goto :goto_48

    .line 587
    nop

    :pswitch_data_56
    .packed-switch 0x9
        :pswitch_4b
        :pswitch_b
        :pswitch_20
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_3a

    .line 232
    :pswitch_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :pswitch_1e
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.picasasync.item"

    .line 230
    :goto_20
    return-object v0

    .line 216
    :pswitch_21
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.picasasync.album"

    goto :goto_20

    .line 218
    :pswitch_24
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.picasasync.post"

    goto :goto_20

    .line 220
    :pswitch_27
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.picasasync.post_album"

    goto :goto_20

    .line 222
    :pswitch_2a
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.picasasync.user"

    goto :goto_20

    .line 224
    :pswitch_2d
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.item"

    goto :goto_20

    .line 226
    :pswitch_30
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.album"

    goto :goto_20

    .line 228
    :pswitch_33
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.user"

    goto :goto_20

    .line 230
    :pswitch_36
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.album_cover"

    goto :goto_20

    .line 212
    nop

    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_2d
        :pswitch_21
        :pswitch_30
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_2a
        :pswitch_33
        :pswitch_36
        :pswitch_24
        :pswitch_27
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 7
    .parameter "uri"
    .parameter "values"

    .prologue
    .line 538
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "INSERT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    .line 540
    .local v0, statsId:I
    :try_start_13
    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_d0

    .line 544
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unsupported uri:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_31
    .catchall {:try_start_13 .. :try_end_31} :catchall_31

    .line 547
    :catchall_31
    move-exception v1

    invoke-static {v0}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    throw v1

    .line 542
    :pswitch_36
    :try_start_36
    const-string v1, "task"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "manual_metadata"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_54

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/picasasync/PicasaSyncManager;->requestMetadataSync(Z)V
    :try_end_50
    .catchall {:try_start_36 .. :try_end_50} :catchall_31

    .line 547
    .end local p1
    :cond_50
    :goto_50
    invoke-static {v0}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    return-object p1

    .line 542
    .restart local p1
    :cond_54
    :try_start_54
    const-string v2, "metadata"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_69

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/picasasync/PicasaSyncManager;->requestMetadataSync(Z)V

    goto :goto_50

    :cond_69
    const-string v2, "immediate_albums"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9b

    const-string v1, "account"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    if-nez v1, :cond_92

    invoke-static {v2}, Lcom/google/android/picasasync/ImmediateSync;->get(Landroid/content/Context;)Lcom/google/android/picasasync/ImmediateSync;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/ImmediateSync;->requestSyncAlbumListForAllAccounts()Ljava/lang/String;

    move-result-object v1

    :goto_85
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    goto :goto_50

    :cond_92
    invoke-static {v2}, Lcom/google/android/picasasync/ImmediateSync;->get(Landroid/content/Context;)Lcom/google/android/picasasync/ImmediateSync;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/picasasync/ImmediateSync;->requestSyncAlbumListForAccount(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_85

    :cond_9b
    const-string v2, "immediate_photos"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_50

    const-string v1, "album_id"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b7

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "album ID missing"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b7
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasasync/ImmediateSync;->get(Landroid/content/Context;)Lcom/google/android/picasasync/ImmediateSync;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/picasasync/ImmediateSync;->requestSyncAlbum(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_ce
    .catchall {:try_start_54 .. :try_end_ce} :catchall_31

    move-result-object p1

    goto :goto_50

    .line 540
    :pswitch_data_d0
    .packed-switch 0xa
        :pswitch_36
    .end packed-switch
.end method

.method public onCreate()Z
    .registers 3

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 187
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    .line 188
    const/4 v1, 0x1

    return v1
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 14
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 440
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "OPEN "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v7

    .line 442
    .local v7, statsId:I
    :try_start_1b
    iget-object v8, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    .line 443
    .local v5, matchType:I
    sparse-switch v5, :sswitch_data_e2

    .line 482
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "unsupported uri: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_39
    .catchall {:try_start_1b .. :try_end_39} :catchall_39

    .line 485
    .end local v5           #matchType:I
    :catchall_39
    move-exception v8

    invoke-static {v7}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    throw v8

    .line 446
    .restart local v5       #matchType:I
    :sswitch_3e
    :try_start_3e
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;
    :try_end_41
    .catchall {:try_start_3e .. :try_end_41} :catchall_39

    move-result-object v1

    .line 447
    .local v1, context:Landroid/content/Context;
    const/4 v6, 0x0

    .line 450
    .local v6, redirectUri:Landroid/net/Uri;
    :try_start_43
    iget-object v8, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mPicasaStoreFacade:Lcom/google/android/picasastore/PicasaStoreFacade;

    if-nez v8, :cond_4d

    .line 451
    invoke-static {v1}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mPicasaStoreFacade:Lcom/google/android/picasastore/PicasaStoreFacade;

    .line 454
    :cond_4d
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mPicasaStoreFacade:Lcom/google/android/picasastore/PicasaStoreFacade;

    invoke-virtual {v9}, Lcom/google/android/picasastore/PicasaStoreFacade;->getAuthority()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 456
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v6, p2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_66
    .catchall {:try_start_43 .. :try_end_66} :catchall_39
    .catch Ljava/io/FileNotFoundException; {:try_start_43 .. :try_end_66} :catch_6b

    move-result-object v8

    .line 485
    invoke-static {v7}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    :goto_6a
    return-object v8

    .line 457
    :catch_6b
    move-exception v2

    .line 458
    .local v2, e:Ljava/io/FileNotFoundException;
    :try_start_6c
    const-string v8, "content_url"

    invoke-virtual {p1, v8}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 460
    .local v0, contentUrl:Ljava/lang/String;
    const-string v8, "w"

    invoke-virtual {p2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7c

    if-eqz v0, :cond_7d

    :cond_7c
    throw v2

    .line 462
    :cond_7d
    const-string v8, "PicasaContentProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "FileNotFoundException, look up photo metadata for "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    invoke-static {p1}, Lcom/google/android/picasasync/PicasaContentProvider;->getItemIdFromUri(Landroid/net/Uri;)J

    move-result-wide v3

    .line 466
    .local v3, id:J
    const-wide/16 v8, -0x1

    cmp-long v8, v3, v8

    if-nez v8, :cond_b1

    .line 467
    new-instance v8, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 470
    :cond_b1
    const/16 v8, 0xe

    if-ne v5, v8, :cond_bc

    invoke-direct {p0, v3, v4}, Lcom/google/android/picasasync/PicasaContentProvider;->lookupAlbumCoverUrl(J)Ljava/lang/String;

    move-result-object v0

    .line 474
    :goto_b9
    if-nez v0, :cond_c7

    throw v2

    .line 470
    :cond_bc
    const-string v8, "type"

    invoke-virtual {p1, v8}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v3, v4, v8}, Lcom/google/android/picasasync/PicasaContentProvider;->lookupContentUrl(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_b9

    .line 475
    :cond_c7
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "content_url"

    invoke-virtual {v8, v9, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 478
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v6, p2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_dc
    .catchall {:try_start_6c .. :try_end_dc} :catchall_39

    move-result-object v8

    .line 485
    invoke-static {v7}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    goto :goto_6a

    .line 443
    nop

    :sswitch_data_e2
    .sparse-switch
        0x2 -> :sswitch_3e
        0xe -> :sswitch_3e
    .end sparse-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 18
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    .line 239
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QUERY "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v11

    .line 241
    .local v11, statsId:I
    :try_start_1b
    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_1ce

    :pswitch_24
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_39
    .catchall {:try_start_1b .. :try_end_39} :catchall_39

    .line 245
    .end local p3
    .end local p4
    :catchall_39
    move-exception v1

    invoke-static {v11}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    throw v1

    .line 241
    .restart local p3
    .restart local p4
    :pswitch_3e
    :try_start_3e
    const-string v1, "limit"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->USER_TABLE_NAME:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 242
    .end local p3
    .end local p4
    .local v10, cursor:Landroid/database/Cursor;
    :goto_58
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->incrementQueryResultCount(I)V
    :try_end_5f
    .catchall {:try_start_3e .. :try_end_5f} :catchall_39

    .line 245
    invoke-static {v11}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    return-object v10

    .line 241
    .end local v10           #cursor:Landroid/database/Cursor;
    .restart local p3
    .restart local p4
    :pswitch_63
    if-nez p3, :cond_1c8

    :try_start_65
    const-string v1, "type"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "image"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9a

    sget-object p3, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TYPE_WHERE:Ljava/lang/String;

    .end local p3
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4
    const/4 v1, 0x0

    const-string v2, "image/%"

    aput-object v2, p4, v1

    move-object/from16 v5, p4

    move-object v4, p3

    :goto_82
    const-string v1, "limit"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TABLE_NAME:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto :goto_58

    .restart local p3
    .restart local p4
    :cond_9a
    const-string v2, "video"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c8

    sget-object p3, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TYPE_WHERE:Ljava/lang/String;

    .end local p3
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4
    const/4 v1, 0x0

    const-string v2, "video/%"

    aput-object v2, p4, v1

    move-object/from16 v5, p4

    move-object v4, p3

    goto :goto_82

    .restart local p3
    .restart local p4
    :pswitch_b2
    new-instance v3, Lcom/google/android/picasasync/PicasaContentProvider$WhereEntry;

    move-object/from16 v0, p5

    invoke-direct {v3, p1, v0}, Lcom/google/android/picasasync/PicasaContentProvider$WhereEntry;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/picasasync/PicasaContentProvider$WhereEntry;->selection:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/picasasync/PicasaContentProvider$WhereEntry;->selectionArgs:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, v3, Lcom/google/android/picasasync/PicasaContentProvider$WhereEntry;->sortOrder:Ljava/lang/String;

    iget-object v9, v3, Lcom/google/android/picasasync/PicasaContentProvider$WhereEntry;->sortLimit:Ljava/lang/String;

    move-object v3, p2

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto :goto_58

    :pswitch_d1
    const-string v4, "album_type = \'Buzz\'"

    const/4 v5, 0x0

    const-string v1, "user_id"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e4

    const-string v4, "album_type = \'Buzz\' AND user_id = ?"

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v5, v2

    :cond_e4
    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TABLE_NAME:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p2

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_58

    :pswitch_f7
    const-string v1, "limit"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_58

    :pswitch_113
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v2

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->USER_TABLE_NAME:Ljava/lang/String;

    const-string v4, "_id=?"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p2

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_58

    :pswitch_138
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v2

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->ALBUM_TABLE_NAME:Ljava/lang/String;

    const-string v4, "_id=?"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p2

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_58

    :pswitch_15d
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v2

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/picasasync/PicasaContentProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const-string v4, "_id=?"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p2

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_58

    :pswitch_182
    invoke-direct {p0, p2}, Lcom/google/android/picasasync/PicasaContentProvider;->querySettings$7be7850c([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_58

    :pswitch_188
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_19b

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid URI: expect /sync_request/<task_ID>"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_19b
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasasync/ImmediateSync;->get(Landroid/content/Context;)Lcom/google/android/picasasync/ImmediateSync;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/picasasync/ImmediateSync;->getResult(Ljava/lang/String;)I

    move-result v1

    new-instance v10, Lcom/google/android/picasastore/PicasaMatrixCursor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "immediate_sync_result"

    aput-object v4, v2, v3

    invoke-direct {v10, v2}, Lcom/google/android/picasastore/PicasaMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/google/android/picasastore/PicasaMatrixCursor;->newRow()Lcom/google/android/picasastore/PicasaMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/picasastore/PicasaMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/picasastore/PicasaMatrixCursor$RowBuilder;
    :try_end_1c6
    .catchall {:try_start_65 .. :try_end_1c6} :catchall_39

    goto/16 :goto_58

    :cond_1c8
    move-object/from16 v5, p4

    move-object v4, p3

    goto/16 :goto_82

    nop

    :pswitch_data_1ce
    .packed-switch 0x1
        :pswitch_f7
        :pswitch_15d
        :pswitch_63
        :pswitch_138
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_182
        :pswitch_24
        :pswitch_188
        :pswitch_3e
        :pswitch_113
        :pswitch_24
        :pswitch_b2
        :pswitch_d1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 13
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 624
    iget-object v5, p0, Lcom/google/android/picasasync/PicasaContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    sparse-switch v5, :sswitch_data_4c

    .line 639
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unsupported uri:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 627
    :sswitch_22
    const-string v5, "cache_flag"

    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 628
    .local v2, cachingFlag:Ljava/lang/Integer;
    if-nez v2, :cond_2b

    .line 636
    .end local v2           #cachingFlag:Ljava/lang/Integer;
    :cond_2a
    :goto_2a
    return v3

    .line 629
    .restart local v2       #cachingFlag:Ljava/lang/Integer;
    :cond_2b
    invoke-static {p1, v6, v7}, Lcom/google/android/picasasync/PicasaContentProvider;->getLastSegmentAsLong(Landroid/net/Uri;J)J

    move-result-wide v0

    .line 630
    .local v0, albumId:J
    cmp-long v3, v0, v6

    if-eqz v3, :cond_42

    .line 631
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/PrefetchHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PrefetchHelper;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v3, v0, v1, v5}, Lcom/google/android/picasasync/PrefetchHelper;->setAlbumCachingFlag(JI)V

    :cond_42
    move v3, v4

    .line 633
    goto :goto_2a

    .line 636
    .end local v0           #albumId:J
    .end local v2           #cachingFlag:Ljava/lang/Integer;
    :sswitch_44
    invoke-direct {p0, p2}, Lcom/google/android/picasasync/PicasaContentProvider;->updateSettings(Landroid/content/ContentValues;)Z

    move-result v5

    if-eqz v5, :cond_2a

    move v3, v4

    goto :goto_2a

    .line 624
    :sswitch_data_4c
    .sparse-switch
        0x4 -> :sswitch_22
        0x9 -> :sswitch_44
    .end sparse-switch
.end method
