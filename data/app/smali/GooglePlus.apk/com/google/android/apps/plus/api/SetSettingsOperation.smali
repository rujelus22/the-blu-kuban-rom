.class public final Lcom/google/android/apps/plus/api/SetSettingsOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "SetSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SetMobileSettingsRequest;",
        "Lcom/google/api/services/plusi/model/SetMobileSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final mWarmWelcomeTimestamp:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "warmWelcomeTimestamp"
    .parameter "intent"
    .parameter "listener"

    .prologue
    const/4 v6, 0x0

    .line 37
    const-string v3, "setmobilesettings"

    invoke-static {}, Lcom/google/api/services/plusi/model/SetMobileSettingsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SetMobileSettingsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SetMobileSettingsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SetMobileSettingsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 40
    iput-wide p3, p0, Lcom/google/android/apps/plus/api/SetSettingsOperation;->mWarmWelcomeTimestamp:J

    .line 41
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/SetMobileSettingsRequest;

    .end local p1
    new-instance v0, Lcom/google/api/services/plusi/model/MobilePreference;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MobilePreference;-><init>()V

    iget-wide v1, p0, Lcom/google/android/apps/plus/api/SetSettingsOperation;->mWarmWelcomeTimestamp:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/MobilePreference;->wwMainFlowAckTimestampMsec:Ljava/lang/Long;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SetMobileSettingsRequest;->preference:Lcom/google/api/services/plusi/model/MobilePreference;

    return-void
.end method
