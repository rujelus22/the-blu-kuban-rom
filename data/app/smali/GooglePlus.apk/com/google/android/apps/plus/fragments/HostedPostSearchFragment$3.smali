.class final Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;
.super Ljava/lang/Object;
.source "HostedPostSearchFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->createAndRunDbCleanup(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$mainThreadPostRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Runnable;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->val$mainThreadPostRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 305
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "search"

    invoke-virtual {v1, v2, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-static {v3}, Lcom/google/android/apps/plus/util/SearchUtils;->getSearchKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x25

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "activity_streams"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "stream_key LIKE "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->val$mainThreadPostRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_55

    .line 307
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 308
    .local v0, handler:Landroid/os/Handler;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;->val$mainThreadPostRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 310
    .end local v0           #handler:Landroid/os/Handler;
    :cond_55
    return-void
.end method
