.class public Lcom/google/android/apps/plus/widget/EsWidgetService;
.super Landroid/app/IntentService;
.source "EsWidgetService.java"


# static fields
.field private static final TEXT_ONLY_VIEW_IDS:[I

.field private static sAuthorBitmap:Landroid/graphics/Bitmap;

.field private static sAutoTextColor:I

.field private static sContentColor:I

.field private static sInitialized:Z

.field public static sWidgetImageFetchSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    return-void

    nop

    :array_a
    .array-data 0x4
        0x65t 0x2t 0x9t 0x7ft
        0x66t 0x2t 0x9t 0x7ft
        0x67t 0x2t 0x9t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 79
    const-string v0, "EsWidgetService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method private static getAutoText(Landroid/content/Context;J)Ljava/lang/String;
    .registers 8
    .parameter "context"
    .parameter "contentFlags"

    .prologue
    const-wide/16 v3, 0x0

    .line 393
    const-wide/16 v1, 0x1000

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_13

    .line 394
    const v0, 0x7f0803bf

    .line 414
    .local v0, autoText:I
    :goto_c
    if-eqz v0, :cond_64

    .line 415
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 417
    :goto_12
    return-object v1

    .line 395
    .end local v0           #autoText:I
    :cond_13
    const-wide/16 v1, 0x4000

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1e

    .line 396
    const v0, 0x7f0803c0

    .restart local v0       #autoText:I
    goto :goto_c

    .line 397
    .end local v0           #autoText:I
    :cond_1e
    const-wide/16 v1, 0x40

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_29

    .line 398
    const v0, 0x7f0803bc

    .restart local v0       #autoText:I
    goto :goto_c

    .line 399
    .end local v0           #autoText:I
    :cond_29
    const-wide/16 v1, 0x80

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_34

    .line 400
    const v0, 0x7f0803be

    .restart local v0       #autoText:I
    goto :goto_c

    .line 401
    .end local v0           #autoText:I
    :cond_34
    const-wide/32 v1, 0x8004

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_40

    .line 403
    const v0, 0x7f0803c2

    .restart local v0       #autoText:I
    goto :goto_c

    .line 404
    .end local v0           #autoText:I
    :cond_40
    const-wide/16 v1, 0x20

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_4b

    .line 405
    const v0, 0x7f0803bd

    .restart local v0       #autoText:I
    goto :goto_c

    .line 406
    .end local v0           #autoText:I
    :cond_4b
    const-wide/16 v1, 0x8

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_56

    .line 407
    const v0, 0x7f0803c1

    .restart local v0       #autoText:I
    goto :goto_c

    .line 408
    .end local v0           #autoText:I
    :cond_56
    const-wide/32 v1, 0x10000

    and-long/2addr v1, p1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_62

    .line 409
    const v0, 0x7f0803c3

    .restart local v0       #autoText:I
    goto :goto_c

    .line 411
    .end local v0           #autoText:I
    :cond_62
    const/4 v0, 0x0

    .restart local v0       #autoText:I
    goto :goto_c

    .line 417
    :cond_64
    const/4 v1, 0x0

    goto :goto_12
.end method

.method private static loadActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "appWidgetId"
    .parameter "circleId"

    .prologue
    const/4 v4, 0x1

    .line 517
    const-string v0, "EsWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 518
    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] loadActivities"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    :cond_24
    const/4 v2, 0x0

    .line 522
    .local v2, view:I
    const-string v0, "v.whatshot"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 523
    const/4 v2, 0x1

    .line 524
    const/4 p3, 0x0

    .line 529
    :cond_2f
    :goto_2f
    new-instance v9, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    .line 530
    .local v9, syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-virtual {v9, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->setFullSync(Z)V

    .line 531
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Get activities for widget circleId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " view: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    .line 532
    const-string v0, "Activities:SyncStream"

    invoke-virtual {v9, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 535
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x14

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    :try_start_61
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_64
    .catchall {:try_start_61 .. :try_end_64} :catchall_a4
    .catch Ljava/lang/Exception; {:try_start_61 .. :try_end_64} :catch_75

    .line 542
    invoke-virtual {v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 543
    invoke-virtual {v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    .line 544
    :goto_6a
    return-void

    .line 525
    .end local v9           #syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    :cond_6b
    const-string v0, "v.all.circles"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 526
    const/4 p3, 0x0

    goto :goto_2f

    .line 537
    .restart local v9       #syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    :catch_75
    move-exception v10

    .line 538
    .local v10, ignore:Ljava/lang/Exception;
    :try_start_76
    const-string v0, "EsWidget"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 539
    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] loadActivities failed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9d
    .catchall {:try_start_76 .. :try_end_9d} :catchall_a4

    .line 542
    :cond_9d
    invoke-virtual {v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 543
    invoke-virtual {v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_6a

    .line 542
    .end local v10           #ignore:Ljava/lang/Exception;
    :catchall_a4
    move-exception v0

    invoke-virtual {v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 543
    invoke-virtual {v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method private loadCursor(Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "account"
    .parameter "appWidgetId"
    .parameter "circleId"

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 477
    const-string v0, "EsWidget"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 478
    const-string v0, "EsWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] loadCursor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :cond_26
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_34

    const-string v0, "v.all.circles"

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 483
    :cond_34
    invoke-static {v4, v4, v4, v5, v7}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 495
    .local v1, streamUri:Landroid/net/Uri;
    :goto_3c
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 498
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_STREAM:[Ljava/lang/String;

    const-string v3, "content_flags&32933!=0 AND content_flags&16=0"

    const-string v5, "sort_index ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 509
    .local v6, cursor:Landroid/database/Cursor;
    return-object v6

    .line 486
    .end local v1           #streamUri:Landroid/net/Uri;
    .end local v6           #cursor:Landroid/database/Cursor;
    :cond_5f
    const-string v0, "v.whatshot"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 487
    invoke-static {v4, v4, v4, v5, v5}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1       #streamUri:Landroid/net/Uri;
    goto :goto_3c

    .line 491
    .end local v1           #streamUri:Landroid/net/Uri;
    :cond_70
    invoke-static {v4, p3, v4, v5, v7}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1       #streamUri:Landroid/net/Uri;
    goto :goto_3c
.end method

.method private static showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V
    .registers 5
    .parameter "remoteViews"
    .parameter "viewId"
    .parameter "text"
    .parameter "color"

    .prologue
    .line 326
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 327
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 328
    invoke-virtual {p0, p1, p3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 329
    return-void
.end method

.method private static showTextLayoutContent(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .registers 14
    .parameter "context"
    .parameter "remoteViews"
    .parameter "annotation"
    .parameter "attribution"
    .parameter "title"
    .parameter "contentFlags"

    .prologue
    .line 359
    const/4 v1, 0x0

    .line 360
    .local v1, index:I
    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    array-length v3, v5

    .line 361
    .local v3, length:I
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_18

    if-lez v3, :cond_18

    .line 362
    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    const/4 v6, 0x0

    add-int/lit8 v1, v1, 0x1

    aget v4, v5, v6

    .line 363
    .local v4, viewId:I
    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {p1, v4, p2, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    .line 366
    .end local v4           #viewId:I
    :cond_18
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2c

    if-ge v1, v3, :cond_2c

    .line 367
    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .local v2, index:I
    aget v4, v5, v1

    .line 368
    .restart local v4       #viewId:I
    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {p1, v4, p3, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    move v1, v2

    .line 371
    .end local v2           #index:I
    .end local v4           #viewId:I
    .restart local v1       #index:I
    :cond_2c
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_67

    if-ge v1, v3, :cond_67

    .line 372
    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v2, v1, 0x1

    .end local v1           #index:I
    .restart local v2       #index:I
    aget v4, v5, v1

    .line 373
    .restart local v4       #viewId:I
    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {p1, v4, p4, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    .line 376
    .end local v4           #viewId:I
    :goto_3f
    if-nez v2, :cond_57

    .line 377
    invoke-static {p0, p5, p6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getAutoText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, autoText:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_57

    .line 379
    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget v4, v5, v2

    .line 380
    .restart local v4       #viewId:I
    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAutoTextColor:I

    invoke-static {p1, v4, v0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    move v2, v1

    .line 384
    .end local v0           #autoText:Ljava/lang/String;
    .end local v1           #index:I
    .end local v4           #viewId:I
    .restart local v2       #index:I
    :cond_57
    :goto_57
    if-ge v2, v3, :cond_66

    .line 385
    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2           #index:I
    .restart local v1       #index:I
    aget v4, v5, v2

    .line 386
    .restart local v4       #viewId:I
    const/16 v5, 0x8

    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move v2, v1

    .line 387
    .end local v1           #index:I
    .restart local v2       #index:I
    goto :goto_57

    .line 388
    .end local v4           #viewId:I
    :cond_66
    return-void

    .end local v2           #index:I
    .restart local v1       #index:I
    :cond_67
    move v2, v1

    .end local v1           #index:I
    .restart local v2       #index:I
    goto :goto_3f
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .registers 29
    .parameter "intent"

    .prologue
    .line 105
    sget-boolean v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sInitialized:Z

    if-nez v3, :cond_3f

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 108
    .local v16, res:Landroid/content/res/Resources;
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAuthorBitmap:Landroid/graphics/Bitmap;

    .line 110
    const v3, 0x7f0a0010

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    .line 111
    const v3, 0x7f0a00cd

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAutoTextColor:I

    .line 113
    const v3, 0x7f0b000b

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v4

    iget v4, v4, Lcom/google/android/apps/plus/phone/ScreenMetrics;->shortDimension:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    .line 116
    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sInitialized:Z

    .line 119
    .end local v16           #res:Landroid/content/res/Resources;
    :cond_3f
    const-string v3, "appWidgetId"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 121
    .local v11, appWidgetId:I
    if-nez v11, :cond_4b

    .line 161
    :cond_4a
    :goto_4a
    return-void

    .line 125
    :cond_4b
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    .line 126
    .local v12, circleId:Ljava/lang/String;
    const-string v3, "refresh"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v15

    .line 127
    .local v15, isRefresh:Z
    const-string v3, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 129
    .local v13, currentActivityId:Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v10

    .line 130
    .local v10, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v10, :cond_6e

    .line 131
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showTapToConfigure(Landroid/content/Context;I)V

    goto :goto_4a

    .line 132
    :cond_6e
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7a

    .line 133
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showLoadingView(Landroid/content/Context;I)V

    goto :goto_4a

    .line 135
    :cond_7a
    if-eqz v15, :cond_81

    .line 136
    move-object/from16 v0, p0

    invoke-static {v0, v10, v11, v12}, Lcom/google/android/apps/plus/widget/EsWidgetService;->loadActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)V

    .line 138
    :cond_81
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v12}, Lcom/google/android/apps/plus/widget/EsWidgetService;->loadCursor(Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 140
    .local v14, cursor:Landroid/database/Cursor;
    if-eqz v14, :cond_9f

    :try_start_89
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_9f

    if-nez v15, :cond_9f

    .line 141
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 143
    move-object/from16 v0, p0

    invoke-static {v0, v10, v11, v12}, Lcom/google/android/apps/plus/widget/EsWidgetService;->loadActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)V

    .line 144
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v12}, Lcom/google/android/apps/plus/widget/EsWidgetService;->loadCursor(Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 147
    :cond_9f
    if-eqz v14, :cond_a7

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_b2

    .line 148
    :cond_a7
    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showNoPostsFound(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    :try_end_ac
    .catchall {:try_start_89 .. :try_end_ac} :catchall_18a

    .line 156
    :goto_ac
    if-eqz v14, :cond_4a

    .line 157
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_4a

    .line 150
    :cond_b2
    :try_start_b2
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_185

    const/4 v3, -0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_bc
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_185

    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_bc

    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_d6

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 151
    :cond_d6
    :goto_d6
    const-string v3, "EsWidget"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_101

    const-string v3, "EsWidget"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] createRemoteViews: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v14}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_101
    new-instance v17, Landroid/widget/RemoteViews;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0300de

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v3, 0x7f090249

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f0900a1

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f090185

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f090257

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f090256

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f090258

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_3a0

    const/16 v3, 0xe

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    if-eqz v4, :cond_468

    invoke-static {v4}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v22

    const/4 v4, 0x0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v18, v4

    :goto_16d
    move/from16 v0, v18

    move/from16 v1, v23

    if-ge v0, v1, :cond_1d0

    if-nez v3, :cond_1d0

    aget-object v4, v22, v18

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v5

    packed-switch v5, :pswitch_data_46e

    :cond_17e
    move-object v4, v9

    :cond_17f
    :goto_17f
    add-int/lit8 v5, v18, 0x1

    move/from16 v18, v5

    move-object v9, v4

    goto :goto_16d

    .line 150
    :cond_185
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_188
    .catchall {:try_start_b2 .. :try_end_188} :catchall_18a

    goto/16 :goto_d6

    .line 156
    :catchall_18a
    move-exception v3

    if-eqz v14, :cond_190

    .line 157
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_190
    throw v3

    .line 151
    :pswitch_191
    :try_start_191
    aget-object v4, v22, v18

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_17e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v22, v18

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&google_plus:widget"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v3, Lcom/google/android/apps/plus/content/MediaImageRequest;

    sget v6, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget v7, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    const/4 v8, 0x1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    const/4 v4, 0x1

    if-ne v5, v4, :cond_17e

    aget-object v4, v22, v18

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbMedia;->getTitlePlaintext()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_17f

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    goto :goto_17f

    :cond_1d0
    move-object/from16 v18, v9

    move-object v9, v3

    :goto_1d3
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3ab

    const/4 v3, 0x1

    move v7, v3

    :goto_1db
    if-nez v7, :cond_3af

    if-nez v9, :cond_3af

    const/4 v3, 0x1

    move v8, v3

    :goto_1e1
    if-eqz v8, :cond_3b3

    const v3, 0x7f090262

    move v6, v3

    :goto_1e7
    if-eqz v8, :cond_3b9

    const v3, 0x7f090263

    move v5, v3

    :goto_1ed
    if-eqz v8, :cond_3bf

    const v3, 0x7f090264

    move v4, v3

    :goto_1f3
    const v22, 0x7f090255

    if-eqz v8, :cond_3c5

    const/16 v3, 0x8

    :goto_1fa
    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v22, 0x7f090261

    if-eqz v8, :cond_3c8

    const/4 v3, 0x0

    :goto_207
    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget-object v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAuthorBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    const/4 v3, 0x2

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v10, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAvatarUrl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_235

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v10, v3, v1, v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_235

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :cond_235
    const/4 v3, 0x0

    if-eqz v9, :cond_2ae

    move-object/from16 v0, p0

    invoke-static {v0, v10, v9}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainImage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_26a

    if-nez v7, :cond_26a

    sget v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget v6, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget-object v22, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v22

    invoke-static {v3, v6, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getBackgroundPattern(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    sget v25, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget v26, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    invoke-virtual/range {v22 .. v26}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_26a
    if-eqz v3, :cond_2ae

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sget v22, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    move/from16 v0, v22

    if-gt v6, v0, :cond_280

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sget v22, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    move/from16 v0, v22

    if-le v6, v0, :cond_28a

    :cond_280
    sget v6, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget v22, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    move/from16 v0, v22

    invoke-static {v3, v6, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeAndCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v3

    :cond_28a
    const v6, 0x7f0900a1

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v6, 0x7f0900a1

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getMediaType()I

    move-result v6

    const/4 v9, 0x2

    if-ne v6, v9, :cond_2ae

    const v6, 0x7f090185

    const/4 v9, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_2ae
    if-eqz v3, :cond_3cc

    const/4 v3, 0x1

    :goto_2b1
    if-eqz v7, :cond_2d1

    const v6, 0x7f090258

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v6, 0x7f090258

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz v3, :cond_3cf

    const v3, 0x7f090257

    const/4 v6, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_2d1
    :goto_2d1
    const/4 v3, 0x3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const-string v3, "com.google.android.apps.plus.widget.EsWidgetUtils"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "circleName_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2ff

    const/4 v3, 0x0

    :cond_2ff
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_30e

    const v3, 0x7f08021e

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_30e
    const v5, 0x7f08040d

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/16 v3, 0xf

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v3, 0x10

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v6, 0x0

    const/16 v3, 0x11

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x12

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_356

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_356

    const v3, 0x7f0800e9

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v6, v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    :cond_356
    if-eqz v8, :cond_3da

    move-object/from16 v3, p0

    move-object/from16 v4, v17

    move-wide/from16 v8, v20

    invoke-static/range {v3 .. v9}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showTextLayoutContent(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    :cond_361
    :goto_361
    const/4 v8, 0x1

    move-object/from16 v3, p0

    move-object v4, v10

    move v5, v11

    move-object/from16 v6, v17

    move-object/from16 v7, v19

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    if-eqz v10, :cond_3a0

    if-eqz v19, :cond_3a0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v10, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.google.android.apps.plus.widget.ACTIVITY_ACTION"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x1400

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/4 v4, 0x0

    const/high16 v5, 0x800

    move-object/from16 v0, p0

    invoke-static {v0, v4, v3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const v4, 0x7f090260

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 153
    .local v17, rv:Landroid/widget/RemoteViews;
    :cond_3a0
    invoke-static/range {p0 .. p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v11, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto/16 :goto_ac

    .line 151
    .end local v17           #rv:Landroid/widget/RemoteViews;
    :cond_3ab
    const/4 v3, 0x0

    move v7, v3

    goto/16 :goto_1db

    :cond_3af
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_1e1

    :cond_3b3
    const v3, 0x7f090259

    move v6, v3

    goto/16 :goto_1e7

    :cond_3b9
    const v3, 0x7f09025a

    move v5, v3

    goto/16 :goto_1ed

    :cond_3bf
    const v3, 0x7f09025b

    move v4, v3

    goto/16 :goto_1f3

    :cond_3c5
    const/4 v3, 0x0

    goto/16 :goto_1fa

    :cond_3c8
    const/16 v3, 0x8

    goto/16 :goto_207

    :cond_3cc
    const/4 v3, 0x0

    goto/16 :goto_2b1

    :cond_3cf
    const v3, 0x7f090256

    const/4 v6, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2d1

    :cond_3da
    const v3, 0x7f09025d

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f09025e

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v3, 0x7f09025c

    const/16 v4, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_41a

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_41a

    const v3, 0x7f09025d

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    move-object/from16 v0, v17

    invoke-static {v0, v3, v5, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    const v3, 0x7f09025e

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    move-object/from16 v0, v17

    invoke-static {v0, v3, v6, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    goto/16 :goto_361

    :cond_41a
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_43c

    const v3, 0x7f09025d

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    move-object/from16 v0, v17

    invoke-static {v0, v3, v6, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_361

    const v3, 0x7f09025e

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    move-object/from16 v0, v17

    invoke-static {v0, v3, v7, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    goto/16 :goto_361

    :cond_43c
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_44e

    const v3, 0x7f09025c

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    move-object/from16 v0, v17

    invoke-static {v0, v3, v7, v4}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    goto/16 :goto_361

    :cond_44e
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getAutoText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_361

    const v4, 0x7f09025c

    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAutoTextColor:I

    move-object/from16 v0, v17

    invoke-static {v0, v4, v3, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V
    :try_end_466
    .catchall {:try_start_191 .. :try_end_466} :catchall_18a

    goto/16 :goto_361

    :cond_468
    move-object/from16 v18, v9

    move-object v9, v3

    goto/16 :goto_1d3

    nop

    :pswitch_data_46e
    .packed-switch 0x1
        :pswitch_191
        :pswitch_191
        :pswitch_191
    .end packed-switch
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 9
    .parameter "intent"
    .parameter "startId"

    .prologue
    const/4 v5, 0x0

    .line 84
    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    .line 86
    const-string v4, "appWidgetId"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 89
    .local v1, appWidgetId:I
    if-eqz v1, :cond_19

    .line 90
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, circleId:Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 92
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_1a

    .line 93
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showTapToConfigure(Landroid/content/Context;I)V

    .line 101
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v2           #circleId:Ljava/lang/String;
    :cond_19
    :goto_19
    return-void

    .line 94
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    .restart local v2       #circleId:Ljava/lang/String;
    :cond_1a
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 95
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showLoadingView(Landroid/content/Context;I)V

    goto :goto_19

    .line 97
    :cond_24
    const-string v4, "refresh"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 98
    .local v3, isRefresh:Z
    invoke-static {p0, v1, v3}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showProgressIndicator(Landroid/content/Context;IZ)V

    goto :goto_19
.end method
