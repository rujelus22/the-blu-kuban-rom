.class public Lcom/google/android/apps/plus/phone/EventActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "EventActivity.java"


# instance fields
.field private mCurrentSpinnerIndex:I

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    return-void
.end method


# virtual methods
.method protected final createHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;
    .registers 2

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;-><init>()V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 4
    .parameter "actionBar"

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    .line 121
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 43
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0300bf

    invoke-direct {v1, p0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 45
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v2, 0x7f080352

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 49
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v2, 0x7f080353

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 51
    if-nez p1, :cond_3f

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notif_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, notificationId:Ljava/lang/String;
    if-eqz v0, :cond_3f

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    .line 63
    .end local v0           #notificationId:Ljava/lang/String;
    :cond_3f
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .registers 7
    .parameter "position"

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onPrimarySpinnerSelectionChange(I)V

    .line 82
    iget v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    if-ne v3, p1, :cond_8

    .line 114
    :cond_7
    :goto_7
    return-void

    .line 87
    :cond_8
    packed-switch p1, :pswitch_data_42

    .line 105
    const/4 v2, 0x0

    .line 110
    .local v2, fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    :cond_c
    :goto_c
    if-eqz v2, :cond_7

    .line 111
    iput p1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    .line 112
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventActivity;->replaceFragment(Lcom/google/android/apps/plus/phone/HostedFragment;)V

    goto :goto_7

    .line 89
    .end local v2           #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    :pswitch_14
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;-><init>()V

    .line 90
    .restart local v2       #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    goto :goto_c

    .line 94
    .end local v2           #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    :pswitch_1a
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;-><init>()V

    .line 95
    .restart local v2       #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "event_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, eventId:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    .local v0, args:Landroid/os/Bundle;
    const-string v3, "event_id"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_c

    .line 87
    nop

    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_14
        :pswitch_1a
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 75
    const-string v0, "spinnerIndex"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    .line 76
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 69
    const-string v0, "spinnerIndex"

    iget v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    return-void
.end method
