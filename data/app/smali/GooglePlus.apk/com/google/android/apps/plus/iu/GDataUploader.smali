.class final Lcom/google/android/apps/plus/iu/GDataUploader;
.super Ljava/lang/Object;
.source "GDataUploader.java"

# interfaces
.implements Lcom/google/android/apps/plus/iu/Uploader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    }
.end annotation


# static fields
.field private static final RE_RANGE_HEADER:Ljava/util/regex/Pattern;

.field private static sUserAgent:Ljava/lang/String;


# instance fields
.field private mAuthToken:Ljava/lang/String;

.field private mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

.field private mContext:Landroid/content/Context;

.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

.field private mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 71
    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/GDataUploader;->RE_RANGE_HEADER:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    .line 86
    invoke-static {p1}, Lcom/google/android/apps/plus/iu/GDataUploader;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/HttpUtils;->createHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 87
    return-void
.end method

.method private executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .registers 11
    .parameter "request"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    .line 402
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 403
    .local v2, startTime:J
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 404
    .local v1, response:Lorg/apache/http/HttpResponse;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementNetworkOpDuration(J)V

    .line 405
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    .line 408
    .local v4, statusCode:I
    const/16 v5, 0x191

    if-eq v4, v5, :cond_23

    const/16 v5, 0x193

    if-ne v4, v5, :cond_b2

    .line 411
    :cond_23
    :try_start_23
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthToken:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/iu/Authorizer;->getFreshAuthToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthToken:Ljava/lang/String;

    .line 412
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthToken:Ljava/lang/String;

    if-nez v5, :cond_7b

    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    const-string v6, "null auth token"

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_3f
    .catch Landroid/accounts/OperationCanceledException; {:try_start_23 .. :try_end_3f} :catch_3f
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_3f} :catch_55
    .catch Landroid/accounts/AuthenticatorException; {:try_start_23 .. :try_end_3f} :catch_66

    .line 413
    :catch_3f
    move-exception v0

    .line 415
    .local v0, e:Landroid/accounts/OperationCanceledException;
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4f

    .line 416
    const-string v5, "iu.UploadsManager"

    const-string v6, "authentication canceled"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 418
    :cond_4f
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 419
    .end local v0           #e:Landroid/accounts/OperationCanceledException;
    :catch_55
    move-exception v0

    .line 422
    .local v0, e:Ljava/io/IOException;
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_65

    .line 423
    const-string v5, "iu.UploadsManager"

    const-string v6, "authentication failed"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 425
    :cond_65
    throw v0

    .line 426
    .end local v0           #e:Ljava/io/IOException;
    :catch_66
    move-exception v0

    .line 427
    .local v0, e:Landroid/accounts/AuthenticatorException;
    const-string v5, "iu.UploadsManager"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_75

    .line 428
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 430
    :cond_75
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 433
    .end local v0           #e:Landroid/accounts/AuthenticatorException;
    :cond_7b
    const-string v5, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "GoogleLogin auth="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthToken:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v5, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_a0

    .line 435
    const-string v5, "iu.UploadsManager"

    const-string v6, "executeWithAuthRetry: attempt #2"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_a0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 438
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 439
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementNetworkOpDuration(J)V

    .line 441
    :cond_b2
    return-object v1
.end method

.method private static getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;
    .registers 6
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 557
    new-instance v0, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 558
    .local v0, entity:Lorg/apache/http/entity/BufferedHttpEntity;
    invoke-virtual {v0}, Lorg/apache/http/entity/BufferedHttpEntity;->getContentLength()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_17

    .line 559
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    .line 560
    const/4 v0, 0x0

    .line 562
    :cond_17
    return-object v0
.end method

.method private static getUserAgent(Landroid/content/Context;)Ljava/lang/String;
    .registers 7
    .parameter "context"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 707
    sget-object v1, Lcom/google/android/apps/plus/iu/GDataUploader;->sUserAgent:Ljava/lang/String;

    if-nez v1, :cond_53

    .line 710
    :try_start_6
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_12
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_12} :catch_56

    move-result-object v0

    .line 715
    .local v0, pi:Landroid/content/pm/PackageInfo;
    const-string v1, "%s/%s; %s/%s/%s/%s; %s/%s/%s/%d"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v3, v2, v5

    const/4 v3, 0x2

    sget-object v4, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    sget-object v4, Landroid/os/Build;->ID:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/iu/GDataUploader;->sUserAgent:Ljava/lang/String;

    .line 727
    .end local v0           #pi:Landroid/content/pm/PackageInfo;
    :cond_53
    sget-object v1, Lcom/google/android/apps/plus/iu/GDataUploader;->sUserAgent:Ljava/lang/String;

    return-object v1

    .line 713
    :catch_56
    move-exception v1

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "getPackageInfo failed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static isIncompeteStatusCode(I)Z
    .registers 2
    .parameter "code"

    .prologue
    .line 528
    const/16 v0, 0x134

    if-ne p0, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private static isSuccessStatusCode(I)Z
    .registers 2
    .parameter "code"

    .prologue
    .line 524
    const/16 v0, 0xc8

    if-eq p0, v0, :cond_8

    const/16 v0, 0xc9

    if-ne p0, v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private newUploadedEntry(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    .registers 12
    .parameter "response"

    .prologue
    .line 387
    new-instance v0, Lcom/google/android/apps/plus/iu/UploadedEntry;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-wide v2, p1, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoId:J

    iget-wide v4, p1, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoSize:J

    iget-wide v6, p1, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->timestamp:J

    iget-object v8, p1, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    iget-object v9, p1, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->fingerprint:Lcom/android/gallery3d/common/Fingerprint;

    invoke-virtual {v9}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/iu/UploadedEntry;-><init>(Lcom/google/android/apps/plus/iu/UploadTaskEntry;JJJLjava/lang/String;[B)V

    return-object v0
.end method

.method private static parseHeaders(Ljava/lang/String;)Ljava/util/HashMap;
    .registers 10
    .parameter "headersString"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 582
    .local v1, headers:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "\r\n"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 583
    .local v5, lines:[Ljava/lang/String;
    move-object v0, v5

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v5

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_e
    if-ge v2, v3, :cond_28

    aget-object v4, v0, v2

    .line 584
    .local v4, line:Ljava/lang/String;
    const-string v7, ":"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 585
    .local v6, pair:[Ljava/lang/String;
    array-length v7, v6

    const/4 v8, 0x2

    if-ne v7, v8, :cond_25

    .line 586
    const/4 v7, 0x0

    aget-object v7, v6, v7

    const/4 v8, 0x1

    aget-object v8, v6, v8

    invoke-virtual {v1, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    :cond_25
    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    .line 589
    .end local v4           #line:Ljava/lang/String;
    .end local v6           #pair:[Ljava/lang/String;
    :cond_28
    return-object v1
.end method

.method private static parseRangeHeaderEndByte(Ljava/lang/String;)J
    .registers 6
    .parameter "header"

    .prologue
    .line 549
    if-eqz p0, :cond_1b

    .line 550
    sget-object v1, Lcom/google/android/apps/plus/iu/GDataUploader;->RE_RANGE_HEADER:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 551
    .local v0, m:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    .line 553
    .end local v0           #m:Ljava/util/regex/Matcher;
    :goto_1a
    return-wide v1

    :cond_1b
    const-wide/16 v1, -0x1

    goto :goto_1a
.end method

.method private static parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    .registers 5
    .parameter "entity"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;
        }
    .end annotation

    .prologue
    .line 446
    if-nez p0, :cond_a

    new-instance v2, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v3, "null HttpEntity in response"

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 447
    :cond_a
    new-instance v0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;-><init>(B)V

    .line 448
    .local v0, contentHandler:Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 450
    .local v1, is:Ljava/io/InputStream;
    :try_start_14
    sget-object v2, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-static {v1, v2, v0}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_19
    .catchall {:try_start_14 .. :try_end_19} :catchall_20

    .line 452
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 454
    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->validateResult()V

    .line 455
    return-object v0

    .line 452
    :catchall_20
    move-exception v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v2
.end method

.method private resetUpload()V
    .registers 4

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    .line 398
    return-void
.end method

.method private resume(Ljava/io/InputStream;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    .registers 13
    .parameter "is"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v8

    new-instance v6, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v6, v8}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v8, "Content-Range"

    const-string v9, "bytes */*"

    invoke-virtual {v6, v8, v9}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    .local v6, resumeRequest:Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/iu/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    .line 216
    .local v7, resumeResponse:Lorg/apache/http/HttpResponse;
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 217
    .local v0, code:I
    invoke-static {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 218
    .local v1, entity:Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_34

    .line 219
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_34

    .line 220
    const-string v8, "iu.UploadsManager"

    const-string v9, "  Entity: content length was 0."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_34
    :try_start_34
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->isIncompeteStatusCode(I)Z

    move-result v8

    if-eqz v8, :cond_88

    const-string v8, "range"

    invoke-interface {v7, v8}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_88

    .line 228
    const-string v8, "range"

    invoke-interface {v7, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    .line 229
    .local v4, rangeHeader:Lorg/apache/http/Header;
    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseRangeHeaderEndByte(Ljava/lang/String;)J

    move-result-wide v2

    .line 230
    .local v2, offset:J
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-gez v8, :cond_73

    .line 231
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/GDataUploader;->resetUpload()V

    .line 232
    new-instance v8, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "negative range offset: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_6e
    .catchall {:try_start_34 .. :try_end_6e} :catchall_6e

    .line 255
    .end local v2           #offset:J
    .end local v4           #rangeHeader:Lorg/apache/http/Header;
    :catchall_6e
    move-exception v8

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    throw v8

    .line 234
    .restart local v2       #offset:J
    .restart local v4       #rangeHeader:Lorg/apache/http/Header;
    :cond_73
    :try_start_73
    invoke-virtual {p1, v2, v3}, Ljava/io/InputStream;->skip(J)J

    .line 235
    const/high16 v8, 0x4

    invoke-virtual {p1, v8}, Ljava/io/InputStream;->mark(I)V

    .line 236
    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v8, v2, v3}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    .line 237
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/GDataUploader;->uploadChunks(Ljava/io/InputStream;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    :try_end_83
    .catchall {:try_start_73 .. :try_end_83} :catchall_6e

    move-result-object v8

    .line 255
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    .end local v2           #offset:J
    .end local v4           #rangeHeader:Lorg/apache/http/Header;
    :goto_87
    return-object v8

    .line 238
    :cond_88
    :try_start_88
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v8

    if-eqz v8, :cond_b8

    .line 239
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    move-result-object v5

    .line 240
    .local v5, response:Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    invoke-static {v5}, Lcom/google/android/apps/plus/iu/GDataUploader;->throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V

    .line 241
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_a5

    .line 242
    const-string v8, "iu.UploadsManager"

    const-string v9, "nothing to resume, upload already completed"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_a5
    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v9, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    .line 245
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/iu/GDataUploader;->newUploadedEntry(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    :try_end_b3
    .catchall {:try_start_88 .. :try_end_b3} :catchall_6e

    move-result-object v8

    .line 255
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    goto :goto_87

    .line 246
    .end local v5           #response:Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    :cond_b8
    const/16 v8, 0x191

    if-ne v0, v8, :cond_ca

    .line 248
    :try_start_bc
    new-instance v8, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 250
    :cond_ca
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/GDataUploader;->resetUpload()V

    .line 251
    new-instance v8, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "unexpected resume response: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_e6
    .catchall {:try_start_bc .. :try_end_e6} :catchall_6e
.end method

.method private static safeConsumeContent(Lorg/apache/http/HttpEntity;)V
    .registers 2
    .parameter "entity"

    .prologue
    .line 572
    if-eqz p0, :cond_5

    .line 574
    :try_start_2
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    .line 578
    :cond_5
    :goto_5
    return-void

    :catch_6
    move-exception v0

    goto :goto_5
.end method

.method private start(Ljava/io/InputStream;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    .registers 15
    .parameter "is"
    .parameter "uri"
    .parameter "requestTemplate"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 172
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const-string v5, "\r\n\r\n"

    invoke-virtual {p3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_3f

    const/4 v6, 0x0

    invoke-virtual {p3, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    move-object p3, v6

    .end local p3
    :goto_1d
    invoke-static {p3}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseHeaders(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_29
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_41

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v9, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_29

    .restart local p3
    :cond_3f
    move-object v7, v8

    goto :goto_1d

    .end local p3
    :cond_41
    if-eqz v7, :cond_4e

    new-instance v5, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v5, v7}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 173
    .local v2, initialRequest:Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_4e
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 174
    .local v3, initialResponse:Lorg/apache/http/HttpResponse;
    invoke-static {v3}, Lcom/google/android/apps/plus/iu/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 176
    .local v1, entity:Lorg/apache/http/HttpEntity;
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 179
    .local v0, code:I
    :try_start_5e
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v5

    if-eqz v5, :cond_8b

    .line 180
    if-eqz v1, :cond_6d

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/GDataUploader;->throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V

    .line 181
    :cond_6d
    const-string v5, "Location"

    invoke-interface {v3, v5}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    .line 182
    .local v4, locationHeader:Lorg/apache/http/Header;
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    .line 183
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const-wide/16 v6, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/GDataUploader;->uploadChunks(Ljava/io/InputStream;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    :try_end_86
    .catchall {:try_start_5e .. :try_end_86} :catchall_a8

    move-result-object v5

    .line 206
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    return-object v5

    .line 185
    .end local v4           #locationHeader:Lorg/apache/http/Header;
    :cond_8b
    const/16 v5, 0x190

    if-ne v0, v5, :cond_ad

    .line 188
    :try_start_8f
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "upload failed (bad payload, file too large) "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_a8
    .catchall {:try_start_8f .. :try_end_a8} :catchall_a8

    .line 206
    :catchall_a8
    move-exception v5

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    throw v5

    .line 191
    :cond_ad
    const/16 v5, 0x191

    if-ne v0, v5, :cond_bf

    .line 193
    :try_start_b1
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 194
    :cond_bf
    const/16 v5, 0x1f4

    if-lt v0, v5, :cond_e0

    const/16 v5, 0x258

    if-ge v0, v5, :cond_e0

    .line 197
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "upload transient error:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 202
    :cond_e0
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_ee
    .catchall {:try_start_b1 .. :try_end_ee} :catchall_a8
.end method

.method private static throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V
    .registers 3
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;
        }
    .end annotation

    .prologue
    .line 566
    if-eqz p0, :cond_14

    const-string v0, "LimitQuota"

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 567
    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_14
    return-void
.end method

.method private uploadChunks(Ljava/io/InputStream;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    .registers 33
    .parameter "is"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;
        }
    .end annotation

    .prologue
    .line 263
    const/high16 v23, 0x4

    move/from16 v0, v23

    new-array v6, v0, [B

    .line 265
    .local v6, buffer:[B
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v25

    cmp-long v23, v23, v25

    if-gez v23, :cond_348

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v23, v0

    if-eqz v23, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-interface/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;->onProgress(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    .line 267
    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->isUploading()Z

    move-result v23

    if-nez v23, :cond_44

    const/16 v23, 0x0

    .line 380
    :goto_43
    return-object v23

    .line 270
    :cond_44
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v15

    .line 271
    .local v15, offset:J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v23

    sub-long v23, v23, v15

    move-wide/from16 v0, v23

    long-to-int v14, v0

    .line 272
    .local v14, length:I
    const/high16 v23, 0x4

    move/from16 v0, v23

    if-gt v14, v0, :cond_90

    const/4 v13, 0x1

    .line 273
    .local v13, lastChunk:Z
    :goto_64
    if-nez v13, :cond_68

    const/high16 v14, 0x4

    .line 276
    :cond_68
    const/high16 v23, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->mark(I)V

    .line 281
    const/16 v18, 0x0

    :goto_73
    move/from16 v0, v18

    if-ge v0, v14, :cond_92

    add-int/lit8 v23, v18, 0x0

    sub-int v24, v14, v18

    :try_start_7b
    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/InputStream;->read([BII)I
    :try_end_84
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_84} :catch_154

    move-result v23

    const/16 v24, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_92

    add-int v18, v18, v23

    goto :goto_73

    .line 272
    .end local v13           #lastChunk:Z
    :cond_90
    const/4 v13, 0x0

    goto :goto_64

    .line 286
    .restart local v13       #lastChunk:Z
    .local v18, read:I
    :cond_92
    if-nez v13, :cond_9c

    const/high16 v23, 0x4

    move/from16 v0, v18

    move/from16 v1, v23

    if-eq v0, v1, :cond_17f

    .line 288
    :cond_9c
    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v7, v0, [J

    .line 289
    .local v7, byteCount:[J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v0, v7}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v11

    .line 292
    .local v11, fingerprint:Lcom/android/gallery3d/common/Fingerprint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/android/gallery3d/common/Fingerprint;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_17f

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 297
    .local v9, contentUri:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setFingerprint(Lcom/android/gallery3d/common/Fingerprint;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v22

    .line 299
    .local v22, uploadUrl:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    const-wide/16 v24, 0x0

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-wide v24, v7, v24

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesTotal(J)V

    .line 304
    :try_start_118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v23, v0

    if-eqz v23, :cond_12f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-interface/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;->onFileChanged(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    :try_end_12f
    .catch Ljava/lang/Throwable; {:try_start_118 .. :try_end_12f} :catch_15d

    .line 311
    :cond_12f
    :goto_12f
    new-instance v23, Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "UPLOAD_SIZE_DATA_MISMATCH: fingerprint changed; uri="

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ",uploadUrl="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 282
    .end local v7           #byteCount:[J
    .end local v9           #contentUri:Ljava/lang/String;
    .end local v11           #fingerprint:Lcom/android/gallery3d/common/Fingerprint;
    .end local v18           #read:I
    .end local v22           #uploadUrl:Ljava/lang/String;
    :catch_154
    move-exception v10

    .line 283
    .local v10, e:Ljava/io/IOException;
    new-instance v23, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;

    move-object/from16 v0, v23

    invoke-direct {v0, v10}, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;-><init>(Ljava/lang/Throwable;)V

    throw v23

    .line 305
    .end local v10           #e:Ljava/io/IOException;
    .restart local v7       #byteCount:[J
    .restart local v9       #contentUri:Ljava/lang/String;
    .restart local v11       #fingerprint:Lcom/android/gallery3d/common/Fingerprint;
    .restart local v18       #read:I
    .restart local v22       #uploadUrl:Ljava/lang/String;
    :catch_15d
    move-exception v12

    .line 306
    .local v12, ignored:Ljava/lang/Throwable;
    const-string v23, "iu.UploadsManager"

    const/16 v24, 0x3

    invoke-static/range {v23 .. v24}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v23

    if-eqz v23, :cond_12f

    .line 307
    const-string v23, "iu.UploadsManager"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "error calling back on file change:"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12f

    .line 317
    .end local v7           #byteCount:[J
    .end local v9           #contentUri:Ljava/lang/String;
    .end local v11           #fingerprint:Lcom/android/gallery3d/common/Fingerprint;
    .end local v12           #ignored:Ljava/lang/Throwable;
    .end local v22           #uploadUrl:Ljava/lang/String;
    :cond_17f
    const-string v23, "iu.UploadsManager"

    const/16 v24, 0x4

    invoke-static/range {v23 .. v24}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v23

    if-eqz v23, :cond_1a3

    .line 318
    const-string v23, "iu.UploadsManager"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "--- UPLOAD task: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_1a3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMimeType()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v25

    new-instance v20, Lorg/apache/http/client/methods/HttpPut;

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    int-to-long v0, v14

    move-wide/from16 v27, v0

    add-long v27, v27, v15

    const-wide/16 v29, 0x1

    sub-long v27, v27, v29

    const-string v23, "Content-Range"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "bytes "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "-"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-wide/from16 v1, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "/"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v23, "Content-Type"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v23, Ljava/io/ByteArrayInputStream;

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v0, v6, v1, v14}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    new-instance v24, Lorg/apache/http/entity/InputStreamEntity;

    int-to-long v0, v14

    move-wide/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    move-wide/from16 v2, v25

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    const/16 v23, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 329
    .local v20, uploadRequest:Lorg/apache/http/client/methods/HttpUriRequest;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v21

    .line 333
    .local v21, uploadResponse:Lorg/apache/http/HttpResponse;
    :try_start_248
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    .line 334
    .local v8, code:I
    invoke-static {v8}, Lcom/google/android/apps/plus/iu/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v23

    if-eqz v23, :cond_29b

    .line 335
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/iu/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    move-result-object v19

    .line 336
    .local v19, response:Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/iu/GDataUploader;->throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V

    .line 339
    const-string v23, "iu.UploadsManager"

    const/16 v24, 0x3

    invoke-static/range {v23 .. v24}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v23

    if-eqz v23, :cond_272

    .line 340
    const-string v23, "iu.UploadsManager"

    const-string v24, "UPLOAD_SUCCESS"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :cond_272
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v24

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    .line 343
    const-wide/16 v23, 0x1

    invoke-static/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementNetworkOpCount(J)V

    .line 344
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->newUploadedEntry(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    :try_end_291
    .catchall {:try_start_248 .. :try_end_291} :catchall_2c3

    move-result-object v23

    .line 380
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_43

    .line 345
    .end local v19           #response:Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    :cond_29b
    :try_start_29b
    invoke-static {v8}, Lcom/google/android/apps/plus/iu/GDataUploader;->isIncompeteStatusCode(I)Z

    move-result v23

    if-eqz v23, :cond_2f6

    .line 347
    const-string v23, "range"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v17

    .line 348
    .local v17, rangeHeader:Lorg/apache/http/Header;
    if-eqz v17, :cond_2cc

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseRangeHeaderEndByte(Ljava/lang/String;)J

    move-result-wide v4

    .line 351
    .local v4, actualOffset:J
    :goto_2b5
    const-wide/16 v23, 0x0

    cmp-long v23, v4, v23

    if-gez v23, :cond_2cf

    .line 352
    new-instance v23, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v24, "malformed or missing range header for subsequent upload"

    invoke-direct/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v23
    :try_end_2c3
    .catchall {:try_start_29b .. :try_end_2c3} :catchall_2c3

    .line 380
    .end local v4           #actualOffset:J
    .end local v8           #code:I
    .end local v17           #rangeHeader:Lorg/apache/http/Header;
    :catchall_2c3
    move-exception v23

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    throw v23

    .line 348
    .restart local v8       #code:I
    .restart local v17       #rangeHeader:Lorg/apache/http/Header;
    :cond_2cc
    const-wide/16 v4, -0x1

    goto :goto_2b5

    .line 356
    .restart local v4       #actualOffset:J
    :cond_2cf
    int-to-long v0, v14

    move-wide/from16 v23, v0

    add-long v23, v23, v15

    cmp-long v23, v4, v23

    if-gez v23, :cond_2e0

    .line 358
    :try_start_2d8
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->reset()V

    .line 359
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Ljava/io/InputStream;->skip(J)J

    .line 361
    :cond_2e0
    move-wide v15, v4

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V
    :try_end_2ed
    .catchall {:try_start_2d8 .. :try_end_2ed} :catchall_2c3

    .line 380
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/iu/GDataUploader;->safeConsumeContent(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_6

    .line 364
    .end local v4           #actualOffset:J
    .end local v17           #rangeHeader:Lorg/apache/http/Header;
    :cond_2f6
    const/16 v23, 0x190

    move/from16 v0, v23

    if-ne v8, v0, :cond_315

    .line 367
    :try_start_2fc
    new-instance v23, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "upload failed (bad payload, file too large) "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 370
    :cond_315
    const/16 v23, 0x1f4

    move/from16 v0, v23

    if-lt v8, v0, :cond_33a

    const/16 v23, 0x258

    move/from16 v0, v23

    if-ge v8, v0, :cond_33a

    .line 373
    new-instance v23, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "upload transient error"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 376
    :cond_33a
    new-instance v23, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v23
    :try_end_348
    .catchall {:try_start_2fc .. :try_end_348} :catchall_2c3

    .line 382
    .end local v8           #code:I
    .end local v13           #lastChunk:Z
    .end local v14           #length:I
    .end local v15           #offset:J
    .end local v18           #read:I
    .end local v20           #uploadRequest:Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v21           #uploadResponse:Lorg/apache/http/HttpResponse;
    :cond_348
    new-instance v23, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v24, "upload is done but no server confirmation"

    invoke-direct/range {v23 .. v24}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v23
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 164
    return-void
.end method

.method public final upload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    .registers 13
    .parameter "task"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileUnavailableException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    .line 95
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-gtz v5, :cond_18

    .line 96
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$MediaFileUnavailableException;

    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Zero length file can\'t be uploaded"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$MediaFileUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 100
    :cond_18
    iput-object p1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    .line 101
    iput-object p2, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    .line 102
    new-instance v5, Lcom/google/android/apps/plus/iu/Authorizer;

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAuthTokenType()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/apps/plus/iu/Authorizer;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

    .line 106
    :try_start_29
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/iu/Authorizer;->getAuthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthToken:Ljava/lang/String;
    :try_end_37
    .catch Landroid/accounts/OperationCanceledException; {:try_start_29 .. :try_end_37} :catch_77
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_37} :catch_8d
    .catch Landroid/accounts/AuthenticatorException; {:try_start_29 .. :try_end_37} :catch_9e

    .line 127
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getRequestTemplate()Ljava/lang/String;

    move-result-object v4

    .line 128
    .local v4, requestTemplate:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthToken:Ljava/lang/String;

    if-nez v5, :cond_b3

    .line 129
    const-string v5, "Authorization: GoogleLogin auth=%=_auth_token_=%\r\n"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    :goto_49
    const/4 v2, 0x0

    .line 138
    .local v2, is:Ljava/io/InputStream;
    :try_start_4a
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 140
    .local v1, in:Ljava/io/InputStream;
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5d
    .catchall {:try_start_4a .. :try_end_5d} :catchall_cf
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4a .. :try_end_5d} :catch_c4
    .catch Ljava/io/FileNotFoundException; {:try_start_4a .. :try_end_5d} :catch_d4
    .catch Lorg/xml/sax/SAXException; {:try_start_4a .. :try_end_5d} :catch_db

    .line 141
    .end local v2           #is:Ljava/io/InputStream;
    .local v3, is:Ljava/io/InputStream;
    :try_start_5d
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_bc

    .line 142
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUrl()Landroid/net/Uri;

    move-result-object v5

    invoke-direct {p0, v3, v5, v4}, Lcom/google/android/apps/plus/iu/GDataUploader;->start(Ljava/io/InputStream;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    :try_end_72
    .catchall {:try_start_5d .. :try_end_72} :catchall_f4
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5d .. :try_end_72} :catch_fd
    .catch Ljava/io/FileNotFoundException; {:try_start_5d .. :try_end_72} :catch_fa
    .catch Lorg/xml/sax/SAXException; {:try_start_5d .. :try_end_72} :catch_f7

    move-result-object v5

    .line 157
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    :goto_76
    return-object v5

    .line 107
    .end local v1           #in:Ljava/io/InputStream;
    .end local v3           #is:Ljava/io/InputStream;
    .end local v4           #requestTemplate:Ljava/lang/String;
    :catch_77
    move-exception v0

    .line 109
    .local v0, e:Landroid/accounts/OperationCanceledException;
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_87

    .line 110
    const-string v5, "iu.UploadsManager"

    const-string v6, "authentication canceled"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 112
    :cond_87
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 113
    .end local v0           #e:Landroid/accounts/OperationCanceledException;
    :catch_8d
    move-exception v0

    .line 116
    .local v0, e:Ljava/io/IOException;
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_9d

    .line 117
    const-string v5, "iu.UploadsManager"

    const-string v6, "authentication failed"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 119
    :cond_9d
    throw v0

    .line 120
    .end local v0           #e:Ljava/io/IOException;
    :catch_9e
    move-exception v0

    .line 121
    .local v0, e:Landroid/accounts/AuthenticatorException;
    const-string v5, "iu.UploadsManager"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_ad

    .line 122
    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 124
    :cond_ad
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 132
    .end local v0           #e:Landroid/accounts/AuthenticatorException;
    .restart local v4       #requestTemplate:Ljava/lang/String;
    :cond_b3
    const-string v5, "%=_auth_token_=%"

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthToken:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_49

    .line 144
    .restart local v1       #in:Ljava/io/InputStream;
    .restart local v3       #is:Ljava/io/InputStream;
    :cond_bc
    :try_start_bc
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/iu/GDataUploader;->resume(Ljava/io/InputStream;)Lcom/google/android/apps/plus/iu/UploadedEntry;
    :try_end_bf
    .catchall {:try_start_bc .. :try_end_bf} :catchall_f4
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_bc .. :try_end_bf} :catch_fd
    .catch Ljava/io/FileNotFoundException; {:try_start_bc .. :try_end_bf} :catch_fa
    .catch Lorg/xml/sax/SAXException; {:try_start_bc .. :try_end_bf} :catch_f7

    move-result-object v5

    .line 157
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_76

    .line 146
    .end local v1           #in:Ljava/io/InputStream;
    .end local v3           #is:Ljava/io/InputStream;
    .restart local v2       #is:Ljava/io/InputStream;
    :catch_c4
    move-exception v0

    .line 147
    .local v0, e:Lorg/apache/http/client/ClientProtocolException;
    :goto_c5
    :try_start_c5
    new-instance v5, Ljava/io/IOException;

    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_cf
    .catchall {:try_start_c5 .. :try_end_cf} :catchall_cf

    .line 157
    .end local v0           #e:Lorg/apache/http/client/ClientProtocolException;
    :catchall_cf
    move-exception v5

    :goto_d0
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5

    .line 148
    :catch_d4
    move-exception v0

    .line 149
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_d5
    :try_start_d5
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 150
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_db
    move-exception v0

    .line 152
    .local v0, e:Lorg/xml/sax/SAXException;
    :goto_dc
    const-string v5, "iu.UploadsManager"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_ec

    .line 153
    const-string v5, "iu.UploadsManager"

    const-string v6, "error in parsing response"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 155
    :cond_ec
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v6, "error in parsing response"

    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_f4
    .catchall {:try_start_d5 .. :try_end_f4} :catchall_cf

    .line 157
    .end local v0           #e:Lorg/xml/sax/SAXException;
    .end local v2           #is:Ljava/io/InputStream;
    .restart local v1       #in:Ljava/io/InputStream;
    .restart local v3       #is:Ljava/io/InputStream;
    :catchall_f4
    move-exception v5

    move-object v2, v3

    .end local v3           #is:Ljava/io/InputStream;
    .restart local v2       #is:Ljava/io/InputStream;
    goto :goto_d0

    .line 150
    .end local v2           #is:Ljava/io/InputStream;
    .restart local v3       #is:Ljava/io/InputStream;
    :catch_f7
    move-exception v0

    move-object v2, v3

    .end local v3           #is:Ljava/io/InputStream;
    .restart local v2       #is:Ljava/io/InputStream;
    goto :goto_dc

    .line 148
    .end local v2           #is:Ljava/io/InputStream;
    .restart local v3       #is:Ljava/io/InputStream;
    :catch_fa
    move-exception v0

    move-object v2, v3

    .end local v3           #is:Ljava/io/InputStream;
    .restart local v2       #is:Ljava/io/InputStream;
    goto :goto_d5

    .line 146
    .end local v2           #is:Ljava/io/InputStream;
    .restart local v3       #is:Ljava/io/InputStream;
    :catch_fd
    move-exception v0

    move-object v2, v3

    .end local v3           #is:Ljava/io/InputStream;
    .restart local v2       #is:Ljava/io/InputStream;
    goto :goto_c5
.end method
