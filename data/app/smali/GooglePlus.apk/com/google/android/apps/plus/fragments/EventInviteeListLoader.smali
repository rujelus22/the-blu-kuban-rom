.class public final Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "EventInviteeListLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$CircleQuery;,
        Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$InviteeQuery;
    }
.end annotation


# static fields
.field private static final INVITEE_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mEventId:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 74
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "is_header"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "blacklisted"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "rsvp"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "is_past"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "invitee_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 125
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mEventId:Ljava/lang/String;

    .line 126
    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mOwnerId:Ljava/lang/String;

    .line 127
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 128
    return-void
.end method

.method private insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V
    .registers 20
    .parameter "event"
    .parameter "rsvpType"
    .parameter
    .parameter "result"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;",
            "Lcom/google/android/apps/plus/phone/EsMatrixCursor;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p3, invitees:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Invitee;>;"
    .local p5, peopleToCirclesMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsEventData;->getInviteeSummary(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Lcom/google/api/services/plusi/model/InviteeSummary;

    move-result-object v3

    .line 210
    .local v3, summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    const/4 v5, 0x0

    const/4 v6, 0x0

    move v7, v6

    :goto_7
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_23

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {v6}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->isPersonVisible(Lcom/google/api/services/plusi/model/EmbedsPerson;)Z

    move-result v6

    if-eqz v6, :cond_1f

    add-int/lit8 v5, v5, 0x1

    :cond_1f
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_7

    .line 211
    .local v5, visibleCount:I
    :cond_23
    if-eqz v3, :cond_ee

    iget-object v6, v3, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 213
    .local v4, totalCount:I
    :goto_2b
    if-lez v4, :cond_126

    .line 214
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 216
    .local v1, now:J
    invoke-static {p1, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v6

    sget-object v7, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    array-length v7, v7

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0x8

    aput-object p2, v7, v8

    const/16 v8, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0x9

    if-eqz v6, :cond_f4

    const/4 v6, 0x1

    :goto_5e
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v8

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 217
    if-eqz p3, :cond_fd

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_6f
    :goto_6f
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_fd

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v7, :cond_6f

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->isPersonVisible(Lcom/google/api/services/plusi/model/EmbedsPerson;)Z

    move-result v7

    if-eqz v7, :cond_6f

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v10, v7, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    if-eqz v7, :cond_f7

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_f7

    const/4 v7, 0x1

    :goto_98
    sget-object v8, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    array-length v8, v8

    new-array v11, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    const/4 v8, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    const/4 v12, 0x2

    if-eqz v10, :cond_f9

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v13, "g:"

    invoke-direct {v8, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_c2
    aput-object v8, v11, v12

    const/4 v8, 0x3

    aput-object v10, v11, v8

    const/4 v8, 0x4

    iget-object v12, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v12, v12, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    aput-object v12, v11, v8

    const/4 v8, 0x5

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EmbedsPerson;->email:Ljava/lang/String;

    aput-object v6, v11, v8

    const/4 v6, 0x6

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v11, v6

    const/4 v8, 0x7

    if-eqz v7, :cond_fb

    const/4 v6, 0x1

    :goto_e2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v11, v8

    move-object/from16 v0, p4

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_6f

    .line 211
    .end local v1           #now:J
    .end local v4           #totalCount:I
    :cond_ee
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    goto/16 :goto_2b

    .line 216
    .restart local v1       #now:J
    .restart local v4       #totalCount:I
    :cond_f4
    const/4 v6, 0x0

    goto/16 :goto_5e

    .line 217
    :cond_f7
    const/4 v7, 0x0

    goto :goto_98

    :cond_f9
    const/4 v8, 0x0

    goto :goto_c2

    :cond_fb
    const/4 v6, 0x0

    goto :goto_e2

    .line 218
    :cond_fd
    sub-int v6, v4, v5

    if-lez v6, :cond_126

    sget-object v7, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    array-length v7, v7

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v8

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 220
    .end local v1           #now:J
    :cond_126
    return-void
.end method

.method private static isPersonVisible(Lcom/google/api/services/plusi/model/EmbedsPerson;)Z
    .registers 2
    .parameter "person"

    .prologue
    .line 236
    if-eqz p0, :cond_14

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EmbedsPerson;->email:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private queryCirclesForPeople(Ljava/util/List;)Ljava/util/HashMap;
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, invitees:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Invitee;>;"
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 301
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 303
    .local v11, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/api/services/plusi/model/Invitee;

    .line 304
    .local v10, invitee:Lcom/google/api/services/plusi/model/Invitee;
    iget-object v0, v10, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v0, :cond_b

    iget-object v0, v10, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 305
    iget-object v0, v10, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    invoke-virtual {v11, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 309
    .end local v10           #invitee:Lcom/google/api/services/plusi/model/Invitee;
    :cond_29
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 310
    .local v12, sb:Ljava/lang/StringBuilder;
    const-string v0, "gaia_id IN("

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const/4 v8, 0x0

    .local v8, i:I
    :goto_34
    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    move-result v0

    if-ge v8, v0, :cond_49

    .line 312
    if-lez v8, :cond_41

    .line 313
    const/16 v0, 0x2c

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 315
    :cond_41
    const/16 v0, 0x3f

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 311
    add-int/lit8 v8, v8, 0x1

    goto :goto_34

    .line 317
    :cond_49
    const/16 v0, 0x29

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 318
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 319
    .local v5, selection:Ljava/lang/String;
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    .line 321
    .local v6, selectionArgs:[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$CircleQuery;->PROJECTION:[Ljava/lang/String;

    move-object v3, v2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 324
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_89

    .line 326
    :goto_6d
    :try_start_6d
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_86

    .line 327
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_80
    .catchall {:try_start_6d .. :try_end_80} :catchall_81

    goto :goto_6d

    .line 331
    :catchall_81
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_86
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 334
    :cond_89
    return-object v11
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 33

    .prologue
    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mEventId:Ljava/lang/String;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mOwnerId:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 136
    :cond_c
    const/4 v6, 0x0

    .line 201
    :goto_d
    return-object v6

    .line 138
    :cond_e
    const/16 v30, 0x0

    .line 139
    .local v30, list:Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mEventId:Ljava/lang/String;

    sget-object v9, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$InviteeQuery;->PROJECTION:[Ljava/lang/String;

    invoke-static {v2, v4, v8, v9}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 142
    .local v27, cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 144
    .local v3, eventInfo:Lcom/google/api/services/plusi/model/PlusEvent;
    :try_start_23
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4f

    .line 145
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    move-object v3, v0

    .line 147
    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v26

    .line 148
    .local v26, bytes:[B
    if-eqz v26, :cond_4f

    .line 149
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/json/EsJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v30

    .end local v30           #list:Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    check-cast v30, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    :try_end_4f
    .catchall {:try_start_23 .. :try_end_4f} :catchall_56

    .line 153
    .end local v26           #bytes:[B
    .restart local v30       #list:Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    :cond_4f
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 156
    if-nez v30, :cond_5b

    .line 157
    const/4 v6, 0x0

    goto :goto_d

    .line 153
    .end local v30           #list:Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    :catchall_56
    move-exception v2

    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    throw v2

    .line 161
    .restart local v30       #list:Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    :cond_5b
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v5, going:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/Invitee;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v11, maybe:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/Invitee;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 164
    .local v15, notGoing:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/Invitee;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .local v19, unknown:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/Invitee;>;"
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v23, removed:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/Invitee;>;"
    move-object/from16 v0, v30

    iget-object v2, v0, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, i$:Ljava/util/Iterator;
    :goto_7c
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e8

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/api/services/plusi/model/Invitee;

    .line 169
    .local v29, invitee:Lcom/google/api/services/plusi/model/Invitee;
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    if-eqz v2, :cond_a0

    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a0

    .line 170
    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7c

    .line 172
    :cond_a0
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    move-object/from16 v31, v0

    .line 173
    .local v31, rsvp:Ljava/lang/String;
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_ba

    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c0

    .line 175
    :cond_ba
    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7c

    .line 176
    :cond_c0
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d0

    .line 177
    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7c

    .line 178
    :cond_d0
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e0

    .line 179
    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7c

    .line 181
    :cond_e0
    move-object/from16 v0, v19

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7c

    .line 186
    .end local v29           #invitee:Lcom/google/api/services/plusi/model/Invitee;
    .end local v31           #rsvp:Ljava/lang/String;
    :cond_e8
    move-object/from16 v0, v30

    iget-object v2, v0, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->queryCirclesForPeople(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v7

    .line 188
    .local v7, peopleToCirclesMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v6, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v2, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    invoke-direct {v6, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 190
    .local v6, result:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    sget-object v4, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    .line 192
    sget-object v10, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    move-object/from16 v8, p0

    move-object v9, v3

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    .line 194
    sget-object v14, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    move-object/from16 v12, p0

    move-object v13, v3

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    invoke-direct/range {v12 .. v17}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    .line 196
    sget-object v18, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    move-object/from16 v16, p0

    move-object/from16 v17, v3

    move-object/from16 v20, v6

    move-object/from16 v21, v7

    invoke-direct/range {v16 .. v21}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    .line 198
    const-string v22, "REMOVED"

    move-object/from16 v20, p0

    move-object/from16 v21, v3

    move-object/from16 v24, v6

    move-object/from16 v25, v7

    invoke-direct/range {v20 .. v25}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    goto/16 :goto_d
.end method
