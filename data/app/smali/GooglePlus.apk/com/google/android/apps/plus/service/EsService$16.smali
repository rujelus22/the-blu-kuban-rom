.class final Lcom/google/android/apps/plus/service/EsService$16;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/EsService;->processIntent2$751513a6(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/EsService;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$eventId:Ljava/lang/String;

.field final synthetic val$fetchNewer:Z

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$invitationToken:Ljava/lang/String;

.field final synthetic val$noToken:Z

.field final synthetic val$pollingToken:Ljava/lang/String;

.field final synthetic val$resumeToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/Intent;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4117
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$16;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$eventId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$pollingToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$resumeToken:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$invitationToken:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$fetchNewer:Z

    iput-boolean p9, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$noToken:Z

    iput-object p10, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 14

    .prologue
    const/4 v12, 0x0

    .line 4120
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 4123
    :try_start_6
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$eventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$pollingToken:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$resumeToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$invitationToken:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$fetchNewer:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$noToken:Z

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->readEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)Z

    move-result v11

    .line 4126
    .local v11, success:Z
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService$16;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$intent:Landroid/content/Intent;

    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v2, v11}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Z)V

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_29} :catch_2a

    .line 4131
    .end local v11           #success:Z
    :goto_29
    return-void

    .line 4127
    :catch_2a
    move-exception v10

    .line 4128
    .local v10, ex:Ljava/lang/Exception;
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService$16;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$16;->val$intent:Landroid/content/Intent;

    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v12, v10}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v0, v1, v2, v12}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto :goto_29
.end method
