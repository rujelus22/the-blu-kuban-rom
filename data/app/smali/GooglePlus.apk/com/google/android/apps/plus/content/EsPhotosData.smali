.class public Lcom/google/android/apps/plus/content/EsPhotosData;
.super Ljava/lang/Object;
.source "EsPhotosData.java"


# static fields
.field static final FINGERPRINT_STREAM_PREFIX_LENGTH:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 69
    const/4 v0, 0x6

    sput v0, Lcom/google/android/apps/plus/content/EsPhotosData;->FINGERPRINT_STREAM_PREFIX_LENGTH:I

    return-void
.end method

.method static cleanupData(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 22
    .parameter "db"
    .parameter "account"

    .prologue
    .line 359
    const-string v17, "SELECT count(*) FROM photo"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v12

    .line 361
    .local v12, origPhotoCount:J
    const-string v17, "SELECT count(*) FROM album"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v10

    .line 364
    .local v10, origAlbumCount:J
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v9

    .line 367
    .local v9, gaiaId:Ljava/lang/String;
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v9, v16, v17

    .line 369
    .local v16, photosOfUserArgs:[Ljava/lang/String;
    const-string v17, "photos_of_user"

    const-string v18, "photo_of_user_id!=?"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 372
    const-string v17, "photos_in_circle"

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 375
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v15, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    aput-object v9, v15, v17

    .line 380
    .local v15, photoStreamArgs:[Ljava/lang/String;
    const-string v17, "photos_in_stream"

    const-string v18, "photo_id IN ( SELECT photo_id FROM photos_by_stream_view WHERE owner_id!=? )"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2, v15}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 383
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v14, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    aput-object v9, v14, v17

    .line 388
    .local v14, photoAlbumArgs:[Ljava/lang/String;
    const-string v17, "photos_in_album"

    const-string v18, "photo_id IN ( SELECT photo_id FROM photos_by_album_view WHERE owner_id!=? )"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 391
    const-string v17, "photos_in_event"

    const-string v18, "event_id NOT IN ( SELECT event_id FROM events )"

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 397
    const-string v17, "photo"

    const-string v18, "photo_id NOT IN ( SELECT photo_id FROM photos_in_album) AND photo_id NOT IN ( SELECT photo_id FROM photos_in_event) AND photo_id NOT IN ( SELECT photo_id FROM photos_in_stream) AND photo_id NOT IN ( SELECT photo_id FROM photos_of_user) AND photo_id NOT IN ( SELECT cover_photo_id FROM album) AND album_id NOT IN ( SELECT album_id FROM activities) "

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 419
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v4, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    aput-object v9, v4, v17

    .line 420
    .local v4, albumArgs:[Ljava/lang/String;
    const-string v17, "album"

    const-string v18, "owner_id != ? AND album_id NOT IN ( SELECT DISTINCT album_id FROM photo) "

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 427
    const-string v17, "SELECT count(*) FROM photo"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v7

    .line 429
    .local v7, finalPhotoCount:J
    const-string v17, "SELECT count(*) FROM album"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v5

    .line 432
    .local v5, finalAlbumCount:J
    const-string v17, "EsPhotosData"

    const/16 v18, 0x3

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_11d

    .line 433
    const-string v17, "EsPhotosData"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "cleanupData removed dead photos; was: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", now: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v17, "EsPhotosData"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "cleanupData removed dead albums; was: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", now: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_11d
    return-void
.end method

.method static getAlbumEntityMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 15
    .parameter "db"
    .parameter "gaiaId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 543
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 545
    .local v10, entityMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "album"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "album_id"

    aput-object v0, v2, v6

    const-string v0, "entity_version"

    aput-object v0, v2, v4

    const-string v3, "owner_id=? AND title IS NOT NULL"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 551
    .local v9, cursor:Landroid/database/Cursor;
    :goto_22
    :try_start_22
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 552
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 553
    .local v8, albumId:Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 554
    .local v11, entityVersion:J
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_39
    .catchall {:try_start_22 .. :try_end_39} :catchall_3a

    goto :goto_22

    .line 557
    .end local v8           #albumId:Ljava/lang/String;
    .end local v11           #entityVersion:J
    :catchall_3a
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3f
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 559
    return-object v10
.end method

.method static getAlbumRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;
    .registers 11
    .parameter "db"
    .parameter "albumId"

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 500
    const-string v1, "album"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v3, "album_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 505
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_18
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_26
    .catchall {:try_start_18 .. :try_end_26} :catchall_2b

    move-result-object v5

    .line 507
    :cond_27
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v5

    :catchall_2b
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static getCurrentAlbumMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 13
    .parameter "db"
    .parameter "albumId"
    .parameter "gaiaId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 568
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 569
    .local v9, returnMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Long;>;"
    const-string v1, "photos_by_album_view"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "photo_id"

    aput-object v0, v2, v6

    const-string v0, "entity_version"

    aput-object v0, v2, v7

    const-string v3, "owner_id=? AND album_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    aput-object p1, v4, v7

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 575
    .local v8, cursor:Landroid/database/Cursor;
    :goto_24
    :try_start_24
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 576
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3f
    .catchall {:try_start_24 .. :try_end_3f} :catchall_40

    goto :goto_24

    .line 579
    :catchall_40
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_45
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 581
    return-object v9
.end method

.method static getCurrentStreamMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
    .registers 13
    .parameter "db"
    .parameter "streamId"
    .parameter "gaiaId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 591
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 592
    .local v9, returnMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Long;>;"
    const-string v1, "photos_by_stream_view"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "photo_id"

    aput-object v0, v2, v6

    const-string v0, "entity_version"

    aput-object v0, v2, v7

    const-string v3, "owner_id=? AND stream_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    aput-object p1, v4, v7

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 598
    .local v8, cursor:Landroid/database/Cursor;
    :goto_24
    :try_start_24
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 599
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3f
    .catchall {:try_start_24 .. :try_end_3f} :catchall_40

    goto :goto_24

    .line 602
    :catchall_40
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_45
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 604
    return-object v9
.end method

.method static getDeltaTime(J)Ljava/lang/String;
    .registers 9
    .parameter "startTime"

    .prologue
    const-wide/16 v5, 0x3e8

    .line 670
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 671
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v0, v3, p0

    .line 673
    .local v0, deltaTime:J
    div-long v3, v0, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    rem-long v4, v0, v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 678
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getImageDataFromCloud(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)[B
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "photoId"

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 309
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 312
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "photo"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "url"

    aput-object v3, v2, v7

    const-string v3, "photo_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 317
    .local v9, cursor:Landroid/database/Cursor;
    if-nez v9, :cond_27

    .line 335
    :goto_26
    return-object v5

    .line 322
    :cond_27
    :try_start_27
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_49

    .line 323
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_49

    .line 324
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 325
    .local v11, url:Ljava/lang/String;
    new-instance v10, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;

    invoke-direct {v10, p0, p1, v11}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 327
    .local v10, op:Lcom/google/android/apps/plus/api/DownloadPhotoOperation;
    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->start()V

    .line 328
    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->getBytes()[B
    :try_end_44
    .catchall {:try_start_27 .. :try_end_44} :catchall_4d

    move-result-object v5

    .line 332
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_26

    .end local v10           #op:Lcom/google/android/apps/plus/api/DownloadPhotoOperation;
    .end local v11           #url:Ljava/lang/String;
    :cond_49
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_26

    :catchall_4d
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method static getPhotoRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;
    .registers 11
    .parameter "db"
    .parameter "photoId"

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 513
    const-string v1, "photo"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v3, "photo_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 518
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_18
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_26
    .catchall {:try_start_18 .. :try_end_26} :catchall_2b

    move-result-object v5

    .line 520
    :cond_27
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v5

    :catchall_2b
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static getPhotosHomeRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    .registers 11
    .parameter "db"
    .parameter "type"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 526
    const-string v1, "photo_home"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v3, "type=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 532
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_18
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_22
    .catchall {:try_start_18 .. :try_end_22} :catchall_2a

    move-result-wide v0

    .line 534
    :goto_23
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 532
    :cond_27
    const-wide/16 v0, -0x1

    goto :goto_23

    .line 534
    :catchall_2a
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static final hexToBytes(Ljava/lang/CharSequence;)[B
    .registers 9
    .parameter "str"

    .prologue
    const/4 v5, 0x0

    .line 646
    if-eqz p0, :cond_9

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_b

    .line 647
    :cond_9
    const/4 v0, 0x0

    .line 665
    :cond_a
    return-object v0

    .line 650
    :cond_b
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    div-int/lit8 v4, v4, 0x2

    new-array v0, v4, [B

    .line 651
    .local v0, bytes:[B
    aput-byte v5, v0, v5

    .line 652
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    rem-int/lit8 v3, v4, 0x2

    .line 653
    .local v3, nibbleIdx:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1e
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ge v2, v4, :cond_a

    .line 654
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 655
    .local v1, c:C
    const/16 v4, 0x30

    if-lt v1, v4, :cond_30

    const/16 v4, 0x39

    if-le v1, v4, :cond_40

    :cond_30
    const/16 v4, 0x61

    if-lt v1, v4, :cond_38

    const/16 v4, 0x66

    if-le v1, v4, :cond_40

    :cond_38
    const/16 v4, 0x41

    if-lt v1, v4, :cond_4b

    const/16 v4, 0x46

    if-gt v1, v4, :cond_4b

    :cond_40
    const/4 v4, 0x1

    :goto_41
    if-nez v4, :cond_4d

    .line 656
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "string contains non-hex chars"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4b
    move v4, v5

    .line 655
    goto :goto_41

    .line 658
    :cond_4d
    rem-int/lit8 v4, v3, 0x2

    if-nez v4, :cond_61

    .line 659
    shr-int/lit8 v4, v3, 0x1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPhotosData;->hexValue(C)I

    move-result v6

    shl-int/lit8 v6, v6, 0x4

    int-to-byte v6, v6

    aput-byte v6, v0, v4

    .line 663
    :goto_5c
    add-int/lit8 v3, v3, 0x1

    .line 653
    add-int/lit8 v2, v2, 0x1

    goto :goto_1e

    .line 661
    :cond_61
    shr-int/lit8 v4, v3, 0x1

    aget-byte v6, v0, v4

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPhotosData;->hexValue(C)I

    move-result v7

    int-to-byte v7, v7

    add-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v0, v4

    goto :goto_5c
.end method

.method private static final hexValue(C)I
    .registers 2
    .parameter "c"

    .prologue
    .line 630
    const/16 v0, 0x30

    if-lt p0, v0, :cond_b

    const/16 v0, 0x39

    if-gt p0, v0, :cond_b

    .line 631
    add-int/lit8 v0, p0, -0x30

    .line 635
    :goto_a
    return v0

    .line 632
    :cond_b
    const/16 v0, 0x61

    if-lt p0, v0, :cond_18

    const/16 v0, 0x66

    if-gt p0, v0, :cond_18

    .line 633
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_a

    .line 635
    :cond_18
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_a
.end method

.method public static loadLocalBitmap(Landroid/content/Context;Lcom/google/android/apps/plus/content/LocalImageRequest;)Landroid/graphics/Bitmap;
    .registers 12
    .parameter "context"
    .parameter "request"

    .prologue
    .line 267
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/LocalImageRequest;->getUri()Landroid/net/Uri;

    move-result-object v5

    .line 268
    .local v5, uri:Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/LocalImageRequest;->getWidth()I

    move-result v6

    .line 269
    .local v6, width:I
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/LocalImageRequest;->getHeight()I

    move-result v1

    .line 271
    .local v1, height:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 272
    .local v4, resolver:Landroid/content/ContentResolver;
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->safeGetMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 276
    .local v2, mimeType:Ljava/lang/String;
    :try_start_14
    invoke-static {v2}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isImageMimeType(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_23

    .line 277
    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-static {v4, v5, v7}, Lcom/google/android/apps/plus/util/ImageUtils;->createLocalBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 296
    :goto_22
    return-object v0

    .line 279
    :cond_23
    invoke-static {v2}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isVideoMimeType(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2e

    .line 280
    invoke-static {p0, v5, v6, v1}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, bitmap:Landroid/graphics/Bitmap;
    goto :goto_22

    .line 283
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :cond_2e
    const-string v7, "EsPhotosData"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4b

    .line 284
    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "LocalImageRequest#loadBytes: unknown mimeType="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_14 .. :try_end_4b} :catch_4d

    .line 286
    :cond_4b
    const/4 v0, 0x0

    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    goto :goto_22

    .line 290
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :catch_4d
    move-exception v3

    .line 291
    .local v3, oome:Ljava/lang/OutOfMemoryError;
    const-string v7, "EsPhotosData"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5e

    .line 292
    const-string v7, "EsPhotosData"

    const-string v8, "Could not load image"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 296
    :cond_5e
    const/4 v0, 0x0

    goto :goto_22
.end method

.method public static loadLocalImageBytes(Landroid/content/Context;Lcom/google/android/apps/plus/content/LocalImageRequest;)[B
    .registers 13
    .parameter "context"
    .parameter "request"

    .prologue
    const/4 v2, 0x0

    .line 228
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/LocalImageRequest;->getUri()Landroid/net/Uri;

    move-result-object v6

    .line 229
    .local v6, uri:Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/LocalImageRequest;->getWidth()I

    move-result v7

    .line 230
    .local v7, width:I
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/LocalImageRequest;->getHeight()I

    move-result v1

    .line 232
    .local v1, height:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 233
    .local v5, resolver:Landroid/content/ContentResolver;
    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->safeGetMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 237
    .local v3, mimeType:Ljava/lang/String;
    :try_start_15
    invoke-static {v3}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isImageMimeType(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_22

    .line 238
    invoke-static {v5, v6, v7, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->createCroppedBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 251
    .local v0, bitmap:Landroid/graphics/Bitmap;
    :goto_1f
    if-nez v0, :cond_4c

    .line 259
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :cond_21
    :goto_21
    return-object v2

    .line 240
    :cond_22
    invoke-static {v3}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isVideoMimeType(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2d

    .line 241
    invoke-static {p0, v6, v7, v1}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    goto :goto_1f

    .line 244
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :cond_2d
    const-string v8, "EsPhotosData"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_4a

    .line 245
    const-string v8, "EsPhotosData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "LocalImageRequest#loadBytes: unknown mimeType="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_4a
    const/4 v0, 0x0

    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    goto :goto_1f

    .line 251
    :cond_4c
    invoke-static {v0}, Lcom/google/android/apps/plus/util/ImageUtils;->compressBitmap(Landroid/graphics/Bitmap;)[B
    :try_end_4f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_15 .. :try_end_4f} :catch_51

    move-result-object v2

    goto :goto_21

    .line 253
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :catch_51
    move-exception v4

    .line 254
    .local v4, oome:Ljava/lang/OutOfMemoryError;
    const-string v8, "EsPhotosData"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_21

    .line 255
    const-string v8, "EsPhotosData"

    const-string v9, "Could not load image"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21
.end method

.method public static loadPicasaImageBytes(Landroid/content/Context;JLjava/lang/String;III)[B
    .registers 34
    .parameter "context"
    .parameter "photoId"
    .parameter "remoteUrl"
    .parameter "width"
    .parameter "height"
    .parameter "cropType"

    .prologue
    .line 131
    invoke-static/range {p0 .. p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/picasastore/PicasaStoreFacade;->getPhotoUri(J)Landroid/net/Uri;

    move-result-object v22

    .line 132
    .local v22, photoUri:Landroid/net/Uri;
    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v26

    .line 134
    .local v26, uriBuilder:Landroid/net/Uri$Builder;
    if-eqz p3, :cond_23

    const-string v3, "content:"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_23

    .line 135
    const-string v3, "content_url"

    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 138
    :cond_23
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 139
    .local v21, maxDimension:I
    const/16 v3, 0x12c

    move/from16 v0, v21

    if-le v0, v3, :cond_ec

    const-string v18, "screennail"

    .line 142
    .local v18, imageType:Ljava/lang/String;
    :goto_2f
    const-string v3, "type"

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 144
    invoke-virtual/range {v26 .. v26}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v23

    .line 146
    .local v23, queryUri:Landroid/net/Uri;
    const/16 v3, 0x12c

    move/from16 v0, v21

    if-le v0, v3, :cond_f0

    const v19, 0x19000

    .line 147
    .local v19, initialStreamSize:I
    :goto_45
    new-instance v12, Ljava/io/ByteArrayOutputStream;

    move/from16 v0, v19

    invoke-direct {v12, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 148
    .local v12, baos:Ljava/io/ByteArrayOutputStream;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 150
    .local v24, startTime:J
    const/16 v3, 0x2800

    :try_start_52
    new-array v11, v3, [B

    .line 154
    .local v11, array:[B
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_5d
    .catchall {:try_start_52 .. :try_end_5d} :catchall_22c
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_5d} :catch_71
    .catch Ljava/lang/OutOfMemoryError; {:try_start_52 .. :try_end_5d} :catch_1d5

    move-result-object v20

    .line 158
    .local v20, is:Ljava/io/InputStream;
    :goto_5e
    :try_start_5e
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/io/InputStream;->read([B)I

    move-result v13

    .line 159
    .local v13, bytesRead:I
    const/4 v3, -0x1

    if-eq v13, v3, :cond_f4

    .line 160
    const/4 v3, 0x0

    invoke-virtual {v12, v11, v3, v13}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_6b
    .catchall {:try_start_5e .. :try_end_6b} :catchall_6c

    goto :goto_5e

    .line 171
    .end local v13           #bytesRead:I
    :catchall_6c
    move-exception v3

    .line 172
    :try_start_6d
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_22c
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_70} :catch_27b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6d .. :try_end_70} :catch_1d5

    .line 175
    :goto_70
    :try_start_70
    throw v3
    :try_end_71
    .catchall {:try_start_70 .. :try_end_71} :catchall_22c
    .catch Ljava/io/IOException; {:try_start_70 .. :try_end_71} :catch_71
    .catch Ljava/lang/OutOfMemoryError; {:try_start_70 .. :try_end_71} :catch_1d5

    .line 177
    .end local v11           #array:[B
    .end local v20           #is:Ljava/io/InputStream;
    :catch_71
    move-exception v16

    .line 178
    .local v16, e:Ljava/io/IOException;
    :try_start_72
    const-string v3, "EsPhotosData"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 180
    if-eqz p3, :cond_187

    const-string v3, "content:"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_187

    .line 181
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    .line 182
    .local v4, account:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v2, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p0

    move-object/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;IIILandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 185
    .local v2, op:Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;->start()V

    .line 186
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;->getBytes()[B
    :try_end_a0
    .catchall {:try_start_72 .. :try_end_a0} :catchall_22c

    move-result-object v17

    .line 191
    const-string v3, "EsPhotosData"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e8

    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v14, v5, v24

    .line 193
    .local v14, deltaTime:J
    const-string v3, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]; Done in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    div-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    rem-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " seconds"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    .end local v14           #deltaTime:J
    :cond_e8
    :try_start_e8
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_eb
    .catch Ljava/io/IOException; {:try_start_e8 .. :try_end_eb} :catch_27e

    .line 202
    .end local v2           #op:Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;
    .end local v4           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v16           #e:Ljava/io/IOException;
    :goto_eb
    return-object v17

    .line 139
    .end local v12           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v18           #imageType:Ljava/lang/String;
    .end local v19           #initialStreamSize:I
    .end local v23           #queryUri:Landroid/net/Uri;
    .end local v24           #startTime:J
    :cond_ec
    const-string v18, "thumbnail"

    goto/16 :goto_2f

    .line 146
    .restart local v18       #imageType:Ljava/lang/String;
    .restart local v23       #queryUri:Landroid/net/Uri;
    :cond_f0
    const/16 v19, 0x2800

    goto/16 :goto_45

    .line 164
    .restart local v11       #array:[B
    .restart local v12       #baos:Ljava/io/ByteArrayOutputStream;
    .restart local v13       #bytesRead:I
    .restart local v19       #initialStreamSize:I
    .restart local v20       #is:Ljava/io/InputStream;
    .restart local v24       #startTime:J
    :cond_f4
    :try_start_f4
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v17

    .line 165
    .local v17, imageArray:[B
    const-string v3, "EsPhotosData"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_132

    .line 166
    const-string v5, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "]; size: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    array-length v6, v0

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", url: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez p3, :cond_184

    const-string v3, "<< NULL >>"

    :goto_127
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_132
    .catchall {:try_start_f4 .. :try_end_132} :catchall_6c

    .line 172
    :cond_132
    :try_start_132
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V
    :try_end_135
    .catchall {:try_start_132 .. :try_end_135} :catchall_22c
    .catch Ljava/io/IOException; {:try_start_132 .. :try_end_135} :catch_278
    .catch Ljava/lang/OutOfMemoryError; {:try_start_132 .. :try_end_135} :catch_1d5

    .line 191
    :goto_135
    const-string v3, "EsPhotosData"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_17c

    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v14, v5, v24

    .line 193
    .restart local v14       #deltaTime:J
    const-string v3, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]; Done in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    div-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    rem-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " seconds"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    .end local v14           #deltaTime:J
    :cond_17c
    :try_start_17c
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_17f
    .catch Ljava/io/IOException; {:try_start_17c .. :try_end_17f} :catch_181

    goto/16 :goto_eb

    :catch_181
    move-exception v3

    goto/16 :goto_eb

    :cond_184
    move-object/from16 v3, p3

    .line 166
    goto :goto_127

    .line 191
    .end local v11           #array:[B
    .end local v13           #bytesRead:I
    .end local v17           #imageArray:[B
    .end local v20           #is:Ljava/io/InputStream;
    .restart local v16       #e:Ljava/io/IOException;
    :cond_187
    const-string v3, "EsPhotosData"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1ce

    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v14, v5, v24

    .line 193
    .restart local v14       #deltaTime:J
    const-string v3, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]; Done in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    div-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    rem-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " seconds"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    .end local v14           #deltaTime:J
    :cond_1ce
    :try_start_1ce
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1d1
    .catch Ljava/io/IOException; {:try_start_1ce .. :try_end_1d1} :catch_281

    .line 202
    .end local v16           #e:Ljava/io/IOException;
    :goto_1d1
    const/16 v17, 0x0

    goto/16 :goto_eb

    .line 188
    :catch_1d5
    move-exception v16

    .line 189
    .local v16, e:Ljava/lang/OutOfMemoryError;
    :try_start_1d6
    const-string v3, "EsPhotosData"

    const-string v5, "#loadPicasaImageBytes"

    move-object/from16 v0, v16

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1df
    .catchall {:try_start_1d6 .. :try_end_1df} :catchall_22c

    .line 191
    const-string v3, "EsPhotosData"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_226

    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v14, v5, v24

    .line 193
    .restart local v14       #deltaTime:J
    const-string v3, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]; Done in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    div-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    rem-long v6, v14, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " seconds"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    .end local v14           #deltaTime:J
    :cond_226
    :try_start_226
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_229
    .catch Ljava/io/IOException; {:try_start_226 .. :try_end_229} :catch_22a

    goto :goto_1d1

    .line 201
    :catch_22a
    move-exception v3

    goto :goto_1d1

    .line 191
    .end local v16           #e:Ljava/lang/OutOfMemoryError;
    :catchall_22c
    move-exception v3

    const-string v5, "EsPhotosData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_274

    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v14, v5, v24

    .line 193
    .restart local v14       #deltaTime:J
    const-string v5, "EsPhotosData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]; Done in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/16 v7, 0x3e8

    div-long v7, v14, v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/16 v7, 0x3e8

    rem-long v7, v14, v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " seconds"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    .end local v14           #deltaTime:J
    :cond_274
    :try_start_274
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_277
    .catch Ljava/io/IOException; {:try_start_274 .. :try_end_277} :catch_284

    .line 200
    :goto_277
    throw v3

    .restart local v11       #array:[B
    .restart local v13       #bytesRead:I
    .restart local v17       #imageArray:[B
    .restart local v20       #is:Ljava/io/InputStream;
    :catch_278
    move-exception v3

    goto/16 :goto_135

    .end local v13           #bytesRead:I
    .end local v17           #imageArray:[B
    :catch_27b
    move-exception v5

    goto/16 :goto_70

    .end local v11           #array:[B
    .end local v20           #is:Ljava/io/InputStream;
    .restart local v2       #op:Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;
    .restart local v4       #account:Lcom/google/android/apps/plus/content/EsAccount;
    .local v16, e:Ljava/io/IOException;
    :catch_27e
    move-exception v3

    goto/16 :goto_eb

    .line 201
    .end local v2           #op:Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;
    .end local v4           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :catch_281
    move-exception v3

    goto/16 :goto_1d1

    .end local v16           #e:Ljava/io/IOException;
    :catch_284
    move-exception v5

    goto :goto_277
.end method

.method public static loadRemoteImageBytes(Landroid/content/Context;Lcom/google/android/apps/plus/content/RemoteImageRequest;)[B
    .registers 13
    .parameter "context"
    .parameter "request"

    .prologue
    .line 210
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getPhotoId()J

    move-result-wide v9

    .line 211
    .local v9, photoId:J
    const-wide/16 v0, 0x0

    cmp-long v0, v9, v0

    if-eqz v0, :cond_21

    .line 212
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getPhotoId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getHeight()I

    move-result v5

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPhotosData;->loadPicasaImageBytes(Landroid/content/Context;JLjava/lang/String;III)[B

    move-result-object v0

    .line 219
    :goto_20
    return-object v0

    .line 215
    :cond_21
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    .line 216
    .local v7, account:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v8, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, p0, v7, v0}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 218
    .local v8, op:Lcom/google/android/apps/plus/api/DownloadPhotoOperation;
    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->start()V

    .line 219
    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->getBytes()[B

    move-result-object v0

    goto :goto_20
.end method

.method public static syncPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 3
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    .line 450
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->syncTopLevel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    .line 451
    return-void
.end method

.method static updateCommentCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .registers 15
    .parameter "db"
    .parameter "photoId"
    .parameter "delta"

    .prologue
    const-wide/16 v10, 0x0

    .line 463
    if-nez p2, :cond_5

    .line 496
    :cond_4
    :goto_4
    return-void

    .line 468
    :cond_5
    const/4 v8, 0x1

    new-array v7, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    .line 472
    .local v7, whereArgs:[Ljava/lang/String;
    :try_start_b
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 473
    .local v5, query:Ljava/lang/StringBuilder;
    const-string v8, "SELECT comment_count"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " FROM photo"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " WHERE photo_id=?"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v7}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_28
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_b .. :try_end_28} :catch_4a

    move-result-wide v0

    .line 486
    .end local v5           #query:Ljava/lang/StringBuilder;
    .local v0, commentCount:J
    :goto_29
    cmp-long v8, v0, v10

    if-ltz v8, :cond_4

    .line 487
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 488
    .local v6, values:Landroid/content/ContentValues;
    int-to-long v8, p2

    add-long v3, v0, v8

    .line 491
    .local v3, finalCount:J
    invoke-static {v3, v4, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    .line 493
    const-string v8, "comment_count"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 494
    const-string v8, "photo"

    const-string v9, "photo_id=?"

    invoke-virtual {p0, v8, v6, v9, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_4

    .line 481
    .end local v0           #commentCount:J
    .end local v3           #finalCount:J
    .end local v6           #values:Landroid/content/ContentValues;
    :catch_4a
    move-exception v2

    .line 483
    .local v2, e:Landroid/database/sqlite/SQLiteDoneException;
    const-wide/16 v0, -0x1

    .restart local v0       #commentCount:J
    goto :goto_29
.end method
