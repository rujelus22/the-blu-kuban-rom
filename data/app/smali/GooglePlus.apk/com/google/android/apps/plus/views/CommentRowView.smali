.class public Lcom/google/android/apps/plus/views/CommentRowView;
.super Landroid/view/View;
.source "CommentRowView.java"

# interfaces
.implements Landroid/widget/Checkable;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;
    }
.end annotation


# static fields
.field private static sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

.field private static sCommentAuthorBitmapDimension:I

.field private static sCommentBackgroundFadePaint:Landroid/graphics/Paint;

.field private static sCommentBackgroundPaint:Landroid/graphics/Paint;

.field private static sCommentBottomMargin:I

.field private static sCommentLeftMargin:I

.field private static sCommentRightMargin:I

.field private static sCommentSeparatorHeight:I

.field private static sCommentTopMargin:I

.field private static sContentPaint:Landroid/text/TextPaint;

.field private static sFlaggedCommentFadeArea:Landroid/graphics/Rect;

.field private static sFontSpacing:F

.field private static sIconRightMargin:I

.field private static sInitialized:Z

.field private static sTimePaint:Landroid/text/TextPaint;

.field private static sTimeYOffset:I

.field private static sUserImageBitmap:Landroid/graphics/Bitmap;


# instance fields
.field private mAuthorId:Ljava/lang/String;

.field private mAuthorName:Ljava/lang/String;

.field private final mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private mChecked:Z

.field private mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private final mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCommentContent:Ljava/lang/String;

.field private mCommentId:Ljava/lang/String;

.field private mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mContentSpan:Landroid/text/Spanned;

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mIsFlagged:Z

.field private mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

.field private mPlusOneCommentClickHandler:Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;

.field private mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

.field private mPosition:I

.field private mRelativeTime:Ljava/lang/String;

.field private mShowPlusOneView:Z

.field private mTimeLayout:Landroid/text/StaticLayout;

.field private mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CommentRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v5, 0x7f0d0028

    const v4, 0x7f0d0027

    const/4 v3, 0x1

    .line 131
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 97
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    .line 115
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mShowPlusOneView:Z

    .line 133
    sget-boolean v1, Lcom/google/android/apps/plus/views/CommentRowView;->sInitialized:Z

    if-nez v1, :cond_113

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 136
    .local v0, res:Landroid/content/res/Resources;
    invoke-static {p1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sUserImageBitmap:Landroid/graphics/Bitmap;

    .line 137
    const v1, 0x7f02017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

    .line 140
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 141
    sput-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sTimePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 142
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sTimePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a0015

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 143
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sTimePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 144
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sTimePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 146
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 147
    sput-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 148
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a0014

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 149
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 150
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a0016

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 151
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 153
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 154
    sput-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a0017

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 157
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 158
    sput-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBackgroundFadePaint:Landroid/graphics/Paint;

    const v2, 0x7f0a0018

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 159
    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBackgroundFadePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 161
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    .line 164
    const v1, 0x7f0d0023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentTopMargin:I

    .line 165
    const v1, 0x7f0d0026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBottomMargin:I

    .line 166
    const v1, 0x7f0d0024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentLeftMargin:I

    .line 167
    const v1, 0x7f0d0025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentRightMargin:I

    .line 168
    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sIconRightMargin:I

    .line 169
    const v1, 0x7f0d0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentAuthorBitmapDimension:I

    .line 171
    const v1, 0x7f0d0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentSeparatorHeight:I

    .line 173
    const v1, 0x7f0d0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sTimeYOffset:I

    .line 174
    const v1, 0x7f0d002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CommentRowView;->sFontSpacing:F

    .line 176
    sput-boolean v3, Lcom/google/android/apps/plus/views/CommentRowView;->sInitialized:Z

    .line 179
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_113
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 180
    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 296
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPosition:I

    .line 297
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 298
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 299
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mTimeLayout:Landroid/text/StaticLayout;

    .line 300
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 301
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 302
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentSpan:Landroid/text/Spanned;

    .line 303
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    .line 304
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorId:Ljava/lang/String;

    .line 305
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCommentId:Ljava/lang/String;

    .line 306
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCommentContent:Ljava/lang/String;

    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mShowPlusOneView:Z

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 310
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 527
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 528
    .local v2, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 529
    .local v3, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_66

    :pswitch_14
    move v4, v5

    .line 564
    :goto_15
    return v4

    .line 531
    :pswitch_16
    iget-object v6, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_34

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 532
    .local v1, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 533
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 534
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->invalidate()V

    goto :goto_15

    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_34
    move v4, v5

    .line 538
    goto :goto_15

    .line 542
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_36
    iput-object v7, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 544
    iget-object v6, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 545
    .restart local v1       #item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_3e

    .line 548
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_4e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->invalidate()V

    move v4, v5

    .line 549
    goto :goto_15

    .line 553
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_53
    iget-object v6, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_63

    .line 554
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 555
    iput-object v7, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 556
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->invalidate()V

    goto :goto_15

    :cond_63
    move v4, v5

    .line 559
    goto :goto_15

    .line 529
    nop

    :pswitch_data_66
    .packed-switch 0x0
        :pswitch_16
        :pswitch_36
        :pswitch_14
        :pswitch_53
    .end packed-switch
.end method

.method public final getAuthorId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCommentContent()Ljava/lang/String;
    .registers 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCommentContent:Ljava/lang/String;

    return-object v0
.end method

.method public final getCommentId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCommentId:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .registers 2

    .prologue
    .line 584
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mChecked:Z

    return v0
.end method

.method public final isFlagged()Z
    .registers 2

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mIsFlagged:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 314
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 316
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_9

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->onAvatarChanged(Ljava/lang/String;)V

    .line 329
    :cond_9
    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .registers 4
    .parameter "button"

    .prologue
    .line 597
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v1, :cond_1f

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCommentClickHandler:Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;

    if-eqz v1, :cond_1f

    .line 599
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v0, v1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    .line 600
    .local v0, plusOneId:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 601
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCommentClickHandler:Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 605
    .end local v0           #plusOneId:Ljava/lang/String;
    :cond_1f
    return-void
.end method

.method public final onClickableImageButtonClick(Lcom/google/android/apps/plus/views/ClickableImageButton;)V
    .registers 3
    .parameter "button"

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    if-ne p1, v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCommentClickHandler:Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;

    if-eqz v0, :cond_e

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCommentClickHandler:Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCommentId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    .line 612
    :cond_e
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 320
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 322
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 14
    .parameter "canvas"

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 478
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 480
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mChecked:Z

    if-eqz v0, :cond_b3

    .line 481
    sget-object v0, Lcom/google/android/apps/plus/views/CommentRowView;->sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 482
    sget-object v0, Lcom/google/android/apps/plus/views/CommentRowView;->sCheckedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 488
    :goto_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_cb

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 490
    .local v6, bitmapToDraw:Landroid/graphics/Bitmap;
    :goto_2a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v6, v11, v0, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->drawSelectionRect(Landroid/graphics/Canvas;)V

    .line 496
    :cond_40
    sget v10, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentTopMargin:I

    .line 497
    .local v10, yStart:I
    sget v0, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentLeftMargin:I

    sget v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentAuthorBitmapDimension:I

    add-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/CommentRowView;->sIconRightMargin:I

    add-int v9, v0, v1

    .line 500
    .local v9, xStart:I
    int-to-float v0, v9

    int-to-float v1, v10

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 502
    neg-int v0, v9

    int-to-float v0, v0

    neg-int v1, v10

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/CommentRowView;->sTimeYOffset:I

    add-int/2addr v0, v1

    add-int/2addr v10, v0

    .line 505
    int-to-float v0, v9

    int-to-float v1, v10

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 507
    neg-int v0, v9

    int-to-float v0, v0

    neg-int v1, v10

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v10, v0

    .line 510
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mShowPlusOneView:Z

    if-eqz v0, :cond_8c

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableImageButton;->draw(Landroid/graphics/Canvas;)V

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    .line 516
    :cond_8c
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mIsFlagged:Z

    if-eqz v0, :cond_b2

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v8

    .line 518
    .local v8, userImageRect:Landroid/graphics/Rect;
    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 519
    .local v7, bottom:I
    sget-object v0, Lcom/google/android/apps/plus/views/CommentRowView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    iget v1, v8, Landroid/graphics/Rect;->left:I

    iget v2, v8, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 521
    sget-object v0, Lcom/google/android/apps/plus/views/CommentRowView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBackgroundFadePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 523
    .end local v7           #bottom:I
    .end local v8           #userImageRect:Landroid/graphics/Rect;
    :cond_b2
    return-void

    .line 484
    .end local v6           #bitmapToDraw:Landroid/graphics/Bitmap;
    .end local v9           #xStart:I
    .end local v10           #yStart:I
    :cond_b3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sget v2, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentSeparatorHeight:I

    sub-int/2addr v0, v2

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1c

    .line 488
    :cond_cb
    sget-object v6, Lcom/google/android/apps/plus/views/CommentRowView;->sUserImageBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_2a
.end method

.method protected onMeasure(II)V
    .registers 22
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 333
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    sparse-switch v1, :sswitch_data_1e6

    const/4 v13, 0x0

    .line 334
    .local v13, width:I
    :sswitch_c
    if-gtz v13, :cond_15

    const/4 v1, 0x0

    :goto_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v1}, Lcom/google/android/apps/plus/views/CommentRowView;->setMeasuredDimension(II)V

    .line 335
    return-void

    .line 334
    :cond_15
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v15

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CommentRowView;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CommentRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x4000

    if-ne v15, v2, :cond_39

    move v1, v14

    :cond_2f
    :goto_2f
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CommentRowView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_f

    :cond_39
    sget v10, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentTopMargin:I

    sget v2, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentLeftMargin:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    sget v4, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentAuthorBitmapDimension:I

    add-int/2addr v4, v2

    sget v5, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentAuthorBitmapDimension:I

    add-int/2addr v5, v10

    invoke-virtual {v3, v2, v10, v4, v5}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    sget v3, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentAuthorBitmapDimension:I

    sget v4, Lcom/google/android/apps/plus/views/CommentRowView;->sIconRightMargin:I

    add-int/2addr v3, v4

    add-int v18, v2, v3

    sget v2, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentLeftMargin:I

    sub-int v2, v13, v2

    sget v3, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentRightMargin:I

    sub-int/2addr v2, v3

    sget v3, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentAuthorBitmapDimension:I

    sub-int/2addr v2, v3

    sget v3, Lcom/google/android/apps/plus/views/CommentRowView;->sIconRightMargin:I

    sub-int v4, v2, v3

    new-instance v2, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorName:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentSpan:Landroid/text/Spanned;

    if-eqz v3, :cond_7a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentSpan:Landroid/text/Spanned;

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_7a
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const v5, 0x7f0a0013

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v3, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v2, v3, v1, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v2, v1, v3, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/CommentRowView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/CommentRowView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v18

    invoke-virtual {v1, v0, v10}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const v1, 0x7f080117

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorName:Ljava/lang/String;

    aput-object v5, v2, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentSpan:Landroid/text/Spanned;

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentAuthorBitmapDimension:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int v11, v10, v1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mShowPlusOneView:Z

    if-eqz v1, :cond_16b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    const/4 v12, 0x0

    move-object/from16 v5, v17

    move-object/from16 v6, p0

    move-object/from16 v7, p0

    move/from16 v9, v18

    move v10, v4

    invoke-static/range {v5 .. v12}, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->setupButtons(Landroid/content/Context;Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;Lcom/google/api/services/plusi/model/DataPlusOne;IIIZ)Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_16b
    new-instance v1, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mRelativeTime:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/views/CommentRowView;->sTimePaint:Landroid/text/TextPaint;

    sget-object v4, Lcom/google/android/apps/plus/views/CommentRowView;->sTimePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mRelativeTime:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/CommentRowView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mTimeLayout:Landroid/text/StaticLayout;

    sget v1, Lcom/google/android/apps/plus/views/CommentRowView;->sTimeYOffset:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v11

    const v2, 0x7f080118

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mRelativeTime:Ljava/lang/String;

    aput-object v5, v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mShowPlusOneView:Z

    if-eqz v2, :cond_1d8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableImageButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_1d8
    sget v2, Lcom/google/android/apps/plus/views/CommentRowView;->sCommentBottomMargin:I

    add-int/2addr v1, v2

    const/high16 v2, -0x8000

    if-ne v15, v2, :cond_2f

    invoke-static {v1, v14}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto/16 :goto_2f

    .line 333
    nop

    :sswitch_data_1e6
    .sparse-switch
        -0x80000000 -> :sswitch_c
        0x40000000 -> :sswitch_c
    .end sparse-switch
.end method

.method public setAuthor(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "authorGaiaId"
    .parameter "authorName"

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorId:Ljava/lang/String;

    .line 221
    iput-object p2, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mAuthorName:Ljava/lang/String;

    .line 223
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableUserImage;

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mUserImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 225
    return-void
.end method

.method public setChecked(Z)V
    .registers 3
    .parameter "checked"

    .prologue
    .line 576
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mChecked:Z

    if-eq p1, v0, :cond_9

    .line 577
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mChecked:Z

    .line 578
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CommentRowView;->invalidate()V

    .line 580
    :cond_9
    return-void
.end method

.method public setClickListener(Lcom/google/android/apps/plus/views/ItemClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 187
    return-void
.end method

.method public setCommentId(Ljava/lang/String;)V
    .registers 2
    .parameter "commentId"

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCommentId:Ljava/lang/String;

    .line 275
    return-void
.end method

.method public setContent(Ljava/lang/String;Z)V
    .registers 5
    .parameter "contentHtml"
    .parameter "truncated"

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mCommentContent:Ljava/lang/String;

    .line 240
    if-eqz p1, :cond_25

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_25

    .line 241
    if-eqz p2, :cond_1f

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 245
    :cond_1f
    invoke-static {p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mContentSpan:Landroid/text/Spanned;

    .line 247
    :cond_25
    return-void
.end method

.method public setIsFlagged(Z)V
    .registers 2
    .parameter "isFlagged"

    .prologue
    .line 285
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mIsFlagged:Z

    .line 286
    return-void
.end method

.method public setPlusOneCommentClickHandler(Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;)V
    .registers 2
    .parameter "plusOneCommentClickHandler"

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneCommentClickHandler:Lcom/google/android/apps/plus/views/CommentRowView$PlusOneCommentClickHandler;

    .line 195
    return-void
.end method

.method public setPlusOneData(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .registers 2
    .parameter "plusOneData"

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    .line 268
    return-void
.end method

.method public setPosition(I)V
    .registers 2
    .parameter "position"

    .prologue
    .line 203
    iput p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mPosition:I

    .line 204
    return-void
.end method

.method public setShowPlusOneView(Z)V
    .registers 2
    .parameter "showPlusOneView"

    .prologue
    .line 592
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mShowPlusOneView:Z

    .line 593
    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .registers 2
    .parameter "time"

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mRelativeTime:Ljava/lang/String;

    .line 261
    return-void
.end method

.method public toggle()V
    .registers 2

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CommentRowView;->mChecked:Z

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CommentRowView;->setChecked(Z)V

    .line 572
    return-void

    .line 571
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method
