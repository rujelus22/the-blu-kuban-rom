.class public Lcom/google/android/apps/plus/fragments/HostedEventListFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedEventListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/ItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;,
        Lcom/google/android/apps/plus/fragments/HostedEventListFragment$Query;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/views/ItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

.field private mCurrentMode:I

.field private mCurrentSpinnerPosition:I

.field private mDataPresent:Z

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mInitialLoadDone:Z

.field private final mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mRefreshNeeded:Z

.field private mSpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    .line 140
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    .line 146
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/HostedEventListFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    return v0
.end method

.method private fetchData()V
    .registers 4

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 375
    .local v0, context:Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v1, :cond_2b

    if-eqz v0, :cond_2b

    .line 376
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-nez v1, :cond_1c

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080096

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    .line 380
    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    .line 381
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->getEventHome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 383
    :cond_2b
    return-void
.end method

.method private setCreationVisibility(I)V
    .registers 4
    .parameter "visibility"

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09010b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09010c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 291
    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 434
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EventCardAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final onActionButtonClicked(I)V
    .registers 5
    .parameter "actionId"

    .prologue
    .line 439
    packed-switch p1, :pswitch_data_16

    .line 446
    :goto_3
    return-void

    .line 441
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getCreateEventActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    .line 443
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 439
    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 10
    .parameter "view"

    .prologue
    .line 391
    instance-of v5, p1, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    if-eqz v5, :cond_20

    move-object v0, p1

    .line 392
    check-cast v0, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    .line 393
    .local v0, eventCardView:Lcom/google/android/apps/plus/views/EventDestinationCardView;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getEvent()Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v1

    .line 395
    .local v1, eventData:Lcom/google/api/services/plusi/model/PlusEvent;
    if-eqz v1, :cond_1f

    .line 396
    iget-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    .line 397
    .local v2, eventId:Ljava/lang/String;
    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    .line 398
    .local v4, ownerId:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v7, 0x0

    invoke-static {v5, v6, v2, v4, v7}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    .line 406
    .end local v0           #eventCardView:Lcom/google/android/apps/plus/views/EventDestinationCardView;
    .end local v1           #eventData:Lcom/google/api/services/plusi/model/PlusEvent;
    .end local v2           #eventId:Ljava/lang/String;
    .end local v4           #ownerId:Ljava/lang/String;
    :cond_1f
    :goto_1f
    return-void

    .line 401
    :cond_20
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f09010b

    if-ne v5, v6, :cond_1f

    .line 402
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getCreateEventActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v3

    .line 404
    .local v3, intent:Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1f
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x0

    .line 191
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 193
    if-eqz p1, :cond_29

    .line 195
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    const-string v1, "events_refresh"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    .line 196
    const-string v0, "events_initialload"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mInitialLoadDone:Z

    .line 197
    const-string v0, "events_currentmode"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    .line 198
    const-string v0, "events_datapresent"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    .line 201
    :cond_29
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 202
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 7
    .parameter "arg0"
    .parameter "arg1"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 13
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 247
    const v0, 0x7f03004f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 249
    .local v8, view:Landroid/view/View;
    const v0, 0x7f090109

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 250
    new-instance v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EventCardAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/ColumnGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 252
    const v0, 0x7f0800de

    invoke-static {v8, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 254
    const v0, 0x7f09010b

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 255
    .local v7, createButton:Landroid/widget/Button;
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setClickable(Z)V

    .line 256
    invoke-virtual {v7, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    return-object v8
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 8
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/EventCardAdapter;->changeCursor(Landroid/database/Cursor;)V

    if-eqz p2, :cond_71

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_71

    move v0, v1

    :goto_12
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-eqz v0, :cond_39

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v0

    if-eqz v0, :cond_73

    move v0, v1

    :goto_37
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentSpinnerPosition:I

    :cond_39
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-nez v0, :cond_75

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-nez v0, :cond_75

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    if-nez v0, :cond_75

    move v3, v1

    :goto_46
    if-eqz v3, :cond_77

    move v0, v2

    :goto_49
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setCreationVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-eqz v0, :cond_7a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showContent(Landroid/view/View;)V

    :goto_57
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-nez v0, :cond_6d

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mInitialLoadDone:Z

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;

    if-eqz v0, :cond_6d

    check-cast p1, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;

    .end local p1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->getCurrentMode()I

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-eqz v1, :cond_9f

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    :cond_6d
    :goto_6d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->invalidateActionBar()V

    return-void

    .restart local p1
    :cond_71
    move v0, v2

    goto :goto_12

    :cond_73
    move v0, v2

    goto :goto_37

    :cond_75
    move v3, v2

    goto :goto_46

    :cond_77
    const/16 v0, 0x8

    goto :goto_49

    :cond_7a
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-eqz v0, :cond_8d

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f080096

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_57

    :cond_8d
    if-eqz v3, :cond_97

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showContent(Landroid/view/View;)V

    goto :goto_57

    :cond_97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_57

    .end local p1
    :cond_9f
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    if-eq v0, v1, :cond_6d

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_6d
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, arg0:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 6
    .parameter "item"

    .prologue
    .line 453
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_2c

    .line 464
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    :goto_b
    return v2

    .line 456
    :pswitch_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080416

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 458
    .local v1, helpUrlParam:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 459
    .local v0, helpUrl:Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startExternalActivity(Landroid/content/Intent;)V

    .line 460
    const/4 v2, 0x1

    goto :goto_b

    .line 453
    nop

    :pswitch_data_2c
    .packed-switch 0x7f090290
        :pswitch_c
    .end packed-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 264
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    .line 265
    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 8
    .parameter "actionBar"

    .prologue
    .line 206
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0300bf

    invoke-direct {v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 208
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v4, 0x1090009

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 212
    .local v1, items:[Ljava/lang/CharSequence;
    const/4 v0, 0x0

    .local v0, i:I
    array-length v2, v1

    .local v2, size:I
    :goto_23
    if-ge v0, v2, :cond_33

    .line 213
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_23

    .line 218
    :cond_33
    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    if-nez v3, :cond_3b

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-eqz v3, :cond_42

    .line 219
    :cond_3b
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    .line 222
    :cond_42
    const/4 v3, 0x0

    const v4, 0x7f020159

    const v5, 0x7f08010b

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    .line 225
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    .line 227
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v3, :cond_56

    .line 228
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    .line 230
    :cond_56
    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .registers 5
    .parameter "position"

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentSpinnerPosition:I

    if-eq v0, p1, :cond_1b

    .line 235
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentSpinnerPosition:I

    .line 236
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentSpinnerPosition:I

    if-nez v0, :cond_1c

    const/4 v0, 0x2

    :goto_b
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionToTop()V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 242
    :cond_1b
    return-void

    .line 236
    :cond_1c
    const/4 v0, 0x1

    goto :goto_b
.end method

.method public final onResume()V
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 270
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    .line 272
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-eqz v0, :cond_f

    .line 273
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->fetchData()V

    .line 275
    :cond_f
    return-void
.end method

.method protected final onResumeContentFetched(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    .prologue
    .line 279
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResumeContentFetched(Landroid/view/View;)V

    .line 280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    .line 281
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 176
    const-string v0, "events_refresh"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    const-string v0, "events_initialload"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mInitialLoadDone:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 178
    const-string v0, "events_currentmode"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mCurrentMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    const-string v0, "events_datapresent"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 180
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 181
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 4
    .parameter "args"

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 186
    const-string v0, "refresh"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    .line 187
    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .registers 2
    .parameter "span"

    .prologue
    .line 421
    return-void
.end method

.method public final onUserImageClick(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "gaiaId"
    .parameter "authorName"

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    .line 412
    return-void
.end method

.method public final refresh()V
    .registers 2

    .prologue
    .line 295
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    .line 296
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setCreationVisibility(I)V

    .line 297
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->fetchData()V

    .line 298
    return-void
.end method

.method protected final showContent(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 362
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 364
    return-void
.end method

.method protected final showEmptyView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 368
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyView(Landroid/view/View;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 370
    return-void
.end method
