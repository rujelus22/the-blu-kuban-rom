.class public final Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "SetCircleMembershipOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;",
        "Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCirclesToAdd:[Ljava/lang/String;

.field private final mCirclesToRemove:[Ljava/lang/String;

.field private final mFireAndForget:Z

.field private final mPersonId:Ljava/lang/String;

.field private final mPersonName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 18
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "personName"
    .parameter "circlesToAdd"
    .parameter "circlesToRemove"
    .parameter "fireAndForget"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 59
    const-string v3, "modifymemberships"

    invoke-static {}, Lcom/google/api/services/plusi/model/ModifyMembershipsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ModifyMembershipsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/ModifyMembershipsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ModifyMembershipsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 63
    iput-object p3, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    .line 65
    iput-object p5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    .line 67
    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mFireAndForget:Z

    .line 68
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 8
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;

    .end local p1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mFireAndForget:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->setCircleMembershipResult(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_12
    const/4 v3, 0x0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;->circlePerson:Ljava/util/List;

    if-eqz v0, :cond_29

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;->circlePerson:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_29

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsResponse;->circlePerson:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataCirclePerson;

    move-object v3, v0

    :cond_29
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;[Ljava/lang/String;[Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mFireAndForget:Z

    if-eqz v0, :cond_45

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->showToastIfNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_45
    return-void
.end method

.method public final onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    .registers 11
    .parameter "inputStream"
    .parameter "contentType"
    .parameter "contentLength"
    .parameter "header"
    .parameter "statusCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->setCircleMembershipResult(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 131
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/plus/network/PlusiOperation;->onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V

    .line 133
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 8
    .parameter "x0"

    .prologue
    const/4 v0, 0x0

    .line 32
    check-cast p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;

    .end local p1
    new-instance v1, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;-><init>()V

    iput-object v1, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleMembershipModificationParams:Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleMembershipModificationParams:Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;->person:Ljava/util/List;

    new-instance v1, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleMemberId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleMemberId;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mPersonName:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataCircleMemberToAdd;->displayName:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleMembershipModificationParams:Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;->person:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    if-eqz v1, :cond_4f

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4f

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToAdd:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToAdd:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_3f
    if-ge v1, v3, :cond_4f

    aget-object v4, v2, v1

    iget-object v5, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToAdd:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->buildCircleId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleId;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3f

    :cond_4f
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    if-eqz v1, :cond_72

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_72

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToRemove:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->mCirclesToRemove:[Ljava/lang/String;

    array-length v2, v1

    :goto_62
    if-ge v0, v2, :cond_72

    aget-object v3, v1, v0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/ModifyMembershipsRequest;->circleToRemove:Ljava/util/List;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->buildCircleId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleId;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_62

    :cond_72
    return-void
.end method
