.class public Lcom/google/android/apps/plus/views/HostLayout;
.super Landroid/widget/FrameLayout;
.source "HostLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
.implements Lcom/google/android/apps/plus/views/SlidingPanelLayout$OnSlidingPanelStateChange;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;
    }
.end annotation


# instance fields
.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

.field private mNavigationBar:Landroid/view/View;

.field private mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 64
    return-void
.end method


# virtual methods
.method public final attachActionBar()V
    .registers 3

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 250
    .local v0, currentFragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_15

    .line 251
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/HostedFragment;->attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    .line 255
    :cond_15
    return-void
.end method

.method public final getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    return-object v0
.end method

.method public final getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;
    .registers 3

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v1, "hosted"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/HostedFragment;

    return-object v0
.end method

.method public final getNavigationBar()Landroid/view/View;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    return-object v0
.end method

.method public final hideNavigationBar()V
    .registers 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->isOpen()Z

    move-result v0

    if-nez v0, :cond_9

    .line 178
    :goto_8
    return-void

    .line 173
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v0, :cond_13

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNavigationBarVisibilityChange(Z)V

    .line 177
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->close()V

    goto :goto_8
.end method

.method public final isNavigationBarVisible()Z
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->isOpen()Z

    move-result v0

    return v0
.end method

.method public final onActionBarInvalidated()V
    .registers 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->attachActionBar()V

    .line 263
    return-void
.end method

.method public final onActionButtonClicked(I)V
    .registers 3
    .parameter "actionId"

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 293
    .local v0, currentFragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_9

    .line 294
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onActionButtonClicked(I)V

    .line 296
    :cond_9
    return-void
.end method

.method public final onAttachFragment(Lcom/google/android/apps/plus/phone/HostedFragment;)V
    .registers 3
    .parameter "fragment"

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    .line 243
    return-void
.end method

.method protected onFinishInflate()V
    .registers 2

    .prologue
    .line 87
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 89
    const v0, 0x7f0900e8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/HostActionBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V

    .line 92
    const v0, 0x7f090106

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    .line 93
    const v0, 0x7f090107

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setOnSlidingPanelStateChange(Lcom/google/android/apps/plus/views/SlidingPanelLayout$OnSlidingPanelStateChange;)V

    .line 95
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 10
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 117
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_2c

    move v0, v1

    :goto_f
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2b

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 122
    :cond_2b
    return-void

    .line 117
    :cond_2c
    const/16 v0, 0x8

    goto :goto_f
.end method

.method protected onMeasure(II)V
    .registers 8
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v4, 0x4000

    .line 99
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getMeasuredHeight()I

    move-result v0

    .line 103
    .local v0, height:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_24

    .line 104
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->getNavigationBarWidth()I

    move-result v1

    .line 105
    .local v1, navigationBarWidth:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 109
    .end local v1           #navigationBarWidth:I
    :cond_24
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 316
    .local v0, fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_b

    .line 317
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    .line 320
    :goto_a
    return v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public final onPanelClosed()V
    .registers 3

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v0, :cond_11

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNavigationBarVisibilityChange(Z)V

    .line 362
    :cond_11
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 305
    .local v0, fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_9

    .line 306
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 308
    :cond_9
    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .registers 3
    .parameter "selectedPosition"

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 271
    .local v0, currentFragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_9

    .line 272
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrimarySpinnerSelectionChange(I)V

    .line 274
    :cond_9
    return-void
.end method

.method public final onRefreshButtonClicked()V
    .registers 2

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 282
    .local v0, currentFragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_9

    .line 283
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    .line 285
    :cond_9
    return-void
.end method

.method public final saveHostedFragmentState()Landroid/support/v4/app/Fragment$SavedState;
    .registers 3

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 338
    .local v0, fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_d

    .line 339
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->saveFragmentInstanceState(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v1

    .line 341
    :goto_c
    return-object v1

    :cond_d
    const/4 v1, 0x0

    goto :goto_c
.end method

.method public setListener(Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    .line 80
    return-void
.end method

.method public final showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V
    .registers 12
    .parameter "fragment"
    .parameter "animated"
    .parameter "savedState"

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    .line 203
    .local v0, currentFragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    if-eqz v0, :cond_45

    .line 204
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    .line 205
    .local v3, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v4

    .line 206
    .local v4, startViewExtras:Landroid/os/Bundle;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->detachActionBar()V

    .line 211
    :goto_11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 213
    .local v1, startTime:J
    iget-object v6, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    .line 215
    if-eqz p3, :cond_1f

    .line 216
    invoke-virtual {p1, p3}, Lcom/google/android/apps/plus/phone/HostedFragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 218
    :cond_1f
    iget-object v6, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    .line 219
    .local v5, transaction:Landroid/support/v4/app/FragmentTransaction;
    const v6, 0x7f0900a6

    const-string v7, "hosted"

    invoke-virtual {v5, v6, p1, v7}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 220
    if-eqz p2, :cond_48

    .line 221
    const/16 v6, 0x1003

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 225
    :goto_34
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    .line 228
    iget-object v6, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 229
    if-nez v3, :cond_4d

    .line 230
    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->recordNavigationAction()V

    .line 234
    :goto_44
    return-void

    .line 208
    .end local v1           #startTime:J
    .end local v3           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    .end local v4           #startViewExtras:Landroid/os/Bundle;
    .end local v5           #transaction:Landroid/support/v4/app/FragmentTransaction;
    :cond_45
    const/4 v3, 0x0

    .line 209
    .restart local v3       #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    const/4 v4, 0x0

    .restart local v4       #startViewExtras:Landroid/os/Bundle;
    goto :goto_11

    .line 223
    .restart local v1       #startTime:J
    .restart local v5       #transaction:Landroid/support/v4/app/FragmentTransaction;
    :cond_48
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_34

    .line 232
    :cond_4d
    invoke-virtual {p1, v3, v1, v2, v4}, Lcom/google/android/apps/plus/phone/HostedFragment;->recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;JLandroid/os/Bundle;)V

    goto :goto_44
.end method

.method public final showNavigationBar()V
    .registers 5

    .prologue
    .line 143
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 163
    :goto_8
    return-void

    .line 147
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v2, :cond_13

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNavigationBarVisibilityChange(Z)V

    .line 151
    :cond_13
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostActionBar;->dismissPopupMenus()V

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostActionBar;->getRootView()Landroid/view/View;

    move-result-object v1

    .line 154
    .local v1, rootView:Landroid/view/View;
    if-eqz v1, :cond_29

    .line 155
    invoke-virtual {v1}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 156
    .local v0, focus:Landroid/view/View;
    if-eqz v0, :cond_29

    .line 157
    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 161
    .end local v0           #focus:Landroid/view/View;
    :cond_29
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 162
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->open()V

    goto :goto_8
.end method

.method public final showNavigationBarDelayed()V
    .registers 4

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/apps/plus/views/HostLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostLayout$1;-><init>(Lcom/google/android/apps/plus/views/HostLayout;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 193
    return-void
.end method

.method public final toggleNavigationBarVisibility()V
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/SlidingPanelLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    .line 137
    :goto_b
    return-void

    .line 135
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->showNavigationBar()V

    goto :goto_b
.end method
