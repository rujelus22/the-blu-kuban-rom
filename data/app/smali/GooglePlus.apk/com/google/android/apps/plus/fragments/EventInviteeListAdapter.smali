.class public final Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;
.super Landroid/support/v4/widget/CursorAdapter;
.source "EventInviteeListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;
    }
.end annotation


# instance fields
.field protected mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mListener:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;

.field private mOwnerId:Ljava/lang/String;

.field private mViewerGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .registers 5
    .parameter "context"
    .parameter "circleNameResolver"

    .prologue
    .line 44
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 46
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 22
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 116
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    packed-switch v13, :pswitch_data_148

    .line 174
    :goto_a
    return-void

    .line 118
    :pswitch_b
    const/16 v13, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 119
    .local v10, rsvpType:Ljava/lang/String;
    const/16 v13, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_55

    const/4 v8, 0x1

    .line 120
    .local v8, past:Z
    :goto_1f
    const/16 v13, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 122
    .local v2, count:I
    sget-object v13, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5b

    .line 123
    if-eqz v8, :cond_57

    const v9, 0x7f080373

    .line 136
    .local v9, resId:I
    :goto_34
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v13, v9, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 137
    .local v11, text:Ljava/lang/String;
    const v13, 0x1020014

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 138
    .local v12, textView:Landroid/widget/TextView;
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 119
    .end local v2           #count:I
    .end local v8           #past:Z
    .end local v9           #resId:I
    .end local v11           #text:Ljava/lang/String;
    .end local v12           #textView:Landroid/widget/TextView;
    :cond_55
    const/4 v8, 0x0

    goto :goto_1f

    .line 123
    .restart local v2       #count:I
    .restart local v8       #past:Z
    :cond_57
    const v9, 0x7f080372

    goto :goto_34

    .line 125
    :cond_5b
    sget-object v13, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_67

    .line 126
    const v9, 0x7f080374

    .line 133
    .restart local v9       #resId:I
    :goto_66
    goto :goto_34

    .line 127
    .end local v9           #resId:I
    :cond_67
    sget-object v13, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_79

    .line 128
    if-eqz v8, :cond_75

    const v9, 0x7f080376

    .restart local v9       #resId:I
    :goto_74
    goto :goto_34

    .end local v9           #resId:I
    :cond_75
    const v9, 0x7f080375

    goto :goto_74

    .line 130
    :cond_79
    const-string v13, "REMOVED"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_85

    .line 131
    const v9, 0x7f080379

    goto :goto_66

    .line 133
    :cond_85
    if-eqz v8, :cond_8b

    const v9, 0x7f080378

    goto :goto_66

    :cond_8b
    const v9, 0x7f080377

    goto :goto_66

    .end local v2           #count:I
    .end local v8           #past:Z
    .end local v10           #rsvpType:Ljava/lang/String;
    :pswitch_8f
    move-object/from16 v5, p1

    .line 142
    check-cast v5, Lcom/google/android/apps/plus/views/PeopleListItemView;

    .line 143
    .local v5, item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    .line 144
    const/4 v13, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, gaiaId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    .line 146
    const/4 v13, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPersonId(Ljava/lang/String;)V

    .line 147
    invoke-virtual {v5, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setGaiaId(Ljava/lang/String;)V

    .line 148
    const/4 v13, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 149
    .local v6, name:Ljava/lang/String;
    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactName(Ljava/lang/String;)V

    .line 150
    const/4 v13, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, email:Ljava/lang/String;
    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setWellFormedEmail(Ljava/lang/String;)V

    .line 152
    const/4 v13, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 154
    .local v7, packedCircleIds:Ljava/lang/String;
    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIds(Ljava/lang/String;)V

    .line 155
    const v13, 0x7f080028

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setActionButtonLabel(I)V

    .line 156
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mViewerGaiaId:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_ef

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mViewerGaiaId:Ljava/lang/String;

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_f8

    .line 157
    :cond_ef
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setActionButtonVisible(Z)V

    .line 165
    :goto_f3
    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    goto/16 :goto_a

    .line 159
    :cond_f8
    const/4 v13, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    if-eqz v13, :cond_10f

    const/4 v1, 0x1

    .line 161
    .local v1, blacklisted:Z
    :goto_102
    if-eqz v1, :cond_111

    const v13, 0x7f080396

    :goto_107
    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setActionButtonLabel(I)V

    .line 163
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setActionButtonVisible(Z)V

    goto :goto_f3

    .line 159
    .end local v1           #blacklisted:Z
    :cond_10f
    const/4 v1, 0x0

    goto :goto_102

    .line 161
    .restart local v1       #blacklisted:Z
    :cond_111
    const v13, 0x7f080395

    goto :goto_107

    .line 169
    .end local v1           #blacklisted:Z
    .end local v3           #email:Ljava/lang/String;
    .end local v4           #gaiaId:Ljava/lang/String;
    .end local v5           #item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    .end local v6           #name:Ljava/lang/String;
    .end local v7           #packedCircleIds:Ljava/lang/String;
    :pswitch_115
    const/16 v13, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 170
    .restart local v2       #count:I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0e0006

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v2, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 172
    .restart local v11       #text:Ljava/lang/String;
    const v13, 0x1020014

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 173
    .restart local v12       #textView:Landroid/widget/TextView;
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 116
    nop

    :pswitch_data_148
    .packed-switch 0x0
        :pswitch_b
        :pswitch_8f
        :pswitch_115
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .registers 5
    .parameter "position"

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 74
    .local v0, cursor:Landroid/database/Cursor;
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_12

    .line 83
    const/4 v1, 0x2

    :goto_f
    :pswitch_f
    return v1

    .line 79
    :pswitch_10
    const/4 v1, 0x1

    goto :goto_f

    .line 74
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 65
    const/4 v0, 0x3

    return v0
.end method

.method public final isEnabled(I)Z
    .registers 4
    .parameter "position"

    .prologue
    const/4 v0, 0x1

    .line 181
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->getItemViewType(I)I

    move-result v1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_28

    .line 108
    const/4 v1, 0x0

    :goto_9
    return-object v1

    .line 95
    :pswitch_a
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 96
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x7f03002a

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_9

    .line 99
    .end local v0           #inflater:Landroid/view/LayoutInflater;
    :pswitch_16
    invoke-static {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;

    move-result-object v1

    goto :goto_9

    .line 102
    :pswitch_1b
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 103
    .restart local v0       #inflater:Landroid/view/LayoutInflater;
    const v1, 0x7f030029

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_9

    .line 93
    nop

    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_a
        :pswitch_16
        :pswitch_1b
    .end packed-switch
.end method

.method public final onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V
    .registers 12
    .parameter "view"
    .parameter "action"

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x5

    const/4 v4, 0x0

    const/4 v7, 0x3

    .line 189
    if-ne p2, v7, :cond_4f

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;

    if-eqz v2, :cond_4f

    .line 190
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, gaiaId:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getWellFormedEmail()Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, email:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_58

    :cond_1c
    invoke-interface {v6, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_62

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_26
    invoke-interface {v6, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_60

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_30
    if-eqz v2, :cond_38

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_40

    :cond_38
    if-eqz v5, :cond_52

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_52

    :cond_40
    const/4 v2, 0x7

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_50

    const/4 v2, 0x1

    :goto_48
    if-eqz v2, :cond_5a

    .line 194
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;->onReInviteInvitee(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .end local v0           #email:Ljava/lang/String;
    .end local v1           #gaiaId:Ljava/lang/String;
    :cond_4f
    :goto_4f
    return-void

    .restart local v0       #email:Ljava/lang/String;
    .restart local v1       #gaiaId:Ljava/lang/String;
    :cond_50
    move v2, v4

    .line 193
    goto :goto_48

    :cond_52
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_58
    move v2, v4

    goto :goto_48

    .line 196
    :cond_5a
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;->onRemoveInvitee(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4f

    :cond_60
    move-object v5, v3

    goto :goto_30

    :cond_62
    move-object v2, v3

    goto :goto_26
.end method

.method public final setEventOwnerId(Ljava/lang/String;)V
    .registers 2
    .parameter "ownerId"

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mOwnerId:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public final setOnActionListener(Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;

    .line 50
    return-void
.end method

.method public final setViewerGaiaId(Ljava/lang/String;)V
    .registers 2
    .parameter "gaiaId"

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->mViewerGaiaId:Ljava/lang/String;

    .line 58
    return-void
.end method
