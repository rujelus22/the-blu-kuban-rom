.class public Lcom/google/android/apps/plus/views/PlaceReviewCardView;
.super Lcom/google/android/apps/plus/views/StreamCardView;
.source "PlaceReviewCardView.java"


# static fields
.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sDividerYPadding:I

.field private static sLocationBitmap:Landroid/graphics/Bitmap;

.field private static sLocationIconPadding:I

.field private static sPlaceReviewCardInitialized:Z

.field private static sPostLocationYPadding:I


# instance fields
.field private mDividerY:F

.field private mLocationIconRect:Landroid/graphics/Rect;

.field private mLocationLayout:Landroid/text/StaticLayout;

.field private mLocationLayoutCorner:Landroid/graphics/Point;

.field private mReview:Lcom/google/api/services/plusi/model/PlaceReview;

.field private mReviewBodyLayout:Landroid/text/StaticLayout;

.field private mWrapContent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/StreamCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/high16 v1, -0x4080

    iput v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mDividerY:F

    .line 59
    sget-boolean v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sPlaceReviewCardInitialized:Z

    if-nez v1, :cond_53

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x7f020169

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sLocationBitmap:Landroid/graphics/Bitmap;

    .line 62
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sPlaceReviewCardInitialized:Z

    .line 64
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 65
    sput-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00d8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0d0160

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 70
    const v1, 0x7f0d015d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sPostLocationYPadding:I

    .line 72
    const v1, 0x7f0d015e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sLocationIconPadding:I

    .line 74
    const v1, 0x7f0d015f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDividerYPadding:I

    .line 78
    .end local v0           #resources:Landroid/content/res/Resources;
    :cond_53
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationIconRect:Landroid/graphics/Rect;

    .line 79
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    .line 80
    return-void
.end method


# virtual methods
.method protected final draw(Landroid/graphics/Canvas;IIII)I
    .registers 14
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v7, 0x0

    .line 224
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->drawAuthorImage$494937f0(Landroid/graphics/Canvas;)V

    .line 226
    sget v0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentXPadding:I

    add-int/2addr v0, v1

    add-int/2addr p2, v0

    .line 227
    sget v0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentXPadding:I

    add-int/2addr v0, v1

    sub-int/2addr p4, v0

    .line 229
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->drawAuthorName(Landroid/graphics/Canvas;II)I

    move-result p3

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2f

    .line 231
    add-int v0, p2, p4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sub-int v1, p3, v1

    sget v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sRelativeTimeYOffset:I

    sub-int/2addr v1, v2

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->drawRelativeTimeLayout(Landroid/graphics/Canvas;II)I

    .line 235
    :cond_2f
    sget v0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentYPadding:I

    add-int/2addr p3, v0

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mContentLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_51

    .line 238
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 240
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    .line 243
    :cond_51
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_70

    .line 244
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 246
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    .line 250
    :cond_70
    iget v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mDividerY:F

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_86

    .line 251
    int-to-float v1, p2

    iget v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mDividerY:F

    add-int v0, p2, p4

    int-to-float v3, v0

    iget v4, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mDividerY:F

    sget-object v5, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 254
    :cond_86
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_cf

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationIconRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 258
    .local v6, sectionBottom:I
    if-le v6, p5, :cond_a3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mWrapContent:Z

    if-eqz v0, :cond_cf

    .line 259
    :cond_a3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 262
    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sLocationBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v7, v1, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 263
    sget v0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sPostLocationYPadding:I

    add-int p3, v6, v0

    .line 267
    .end local v6           #sectionBottom:I
    :cond_cf
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReviewBodyLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_eb

    .line 268
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReviewBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 270
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReviewBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    sget v0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentYPadding:I

    .line 276
    :cond_eb
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->drawPlusOneBar(Landroid/graphics/Canvas;)V

    .line 277
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->drawWhatsHot(Landroid/graphics/Canvas;)V

    .line 278
    return p5
.end method

.method public final init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V
    .registers 12
    .parameter "cursor"
    .parameter "displaySizeType"
    .parameter "size"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewedListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"

    .prologue
    .line 93
    invoke-super/range {p0 .. p8}, Lcom/google/android/apps/plus/views/StreamCardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 102
    const/16 v2, 0x14

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 103
    .local v1, embedData:[B
    if-eqz v1, :cond_1b

    .line 104
    invoke-static {}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->getInstance()Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    .line 106
    .local v0, embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    if-eqz v0, :cond_1b

    .line 107
    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->placeReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    .line 110
    .end local v0           #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    :cond_1b
    return-void
.end method

.method protected final layoutElements(IIII)I
    .registers 20
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 148
    add-int v2, p2, p4

    move/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->createPlusOneBar(III)I

    .line 149
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int p4, p4, v2

    .line 150
    invoke-virtual/range {p0 .. p2}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->setAuthorImagePosition(II)V

    .line 152
    sget v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentXPadding:I

    add-int/2addr v2, v3

    add-int p1, p1, v2

    .line 153
    sget v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentXPadding:I

    add-int/2addr v2, v3

    sub-int p3, p3, v2

    .line 155
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->createAuthorNameAndRelativeTimeLayoutOnSameLine$4868d301(II)I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentYPadding:I

    add-int p2, v2, v3

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mContent:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10f

    .line 159
    sub-int v2, p4, p2

    sget-object v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int v13, v2, v3

    .line 161
    .local v13, maxLines:I
    if-lez v13, :cond_62

    .line 162
    sget-object v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mContent:Ljava/lang/CharSequence;

    move/from16 v0, p3

    invoke-static {v2, v3, v0, v13}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mContentLayout:Landroid/text/StaticLayout;

    .line 164
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int p2, p2, v2

    .line 177
    .end local v13           #maxLines:I
    :cond_62
    :goto_62
    sget v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDividerYPadding:I

    add-int p2, p2, v2

    .line 178
    move/from16 v0, p2

    int-to-float v2, v0

    iput v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mDividerY:F

    .line 179
    sget v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDividerYPadding:I

    add-int p2, p2, v2

    .line 182
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_145

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v9, v2, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    .line 183
    .local v9, locationText:Ljava/lang/String;
    :goto_7d
    if-eqz v9, :cond_a2

    .line 184
    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sLocationBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationIconRect:Landroid/graphics/Rect;

    sget v8, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sLocationIconPadding:I

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    sget-object v11, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    const/4 v12, 0x1

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayout:Landroid/text/StaticLayout;

    .line 188
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sPostLocationYPadding:I

    add-int/2addr v2, v3

    add-int p2, p2, v2

    .line 192
    :cond_a2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d9

    .line 193
    sub-int v2, p4, p2

    sget-object v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int v13, v2, v3

    .line 195
    .restart local v13       #maxLines:I
    if-lez v13, :cond_d9

    .line 196
    sget-object v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    move/from16 v0, p3

    invoke-static {v2, v3, v0, v13}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReviewBodyLayout:Landroid/text/StaticLayout;

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReviewBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sContentYPadding:I

    add-int/2addr v2, v3

    add-int p2, p2, v2

    .line 204
    .end local v13           #maxLines:I
    :cond_d9
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v14

    .line 205
    .local v14, r:Landroid/graphics/Rect;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mWrapContent:Z

    if-eqz v2, :cond_10c

    .line 206
    iget v2, v14, Landroid/graphics/Rect;->left:I

    move/from16 v0, p2

    invoke-virtual {v14, v2, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_fb

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v14

    .line 210
    iget v2, v14, Landroid/graphics/Rect;->left:I

    move/from16 v0, p2

    invoke-virtual {v14, v2, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 213
    :cond_fb
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_10c

    .line 214
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v14

    .line 215
    iget v2, v14, Landroid/graphics/Rect;->left:I

    move/from16 v0, p2

    invoke-virtual {v14, v2, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 219
    :cond_10c
    iget v2, v14, Landroid/graphics/Rect;->bottom:I

    return v2

    .line 166
    .end local v9           #locationText:Ljava/lang/String;
    .end local v14           #r:Landroid/graphics/Rect;
    :cond_10f
    iget v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mAutoText:I

    if-eqz v2, :cond_62

    .line 167
    sub-int v2, p4, p2

    sget-object v3, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int v13, v2, v3

    .line 169
    .restart local v13       #maxLines:I
    if-lez v13, :cond_62

    .line 170
    sget-object v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mAutoText:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move/from16 v0, p3

    invoke-static {v2, v3, v0, v13}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    .line 172
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int p2, p2, v2

    goto/16 :goto_62

    .line 182
    .end local v13           #maxLines:I
    :cond_145
    const/4 v9, 0x0

    goto/16 :goto_7d
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 114
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 115
    .local v3, widthDimension:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 117
    .local v1, heightDimensionArg:I
    invoke-static {p2}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->shouldWrapContent(I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mWrapContent:Z

    .line 118
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mWrapContent:Z

    if-eqz v8, :cond_4c

    move v0, v3

    .line 120
    .local v0, heightDimension:I
    :goto_13
    const/4 v5, 0x0

    .line 121
    .local v5, xPadding:I
    const/4 v7, 0x0

    .line 122
    .local v7, yPadding:I
    const/4 v4, 0x0

    .line 123
    .local v4, xDoublePadding:I
    const/4 v6, 0x0

    .line 125
    .local v6, yDoublePadding:I
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mPaddingEnabled:Z

    if-eqz v8, :cond_23

    .line 126
    sget v5, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sXPadding:I

    .line 127
    sget v7, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sYPadding:I

    .line 128
    sget v4, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sXDoublePadding:I

    .line 129
    sget v6, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sYDoublePadding:I

    .line 132
    :cond_23
    sget v8, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sLeftBorderPadding:I

    add-int/2addr v8, v5

    sget v9, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sTopBorderPadding:I

    add-int/2addr v9, v7

    sget v10, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sLeftBorderPadding:I

    add-int/2addr v10, v4

    sget v11, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sRightBorderPadding:I

    add-int/2addr v10, v11

    sub-int v10, v3, v10

    sget v11, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sTopBorderPadding:I

    add-int/2addr v11, v6

    sget v12, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sBottomBorderPadding:I

    add-int/2addr v11, v12

    sub-int v11, v0, v11

    invoke-virtual {p0, v8, v9, v10, v11}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->layoutElements(IIII)I

    move-result v2

    .line 137
    .local v2, measuredHeight:I
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mWrapContent:Z

    if-eqz v8, :cond_4e

    .line 138
    sget v8, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sTopBorderPadding:I

    add-int/2addr v8, v2

    add-int/2addr v8, v6

    sget v9, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->sBottomBorderPadding:I

    add-int/2addr v8, v9

    invoke-virtual {p0, v3, v8}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->setMeasuredDimension(II)V

    .line 143
    :goto_4b
    return-void

    .end local v0           #heightDimension:I
    .end local v2           #measuredHeight:I
    .end local v4           #xDoublePadding:I
    .end local v5           #xPadding:I
    .end local v6           #yDoublePadding:I
    .end local v7           #yPadding:I
    :cond_4c
    move v0, v1

    .line 118
    goto :goto_13

    .line 141
    .restart local v0       #heightDimension:I
    .restart local v2       #measuredHeight:I
    .restart local v4       #xDoublePadding:I
    .restart local v5       #xPadding:I
    .restart local v6       #yDoublePadding:I
    .restart local v7       #yPadding:I
    :cond_4e
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->setMeasuredDimension(II)V

    goto :goto_4b
.end method

.method public onRecycle()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 283
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onRecycle()V

    .line 284
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mWrapContent:Z

    .line 285
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayout:Landroid/text/StaticLayout;

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mLocationLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 288
    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardView;->mReviewBodyLayout:Landroid/text/StaticLayout;

    .line 289
    return-void
.end method
