.class public Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "CirclesMembershipActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

.field private mPositiveButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private getPersonId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "person_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isEmptySelectionAllowed()Z
    .registers 4

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "empty_selection_allowed"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 174
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 42
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    if-eqz v0, :cond_22

    .line 43
    check-cast p1, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setCircleUsageType(I)V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setNewCircleEnabled(Z)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setPersonId(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setOnCircleSelectionListener(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;)V

    .line 49
    :cond_22
    return-void
.end method

.method public final onCircleSelectionChange()V
    .registers 3

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->isEmptySelectionAllowed()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 109
    :cond_6
    :goto_6
    return-void

    .line 106
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getSelectedCircleIds()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_22

    const/4 v0, 0x1

    :goto_1e
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_6

    :cond_22
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter "v"

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_60

    .line 95
    :cond_7
    :goto_7
    return-void

    .line 86
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getOriginalCircleIds()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getSelectedCircleIds()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    :cond_29
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "person_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getPersonId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "display_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "display_name"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "original_circle_ids"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "selected_circle_ids"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    goto :goto_7

    .line 91
    :pswitch_5c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    goto :goto_7

    .line 84
    :pswitch_data_60
    .packed-switch 0x7f09005c
        :pswitch_8
        :pswitch_5c
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f030012

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setContentView(I)V

    .line 59
    const v0, 0x7f080221

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setTitle(I)V

    .line 61
    const v0, 0x7f09005c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->isEmptySelectionAllowed()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const v0, 0x7f09005d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    .line 77
    :cond_c
    return-void
.end method
