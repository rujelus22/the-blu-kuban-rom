.class public Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "NetworkTransactionsActivity.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 152
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .registers 4

    .prologue
    .line 74
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_20

    .line 75
    const v1, 0x7f09023d

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 80
    .local v0, progressBarView:Landroid/widget/ProgressBar;
    :goto_f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f09013d

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 82
    return-void

    .line 77
    .end local v0           #progressBarView:Landroid/widget/ProgressBar;
    :cond_20
    const v1, 0x7f09004a

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .restart local v0       #progressBarView:Landroid/widget/ProgressBar;
    goto :goto_f
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x1

    .line 33
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v2, 0x7f030062

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->setContentView(I)V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 38
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 40
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_26

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 43
    .local v0, actionBar:Landroid/app/ActionBar;
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 54
    .end local v0           #actionBar:Landroid/app/ActionBar;
    :goto_25
    return-void

    .line 46
    :cond_26
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->showTitlebar(Z)V

    .line 49
    const v2, 0x7f0801d2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 52
    const v2, 0x7f100015

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->createTitlebarButtons(I)V

    goto :goto_25
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100015

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 121
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_26

    .line 134
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_c
    return v0

    .line 123
    :sswitch_d
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    move v0, v1

    .line 124
    goto :goto_c

    .line 128
    :sswitch_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v2, 0x7f09013d

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->clear()V

    move v0, v1

    .line 130
    goto :goto_c

    .line 121
    :sswitch_data_26
    .sparse-switch
        0x102002c -> :sswitch_d
        0x7f0902c9 -> :sswitch_14
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 99
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_11

    .line 100
    const v0, 0x7f0902c9

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 103
    :cond_11
    const/4 v0, 0x1

    return v0
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    .line 114
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->finish()V

    .line 66
    :cond_c
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 145
    return-void
.end method
