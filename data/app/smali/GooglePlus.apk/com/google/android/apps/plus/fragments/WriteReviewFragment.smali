.class public Lcom/google/android/apps/plus/fragments/WriteReviewFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "WriteReviewFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/RadioGroup$OnCheckedChangeListener;"
    }
.end annotation


# static fields
.field private static final BUCKETED_PRICE_DRAWABLES:[I

.field private static final sRatingValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sRatingViews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAspectCost:Landroid/widget/EditText;

.field private mAspectCostTextWatcher:Landroid/text/TextWatcher;

.field private mAspectRatings:Landroid/widget/LinearLayout;

.field private mBucketedPriceContainer:Landroid/widget/LinearLayout;

.field private mBucketedPriceGroup:Landroid/widget/RadioGroup;

.field private mBucketedPriceTip:Landroid/widget/TextView;

.field private mBucketedPrices:[Landroid/widget/RadioButton;

.field private mBusinessAddress:Landroid/widget/TextView;

.field private mBusinessPhoto:Lcom/google/android/apps/plus/views/EsImageView;

.field private mBusinessTitle:Landroid/widget/TextView;

.field private mCancelButton:Landroid/widget/Button;

.field private mCid:Ljava/lang/String;

.field private mContinuousCostContainer:Landroid/widget/LinearLayout;

.field private mCostCurrencySymbol:Landroid/widget/TextView;

.field private mCostExplanation:Landroid/widget/TextView;

.field private mCostLabel:Landroid/widget/TextView;

.field private mPendingDeleteRequestId:Ljava/lang/Integer;

.field private mPendingWriteRequestId:Ljava/lang/Integer;

.field private mPersonId:Ljava/lang/String;

.field private mPostingPubliclyNotice:Landroid/widget/TextView;

.field private final mPriceLevels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/api/services/plusi/model/PriceLevelProto;",
            ">;"
        }
    .end annotation
.end field

.field private mPublishButton:Landroid/widget/Button;

.field private mReviewExists:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mWriteReview:Landroid/widget/EditText;

.field private mWriteReviewTextWatcher:Landroid/text/TextWatcher;

.field private mYourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const v6, 0x7f09026d

    const v5, 0x7f09026c

    const v4, 0x7f09026b

    const v3, 0x7f09026a

    .line 72
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_7c

    sput-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->BUCKETED_PRICE_DRAWABLES:[I

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingViews:Ljava/util/HashMap;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingValues:Ljava/util/HashMap;

    .line 82
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingViews:Ljava/util/HashMap;

    const-string v1, "0"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingViews:Ljava/util/HashMap;

    const-string v1, "1"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingViews:Ljava/util/HashMap;

    const-string v1, "2"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingViews:Ljava/util/HashMap;

    const-string v1, "3"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingValues:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingValues:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingValues:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingValues:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    return-void

    .line 72
    nop

    :array_7c
    .array-data 0x4
        0x4bt 0x0t 0x2t 0x7ft
        0x51t 0x0t 0x2t 0x7ft
        0x4et 0x0t 0x2t 0x7ft
        0x48t 0x0t 0x2t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPriceLevels:Ljava/util/HashMap;

    .line 96
    new-instance v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 117
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPrices:[Landroid/widget/RadioButton;

    .line 128
    new-instance v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mWriteReviewTextWatcher:Landroid/text/TextWatcher;

    .line 143
    new-instance v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectCostTextWatcher:Landroid/text/TextWatcher;

    .line 450
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)Lcom/google/api/services/plusi/model/GoogleReviewProto;
    .registers 2
    .parameter "x0"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mYourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mWriteReview:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectCost:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->handleWriteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->handleDeleteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private handleDeleteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v3, 0x0

    .line 500
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_e

    .line 519
    :cond_d
    :goto_d
    return-void

    .line 503
    :cond_e
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    .line 505
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "write_review_request_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 507
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_22

    .line 508
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 511
    :cond_22
    if-eqz p2, :cond_39

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 512
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0801bd

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 515
    :cond_39
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0801be

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_d
.end method

.method private handleWriteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v3, 0x0

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_e

    .line 483
    :cond_d
    :goto_d
    return-void

    .line 467
    :cond_e
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    .line 469
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "write_review_request_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 471
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_22

    .line 472
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 475
    :cond_22
    if-eqz p2, :cond_39

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0801b5

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 479
    :cond_39
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0801b6

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 481
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_d
.end method

.method private showProgressDialog(Ljava/lang/String;)V
    .registers 5
    .parameter "text"

    .prologue
    .line 441
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 443
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "write_review_request_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 444
    return-void
.end method

.method private updateBucketedPriceViews(Lcom/google/api/services/plusi/model/PriceLevelsProto;)V
    .registers 9
    .parameter "priceLevels"

    .prologue
    .line 366
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceContainer:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 367
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostLabel:Landroid/widget/TextView;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/PriceLevelsProto;->labelDisplay:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostExplanation:Landroid/widget/TextView;

    const v6, 0x7f0801b3

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    const/4 v1, 0x0

    .line 370
    .local v1, i:I
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPriceLevels:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 371
    iget-object v5, p1, Lcom/google/api/services/plusi/model/PriceLevelsProto;->priceLevel:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_25
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_66

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/PriceLevelProto;

    .line 372
    .local v3, priceLevel:Lcom/google/api/services/plusi/model/PriceLevelProto;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPrices:[Landroid/widget/RadioButton;

    aget-object v0, v5, v1

    .line 373
    .local v0, button:Landroid/widget/RadioButton;
    iget-object v4, v3, Lcom/google/api/services/plusi/model/PriceLevelProto;->labelDisplay:Ljava/lang/String;

    .line 376
    .local v4, priceLevelLabel:Ljava/lang/String;
    if-eqz v4, :cond_5e

    .line 377
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :goto_3c
    iget-object v5, p1, Lcom/google/api/services/plusi/model/PriceLevelsProto;->ratedValueId:Ljava/lang/Long;

    if-eqz v5, :cond_4e

    iget-object v5, p1, Lcom/google/api/services/plusi/model/PriceLevelsProto;->ratedValueId:Ljava/lang/Long;

    iget-object v6, v3, Lcom/google/api/services/plusi/model/PriceLevelProto;->valueId:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4e

    .line 384
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 386
    :cond_4e
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPriceLevels:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    add-int/lit8 v1, v1, 0x1

    .line 388
    goto :goto_25

    .line 379
    :cond_5e
    sget-object v5, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->BUCKETED_PRICE_DRAWABLES:[I

    aget v5, v5, v1

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setBackgroundResource(I)V

    goto :goto_3c

    .line 389
    .end local v0           #button:Landroid/widget/RadioButton;
    .end local v3           #priceLevel:Lcom/google/api/services/plusi/model/PriceLevelProto;
    .end local v4           #priceLevelLabel:Ljava/lang/String;
    :cond_66
    return-void
.end method

.method private updateZagatAspectViews(Ljava/util/List;)V
    .registers 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 331
    .local p1, zagatAspects:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;>;"
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectRatings:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 332
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_68

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    .line 333
    .local v0, aspect:Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0300e0

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectRatings:Landroid/widget/LinearLayout;

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 335
    .local v1, aspectRating:Landroid/widget/LinearLayout;
    const v7, 0x7f090268

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 336
    .local v4, label:Landroid/widget/TextView;
    iget-object v7, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelDisplay:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectRatings:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 339
    const v7, 0x7f090269

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    .line 341
    .local v2, aspectRatingGroup:Landroid/widget/RadioGroup;
    invoke-virtual {v2, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 342
    invoke-virtual {v2, v0}, Landroid/widget/RadioGroup;->setTag(Ljava/lang/Object;)V

    .line 344
    iget-object v7, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    if-eqz v7, :cond_9

    .line 345
    sget-object v7, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingViews:Ljava/util/HashMap;

    iget-object v8, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 346
    .local v6, ratingViewId:I
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    .line 347
    .local v5, ratingView:Landroid/widget/RadioButton;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_9

    .line 350
    .end local v0           #aspect:Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;
    .end local v1           #aspectRating:Landroid/widget/LinearLayout;
    .end local v2           #aspectRatingGroup:Landroid/widget/RadioGroup;
    .end local v4           #label:Landroid/widget/TextView;
    .end local v5           #ratingView:Landroid/widget/RadioButton;
    .end local v6           #ratingViewId:I
    :cond_68
    return-void
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 587
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .registers 9
    .parameter "radioGroup"
    .parameter "checkedId"

    .prologue
    .line 622
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getId()I

    move-result v1

    .line 623
    .local v1, id:I
    const v2, 0x7f090269

    if-ne v1, v2, :cond_1e

    .line 624
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    .line 625
    .local v0, aspect:Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;
    sget-object v2, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->sRatingValues:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    .line 630
    .end local v0           #aspect:Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;
    :cond_1d
    :goto_1d
    return-void

    .line 626
    :cond_1e
    const v2, 0x7f090276

    if-ne v1, v2, :cond_1d

    .line 627
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mYourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v3, v2, Lcom/google/api/services/plusi/model/GoogleReviewProto;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPriceLevels:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/PriceLevelProto;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PriceLevelProto;->valueId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v3, Lcom/google/api/services/plusi/model/PriceLevelsProto;->ratedValueId:Ljava/lang/Long;

    .line 628
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_60

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPriceLevels:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/PriceLevelProto;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceTip:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceTip:Landroid/widget/TextView;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PriceLevelProto;->labelHintDisplay:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1d

    :cond_60
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceTip:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1d
.end method

.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter "view"

    .prologue
    const/4 v3, 0x0

    .line 487
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 488
    .local v0, id:I
    const v1, 0x7f090282

    if-ne v0, v1, :cond_70

    move v2, v3

    .line 489
    :goto_b
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectRatings:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_4d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectRatings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const v4, 0x7f090269

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    const/4 v4, -0x1

    if-ne v1, v4, :cond_49

    :goto_29
    if-nez v3, :cond_4f

    .line 490
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0801bb

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0801c4

    new-instance v3, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$3;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 497
    :cond_48
    :goto_48
    return-void

    .line 489
    :cond_49
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_b

    :cond_4d
    const/4 v3, 0x1

    goto :goto_29

    .line 492
    :cond_4f
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mYourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCid:Ljava/lang/String;

    const v3, 0x7f0801b4

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->showProgressDialog(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v3, v4, v5, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->writeReview(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/GoogleReviewProto;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    goto :goto_48

    .line 494
    :cond_70
    const v1, 0x7f090188

    if-ne v0, v1, :cond_48

    .line 495
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_48
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 166
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 167
    const-string v1, "person_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPersonId:Ljava/lang/String;

    .line 169
    if-eqz p1, :cond_43

    .line 170
    const-string v1, "write_review_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 171
    const-string v1, "write_review_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    .line 173
    :cond_2f
    const-string v1, "delete_review_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 174
    const-string v1, "delete_review_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    .line 178
    :cond_43
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 179
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    const-string v0, "WriteReviewFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 257
    const-string v0, "WriteReviewFragment"

    const-string v1, "Loader<ProfileAndContactData> onCreateLoader()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_10
    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPersonId:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProfileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 184
    const v1, 0x7f0300e2

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 185
    .local v0, view:Landroid/view/View;
    const v1, 0x7f09027d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBusinessPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    .line 186
    const v1, 0x7f09027e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBusinessTitle:Landroid/widget/TextView;

    .line 187
    const v1, 0x7f09027f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBusinessAddress:Landroid/widget/TextView;

    .line 188
    const v1, 0x7f090284

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mWriteReview:Landroid/widget/EditText;

    .line 189
    const v1, 0x7f090280

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectRatings:Landroid/widget/LinearLayout;

    .line 190
    const v1, 0x7f090271

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mContinuousCostContainer:Landroid/widget/LinearLayout;

    .line 192
    const v1, 0x7f090274

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceContainer:Landroid/widget/LinearLayout;

    .line 194
    const v1, 0x7f090275

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceTip:Landroid/widget/TextView;

    .line 195
    const v1, 0x7f09026f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostLabel:Landroid/widget/TextView;

    .line 196
    const v1, 0x7f090270

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostExplanation:Landroid/widget/TextView;

    .line 197
    const v1, 0x7f090272

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostCurrencySymbol:Landroid/widget/TextView;

    .line 198
    const v1, 0x7f090273

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectCost:Landroid/widget/EditText;

    .line 199
    const v1, 0x7f090281

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPostingPubliclyNotice:Landroid/widget/TextView;

    .line 200
    const v1, 0x7f090282

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPublishButton:Landroid/widget/Button;

    .line 201
    const v1, 0x7f090188

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCancelButton:Landroid/widget/Button;

    .line 203
    const v1, 0x7f090276

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceGroup:Landroid/widget/RadioGroup;

    .line 204
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPrices:[Landroid/widget/RadioButton;

    const v1, 0x7f090277

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    .line 205
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPrices:[Landroid/widget/RadioButton;

    const/4 v3, 0x1

    const v1, 0x7f090278

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    .line 206
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPrices:[Landroid/widget/RadioButton;

    const/4 v3, 0x2

    const v1, 0x7f090279

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPrices:[Landroid/widget/RadioButton;

    const/4 v3, 0x3

    const v1, 0x7f09027a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    .line 209
    const v1, 0x7f0801b1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPublishButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPublishButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0801b2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBucketedPriceGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v1, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectCost:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectCostTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mWriteReview:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mWriteReviewTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 214
    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 12
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    check-cast p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    .end local p2
    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->cid:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCid:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getYourReview(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-result-object v4

    if-eqz v4, :cond_87

    move v0, v1

    :goto_18
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mReviewExists:Z

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getUserActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;

    move-result-object v0

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;->reviewTemplate:Ljava/util/List;

    if-nez v0, :cond_89

    move-object v3, v5

    :goto_25
    if-nez v4, :cond_91

    move-object v0, v3

    :goto_28
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mYourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->invalidateActionBar()V

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->photoUrl:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->displayName:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getFullAddress(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBusinessPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBusinessTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mBusinessAddress:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mYourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->updateZagatAspectViews(Ljava/util/List;)V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;->price:Lcom/google/api/services/plusi/model/PriceProto;

    iget-object v4, v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    if-nez v0, :cond_fe

    if-eqz v4, :cond_5e

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->updateBucketedPriceViews(Lcom/google/api/services/plusi/model/PriceLevelsProto;)V

    :cond_5e
    :goto_5e
    iget-object v0, v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;->fullText:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;->snippet:Ljava/lang/String;

    if-eqz v0, :cond_6a

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6b

    :cond_6a
    move-object v0, v3

    :cond_6b
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mWriteReview:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0801c1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPostingPubliclyNotice:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_87
    move v0, v2

    goto :goto_18

    :cond_89
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-object v3, v0

    goto :goto_25

    :cond_91
    if-eqz v3, :cond_fb

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getZagatAspects(Lcom/google/api/services/plusi/model/GoogleReviewProto;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_bc

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_a2
    :goto_a2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_bc

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    iget-object v8, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->isEditable:Ljava/lang/Boolean;

    if-eqz v8, :cond_b8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_a2

    :cond_b8
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a2

    :cond_bc
    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getZagatAspects(Lcom/google/api/services/plusi/model/GoogleReviewProto;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_d6

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_c6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c6

    :cond_d6
    iget-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    if-nez v0, :cond_e1

    new-instance v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;-><init>()V

    iput-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    :cond_e1
    iget-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iput-object v6, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->price:Lcom/google/api/services/plusi/model/PriceProto;

    if-nez v0, :cond_ed

    iget-object v0, v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;->price:Lcom/google/api/services/plusi/model/PriceProto;

    iput-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->price:Lcom/google/api/services/plusi/model/PriceProto;

    :cond_ed
    iget-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    if-nez v0, :cond_f5

    iget-object v0, v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    iput-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    :cond_f5
    iget-object v0, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->price:Lcom/google/api/services/plusi/model/PriceProto;

    if-eqz v0, :cond_fb

    iput-object v5, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    :cond_fb
    move-object v0, v4

    goto/16 :goto_28

    :cond_fe
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mContinuousCostContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostLabel:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PriceProto;->labelDisplay:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostExplanation:Landroid/widget/TextView;

    const v5, 0x7f0801b3

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCostCurrencySymbol:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PriceProto;->currency:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAspectCost:Landroid/widget/EditText;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PriceProto;->valueDisplay:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5e
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, arg0:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;>;"
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    .line 305
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_28

    .line 311
    const/4 v0, 0x0

    .line 314
    :goto_8
    return v0

    .line 307
    :pswitch_9
    const v0, 0x7f0801bc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->showProgressDialog(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mCid:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->deleteReview(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    .line 314
    const/4 v0, 0x1

    goto :goto_8

    .line 305
    nop

    :pswitch_data_28
    .packed-switch 0x7f0902c5
        :pswitch_9
    .end packed-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 239
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPause()V

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 241
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 4
    .parameter "menu"

    .prologue
    .line 297
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mReviewExists:Z

    if-eqz v1, :cond_f

    .line 298
    const v1, 0x7f0902c5

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 299
    .local v0, deleteItem:Landroid/view/MenuItem;
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 301
    .end local v0           #deleteItem:Landroid/view/MenuItem;
    :cond_f
    return-void
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 219
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onResume()V

    .line 220
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2b

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2b

    .line 225
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 226
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->handleWriteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 229
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_2b
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_4e

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_4e

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 232
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->handleDeleteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 235
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_4e
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 245
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 247
    const-string v0, "write_review_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingWriteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 249
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_21

    .line 250
    const-string v0, "delete_review_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mPendingDeleteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 252
    :cond_21
    return-void
.end method

.method public final recordNavigationAction()V
    .registers 1

    .prologue
    .line 583
    return-void
.end method
