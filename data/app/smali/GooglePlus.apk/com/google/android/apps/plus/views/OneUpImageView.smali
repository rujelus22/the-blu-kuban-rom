.class public Lcom/google/android/apps/plus/views/OneUpImageView;
.super Lcom/google/android/apps/plus/views/OneUpBackgroundView;
.source "OneUpImageView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;


# instance fields
.field protected mDesiredHeight:I

.field protected mDesiredWidth:I

.field protected mRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/OneUpImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/OneUpImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method private fitToHeight(IIII)V
    .registers 10
    .parameter "bmpWidth"
    .parameter "bmpHeight"
    .parameter "viewWidth"
    .parameter "viewHeight"

    .prologue
    const/4 v4, 0x0

    .line 167
    const/high16 v2, 0x3f80

    int-to-float v3, p4

    mul-float/2addr v2, v3

    int-to-float v3, p2

    div-float/2addr v2, v3

    int-to-float v3, p1

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 168
    .local v0, aspectWidth:I
    sub-int v2, p3, v0

    div-int/lit8 v2, v2, 0x2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 169
    .local v1, offset:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mRect:Landroid/graphics/Rect;

    add-int v3, v1, v0

    invoke-virtual {v2, v1, v4, v3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 170
    return-void
.end method

.method private fitToWidth(IIII)V
    .registers 10
    .parameter "bmpWidth"
    .parameter "bmpHeight"
    .parameter "viewWidth"
    .parameter "viewHeight"

    .prologue
    const/4 v4, 0x0

    .line 161
    const/high16 v2, 0x3f80

    int-to-float v3, p3

    mul-float/2addr v2, v3

    int-to-float v3, p1

    div-float/2addr v2, v3

    int-to-float v3, p2

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 162
    .local v0, aspectHeight:I
    sub-int v2, p4, v0

    div-int/lit8 v2, v2, 0x2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 163
    .local v1, offset:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mRect:Landroid/graphics/Rect;

    add-int v3, v1, v0

    invoke-virtual {v2, v4, v1, p3, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 164
    return-void
.end method

.method private makeBestFitRect()V
    .registers 7

    .prologue
    .line 134
    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mRect:Landroid/graphics/Rect;

    if-nez v5, :cond_b

    .line 135
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mRect:Landroid/graphics/Rect;

    .line 138
    :cond_b
    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v5, :cond_2c

    .line 139
    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 140
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2c

    .line 141
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 142
    .local v2, bmpWidth:I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 143
    .local v1, bmpHeight:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->getMeasuredWidth()I

    move-result v4

    .line 144
    .local v4, viewWidth:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->getMeasuredHeight()I

    move-result v3

    .line 145
    .local v3, viewHeight:I
    if-le v2, v1, :cond_2d

    .line 146
    invoke-direct {p0, v2, v1, v4, v3}, Lcom/google/android/apps/plus/views/OneUpImageView;->fitToWidth(IIII)V

    .line 158
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #bmpHeight:I
    .end local v2           #bmpWidth:I
    .end local v3           #viewHeight:I
    .end local v4           #viewWidth:I
    :cond_2c
    :goto_2c
    return-void

    .line 147
    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    .restart local v1       #bmpHeight:I
    .restart local v2       #bmpWidth:I
    .restart local v3       #viewHeight:I
    .restart local v4       #viewWidth:I
    :cond_2d
    if-lt v2, v1, :cond_35

    .line 148
    if-ge v4, v3, :cond_35

    .line 151
    invoke-direct {p0, v2, v1, v4, v3}, Lcom/google/android/apps/plus/views/OneUpImageView;->fitToWidth(IIII)V

    goto :goto_2c

    .line 153
    :cond_35
    invoke-direct {p0, v2, v1, v4, v3}, Lcom/google/android/apps/plus/views/OneUpImageView;->fitToHeight(IIII)V

    goto :goto_2c
.end method


# virtual methods
.method public final init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;II)V
    .registers 6
    .parameter "mediaRef"
    .parameter "type"
    .parameter "listener"
    .parameter "desiredWidth"
    .parameter "desiredHeight"

    .prologue
    .line 42
    iput p4, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    .line 43
    iput p5, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;)V

    .line 46
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/plus/views/OneUpImageView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V

    .line 51
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onAttachedToWindow()V

    .line 52
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/apps/plus/views/OneUpImageView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V

    .line 57
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onDetachedFromWindow()V

    .line 58
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    .prologue
    .line 108
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mHasSeenImage:Z

    .line 110
    .local v1, hasSeenImage:Z
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onDraw(Landroid/graphics/Canvas;)V

    .line 112
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 113
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1a

    .line 114
    if-nez v1, :cond_12

    .line 115
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->makeBestFitRect()V

    .line 117
    :cond_12
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mRect:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/apps/plus/views/OneUpImageView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 119
    :cond_1a
    return-void
.end method

.method public onLayout(ZIIII)V
    .registers 15
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    const/4 v8, -0x1

    const/high16 v7, 0x3f80

    .line 62
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onLayout(ZIIII)V

    .line 64
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->makeBestFitRect()V

    .line 66
    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-nez v3, :cond_58

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v3, :cond_58

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_25

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_58

    .line 69
    :cond_25
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v2

    .line 70
    .local v2, screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    if-ne v3, v8, :cond_59

    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    if-ne v3, v8, :cond_59

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->getMeasuredWidth()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->getMeasuredHeight()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    .line 99
    :goto_41
    new-instance v1, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    iget v5, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/apps/plus/content/RemoteImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .line 101
    .local v1, req:Lcom/google/android/apps/plus/content/ImageRequest;
    new-instance v3, Lcom/google/android/apps/plus/views/MediaImage;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 102
    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 104
    .end local v1           #req:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v2           #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    :cond_58
    return-void

    .line 77
    .restart local v2       #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    :cond_59
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    if-le v3, v4, :cond_8f

    .line 78
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    iget v4, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    if-le v3, v4, :cond_7a

    .line 79
    iget v3, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    int-to-float v3, v3

    mul-float/2addr v3, v7

    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 80
    .local v0, ratio:F
    iget v3, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    .line 81
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    goto :goto_41

    .line 83
    .end local v0           #ratio:F
    :cond_7a
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    int-to-float v3, v3

    mul-float/2addr v3, v7

    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 84
    .restart local v0       #ratio:F
    iget v3, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    .line 85
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    goto :goto_41

    .line 88
    .end local v0           #ratio:F
    :cond_8f
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    iget v4, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    if-le v3, v4, :cond_aa

    .line 89
    iget v3, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    int-to-float v3, v3

    mul-float/2addr v3, v7

    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 90
    .restart local v0       #ratio:F
    iget v3, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    .line 91
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    goto :goto_41

    .line 93
    .end local v0           #ratio:F
    :cond_aa
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    int-to-float v3, v3

    mul-float/2addr v3, v7

    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 94
    .restart local v0       #ratio:F
    iget v3, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    .line 95
    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredHeight:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mDesiredWidth:I

    goto :goto_41
.end method

.method public final onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/Bitmap;)V
    .registers 8
    .parameter "request"
    .parameter "bitmap"

    .prologue
    .line 123
    instance-of v1, p1, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v1, :cond_9

    .line 131
    :cond_8
    :goto_8
    return-void

    :cond_9
    move-object v0, p1

    .line 126
    check-cast v0, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    .line 127
    .local v0, remoteRequest:Lcom/google/android/apps/plus/content/RemoteImageRequest;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getPhotoId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpImageView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_8

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->onImageChanged()V

    goto :goto_8
.end method
