.class final Lcom/google/android/apps/plus/service/PhotoCache$1;
.super Landroid/support/v4/util/LruCache;
.source "PhotoCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/PhotoCache;-><init>(Lcom/google/android/apps/plus/service/PhotoCache$CacheListener;Lcom/google/android/apps/plus/service/PhotoCache$PhotoLoader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<TE;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/PhotoCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/PhotoCache;I)V
    .registers 3
    .parameter
    .parameter "x0"

    .prologue
    .line 92
    .local p0, this:Lcom/google/android/apps/plus/service/PhotoCache$1;,"Lcom/google/android/apps/plus/service/PhotoCache.1;"
    iput-object p1, p0, Lcom/google/android/apps/plus/service/PhotoCache$1;->this$0:Lcom/google/android/apps/plus/service/PhotoCache;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 92
    .local p0, this:Lcom/google/android/apps/plus/service/PhotoCache$1;,"Lcom/google/android/apps/plus/service/PhotoCache.1;"
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2
    if-nez p2, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_5
.end method
