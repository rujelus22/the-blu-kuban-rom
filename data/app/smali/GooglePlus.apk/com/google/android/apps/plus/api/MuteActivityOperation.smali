.class public final Lcom/google/android/apps/plus/api/MuteActivityOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "MuteActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MuteActivityRequest;",
        "Lcom/google/api/services/plusi/model/MuteActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mIsMuted:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "isMuted"

    .prologue
    .line 38
    const-string v3, "muteactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/MuteActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MuteActivityRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MuteActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MuteActivityResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 45
    iput-object p5, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mActivityId:Ljava/lang/String;

    .line 46
    iput-boolean p6, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mIsMuted:Z

    .line 47
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mActivityId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mIsMuted:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->muteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 20
    check-cast p1, Lcom/google/api/services/plusi/model/MuteActivityRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mActivityId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MuteActivityRequest;->activityId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MuteActivityOperation;->mIsMuted:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MuteActivityRequest;->muteState:Ljava/lang/Boolean;

    return-void
.end method
