.class public abstract Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "InstrumentedActivity.java"


# instance fields
.field private mExited:Z

.field private mRecorded:Z

.field private mStartingActivity:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private static getStartTime(Landroid/content/Intent;)J
    .registers 4
    .parameter "intent"

    .prologue
    .line 379
    const-string v0, "com.google.plus.analytics.intent.extra.START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 4
    .parameter "intent"

    .prologue
    .line 367
    const-string v1, "com.google.plus.analytics.intent.extra.START_VIEW"

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 368
    .local v0, number:I
    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->valueOf(I)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    return-object v1
.end method

.method private instrument(Landroid/content/Intent;)Landroid/content/Intent;
    .registers 9
    .parameter "intent"

    .prologue
    .line 515
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    .line 516
    .local v3, view:Lcom/google/android/apps/plus/analytics/OzViews;
    if-eqz v3, :cond_2d

    .line 517
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V
    :try_end_b
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_b} :catch_2f

    .line 518
    .end local p1
    .local v2, intent:Landroid/content/Intent;
    :try_start_b
    const-string v4, "com.google.plus.analytics.intent.extra.START_VIEW"

    invoke-virtual {v3}, Lcom/google/android/apps/plus/analytics/OzViews;->ordinal()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 519
    const-string v4, "com.google.plus.analytics.intent.extra.START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 521
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v1

    .line 522
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2c

    .line 523
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
    :try_end_2c
    .catch Landroid/os/BadParcelableException; {:try_start_b .. :try_end_2c} :catch_42

    :cond_2c
    move-object p1, v2

    .end local v1           #extras:Landroid/os/Bundle;
    .end local v2           #intent:Landroid/content/Intent;
    .restart local p1
    :cond_2d
    move-object v2, p1

    .line 537
    .end local v3           #view:Lcom/google/android/apps/plus/analytics/OzViews;
    .end local p1
    .restart local v2       #intent:Landroid/content/Intent;
    :goto_2e
    return-object v2

    .line 528
    .end local v2           #intent:Landroid/content/Intent;
    .restart local p1
    :catch_2f
    move-exception v0

    .line 533
    .local v0, e:Landroid/os/BadParcelableException;
    :goto_30
    const-string v4, "InstrumentedActivity"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_40

    .line 534
    const-string v4, "InstrumentedActivity"

    const-string v5, "Unable to instrument Intent"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_40
    move-object v2, p1

    .line 537
    .end local p1
    .restart local v2       #intent:Landroid/content/Intent;
    goto :goto_2e

    .line 528
    .end local v0           #e:Landroid/os/BadParcelableException;
    .restart local v3       #view:Lcom/google/android/apps/plus/analytics/OzViews;
    :catch_42
    move-exception v0

    move-object p1, v2

    .end local v2           #intent:Landroid/content/Intent;
    .restart local p1
    goto :goto_30
.end method

.method private recordNotificationActionEvent(Landroid/content/Intent;)V
    .registers 11
    .parameter "intent"

    .prologue
    const/4 v7, 0x0

    .line 109
    if-nez p1, :cond_75

    move v6, v7

    :goto_4
    if-eqz v6, :cond_74

    .line 110
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 111
    .local v2, extras:Landroid/os/Bundle;
    const-string v6, "notif_id"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, notificationId:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7c

    .line 115
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 116
    .local v0, action:Lcom/google/android/apps/plus/analytics/OzActions;
    const-string v6, "extra_notification_id"

    invoke-virtual {v2, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v6, "extra_notification_read"

    const-string v8, "com.google.plus.analytics.intent.extra.NOTIFICATION_READ"

    invoke-virtual {p1, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v2, v6, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 123
    :goto_29
    const-string v6, "notif_types"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7f

    const-string v6, "notif_types"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 124
    .local v4, notificationTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_37
    const-string v6, "coalescing_codes"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_85

    const-string v6, "coalescing_codes"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 125
    .local v1, coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_45
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5f

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v6, v7, :cond_5f

    .line 127
    const-string v6, "extra_notification_types"

    invoke-virtual {v2, v6, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 129
    const-string v6, "extra_coalescing_codes"

    invoke-virtual {v2, v6, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 133
    :cond_5f
    const-string v6, "com.google.plus.analytics.intent.extra.START_VIEW"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8b

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 137
    .local v5, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    :goto_69
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-static {v6, v7, v0, v5, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    .line 140
    .end local v0           #action:Lcom/google/android/apps/plus/analytics/OzActions;
    .end local v1           #coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2           #extras:Landroid/os/Bundle;
    .end local v3           #notificationId:Ljava/lang/String;
    .end local v4           #notificationTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    :cond_74
    return-void

    .line 109
    :cond_75
    const-string v6, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    goto :goto_4

    .line 120
    .restart local v2       #extras:Landroid/os/Bundle;
    .restart local v3       #notificationId:Ljava/lang/String;
    :cond_7c
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    .restart local v0       #action:Lcom/google/android/apps/plus/analytics/OzActions;
    goto :goto_29

    .line 123
    :cond_7f
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_37

    .line 124
    .restart local v4       #notificationTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_45

    .line 133
    .restart local v1       #coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_8b
    sget-object v5, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_69
.end method

.method public static recordReverseViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V
    .registers 13
    .parameter "activity"
    .parameter "account"
    .parameter "closedView"
    .parameter "closedViewExtras"

    .prologue
    const/4 v4, 0x0

    .line 261
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 262
    .local v8, intent:Landroid/content/Intent;
    invoke-static {v8}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    .line 263
    .local v3, endView:Lcom/google/android/apps/plus/analytics/OzViews;
    if-eqz v3, :cond_19

    .line 264
    move-object v2, p2

    .line 265
    .local v2, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    move-object v6, p3

    .line 266
    .local v6, startViewExtras:Landroid/os/Bundle;
    const-string v0, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .local v7, endViewExtras:Landroid/os/Bundle;
    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    .line 267
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 270
    .end local v2           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    .end local v6           #startViewExtras:Landroid/os/Bundle;
    .end local v7           #endViewExtras:Landroid/os/Bundle;
    :cond_19
    return-void
.end method

.method private recordViewNavigation()V
    .registers 4

    .prologue
    .line 156
    iget-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    if-eqz v2, :cond_5

    .line 166
    :cond_4
    :goto_4
    return-void

    .line 160
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    .line 161
    .local v1, endView:Lcom/google/android/apps/plus/analytics/OzViews;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 162
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 163
    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 164
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    goto :goto_4
.end method

.method public static recordViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;)V
    .registers 11
    .parameter "activity"
    .parameter "account"
    .parameter "view"

    .prologue
    const/4 v5, 0x0

    .line 184
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    const-string v1, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartTime(Landroid/content/Intent;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v7, v5

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 185
    return-void
.end method


# virtual methods
.method public finish()V
    .registers 4

    .prologue
    .line 493
    iget-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    if-nez v0, :cond_f

    .line 496
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 497
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 502
    :cond_f
    :goto_f
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 503
    return-void

    .line 499
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_f

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p0, v1, v0, v2}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordReverseViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_f
.end method

.method protected abstract getAccount()Lcom/google/android/apps/plus/content/EsAccount;
.end method

.method public final getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/plus/analytics/AnalyticsInfo;"
        }
    .end annotation

    .prologue
    .line 225
    new-instance v0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartTime(Landroid/content/Intent;)J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;J)V

    return-object v0
.end method

.method protected getExtrasForLogging()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 553
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
.end method

.method protected needsAsyncData()Z
    .registers 2

    .prologue
    .line 571
    const/4 v0, 0x0

    return v0
.end method

.method public final onAsyncData()V
    .registers 1

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordViewNavigation()V

    .line 442
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    if-eqz p1, :cond_1d

    .line 67
    const-string v0, "analytics:recorded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    .line 68
    const-string v0, "analytics:exited"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    .line 72
    :goto_15
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordNotificationActionEvent(Landroid/content/Intent;)V

    .line 73
    return-void

    .line 70
    :cond_1d
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    goto :goto_15
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 10
    .parameter "intent"

    .prologue
    const/4 v4, 0x0

    .line 90
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordNotificationActionEvent(Landroid/content/Intent;)V

    .line 97
    invoke-static {p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    .line 98
    .local v2, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    const-string v0, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 99
    .local v6, startViewExtras:Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    .line 100
    .local v3, endView:Lcom/google/android/apps/plus/analytics/OzViews;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v7

    .line 101
    .local v7, endViewExtras:Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    .line 102
    .local v1, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v1, :cond_2a

    if-eqz v2, :cond_2a

    if-eqz v3, :cond_2a

    if-eq v2, v3, :cond_2a

    move-object v0, p0

    move-object v5, v4

    .line 103
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 106
    :cond_2a
    return-void
.end method

.method protected onPause()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 467
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 470
    iput-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    .line 472
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_23

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_2e

    :cond_23
    move v0, v3

    :goto_24
    if-eqz v0, :cond_2d

    .line 474
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 475
    iput-boolean v3, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    .line 477
    :cond_2d
    return-void

    :cond_2e
    move v0, v2

    .line 472
    goto :goto_24
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 449
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 451
    iget-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 453
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 454
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    .line 457
    :cond_15
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->needsAsyncData()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordViewNavigation()V

    .line 460
    :cond_1e
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    const-string v0, "analytics:recorded"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 82
    const-string v0, "analytics:exited"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 83
    return-void
.end method

.method protected onUserLeaveHint()V
    .registers 2

    .prologue
    .line 425
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onUserLeaveHint()V

    .line 426
    iget-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    if-nez v0, :cond_f

    .line 431
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 432
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    .line 434
    :cond_f
    return-void
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 4
    .parameter "info"
    .parameter "action"

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 307
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_9

    .line 308
    invoke-static {p0, v0, p1, p2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 310
    :cond_9
    return-void
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 4
    .parameter "action"

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 294
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_d

    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    invoke-static {p0, v0, p1, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 297
    :cond_d
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    .prologue
    .line 398
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->instrument(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    .line 400
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .registers 4
    .parameter "intent"
    .parameter "requestCode"

    .prologue
    .line 407
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->instrument(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0, p2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 408
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    .line 409
    return-void
.end method

.method public final startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .registers 5
    .parameter "fragment"
    .parameter "intent"
    .parameter "requestCode"

    .prologue
    .line 416
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->instrument(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Landroid/support/v4/app/FragmentActivity;->startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    .line 418
    return-void
.end method

.method public final startExternalActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    .prologue
    .line 239
    const/high16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 240
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->startActivity(Landroid/content/Intent;)V

    .line 241
    return-void
.end method
