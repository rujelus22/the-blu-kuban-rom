.class final Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
.source "HostedHangoutFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RTCListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    return-void
.end method


# virtual methods
.method public final onResponseReceived$1587694a(ILcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 159
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_37

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p1, v1, :cond_37

    .line 160
    invoke-virtual {p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_37

    invoke-virtual {p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSuggestionsResponse()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 162
    invoke-virtual {p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    .line 163
    .local v0, response:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$300(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 167
    .end local v0           #response:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    :cond_37
    return-void
.end method

.method public final onResponseTimeout(I)V
    .registers 3
    .parameter "requestId"

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 174
    return-void
.end method
