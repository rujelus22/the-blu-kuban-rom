.class public Lcom/google/android/apps/plus/phone/NewEventActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "NewEventActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;


# instance fields
.field private mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

.field private mShakeDetectorWasRunning:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected final getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x7f080146

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 3
    .parameter "fragment"

    .prologue
    .line 53
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_12

    .line 54
    check-cast p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->createEvent()V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setOnEventChangedListener(Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;)V

    .line 58
    :cond_12
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_9

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onDiscard()V

    .line 119
    :cond_9
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v1, 0x7f030064

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NewEventActivity;->setContentView(I)V

    .line 36
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NewEventActivity;->showTitlebar(Z)V

    .line 40
    const v1, 0x7f08037a

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NewEventActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NewEventActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 41
    const v1, 0x7f10001b

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NewEventActivity;->createTitlebarButtons(I)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 46
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_2d

    .line 47
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mShakeDetectorWasRunning:Z

    .line 49
    :cond_2d
    return-void
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 77
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    .line 80
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_14

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 82
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_14

    .line 83
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 86
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_14
    return-void
.end method

.method public final onEventClosed()V
    .registers 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    .line 128
    return-void
.end method

.method public final onEventSaved()V
    .registers 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    .line 133
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 90
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_18

    .line 104
    const/4 v0, 0x0

    :cond_9
    :goto_9
    return v0

    .line 92
    :sswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->onBackPressed()V

    goto :goto_9

    .line 97
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v1, :cond_9

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->save()V

    goto :goto_9

    .line 90
    :sswitch_data_18
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f09029b -> :sswitch_e
    .end sparse-switch
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    .line 73
    :cond_c
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->onBackPressed()V

    .line 112
    return-void
.end method
