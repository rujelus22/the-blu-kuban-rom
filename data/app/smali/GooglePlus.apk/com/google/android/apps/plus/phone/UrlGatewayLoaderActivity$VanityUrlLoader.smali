.class public final Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;
.super Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;
.source "UrlGatewayLoaderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VanityUrlLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

.field private final mVanityId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 4
    .parameter "context"
    .parameter "account"
    .parameter "vanityId"

    .prologue
    .line 209
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 210
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 211
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->mVanityId:Ljava/lang/String;

    .line 212
    return-void
.end method


# virtual methods
.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 199
    check-cast p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->isReset()Z

    move-result v0

    if-nez v0, :cond_13

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_13
    return-void
.end method

.method public final bridge synthetic esLoadInBackground()Ljava/lang/Object;
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 199
    new-instance v0, Lcom/google/android/apps/plus/api/ResolveVanityIdOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->mVanityId:Ljava/lang/String;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/ResolveVanityIdOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ResolveVanityIdOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ResolveVanityIdOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1e

    const-string v1, "VanityUrlLoader"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/ResolveVanityIdOperation;->logError(Ljava/lang/String;)V

    :cond_1d
    :goto_1d
    return-object v4

    :cond_1e
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ResolveVanityIdOperation;->getPerson()Lcom/google/api/services/plusi/model/DataCirclePerson;

    move-result-object v0

    if-eqz v0, :cond_1d

    new-instance v4, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-direct {v4}, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;-><init>()V

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    iput-object v1, v4, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->gaiaId:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->displayName:Ljava/lang/String;

    iget-object v0, v4, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->gaiaId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1d

    const/4 v0, 0x2

    iput v0, v4, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    goto :goto_1d
.end method

.method protected final onStartLoading()V
    .registers 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    if-nez v0, :cond_7

    .line 244
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->forceLoad()V

    .line 246
    :cond_7
    return-void
.end method
