.class public Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "BirthdayFieldLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;
    }
.end annotation


# instance fields
.field private mInput:Landroid/widget/DatePicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .registers 13
    .parameter "field"
    .parameter "id"
    .parameter "actionCallback"

    .prologue
    const/4 v6, 0x0

    .line 63
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v5

    iget-object v2, v5, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    .line 66
    .local v2, inputField:Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getLabelView()Landroid/widget/TextView;

    move-result-object v5

    iget-object v7, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/DatePicker;

    iput-object v5, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getServerDateValue()Lcom/google/api/services/plusi/model/MobileCoarseDate;

    move-result-object v0

    .line 71
    .local v0, birthday:Lcom/google/api/services/plusi/model/MobileCoarseDate;
    if-eqz v0, :cond_4d

    .line 72
    iget-object v5, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    if-eqz v5, :cond_47

    iget-object v5, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 73
    .local v4, year:I
    :goto_2b
    iget-object v5, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    if-eqz v5, :cond_49

    iget-object v5, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .line 74
    .local v3, month:I
    :goto_37
    iget-object v5, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    if-eqz v5, :cond_4b

    iget-object v5, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 75
    .local v1, dayOfMonth:I
    :goto_41
    iget-object v5, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v5, v4, v3, v1}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 79
    .end local v1           #dayOfMonth:I
    .end local v3           #month:I
    .end local v4           #year:I
    :goto_46
    return-void

    :cond_47
    move v4, v6

    .line 72
    goto :goto_2b

    .restart local v4       #year:I
    :cond_49
    move v3, v6

    .line 73
    goto :goto_37

    .restart local v3       #month:I
    :cond_4b
    move v1, v6

    .line 74
    goto :goto_41

    .line 77
    .end local v3           #month:I
    .end local v4           #year:I
    :cond_4d
    iget-object v5, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    const/16 v7, 0x7b2

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v6, v8}, Landroid/widget/DatePicker;->updateDate(III)V

    goto :goto_46
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .registers 5

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v1

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-static {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    .line 87
    .local v0, inputField:Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    new-instance v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    .line 88
    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    new-instance v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/MobileCoarseDate;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    .line 89
    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 7
    .parameter "state"

    .prologue
    .line 125
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;

    .line 126
    .local v0, savedState:Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    iget v2, v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->year:I

    iget v3, v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->month:I

    iget v4, v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->day:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 128
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 6

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 114
    .local v2, superState:Landroid/os/Parcelable;
    iget-object v4, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    .line 115
    .local v3, year:I
    iget-object v4, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getMonth()I

    move-result v1

    .line 116
    .local v1, month:I
    iget-object v4, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v0

    .line 117
    .local v0, day:I
    new-instance v4, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;

    invoke-direct {v4, v2, v3, v1, v0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;-><init>(Landroid/os/Parcelable;III)V

    return-object v4
.end method
