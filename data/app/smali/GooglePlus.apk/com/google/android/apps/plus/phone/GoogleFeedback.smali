.class public final Lcom/google/android/apps/plus/phone/GoogleFeedback;
.super Ljava/lang/Object;
.source "GoogleFeedback.java"


# static fields
.field private static final FEEDBACK_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 29
    const-string v0, "http://support.google.com/mobile/?p=plus_survey_android"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/GoogleFeedback;->FEEDBACK_URI:Landroid/net/Uri;

    return-void
.end method

.method public static launch(Landroid/app/Activity;)V
    .registers 11
    .parameter "parentActivity"

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/apps/plus/phone/GoogleFeedback$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/GoogleFeedback$1;-><init>(Landroid/app/Activity;)V

    .line 91
    .local v0, connection:Landroid/content/ServiceConnection;
    new-instance v2, Landroid/content/Intent;

    const-string v8, "android.intent.action.BUG_REPORT"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 93
    .local v2, feedbackIntent:Landroid/content/Intent;
    const/4 v8, 0x1

    invoke-virtual {p0, v2, v0, v8}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    .line 94
    .local v3, installed:Z
    if-nez v3, :cond_51

    .line 98
    invoke-virtual {p0, v0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 100
    sget-object v8, Lcom/google/android/apps/plus/phone/GoogleFeedback;->FEEDBACK_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 103
    .local v6, uriBuilder:Landroid/net/Uri$Builder;
    :try_start_1c
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 104
    .local v5, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    iget-object v7, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_2b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1c .. :try_end_2b} :catch_52

    .line 108
    .end local v5           #pm:Landroid/content/pm/PackageManager;
    .local v7, versionName:Ljava/lang/String;
    :goto_2b
    const-string v8, "version"

    invoke-virtual {v6, v8, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 110
    new-instance v4, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 111
    .local v4, intent:Landroid/content/Intent;
    const/high16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 112
    const-string v8, "android.intent.category.BROWSABLE"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v8, "com.android.browser.application_id"

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 116
    .end local v4           #intent:Landroid/content/Intent;
    .end local v6           #uriBuilder:Landroid/net/Uri$Builder;
    .end local v7           #versionName:Ljava/lang/String;
    :cond_51
    return-void

    .line 105
    .restart local v6       #uriBuilder:Landroid/net/Uri$Builder;
    :catch_52
    move-exception v1

    .line 106
    .local v1, exception:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "unknown"

    .restart local v7       #versionName:Ljava/lang/String;
    goto :goto_2b
.end method
