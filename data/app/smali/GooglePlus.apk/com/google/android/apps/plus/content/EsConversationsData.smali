.class public final Lcom/google/android/apps/plus/content/EsConversationsData;
.super Ljava/lang/Object;
.source "EsConversationsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsConversationsData$4;
    }
.end annotation


# static fields
.field private static final sConversationComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;",
            ">;"
        }
    .end annotation
.end field

.field private static final sHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 112
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsConversationsData;->sHandler:Landroid/os/Handler;

    .line 419
    new-instance v0, Lcom/google/android/apps/plus/content/EsConversationsData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/EsConversationsData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsConversationsData;->sConversationComparator:Ljava/util/Comparator;

    return-void
.end method

.method public static acceptConversationLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "inviterId"
    .parameter "operationState"

    .prologue
    const/4 v2, 0x3

    .line 3055
    const-string v1, "EsConversationsData"

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 3056
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "acceptConversationLocally conversationRowId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3059
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 3061
    .local v6, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3063
    :try_start_28
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 3064
    .local v7, values:Landroid/content/ContentValues;
    const-string v1, "is_pending_accept"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3065
    const-string v1, "conversations"

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v6, v1, v7, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3069
    invoke-static {v6, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v0

    .line 3070
    .local v0, conversationId:Ljava/lang/String;
    invoke-static {v0, p4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->replyToInviteRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-virtual {p5, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3072
    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    sget v5, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->MAX_EVENTS_PER_REQUEST:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getEventStream(Ljava/lang/String;JJI)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-virtual {p5, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3075
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7d

    .line 3076
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requesting 20 events since 0 for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3079
    :cond_7d
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_80
    .catchall {:try_start_28 .. :try_end_80} :catchall_87

    .line 3081
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3083
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3084
    return-void

    .line 3081
    .end local v0           #conversationId:Ljava/lang/String;
    .end local v7           #values:Landroid/content/ContentValues;
    :catchall_87
    move-exception v1

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public static checkMessageSentLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)I
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "messageRowId"
    .parameter "operationState"

    .prologue
    .line 2800
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 2801
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkMessageSentLocally messageRowId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2803
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2805
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2806
    const/4 v10, 0x0

    .line 2807
    .local v10, messageStatus:I
    const/4 v8, 0x0

    .line 2809
    .local v8, conversationRowId:Ljava/lang/Long;
    const/4 v9, 0x0

    .line 2811
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_2b
    const-string v1, "messages"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "status"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2816
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_68

    .line 2817
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2818
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_68
    .catchall {:try_start_2b .. :try_end_68} :catchall_9f

    .line 2821
    :cond_68
    if-eqz v9, :cond_6d

    .line 2822
    :try_start_6a
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2825
    :cond_6d
    invoke-static {v0, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryMessageStatus(Landroid/database/sqlite/SQLiteDatabase;J)I

    move-result v10

    .line 2827
    if-eqz v10, :cond_76

    const/4 v1, 0x1

    if-ne v10, v1, :cond_ab

    .line 2829
    :cond_76
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_86

    .line 2830
    const-string v1, "EsConversationsData"

    const-string v2, "sending again"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2832
    :cond_86
    const/4 v10, 0x7

    .line 2833
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p1

    move-wide v2, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->sendMessageInDatabase$728fb81e(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;JZZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;

    .line 2849
    :cond_8f
    :goto_8f
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_92
    .catchall {:try_start_6a .. :try_end_92} :catchall_a6

    .line 2851
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2853
    if-eqz v8, :cond_9e

    .line 2854
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2856
    :cond_9e
    return v10

    .line 2821
    :catchall_9f
    move-exception v1

    if-eqz v9, :cond_a5

    .line 2822
    :try_start_a2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_a5
    throw v1
    :try_end_a6
    .catchall {:try_start_a2 .. :try_end_a6} :catchall_a6

    .line 2851
    :catchall_a6
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 2836
    :cond_ab
    const/4 v1, 0x7

    if-ne v10, v1, :cond_c6

    .line 2838
    :try_start_ae
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_be

    .line 2839
    const-string v1, "EsConversationsData"

    const-string v2, "giving up"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2841
    :cond_be
    const/16 v10, 0x8

    .line 2842
    const/16 v1, 0x8

    invoke-static {v0, p2, p3, v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateMessageStatus$4372adf(Landroid/database/sqlite/SQLiteDatabase;JI)V

    goto :goto_8f

    .line 2845
    :cond_c6
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8f

    .line 2846
    const-string v1, "EsConversationsData"

    const-string v2, "message sent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d6
    .catchall {:try_start_ae .. :try_end_d6} :catchall_a6

    goto :goto_8f
.end method

.method public static cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 4
    .parameter "db"

    .prologue
    .line 3525
    const-string v0, "EsConversationsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 3526
    const-string v0, "EsConversationsData"

    const-string v1, "cleanupData"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3530
    :cond_10
    const-string v0, "participants"

    const-string v1, "(SELECT COUNT(participant_id) FROM conversation_participants WHERE participants.participant_id=conversation_participants.participant_id)=0"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3538
    return-void
.end method

.method public static connectionStarted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 7
    .parameter "context"
    .parameter "account"

    .prologue
    .line 3490
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3492
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3494
    :try_start_b
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3495
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "status"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3496
    const-string v2, "messages"

    const-string v3, "status=0 OR status=1"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3500
    const-string v2, "status"

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3501
    const-string v2, "messages"

    const-string v3, "status=7"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3504
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 3505
    const-string v2, "key"

    const-string v3, "awaiting_conversation_list"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3507
    const-string v2, "value"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3509
    const-string v2, "realtimechat_metadata"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 3512
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_53
    .catchall {:try_start_b .. :try_end_53} :catchall_57

    .line 3514
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3515
    return-void

    .line 3514
    .end local v1           #values:Landroid/content/ContentValues;
    :catchall_57
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static final convertParticipantType(Lcom/google/wireless/realtimechat/proto/Data$Participant;)I
    .registers 4
    .parameter "participant"

    .prologue
    .line 5172
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasType()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 5173
    sget-object v0, Lcom/google/android/apps/plus/content/EsConversationsData$4;->$SwitchMap$com$google$wireless$realtimechat$proto$Data$Participant$Type:[I

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getType()Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_40

    .line 5183
    const-string v0, "EsConversationsData"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 5184
    const-string v0, "EsConversationsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown participant type of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getType()Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5188
    :cond_36
    const/4 v0, 0x0

    :goto_37
    return v0

    .line 5175
    :pswitch_38
    const/4 v0, 0x1

    goto :goto_37

    .line 5177
    :pswitch_3a
    const/4 v0, 0x2

    goto :goto_37

    .line 5179
    :pswitch_3c
    const/4 v0, 0x3

    goto :goto_37

    .line 5181
    :pswitch_3e
    const/4 v0, 0x4

    goto :goto_37

    .line 5173
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_38
        :pswitch_3a
        :pswitch_3c
        :pswitch_3e
    .end packed-switch
.end method

.method public static final convertParticipantType(I)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;
    .registers 2
    .parameter "typeValue"

    .prologue
    .line 5192
    packed-switch p0, :pswitch_data_12

    .line 5202
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->ANDROID:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    :goto_5
    return-object v0

    .line 5194
    :pswitch_6
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_5

    .line 5196
    :pswitch_9
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->SMS:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_5

    .line 5198
    :pswitch_c
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->ANDROID:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_5

    .line 5200
    :pswitch_f
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->IPHONE:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_5

    .line 5192
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method public static createConversationLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/String;ZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;
    .registers 35
    .parameter "context"
    .parameter "account"
    .parameter "conversation"
    .parameter "text"
    .parameter "connected"
    .parameter "operationState"

    .prologue
    .line 2375
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 2376
    const-string v5, "EsConversationsData"

    const-string v6, "createConversationLocally:"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2378
    :cond_10
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2380
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2381
    const/4 v14, 0x0

    .line 2382
    .local v14, conversationRowId:Ljava/lang/Long;
    const/16 v19, 0x0

    .line 2383
    .local v19, messageRowId:Ljava/lang/Long;
    const/16 v24, 0x0

    .line 2386
    .local v24, suggestionsChanged:Z
    :try_start_20
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getParticipantList()Ljava/util/List;

    move-result-object v21

    .line 2387
    .local v21, participants:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3c

    .line 2389
    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryOneToOneConversation$51a85815(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v14

    .line 2394
    :cond_3c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long v25, v5, v7

    .line 2396
    .local v25, timestamp:J
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "c:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x20

    invoke-static {v6}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 2398
    .local v12, clientMessageId:Ljava/lang/String;
    if-nez v14, :cond_20e

    .line 2400
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_6b

    .line 2401
    const-string v5, "EsConversationsData"

    const-string v6, "Creating new conversation"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2404
    :cond_6b
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-wide/from16 v2, v25

    invoke-static {v4, v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertConversation$2157227a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_78
    .catchall {:try_start_20 .. :try_end_78} :catchall_205

    move-result-object v14

    .line 2406
    const/16 v28, 0x0

    :try_start_7b
    const-string v5, "participants"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "participant_id"

    aput-object v8, v6, v7

    const-string v7, "participant_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_97
    .catchall {:try_start_7b .. :try_end_97} :catchall_1fc

    move-result-object v6

    :try_start_98
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_bb

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "participant_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "full_name"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "participants"

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_bb
    .catchall {:try_start_98 .. :try_end_bb} :catchall_2c0

    :cond_bb
    if-eqz v6, :cond_c0

    :try_start_bd
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2408
    :cond_c0
    if-eqz p4, :cond_20a

    .line 2409
    const/16 v23, 0x1

    .line 2410
    .local v23, status:I
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v12, v1, v5, v6}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->createConversation(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 2455
    :goto_d3
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasType()Z

    move-result v5

    if-eqz v5, :cond_106

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getType()Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    move-result-object v5

    sget-object v6, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->ONE_TO_ONE:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    if-ne v5, v6, :cond_106

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getParticipantCount()I

    move-result v5

    if-lez v5, :cond_106

    .line 2459
    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v20

    .line 2460
    .local v20, oneToOneParticipantId:Ljava/lang/String;
    const-string v5, "messenger_suggestions"

    const-string v6, "participant_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v20, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 2463
    .local v15, count:I
    if-lez v15, :cond_106

    .line 2464
    const/16 v24, 0x1

    .line 2468
    .end local v15           #count:I
    .end local v20           #oneToOneParticipantId:Ljava/lang/String;
    :cond_106
    if-eqz p3, :cond_1c7

    .line 2470
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 2471
    .local v27, values:Landroid/content/ContentValues;
    const-string v5, "message_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2472
    const-string v5, "conversation_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2473
    const-string v5, "author_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2474
    const-string v5, "text"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475
    const-string v5, "status"

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2476
    const-string v5, "type"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2477
    const-string v5, "timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2478
    const-string v5, "notification_seen"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2479
    const-string v5, "messages"

    const/4 v6, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v4, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    .line 2483
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    .line 2484
    const-string v5, "is_visible"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2485
    const-string v5, "latest_message_timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2486
    const-string v5, "latest_message_author_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2488
    const-string v5, "latest_message_text"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2489
    const-string v5, "latest_message_image_url"

    const/4 v6, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490
    const-string v5, "latest_message_type"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2493
    const-string v5, "conversations"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "_id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2498
    .end local v27           #values:Landroid/content/ContentValues;
    :cond_1c7
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1ca
    .catchall {:try_start_bd .. :try_end_1ca} :catchall_205

    .line 2500
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2502
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5, v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2503
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2504
    if-eqz v24, :cond_1e0

    .line 2505
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifySuggestionsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2508
    :cond_1e0
    new-instance v22, Landroid/os/Bundle;

    invoke-direct/range {v22 .. v22}, Landroid/os/Bundle;-><init>()V

    .line 2509
    .local v22, result:Landroid/os/Bundle;
    const-string v5, "conversation_row_id"

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2510
    const-string v5, "message_row_id"

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2511
    return-object v22

    .line 2406
    .end local v22           #result:Landroid/os/Bundle;
    .end local v23           #status:I
    :catchall_1fc
    move-exception v5

    move-object/from16 v6, v28

    :goto_1ff
    if-eqz v6, :cond_204

    :try_start_201
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_204
    throw v5
    :try_end_205
    .catchall {:try_start_201 .. :try_end_205} :catchall_205

    .line 2500
    .end local v12           #clientMessageId:Ljava/lang/String;
    .end local v21           #participants:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    .end local v25           #timestamp:J
    :catchall_205
    move-exception v5

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .line 2413
    .restart local v12       #clientMessageId:Ljava/lang/String;
    .restart local v21       #participants:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    .restart local v25       #timestamp:J
    :cond_20a
    const/16 v23, 0x2

    .restart local v23       #status:I
    goto/16 :goto_d3

    .line 2416
    .end local v23           #status:I
    :cond_20e
    :try_start_20e
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_22b

    .line 2417
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Using existing conversation "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_22b
    .catchall {:try_start_20e .. :try_end_22b} :catchall_205

    .line 2420
    :cond_22b
    const-wide/16 v16, 0x0

    .line 2421
    .local v16, currentMessageTimestamp:J
    const/16 v18, 0x0

    .line 2423
    .local v18, cursor:Landroid/database/Cursor;
    :try_start_22f
    const-string v5, "conversations"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "latest_message_timestamp"

    aput-object v8, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_id="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 2427
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_261

    .line 2428
    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J
    :try_end_260
    .catchall {:try_start_22f .. :try_end_260} :catchall_2b5

    move-result-wide v16

    .line 2431
    :cond_261
    if-eqz v18, :cond_266

    .line 2432
    :try_start_263
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 2435
    :cond_266
    cmp-long v5, v16, v25

    if-lez v5, :cond_26e

    .line 2436
    const-wide/16 v5, 0x1

    add-long v25, v16, v5

    .line 2440
    :cond_26e
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 2441
    .restart local v27       #values:Landroid/content/ContentValues;
    const-string v5, "is_pending_leave"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2442
    const-string v5, "conversations"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "_id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2445
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v13

    .line 2446
    .local v13, conversationId:Ljava/lang/String;
    if-eqz p4, :cond_2bc

    .line 2447
    const/16 v23, 0x1

    .line 2448
    .restart local v23       #status:I
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-static {v13, v12, v0, v5, v6}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    goto/16 :goto_d3

    .line 2431
    .end local v13           #conversationId:Ljava/lang/String;
    .end local v23           #status:I
    .end local v27           #values:Landroid/content/ContentValues;
    :catchall_2b5
    move-exception v5

    if-eqz v18, :cond_2bb

    .line 2432
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_2bb
    throw v5
    :try_end_2bc
    .catchall {:try_start_263 .. :try_end_2bc} :catchall_205

    .line 2451
    .restart local v13       #conversationId:Ljava/lang/String;
    .restart local v27       #values:Landroid/content/ContentValues;
    :cond_2bc
    const/16 v23, 0x2

    .restart local v23       #status:I
    goto/16 :goto_d3

    .line 2406
    .end local v13           #conversationId:Ljava/lang/String;
    .end local v16           #currentMessageTimestamp:J
    .end local v18           #cursor:Landroid/database/Cursor;
    .end local v23           #status:I
    .end local v27           #values:Landroid/content/ContentValues;
    :catchall_2c0
    move-exception v5

    goto/16 :goto_1ff
.end method

.method private static determineMessageState(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)I
    .registers 4
    .parameter "message"

    .prologue
    const/4 v0, 0x3

    .line 5212
    sget-object v1, Lcom/google/android/apps/plus/content/EsConversationsData$4;->$SwitchMap$com$google$wireless$realtimechat$proto$Client$ChatMessage$ReceiverState:[I

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getReceiverState()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$ReceiverState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$ReceiverState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_26

    .line 5222
    const-string v0, "EsConversationsData"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 5223
    const-string v0, "EsConversationsData"

    const-string v1, "ChatMessage\'s read state could not be determined."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5225
    :cond_20
    const/4 v0, 0x0

    :goto_21
    :pswitch_21
    return v0

    .line 5216
    :pswitch_22
    const/4 v0, 0x4

    goto :goto_21

    .line 5218
    :pswitch_24
    const/4 v0, 0x5

    goto :goto_21

    .line 5212
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_21
        :pswitch_22
        :pswitch_24
        :pswitch_21
    .end packed-switch
.end method

.method private static insertConversation$2157227a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J
    .registers 16
    .parameter "db"
    .parameter "account"
    .parameter "conversation"
    .parameter "latestMessageTimestamp"

    .prologue
    .line 4698
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 4699
    .local v8, values:Landroid/content/ContentValues;
    const-string v1, "is_muted"

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getMuted()Z

    move-result v0

    if-eqz v0, :cond_117

    const/4 v0, 0x1

    :goto_e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4701
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasType()Z

    move-result v0

    if-eqz v0, :cond_11a

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getType()Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    move-result-object v0

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->GROUP:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    if-ne v0, v1, :cond_11a

    const/4 v4, 0x1

    .line 4703
    .local v4, isGroup:Z
    :goto_24
    const-string v0, "is_group"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 4704
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasName()Z

    move-result v0

    if-eqz v0, :cond_11d

    .line 4705
    const-string v0, "name"

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4710
    :goto_3c
    const-string v0, "unread_count"

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getUnreadCount()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4711
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasHidden()Z

    move-result v0

    if-eqz v0, :cond_124

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getHidden()Z

    move-result v0

    if-eqz v0, :cond_124

    const/4 v7, 0x1

    .line 4712
    .local v7, isHidden:Z
    :goto_56
    const-string v1, "is_visible"

    if-eqz v7, :cond_127

    const/4 v0, 0x0

    :goto_5b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4713
    const-string v0, "is_pending_leave"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4714
    const-string v0, "is_awaiting_event_stream"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4715
    const-string v0, "latest_message_timestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4716
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasFirstEventTimestamp()Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 4717
    const-string v0, "EsConversationsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 4718
    const-string v0, "EsConversationsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "setting first event timestamp "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getFirstEventTimestamp()J

    move-result-wide v9

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4721
    :cond_a6
    const-string v0, "first_event_timestamp"

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getFirstEventTimestamp()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4724
    :cond_b3
    const/4 v6, 0x0

    .line 4725
    .local v6, inviter:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasInviter()Z

    move-result v0

    if-eqz v0, :cond_12a

    .line 4726
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getInviter()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v6

    .line 4727
    const-string v0, "EsConversationsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 4728
    const-string v0, "EsConversationsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "conversation inviter "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4735
    :cond_df
    :goto_df
    if-eqz v6, :cond_ea

    .line 4736
    const-string v0, "inviter_id"

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4738
    :cond_ea
    const-string v1, "is_pending_accept"

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasNeedsAccept()Z

    move-result v0

    if-eqz v0, :cond_13b

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getNeedsAccept()Z

    move-result v0

    if-eqz v0, :cond_13b

    const/4 v0, 0x1

    :goto_f9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4741
    const-string v0, "conversation_id"

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4742
    const-string v0, "conversations"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .local v2, localRowId:J
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 4743
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->syncParticipants(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;JZLcom/google/wireless/realtimechat/proto/Client$ClientConversation;)V

    .line 4745
    return-wide v2

    .line 4699
    .end local v2           #localRowId:J
    .end local v4           #isGroup:Z
    .end local v6           #inviter:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v7           #isHidden:Z
    :cond_117
    const/4 v0, 0x0

    goto/16 :goto_e

    .line 4701
    :cond_11a
    const/4 v4, 0x0

    goto/16 :goto_24

    .line 4708
    .restart local v4       #isGroup:Z
    :cond_11d
    const-string v0, "name"

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_3c

    .line 4711
    :cond_124
    const/4 v7, 0x0

    goto/16 :goto_56

    .line 4712
    .restart local v7       #isHidden:Z
    :cond_127
    const/4 v0, 0x1

    goto/16 :goto_5b

    .line 4731
    .restart local v6       #inviter:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_12a
    const-string v0, "EsConversationsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 4732
    const-string v0, "EsConversationsData"

    const-string v1, "no inviter"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_df

    .line 4738
    :cond_13b
    const/4 v0, 0x0

    goto :goto_f9
.end method

.method public static insertLocalPhotoLocally$341823c7(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)Landroid/os/Bundle;
    .registers 26
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "uri"

    .prologue
    .line 2624
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 2625
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "insertLocalPhotoLocally conversationRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2627
    :cond_1f
    new-instance v17, Landroid/os/Bundle;

    invoke-direct/range {v17 .. v17}, Landroid/os/Bundle;-><init>()V

    .line 2628
    .local v17, result:Landroid/os/Bundle;
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2630
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2631
    const/16 v16, 0x0

    .line 2633
    .local v16, messageRowId:Ljava/lang/Long;
    const/4 v11, 0x0

    .line 2634
    .local v11, conversationId:Ljava/lang/String;
    const/4 v12, 0x0

    .line 2635
    .local v12, conversationName:Ljava/lang/String;
    const-wide/16 v14, 0x0

    .line 2636
    .local v14, latestMessageTimestamp:J
    const/4 v13, 0x0

    .line 2638
    .local v13, cursor:Landroid/database/Cursor;
    :try_start_36
    const-string v3, "conversations"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "conversation_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "name"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "generated_name"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "latest_message_timestamp"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 2646
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_8a

    .line 2647
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2648
    const/4 v3, 0x1

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 2649
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_85

    .line 2650
    const/4 v3, 0x2

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 2652
    :cond_85
    const/4 v3, 0x3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_89
    .catchall {:try_start_36 .. :try_end_89} :catchall_176

    move-result-wide v14

    .line 2655
    :cond_8a
    if-eqz v13, :cond_8f

    .line 2656
    :try_start_8c
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 2659
    :cond_8f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v18, v3, v5

    .line 2660
    .local v18, timestamp:J
    cmp-long v3, v18, v14

    if-gtz v3, :cond_9f

    .line 2661
    const-wide/16 v3, 0x1

    add-long v18, v14, v3

    .line 2664
    :cond_9f
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "c:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x20

    invoke-static {v4}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2667
    .local v10, clientMessageId:Ljava/lang/String;
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 2668
    .local v20, values:Landroid/content/ContentValues;
    const-string v3, "latest_message_image_url"

    move-object/from16 v0, v20

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2669
    const-string v3, "latest_message_author_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2671
    const-string v3, "latest_message_timestamp"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2672
    const-string v3, "conversations"

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2675
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentValues;->clear()V

    .line 2676
    const-string v3, "message_id"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677
    const-string v3, "conversation_id"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2678
    const-string v3, "author_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679
    const-string v3, "status"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2680
    const-string v3, "type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2681
    const-string v3, "timestamp"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2682
    const-string v3, "notification_seen"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2683
    const-string v3, "image_url"

    move-object/from16 v0, v20

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684
    const-string v3, "messages"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 2686
    const-string v3, "conversation_id"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2687
    const-string v3, "conversation_name"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2688
    const-string v3, "message_row_id"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2689
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_16c
    .catchall {:try_start_8c .. :try_end_16c} :catchall_17d

    .line 2691
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2693
    invoke-static/range {p0 .. p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2694
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2695
    return-object v17

    .line 2655
    .end local v10           #clientMessageId:Ljava/lang/String;
    .end local v18           #timestamp:J
    .end local v20           #values:Landroid/content/ContentValues;
    :catchall_176
    move-exception v3

    if-eqz v13, :cond_17c

    .line 2656
    :try_start_179
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_17c
    throw v3
    :try_end_17d
    .catchall {:try_start_179 .. :try_end_17d} :catchall_17d

    .line 2691
    :catchall_17d
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private static insertSystemMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;IZLjava/lang/String;IJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J
    .registers 34
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "text"
    .parameter "messageState"
    .parameter "allowNotification"
    .parameter "senderId"
    .parameter "messageType"
    .parameter "timestamp"
    .parameter "sendReceipts"
    .parameter "operationState"

    .prologue
    .line 4406
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5b

    .line 4407
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "insertSystemMessage  text: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messageType: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p9

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " senderId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messageState: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " allowNotification: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " timestamp: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p10

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4415
    :cond_5b
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    move-wide/from16 v3, p10

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryMessageRowId(Landroid/database/sqlite/SQLiteDatabase;JJ)Ljava/lang/Long;

    move-result-object v17

    .line 4417
    .local v17, messageRowId:Ljava/lang/Long;
    if-nez v17, :cond_210

    .line 4418
    const/4 v5, 0x5

    move/from16 v0, p6

    if-eq v0, v5, :cond_83

    if-eqz p7, :cond_83

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p8

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_83

    const-wide v5, 0x4bb50894ce390L

    cmp-long v5, p10, v5

    if-gez v5, :cond_205

    :cond_83
    const/16 v18, 0x1

    .line 4424
    .local v18, notificationSeen:Z
    :goto_85
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 4425
    .local v19, values:Landroid/content/ContentValues;
    const-string v5, "conversation_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4426
    const-string v5, "author_id"

    move-object/from16 v0, v19

    move-object/from16 v1, p8

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4427
    const-string v5, "text"

    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4428
    const-string v5, "status"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4429
    const-string v5, "type"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4430
    const-string v5, "timestamp"

    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4431
    const-string v5, "notification_seen"

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 4432
    const-string v5, "messages"

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 4434
    if-eqz p7, :cond_200

    .line 4435
    const-wide/16 v15, 0x0

    .line 4436
    .local v15, latestMessageTimestamp:J
    const/4 v14, 0x0

    .line 4437
    .local v14, conversationId:Ljava/lang/String;
    const/4 v13, 0x0

    .line 4439
    .local v13, conversationCursor:Landroid/database/Cursor;
    :try_start_e8
    const-string v6, "conversations"

    const/4 v5, 0x2

    new-array v7, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "conversation_id"

    aput-object v8, v7, v5

    const/4 v5, 0x1

    const-string v8, "latest_message_timestamp"

    aput-object v8, v7, v5

    const-string v8, "_id=?"

    const/4 v5, 0x1

    new-array v9, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v5

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 4445
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_11c

    .line 4446
    const/4 v5, 0x0

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 4447
    const/4 v5, 0x1

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getLong(I)J
    :try_end_11b
    .catchall {:try_start_e8 .. :try_end_11b} :catchall_209

    move-result-wide v15

    .line 4450
    :cond_11c
    if-eqz v13, :cond_121

    .line 4451
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 4455
    :cond_121
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_14b

    .line 4456
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "new message timestamp "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p10

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " conversation latest "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide v0, v15

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4459
    :cond_14b
    cmp-long v5, p10, v15

    if-ltz v5, :cond_1b3

    .line 4461
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_15f

    .line 4462
    const-string v5, "EsConversationsData"

    const-string v6, "updating latest message"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4464
    :cond_15f
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentValues;->clear()V

    .line 4465
    const-string v5, "is_visible"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4466
    const-string v5, "latest_message_timestamp"

    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4467
    const-string v5, "latest_message_author_id"

    move-object/from16 v0, v19

    move-object/from16 v1, p8

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4468
    const-string v5, "latest_message_text"

    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4469
    const-string v5, "latest_message_image_url"

    const/4 v6, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4470
    const-string v5, "latest_message_type"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4471
    const-string v5, "conversations"

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4476
    :cond_1b3
    sget-object v5, Lcom/google/android/apps/plus/content/EsConversationsData;->sHandler:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/apps/plus/content/EsConversationsData$3;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v6, v0, v1}, Lcom/google/android/apps/plus/content/EsConversationsData$3;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4496
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p8

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_200

    if-eqz p12, :cond_200

    const/4 v5, 0x3

    move/from16 v0, p6

    if-ne v0, v5, :cond_200

    .line 4498
    sget-object v5, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->DELIVERED:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-wide/from16 v0, p10

    invoke-static {v14, v0, v1, v5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReceipt(Ljava/lang/String;JLcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v5

    move-object/from16 v0, p13

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 4500
    invoke-virtual/range {p13 .. p13}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v5

    if-eqz v5, :cond_200

    invoke-virtual/range {p13 .. p13}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v5, v5, p3

    if-nez v5, :cond_200

    .line 4502
    sget-object v5, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->READ:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-wide/from16 v0, p10

    invoke-static {v14, v0, v1, v5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReceipt(Ljava/lang/String;JLcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v5

    move-object/from16 v0, p13

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 4508
    .end local v13           #conversationCursor:Landroid/database/Cursor;
    .end local v14           #conversationId:Ljava/lang/String;
    .end local v15           #latestMessageTimestamp:J
    :cond_200
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 4511
    .end local v18           #notificationSeen:Z
    .end local v19           #values:Landroid/content/ContentValues;
    :goto_204
    return-wide v5

    .line 4418
    :cond_205
    const/16 v18, 0x0

    goto/16 :goto_85

    .line 4450
    .restart local v13       #conversationCursor:Landroid/database/Cursor;
    .restart local v14       #conversationId:Ljava/lang/String;
    .restart local v15       #latestMessageTimestamp:J
    .restart local v18       #notificationSeen:Z
    .restart local v19       #values:Landroid/content/ContentValues;
    :catchall_209
    move-exception v5

    if-eqz v13, :cond_20f

    .line 4451
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_20f
    throw v5

    .line 4511
    .end local v13           #conversationCursor:Landroid/database/Cursor;
    .end local v14           #conversationId:Ljava/lang/String;
    .end local v15           #latestMessageTimestamp:J
    .end local v18           #notificationSeen:Z
    .end local v19           #values:Landroid/content/ContentValues;
    :cond_210
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_204
.end method

.method public static inviteParticipantsLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 22
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter
    .parameter "operationState"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3364
    .local p4, participants:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 3365
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "inviteParticipantsLocally  conversationRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3368
    :cond_1f
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 3370
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3373
    :try_start_2a
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V
    :try_end_2f
    .catchall {:try_start_2a .. :try_end_2f} :catchall_77

    .line 3374
    .local v11, currentParticipants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 3376
    .local v12, cursor:Landroid/database/Cursor;
    :try_start_30
    const-string v3, "participants_view"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "participant_id"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "conversation_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND active"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 3382
    :goto_61
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_7c

    .line 3383
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_6f
    .catchall {:try_start_30 .. :try_end_6f} :catchall_70

    goto :goto_61

    .line 3386
    :catchall_70
    move-exception v3

    if-eqz v12, :cond_76

    .line 3387
    :try_start_73
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_76
    throw v3
    :try_end_77
    .catchall {:try_start_73 .. :try_end_77} :catchall_77

    .line 3410
    .end local v11           #currentParticipants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v12           #cursor:Landroid/database/Cursor;
    :catchall_77
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 3386
    .restart local v11       #currentParticipants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v12       #cursor:Landroid/database/Cursor;
    :cond_7c
    if-eqz v12, :cond_81

    .line 3387
    :try_start_7e
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 3392
    :cond_81
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 3393
    .local v14, needInvite:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, i$:Ljava/util/Iterator;
    :cond_8a
    :goto_8a
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 3394
    .local v15, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8a

    .line 3395
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_cf

    .line 3396
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "inviting "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3399
    :cond_cf
    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8a

    .line 3403
    .end local v15           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_d3
    move-wide/from16 v0, p2

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v10

    .line 3405
    .local v10, conversationId:Ljava/lang/String;
    invoke-static {v10, v14}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->inviteParticipants(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3408
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e5
    .catchall {:try_start_7e .. :try_end_e5} :catchall_77

    .line 3410
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3411
    return-void
.end method

.method public static leaveConversationLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "operationState"

    .prologue
    .line 3425
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 3426
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "leaveConversationLocally  conversationRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3429
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3431
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3433
    :try_start_28
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v0

    .line 3435
    .local v0, conversationId:Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3436
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "is_pending_leave"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3437
    const-string v3, "latest_event_timestamp"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3438
    const-string v3, "earliest_event_timestamp"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3439
    const-string v3, "EsConversationsData"

    const-string v4, "updating latest event timestamp 0"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3440
    const-string v3, "conversations"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3445
    const-string v3, "messages"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "conversation_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3449
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->leaveConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    invoke-virtual {p4, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3451
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_92
    .catchall {:try_start_28 .. :try_end_92} :catchall_99

    .line 3453
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3455
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3456
    return-void

    .line 3453
    .end local v0           #conversationId:Ljava/lang/String;
    .end local v2           #values:Landroid/content/ContentValues;
    :catchall_99
    move-exception v3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method public static markAllNotificationsAsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 7
    .parameter "context"
    .parameter "account"

    .prologue
    .line 3466
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3468
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3470
    :try_start_b
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3471
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "notification_seen"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3472
    const-string v2, "messages"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3473
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_24
    .catchall {:try_start_b .. :try_end_24} :catchall_2b

    .line 3475
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3477
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatNotifications;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3478
    return-void

    .line 3475
    .end local v1           #values:Landroid/content/ContentValues;
    :catchall_2b
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static markConversationReadLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 22
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "operationState"

    .prologue
    .line 2869
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 2870
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "markConversationReadLocally conversationRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2872
    :cond_1f
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2874
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2876
    :try_start_2a
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 2877
    .local v16, values:Landroid/content/ContentValues;
    const-string v3, "notification_seen"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2878
    const-string v3, "messages"

    const-string v4, "conversation_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2882
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentValues;->clear()V

    .line 2883
    const-string v3, "unread_count"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2884
    const-string v3, "conversations"

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2888
    move-wide/from16 v0, p2

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;
    :try_end_75
    .catchall {:try_start_2a .. :try_end_75} :catchall_107

    move-result-object v10

    .line 2890
    .local v10, conversationId:Ljava/lang/String;
    const/4 v11, 0x0

    .line 2892
    .local v11, cursor:Landroid/database/Cursor;
    :try_start_77
    const-string v3, "messages_view"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "timestamp"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "status"

    aput-object v6, v4, v5

    const-string v5, "(status=? OR status=?) AND author_id!=? AND conversation_id=?"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x4

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2905
    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    .line 2906
    .local v15, timestamps:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    :cond_b5
    :goto_b5
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_10c

    .line 2907
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_dd

    .line 2908
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sending read receipt "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-interface {v11, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2910
    :cond_dd
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 2911
    .local v13, timestamp:J
    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 2913
    .local v12, status:I
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2914
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->setShouldTriggerNotifications()V

    .line 2916
    const/4 v3, 0x4

    if-eq v12, v3, :cond_b5

    .line 2919
    sget-object v3, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->DELIVERED:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    invoke-static {v10, v13, v14, v3}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReceipt(Ljava/lang/String;JLcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_ff
    .catchall {:try_start_77 .. :try_end_ff} :catchall_100

    goto :goto_b5

    .line 2929
    .end local v12           #status:I
    .end local v13           #timestamp:J
    .end local v15           #timestamps:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    :catchall_100
    move-exception v3

    if-eqz v11, :cond_106

    .line 2930
    :try_start_103
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_106
    throw v3
    :try_end_107
    .catchall {:try_start_103 .. :try_end_107} :catchall_107

    .line 2936
    .end local v10           #conversationId:Ljava/lang/String;
    .end local v11           #cursor:Landroid/database/Cursor;
    .end local v16           #values:Landroid/content/ContentValues;
    :catchall_107
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 2923
    .restart local v10       #conversationId:Ljava/lang/String;
    .restart local v11       #cursor:Landroid/database/Cursor;
    .restart local v15       #timestamps:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v16       #values:Landroid/content/ContentValues;
    :cond_10c
    :try_start_10c
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_11b

    .line 2924
    invoke-static {v10, v15}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReadReceipts(Ljava/lang/String;Ljava/util/List;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_11b
    .catchall {:try_start_10c .. :try_end_11b} :catchall_100

    .line 2929
    :cond_11b
    if-eqz v11, :cond_120

    .line 2930
    :try_start_11d
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2934
    :cond_120
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_123
    .catchall {:try_start_11d .. :try_end_123} :catchall_107

    .line 2936
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2939
    invoke-static/range {p0 .. p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2940
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2941
    return-void
.end method

.method public static markNotificationsSeenLocally$785b8fa1(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    .line 2953
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2954
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "markNotificationsSeenLocally conversationRowId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2958
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2960
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2962
    :try_start_28
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2963
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "notification_seen"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2964
    const-string v2, "messages"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2968
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4b
    .catchall {:try_start_28 .. :try_end_4b} :catchall_52

    .line 2970
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2972
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2973
    return-void

    .line 2970
    .end local v1           #values:Landroid/content/ContentValues;
    :catchall_52
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private static notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 5143
    const-string v0, "EsConversationsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 5144
    const-string v0, "EsConversationsData"

    const-string v1, "NOTIFY CONVERSATIONS"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5146
    :cond_10
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5148
    return-void
.end method

.method private static notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    const/4 v3, 0x0

    .line 5127
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 5128
    const-string v1, "EsConversationsData"

    const-string v2, "NOTIFY MESSAGES"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5130
    :cond_11
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5132
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/plus/content/EsProvider;->buildMessagesUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v0

    .line 5133
    .local v0, messagesUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5134
    return-void
.end method

.method private static notifyParticipantsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    const/4 v3, 0x0

    .line 5109
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 5110
    const-string v1, "EsConversationsData"

    const-string v2, "NOTIFY PARTICIPANTS"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5112
    :cond_11
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5114
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/plus/content/EsProvider;->buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v0

    .line 5115
    .local v0, participantsUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5116
    return-void
.end method

.method private static notifySuggestionsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 5157
    const-string v0, "EsConversationsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 5158
    const-string v0, "EsConversationsData"

    const-string v1, "NOTIFY SUGGESTIONS"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5160
    :cond_10
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_URI:Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5163
    return-void
.end method

.method public static processBunchServerUpdate(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 42
    .parameter "context"
    .parameter "account"
    .parameter "update"
    .parameter "operationState"

    .prologue
    .line 222
    const/16 v26, 0x0

    .line 223
    .local v26, conversationRowId:Ljava/lang/Long;
    const/16 v31, 0x0

    .line 224
    .local v31, messagesChanged:Z
    const/16 v32, 0x0

    .line 225
    .local v32, participantsChanged:Z
    const/16 v27, 0x0

    .line 227
    .local v27, conversationsChanged:Z
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v28

    .line 229
    .local v28, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 231
    :try_start_13
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasEventMetadata()Z

    move-result v4

    if-eqz v4, :cond_65

    .line 235
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getEventMetadata()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getConversationId()Ljava/lang/String;

    move-result-object v25

    .line 236
    .local v25, conversationId:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getEventMetadata()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getEventTimestamp()J

    move-result-wide v35

    .line 237
    .local v35, timestamp:J
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getEventMetadata()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 239
    if-eqz v26, :cond_65

    .line 240
    sget-object v4, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->DELIVERED:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-object/from16 v0, v25

    move-wide/from16 v1, v35

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReceipt(Ljava/lang/String;JLcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 242
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_65

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v26

    if-ne v4, v0, :cond_65

    .line 244
    sget-object v4, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->READ:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-object/from16 v0, v25

    move-wide/from16 v1, v35

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReceipt(Ljava/lang/String;JLcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 250
    .end local v25           #conversationId:Ljava/lang/String;
    .end local v35           #timestamp:J
    :cond_65
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasInvalidateLocalCache()Z

    move-result v4

    if-eqz v4, :cond_18e

    .line 251
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getInvalidateLocalCache()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v5

    const-string v4, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_90

    const-string v4, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "processInvalidateLocalCache version: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getVersion()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_90
    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->hasVersion()Z

    move-result v4

    if-eqz v4, :cond_17c

    invoke-static/range {v28 .. v28}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryDatastoreVersion(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;
    :try_end_99
    .catchall {:try_start_13 .. :try_end_99} :catchall_177

    move-result-object v6

    const/4 v4, -0x1

    if-eqz v6, :cond_a1

    :try_start_9d
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_a0
    .catchall {:try_start_9d .. :try_end_a0} :catchall_177
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_a0} :catch_131

    move-result v4

    :cond_a1
    :goto_a1
    :try_start_a1
    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getVersion()I

    move-result v7

    if-ge v4, v7, :cond_145

    const-string v4, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_c8

    const-string v4, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Clearing local storage. New storage version = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getVersion()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c8
    const-string v4, "conversations"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getVersion()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "key"

    const-string v8, "datastore_version"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "value"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "realtimechat_metadata"

    const/4 v7, 0x0

    const/4 v8, 0x5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v7, v6, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getVersion()I

    move-result v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->setClientVersion(I)V

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationList(J)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 253
    :cond_107
    :goto_107
    const/16 v27, 0x1

    .line 401
    :cond_109
    :goto_109
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_10c
    .catchall {:try_start_a1 .. :try_end_10c} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 406
    if-eqz v26, :cond_130

    .line 407
    if-eqz v27, :cond_116

    .line 408
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 410
    :cond_116
    if-eqz v32, :cond_123

    .line 411
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyParticipantsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 413
    :cond_123
    if-eqz v31, :cond_130

    .line 414
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 417
    :cond_130
    :goto_130
    return-void

    .line 251
    :catch_131
    move-exception v4

    :try_start_132
    const-string v4, "EsConversationsData"

    const/4 v7, 0x5

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_142

    const-string v4, "EsConversationsData"

    const-string v7, "Failed to parse database version"

    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_142
    const/4 v4, -0x1

    goto/16 :goto_a1

    :cond_145
    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getVersion()I

    move-result v7

    if-le v4, v7, :cond_107

    const-string v4, "EsConversationsData"

    const/4 v7, 0x5

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_107

    const-string v4, "EsConversationsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Ignoring InvalidateLocalCache message! Local version = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ". Server version = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getVersion()I

    move-result v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_176
    .catchall {:try_start_132 .. :try_end_176} :catchall_177

    goto :goto_107

    .line 403
    :catchall_177
    move-exception v4

    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    .line 251
    :cond_17c
    :try_start_17c
    const-string v4, "EsConversationsData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_107

    const-string v4, "EsConversationsData"

    const-string v5, "Ignoring InvalidateLocalCache message without version number."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_107

    .line 254
    :cond_18e
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasMigration()Z

    move-result v4

    if-eqz v4, :cond_216

    .line 255
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v4

    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1a8

    const-string v5, "EsConversationsData"

    const-string v6, "processMigration"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a8
    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getOldId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getNewId()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "participant_id"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "conversation_participants"

    const-string v8, "participant_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    move-object/from16 v0, v28

    invoke-virtual {v0, v7, v6, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "author_id"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "messages"

    const-string v8, "author_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    move-object/from16 v0, v28

    invoke-virtual {v0, v7, v6, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "latest_message_author_id"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "conversations"

    const-string v8, "latest_message_author_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    move-object/from16 v0, v28

    invoke-virtual {v0, v7, v6, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "inviter_id"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "conversations"

    const-string v7, "inviter_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v5, v8, v10

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_109

    .line 257
    :cond_216
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasNewConversation()Z

    move-result v4

    if-eqz v4, :cond_2bf

    .line 258
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getNewConversation()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v4

    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_245

    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "processNewConversation  conversationId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->getClientConversation()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_245
    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->getClientConversation()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v10

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-static {v0, v1, v2, v10, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->processConversation(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "is_awaiting_event_stream"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "conversations"

    const-string v6, "conversation_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v10}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v7, v8

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v10}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const/16 v9, 0x14

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getEventStream(Ljava/lang/String;JJI)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2af

    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "requesting 20 events since 0 for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2af
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    .line 260
    const/16 v27, 0x1

    .line 261
    const/16 v31, 0x1

    .line 262
    const/16 v32, 0x1

    goto/16 :goto_109

    .line 263
    :cond_2bf
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasChatMessage()Z

    move-result v4

    if-eqz v4, :cond_40e

    .line 264
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v30

    .line 265
    .local v30, message:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 267
    if-nez v26, :cond_31c

    .line 268
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_30a

    .line 269
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received message ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] for a nonexistant conversation id ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_30a
    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_317
    .catchall {:try_start_17c .. :try_end_317} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_130

    .line 278
    :cond_31c
    :try_start_31c
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_36b

    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processSingleMessage messageClientId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getMessageClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " conversationId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " timestamp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " receiverState: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getReceiverState()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$ReceiverState;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_36b
    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getConversationId()Ljava/lang/String;

    move-result-object v14

    invoke-static/range {v30 .. v30}, Lcom/google/android/apps/plus/content/EsConversationsData;->determineMessageState(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)I

    move-result v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v5

    if-eqz v5, :cond_385

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v5, v16, v5

    if-eqz v5, :cond_3ef

    :cond_385
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_390
    .catchall {:try_start_31c .. :try_end_390} :catchall_177

    move-result v5

    if-nez v5, :cond_3ef

    const/4 v5, 0x3

    if-eq v4, v5, :cond_399

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3ef

    :cond_399
    const/4 v12, 0x0

    const/4 v13, 0x0

    :try_start_39b
    const-string v5, "conversations"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "unread_count"

    aput-object v7, v6, v4

    const-string v7, "conversation_id=?"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v14, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, v28

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3b5
    .catchall {:try_start_39b .. :try_end_3b5} :catchall_406

    move-result-object v5

    :try_start_3b6
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_79e

    const/4 v4, 0x0

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_3c0
    .catchall {:try_start_3b6 .. :try_end_3c0} :catchall_79b

    move-result v4

    :goto_3c1
    if-eqz v5, :cond_3c6

    :try_start_3c3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_3c6
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "unread_count"

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "is_visible"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "conversations"

    const-string v6, "conversation_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v14, v7, v8

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3ef
    const/4 v11, 0x0

    move-object/from16 v4, v28

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, v30

    move-object v8, v14

    move-wide/from16 v9, v16

    move-object/from16 v12, p3

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/plus/content/EsConversationsData;->processMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;Ljava/lang/String;JZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    .line 280
    const/16 v27, 0x1

    .line 281
    const/16 v31, 0x1

    .line 282
    goto/16 :goto_109

    .line 278
    :catchall_406
    move-exception v4

    move-object v5, v12

    :goto_408
    if-eqz v5, :cond_40d

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_40d
    throw v4

    .line 282
    .end local v30           #message:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    :cond_40e
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasMembershipChange()Z

    move-result v4

    if-eqz v4, :cond_484

    .line 283
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v9

    .line 284
    .local v9, change:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 286
    if-nez v26, :cond_46b

    .line 287
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_459

    .line 288
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received membershipChange ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] for a nonexistant conversation id ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_459
    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_466
    .catchall {:try_start_3c3 .. :try_end_466} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_130

    .line 297
    :cond_46b
    :try_start_46b
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const/4 v10, 0x3

    const/4 v11, 0x0

    move-object/from16 v4, v28

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v12, p3

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/plus/content/EsConversationsData;->processMembershipChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$MembershipChange;IZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    .line 300
    const/16 v27, 0x1

    .line 301
    const/16 v31, 0x1

    .line 302
    const/16 v32, 0x1

    .line 303
    goto/16 :goto_109

    .end local v9           #change:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    :cond_484
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasGroupConversationRename()Z

    move-result v4

    if-eqz v4, :cond_4fc

    .line 304
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v15

    .line 305
    .local v15, rename:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 307
    if-nez v26, :cond_4e1

    .line 308
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4cf

    .line 309
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received rename ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] for a nonexistant conversation id ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_4cf
    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_4dc
    .catchall {:try_start_46b .. :try_end_4dc} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_130

    .line 318
    :cond_4e1
    :try_start_4e1
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    const/16 v16, 0x1

    const/16 v17, 0x3

    const/16 v18, 0x0

    move-object/from16 v10, v28

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v19, p3

    invoke-static/range {v10 .. v19}, Lcom/google/android/apps/plus/content/EsConversationsData;->processGroupConversationRename(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;ZIZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    .line 321
    const/16 v27, 0x1

    .line 322
    const/16 v31, 0x1

    .line 323
    goto/16 :goto_109

    .end local v15           #rename:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    :cond_4fc
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasTileEvent()Z

    move-result v4

    if-eqz v4, :cond_576

    .line 324
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v29

    .line 325
    .local v29, event:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 327
    if-nez v26, :cond_559

    .line 328
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_547

    .line 329
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received TileEvent ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] for a nonexistant conversation id ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :cond_547
    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_554
    .catchall {:try_start_4e1 .. :try_end_554} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_130

    .line 338
    :cond_559
    :try_start_559
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v21

    const/16 v22, 0x3

    const/16 v23, 0x0

    move-object/from16 v16, v28

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    move-object/from16 v24, p3

    invoke-static/range {v16 .. v24}, Lcom/google/android/apps/plus/content/EsConversationsData;->processTileEvent(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$TileEvent;IZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    .line 341
    const/16 v27, 0x1

    .line 342
    const/16 v31, 0x1

    .line 343
    goto/16 :goto_109

    .end local v29           #event:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    :cond_576
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasPresence()Z

    move-result v4

    if-eqz v4, :cond_671

    .line 344
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getPresence()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v33

    .line 345
    .local v33, presence:Lcom/google/wireless/realtimechat/proto/Client$Presence;
    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 347
    if-nez v26, :cond_5d3

    .line 348
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5c1

    .line 349
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received presence ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] for a nonexistant conversation id ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_5c1
    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_5ce
    .catchall {:try_start_559 .. :try_end_5ce} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_130

    .line 358
    :cond_5d3
    :try_start_5d3
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-string v4, "EsConversationsData"

    const/4 v7, 0x3

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5f8

    const-string v4, "EsConversationsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "processPresence conversationId: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getConversationId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5f8
    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->hasConversationId()Z

    move-result v4

    if-nez v4, :cond_610

    const-string v4, "EsConversationsData"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_109

    const-string v4, "EsConversationsData"

    const-string v5, "Received a Presence message without conversation id"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_109

    :cond_610
    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->hasReciprocate()Z

    move-result v8

    if-eqz v8, :cond_64f

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getReciprocate()Z

    move-result v8

    if-eqz v8, :cond_64f

    if-eqz v7, :cond_64f

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const-wide/16 v10, 0x0

    cmp-long v7, v7, v10

    if-eqz v7, :cond_64f

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object/from16 v0, v28

    invoke-static {v0, v7, v8}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_64f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->presenceRequest(Ljava/lang/String;ZZ)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    :cond_64f
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getSenderId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_109

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getSenderId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v33 .. v33}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getType()Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    move-result-object v4

    sget-object v8, Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;->FOCUS:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    if-ne v4, v8, :cond_66f

    const/4 v4, 0x1

    :goto_66a
    invoke-static {v5, v6, v7, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->notifyUserPresenceChanged(JLjava/lang/String;Z)V

    goto/16 :goto_109

    :cond_66f
    const/4 v4, 0x0

    goto :goto_66a

    .line 360
    .end local v33           #presence:Lcom/google/wireless/realtimechat/proto/Client$Presence;
    :cond_671
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasTyping()Z

    move-result v4

    if-eqz v4, :cond_71d

    .line 361
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getTyping()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v37

    .line 362
    .local v37, typing:Lcom/google/wireless/realtimechat/proto/Client$Typing;
    invoke-virtual/range {v37 .. v37}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 364
    if-nez v26, :cond_6ce

    .line 365
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6bc

    .line 366
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received typing ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v37 .. v37}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] for a nonexistant conversation id ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v37 .. v37}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_6bc
    invoke-virtual/range {v37 .. v37}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_6c9
    .catchall {:try_start_5d3 .. :try_end_6c9} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_130

    .line 375
    :cond_6ce
    :try_start_6ce
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getTyping()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v4

    const-string v7, "EsConversationsData"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_6f7

    const-string v7, "EsConversationsData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "processTyping conversationId: "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getConversationId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6f7
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getSenderId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_109

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getConversationId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getSenderId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getType()Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    move-result-object v4

    sget-object v10, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    if-ne v4, v10, :cond_71b

    const/4 v4, 0x1

    :goto_716
    invoke-static {v5, v6, v7, v8, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->notifyUserTypingStatusChanged(JLjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_109

    :cond_71b
    const/4 v4, 0x0

    goto :goto_716

    .line 377
    .end local v37           #typing:Lcom/google/wireless/realtimechat/proto/Client$Typing;
    :cond_71d
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasReceipt()Z

    move-result v4

    if-eqz v4, :cond_789

    .line 378
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v34

    .line 379
    .local v34, receipt:Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    invoke-virtual/range {v34 .. v34}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getConversationId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    .line 381
    if-nez v26, :cond_77a

    .line 382
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_768

    .line 383
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "received receipt ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v34 .. v34}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] for a nonexistant conversation id ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v34 .. v34}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_768
    invoke-virtual/range {v34 .. v34}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_775
    .catchall {:try_start_6ce .. :try_end_775} :catchall_177

    .line 403
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_130

    .line 392
    :cond_77a
    :try_start_77a
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-static {v0, v4, v5, v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->processReceipt$4fede216(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/wireless/realtimechat/proto/Client$Receipt;)V

    .line 394
    const/16 v31, 0x1

    .line 395
    goto/16 :goto_109

    .line 396
    .end local v34           #receipt:Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    :cond_789
    const-string v4, "EsConversationsData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_109

    .line 397
    const-string v4, "EsConversationsData"

    const-string v5, "Unexpected update from bunch server"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_799
    .catchall {:try_start_77a .. :try_end_799} :catchall_177

    goto/16 :goto_109

    .line 278
    .restart local v30       #message:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    :catchall_79b
    move-exception v4

    goto/16 :goto_408

    :cond_79e
    move v4, v13

    goto/16 :goto_3c1
.end method

.method public static processChatMessageResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "response"
    .parameter "operationState"

    .prologue
    const/4 v4, 0x3

    .line 883
    const/4 v0, 0x0

    .line 884
    .local v0, clientMessageId:Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->hasMessageClientId()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 885
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getMessageClientId()Ljava/lang/String;

    move-result-object v0

    .line 887
    :cond_c
    const-string v3, "EsConversationsData"

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_44

    .line 888
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "processChatMessageResponse ConversationId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getConversationId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " clientMessageId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Timestamp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getTimestamp()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    :cond_44
    const/4 v1, 0x0

    .line 894
    .local v1, conversationRowId:Ljava/lang/Long;
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 896
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 898
    :try_start_50
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getConversationId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 900
    if-nez v1, :cond_90

    .line 901
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_81

    .line 902
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "attempt to process ChatMessageResponse for a nonexistant conversation id ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getConversationId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    :cond_81
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getConversationId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_8c
    .catchall {:try_start_50 .. :try_end_8c} :catchall_a8

    .line 914
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 918
    :goto_8f
    return-void

    .line 910
    :cond_90
    :try_start_90
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4, p2}, Lcom/google/android/apps/plus/content/EsConversationsData;->processChatMessageResponse$7be9be32(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)V

    .line 912
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_9a
    .catchall {:try_start_90 .. :try_end_9a} :catchall_a8

    .line 914
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 916
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 917
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {p0, p1, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    goto :goto_8f

    .line 914
    :catchall_a8
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private static processChatMessageResponse$7be9be32(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)V
    .registers 20
    .parameter "db"
    .parameter "conversationRowId"
    .parameter "response"

    .prologue
    .line 1666
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getMessageClientId()Ljava/lang/String;

    move-result-object v10

    .line 1667
    .local v10, clientMessageId:Ljava/lang/String;
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 1668
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processChatMessageResponse Conversation row id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", clientMessageId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Timestamp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getTimestamp()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1672
    :cond_3b
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1674
    .local v15, values:Landroid/content/ContentValues;
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getTimestamp()J

    move-result-wide v13

    .line 1675
    .local v13, timestamp:J
    const/4 v11, 0x0

    .line 1676
    .local v11, cursor:Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 1678
    .local v12, originalTimestamp:Ljava/lang/Long;
    :try_start_46
    const-string v3, "messages"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "timestamp"

    aput-object v5, v4, v2

    const-string v5, "conversation_id=? AND message_id=?"

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    aput-object v10, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1685
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_77

    .line 1686
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_76
    .catchall {:try_start_46 .. :try_end_76} :catchall_10e

    move-result-object v12

    .line 1689
    :cond_77
    if-eqz v11, :cond_7c

    .line 1690
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1694
    :cond_7c
    const-string v2, "status"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1695
    if-eqz v10, :cond_ab

    .line 1696
    const-string v2, "timestamp"

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1697
    const-string v2, "messages"

    const-string v3, "message_id=? AND conversation_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1715
    :cond_ab
    :try_start_ab
    const-string v3, "messages"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const-string v5, "conversation_id=? AND (status=1 OR status=7) AND timestamp>? AND timestamp<?"

    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x2

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "timestamp ASC"

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1728
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 1729
    :goto_dc
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_115

    .line 1730
    const-wide/16 v2, 0x1

    add-long/2addr v13, v2

    .line 1731
    const-string v2, "timestamp"

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1732
    const-string v2, "messages"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_106
    .catchall {:try_start_ab .. :try_end_106} :catchall_107

    goto :goto_dc

    .line 1737
    :catchall_107
    move-exception v2

    if-eqz v11, :cond_10d

    .line 1738
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_10d
    throw v2

    .line 1689
    :catchall_10e
    move-exception v2

    if-eqz v11, :cond_114

    .line 1690
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_114
    throw v2

    .line 1737
    :cond_115
    if-eqz v11, :cond_11a

    .line 1738
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1757
    :cond_11a
    return-void
.end method

.method private static processConversation(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J
    .registers 41
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "conversation"
    .parameter "operationState"

    .prologue
    .line 1878
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v23

    .line 1879
    .local v23, conversationId:Ljava/lang/String;
    const/16 v22, 0x0

    .line 1880
    .local v22, clientConversationId:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasConversationClientId()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 1881
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getConversationClientId()Ljava/lang/String;

    move-result-object v22

    .line 1882
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateConversationId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1884
    :cond_19
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8e

    .line 1885
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "processConversation conversationId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " conversationClientId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getConversationClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " participantCount: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getParticipantCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " isPendingAccept: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getNeedsAccept()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " hidden: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getHidden()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " inactiveParticipantCount: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getInactiveParticipantCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " unreadCount: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getUnreadCount()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1894
    :cond_8e
    const/16 v25, 0x0

    .line 1895
    .local v25, cursor:Landroid/database/Cursor;
    const/16 v24, 0x0

    .line 1896
    .local v24, conversationRowId:Ljava/lang/Long;
    const/16 v28, 0x0

    .line 1898
    .local v28, isPendingLeave:Z
    :try_start_94
    const-string v6, "conversations"

    const/4 v5, 0x2

    new-array v7, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v7, v5

    const/4 v5, 0x1

    const-string v10, "is_pending_leave"

    aput-object v10, v7, v5

    const-string v8, "conversation_id=?"

    const/4 v5, 0x1

    new-array v9, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v23, v9, v5

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 1904
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_d9

    .line 1905
    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    .line 1906
    const/4 v5, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_ee

    const/4 v5, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_d4
    .catchall {:try_start_94 .. :try_end_d4} :catchall_f1

    move-result v5

    if-eqz v5, :cond_ee

    const/16 v28, 0x1

    .line 1909
    :cond_d9
    :goto_d9
    if-eqz v25, :cond_de

    .line 1910
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 1913
    :cond_de
    if-eqz v28, :cond_f8

    .line 1917
    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->leaveConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 1918
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 2055
    :goto_ed
    return-wide v5

    .line 1906
    :cond_ee
    const/16 v28, 0x0

    goto :goto_d9

    .line 1909
    :catchall_f1
    move-exception v5

    if-eqz v25, :cond_f7

    .line 1910
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_f7
    throw v5

    .line 1920
    :cond_f8
    if-nez v24, :cond_207

    .line 1922
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_10a

    .line 1923
    const-string v5, "EsConversationsData"

    const-string v6, " creating conversation"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1925
    :cond_10a
    const-wide/16 v33, 0x0

    .line 1926
    .local v33, timestamp:J
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasLastPreviewEvent()Z

    move-result v5

    if-eqz v5, :cond_13f

    .line 1927
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTimestamp()J

    move-result-wide v33

    .line 1928
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_13f

    .line 1929
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "last preview event timestamp "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTimestamp()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1933
    :cond_13f
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-wide/from16 v3, v33

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertConversation$2157227a(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    .line 1943
    .end local v33           #timestamp:J
    :goto_14f
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasLastPreviewEvent()Z

    move-result v5

    if-eqz v5, :cond_180

    .line 1944
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v26

    .line 1945
    .local v26, event:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasChatMessage()Z

    move-result v5

    if-eqz v5, :cond_376

    .line 1946
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_16f

    .line 1947
    const-string v5, "EsConversationsData"

    const-string v6, "Got ChatMessage"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1949
    :cond_16f
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v21

    .line 1950
    .local v21, chatMessage:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2, v5, v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->processPreviewMessage$43824df1(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;Ljava/lang/String;J)V

    .line 2029
    .end local v21           #chatMessage:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .end local v26           #event:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    :cond_180
    :goto_180
    :try_start_180
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const/4 v7, 0x3

    new-array v12, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v10, "message_id"

    aput-object v10, v12, v7

    const/4 v7, 0x1

    const-string v10, "text"

    aput-object v10, v12, v7

    const/4 v7, 0x2

    const-string v10, "image_url"

    aput-object v10, v12, v7

    const-string v11, "messages"

    const-string v13, "status=0 AND conversation_id=?"

    const/4 v7, 0x1

    new-array v14, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v14, v7

    const/4 v15, 0x0

    const/16 v16, 0x0

    const-string v17, "timestamp ASC"

    const/16 v18, 0x0

    move-object/from16 v10, p0

    invoke-virtual/range {v10 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 2034
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1d8

    .line 2035
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sending "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " pending messages"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2037
    :cond_1d8
    :goto_1d8
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_532

    .line 2038
    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    move-object/from16 v0, v23

    invoke-static {v0, v5, v6, v7, v10}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_1ff
    .catchall {:try_start_180 .. :try_end_1ff} :catchall_200

    goto :goto_1d8

    .line 2043
    :catchall_200
    move-exception v5

    if-eqz v25, :cond_206

    .line 2044
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_206
    throw v5

    .line 1936
    :cond_207
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_226

    .line 1937
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " updating conversation "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1939
    :cond_226
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_27b

    const-string v6, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v10, "updateConversation Conversation id: "

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", Muted: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getMuted()Z

    move-result v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", UnreadCount: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getUnreadCount()J

    move-result-wide v10

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", Name: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasName()Z

    move-result v5

    if-eqz v5, :cond_354

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getName()Ljava/lang/String;

    move-result-object v5

    :goto_270
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_27b
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "is_muted"

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getMuted()Z

    move-result v5

    if-eqz v5, :cond_358

    const/4 v5, 0x1

    :goto_289
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasId()Z

    move-result v5

    if-eqz v5, :cond_35b

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v5

    const-string v6, "g"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    :goto_2a0
    const-string v5, "is_group"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v10, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasName()Z

    move-result v5

    if-eqz v5, :cond_36c

    const-string v5, "name"

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2b8
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getUnreadCount()J

    move-result-wide v5

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v11

    if-eqz v11, :cond_2d0

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    cmp-long v11, v11, v7

    if-nez v11, :cond_2d0

    const-wide/16 v5, 0x0

    :cond_2d0
    const-string v11, "unread_count"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v10, v11, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "conversation_id"

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "is_visible"

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasHidden()Z

    move-result v5

    if-eqz v5, :cond_373

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getHidden()Z

    move-result v5

    if-eqz v5, :cond_373

    const/4 v5, 0x0

    :goto_2f1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "is_awaiting_event_stream"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v10, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasFirstEventTimestamp()Z

    move-result v5

    if-eqz v5, :cond_336

    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_329

    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v11, "setting first event timestamp "

    invoke-direct {v6, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getFirstEventTimestamp()J

    move-result-wide v11

    invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_329
    const-string v5, "first_event_timestamp"

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getFirstEventTimestamp()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v10, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_336
    const-string v5, "conversations"

    const-string v6, "_id=?"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10, v6, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move-object/from16 v10, p3

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/plus/content/EsConversationsData;->syncParticipants(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;JZLcom/google/wireless/realtimechat/proto/Client$ClientConversation;)V

    goto/16 :goto_14f

    :cond_354
    const-string v5, ""

    goto/16 :goto_270

    :cond_358
    const/4 v5, 0x0

    goto/16 :goto_289

    :cond_35b
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getParticipantList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_369

    const/4 v9, 0x1

    goto/16 :goto_2a0

    :cond_369
    const/4 v9, 0x0

    goto/16 :goto_2a0

    :cond_36c
    const-string v5, "name"

    invoke-virtual {v10, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2b8

    :cond_373
    const/4 v5, 0x1

    goto/16 :goto_2f1

    .line 1952
    .restart local v26       #event:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    :cond_376
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasMembershipChange()Z

    move-result v5

    if-eqz v5, :cond_413

    .line 1953
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_38c

    .line 1954
    const-string v5, "EsConversationsData"

    const-string v6, "Got MembershipChange"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1956
    :cond_38c
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v20

    .line 1957
    .local v20, change:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getParticipantList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .local v27, i$:Ljava/util/Iterator;
    :cond_398
    :goto_398
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_180

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 1958
    .local v30, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasParticipantId()Z

    move-result v5

    if-eqz v5, :cond_398

    .line 1959
    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getType()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Type;

    move-result-object v5

    sget-object v6, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Type;->JOIN:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Type;

    if-ne v5, v6, :cond_398

    .line 1962
    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getSenderId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryParticipantFullName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1964
    .local v19, actorName:Ljava/lang/String;
    if-eqz v19, :cond_398

    .line 1965
    const v5, 0x7f0801fd

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "<b>"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</b>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    const/4 v7, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "<b>"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v30 .. v30}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</b>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1970
    .local v8, text:Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getSenderId()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual/range {v20 .. v20}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getTimestamp()J

    move-result-wide v11

    move-object/from16 v5, p0

    invoke-static/range {v5 .. v12}, Lcom/google/android/apps/plus/content/EsConversationsData;->updatePreviewSystemMessage$5091a27(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;IJ)V

    goto :goto_398

    .line 1977
    .end local v8           #text:Ljava/lang/String;
    .end local v19           #actorName:Ljava/lang/String;
    .end local v20           #change:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    .end local v27           #i$:Ljava/util/Iterator;
    .end local v30           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_413
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasGroupConversationRename()Z

    move-result v5

    if-eqz v5, :cond_48f

    .line 1978
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_429

    .line 1979
    const-string v5, "EsConversationsData"

    const-string v6, "Got GroupConversationRename"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1981
    :cond_429
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v31

    .line 1982
    .local v31, rename:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    invoke-virtual/range {v31 .. v31}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getSenderId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryParticipantFullName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1983
    .restart local v19       #actorName:Ljava/lang/String;
    if-eqz v19, :cond_180

    .line 1984
    const v5, 0x7f0801fb

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "<b>"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</b>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    const/4 v7, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "<i>"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getNewDisplayName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</i>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1989
    .restart local v8       #text:Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual/range {v31 .. v31}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getSenderId()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual/range {v31 .. v31}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getTimestamp()J

    move-result-wide v11

    move-object/from16 v5, p0

    invoke-static/range {v5 .. v12}, Lcom/google/android/apps/plus/content/EsConversationsData;->updatePreviewSystemMessage$5091a27(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;IJ)V

    goto/16 :goto_180

    .line 1994
    .end local v8           #text:Ljava/lang/String;
    .end local v19           #actorName:Ljava/lang/String;
    .end local v31           #rename:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    :cond_48f
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasTileEvent()Z

    move-result v5

    if-eqz v5, :cond_180

    .line 1995
    const-string v5, "EsConversationsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4a5

    .line 1996
    const-string v5, "EsConversationsData"

    const-string v6, "Got TileEvent"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1998
    :cond_4a5
    invoke-virtual/range {v26 .. v26}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v32

    .line 1999
    .local v32, tileEvent:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    invoke-virtual/range {v32 .. v32}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getEventType()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JOIN_HANGOUT"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_180

    .line 2000
    const/4 v9, 0x0

    .line 2001
    .local v9, authorId:Ljava/lang/String;
    invoke-virtual/range {v32 .. v32}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getEventDataList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .restart local v27       #i$:Ljava/util/Iterator;
    :cond_4be
    :goto_4be
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_50b

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;

    .line 2002
    .local v29, pair:Lcom/google/wireless/realtimechat/proto/Data$KeyValue;
    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getKey()Ljava/lang/String;

    move-result-object v5

    const-string v6, "AUTHOR_PROFILE_ID"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4db

    .line 2003
    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getValue()Ljava/lang/String;

    move-result-object v9

    goto :goto_4be

    .line 2005
    :cond_4db
    const-string v5, "EsConversationsData"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4be

    .line 2006
    const-string v5, "EsConversationsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Got unexpected data in join hangout tile event: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v29 .. v29}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4be

    .line 2011
    .end local v29           #pair:Lcom/google/wireless/realtimechat/proto/Data$KeyValue;
    :cond_50b
    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryParticipantFirstName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2012
    .restart local v19       #actorName:Ljava/lang/String;
    if-eqz v19, :cond_180

    .line 2013
    const v5, 0x7f0801fc

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v19, v6, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2017
    .restart local v8       #text:Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v10, 0x3

    invoke-virtual/range {v32 .. v32}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getTimestamp()J

    move-result-wide v11

    move-object/from16 v5, p0

    invoke-static/range {v5 .. v12}, Lcom/google/android/apps/plus/content/EsConversationsData;->updatePreviewSystemMessage$5091a27(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;IJ)V

    goto/16 :goto_180

    .line 2043
    .end local v8           #text:Ljava/lang/String;
    .end local v9           #authorId:Ljava/lang/String;
    .end local v19           #actorName:Ljava/lang/String;
    .end local v26           #event:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .end local v27           #i$:Ljava/util/Iterator;
    .end local v32           #tileEvent:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    :cond_532
    if-eqz v25, :cond_537

    .line 2044
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 2048
    :cond_537
    new-instance v35, Landroid/content/ContentValues;

    invoke-direct/range {v35 .. v35}, Landroid/content/ContentValues;-><init>()V

    .line 2049
    .local v35, values:Landroid/content/ContentValues;
    const-string v5, "status"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2050
    const-string v5, "messages"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "conversation_id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND status"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2055
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_ed
.end method

.method public static processConversationListResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 28
    .parameter "context"
    .parameter "account"
    .parameter "response"
    .parameter "operationState"

    .prologue
    .line 444
    const-string v7, "EsConversationsData"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_21

    .line 445
    const-string v7, "EsConversationsData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "processConversationListResponse count: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getClientConversationCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :cond_21
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 451
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getClientConversationList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_34

    .line 562
    :goto_33
    return-void

    .line 454
    :cond_34
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 456
    :try_start_37
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getClientConversationList()Ljava/util/List;

    move-result-object v16

    .line 459
    .local v16, conversations:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;>;"
    new-instance v21, Ljava/util/PriorityQueue;

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v7

    sget-object v8, Lcom/google/android/apps/plus/content/EsConversationsData;->sConversationComparator:Ljava/util/Comparator;

    move-object/from16 v0, v21

    invoke-direct {v0, v7, v8}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 462
    .local v21, orderedConversations:Ljava/util/PriorityQueue;,"Ljava/util/PriorityQueue<Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, i$:Ljava/util/Iterator;
    :goto_4c
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6c

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    .line 463
    .local v15, conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v3, v0, v1, v15, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->processConversation(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    .line 465
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_66
    .catchall {:try_start_37 .. :try_end_66} :catchall_67

    goto :goto_4c

    .line 559
    .end local v15           #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .end local v16           #conversations:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;>;"
    .end local v20           #i$:Ljava/util/Iterator;
    .end local v21           #orderedConversations:Ljava/util/PriorityQueue;,"Ljava/util/PriorityQueue<Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;>;"
    :catchall_67
    move-exception v7

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v7

    .line 469
    .restart local v16       #conversations:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;>;"
    .restart local v20       #i$:Ljava/util/Iterator;
    .restart local v21       #orderedConversations:Ljava/util/PriorityQueue;,"Ljava/util/PriorityQueue<Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;>;"
    :cond_6c
    const/16 v17, 0x0

    .line 470
    .local v17, count:I
    const/16 v18, 0x0

    .line 472
    .local v18, cursor:Landroid/database/Cursor;
    :try_start_70
    const-string v4, "conversations"

    const/4 v7, 0x2

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "conversation_id"

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const-string v8, "latest_event_timestamp"

    aput-object v8, v5, v7

    const-string v6, "is_awaiting_event_stream=1"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 477
    :goto_89
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_df

    .line 478
    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 479
    .local v4, conversationId:Ljava/lang/String;
    const/4 v7, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 480
    .local v5, latest:J
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-lez v7, :cond_a6

    .line 481
    const-wide/16 v7, 0x1

    sub-long/2addr v5, v7

    .line 483
    :cond_a6
    const-wide/16 v7, 0x0

    const/16 v9, 0x14

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getEventStream(Ljava/lang/String;JJI)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v22

    .line 485
    .local v22, request:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 486
    const-string v7, "EsConversationsData"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_dc

    .line 487
    const-string v7, "EsConversationsData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "requesting 20 events since "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_dc
    .catchall {:try_start_70 .. :try_end_dc} :catchall_1b7

    .line 490
    :cond_dc
    add-int/lit8 v17, v17, 0x1

    .line 491
    goto :goto_89

    .line 493
    .end local v4           #conversationId:Ljava/lang/String;
    .end local v5           #latest:J
    .end local v22           #request:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    :cond_df
    if-eqz v18, :cond_e4

    .line 494
    :try_start_e1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 497
    :cond_e4
    new-instance v23, Landroid/content/ContentValues;

    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 498
    .local v23, values:Landroid/content/ContentValues;
    const-string v7, "is_awaiting_event_stream"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v23

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 499
    const/4 v15, 0x0

    .line 501
    .restart local v15       #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    const/16 v19, 0x19

    .line 503
    .local v19, eventsToLoad:I
    :goto_f8
    const/16 v7, 0x28

    move/from16 v0, v17

    if-ge v0, v7, :cond_1c5

    invoke-virtual/range {v21 .. v21}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v15

    .end local v15           #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    check-cast v15, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    :try_end_104
    .catchall {:try_start_e1 .. :try_end_104} :catchall_67

    .restart local v15       #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    if-eqz v15, :cond_1c5

    .line 505
    const/16 v7, 0x14

    move/from16 v0, v17

    if-lt v0, v7, :cond_10e

    .line 506
    const/16 v19, 0xa

    .line 510
    :cond_10e
    :try_start_10e
    const-string v8, "conversations"

    const/4 v7, 0x1

    new-array v9, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v10, "latest_event_timestamp"

    aput-object v10, v9, v7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v10, "is_awaiting_event_stream=0 AND conversation_id=\'"

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "\'"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v7, v3

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 515
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_19b

    .line 516
    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 517
    .restart local v5       #latest:J
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-lez v7, :cond_150

    .line 518
    const-wide/16 v7, 0x1

    sub-long/2addr v5, v7

    .line 520
    :cond_150
    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v7

    const-wide/16 v10, 0x0

    move-wide v8, v5

    move/from16 v12, v19

    invoke-static/range {v7 .. v12}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getEventStream(Ljava/lang/String;JJI)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v22

    .line 522
    .restart local v22       #request:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 523
    const-string v7, "EsConversationsData"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_19b

    .line 524
    const-string v7, "EsConversationsData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "requesting "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " events since "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19b
    .catchall {:try_start_10e .. :try_end_19b} :catchall_1be

    .line 529
    .end local v5           #latest:J
    .end local v22           #request:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    :cond_19b
    if-eqz v18, :cond_1a0

    .line 530
    :try_start_19d
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 533
    :cond_1a0
    const-string v7, "conversations"

    const-string v8, "conversation_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, v23

    invoke-virtual {v3, v7, v0, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 536
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_f8

    .line 493
    .end local v15           #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .end local v19           #eventsToLoad:I
    .end local v23           #values:Landroid/content/ContentValues;
    :catchall_1b7
    move-exception v7

    if-eqz v18, :cond_1bd

    .line 494
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_1bd
    throw v7

    .line 529
    .restart local v15       #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .restart local v19       #eventsToLoad:I
    .restart local v23       #values:Landroid/content/ContentValues;
    :catchall_1be
    move-exception v7

    if-eqz v18, :cond_1c4

    .line 530
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_1c4
    throw v7

    .line 540
    :cond_1c5
    invoke-virtual/range {v23 .. v23}, Landroid/content/ContentValues;->clear()V

    .line 541
    const-string v7, "key"

    const-string v8, "awaiting_conversation_list"

    move-object/from16 v0, v23

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string v7, "value"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v23

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 545
    const-string v7, "realtimechat_metadata"

    const/4 v8, 0x0

    const/4 v9, 0x5

    move-object/from16 v0, v23

    invoke-virtual {v3, v7, v8, v0, v9}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 550
    const-string v7, "realtimechat_metadata"

    const-string v8, "key=\'requested_conversations_since\'"

    const/4 v9, 0x0

    invoke-virtual {v3, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 555
    const-string v7, "conversations"

    const-string v8, "is_pending_leave=1"

    const/4 v9, 0x0

    invoke-virtual {v3, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    const-string v8, "EsConversationsData"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_214

    const-string v8, "EsConversationsData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "RemoveLeftConversations "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :cond_214
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_217
    .catchall {:try_start_19d .. :try_end_217} :catchall_67

    .line 559
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 561
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_33
.end method

.method public static processConversationPreferenceResponse$43e73c50(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)V
    .registers 4
    .parameter "response"

    .prologue
    const/4 v2, 0x3

    .line 1030
    const-string v0, "EsConversationsData"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1031
    const-string v0, "EsConversationsData"

    const-string v1, "processConversationPreferenceResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    :cond_10
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v0

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-ne v0, v1, :cond_28

    .line 1034
    const-string v0, "EsConversationsData"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1035
    const-string v0, "EsConversationsData"

    const-string v1, "Conversation preference set successfully"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    :cond_27
    :goto_27
    return-void

    .line 1038
    :cond_28
    const-string v0, "EsConversationsData"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1039
    const-string v0, "EsConversationsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to set conversation preference with code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_27
.end method

.method public static processConversationResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 24
    .parameter "context"
    .parameter "account"
    .parameter "response"
    .parameter "operationState"

    .prologue
    .line 805
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 806
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "processConversationResponse conversationClientId: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getConversationClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    :cond_21
    const/16 v16, 0x0

    .line 810
    .local v16, conversationRowId:Ljava/lang/Long;
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 812
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 814
    :try_start_2e
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getClientConversation()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v15

    .line 815
    .local v15, conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v19

    .line 816
    .local v19, status:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    sget-object v4, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v19

    if-ne v0, v4, :cond_cf

    .line 817
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->hasConversationClientId()Z

    move-result v4

    if-eqz v4, :cond_4d

    .line 818
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getConversationClientId()Ljava/lang/String;

    move-result-object v13

    .line 819
    .local v13, clientConversationId:Ljava/lang/String;
    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v13, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateConversationId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    .end local v13           #clientConversationId:Ljava/lang/String;
    :cond_4d
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v3, v0, v1, v15, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->processConversation(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 825
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getParticipantErrorList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, i$:Ljava/util/Iterator;
    :goto_63
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_87

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    .line 826
    .local v6, error:Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getCreatedAt()J

    move-result-wide v9

    const/4 v11, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v12, p3

    invoke-static/range {v3 .. v12}, Lcom/google/android/apps/plus/content/EsConversationsData;->processParticipantError(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;JJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    :try_end_81
    .catchall {:try_start_2e .. :try_end_81} :catchall_82

    goto :goto_63

    .line 863
    .end local v6           #error:Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;
    .end local v15           #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .end local v18           #i$:Ljava/util/Iterator;
    .end local v19           #status:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    :catchall_82
    move-exception v4

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    .line 829
    .restart local v15       #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .restart local v18       #i$:Ljava/util/Iterator;
    .restart local v19       #status:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    :cond_87
    :try_start_87
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->hasReceipt()Z

    move-result v4

    if-eqz v4, :cond_98

    .line 830
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v7

    invoke-static {v3, v4, v5, v7}, Lcom/google/android/apps/plus/content/EsConversationsData;->processReceipt$4fede216(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/wireless/realtimechat/proto/Client$Receipt;)V

    .line 833
    :cond_98
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->hasRecentMessage()Z

    move-result v4

    if-eqz v4, :cond_ad

    .line 834
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getRecentMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v4

    invoke-virtual {v15}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v3, v4, v5, v7, v8}, Lcom/google/android/apps/plus/content/EsConversationsData;->processPreviewMessage$43824df1(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;Ljava/lang/String;J)V

    .line 861
    .end local v18           #i$:Ljava/util/Iterator;
    :cond_ad
    :goto_ad
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_b0
    .catchall {:try_start_87 .. :try_end_b0} :catchall_82

    .line 863
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 865
    if-eqz v16, :cond_ce

    .line 866
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 867
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyParticipantsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 868
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 870
    :cond_ce
    return-void

    .line 838
    :cond_cf
    :try_start_cf
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_ee

    .line 839
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Received conversation response error "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    :cond_ee
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->hasConversationClientId()Z

    move-result v4

    if-eqz v4, :cond_ad

    .line 842
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getConversationClientId()Ljava/lang/String;

    move-result-object v14

    .line 843
    .local v14, clientId:Ljava/lang/String;
    const/16 v17, 0x1

    .line 844
    .local v17, fatalErrorType:I
    sget-object v4, Lcom/google/android/apps/plus/content/EsConversationsData$4;->$SwitchMap$com$google$wireless$realtimechat$proto$Data$ResponseStatus:[I

    invoke-virtual/range {v19 .. v19}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_158

    .line 853
    :goto_105
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_12e

    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "updateConversationErrorType Conversation client Id: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", FatalErrorType: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12e
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "fatal_error_type"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "conversations"

    const-string v7, "conversation_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v5, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_14d
    .catchall {:try_start_cf .. :try_end_14d} :catchall_82

    goto/16 :goto_ad

    .line 846
    :pswitch_14f
    const/16 v17, 0x2

    .line 847
    goto :goto_105

    .line 849
    :pswitch_152
    const/16 v17, 0x3

    .line 850
    goto :goto_105

    .line 852
    :pswitch_155
    const/16 v17, 0x4

    goto :goto_105

    .line 844
    :pswitch_data_158
    .packed-switch 0x1
        :pswitch_14f
        :pswitch_152
        :pswitch_155
    .end packed-switch
.end method

.method public static processEventStreamResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 55
    .parameter "context"
    .parameter "account"
    .parameter "response"
    .parameter "operationState"

    .prologue
    .line 573
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4b

    .line 574
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "processEventStreamResponse conversationId: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " earliest: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getEarliest()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " latest: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getLatest()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " eventCount: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getEventCount()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :cond_4b
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 583
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const/16 v31, 0x0

    .line 584
    .local v31, conversationRowId:Ljava/lang/Long;
    const/16 v44, 0x0

    .line 585
    .local v44, messagesChanged:Z
    const/16 v46, 0x0

    .line 587
    .local v46, participantsChanged:Z
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 589
    :try_start_5c
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getConversationId()Ljava/lang/String;
    :try_end_5f
    .catchall {:try_start_5c .. :try_end_5f} :catchall_15f

    move-result-object v30

    .line 590
    .local v30, conversationId:Ljava/lang/String;
    const-wide/16 v34, 0x0

    .line 591
    .local v34, currentLatestEventTimestamp:J
    const-wide/16 v32, 0x0

    .line 592
    .local v32, currentEarliestEventTimestamp:J
    const/16 v43, 0x0

    .line 593
    .local v43, isAwaitingOlderEvents:Z
    const/16 v29, 0x0

    .line 595
    .local v29, conversationCursor:Landroid/database/Cursor;
    :try_start_68
    const-string v3, "conversations"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string v7, "earliest_event_timestamp"

    aput-object v7, v4, v6

    const/4 v6, 0x2

    const-string v7, "latest_event_timestamp"

    aput-object v7, v4, v6

    const/4 v6, 0x3

    const-string v7, "is_awaiting_older_events"

    aput-object v7, v4, v6

    const-string v5, "conversation_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v30, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    .line 602
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_127

    .line 603
    const/4 v3, 0x0

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    .line 604
    const/4 v3, 0x1

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v32

    .line 605
    const/4 v3, 0x2

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v34

    .line 606
    const/4 v3, 0x3

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_b5
    .catchall {:try_start_68 .. :try_end_b5} :catchall_158

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_124

    const/16 v43, 0x1

    .line 617
    :goto_bb
    if-eqz v29, :cond_c0

    .line 618
    :try_start_bd
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    .line 622
    :cond_c0
    new-instance v50, Landroid/content/ContentValues;

    invoke-direct/range {v50 .. v50}, Landroid/content/ContentValues;-><init>()V

    .line 623
    .local v50, values:Landroid/content/ContentValues;
    const-string v3, "is_awaiting_event_stream"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 625
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getEventCount()I

    move-result v37

    .line 626
    .local v37, eventCount:I
    if-lez v37, :cond_39f

    .line 627
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getEarliest()J

    move-result-wide v38

    .line 628
    .local v38, eventStreamEarliest:J
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getLatest()J

    move-result-wide v40

    .line 629
    .local v40, eventStreamLatest:J
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_10a

    .line 630
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "currentLatestEventTimestamp "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " eventStreamEarliest "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v38

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    :cond_10a
    cmp-long v3, v38, v32

    if-gez v3, :cond_164

    if-nez v43, :cond_164

    .line 636
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_120

    .line 637
    const-string v3, "EsConversationsData"

    const-string v4, "isAwaitingOlderEvents is blank, ignoring"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_120
    .catchall {:try_start_bd .. :try_end_120} :catchall_15f

    .line 785
    :cond_120
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 794
    .end local v37           #eventCount:I
    .end local v38           #eventStreamEarliest:J
    .end local v40           #eventStreamLatest:J
    .end local v50           #values:Landroid/content/ContentValues;
    :cond_123
    :goto_123
    return-void

    .line 606
    :cond_124
    const/16 v43, 0x0

    goto :goto_bb

    .line 608
    :cond_127
    :try_start_127
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_146

    .line 609
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "eventStream for unknown conversation "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    :cond_146
    invoke-static/range {v30 .. v30}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_14f
    .catchall {:try_start_127 .. :try_end_14f} :catchall_158

    .line 617
    if-eqz v29, :cond_154

    .line 618
    :try_start_151
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V
    :try_end_154
    .catchall {:try_start_151 .. :try_end_154} :catchall_15f

    .line 785
    :cond_154
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_123

    .line 617
    :catchall_158
    move-exception v3

    if-eqz v29, :cond_15e

    .line 618
    :try_start_15b
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    :cond_15e
    throw v3
    :try_end_15f
    .catchall {:try_start_15b .. :try_end_15f} :catchall_15f

    .line 785
    .end local v29           #conversationCursor:Landroid/database/Cursor;
    .end local v30           #conversationId:Ljava/lang/String;
    .end local v32           #currentEarliestEventTimestamp:J
    .end local v34           #currentLatestEventTimestamp:J
    .end local v43           #isAwaitingOlderEvents:Z
    :catchall_15f
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 642
    .restart local v29       #conversationCursor:Landroid/database/Cursor;
    .restart local v30       #conversationId:Ljava/lang/String;
    .restart local v32       #currentEarliestEventTimestamp:J
    .restart local v34       #currentLatestEventTimestamp:J
    .restart local v37       #eventCount:I
    .restart local v38       #eventStreamEarliest:J
    .restart local v40       #eventStreamLatest:J
    .restart local v43       #isAwaitingOlderEvents:Z
    .restart local v50       #values:Landroid/content/ContentValues;
    :cond_164
    :try_start_164
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getEventList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v42

    .local v42, i$:Ljava/util/Iterator;
    :cond_16c
    :goto_16c
    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2ce

    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    .line 643
    .local v36, event:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTimestamp()J

    move-result-wide v48

    .line 644
    .local v48, timestamp:J
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getReceiverState()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsConversationsData$4;->$SwitchMap$com$google$wireless$realtimechat$proto$Client$EventStreamResponse$ReceiverState:[I

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_456

    const-string v3, "EsConversationsData"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_19b

    const-string v3, "EsConversationsData"

    const-string v4, "ChatMessage\'s read state could not be determined."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_19b
    const/4 v12, 0x0

    .line 645
    .local v12, messageState:I
    :goto_19c
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1e1

    .line 646
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "event timestamp "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v48

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " current conversation "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v32

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " state "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getReceiverState()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$ReceiverState;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    :cond_1e1
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasReceipt()Z

    move-result v3

    if-eqz v3, :cond_20e

    .line 653
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1f7

    .line 654
    const-string v3, "EsConversationsData"

    const-string v4, "Got Receipt"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    :cond_1f7
    const/16 v44, 0x1

    .line 657
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v6

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->processReceipt$4fede216(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/wireless/realtimechat/proto/Client$Receipt;)V

    goto/16 :goto_16c

    .line 644
    .end local v12           #messageState:I
    :pswitch_206
    const/4 v12, 0x3

    goto :goto_19c

    :pswitch_208
    const/4 v12, 0x4

    goto :goto_19c

    :pswitch_20a
    const/4 v12, 0x5

    goto :goto_19c

    :pswitch_20c
    const/4 v12, 0x5

    goto :goto_19c

    .line 659
    .restart local v12       #messageState:I
    :cond_20e
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasChatMessage()Z

    move-result v3

    if-eqz v3, :cond_23c

    .line 660
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_224

    .line 661
    const-string v3, "EsConversationsData"

    const-string v4, "Got ChatMessage"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    :cond_224
    const/16 v44, 0x1

    .line 664
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v5

    .line 665
    .local v5, chatMessage:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const/4 v9, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v6, v30

    move-object/from16 v10, p3

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/plus/content/EsConversationsData;->processMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;Ljava/lang/String;JZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_16c

    .line 667
    .end local v5           #chatMessage:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    :cond_23c
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasMembershipChange()Z

    move-result v3

    if-eqz v3, :cond_269

    .line 668
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_252

    .line 669
    const-string v3, "EsConversationsData"

    const-string v4, "Got MembershipChange"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :cond_252
    const/16 v46, 0x1

    .line 672
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v11

    .line 673
    .local v11, change:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const/4 v13, 0x1

    move-object v6, v2

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v14, p3

    invoke-static/range {v6 .. v14}, Lcom/google/android/apps/plus/content/EsConversationsData;->processMembershipChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$MembershipChange;IZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_16c

    .line 676
    .end local v11           #change:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    :cond_269
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasGroupConversationRename()Z

    move-result v3

    if-eqz v3, :cond_299

    .line 677
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_27f

    .line 678
    const-string v3, "EsConversationsData"

    const-string v4, "Got GroupConversationRename"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    :cond_27f
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v18

    .line 681
    .local v18, rename:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    const/16 v19, 0x0

    const/16 v21, 0x1

    move-object v13, v2

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move/from16 v20, v12

    move-object/from16 v22, p3

    invoke-static/range {v13 .. v22}, Lcom/google/android/apps/plus/content/EsConversationsData;->processGroupConversationRename(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;ZIZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_16c

    .line 684
    .end local v18           #rename:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    :cond_299
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasMigration()Z

    move-result v3

    if-nez v3, :cond_16c

    .line 688
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->hasTileEvent()Z

    move-result v3

    if-eqz v3, :cond_16c

    .line 689
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2b5

    .line 690
    const-string v3, "EsConversationsData"

    const-string v4, "Got TileEvent"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    :cond_2b5
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v24

    .line 693
    .local v24, tileEvent:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    const/16 v26, 0x1

    move-object/from16 v19, v2

    move-object/from16 v20, p0

    move-object/from16 v21, p1

    move/from16 v25, v12

    move-object/from16 v27, p3

    invoke-static/range {v19 .. v27}, Lcom/google/android/apps/plus/content/EsConversationsData;->processTileEvent(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$TileEvent;IZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_16c

    .line 698
    .end local v12           #messageState:I
    .end local v24           #tileEvent:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    .end local v36           #event:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .end local v48           #timestamp:J
    :cond_2ce
    const/16 v45, 0x0

    .line 699
    .local v45, newEarliestEventTimestamp:Ljava/lang/Long;
    const/16 v28, 0x0

    .line 700
    .local v28, clearIsAwaitingOlder:Z
    cmp-long v3, v34, v38

    if-gez v3, :cond_3dd

    .line 707
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2e6

    .line 708
    const-string v3, "EsConversationsData"

    const-string v4, "newer messages with gap"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    :cond_2e6
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-string v6, "messages"

    const-string v7, "conversation_id=? AND timestamp<?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v9

    const/4 v3, 0x1

    invoke-static/range {v38 .. v39}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v3

    invoke-virtual {v2, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v47

    .line 712
    .local v47, removed:I
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_340

    .line 713
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Latest current event "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " earliest in eventStream "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v38

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " deleted "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " older messages"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_340
    if-lez v47, :cond_344

    .line 718
    const/16 v28, 0x1

    .line 720
    :cond_344
    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v45

    .line 757
    .end local v47           #removed:I
    :cond_348
    :goto_348
    if-eqz v28, :cond_366

    .line 758
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_35a

    .line 759
    const-string v3, "EsConversationsData"

    const-string v4, "clearing is_awaiting_older_events"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    :cond_35a
    const-string v3, "is_awaiting_older_events"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 763
    :cond_366
    if-eqz v45, :cond_390

    .line 764
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_387

    .line 765
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "updating earliest event timestamp "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    :cond_387
    const-string v3, "earliest_event_timestamp"

    move-object/from16 v0, v50

    move-object/from16 v1, v45

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 771
    :cond_390
    cmp-long v3, v40, v34

    if-lez v3, :cond_39f

    .line 772
    const-string v3, "latest_event_timestamp"

    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 777
    .end local v28           #clearIsAwaitingOlder:Z
    .end local v38           #eventStreamEarliest:J
    .end local v40           #eventStreamLatest:J
    .end local v42           #i$:Ljava/util/Iterator;
    .end local v45           #newEarliestEventTimestamp:Ljava/lang/Long;
    :cond_39f
    invoke-virtual/range {v50 .. v50}, Landroid/content/ContentValues;->size()I

    move-result v3

    if-lez v3, :cond_3b8

    .line 778
    const-string v3, "conversations"

    const-string v4, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v50

    invoke-virtual {v2, v3, v0, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 783
    :cond_3b8
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3bb
    .catchall {:try_start_164 .. :try_end_3bb} :catchall_15f

    .line 785
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 787
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 788
    if-eqz v44, :cond_3ce

    .line 789
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 791
    :cond_3ce
    if-eqz v46, :cond_123

    .line 792
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyParticipantsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    goto/16 :goto_123

    .line 721
    .restart local v28       #clearIsAwaitingOlder:Z
    .restart local v38       #eventStreamEarliest:J
    .restart local v40       #eventStreamLatest:J
    .restart local v42       #i$:Ljava/util/Iterator;
    .restart local v45       #newEarliestEventTimestamp:Ljava/lang/Long;
    :cond_3dd
    cmp-long v3, v38, v32

    if-lez v3, :cond_43e

    const-wide/16 v3, 0x0

    cmp-long v3, v32, v3

    if-eqz v3, :cond_43e

    .line 730
    :try_start_3e7
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3f7

    .line 731
    const-string v3, "EsConversationsData"

    const-string v4, "newer messages with overlap"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    :cond_3f7
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_348

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v31

    if-eq v0, v3, :cond_348

    .line 736
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->removeExcessMessages(Landroid/database/sqlite/SQLiteDatabase;J)I

    move-result v47

    .line 737
    .restart local v47       #removed:I
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_42c

    .line 738
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "trim excess messages removed "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    :cond_42c
    if-lez v47, :cond_430

    .line 741
    const/16 v28, 0x1

    .line 743
    :cond_430
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryOldestMessageTimestamp(Landroid/database/sqlite/SQLiteDatabase;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v45

    .line 744
    goto/16 :goto_348

    .line 749
    .end local v47           #removed:I
    :cond_43e
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_44e

    .line 750
    const-string v3, "EsConversationsData"

    const-string v4, "older messages"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    :cond_44e
    const/16 v28, 0x1

    .line 753
    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_453
    .catchall {:try_start_3e7 .. :try_end_453} :catchall_15f

    move-result-object v45

    goto/16 :goto_348

    .line 644
    :pswitch_data_456
    .packed-switch 0x1
        :pswitch_206
        :pswitch_208
        :pswitch_20a
        :pswitch_20c
    .end packed-switch
.end method

.method private static processGroupConversationRename(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;ZIZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 29
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "rename"
    .parameter "shouldUpdateName"
    .parameter "messageState"
    .parameter "sendReceipts"
    .parameter "operationState"

    .prologue
    .line 2239
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 2240
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "processGroupConversationRename conversationId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getConversationId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2244
    :cond_21
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->hasNewDisplayName()Z

    move-result v3

    if-eqz v3, :cond_104

    .line 2245
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getNewDisplayName()Ljava/lang/String;

    move-result-object v18

    .line 2246
    .local v18, newName:Ljava/lang/String;
    if-eqz p6, :cond_6e

    .line 2247
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4c

    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "updateConversationName Conversation Row id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4c
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "name"

    move-object/from16 v0, v18

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "conversations"

    const-string v5, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2250
    :cond_6e
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getTimestamp()J

    move-result-wide v13

    .line 2251
    .local v13, timestamp:J
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2, v13, v14}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateLatestEventTimestamp(Landroid/database/sqlite/SQLiteDatabase;JJ)V

    .line 2253
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getSenderId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryParticipantFullName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2254
    .local v17, actorName:Ljava/lang/String;
    if-eqz v17, :cond_105

    .line 2255
    const v3, 0x7f0801fb

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "<b>"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</b>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "<i>"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</i>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2260
    .local v8, text:Ljava/lang/String;
    const/4 v10, 0x1

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getSenderId()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v9, p7

    move/from16 v15, p8

    move-object/from16 v16, p9

    invoke-static/range {v3 .. v16}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertSystemMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;IZLjava/lang/String;IJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    .line 2265
    invoke-virtual/range {p9 .. p9}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_ee

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, p3

    if-eqz v3, :cond_104

    :cond_ee
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getSenderId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_104

    const/4 v3, 0x5

    move/from16 v0, p7

    if-eq v0, v3, :cond_104

    .line 2269
    invoke-virtual/range {p9 .. p9}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->setShouldTriggerNotifications()V

    .line 2277
    .end local v8           #text:Ljava/lang/String;
    .end local v13           #timestamp:J
    .end local v17           #actorName:Ljava/lang/String;
    .end local v18           #newName:Ljava/lang/String;
    :cond_104
    :goto_104
    return-void

    .line 2272
    .restart local v13       #timestamp:J
    .restart local v17       #actorName:Ljava/lang/String;
    .restart local v18       #newName:Ljava/lang/String;
    :cond_105
    const-string v3, "EsConversationsData"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_104

    .line 2273
    const-string v3, "EsConversationsData"

    const-string v4, "Participant who changed conversation name could not be found"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_104
.end method

.method private static processHangoutTile(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$TileEvent;JIZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 27
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "tileEvent"
    .parameter "conversationRowId"
    .parameter "messageState"
    .parameter "sendReceipts"
    .parameter "operationState"

    .prologue
    .line 2327
    const/4 v9, 0x0

    .line 2328
    .local v9, authorId:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getEventDataList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_56

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;

    .line 2329
    .local v17, pair:Lcom/google/wireless/realtimechat/proto/Data$KeyValue;
    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AUTHOR_PROFILE_ID"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 2330
    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getValue()Ljava/lang/String;

    move-result-object v9

    goto :goto_9

    .line 2332
    :cond_26
    const-string v1, "EsConversationsData"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2333
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got unexpected data in join hangout tile event: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/realtimechat/proto/Data$KeyValue;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 2338
    .end local v17           #pair:Lcom/google/wireless/realtimechat/proto/Data$KeyValue;
    :cond_56
    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryParticipantFirstName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 2339
    .local v15, actorName:Ljava/lang/String;
    if-eqz v15, :cond_a9

    .line 2340
    const v1, 0x7f0801fc

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v15, v2, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2344
    .local v6, text:Ljava/lang/String;
    const/4 v8, 0x1

    const/4 v10, 0x6

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getTimestamp()J

    move-result-wide v11

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p4

    move/from16 v7, p6

    move/from16 v13, p7

    move-object/from16 v14, p8

    invoke-static/range {v1 .. v14}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertSystemMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;IZLjava/lang/String;IJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    .line 2349
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_96

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v1, v1, p4

    if-eqz v1, :cond_a8

    :cond_96
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a8

    const/4 v1, 0x5

    move/from16 v0, p6

    if-eq v0, v1, :cond_a8

    .line 2353
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->setShouldTriggerNotifications()V

    .line 2360
    .end local v6           #text:Ljava/lang/String;
    :cond_a8
    :goto_a8
    return-void

    .line 2356
    :cond_a9
    const-string v1, "EsConversationsData"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a8

    .line 2357
    const-string v1, "EsConversationsData"

    const-string v2, "Participant who joined hangout could not be found locally"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a8
.end method

.method public static processInviteResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 20
    .parameter "context"
    .parameter "account"
    .parameter "inviteResponse"
    .parameter "request"
    .parameter "operationState"

    .prologue
    .line 978
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 979
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "processInviteResponse participantErrorCount: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getParticipantErrorCount()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " requestError: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getRequestError()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    :cond_2f
    const/4 v12, 0x0

    .line 984
    .local v12, conversationRowId:Ljava/lang/Long;
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 986
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 988
    :try_start_3b
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasInviteRequest()Z

    move-result v2

    if-eqz v2, :cond_dd

    .line 989
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getInviteRequest()Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    move-result-object v14

    .line 990
    .local v14, inviteRequest:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    invoke-virtual {v14}, Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;->getConversationId()Ljava/lang/String;

    move-result-object v11

    .line 991
    .local v11, conversationId:Ljava/lang/String;
    invoke-static {v1, v11}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v12

    .line 992
    if-nez v12, :cond_7f

    .line 993
    const-string v2, "EsConversationsData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_72

    .line 994
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "attempt to process invite response for a nonexistant conversation id ["

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    :cond_72
    invoke-static {v11}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_7b
    .catchall {:try_start_3b .. :try_end_7b} :catchall_a9

    .line 1013
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1014
    .end local v11           #conversationId:Ljava/lang/String;
    .end local v14           #inviteRequest:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    :goto_7e
    return-void

    .line 1003
    .restart local v11       #conversationId:Ljava/lang/String;
    .restart local v14       #inviteRequest:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    :cond_7f
    :try_start_7f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v5, 0x3e8

    mul-long v7, v2, v5

    .line 1004
    .local v7, timestamp:J
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getParticipantErrorList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, i$:Ljava/util/Iterator;
    :goto_8f
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_ae

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    .line 1005
    .local v4, error:Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const/4 v9, 0x1

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v10, p4

    invoke-static/range {v1 .. v10}, Lcom/google/android/apps/plus/content/EsConversationsData;->processParticipantError(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;JJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    :try_end_a8
    .catchall {:try_start_7f .. :try_end_a8} :catchall_a9

    goto :goto_8f

    .line 1013
    .end local v4           #error:Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;
    .end local v7           #timestamp:J
    .end local v11           #conversationId:Ljava/lang/String;
    .end local v13           #i$:Ljava/util/Iterator;
    .end local v14           #inviteRequest:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    :catchall_a9
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 1008
    .restart local v7       #timestamp:J
    .restart local v11       #conversationId:Ljava/lang/String;
    .restart local v13       #i$:Ljava/util/Iterator;
    .restart local v14       #inviteRequest:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    :cond_ae
    :try_start_ae
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_be

    const-string v2, "EsConversationsData"

    const-string v3, "NOTIFY ALL PARTICIPANTS"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_be
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsProvider;->buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1009
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 1011
    .end local v7           #timestamp:J
    .end local v11           #conversationId:Ljava/lang/String;
    .end local v13           #i$:Ljava/util/Iterator;
    .end local v14           #inviteRequest:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    :cond_dd
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e0
    .catchall {:try_start_ae .. :try_end_e0} :catchall_a9

    .line 1013
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_7e
.end method

.method public static processLeaveConversationResponse$6cb3bb58(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "request"

    .prologue
    .line 1057
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 1058
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processLeaveConversationResponse "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getLeaveConversationRequest()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;->getConversationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    :cond_25
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1063
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1065
    :try_start_30
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasLeaveConversationRequest()Z

    move-result v1

    if-eqz v1, :cond_c6

    .line 1066
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getLeaveConversationRequest()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;->getConversationId()Ljava/lang/String;
    :try_end_3d
    .catchall {:try_start_30 .. :try_end_3d} :catchall_ae

    move-result-object v8

    .line 1067
    .local v8, conversationId:Ljava/lang/String;
    const/4 v10, 0x0

    .line 1068
    .local v10, cursor:Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 1069
    .local v9, conversationRowId:Ljava/lang/Long;
    const/4 v11, 0x0

    .line 1071
    .local v11, pendingLeave:Z
    :try_start_41
    const-string v1, "conversations"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "is_pending_leave"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v8, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1077
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_77

    .line 1078
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 1079
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_72
    .catchall {:try_start_41 .. :try_end_72} :catchall_a7

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a5

    const/4 v11, 0x1

    .line 1082
    :cond_77
    :goto_77
    if-eqz v10, :cond_7c

    .line 1083
    :try_start_79
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1086
    :cond_7c
    if-nez v9, :cond_b3

    .line 1087
    const-string v1, "EsConversationsData"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a1

    .line 1088
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "attempt to process presence for a nonexistant conversation id ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a1
    .catchall {:try_start_79 .. :try_end_a1} :catchall_ae

    .line 1103
    :cond_a1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1106
    .end local v8           #conversationId:Ljava/lang/String;
    .end local v9           #conversationRowId:Ljava/lang/Long;
    .end local v10           #cursor:Landroid/database/Cursor;
    .end local v11           #pendingLeave:Z
    :goto_a4
    return-void

    .line 1079
    .restart local v8       #conversationId:Ljava/lang/String;
    .restart local v9       #conversationRowId:Ljava/lang/Long;
    .restart local v10       #cursor:Landroid/database/Cursor;
    .restart local v11       #pendingLeave:Z
    :cond_a5
    const/4 v11, 0x0

    goto :goto_77

    .line 1082
    :catchall_a7
    move-exception v1

    if-eqz v10, :cond_ad

    .line 1083
    :try_start_aa
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_ad
    throw v1
    :try_end_ae
    .catchall {:try_start_aa .. :try_end_ae} :catchall_ae

    .line 1103
    .end local v8           #conversationId:Ljava/lang/String;
    .end local v9           #conversationRowId:Ljava/lang/Long;
    .end local v10           #cursor:Landroid/database/Cursor;
    .end local v11           #pendingLeave:Z
    :catchall_ae
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 1092
    .restart local v8       #conversationId:Ljava/lang/String;
    .restart local v9       #conversationRowId:Ljava/lang/Long;
    .restart local v10       #cursor:Landroid/database/Cursor;
    .restart local v11       #pendingLeave:Z
    :cond_b3
    if-eqz v11, :cond_c6

    .line 1093
    :try_start_b5
    const-string v1, "conversations"

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1101
    .end local v8           #conversationId:Ljava/lang/String;
    .end local v9           #conversationRowId:Ljava/lang/Long;
    .end local v10           #cursor:Landroid/database/Cursor;
    .end local v11           #pendingLeave:Z
    :cond_c6
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_c9
    .catchall {:try_start_b5 .. :try_end_c9} :catchall_ae

    .line 1103
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1105
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_a4
.end method

.method private static processMembershipChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$MembershipChange;IZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 48
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "change"
    .parameter "messageState"
    .parameter "sendReceipts"
    .parameter "operationState"

    .prologue
    .line 2071
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 2072
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processMembershipChange conversationId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getType()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Type;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2076
    :cond_2f
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getTimestamp()J

    move-result-wide v12

    .line 2078
    .local v12, timestamp:J
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getParticipantList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, i$:Ljava/util/Iterator;
    :cond_3b
    :goto_3b
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_249

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 2079
    .local v36, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasParticipantId()Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 2080
    sget-object v2, Lcom/google/android/apps/plus/content/EsConversationsData$4;->$SwitchMap$com$google$wireless$realtimechat$proto$Client$MembershipChange$Type:[I

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getType()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_3fa

    .line 2124
    const-string v2, "EsConversationsData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 2125
    const-string v2, "EsConversationsData"

    const-string v3, "Ignoring unsupported membership change event"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3b

    .line 2084
    :pswitch_6d
    const/4 v10, 0x0

    const/4 v11, 0x0

    :try_start_6f
    const-string v3, "conversation_participants"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "MAX(sequence)"

    aput-object v5, v4, v2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "conversation_id="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_93
    .catchall {:try_start_6f .. :try_end_93} :catchall_1ae

    move-result-object v3

    :try_start_94
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3f7

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_9e
    .catchall {:try_start_94 .. :try_end_9e} :catchall_3f4

    move-result v2

    :goto_9f
    if-eqz v3, :cond_a4

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_a4
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "participant_id"

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "first_name"

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "full_name"

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "type"

    invoke-static/range {v36 .. v36}, Lcom/google/android/apps/plus/content/EsConversationsData;->convertParticipantType(Lcom/google/wireless/realtimechat/proto/Data$Participant;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "participants"

    const-string v5, "participant_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_ef

    const-string v4, "participants"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_ef
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    const-string v4, "conversation_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "participant_id"

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "active"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "sequence"

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "conversation_participants"

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2087
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getSenderId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryParticipantFullName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 2089
    .local v22, actorName:Ljava/lang/String;
    const v2, 0x7f0801fd

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "<b>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "<b>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2094
    .local v7, text:Ljava/lang/String;
    const/4 v9, 0x1

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getSenderId()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v5, p3

    move/from16 v8, p6

    move/from16 v14, p7

    move-object/from16 v15, p8

    invoke-static/range {v2 .. v15}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertSystemMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;IZLjava/lang/String;IJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    .line 2099
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_196

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, p3

    if-eqz v2, :cond_3b

    :cond_196
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getSenderId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3b

    const/4 v2, 0x5

    move/from16 v0, p6

    if-eq v0, v2, :cond_3b

    .line 2103
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->setShouldTriggerNotifications()V

    goto/16 :goto_3b

    .line 2084
    .end local v7           #text:Ljava/lang/String;
    .end local v22           #actorName:Ljava/lang/String;
    :catchall_1ae
    move-exception v2

    move-object v3, v10

    :goto_1b0
    if-eqz v3, :cond_1b5

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_1b5
    throw v2

    .line 2109
    :pswitch_1b6
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1e3

    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "markParticipantInactive ConversationRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Participant id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e3
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "active"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "conversation_participants"

    const-string v5, "conversation_id=? AND participant_id=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    const/4 v8, 0x1

    aput-object v2, v6, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2111
    const v2, 0x7f0801fe

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "<b>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</b>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2116
    .restart local v7       #text:Ljava/lang/String;
    const/4 v9, 0x0

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getSenderId()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x7

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v5, p3

    move/from16 v8, p6

    move/from16 v14, p7

    move-object/from16 v15, p8

    invoke-static/range {v2 .. v15}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertSystemMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;IZLjava/lang/String;IJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    goto/16 :goto_3b

    .line 2132
    .end local v7           #text:Ljava/lang/String;
    .end local v36           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_249
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_259

    .line 2133
    const-string v2, "EsConversationsData"

    const-string v3, "renaming conversation"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2136
    :cond_259
    const/16 v23, 0x0

    .line 2137
    .local v23, conversationCursor:Landroid/database/Cursor;
    const/16 v29, 0x0

    .line 2138
    .local v29, isAwaitingEventStream:Z
    const-wide/16 v31, 0x0

    .line 2140
    .local v31, latestEventTimestamp:J
    :try_start_25f
    const-string v15, "conversations"

    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/4 v2, 0x0

    const-string v3, "is_awaiting_event_stream"

    aput-object v3, v16, v2

    const/4 v2, 0x1

    const-string v3, "latest_event_timestamp"

    aput-object v3, v16, v2

    const/4 v2, 0x2

    const-string v3, "is_muted"

    aput-object v3, v16, v2

    const-string v17, "_id=?"

    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v2, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v18, v2

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v14, p0

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 2147
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2b0

    .line 2148
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_34b

    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_34b

    const/16 v29, 0x1

    .line 2150
    :goto_2a9
    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2af
    .catchall {:try_start_25f .. :try_end_2af} :catchall_34f

    move-result-wide v31

    .line 2153
    :cond_2b0
    if-eqz v23, :cond_2b5

    .line 2154
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 2158
    :cond_2b5
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    .line 2159
    .local v27, generatedNameBuilder:Ljava/lang/StringBuilder;
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    .line 2160
    .local v35, packedParticipantsBuilder:Ljava/lang/StringBuilder;
    const/16 v25, 0x0

    .line 2162
    .local v25, cursor:Landroid/database/Cursor;
    :try_start_2c1
    const-string v15, "participants_view"

    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/4 v2, 0x0

    const-string v3, "participant_id"

    aput-object v3, v16, v2

    const/4 v2, 0x1

    const-string v3, "first_name"

    aput-object v3, v16, v2

    const-string v17, "conversation_id=? AND active=1"

    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v2, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v18, v2

    const/16 v19, 0x0

    const/16 v20, 0x0

    const-string v21, "sequence ASC"

    move-object/from16 v14, p0

    invoke-virtual/range {v14 .. v21}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 2169
    const/16 v30, 0x1

    .line 2170
    .local v30, isFirst:Z
    const/16 v24, 0x0

    .line 2171
    .local v24, count:I
    :cond_2f0
    :goto_2f0
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_356

    .line 2172
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 2173
    .local v37, participantId:Ljava/lang/String;
    const/4 v2, 0x5

    move/from16 v0, v24

    if-ge v0, v2, :cond_314

    .line 2174
    if-lez v24, :cond_30b

    .line 2175
    const/16 v2, 0x7c

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2177
    :cond_30b
    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2178
    add-int/lit8 v24, v24, 0x1

    .line 2180
    :cond_314
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f0

    .line 2182
    if-nez v30, :cond_329

    .line 2183
    const-string v2, ", "

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2185
    :cond_329
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    .line 2186
    .local v33, name:Ljava/lang/String;
    if-eqz v33, :cond_2f0

    const-string v2, ""

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f0

    .line 2187
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_348
    .catchall {:try_start_2c1 .. :try_end_348} :catchall_3ed

    .line 2188
    const/16 v30, 0x0

    goto :goto_2f0

    .line 2148
    .end local v24           #count:I
    .end local v25           #cursor:Landroid/database/Cursor;
    .end local v27           #generatedNameBuilder:Ljava/lang/StringBuilder;
    .end local v30           #isFirst:Z
    .end local v33           #name:Ljava/lang/String;
    .end local v35           #packedParticipantsBuilder:Ljava/lang/StringBuilder;
    .end local v37           #participantId:Ljava/lang/String;
    :cond_34b
    const/16 v29, 0x0

    goto/16 :goto_2a9

    .line 2153
    :catchall_34f
    move-exception v2

    if-eqz v23, :cond_355

    .line 2154
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_355
    throw v2

    .line 2193
    .restart local v24       #count:I
    .restart local v25       #cursor:Landroid/database/Cursor;
    .restart local v27       #generatedNameBuilder:Ljava/lang/StringBuilder;
    .restart local v30       #isFirst:Z
    .restart local v35       #packedParticipantsBuilder:Ljava/lang/StringBuilder;
    :cond_356
    if-eqz v25, :cond_35b

    .line 2194
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 2197
    :cond_35b
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 2198
    .local v26, generatedName:Ljava/lang/String;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 2199
    .local v34, packedParticipants:Ljava/lang/String;
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_382

    .line 2200
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "generated name "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2202
    :cond_382
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3a1

    .line 2203
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packed participants "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2206
    :cond_3a1
    new-instance v38, Landroid/content/ContentValues;

    invoke-direct/range {v38 .. v38}, Landroid/content/ContentValues;-><init>()V

    .line 2207
    .local v38, values:Landroid/content/ContentValues;
    const-string v2, "generated_name"

    move-object/from16 v0, v38

    move-object/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208
    const-string v2, "packed_participants"

    move-object/from16 v0, v38

    move-object/from16 v1, v34

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2210
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->hasTimestamp()Z

    move-result v2

    if-eqz v2, :cond_3d7

    .line 2211
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getTimestamp()J

    move-result-wide v2

    cmp-long v2, v2, v31

    if-lez v2, :cond_3d7

    if-nez v29, :cond_3d7

    .line 2213
    const-string v2, "latest_event_timestamp"

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getTimestamp()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2217
    :cond_3d7
    const-string v2, "conversations"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2220
    return-void

    .line 2193
    .end local v24           #count:I
    .end local v26           #generatedName:Ljava/lang/String;
    .end local v30           #isFirst:Z
    .end local v34           #packedParticipants:Ljava/lang/String;
    .end local v38           #values:Landroid/content/ContentValues;
    :catchall_3ed
    move-exception v2

    if-eqz v25, :cond_3f3

    .line 2194
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_3f3
    throw v2

    .line 2084
    .end local v23           #conversationCursor:Landroid/database/Cursor;
    .end local v25           #cursor:Landroid/database/Cursor;
    .end local v27           #generatedNameBuilder:Ljava/lang/StringBuilder;
    .end local v29           #isAwaitingEventStream:Z
    .end local v31           #latestEventTimestamp:J
    .end local v35           #packedParticipantsBuilder:Ljava/lang/StringBuilder;
    .restart local v36       #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :catchall_3f4
    move-exception v2

    goto/16 :goto_1b0

    :cond_3f7
    move v2, v11

    goto/16 :goto_9f

    .line 2080
    :pswitch_data_3fa
    .packed-switch 0x1
        :pswitch_6d
        :pswitch_1b6
    .end packed-switch
.end method

.method private static processMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;Ljava/lang/String;JZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 37
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "message"
    .parameter "conversationId"
    .parameter "conversationRowId"
    .parameter "sendReceipts"
    .parameter "operationState"

    .prologue
    .line 1267
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContentCount()I

    move-result v3

    if-lez v3, :cond_139

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContent(I)Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getText()Ljava/lang/String;

    move-result-object v24

    .line 1269
    .local v24, text:Ljava/lang/String;
    :goto_11
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->determineMessageState(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)I

    move-result v23

    .line 1270
    .local v23, state:I
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4e

    .line 1271
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "processMessage conversationId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " timestamp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1275
    :cond_4e
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v25

    .line 1276
    .local v25, timestamp:J
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->hasMessageClientId()Z

    move-result v3

    if-eqz v3, :cond_d0

    .line 1277
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getMessageClientId()Ljava/lang/String;

    move-result-object v12

    .line 1278
    .local v12, clientMessageId:Ljava/lang/String;
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_85

    .line 1279
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "client Id "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new message timestamp "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v25

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    :cond_85
    if-eqz v12, :cond_d0

    .line 1283
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    if-nez v4, :cond_13d

    const-string v3, "EsConversationsData"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_d0

    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "attempt to update a message timestamp ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v25

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] for nonexistant conversation id ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1288
    .end local v12           #clientMessageId:Ljava/lang/String;
    :cond_d0
    :goto_d0
    const/16 v22, 0x0

    .line 1290
    .local v22, participantCursor:Landroid/database/Cursor;
    :try_start_d2
    const-string v4, "participants"

    const/4 v3, 0x1

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "_id"

    aput-object v6, v5, v3

    const-string v6, "participant_id=?"

    const/4 v3, 0x1

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 1295
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_159

    .line 1296
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_12c

    .line 1297
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "attempt to insert a message timestamp ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] for nonexistant participant id ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1302
    :cond_12c
    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationListForConversation(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    :try_end_135
    .catchall {:try_start_d2 .. :try_end_135} :catchall_384

    .line 1308
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 1463
    :cond_138
    :goto_138
    return-void

    .line 1267
    .end local v22           #participantCursor:Landroid/database/Cursor;
    .end local v23           #state:I
    .end local v24           #text:Ljava/lang/String;
    .end local v25           #timestamp:J
    :cond_139
    const-string v24, ""

    goto/16 :goto_11

    .line 1283
    .restart local v12       #clientMessageId:Ljava/lang/String;
    .restart local v23       #state:I
    .restart local v24       #text:Ljava/lang/String;
    .restart local v25       #timestamp:J
    :cond_13d
    const-string v5, "messages"

    const-string v6, "message_id=? AND conversation_id=?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_d0

    .line 1308
    .end local v12           #clientMessageId:Ljava/lang/String;
    .restart local v22       #participantCursor:Landroid/database/Cursor;
    :cond_159
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 1311
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 1312
    .local v27, values:Landroid/content/ContentValues;
    const/16 v16, 0x0

    .line 1313
    .local v16, imageUrl:Ljava/lang/String;
    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContent(I)Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->hasPhotoMetadata()Z

    move-result v3

    if-eqz v3, :cond_188

    .line 1314
    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContent(I)Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getPhotoMetadata()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->getUrl()Ljava/lang/String;

    move-result-object v16

    .line 1315
    const-string v3, "image_url"

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    :cond_188
    const-string v3, "conversation_id"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1318
    const-string v3, "author_id"

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    const-string v3, "text"

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    const-string v3, "status"

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1321
    const-string v3, "type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1322
    const-string v3, "timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1324
    const/16 v21, 0x0

    .line 1325
    .local v21, messageRowId:Ljava/lang/Long;
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->hasTimestamp()Z

    move-result v3

    if-eqz v3, :cond_1dd

    .line 1326
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v3

    move-object/from16 v0, p0

    move-wide/from16 v1, p5

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryMessageRowId(Landroid/database/sqlite/SQLiteDatabase;JJ)Ljava/lang/Long;

    move-result-object v21

    .line 1328
    :cond_1dd
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_208

    .line 1329
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "conversationRowId "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " messageRowId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1332
    :cond_208
    const/4 v11, 0x1

    .line 1333
    .local v11, allowUnhide:Z
    if-eqz v21, :cond_389

    .line 1334
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_21b

    .line 1335
    const-string v3, "EsConversationsData"

    const-string v4, "updating message"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1337
    :cond_21b
    const-string v3, "messages"

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1340
    const/4 v11, 0x0

    .line 1387
    :goto_231
    const-wide/16 v19, 0x0

    .line 1388
    .local v19, latestMessageTimestamp:J
    const-wide/16 v14, 0x0

    .line 1389
    .local v14, earliestEventTimestamp:J
    const/16 v17, 0x0

    .line 1390
    .local v17, isAwaitingEventStream:Z
    const/16 v18, 0x0

    .line 1391
    .local v18, isMuted:Z
    const/4 v13, 0x0

    .line 1393
    .local v13, conversationCursor:Landroid/database/Cursor;
    :try_start_23a
    const-string v4, "conversations"

    const/4 v3, 0x4

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "is_awaiting_event_stream"

    aput-object v6, v5, v3

    const/4 v3, 0x1

    const-string v6, "latest_message_timestamp"

    aput-object v6, v5, v3

    const/4 v3, 0x2

    const-string v6, "earliest_event_timestamp"

    aput-object v6, v5, v3

    const/4 v3, 0x3

    const-string v6, "is_muted"

    aput-object v6, v5, v3

    const-string v6, "_id=?"

    const/4 v3, 0x1

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static/range {p5 .. p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1401
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_298

    .line 1402
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_43e

    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_43e

    const/16 v17, 0x1

    .line 1404
    :goto_27e
    const/4 v3, 0x1

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    .line 1405
    const/4 v3, 0x2

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1406
    const/4 v3, 0x3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_442

    const/4 v3, 0x3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_293
    .catchall {:try_start_23a .. :try_end_293} :catchall_446

    move-result v3

    if-eqz v3, :cond_442

    const/16 v18, 0x1

    .line 1410
    :cond_298
    :goto_298
    if-eqz v13, :cond_29d

    .line 1411
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1415
    :cond_29d
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2c8

    .line 1416
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "new message timestamp "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v25

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " conversation latest "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v19

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1419
    :cond_2c8
    cmp-long v3, v25, v19

    if-ltz v3, :cond_138

    .line 1421
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2dc

    .line 1422
    const-string v3, "EsConversationsData"

    const-string v4, "updating latest message"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1424
    :cond_2dc
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    .line 1425
    if-eqz v11, :cond_2ed

    .line 1426
    const-string v3, "is_visible"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1428
    :cond_2ed
    const-string v3, "latest_message_text"

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    const-string v3, "latest_message_image_url"

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    const-string v3, "latest_message_author_id"

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1431
    const-string v3, "latest_message_timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1432
    const-string v3, "latest_message_type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1434
    if-nez v17, :cond_44d

    .line 1435
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_333

    .line 1436
    const-string v3, "EsConversationsData"

    const-string v4, "updating latest event timestamp"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1438
    :cond_333
    const-string v3, "latest_event_timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1450
    :cond_33e
    :goto_33e
    const-string v3, "conversations"

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p5 .. p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1454
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_138

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_373

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, p5, v3

    if-eqz v3, :cond_138

    :cond_373
    if-nez v18, :cond_138

    const/4 v3, 0x4

    move/from16 v0, v23

    if-eq v0, v3, :cond_37f

    const/4 v3, 0x3

    move/from16 v0, v23

    if-ne v0, v3, :cond_138

    .line 1460
    :cond_37f
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->setShouldTriggerNotifications()V

    goto/16 :goto_138

    .line 1308
    .end local v11           #allowUnhide:Z
    .end local v13           #conversationCursor:Landroid/database/Cursor;
    .end local v14           #earliestEventTimestamp:J
    .end local v16           #imageUrl:Ljava/lang/String;
    .end local v17           #isAwaitingEventStream:Z
    .end local v18           #isMuted:Z
    .end local v19           #latestMessageTimestamp:J
    .end local v21           #messageRowId:Ljava/lang/Long;
    .end local v27           #values:Landroid/content/ContentValues;
    :catchall_384
    move-exception v3

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    throw v3

    .line 1342
    .restart local v11       #allowUnhide:Z
    .restart local v16       #imageUrl:Ljava/lang/String;
    .restart local v21       #messageRowId:Ljava/lang/Long;
    .restart local v27       #values:Landroid/content/ContentValues;
    :cond_389
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_399

    .line 1343
    const-string v3, "EsConversationsData"

    const-string v4, "inserting message"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    :cond_399
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_3ab

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, p5, v3

    if-eqz v3, :cond_3be

    :cond_3ab
    const/4 v3, 0x5

    move/from16 v0, v23

    if-eq v0, v3, :cond_3be

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_431

    .line 1349
    :cond_3be
    const-string v3, "notification_seen"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1354
    :goto_3ca
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_413

    if-eqz p7, :cond_413

    const/4 v3, 0x3

    move/from16 v0, v23

    if-ne v0, v3, :cond_413

    .line 1356
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v3

    sget-object v5, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->DELIVERED:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-object/from16 v0, p4

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReceipt(Ljava/lang/String;JLcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 1358
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_413

    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, p5

    if-nez v3, :cond_413

    .line 1360
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v3

    sget-object v5, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->READ:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-object/from16 v0, p4

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendReceipt(Ljava/lang/String;JLcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 1365
    :cond_413
    sget-object v3, Lcom/google/android/apps/plus/content/EsConversationsData;->sHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/apps/plus/content/EsConversationsData$2;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/plus/content/EsConversationsData$2;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1385
    const-string v3, "messages"

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    goto/16 :goto_231

    .line 1351
    :cond_431
    const-string v3, "notification_seen"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_3ca

    .line 1402
    .restart local v13       #conversationCursor:Landroid/database/Cursor;
    .restart local v14       #earliestEventTimestamp:J
    .restart local v17       #isAwaitingEventStream:Z
    .restart local v18       #isMuted:Z
    .restart local v19       #latestMessageTimestamp:J
    :cond_43e
    const/16 v17, 0x0

    goto/16 :goto_27e

    .line 1406
    :cond_442
    const/16 v18, 0x0

    goto/16 :goto_298

    .line 1410
    :catchall_446
    move-exception v3

    if-eqz v13, :cond_44c

    .line 1411
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_44c
    throw v3

    .line 1439
    :cond_44d
    const-wide/16 v3, 0x0

    cmp-long v3, v14, v3

    if-nez v3, :cond_33e

    .line 1443
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_472

    .line 1444
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "updating earliest and latest event timestamp "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v25

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1446
    :cond_472
    const-string v3, "latest_event_timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1447
    const-string v3, "earliest_event_timestamp"

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_33e
.end method

.method private static processParticipantError(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;JJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 29
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "error"
    .parameter "conversationRowId"
    .parameter "timestamp"
    .parameter "sendReceipts"
    .parameter "operationState"

    .prologue
    .line 1581
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 1582
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processParticipantError status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    :cond_25
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;->hasFullName()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;->hasStatus()Z

    move-result v2

    if-nez v2, :cond_42

    .line 1587
    :cond_31
    const-string v2, "EsConversationsData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_41

    .line 1588
    const-string v2, "EsConversationsData"

    const-string v3, "Participant error with no name or status"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    :cond_41
    :goto_41
    return-void

    .line 1592
    :cond_42
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;->getFullName()Ljava/lang/String;

    move-result-object v16

    .line 1593
    .local v16, participantName:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v18

    .line 1595
    .local v18, status:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    sget-object v2, Lcom/google/android/apps/plus/content/EsConversationsData$4;->$SwitchMap$com$google$wireless$realtimechat$proto$Data$ResponseStatus:[I

    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_a6

    .line 1609
    const v17, 0x7f080208

    .line 1611
    .local v17, resourceId:I
    :goto_58
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<b>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "</b>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1613
    .local v7, text:Ljava/lang/String;
    const/4 v8, 0x5

    const/4 v9, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x4

    const/4 v14, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v5, p4

    move-wide/from16 v12, p6

    move-object/from16 v15, p9

    invoke-static/range {v2 .. v15}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertSystemMessage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;IZLjava/lang/String;IJZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    goto :goto_41

    .line 1597
    .end local v7           #text:Ljava/lang/String;
    .end local v17           #resourceId:I
    :pswitch_95
    const v17, 0x7f080204

    .line 1598
    .restart local v17       #resourceId:I
    goto :goto_58

    .line 1600
    .end local v17           #resourceId:I
    :pswitch_99
    const v17, 0x7f080205

    .line 1601
    .restart local v17       #resourceId:I
    goto :goto_58

    .line 1603
    .end local v17           #resourceId:I
    :pswitch_9d
    const v17, 0x7f080206

    .line 1604
    .restart local v17       #resourceId:I
    goto :goto_58

    .line 1606
    .end local v17           #resourceId:I
    :pswitch_a1
    const v17, 0x7f080207

    .line 1607
    .restart local v17       #resourceId:I
    goto :goto_58

    .line 1595
    nop

    :pswitch_data_a6
    .packed-switch 0x5
        :pswitch_95
        :pswitch_99
        :pswitch_9d
        :pswitch_a1
    .end packed-switch
.end method

.method private static processPreviewMessage$43824df1(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;Ljava/lang/String;J)V
    .registers 16
    .parameter "db"
    .parameter "message"
    .parameter "conversationId"
    .parameter "conversationRowId"

    .prologue
    .line 1630
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContentCount()I

    move-result v6

    if-lez v6, :cond_a2

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContent(I)Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getText()Ljava/lang/String;

    move-result-object v2

    .line 1631
    .local v2, text:Ljava/lang/String;
    :goto_f
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->determineMessageState(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)I

    move-result v1

    .line 1632
    .local v1, state:I
    const-string v6, "EsConversationsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_48

    .line 1633
    const-string v6, "EsConversationsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "processPreviewMessage conversationId: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " state: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " timestamp: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1637
    :cond_48
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getTimestamp()J

    move-result-wide v3

    .line 1638
    .local v3, timestamp:J
    const/4 v0, 0x0

    .line 1639
    .local v0, imageUrl:Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContent(I)Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Data$Content;->hasPhotoMetadata()Z

    move-result v6

    if-eqz v6, :cond_65

    .line 1640
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getContent(I)Lcom/google/wireless/realtimechat/proto/Data$Content;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Data$Content;->getPhotoMetadata()Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Data$PhotoMetadata;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 1643
    :cond_65
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1644
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "latest_message_text"

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1645
    const-string v6, "latest_message_image_url"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    const-string v6, "latest_message_author_id"

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getSenderId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1647
    const-string v6, "latest_message_timestamp"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1648
    const-string v6, "latest_message_type"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1651
    const-string v6, "conversations"

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1653
    return-void

    .line 1630
    .end local v0           #imageUrl:Ljava/lang/String;
    .end local v1           #state:I
    .end local v2           #text:Ljava/lang/String;
    .end local v3           #timestamp:J
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_a2
    const-string v2, ""

    goto/16 :goto_f
.end method

.method private static processReceipt$4fede216(Landroid/database/sqlite/SQLiteDatabase;JLcom/google/wireless/realtimechat/proto/Client$Receipt;)V
    .registers 21
    .parameter "db"
    .parameter "conversationRowId"
    .parameter "receipt"

    .prologue
    .line 1770
    const/4 v10, 0x0

    .line 1771
    .local v10, clientMessageId:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->hasMessageClientId()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1772
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getMessageClientId()Ljava/lang/String;

    move-result-object v10

    .line 1774
    :cond_b
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_50

    .line 1775
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processReceipt Conversation row id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", clientMessageId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getType()Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Timestamp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getMessageTimestamp()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1780
    :cond_50
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 1782
    .local v16, values:Landroid/content/ContentValues;
    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getMessageTimestamp()J

    move-result-wide v14

    .line 1784
    .local v14, timestamp:J
    const/4 v12, 0x0

    .line 1785
    .local v12, cursor:Landroid/database/Cursor;
    const/4 v11, 0x3

    .line 1786
    .local v11, currentStatus:I
    const/4 v13, 0x0

    .line 1788
    .local v13, messageRowId:Ljava/lang/Long;
    :try_start_5c
    const-string v3, "messages"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "status"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "_id"

    aput-object v5, v4, v2

    const-string v5, "conversation_id=? AND timestamp=?"

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1795
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_d1

    .line 1796
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 1797
    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_9a
    .catchall {:try_start_5c .. :try_end_9a} :catchall_10f

    move-result-object v13

    .line 1816
    :cond_9b
    :goto_9b
    if-eqz v12, :cond_a0

    .line 1817
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1821
    :cond_a0
    sget-object v2, Lcom/google/android/apps/plus/content/EsConversationsData$4;->$SwitchMap$com$google$wireless$realtimechat$proto$Client$Receipt$Type:[I

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getType()Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_148

    .line 1844
    :cond_af
    :goto_af
    if-eqz v13, :cond_d0

    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-eqz v2, :cond_d0

    .line 1845
    const-string v2, "messages"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1865
    :cond_d0
    return-void

    .line 1798
    :cond_d1
    if-eqz v10, :cond_9b

    .line 1802
    :try_start_d3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1803
    const-string v3, "messages"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "status"

    aput-object v5, v4, v2

    const-string v5, "conversation_id=? AND message_id=?"

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    aput-object v10, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1810
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9b

    .line 1811
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 1812
    const-string v2, "timestamp"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_10e
    .catchall {:try_start_d3 .. :try_end_10e} :catchall_10f

    goto :goto_9b

    .line 1816
    :catchall_10f
    move-exception v2

    if-eqz v12, :cond_115

    .line 1817
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_115
    throw v2

    .line 1825
    :pswitch_116
    const/4 v2, 0x4

    if-eq v11, v2, :cond_af

    const/4 v2, 0x5

    if-eq v11, v2, :cond_af

    .line 1827
    const-string v2, "status"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_af

    .line 1833
    :pswitch_129
    const/4 v2, 0x5

    if-eq v11, v2, :cond_af

    .line 1834
    const-string v2, "status"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_af

    .line 1839
    :pswitch_13a
    const-string v2, "status"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_af

    .line 1821
    :pswitch_data_148
    .packed-switch 0x1
        :pswitch_116
        :pswitch_129
        :pswitch_13a
    .end packed-switch
.end method

.method public static processSuggestionsResponse$541cf8e7(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "response"
    .parameter "request"

    .prologue
    const/4 v11, 0x3

    .line 119
    const-string v9, "EsConversationsData"

    invoke-static {v9, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 120
    const-string v9, "EsConversationsData"

    const-string v10, "processSuggestionsResponse"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_10
    invoke-virtual {p3}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasSuggestionsRequest()Z

    move-result v9

    if-eqz v9, :cond_32

    .line 124
    invoke-virtual {p3}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getSuggestionsRequest()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->getSuggestionsType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    move-result-object v9

    sget-object v10, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->ONLY_NEW:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    if-eq v9, v10, :cond_32

    .line 126
    const-string v9, "EsConversationsData"

    invoke-static {v9, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_31

    .line 127
    const-string v9, "EsConversationsData"

    const-string v10, "ignoring"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_31
    :goto_31
    return-void

    .line 132
    :cond_32
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 134
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 136
    :try_start_3d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .local v4, participantIds:Ljava/lang/StringBuilder;
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 138
    .local v8, values:Landroid/content/ContentValues;
    const/4 v5, 0x0

    .line 139
    .local v5, sequence:I
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getSuggestionList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_50
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_bf

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    .line 140
    .local v7, suggestion:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->getSuggestedUserList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    move v6, v5

    .end local v5           #sequence:I
    .local v6, sequence:I
    :goto_65
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_bd

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 141
    .local v3, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    if-lez v6, :cond_78

    .line 142
    const/16 v9, 0x2c

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 144
    :cond_78
    const-string v9, "\'"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 146
    const-string v9, "full_name"

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v9, "first_name"

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v9, "participant_id"

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v9, "sequence"

    add-int/lit8 v5, v6, 0x1

    .end local v6           #sequence:I
    .restart local v5       #sequence:I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    const-string v9, "messenger_suggestions"

    const/4 v10, 0x0

    const/4 v11, 0x5

    invoke-virtual {v0, v9, v10, v8, v11}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move v6, v5

    .end local v5           #sequence:I
    .restart local v6       #sequence:I
    goto :goto_65

    .end local v3           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_bd
    move v5, v6

    .end local v6           #sequence:I
    .restart local v5       #sequence:I
    goto :goto_50

    .line 156
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v7           #suggestion:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    :cond_bf
    const-string v9, "messenger_suggestions"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "participant_id NOT IN ("

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v0, v9, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 160
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e1
    .catchall {:try_start_3d .. :try_end_e1} :catchall_e9

    .line 162
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 164
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifySuggestionsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_31

    .line 162
    .end local v4           #participantIds:Ljava/lang/StringBuilder;
    .end local v5           #sequence:I
    .end local v8           #values:Landroid/content/ContentValues;
    :catchall_e9
    move-exception v9

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v9
.end method

.method private static processTileEvent(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$TileEvent;IZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 23
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "tileEvent"
    .parameter "messageState"
    .parameter "sendReceipts"
    .parameter "operationState"

    .prologue
    .line 2292
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getEventType()Ljava/lang/String;

    move-result-object v11

    .line 2293
    .local v11, eventType:Ljava/lang/String;
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getTimestamp()J

    move-result-wide v12

    .line 2294
    .local v12, timestamp:J
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_59

    .line 2295
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processTileEvent conversation id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Event Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Tile Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getTileType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Tile Version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getTileVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Timestamp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2301
    :cond_59
    const-string v2, "JOIN_HANGOUT"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7c

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    move-wide/from16 v6, p3

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    .line 2302
    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/plus/content/EsConversationsData;->processHangoutTile(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$TileEvent;JIZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    .line 2309
    :cond_72
    :goto_72
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getTimestamp()J

    move-result-wide v2

    move-wide/from16 v0, p3

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateLatestEventTimestamp(Landroid/database/sqlite/SQLiteDatabase;JJ)V

    .line 2310
    return-void

    .line 2305
    :cond_7c
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_72

    .line 2306
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processTileEvent for unexpected event type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_72
.end method

.method public static processUserCreationResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "response"
    .parameter "operationState"

    .prologue
    const/4 v5, 0x3

    .line 175
    const-string v4, "EsConversationsData"

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 176
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processUserCreationResponse participantId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getParticipantId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_21
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 182
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 184
    :try_start_2c
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryLoadConversationsSince(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v1

    .line 188
    .local v1, latestEventTimestamp:J
    const-string v4, "EsConversationsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4d

    .line 189
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "requesting conversations changed since "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_4d
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getConversationList(J)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 192
    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getParticipantId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getUserInfo(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 193
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getOOBSuggestionsRequest()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 199
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 200
    .local v3, values:Landroid/content/ContentValues;
    const-string v4, "key"

    const-string v5, "requested_conversations_since"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v4, "value"

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v4, "realtimechat_metadata"

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-virtual {v0, v4, v5, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 205
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_85
    .catchall {:try_start_2c .. :try_end_85} :catchall_89

    .line 207
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 208
    return-void

    .line 207
    .end local v1           #latestEventTimestamp:J
    .end local v3           #values:Landroid/content/ContentValues;
    :catchall_89
    move-exception v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method private static queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;
    .registers 7
    .parameter "db"
    .parameter "conversationRowId"

    .prologue
    .line 3905
    const-string v0, "EsConversationsData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 3906
    const-string v0, "EsConversationsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryConversationId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3908
    :cond_1d
    const-string v0, "SELECT conversation_id  FROM conversations WHERE _id=?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static queryConversationRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;
    .registers 12
    .parameter "db"
    .parameter "conversationId"

    .prologue
    const/4 v9, 0x0

    .line 3877
    const/4 v8, 0x0

    .line 3879
    .local v8, c:Landroid/database/Cursor;
    :try_start_2
    const-string v1, "conversations"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const-string v3, "conversation_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3884
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3885
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_34

    .line 3886
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_2d
    .catchall {:try_start_2 .. :try_end_2d} :catchall_3b

    move-result-object v0

    .line 3889
    if-eqz v8, :cond_33

    .line 3890
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3893
    :cond_33
    :goto_33
    return-object v0

    .line 3889
    :cond_34
    if-eqz v8, :cond_39

    .line 3890
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_39
    move-object v0, v9

    .line 3893
    goto :goto_33

    .line 3889
    :catchall_3b
    move-exception v0

    if-eqz v8, :cond_41

    .line 3890
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_41
    throw v0
.end method

.method public static queryDatastoreVersion(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 3548
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3550
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3552
    :try_start_b
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryDatastoreVersion(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;

    move-result-object v1

    .line 3553
    .local v1, version:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_12
    .catchall {:try_start_b .. :try_end_12} :catchall_16

    .line 3556
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object v1

    .end local v1           #version:Ljava/lang/String;
    :catchall_16
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private static queryDatastoreVersion(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;
    .registers 12
    .parameter "db"

    .prologue
    const/4 v10, 0x0

    .line 3564
    const/4 v9, 0x0

    .line 3566
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_2
    const-string v1, "realtimechat_metadata"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "value"

    aput-object v3, v2, v0

    const-string v3, "key = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "datastore_version"

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3571
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 3572
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_29
    .catchall {:try_start_2 .. :try_end_29} :catchall_37

    move-result-object v0

    .line 3575
    if-eqz v9, :cond_2f

    .line 3576
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3579
    :cond_2f
    :goto_2f
    return-object v0

    .line 3575
    :cond_30
    if-eqz v9, :cond_35

    .line 3576
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_35
    move-object v0, v10

    .line 3579
    goto :goto_2f

    .line 3575
    :catchall_37
    move-exception v0

    if-eqz v9, :cond_3d

    .line 3576
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3d
    throw v0
.end method

.method private static queryLoadConversationsSince(Landroid/database/sqlite/SQLiteDatabase;)J
    .registers 14
    .parameter "db"

    .prologue
    const-wide/16 v11, 0x0

    .line 3663
    const/4 v9, 0x0

    .line 3669
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_3
    const-string v1, "realtimechat_metadata"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "value"

    aput-object v3, v2, v0

    const-string v3, "key = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "requested_conversations_since"

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3674
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_89

    move-result v0

    if-eqz v0, :cond_5e

    .line 3676
    const/4 v0, 0x0

    :try_start_27
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_32
    .catchall {:try_start_27 .. :try_end_32} :catchall_89
    .catch Ljava/lang/NumberFormatException; {:try_start_27 .. :try_end_32} :catch_39

    move-result-wide v0

    .line 3685
    if-eqz v9, :cond_38

    .line 3686
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3704
    :cond_38
    :goto_38
    return-wide v0

    .line 3677
    :catch_39
    move-exception v10

    .line 3678
    .local v10, ex:Ljava/lang/NumberFormatException;
    :try_start_3a
    const-string v0, "EsConversationsData"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 3679
    const-string v0, "EsConversationsData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid requested conversations since "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_57
    .catchall {:try_start_3a .. :try_end_57} :catchall_89

    .line 3681
    :cond_57
    if-eqz v9, :cond_5c

    .line 3686
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_5c
    move-wide v0, v11

    goto :goto_38

    .line 3685
    .end local v10           #ex:Ljava/lang/NumberFormatException;
    :cond_5e
    if-eqz v9, :cond_63

    .line 3686
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3690
    :cond_63
    const/4 v9, 0x0

    .line 3692
    :try_start_64
    const-string v1, "conversations"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "MAX(latest_event_timestamp)"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3696
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_90

    .line 3697
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_82
    .catchall {:try_start_64 .. :try_end_82} :catchall_97

    move-result-wide v0

    .line 3700
    if-eqz v9, :cond_38

    .line 3701
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_38

    .line 3685
    :catchall_89
    move-exception v0

    if-eqz v9, :cond_8f

    .line 3686
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_8f
    throw v0

    .line 3700
    :cond_90
    if-eqz v9, :cond_95

    .line 3701
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_95
    move-wide v0, v11

    .line 3704
    goto :goto_38

    .line 3700
    :catchall_97
    move-exception v0

    if-eqz v9, :cond_9d

    .line 3701
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_9d
    throw v0
.end method

.method private static queryMessageRowId(Landroid/database/sqlite/SQLiteDatabase;JJ)Ljava/lang/Long;
    .registers 15
    .parameter "db"
    .parameter "conversationRowId"
    .parameter "timestamp"

    .prologue
    const/4 v9, 0x0

    .line 3927
    const/4 v8, 0x0

    .line 3929
    .local v8, c:Landroid/database/Cursor;
    :try_start_2
    const-string v1, "messages"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const-string v3, "conversation_id=? AND timestamp=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3936
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3937
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 3938
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_38
    .catchall {:try_start_2 .. :try_end_38} :catchall_46

    move-result-object v0

    .line 3941
    if-eqz v8, :cond_3e

    .line 3942
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3945
    :cond_3e
    :goto_3e
    return-object v0

    .line 3941
    :cond_3f
    if-eqz v8, :cond_44

    .line 3942
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_44
    move-object v0, v9

    .line 3945
    goto :goto_3e

    .line 3941
    :catchall_46
    move-exception v0

    if-eqz v8, :cond_4c

    .line 3942
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4c
    throw v0
.end method

.method private static queryMessageStatus(Landroid/database/sqlite/SQLiteDatabase;J)I
    .registers 12
    .parameter "db"
    .parameter "messageRowId"

    .prologue
    .line 3716
    const/4 v8, 0x0

    .line 3718
    .local v8, c:Landroid/database/Cursor;
    :try_start_1
    const-string v1, "messages"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "status"

    aput-object v3, v2, v0

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3723
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3724
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_31

    .line 3725
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_36

    move-result v0

    .line 3728
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3730
    :goto_30
    return v0

    .line 3728
    :cond_31
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3730
    const/4 v0, -0x1

    goto :goto_30

    .line 3728
    :catchall_36
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static queryOldestMessageTimestamp(Landroid/database/sqlite/SQLiteDatabase;J)J
    .registers 12
    .parameter "db"
    .parameter "conversationRowId"

    .prologue
    .line 3589
    const/4 v8, 0x0

    .line 3591
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1
    const-string v1, "messages"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "MIN(timestamp)"

    aput-object v3, v2, v0

    const-string v3, "conversation_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3596
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 3597
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_29
    .catchall {:try_start_1 .. :try_end_29} :catchall_38

    move-result-wide v0

    .line 3600
    if-eqz v8, :cond_2f

    .line 3601
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3604
    :cond_2f
    :goto_2f
    return-wide v0

    .line 3600
    :cond_30
    if-eqz v8, :cond_35

    .line 3601
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3604
    :cond_35
    const-wide/16 v0, 0x0

    goto :goto_2f

    .line 3600
    :catchall_38
    move-exception v0

    if-eqz v8, :cond_3e

    .line 3601
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3e
    throw v0
.end method

.method private static queryOneToOneConversation$51a85815(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;
    .registers 6
    .parameter "db"
    .parameter "participantId"

    .prologue
    .line 4014
    const/4 v0, 0x0

    .line 4016
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "select conversations"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "._id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",conversations"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "._id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " FROM conversations"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",conversation_participants"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE conversation_participants"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".conversation_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=conversations"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "._id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND is_group"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=0 AND participant_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4041
    .local v1, query:Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 4042
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_73

    .line 4043
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_6c
    .catchall {:try_start_1 .. :try_end_6c} :catchall_7a

    move-result-object v2

    .line 4046
    if-eqz v0, :cond_72

    .line 4047
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 4050
    :cond_72
    :goto_72
    return-object v2

    .line 4046
    :cond_73
    if-eqz v0, :cond_78

    .line 4047
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 4050
    :cond_78
    const/4 v2, 0x0

    goto :goto_72

    .line 4046
    .end local v1           #query:Ljava/lang/String;
    :catchall_7a
    move-exception v2

    if-eqz v0, :cond_80

    .line 4047
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_80
    throw v2
.end method

.method private static queryParticipantFirstName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "db"
    .parameter "participantId"

    .prologue
    const/4 v9, 0x0

    .line 3983
    if-nez p1, :cond_5

    move-object v0, v9

    .line 4000
    :cond_4
    :goto_4
    return-object v0

    .line 3986
    :cond_5
    const/4 v8, 0x0

    .line 3988
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_6
    const-string v1, "participants"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "first_name"

    aput-object v3, v2, v0

    const-string v3, "participant_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3992
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 3993
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2a
    .catchall {:try_start_6 .. :try_end_2a} :catchall_38

    move-result-object v0

    .line 3996
    if-eqz v8, :cond_4

    .line 3997
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 3996
    :cond_31
    if-eqz v8, :cond_36

    .line 3997
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_36
    move-object v0, v9

    .line 4000
    goto :goto_4

    .line 3996
    :catchall_38
    move-exception v0

    if-eqz v8, :cond_3e

    .line 3997
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3e
    throw v0
.end method

.method private static queryParticipantFullName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "db"
    .parameter "participantId"

    .prologue
    const/4 v9, 0x0

    .line 3957
    const/4 v8, 0x0

    .line 3959
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_2
    const-string v1, "participants"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "full_name"

    aput-object v3, v2, v0

    const-string v3, "participant_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3963
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 3964
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_26
    .catchall {:try_start_2 .. :try_end_26} :catchall_34

    move-result-object v0

    .line 3967
    if-eqz v8, :cond_2c

    .line 3968
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3971
    :cond_2c
    :goto_2c
    return-object v0

    .line 3967
    :cond_2d
    if-eqz v8, :cond_32

    .line 3968
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_32
    move-object v0, v9

    .line 3971
    goto :goto_2c

    .line 3967
    :catchall_34
    move-exception v0

    if-eqz v8, :cond_3a

    .line 3968
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3a
    throw v0
.end method

.method public static removeAllConversationsFromInviterLocally$37a126b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "inviterId"

    .prologue
    .line 3145
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 3146
    const-string v1, "EsConversationsData"

    const-string v2, "removeAllConversationsFromInviterLocally"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3148
    :cond_10
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3150
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3152
    :try_start_1b
    const-string v1, "conversations"

    const-string v2, "inviter_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3154
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2b
    .catchall {:try_start_1b .. :try_end_2b} :catchall_32

    .line 3156
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3158
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3159
    return-void

    .line 3156
    :catchall_32
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method private static removeExcessMessages(Landroid/database/sqlite/SQLiteDatabase;J)I
    .registers 15
    .parameter "db"
    .parameter "conversationRowId"

    .prologue
    .line 4646
    const-wide/16 v10, 0x0

    .line 4647
    .local v10, timestamp:J
    const/4 v9, 0x0

    .line 4649
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_3
    const-string v1, "messages"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "timestamp"

    aput-object v3, v2, v0

    const-string v3, "conversation_id= ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "timestamp DESC"

    const-string v8, "20,1"

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 4655
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 4656
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_4c

    move-result-wide v10

    .line 4659
    :cond_2f
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 4661
    const-string v0, "messages"

    const-string v1, "conversation_id=? AND timestamp<?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0

    .line 4659
    :catchall_4c
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static removeMessageLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "messageRowId"

    .prologue
    .line 3212
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 3213
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeMessageLocally  messageRowId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3216
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3218
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 3219
    .local v8, conversationRowId:Ljava/lang/Long;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3221
    const/4 v9, 0x0

    .line 3223
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_2a
    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3227
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5e

    .line 3228
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_5d
    .catchall {:try_start_2a .. :try_end_5d} :catchall_13b

    move-result-object v8

    .line 3231
    :cond_5e
    if-eqz v9, :cond_63

    .line 3232
    :try_start_60
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3236
    :cond_63
    const-string v1, "messages"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7c
    .catchall {:try_start_60 .. :try_end_7c} :catchall_142

    .line 3243
    :try_start_7c
    const-string v1, "messages"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "text"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "image_url"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "timestamp"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "author_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "conversation_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "timestamp DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3249
    :cond_b2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_c9

    .line 3250
    const/4 v1, 0x3

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 3251
    .local v10, messageType:I
    const/4 v1, 0x2

    if-eq v10, v1, :cond_c9

    const/4 v1, 0x1

    if-eq v10, v1, :cond_c9

    const/4 v1, 0x6

    if-eq v10, v1, :cond_c9

    const/4 v1, 0x3

    if-ne v10, v1, :cond_b2

    .line 3259
    .end local v10           #messageType:I
    :cond_c9
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 3260
    .local v11, values:Landroid/content/ContentValues;
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_147

    .line 3261
    const-string v1, "latest_message_text"

    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3263
    const-string v1, "latest_message_image_url"

    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3265
    const-string v1, "latest_message_timestamp"

    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3266
    const-string v1, "latest_message_type"

    const/4 v2, 0x3

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3267
    const-string v1, "latest_message_author_id"

    const/4 v2, 0x4

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3274
    :goto_10e
    const-string v1, "conversations"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v11, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_123
    .catchall {:try_start_7c .. :try_end_123} :catchall_164

    .line 3277
    if-eqz v9, :cond_128

    .line 3278
    :try_start_125
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3282
    :cond_128
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_12b
    .catchall {:try_start_125 .. :try_end_12b} :catchall_142

    .line 3284
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3286
    if-eqz v8, :cond_137

    .line 3287
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 3289
    :cond_137
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3290
    return-void

    .line 3231
    .end local v11           #values:Landroid/content/ContentValues;
    :catchall_13b
    move-exception v1

    if-eqz v9, :cond_141

    .line 3232
    :try_start_13e
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_141
    throw v1
    :try_end_142
    .catchall {:try_start_13e .. :try_end_142} :catchall_142

    .line 3284
    :catchall_142
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 3269
    .restart local v11       #values:Landroid/content/ContentValues;
    :cond_147
    :try_start_147
    const-string v1, "latest_message_text"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3270
    const-string v1, "latest_message_image_url"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3271
    const-string v1, "latest_message_type"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3272
    const-string v1, "latest_message_author_id"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_163
    .catchall {:try_start_147 .. :try_end_163} :catchall_164

    goto :goto_10e

    .line 3277
    .end local v11           #values:Landroid/content/ContentValues;
    :catchall_164
    move-exception v1

    if-eqz v9, :cond_16a

    .line 3278
    :try_start_167
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_16a
    throw v1
    :try_end_16b
    .catchall {:try_start_167 .. :try_end_16b} :catchall_142
.end method

.method public static requestOlderEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "operationState"

    .prologue
    .line 3096
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3098
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3100
    const/4 v9, 0x0

    .line 3102
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_c
    const-string v1, "conversations"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "earliest_event_timestamp"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3107
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_82

    .line 3108
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6d

    .line 3109
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requesting events since "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3113
    :cond_6d
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    sget v6, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->MAX_EVENTS_PER_REQUEST:I

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getEventStream(Ljava/lang/String;JJI)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v8

    .line 3116
    .local v8, command:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    invoke-virtual {p4, v8}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3118
    .end local v8           #command:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    :cond_82
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 3119
    .local v10, values:Landroid/content/ContentValues;
    const-string v1, "is_awaiting_older_events"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3120
    const-string v1, "conversations"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_aa
    .catchall {:try_start_c .. :try_end_aa} :catchall_b6

    .line 3124
    if-eqz v9, :cond_af

    .line 3125
    :try_start_ac
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3128
    :cond_af
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_b2
    .catchall {:try_start_ac .. :try_end_b2} :catchall_bd

    .line 3130
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3131
    return-void

    .line 3124
    .end local v10           #values:Landroid/content/ContentValues;
    :catchall_b6
    move-exception v1

    if-eqz v9, :cond_bc

    .line 3125
    :try_start_b9
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_bc
    throw v1
    :try_end_bd
    .catchall {:try_start_b9 .. :try_end_bd} :catchall_bd

    .line 3130
    :catchall_bd
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public static resendMessageLocally$65290203(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "messageRowId"
    .parameter "operationState"

    .prologue
    .line 2713
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 2714
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "resendMessageLocally messageRowId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2717
    :cond_1d
    const/4 v8, 0x0

    .line 2718
    .local v8, conversationRowId:Ljava/lang/Long;
    const/4 v10, 0x0

    .line 2719
    .local v10, result:Landroid/os/Bundle;
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2721
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2723
    const/4 v9, 0x0

    .line 2725
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_2b
    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2729
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5f

    .line 2730
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_5e
    .catchall {:try_start_2b .. :try_end_5e} :catchall_7d

    move-result-object v8

    .line 2733
    :cond_5f
    if-eqz v9, :cond_64

    .line 2734
    :try_start_61
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2738
    :cond_64
    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v1, p1

    move-wide v2, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->sendMessageInDatabase$728fb81e(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;JZZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;

    move-result-object v10

    .line 2740
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_70
    .catchall {:try_start_61 .. :try_end_70} :catchall_84

    .line 2742
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2744
    if-eqz v8, :cond_7c

    .line 2745
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2747
    :cond_7c
    return-object v10

    .line 2733
    :catchall_7d
    move-exception v1

    if-eqz v9, :cond_83

    .line 2734
    :try_start_80
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_83
    throw v1
    :try_end_84
    .catchall {:try_start_80 .. :try_end_84} :catchall_84

    .line 2742
    :catchall_84
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method private static sendMessageInDatabase$728fb81e(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;JZZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;
    .registers 39
    .parameter "db"
    .parameter "account"
    .parameter "messageRowId"
    .parameter "retry"
    .parameter "manual"
    .parameter "operationState"

    .prologue
    .line 4166
    const/16 v16, 0x0

    .line 4167
    .local v16, conversationRowId:Ljava/lang/Long;
    const/16 v23, 0x0

    .line 4168
    .local v23, messageId:Ljava/lang/String;
    const/16 v28, 0x0

    .line 4169
    .local v28, text:Ljava/lang/String;
    const/16 v20, 0x0

    .line 4170
    .local v20, imageUri:Ljava/lang/String;
    new-instance v26, Landroid/os/Bundle;

    invoke-direct/range {v26 .. v26}, Landroid/os/Bundle;-><init>()V

    .line 4172
    .local v26, result:Landroid/os/Bundle;
    const/16 v17, 0x0

    .line 4174
    .local v17, cursor:Landroid/database/Cursor;
    :try_start_f
    const-string v5, "messages"

    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "conversation_id"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "message_id"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "text"

    aput-object v7, v6, v4

    const/4 v4, 0x3

    const-string v7, "image_url"

    aput-object v7, v6, v4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "_id="

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 4181
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6b

    .line 4182
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 4183
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 4184
    const/4 v4, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 4185
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_6a
    .catchall {:try_start_f .. :try_end_6a} :catchall_1ca

    move-result-object v20

    .line 4188
    :cond_6b
    if-eqz v17, :cond_70

    .line 4189
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 4192
    :cond_70
    if-nez v16, :cond_91

    .line 4193
    const-string v4, "EsConversationsData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_91

    .line 4194
    const-string v4, "EsConversationsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No such message "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4200
    :cond_91
    const/4 v14, 0x0

    .line 4201
    .local v14, conversationId:Ljava/lang/String;
    const/4 v15, 0x0

    .line 4202
    .local v15, conversationName:Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long v21, v4, v6

    .line 4203
    .local v21, latestEventTimestamp:J
    const/16 v17, 0x0

    .line 4205
    :try_start_9d
    const-string v5, "conversations"

    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "conversation_id"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "name"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "generated_name"

    aput-object v7, v6, v4

    const/4 v4, 0x3

    const-string v7, "latest_message_timestamp"

    aput-object v7, v6, v4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "_id="

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 4212
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_fb

    .line 4213
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 4214
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 4215
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f4

    .line 4216
    const/4 v4, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 4218
    :cond_f4
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_fa
    .catchall {:try_start_9d .. :try_end_fa} :catchall_1d1

    move-result-wide v21

    .line 4221
    :cond_fb
    if-eqz v17, :cond_100

    .line 4222
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 4226
    :cond_100
    const-string v4, "conversation_id"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4227
    const-string v4, "conversation_name"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4228
    const-string v4, "message_row_id"

    move-object/from16 v0, v26

    move-wide/from16 v1, p2

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 4229
    const-string v4, "local_uri"

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4231
    if-eqz v20, :cond_12c

    const-string v4, "content://"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1fd

    .line 4236
    :cond_12c
    if-eqz v14, :cond_295

    const-string v4, "c:"

    invoke-virtual {v14, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_295

    .line 4240
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v12

    .line 4241
    .local v12, builder:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    invoke-virtual {v12, v14}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    .line 4242
    const/16 v25, 0x0

    .line 4244
    .local v25, participantCursor:Landroid/database/Cursor;
    :try_start_13f
    const-string v5, "participants_view"

    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "participant_id"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "first_name"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "full_name"

    aput-object v7, v6, v4

    const/4 v4, 0x3

    const-string v7, "type"

    aput-object v7, v6, v4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "conversation_id="

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 4251
    :goto_175
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1d8

    .line 4252
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v24

    .line 4253
    .local v24, participantBuilder:Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    .line 4254
    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 4255
    .local v18, firstName:Ljava/lang/String;
    if-eqz v18, :cond_19b

    .line 4256
    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    .line 4258
    :cond_19b
    const/4 v4, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 4259
    .local v19, fullName:Ljava/lang/String;
    if-eqz v19, :cond_1ab

    .line 4260
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    .line 4262
    :cond_1ab
    const/4 v4, 0x3

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->convertParticipantType(I)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    .line 4264
    invoke-virtual/range {v24 .. v24}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v4

    invoke-virtual {v12, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->addParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    :try_end_1c2
    .catchall {:try_start_13f .. :try_end_1c2} :catchall_1c3

    goto :goto_175

    .line 4273
    .end local v18           #firstName:Ljava/lang/String;
    .end local v19           #fullName:Ljava/lang/String;
    .end local v24           #participantBuilder:Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    :catchall_1c3
    move-exception v4

    if-eqz v25, :cond_1c9

    .line 4274
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_1c9
    throw v4

    .line 4188
    .end local v12           #builder:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .end local v14           #conversationId:Ljava/lang/String;
    .end local v15           #conversationName:Ljava/lang/String;
    .end local v21           #latestEventTimestamp:J
    .end local v25           #participantCursor:Landroid/database/Cursor;
    :catchall_1ca
    move-exception v4

    if-eqz v17, :cond_1d0

    .line 4189
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    :cond_1d0
    throw v4

    .line 4221
    .restart local v14       #conversationId:Ljava/lang/String;
    .restart local v15       #conversationName:Ljava/lang/String;
    .restart local v21       #latestEventTimestamp:J
    :catchall_1d1
    move-exception v4

    if-eqz v17, :cond_1d7

    .line 4222
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    :cond_1d7
    throw v4

    .line 4266
    .restart local v12       #builder:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .restart local v25       #participantCursor:Landroid/database/Cursor;
    :cond_1d8
    :try_start_1d8
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_28e

    .line 4267
    sget-object v4, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->GROUP:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    invoke-virtual {v12, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    :try_end_1e4
    .catchall {:try_start_1d8 .. :try_end_1e4} :catchall_1c3

    .line 4273
    :goto_1e4
    if-eqz v25, :cond_1e9

    .line 4274
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 4277
    :cond_1e9
    invoke-virtual {v12}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v13

    .line 4279
    .local v13, conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    const/4 v4, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    move/from16 v2, p4

    invoke-static {v13, v0, v1, v4, v2}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->createConversation(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 4287
    .end local v12           #builder:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .end local v13           #conversation:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .end local v25           #participantCursor:Landroid/database/Cursor;
    :cond_1fd
    :goto_1fd
    const-wide/16 v4, 0x1

    add-long v29, v21, v4

    .line 4288
    .local v29, timestamp:J
    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V

    .line 4289
    .local v31, values:Landroid/content/ContentValues;
    if-eqz p5, :cond_251

    .line 4290
    const-string v4, "latest_message_text"

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4291
    const-string v4, "latest_message_image_url"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4292
    const-string v4, "latest_message_author_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4294
    const-string v4, "latest_message_timestamp"

    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4295
    const-string v4, "latest_message_type"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4297
    const-string v4, "conversations"

    const-string v5, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4301
    :cond_251
    invoke-virtual/range {v31 .. v31}, Landroid/content/ContentValues;->clear()V

    .line 4302
    if-eqz p4, :cond_2a8

    const/16 v27, 0x7

    .line 4304
    .local v27, status:I
    :goto_258
    const-string v4, "status"

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4305
    if-eqz p5, :cond_270

    .line 4306
    const-string v4, "timestamp"

    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4308
    :cond_270
    const-string v4, "messages"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4311
    return-object v26

    .line 4269
    .end local v27           #status:I
    .end local v29           #timestamp:J
    .end local v31           #values:Landroid/content/ContentValues;
    .restart local v12       #builder:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .restart local v25       #participantCursor:Landroid/database/Cursor;
    :cond_28e
    :try_start_28e
    sget-object v4, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->ONE_TO_ONE:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    invoke-virtual {v12, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    :try_end_293
    .catchall {:try_start_28e .. :try_end_293} :catchall_1c3

    goto/16 :goto_1e4

    .line 4283
    .end local v12           #builder:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .end local v25           #participantCursor:Landroid/database/Cursor;
    :cond_295
    move-object/from16 v0, v23

    move-object/from16 v1, v28

    move-object/from16 v2, v20

    move/from16 v3, p4

    invoke-static {v14, v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    goto/16 :goto_1fd

    .line 4302
    .restart local v29       #timestamp:J
    .restart local v31       #values:Landroid/content/ContentValues;
    :cond_2a8
    const/16 v27, 0x1

    goto :goto_258
.end method

.method public static sendMessageLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J
    .registers 28
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "text"
    .parameter "uri"
    .parameter "connected"
    .parameter "operationState"

    .prologue
    .line 2528
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 2529
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendMessageLocally conversationRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2531
    :cond_1f
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2533
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2534
    const/4 v15, 0x0

    .line 2536
    .local v15, messageRowId:Ljava/lang/Long;
    :try_start_2b
    move-wide/from16 v0, p2

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v11

    .line 2537
    .local v11, conversationId:Ljava/lang/String;
    const/16 v16, 0x1

    .line 2538
    .local v16, status:I
    const-string v3, "c:"

    invoke-virtual {v11, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_174

    .line 2540
    const/16 v16, 0x0

    .line 2544
    :cond_3d
    :goto_3d
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "c:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x20

    invoke-static {v4}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_51
    .catchall {:try_start_2b .. :try_end_51} :catchall_181

    move-result-object v10

    .line 2547
    .local v10, clientMessageId:Ljava/lang/String;
    const-wide/16 v13, 0x0

    .line 2548
    .local v13, latestMessageTimestamp:J
    const/4 v12, 0x0

    .line 2550
    .local v12, cursor:Landroid/database/Cursor;
    :try_start_55
    const-string v3, "messages"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "MAX(timestamp)"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "conversation_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 2554
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_85

    .line 2555
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_84
    .catchall {:try_start_55 .. :try_end_84} :catchall_17a

    move-result-wide v13

    .line 2558
    :cond_85
    if-eqz v12, :cond_8a

    .line 2559
    :try_start_87
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 2562
    :cond_8a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v17, v3, v5

    .line 2563
    .local v17, timestamp:J
    cmp-long v3, v17, v13

    if-gtz v3, :cond_9a

    .line 2564
    const-wide/16 v3, 0x1

    add-long v17, v13, v3

    .line 2571
    :cond_9a
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 2572
    .local v19, values:Landroid/content/ContentValues;
    const-string v3, "latest_message_text"

    move-object/from16 v0, v19

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573
    const-string v3, "latest_message_image_url"

    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2574
    const-string v3, "latest_message_author_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576
    const-string v3, "latest_message_timestamp"

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2577
    const-string v3, "latest_message_type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2579
    const-string v3, "conversations"

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2582
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentValues;->clear()V

    .line 2583
    const-string v3, "message_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2584
    const-string v3, "conversation_id"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2585
    const-string v3, "author_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586
    const-string v3, "text"

    move-object/from16 v0, v19

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2587
    const-string v3, "status"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2588
    const-string v3, "type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2589
    const-string v3, "timestamp"

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2590
    const-string v3, "notification_seen"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2591
    const-string v3, "image_url"

    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2592
    const-string v3, "messages"

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 2594
    if-eqz p6, :cond_163

    .line 2595
    const/4 v3, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-static {v11, v10, v0, v1, v3}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 2599
    :cond_163
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_166
    .catchall {:try_start_87 .. :try_end_166} :catchall_181

    .line 2601
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2603
    invoke-static/range {p0 .. p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2604
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2605
    if-nez v15, :cond_186

    const-wide/16 v3, -0x1

    :goto_173
    return-wide v3

    .line 2541
    .end local v10           #clientMessageId:Ljava/lang/String;
    .end local v12           #cursor:Landroid/database/Cursor;
    .end local v13           #latestMessageTimestamp:J
    .end local v17           #timestamp:J
    .end local v19           #values:Landroid/content/ContentValues;
    :cond_174
    if-nez p6, :cond_3d

    .line 2542
    const/16 v16, 0x2

    goto/16 :goto_3d

    .line 2558
    .restart local v10       #clientMessageId:Ljava/lang/String;
    .restart local v12       #cursor:Landroid/database/Cursor;
    .restart local v13       #latestMessageTimestamp:J
    :catchall_17a
    move-exception v3

    if-eqz v12, :cond_180

    .line 2559
    :try_start_17d
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_180
    throw v3
    :try_end_181
    .catchall {:try_start_17d .. :try_end_181} :catchall_181

    .line 2601
    .end local v10           #clientMessageId:Ljava/lang/String;
    .end local v11           #conversationId:Ljava/lang/String;
    .end local v12           #cursor:Landroid/database/Cursor;
    .end local v13           #latestMessageTimestamp:J
    .end local v16           #status:I
    :catchall_181
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 2605
    .restart local v10       #clientMessageId:Ljava/lang/String;
    .restart local v11       #conversationId:Ljava/lang/String;
    .restart local v12       #cursor:Landroid/database/Cursor;
    .restart local v13       #latestMessageTimestamp:J
    .restart local v16       #status:I
    .restart local v17       #timestamp:J
    .restart local v19       #values:Landroid/content/ContentValues;
    :cond_186
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    goto :goto_173
.end method

.method public static sendPresenceRequestLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "isPresent"
    .parameter "reciprocate"
    .parameter "operationState"

    .prologue
    .line 3334
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 3335
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendPresenceRequestLocally  conversationRowId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isPresent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3339
    :cond_27
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3341
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3343
    :try_start_32
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v0

    .line 3344
    .local v0, conversationId:Ljava/lang/String;
    invoke-static {v0, p4, p5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->presenceRequest(Ljava/lang/String;ZZ)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    invoke-virtual {p6, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3346
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_40
    .catchall {:try_start_32 .. :try_end_40} :catchall_44

    .line 3348
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3349
    return-void

    .line 3348
    .end local v0           #conversationId:Ljava/lang/String;
    :catchall_44
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static sendTypingRequestLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "typingType"
    .parameter "operationState"

    .prologue
    .line 3303
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 3304
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendTypingRequestLocally  conversationRowId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " typingType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p4}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3308
    :cond_2b
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3310
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3312
    :try_start_36
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v0

    .line 3313
    .local v0, conversationId:Ljava/lang/String;
    invoke-static {v0, p4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->typingRequest(Ljava/lang/String;Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    invoke-virtual {p5, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3315
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_44
    .catchall {:try_start_36 .. :try_end_44} :catchall_48

    .line 3317
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3318
    return-void

    .line 3317
    .end local v0           #conversationId:Ljava/lang/String;
    :catchall_48
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static setMessageStatusLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "messageRowId"
    .parameter "status"

    .prologue
    .line 3170
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 3171
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setMessageStatusLocally  messageRowId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3174
    :cond_1d
    const/4 v8, 0x0

    .line 3175
    .local v8, conversationRowId:Ljava/lang/Long;
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3177
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3179
    const/4 v9, 0x0

    .line 3181
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_2a
    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3185
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5e

    .line 3186
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_5d
    .catchall {:try_start_2a .. :try_end_5d} :catchall_78

    move-result-object v8

    .line 3189
    :cond_5e
    if-eqz v9, :cond_63

    .line 3190
    :try_start_60
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3193
    :cond_63
    const/16 v1, 0x8

    invoke-static {v0, p2, p3, v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateMessageStatus$4372adf(Landroid/database/sqlite/SQLiteDatabase;JI)V

    .line 3194
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6b
    .catchall {:try_start_60 .. :try_end_6b} :catchall_7f

    .line 3196
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3198
    if-eqz v8, :cond_77

    .line 3199
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 3201
    :cond_77
    return-void

    .line 3189
    :catchall_78
    move-exception v1

    if-eqz v9, :cond_7e

    .line 3190
    :try_start_7b
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_7e
    throw v1
    :try_end_7f
    .catchall {:try_start_7b .. :try_end_7f} :catchall_7f

    .line 3196
    :catchall_7f
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method private static final syncParticipants(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;JZLcom/google/wireless/realtimechat/proto/Client$ClientConversation;)V
    .registers 24
    .parameter "db"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "isConversationGroup"
    .parameter "conversation"

    .prologue
    .line 4879
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 4880
    .local v12, values:Landroid/content/ContentValues;
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getParticipantList()Ljava/util/List;

    move-result-object v9

    .line 4882
    .local v9, participants:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-virtual/range {p5 .. p5}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getInactiveParticipantList()Ljava/util/List;

    move-result-object v6

    .line 4886
    .local v6, inactiveParticipants:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 4887
    .local v10, sb:Ljava/lang/StringBuilder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4888
    .local v1, args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 4889
    .local v2, count:I
    const-string v13, "conversation_id=?"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4890
    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4891
    const-string v13, " AND participant_id NOT IN ("

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4892
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, i$:Ljava/util/Iterator;
    :cond_2d
    :goto_2d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 4893
    .local v8, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasParticipantId()Z

    move-result v13

    if-eqz v13, :cond_2d

    .line 4894
    add-int/lit8 v2, v2, 0x1

    .line 4895
    const-string v13, "?,"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4896
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2d

    .line 4899
    .end local v8           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_4e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 4900
    const-string v13, ")"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4901
    const-string v13, "active"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4902
    if-lez v2, :cond_7c

    .line 4903
    const-string v14, "conversation_participants"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/String;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v12, v15, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4908
    :cond_7c
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 4909
    .local v7, packedParticipantBuilder:Ljava/lang/StringBuilder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 4910
    .local v4, generatedName:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 4911
    const/4 v3, 0x1

    .line 4912
    .local v3, firstParticipant:Z
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_18d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 4913
    .restart local v8       #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    const/4 v13, 0x5

    if-ge v2, v13, :cond_ca

    .line 4914
    if-lez v2, :cond_a2

    .line 4915
    const/16 v13, 0x7c

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4917
    :cond_a2
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4919
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_c8

    .line 4921
    if-nez v3, :cond_be

    .line 4922
    const-string v13, ", "

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4924
    :cond_be
    const/4 v3, 0x0

    .line 4925
    if-eqz p4, :cond_184

    .line 4926
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4931
    :cond_c8
    :goto_c8
    add-int/lit8 v2, v2, 0x1

    .line 4934
    :cond_ca
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 4935
    const-string v13, "participant_id"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4936
    const-string v13, "full_name"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4937
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_fa

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_fa

    .line 4938
    const-string v13, "first_name"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4940
    :cond_fa
    const-string v13, "type"

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsConversationsData;->convertParticipantType(Lcom/google/wireless/realtimechat/proto/Data$Participant;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4941
    const-string v13, "participants"

    const-string v14, "participant_id=?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    .line 4944
    .local v11, updateCount:I
    const-string v13, "EsConversationsData"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_147

    .line 4945
    const-string v13, "EsConversationsData"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Update Participant "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4948
    :cond_147
    if-nez v11, :cond_151

    .line 4949
    const-string v13, "participants"

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14, v12}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 4952
    :cond_151
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 4953
    const-string v13, "conversation_id"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4954
    const-string v13, "participant_id"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4956
    const-string v13, "active"

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4957
    const-string v13, "sequence"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4958
    const-string v13, "conversation_participants"

    const/4 v14, 0x0

    const/4 v15, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14, v12, v15}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto/16 :goto_8c

    .line 4928
    .end local v11           #updateCount:I
    :cond_184
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c8

    .line 4962
    .end local v8           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_18d
    if-eqz v6, :cond_250

    .line 4963
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_193
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_250

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 4964
    .restart local v8       #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 4965
    const-string v13, "participant_id"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4966
    const-string v13, "full_name"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4967
    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_1cf

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1cf

    .line 4968
    const-string v13, "first_name"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4970
    :cond_1cf
    const-string v13, "type"

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsConversationsData;->convertParticipantType(Lcom/google/wireless/realtimechat/proto/Data$Participant;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4971
    const-string v13, "participants"

    const-string v14, "participant_id=?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    .line 4974
    .restart local v11       #updateCount:I
    const-string v13, "EsConversationsData"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_21c

    .line 4975
    const-string v13, "EsConversationsData"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Update Participant "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4977
    :cond_21c
    if-nez v11, :cond_226

    .line 4978
    const-string v13, "participants"

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14, v12}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 4981
    :cond_226
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 4982
    const-string v13, "conversation_id"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4983
    const-string v13, "participant_id"

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4985
    const-string v13, "active"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4986
    const-string v13, "conversation_participants"

    const/4 v14, 0x0

    const/4 v15, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14, v12, v15}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto/16 :goto_193

    .line 4991
    .end local v8           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v11           #updateCount:I
    :cond_250
    const-string v13, "EsConversationsData"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_271

    .line 4992
    const-string v13, "EsConversationsData"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "generatedName "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4994
    :cond_271
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 4995
    const-string v13, "generated_name"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4996
    const-string v13, "packed_participants"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4998
    const-string v13, "conversations"

    const-string v14, "_id=?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5001
    return-void
.end method

.method private static final updateConversationId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .registers 19
    .parameter "db"
    .parameter "from"
    .parameter "to"

    .prologue
    .line 4816
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 4817
    .local v15, values:Landroid/content/ContentValues;
    const-string v2, "conversations"

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "_id"

    aput-object v4, v3, v1

    const-string v4, "conversation_id=?"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v5, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 4822
    .local v14, toCursor:Landroid/database/Cursor;
    :try_start_20
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_c1

    .line 4823
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_45

    .line 4824
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Already have a conversation Id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4826
    :cond_45
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 4827
    .local v12, oldConversationRowId:J
    const-string v2, "conversations"

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "_id"

    aput-object v4, v3, v1

    const-string v4, "conversation_id=?"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v5, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_64
    .catchall {:try_start_20 .. :try_end_64} :catchall_e3

    move-result-object v9

    .line 4832
    .local v9, fromCursor:Landroid/database/Cursor;
    :try_start_65
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_be

    .line 4833
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 4836
    .local v10, newConversationRowId:J
    const-string v1, "conversation_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4837
    const-string v1, "messages"

    const-string v2, "conversation_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4841
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 4842
    const-string v1, "conversation_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v15, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4844
    const-string v1, "conversation_participants"

    const-string v2, "conversation_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4849
    const-string v1, "conversations"

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_be
    .catchall {:try_start_65 .. :try_end_be} :catchall_de

    .line 4854
    .end local v10           #newConversationRowId:J
    :cond_be
    :try_start_be
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_c1
    .catchall {:try_start_be .. :try_end_c1} :catchall_e3

    .line 4858
    .end local v9           #fromCursor:Landroid/database/Cursor;
    .end local v12           #oldConversationRowId:J
    :cond_c1
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 4860
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 4861
    const-string v1, "conversation_id"

    move-object/from16 v0, p2

    invoke-virtual {v15, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4862
    const-string v1, "conversations"

    const-string v2, "conversation_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4865
    return-void

    .line 4854
    .restart local v9       #fromCursor:Landroid/database/Cursor;
    .restart local v12       #oldConversationRowId:J
    :catchall_de
    move-exception v1

    :try_start_df
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_e3
    .catchall {:try_start_df .. :try_end_e3} :catchall_e3

    .line 4858
    .end local v9           #fromCursor:Landroid/database/Cursor;
    .end local v12           #oldConversationRowId:J
    :catchall_e3
    move-exception v1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static updateConversationMutedLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "muted"
    .parameter "operationState"

    .prologue
    .line 3017
    const-string v3, "EsConversationsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 3018
    const-string v3, "EsConversationsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "updateConversationMutedLocally conversationRowId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " muted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3023
    :cond_27
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3025
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3027
    :try_start_32
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3028
    .local v2, values:Landroid/content/ContentValues;
    const-string v4, "is_muted"

    if-eqz p4, :cond_71

    const/4 v3, 0x1

    :goto_3c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3029
    const-string v3, "conversations"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3033
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v0

    .line 3034
    .local v0, conversationId:Ljava/lang/String;
    invoke-static {v0, p4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->setConversationMuted(Ljava/lang/String;Z)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    invoke-virtual {p5, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 3036
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6a
    .catchall {:try_start_32 .. :try_end_6a} :catchall_73

    .line 3038
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3040
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3041
    return-void

    .line 3028
    .end local v0           #conversationId:Ljava/lang/String;
    :cond_71
    const/4 v3, 0x0

    goto :goto_3c

    .line 3038
    .end local v2           #values:Landroid/content/ContentValues;
    :catchall_73
    move-exception v3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method public static updateConversationNameLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "name"
    .parameter "operationState"

    .prologue
    .line 2987
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2988
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateConversationNameLocally conversationRowId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2991
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2993
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2995
    :try_start_28
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryConversationId(Landroid/database/sqlite/SQLiteDatabase;J)Ljava/lang/String;

    move-result-object v0

    .line 2996
    .local v0, conversationId:Ljava/lang/String;
    invoke-static {v0, p4}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->setConversationName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    invoke-virtual {p5, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    .line 2998
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_36
    .catchall {:try_start_28 .. :try_end_36} :catchall_3d

    .line 3000
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3002
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyConversationsChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3003
    return-void

    .line 3000
    .end local v0           #conversationId:Ljava/lang/String;
    :catchall_3d
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private static updateLatestEventTimestamp(Landroid/database/sqlite/SQLiteDatabase;JJ)V
    .registers 25
    .parameter "db"
    .parameter "conversationRowId"
    .parameter "timestamp"

    .prologue
    .line 5012
    const-string v2, "EsConversationsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 5013
    const-string v2, "EsConversationsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateLatestEventTimestamp ConversationRowId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Timestamp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5017
    :cond_2b
    const-wide/16 v17, 0x0

    .line 5018
    .local v17, oldTimestamp:J
    const-wide/16 v15, 0x0

    .line 5019
    .local v15, oldLatestMessage:J
    const/4 v13, 0x0

    .line 5020
    .local v13, isAwaitingEventStream:Z
    const/4 v12, 0x0

    .line 5021
    .local v12, isAwaitingConversationList:Z
    const/4 v14, 0x0

    .line 5023
    .local v14, metadataCursor:Landroid/database/Cursor;
    :try_start_32
    const-string v3, "realtimechat_metadata"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "value"

    aput-object v5, v4, v2

    const-string v5, "key = ?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "awaiting_conversation_list"

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 5028
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 5029
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_5a
    .catchall {:try_start_32 .. :try_end_5a} :catchall_fb

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5f

    .line 5030
    const/4 v12, 0x1

    .line 5034
    :cond_5f
    if-eqz v14, :cond_64

    .line 5035
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 5039
    :cond_64
    const/4 v11, 0x0

    .line 5041
    .local v11, conversationCursor:Landroid/database/Cursor;
    :try_start_65
    const-string v3, "conversations"

    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "is_awaiting_event_stream"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "latest_event_timestamp"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "latest_message_timestamp"

    aput-object v5, v4, v2

    const-string v5, "_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 5048
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_ad

    .line 5049
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_102

    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_102

    const/4 v13, 0x1

    .line 5051
    :goto_a3
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 5052
    const/4 v2, 0x2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_ac
    .catchall {:try_start_65 .. :try_end_ac} :catchall_104

    move-result-wide v15

    .line 5055
    :cond_ad
    if-eqz v11, :cond_b2

    .line 5056
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 5060
    :cond_b2
    cmp-long v2, p3, v17

    if-lez v2, :cond_fa

    .line 5061
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 5062
    .local v19, values:Landroid/content/ContentValues;
    if-nez v13, :cond_ca

    if-nez v12, :cond_ca

    .line 5063
    const-string v2, "latest_event_timestamp"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5065
    :cond_ca
    const-wide/16 v2, 0x0

    cmp-long v2, v17, v2

    if-lez v2, :cond_df

    cmp-long v2, v15, p3

    if-lez v2, :cond_df

    .line 5072
    const-string v2, "latest_message_timestamp"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5074
    :cond_df
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-lez v2, :cond_fa

    .line 5075
    const-string v2, "conversations"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5080
    .end local v19           #values:Landroid/content/ContentValues;
    :cond_fa
    return-void

    .line 5034
    .end local v11           #conversationCursor:Landroid/database/Cursor;
    :catchall_fb
    move-exception v2

    if-eqz v14, :cond_101

    .line 5035
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_101
    throw v2

    .line 5049
    .restart local v11       #conversationCursor:Landroid/database/Cursor;
    :cond_102
    const/4 v13, 0x0

    goto :goto_a3

    .line 5055
    :catchall_104
    move-exception v2

    if-eqz v11, :cond_10a

    .line 5056
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_10a
    throw v2
.end method

.method private static updateMessageStatus$4372adf(Landroid/database/sqlite/SQLiteDatabase;JI)V
    .registers 10
    .parameter "db"
    .parameter "messageRowId"
    .parameter "status"

    .prologue
    .line 4563
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4564
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4565
    const-string v1, "messages"

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4568
    return-void
.end method

.method public static updateMessageUriAndSendLocally$4f1d5505(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJLjava/lang/String;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "messageRowId"
    .parameter "newUri"
    .parameter "operationState"

    .prologue
    .line 2766
    const-string v1, "EsConversationsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 2767
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updateMessageUriAndSendLocally messageRowId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2769
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2771
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2773
    :try_start_28
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2774
    .local v7, values:Landroid/content/ContentValues;
    const-string v1, "image_url"

    invoke-virtual {v7, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2775
    const-string v1, "messages"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2779
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-wide v2, p4

    move-object v6, p7

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->sendMessageInDatabase$728fb81e(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;JZZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;

    .line 2782
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_56
    .catchall {:try_start_28 .. :try_end_56} :catchall_5d

    .line 2784
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2786
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsConversationsData;->notifyMessagesChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 2787
    return-void

    .line 2784
    .end local v7           #values:Landroid/content/ContentValues;
    :catchall_5d
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method private static updatePreviewSystemMessage$5091a27(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;IJ)V
    .registers 14
    .parameter "db"
    .parameter "conversationRowId"
    .parameter "text"
    .parameter "senderId"
    .parameter "messageType"
    .parameter "timestamp"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x3

    .line 4532
    const-string v1, "EsConversationsData"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 4533
    const-string v1, "EsConversationsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updatePreviewSystemMessage  messageType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " senderId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " timestamp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4538
    :cond_32
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4539
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "is_visible"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4540
    const-string v1, "latest_message_timestamp"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4541
    const-string v1, "latest_message_author_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4542
    const-string v1, "latest_message_text"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4543
    const-string v1, "latest_message_image_url"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4544
    const-string v1, "latest_message_type"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4545
    const-string v1, "conversations"

    const-string v2, "_id=?"

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4548
    return-void
.end method
