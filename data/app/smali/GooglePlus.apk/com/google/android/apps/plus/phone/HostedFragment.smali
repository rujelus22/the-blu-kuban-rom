.class public abstract Lcom/google/android/apps/plus/phone/HostedFragment;
.super Landroid/support/v4/app/Fragment;
.source "HostedFragment.java"


# instance fields
.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mCalled:Z

.field private mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

.field private mEndViewExtras:Landroid/os/Bundle;

.field private mPaused:Z

.field private mRecorded:Z

.field private mStartTime:J

.field private mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

.field private mStartViewExtras:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 2
    .parameter "actionBar"

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    .line 81
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 82
    return-void
.end method

.method public final clearNavigationAction()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    .line 192
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 193
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartViewExtras:Landroid/os/Bundle;

    .line 194
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartTime:J

    .line 195
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 196
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndViewExtras:Landroid/os/Bundle;

    .line 197
    return-void
.end method

.method public final detachActionBar()V
    .registers 2

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    .line 89
    return-void
.end method

.method public abstract getAccount()Lcom/google/android/apps/plus/content/EsAccount;
.end method

.method public final getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    return-object v0
.end method

.method public getExtrasForLogging()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 178
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
.end method

.method protected final invalidateActionBar()V
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz v0, :cond_9

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->invalidateActionBar()V

    .line 138
    :cond_9
    return-void
.end method

.method protected final isPaused()Z
    .registers 2

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mPaused:Z

    return v0
.end method

.method protected needsAsyncData()Z
    .registers 2

    .prologue
    .line 246
    const/4 v0, 0x0

    return v0
.end method

.method public onActionButtonClicked(I)V
    .registers 2
    .parameter "actionId"

    .prologue
    .line 129
    return-void
.end method

.method protected onAsyncData()V
    .registers 9

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    .line 231
    .local v1, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v1, :cond_27

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    if-nez v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    if-eqz v0, :cond_27

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    iget-wide v4, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartViewExtras:Landroid/os/Bundle;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndViewExtras:Landroid/os/Bundle;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    .line 237
    :cond_27
    return-void
.end method

.method public onBackPressed()Z
    .registers 2

    .prologue
    .line 260
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mCalled:Z

    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 40
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mCalled:Z

    if-nez v0, :cond_19

    .line 41
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did you forget to call super.onSetArguments()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_19
    return-void
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 53
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mPaused:Z

    .line 55
    return-void
.end method

.method protected onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 2
    .parameter "actionBar"

    .prologue
    .line 105
    return-void
.end method

.method public onPrimarySpinnerSelectionChange(I)V
    .registers 2
    .parameter "position"

    .prologue
    .line 111
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 47
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mPaused:Z

    .line 49
    return-void
.end method

.method protected onSetArguments(Landroid/os/Bundle;)V
    .registers 3
    .parameter "args"

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mCalled:Z

    .line 62
    return-void
.end method

.method public onUpButtonClicked()Z
    .registers 2

    .prologue
    .line 267
    const/4 v0, 0x0

    return v0
.end method

.method public recordNavigationAction()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p0

    move-object v3, v1

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/HostedFragment;->recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 165
    return-void
.end method

.method public final recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;JLandroid/os/Bundle;)V
    .registers 11
    .parameter "startView"
    .parameter "startTime"
    .parameter "startViewExtras"

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/HostedFragment;->recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 170
    return-void
.end method

.method public final recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .registers 14
    .parameter "startView"
    .parameter "endView"
    .parameter "startTime"
    .parameter "startViewExtras"
    .parameter "endViewExtras"

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->needsAsyncData()Z

    move-result v0

    if-nez v0, :cond_23

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    .line 210
    .local v1, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v1, :cond_22

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    if-nez v0, :cond_22

    if-eqz p2, :cond_22

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    .line 223
    .end local v1           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_22
    :goto_22
    return-void

    .line 217
    :cond_23
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 218
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartViewExtras:Landroid/os/Bundle;

    .line 219
    if-nez p3, :cond_34

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    :goto_2d
    iput-wide v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartTime:J

    .line 220
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 221
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndViewExtras:Landroid/os/Bundle;

    goto :goto_22

    .line 219
    :cond_34
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_2d
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 5
    .parameter "action"

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 147
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_11

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v1, v0, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 150
    :cond_11
    return-void
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
    .registers 6
    .parameter "action"
    .parameter "extras"

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 154
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_11

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v1, v0, p1, v2, p2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    .line 158
    :cond_11
    return-void
.end method

.method public refresh()V
    .registers 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageCache;->clearFailedRequests()V

    .line 121
    return-void
.end method
