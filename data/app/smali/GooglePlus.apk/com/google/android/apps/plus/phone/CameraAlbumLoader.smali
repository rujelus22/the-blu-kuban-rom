.class public Lcom/google/android/apps/plus/phone/CameraAlbumLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "CameraAlbumLoader.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/Pageable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/CameraAlbumLoader$PhotoQuery;,
        Lcom/google/android/apps/plus/phone/CameraAlbumLoader$CorrectedMediaStoreColumn;
    }
.end annotation


# static fields
.field protected static final sMediaStoreUri:[Landroid/net/Uri;


# instance fields
.field private mHasMore:Z

.field private mLoadLimit:I

.field private mPageable:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 110
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_IMAGES_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_VIDEO_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sMediaStoreUri:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 4
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v0, 0x1

    .line 133
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;-><init>(Landroid/content/Context;ZI)V

    .line 134
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ZI)V
    .registers 6
    .parameter "context"
    .parameter "paging"
    .parameter "initialPageCount"

    .prologue
    const/16 v0, 0x10

    .line 138
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 123
    iput v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    .line 139
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    .line 140
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v1, :cond_12

    :goto_f
    iput v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    .line 142
    return-void

    .line 140
    :cond_12
    const/4 v0, -0x1

    goto :goto_f
.end method


# virtual methods
.method protected buildMatrixCursor(Landroid/content/Context;[Landroid/database/Cursor;[Landroid/net/Uri;)Landroid/database/Cursor;
    .registers 23
    .parameter "context"
    .parameter "cursorList"
    .parameter "baseUri"

    .prologue
    .line 208
    new-instance v9, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v16, Lcom/google/android/apps/plus/phone/AlbumViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 212
    .local v9, returnCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    :goto_9
    const-wide/16 v11, -0x1

    .line 213
    .local v11, timestamp:J
    const/4 v3, -0x1

    .line 216
    .local v3, cursorIndex:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_d
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v6, v0, :cond_3e

    .line 217
    aget-object v1, p2, v6

    .line 218
    .local v1, c:Landroid/database/Cursor;
    if-eqz v1, :cond_32

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v16

    if-nez v16, :cond_32

    .line 219
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v16

    if-eqz v16, :cond_35

    const-wide/16 v4, 0x0

    .line 224
    .local v4, datetaken:J
    :goto_2c
    cmp-long v16, v4, v11

    if-lez v16, :cond_32

    .line 225
    move-wide v11, v4

    .line 226
    move v3, v6

    .line 216
    .end local v4           #datetaken:J
    :cond_32
    add-int/lit8 v6, v6, 0x1

    goto :goto_d

    .line 219
    :cond_35
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    goto :goto_2c

    .line 231
    .end local v1           #c:Landroid/database/Cursor;
    :cond_3e
    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v3, v0, :cond_d0

    .line 232
    aget-object v2, p2, v3

    .line 238
    .local v2, cursor:Landroid/database/Cursor;
    const/16 v16, 0x0

    :try_start_48
    move/from16 v0, v16

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 239
    .local v7, id:J
    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 240
    .local v13, title:Ljava/lang/String;
    aget-object v16, p3, v3

    move-object/from16 v0, v16

    invoke-static {v0, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v14

    .line 241
    .local v14, uri:Landroid/net/Uri;
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoDataBytes(Landroid/content/Context;Landroid/net/Uri;)[B

    move-result-object v15

    .line 243
    .local v15, videoDataBytes:[B
    sget-object v16, Lcom/google/android/apps/plus/phone/AlbumViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    new-array v10, v0, [Ljava/lang/Object;

    .line 244
    .local v10, row:[Ljava/lang/Object;
    const/16 v16, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v10, v16

    .line 245
    const/16 v16, 0x1

    const/16 v17, 0x0

    aput-object v17, v10, v16

    .line 246
    const/16 v16, 0x2

    const/16 v17, 0x0

    aput-object v17, v10, v16

    .line 247
    const/16 v16, 0x3

    const/16 v17, 0x0

    aput-object v17, v10, v16

    .line 248
    const/16 v16, 0x4

    const/16 v17, 0x0

    aput-object v17, v10, v16

    .line 249
    const/16 v16, 0x5

    const/16 v17, 0x0

    aput-object v17, v10, v16

    .line 250
    const/16 v16, 0x6

    const/16 v17, 0x0

    aput-object v17, v10, v16

    .line 251
    const/16 v16, 0x7

    aput-object v13, v10, v16

    .line 252
    const/16 v16, 0x8

    const-wide/16 v17, 0x0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v10, v16

    .line 253
    const/16 v16, 0x9

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v10, v16

    .line 254
    const/16 v16, 0xa

    const/16 v17, 0x0

    aput-object v17, v10, v16

    .line 255
    const/16 v16, 0xb

    aput-object v15, v10, v16

    .line 256
    const/16 v16, 0xc

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v10, v16

    .line 257
    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_c6
    .catchall {:try_start_48 .. :try_end_c6} :catchall_cb

    .line 260
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_9

    .end local v7           #id:J
    .end local v10           #row:[Ljava/lang/Object;
    .end local v13           #title:Ljava/lang/String;
    .end local v14           #uri:Landroid/net/Uri;
    .end local v15           #videoDataBytes:[B
    :catchall_cb
    move-exception v16

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    throw v16

    .line 262
    .end local v2           #cursor:Landroid/database/Cursor;
    :cond_d0
    return-object v9
.end method

.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 16

    .prologue
    const/4 v12, 0x2

    .line 146
    const/4 v10, 0x0

    .line 147
    .local v10, watch:Lcom/google/android/apps/plus/util/StopWatch;
    const-string v11, "IUAlbumLoader"

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_2f

    .line 148
    new-instance v11, Lcom/google/android/apps/plus/util/StopWatch;

    invoke-direct {v11}, Lcom/google/android/apps/plus/util/StopWatch;-><init>()V

    invoke-virtual {v11}, Lcom/google/android/apps/plus/util/StopWatch;->start()Lcom/google/android/apps/plus/util/StopWatch;

    move-result-object v10

    .line 149
    const-string v11, "IUAlbumLoader"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "esLoadInBackground: BEGIN thread="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_2f
    sget-object v11, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sMediaStoreUri:[Landroid/net/Uri;

    array-length v11, v11

    new-array v3, v11, [Landroid/database/Cursor;

    .line 155
    .local v3, cursorList:[Landroid/database/Cursor;
    :try_start_34
    iget v7, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    .line 156
    .local v7, loadLimit:I
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z

    .line 158
    const/4 v4, 0x0

    .local v4, i:I
    :goto_3a
    sget-object v11, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sMediaStoreUri:[Landroid/net/Uri;

    array-length v11, v11

    if-ge v4, v11, :cond_8b

    .line 159
    sget-object v11, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sMediaStoreUri:[Landroid/net/Uri;

    aget-object v11, v11, v4

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->setUri(Landroid/net/Uri;)V

    .line 160
    sget-object v11, Lcom/google/android/apps/plus/phone/CameraAlbumLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->setProjection([Ljava/lang/String;)V

    .line 163
    iget-boolean v11, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v11, :cond_86

    .line 164
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "corrected_date_taken desc LIMIT 0, "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 168
    .local v9, sortOrder:Ljava/lang/String;
    :goto_5e
    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->setSortOrder(Ljava/lang/String;)V

    .line 170
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v11

    aput-object v11, v3, v4

    .line 173
    aget-object v11, v3, v4

    if-eqz v11, :cond_89

    .line 174
    aget-object v11, v3, v4

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 175
    .local v2, cursorCount:I
    aget-object v11, v3, v4

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 181
    :goto_76
    iget-boolean v11, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z

    if-nez v11, :cond_83

    iget-boolean v11, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v11, :cond_83

    if-ne v2, v7, :cond_83

    .line 182
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z

    .line 158
    :cond_83
    add-int/lit8 v4, v4, 0x1

    goto :goto_3a

    .line 166
    .end local v2           #cursorCount:I
    .end local v9           #sortOrder:Ljava/lang/String;
    :cond_86
    const-string v9, "corrected_date_taken desc"

    .restart local v9       #sortOrder:Ljava/lang/String;
    goto :goto_5e

    .line 177
    :cond_89
    const/4 v2, 0x0

    .restart local v2       #cursorCount:I
    goto :goto_76

    .line 186
    .end local v2           #cursorCount:I
    .end local v9           #sortOrder:Ljava/lang/String;
    :cond_8b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->getContext()Landroid/content/Context;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sMediaStoreUri:[Landroid/net/Uri;

    invoke-virtual {p0, v11, v3, v12}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->buildMatrixCursor(Landroid/content/Context;[Landroid/database/Cursor;[Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v8

    .line 188
    .local v8, returnCursor:Landroid/database/Cursor;
    const-string v11, "IUAlbumLoader"

    const/4 v12, 0x2

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_d6

    .line 189
    const-string v11, "IUAlbumLoader"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "esLoadInBackground: END totalRows="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", msec="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v10}, Lcom/google/android/apps/plus/util/StopWatch;->getElapsedMsec()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", thread="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d6
    .catchall {:try_start_34 .. :try_end_d6} :catchall_e5

    .line 196
    :cond_d6
    move-object v0, v3

    .local v0, arr$:[Landroid/database/Cursor;
    array-length v6, v3

    .local v6, len$:I
    const/4 v5, 0x0

    .local v5, i$:I
    :goto_d9
    if-ge v5, v6, :cond_f6

    aget-object v1, v0, v5

    .line 197
    .local v1, cursor:Landroid/database/Cursor;
    if-eqz v1, :cond_e2

    .line 198
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 196
    :cond_e2
    add-int/lit8 v5, v5, 0x1

    goto :goto_d9

    .end local v0           #arr$:[Landroid/database/Cursor;
    .end local v1           #cursor:Landroid/database/Cursor;
    .end local v4           #i:I
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v7           #loadLimit:I
    .end local v8           #returnCursor:Landroid/database/Cursor;
    :catchall_e5
    move-exception v11

    move-object v0, v3

    .restart local v0       #arr$:[Landroid/database/Cursor;
    array-length v6, v3

    .restart local v6       #len$:I
    const/4 v5, 0x0

    .restart local v5       #i$:I
    :goto_e9
    if-ge v5, v6, :cond_f5

    aget-object v1, v0, v5

    .line 197
    .restart local v1       #cursor:Landroid/database/Cursor;
    if-eqz v1, :cond_f2

    .line 198
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 196
    :cond_f2
    add-int/lit8 v5, v5, 0x1

    goto :goto_e9

    .end local v1           #cursor:Landroid/database/Cursor;
    :cond_f5
    throw v11

    .restart local v4       #i:I
    .restart local v7       #loadLimit:I
    .restart local v8       #returnCursor:Landroid/database/Cursor;
    :cond_f6
    return-object v8
.end method

.method public final getCurrentPage()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 294
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v1, :cond_d

    iget v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    if-eq v1, v0, :cond_d

    iget v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    div-int/lit8 v0, v0, 0x10

    :cond_d
    return v0
.end method

.method public final hasMore()Z
    .registers 2

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final isDataSourceLoading()Z
    .registers 2

    .prologue
    .line 283
    const/4 v0, 0x0

    return v0
.end method

.method public final loadMore()V
    .registers 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 275
    iget v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    add-int/lit8 v0, v0, 0x30

    iput v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->onContentChanged()V

    .line 278
    :cond_f
    return-void
.end method

.method public final setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 290
    return-void
.end method
