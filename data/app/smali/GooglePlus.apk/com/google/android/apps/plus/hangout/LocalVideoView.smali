.class public Lcom/google/android/apps/plus/hangout/LocalVideoView;
.super Lcom/google/android/apps/plus/hangout/HangoutVideoView;
.source "LocalVideoView.java"

# interfaces
.implements Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;
    }
.end annotation


# instance fields
.field private audioVideoFailed:Z

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

.field private isRegistered:Z

.field private numPendingStartOutgoingVideoRequests:I

.field private selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

.field private selfFrameHeight:I

.field private selfFrameWidth:I

.field private final textureView:Landroid/view/TextureView;

.field private final toggleFlashButton:Landroid/widget/ImageButton;

.field private final videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v3, -0x2

    .line 113
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    const/16 v1, 0x140

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    .line 105
    const/16 v1, 0xf0

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    .line 106
    new-instance v1, Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/LocalVideoView;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    .line 116
    new-instance v1, Landroid/view/TextureView;

    invoke-direct {v1, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setVideoSurface(Landroid/view/View;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 121
    .local v0, layoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 122
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    .line 129
    new-instance v1, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-direct {v1, p1, v2, v3, p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;Landroid/view/TextureView;Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getFlashToggleButton()Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/plus/hangout/LocalVideoView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView$1;-><init>(Lcom/google/android/apps/plus/hangout/LocalVideoView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/LocalVideoView;Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/LocalVideoView;)Lcom/google/android/apps/plus/hangout/VideoCapturer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    return v0
.end method

.method static synthetic access$210(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .registers 3
    .parameter "x0"

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/LocalVideoView;)Lcom/google/android/apps/plus/hangout/Cameras$CameraType;
    .registers 2
    .parameter "x0"

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/hangout/LocalVideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 22
    iput p1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/hangout/LocalVideoView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 22
    iput p1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/hangout/LocalVideoView;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->audioVideoFailed:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/LocalVideoView;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->updateFlashLightButtonState()V

    return-void
.end method

.method private restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .registers 6
    .parameter "type"

    .prologue
    .line 244
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->audioVideoFailed:Z

    if-eqz v1, :cond_5

    .line 270
    :goto_4
    return-void

    .line 249
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-eqz v1, :cond_5e

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setLastUsedCameraType(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    .line 254
    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->getSizeOfCapturedFrames(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 255
    .local v0, size:Landroid/hardware/Camera$Size;
    if-nez v0, :cond_3d

    .line 256
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->onCameraOpenError(Ljava/lang/RuntimeException;)V

    goto :goto_4

    .line 260
    :cond_3d
    const-string v1, "Starting outgoing video"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->startOutgoingVideo(II)V

    .line 264
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    .line 265
    iget v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    goto :goto_4

    .line 268
    .end local v0           #size:Landroid/hardware/Camera$Size;
    :cond_5e
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    goto :goto_4
.end method

.method private updateFlashLightButtonState()V
    .registers 3

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing()Z

    move-result v0

    if-nez v0, :cond_18

    .line 290
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 300
    :goto_17
    return-void

    .line 294
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_17

    .line 298
    :cond_2f
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200b2

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_17
.end method


# virtual methods
.method public final isVideoShowing()Z
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing()Z

    move-result v0

    return v0
.end method

.method public final onCameraOpenError(Ljava/lang/RuntimeException;)V
    .registers 3
    .parameter "e"

    .prologue
    .line 186
    const-string v0, "Video capturer failed to start"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    .line 187
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideVideoSurface()V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideLogo()V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->showCameraError()V

    .line 191
    return-void
.end method

.method public final onCapturingStateChanged$1385ff()V
    .registers 1

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->updateFlashLightButtonState()V

    .line 179
    return-void
.end method

.method public final onMeasure$3b4dfe4b(II)V
    .registers 5
    .parameter "contentWidth"
    .parameter "contentHeight"

    .prologue
    .line 278
    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->layoutVideo(IIII)V

    .line 279
    return-void
.end method

.method public final onPause()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 162
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    if-eqz v1, :cond_14

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 164
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 165
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    .line 170
    .end local v0           #context:Landroid/content/Context;
    :cond_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    :cond_29
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    .line 171
    return-void
.end method

.method public final onResume()V
    .registers 5

    .prologue
    .line 149
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    if-nez v1, :cond_15

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 151
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 152
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    .line 154
    .end local v0           #context:Landroid/content/Context;
    :cond_15
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v1

    if-nez v1, :cond_5a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v1

    if-nez v1, :cond_43

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-nez v1, :cond_5b

    :cond_43
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideLogo()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->showVideoSurface()V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    .line 155
    :cond_5a
    :goto_5a
    return-void

    .line 154
    :cond_5b
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v1

    if-eqz v1, :cond_7e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v1

    if-nez v1, :cond_77

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v1

    if-eqz v1, :cond_7b

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    :cond_77
    :goto_77
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    goto :goto_5a

    :cond_7b
    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    goto :goto_77

    :cond_7e
    const-string v1, "Not starting outgoing video because device is not capable."

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_5a
.end method
