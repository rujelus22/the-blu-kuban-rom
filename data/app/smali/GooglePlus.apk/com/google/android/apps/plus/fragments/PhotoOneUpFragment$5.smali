.class final Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;
.super Landroid/os/AsyncTask;
.source "PhotoOneUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

.field final synthetic val$toastError:Ljava/lang/String;

.field final synthetic val$toastSuccess:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1485
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->val$toastSuccess:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->val$toastError:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs doInBackground([Landroid/graphics/Bitmap;)Ljava/lang/Boolean;
    .registers 6
    .parameter "bitmaps"

    .prologue
    .line 1496
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    .line 1498
    .local v1, manager:Landroid/app/WallpaperManager;
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1500
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_12} :catch_13

    .line 1506
    .end local v1           #manager:Landroid/app/WallpaperManager;
    :goto_12
    return-object v2

    .line 1501
    :catch_13
    move-exception v0

    .line 1502
    .local v0, e:Ljava/io/IOException;
    const-string v2, "StreamOneUp"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 1503
    const-string v2, "StreamOneUp"

    const-string v3, "Exception setting wallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1506
    :cond_24
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_12
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 1485
    check-cast p1, [Landroid/graphics/Bitmap;

    .end local p1
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->doInBackground([Landroid/graphics/Bitmap;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 5
    .parameter "x0"

    .prologue
    .line 1485
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->val$toastSuccess:Ljava/lang/String;

    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$700(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    return-void

    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->val$toastError:Ljava/lang/String;

    goto :goto_a
.end method
