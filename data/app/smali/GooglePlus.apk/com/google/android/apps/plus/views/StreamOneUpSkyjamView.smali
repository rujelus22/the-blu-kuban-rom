.class public Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;
.super Landroid/view/View;
.source "StreamOneUpSkyjamView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;
.implements Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;
.implements Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;


# static fields
.field protected static sDefaultTextPaint:Landroid/text/TextPaint;

.field private static sEmptyImage:Landroid/graphics/Bitmap;

.field private static sGoogleMusic:Landroid/graphics/Bitmap;

.field private static sHangoutCardViewInitialized:Z

.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field protected static sMaxWidth:I

.field protected static sNameMargin:I

.field private static sPlayIcon:Landroid/graphics/Bitmap;

.field protected static sPlayStopButtonPadding:I

.field private static sPreviewPaint:Landroid/graphics/Paint;

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

.field private static sStopIcon:Landroid/graphics/Bitmap;

.field private static sTagIcon:Landroid/graphics/Bitmap;

.field private static sTagPaint:Landroid/graphics/Paint;

.field protected static sTagTextPaint:Landroid/text/TextPaint;


# instance fields
.field private mActionIcon:Landroid/graphics/Bitmap;

.field private mActionIconPoint:Landroid/graphics/PointF;

.field private mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field private mActivityId:Ljava/lang/String;

.field private final mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mGoogleMusicRect:Landroid/graphics/Rect;

.field private mImage:Lcom/google/android/apps/plus/views/MediaImage;

.field private mImageRect:Landroid/graphics/Rect;

.field private mImageUrl:Ljava/lang/String;

.field private mIsAlbum:Z

.field private mMusicUrl:Ljava/lang/String;

.field private mPreviewBackground:Landroid/graphics/RectF;

.field private mPreviewStatus:Ljava/lang/String;

.field private mPreviewStatusPoint:Landroid/graphics/PointF;

.field private mTagBackground:Landroid/graphics/RectF;

.field private mTagIconRect:Landroid/graphics/Rect;

.field private mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mTagTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    .prologue
    const v4, 0x7f0d0196

    const/4 v3, 0x1

    .line 81
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 70
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 96
    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sHangoutCardViewInitialized:Z

    if-nez v1, :cond_114

    .line 97
    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sHangoutCardViewInitialized:Z

    .line 99
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f020102

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    .line 104
    const v1, 0x7f02006b

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    .line 105
    const v1, 0x7f02006e

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    .line 106
    const v1, 0x7f02012d

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    .line 108
    const v1, 0x7f020143

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    .line 111
    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    .line 112
    const v1, 0x7f0d0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sNameMargin:I

    .line 113
    const v1, 0x7f0d0188

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayStopButtonPadding:I

    .line 116
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    .line 118
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 119
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 120
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00f7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 123
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 124
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 125
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00f9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 130
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 131
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00fa

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 132
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 134
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 137
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 138
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 139
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00f8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 140
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 142
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 146
    const v1, 0x7f0803f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    .line 82
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v4, 0x7f0d0196

    const/4 v3, 0x1

    .line 85
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 96
    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sHangoutCardViewInitialized:Z

    if-nez v1, :cond_114

    .line 97
    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sHangoutCardViewInitialized:Z

    .line 99
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f020102

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    .line 104
    const v1, 0x7f02006b

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    .line 105
    const v1, 0x7f02006e

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    .line 106
    const v1, 0x7f02012d

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    .line 108
    const v1, 0x7f020143

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    .line 111
    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    .line 112
    const v1, 0x7f0d0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sNameMargin:I

    .line 113
    const v1, 0x7f0d0188

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayStopButtonPadding:I

    .line 116
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    .line 118
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 119
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 120
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00f7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 123
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 124
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 125
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00f9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 130
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 131
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00fa

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 132
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 134
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 137
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 138
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 139
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00f8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 140
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 142
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 146
    const v1, 0x7f0803f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    .line 86
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v4, 0x7f0d0196

    const/4 v3, 0x1

    .line 90
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 96
    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sHangoutCardViewInitialized:Z

    if-nez v1, :cond_114

    .line 97
    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sHangoutCardViewInitialized:Z

    .line 99
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f020102

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    .line 104
    const v1, 0x7f02006b

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    .line 105
    const v1, 0x7f02006e

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    .line 106
    const v1, 0x7f02012d

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    .line 108
    const v1, 0x7f020143

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    .line 111
    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    .line 112
    const v1, 0x7f0d0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sNameMargin:I

    .line 113
    const v1, 0x7f0d0188

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayStopButtonPadding:I

    .line 116
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    .line 118
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 119
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 120
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00f7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 123
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 124
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 125
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00f9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 130
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 131
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00fa

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 132
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 134
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 137
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 138
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 139
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00f8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 140
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 142
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 146
    const v1, 0x7f0803f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    .line 91
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_114
    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "albumName"
    .parameter "trackName"
    .parameter "thumbnailUrl"
    .parameter "playerUrl"
    .parameter "previewUrl"
    .parameter "activityId"

    .prologue
    .line 152
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 154
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    .line 158
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_9b

    .line 159
    const-string v4, "https://"

    invoke-virtual {p5, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 160
    .local v2, musicUrlIndex:I
    if-gez v2, :cond_20

    .line 161
    const-string v4, "http://"

    invoke-virtual {p5, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 170
    :cond_20
    :goto_20
    if-ltz v2, :cond_94

    .line 171
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    .line 172
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_ab

    .line 173
    invoke-virtual {p5, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    .line 182
    :goto_32
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getPlaybackStatus(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 185
    .local v1, context:Landroid/content/Context;
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_e2

    invoke-static {p1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, album:Ljava/lang/String;
    :goto_4a
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_f9

    .end local v0           #album:Ljava/lang/String;
    :goto_4e
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    .line 190
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6c

    .line 191
    iput-object p3, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageUrl:Ljava/lang/String;

    .line 192
    new-instance v4, Lcom/google/android/apps/plus/views/MediaImage;

    new-instance v5, Lcom/google/android/apps/plus/content/MediaImageRequest;

    const/4 v6, 0x3

    const/16 v7, 0x12c

    invoke-direct {v5, p3, v6, v7}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;II)V

    invoke-direct {v4, p0, v5}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 194
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MediaImage;->load()V

    .line 197
    :cond_6c
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 198
    .local v3, sb:Ljava/lang/StringBuilder;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_82

    .line 199
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    :cond_82
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8b

    .line 202
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    :cond_8b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 206
    iput-object p6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActivityId:Ljava/lang/String;

    .line 209
    .end local v1           #context:Landroid/content/Context;
    .end local v3           #sb:Ljava/lang/StringBuilder;
    :cond_94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->requestLayout()V

    .line 211
    return-void

    .line 164
    .end local v2           #musicUrlIndex:I
    :cond_9b
    const-string v4, "https://"

    invoke-virtual {p4, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 165
    .restart local v2       #musicUrlIndex:I
    if-gez v2, :cond_20

    .line 166
    const-string v4, "http://"

    invoke-virtual {p4, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_20

    .line 175
    :cond_ab
    invoke-virtual {p4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    .line 176
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    const-string v5, "mode=inline"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c9

    .line 177
    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    const-string v5, "mode=inline"

    const-string v6, "mode=streaming"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    goto/16 :goto_32

    .line 179
    :cond_c9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&mode=streaming"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    goto/16 :goto_32

    .line 185
    .restart local v1       #context:Landroid/content/Context;
    :cond_e2
    const v4, 0x7f080171

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4a

    .restart local v0       #album:Ljava/lang/String;
    :cond_f9
    move-object v0, p2

    .line 188
    goto/16 :goto_4e
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 235
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 236
    .local v2, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 238
    .local v3, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_68

    move v4, v5

    .line 277
    :goto_15
    return v4

    .line 240
    :pswitch_16
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_34

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 241
    .local v1, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 242
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    goto :goto_15

    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_34
    move v4, v5

    .line 248
    goto :goto_15

    .line 252
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_36
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 253
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 254
    .restart local v1       #item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_3e

    .line 257
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_4e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    move v4, v5

    .line 258
    goto :goto_15

    .line 262
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_53
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_63

    .line 263
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 264
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    goto :goto_15

    :cond_63
    move v4, v5

    .line 268
    goto :goto_15

    :pswitch_65
    move v4, v5

    .line 273
    goto :goto_15

    .line 238
    nop

    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_16
        :pswitch_36
        :pswitch_65
        :pswitch_53
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 221
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 222
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 223
    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->registerListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V

    .line 224
    return-void
.end method

.method public final onClickableRectClick$598f98c1()V
    .registers 6

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 293
    .local v0, context:Landroid/content/Context;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3d

    const/4 v2, 0x1

    .line 294
    .local v2, play:Z
    :goto_d
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 295
    .local v1, intent:Landroid/content/Intent;
    if-eqz v2, :cond_3f

    const-string v3, "com.google.android.apps.plus.service.SkyjamPlaybackService.PLAY"

    :goto_18
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    const-string v3, "music_account"

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 298
    const-string v3, "music_url"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v3, "song"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    const-string v3, "activity_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActivityId:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 302
    return-void

    .line 293
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #play:Z
    :cond_3d
    const/4 v2, 0x0

    goto :goto_d

    .line 295
    .restart local v1       #intent:Landroid/content/Intent;
    .restart local v2       #play:Z
    :cond_3f
    const-string v3, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    goto :goto_18
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 228
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 229
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 230
    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->unregisterListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V

    .line 231
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    .prologue
    const/high16 v2, 0x40a0

    const/4 v4, 0x0

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_48

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_41

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    :cond_41
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_48
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    if-eqz v0, :cond_76

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIconPoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIconPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mGoogleMusicRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 358
    :cond_76
    return-void
.end method

.method protected onMeasure(II)V
    .registers 25
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 326
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    .line 328
    .local v15, widthDimension:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_20e

    .line 337
    sget v14, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    .line 340
    .local v14, width:I
    :goto_d
    const/high16 v2, 0x4000

    invoke-static {v14, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-super {v0, v2, v1}, Landroid/view/View;->onMeasure(II)V

    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingLeft()I

    move-result v16

    .line 343
    .local v16, xStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingTop()I

    move-result v17

    .line 345
    .local v17, yStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getMeasuredWidth()I

    move-result v13

    .line 346
    .local v13, measuredWidth:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getMeasuredHeight()I

    move-result v12

    .line 347
    .local v12, measuredHeight:I
    sub-int v2, v13, v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingRight()I

    move-result v3

    sub-int v11, v2, v3

    .line 348
    .local v11, contentWidth:I
    sub-int v2, v12, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingBottom()I

    move-result v3

    sub-int v10, v2, v3

    .line 350
    .local v10, contentHeight:I
    add-int/lit8 v18, v16, 0xd

    add-int/lit8 v19, v17, 0xd

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v5, v2

    add-int/lit8 v2, v20, 0xf

    add-int/2addr v2, v5

    new-instance v3, Landroid/graphics/RectF;

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v19

    int-to-float v6, v0

    add-int v2, v2, v18

    int-to-float v2, v2

    add-int/lit8 v7, v19, 0x27

    int-to-float v7, v7

    invoke-direct {v3, v4, v6, v2, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    new-instance v2, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f80

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v2

    add-int/lit8 v3, v18, 0x5

    rsub-int/lit8 v4, v21, 0x27

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, v19

    new-instance v5, Landroid/graphics/Rect;

    add-int v6, v3, v20

    add-int v7, v4, v21

    invoke-direct {v5, v3, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagIconRect:Landroid/graphics/Rect;

    add-int/lit8 v4, v20, 0x5

    add-int/2addr v3, v4

    add-int/lit8 v4, v17, 0xd

    rsub-int/lit8 v2, v2, 0x27

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    add-int/lit8 v2, v17, 0xd

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v4, v2, 0xd

    add-int/lit8 v3, v11, -0x34

    sub-int v2, v10, v4

    add-int v2, v2, v17

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v5, :cond_1fc

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    if-nez v5, :cond_e1

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    :cond_e1
    sub-int/2addr v3, v2

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1a

    add-int v3, v3, v16

    new-instance v5, Landroid/graphics/Rect;

    add-int v6, v3, v2

    add-int v7, v4, v2

    invoke-direct {v5, v3, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageRect:Landroid/graphics/Rect;

    move v9, v2

    :goto_f6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-nez v2, :cond_1cf

    new-instance v2, Landroid/graphics/RectF;

    int-to-float v5, v3

    int-to-float v6, v4

    add-int v7, v3, v9

    int-to-float v7, v7

    add-int/lit8 v8, v4, 0x42

    int-to-float v8, v8

    invoke-direct {v2, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableRect;

    add-int v5, v3, v9

    add-int/lit8 v6, v4, 0x42

    sget-object v8, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    move-object/from16 v7, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_201

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    :goto_140
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, 0xd

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    rsub-int/lit8 v2, v2, 0x42

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    new-instance v4, Landroid/graphics/PointF;

    int-to-float v5, v3

    int-to-float v2, v2

    invoke-direct {v4, v5, v2}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIconPoint:Landroid/graphics/PointF;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v2, v4

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    rsub-int/lit8 v2, v2, 0x42

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v2, v4

    new-instance v4, Landroid/graphics/PointF;

    int-to-float v3, v3

    int-to-float v2, v2

    invoke-direct {v4, v3, v2}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/PointF;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    sub-int/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    rsub-int/lit8 v6, v3, 0x42

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    new-instance v6, Landroid/graphics/Rect;

    add-int/2addr v2, v4

    add-int/2addr v3, v5

    invoke-direct {v6, v4, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mGoogleMusicRect:Landroid/graphics/Rect;

    :cond_1cf
    add-int/lit8 v2, v17, 0xd

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v17, v2, 0xd

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    .end local v17           #yStart:I
    if-eqz v2, :cond_205

    add-int v17, v17, v9

    .line 352
    .restart local v17       #yStart:I
    :cond_1e5
    :goto_1e5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingBottom()I

    move-result v2

    add-int v2, v2, v17

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v2}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->setMeasuredDimension(II)V

    .line 353
    return-void

    .line 330
    .end local v10           #contentHeight:I
    .end local v11           #contentWidth:I
    .end local v12           #measuredHeight:I
    .end local v13           #measuredWidth:I
    .end local v14           #width:I
    .end local v16           #xStart:I
    .end local v17           #yStart:I
    :sswitch_1f1
    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    invoke-static {v15, v2}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 331
    .restart local v14       #width:I
    goto/16 :goto_d

    .line 333
    .end local v14           #width:I
    :sswitch_1f9
    move v14, v15

    .line 334
    .restart local v14       #width:I
    goto/16 :goto_d

    .restart local v10       #contentHeight:I
    .restart local v11       #contentWidth:I
    .restart local v12       #measuredHeight:I
    .restart local v13       #measuredWidth:I
    .restart local v16       #xStart:I
    .restart local v17       #yStart:I
    :cond_1fc
    move v9, v3

    move/from16 v3, v16

    .line 350
    goto/16 :goto_f6

    :cond_201
    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_140

    .end local v17           #yStart:I
    :cond_205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    if-eqz v2, :cond_1e5

    add-int/lit8 v17, v17, 0x42

    goto :goto_1e5

    .line 328
    :sswitch_data_20e
    .sparse-switch
        -0x80000000 -> :sswitch_1f1
        0x40000000 -> :sswitch_1f9
    .end sparse-switch
.end method

.method public final onMediaImageChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageUrl:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 287
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    .line 288
    return-void
.end method

.method public final onPlaybackStatusUpdate(Ljava/lang/String;ZLjava/lang/String;)V
    .registers 7
    .parameter "musicUrl"
    .parameter "playing"
    .parameter "status"

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    .line 307
    .local v0, oldBitmap:Landroid/graphics/Bitmap;
    if-eqz p2, :cond_31

    if-eqz p1, :cond_31

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 308
    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    .line 313
    :goto_12
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_36

    const/4 v1, 0x1

    .line 315
    .local v1, statusChanged:Z
    :goto_23
    if-eqz v1, :cond_27

    .line 316
    iput-object p3, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    .line 319
    :cond_27
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    if-ne v0, v2, :cond_2d

    if-eqz v1, :cond_30

    .line 320
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    .line 322
    :cond_30
    return-void

    .line 310
    .end local v1           #statusChanged:Z
    :cond_31
    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    goto :goto_12

    .line 313
    :cond_36
    const/4 v1, 0x0

    goto :goto_23
.end method

.method public final processClick(FF)V
    .registers 7
    .parameter "x"
    .parameter "y"

    .prologue
    .line 514
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-eqz v2, :cond_33

    .line 515
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 516
    .local v1, rect:Landroid/graphics/Rect;
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 517
    .local v0, loc:[I
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getLocationOnScreen([I)V

    .line 518
    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    float-to-int v2, v2

    const/4 v3, 0x1

    aget v3, v0, v3

    int-to-float v3, v3

    sub-float v3, p2, v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_2e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 520
    :cond_2e
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->onClickableRectClick$598f98c1()V

    .line 523
    .end local v0           #loc:[I
    .end local v1           #rect:Landroid/graphics/Rect;
    :cond_33
    return-void
.end method

.method public final startAutoPlay()V
    .registers 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->onClickableRectClick$598f98c1()V

    .line 217
    :cond_b
    return-void
.end method
