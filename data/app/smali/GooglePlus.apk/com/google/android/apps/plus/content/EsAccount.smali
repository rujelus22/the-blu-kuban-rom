.class public Lcom/google/android/apps/plus/content/EsAccount;
.super Ljava/lang/Object;
.source "EsAccount.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDisplayName:Ljava/lang/String;

.field private final mGaiaId:Ljava/lang/String;

.field private final mIndex:I

.field private final mIsChild:Z

.field private final mIsPlusPage:Z

.field private final mName:Ljava/lang/String;

.field private final mRealTimeChatParticipantId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 218
    new-instance v0, Lcom/google/android/apps/plus/content/EsAccount$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/EsAccount$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsAccount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 6
    .parameter "in"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mRealTimeChatParticipantId:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mDisplayName:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIndex:I

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_42

    move v0, v1

    :goto_37
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsChild:Z

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_44

    :goto_3f
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsPlusPage:Z

    .line 55
    return-void

    :cond_42
    move v0, v2

    .line 53
    goto :goto_37

    :cond_44
    move v1, v2

    .line 54
    goto :goto_3f
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/EsAccount;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .registers 9
    .parameter "name"
    .parameter "gaiaId"
    .parameter "displayName"
    .parameter "isChild"
    .parameter "isPlusPage"
    .parameter "index"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "g:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mRealTimeChatParticipantId:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/plus/content/EsAccount;->mDisplayName:Ljava/lang/String;

    .line 39
    iput-boolean p4, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsChild:Z

    .line 40
    iput-boolean p5, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsPlusPage:Z

    .line 41
    iput p6, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIndex:I

    .line 42
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "other"

    .prologue
    const/4 v1, 0x0

    .line 161
    if-nez p1, :cond_4

    .line 175
    :cond_3
    :goto_3
    return v1

    .line 163
    :cond_4
    instance-of v2, p1, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_3

    move-object v0, p1

    .line 166
    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 167
    .local v0, otherAccount:Lcom/google/android/apps/plus/content/EsAccount;
    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_1d

    iget-object v1, v0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    if-nez v1, :cond_1f

    .line 173
    :cond_1d
    const/4 v1, 0x1

    goto :goto_3

    .line 175
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_3
.end method

.method public final getDisplayName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGaiaId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Gaia id not yet set. Out of box not yet done?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public final getIndex()I
    .registers 2

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIndex:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPersonId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "g:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRealTimeChatParticipantId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mRealTimeChatParticipantId:Ljava/lang/String;

    return-object v0
.end method

.method public final hasGaiaId()Z
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isChild()Z
    .registers 2

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsChild:Z

    return v0
.end method

.method public final isMyGaiaId(Ljava/lang/String;)Z
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 87
    if-nez p1, :cond_4

    .line 88
    const/4 v0, 0x0

    .line 91
    :goto_3
    return v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method public final isPlusPage()Z
    .registers 2

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsPlusPage:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 186
    .local v0, buffer:Ljava/lang/StringBuilder;
    const-string v1, "Account name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string v1, ", Gaia id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string v1, ", Display name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsAccount;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string v1, ", Plotnikov index: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 190
    const-string v1, ", isPlusPage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsPlusPage:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 210
    iget v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsChild:Z

    if-eqz v0, :cond_26

    move v0, v1

    :goto_1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 212
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/EsAccount;->mIsPlusPage:Z

    if-eqz v0, :cond_28

    :goto_22
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 213
    return-void

    :cond_26
    move v0, v2

    .line 211
    goto :goto_1b

    :cond_28
    move v1, v2

    .line 212
    goto :goto_22
.end method
