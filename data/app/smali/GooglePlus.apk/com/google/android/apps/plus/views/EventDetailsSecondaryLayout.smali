.class public Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsSecondaryLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sInitialized:Z

.field private static sPadding:I

.field private static sSeeInviteesTextColor:I

.field private static sSeeInvitesHeight:I

.field private static sSeeInvitesText:Ljava/lang/String;

.field private static sSeeInvitesTextSize:F


# instance fields
.field private mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

.field private mHideInvitees:Z

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mViewInvitees:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    sget-boolean v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sInitialized:Z

    if-nez v2, :cond_39

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 58
    .local v1, resources:Landroid/content/res/Resources;
    const v2, 0x7f0d00a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sPadding:I

    .line 61
    const v2, 0x7f0a0064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInviteesTextColor:I

    .line 63
    const v2, 0x7f08010a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesText:Ljava/lang/String;

    .line 64
    const v2, 0x7f0d00a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesTextSize:F

    .line 66
    const v2, 0x7f0d00a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesHeight:I

    .line 68
    sput-boolean v5, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sInitialized:Z

    .line 71
    .end local v1           #resources:Landroid/content/res/Resources;
    :cond_39
    new-instance v2, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->addView(Landroid/view/View;)V

    .line 74
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->addView(Landroid/view/View;)V

    .line 76
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget-object v3, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesTextSize:F

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInviteesTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 80
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setClickable(Z)V

    .line 81
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 84
    .local v0, a:Landroid/content/res/TypedArray;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 88
    sget v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sPadding:I

    invoke-virtual {p0, v4, v4, v4, v2}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->addPadding(IIII)V

    .line 89
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .registers 12
    .parameter "event"
    .parameter "state"
    .parameter "listener"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 123
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 124
    .local v0, currentTime:J
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v6

    cmp-long v6, v0, v6

    if-lez v6, :cond_44

    move v3, v4

    .line 126
    .local v3, past:Z
    :goto_f
    iget-object v6, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v6, :cond_46

    iget-object v6, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventOptions;->hideGuestList:Ljava/lang/Boolean;

    if-eqz v6, :cond_46

    iget-object v6, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventOptions;->hideGuestList:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_46

    iget-boolean v6, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isOwner:Z

    if-nez v6, :cond_46

    :goto_27
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mHideInvitees:Z

    .line 129
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mHideInvitees:Z

    if-eqz v4, :cond_48

    const/16 v2, 0x8

    .line 130
    .local v2, inviteeVisibility:I
    :goto_2f
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setVisibility(I)V

    .line 133
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v4, p1, p3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/EventActionListener;Z)V

    .line 134
    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->invalidate()V

    .line 137
    return-void

    .end local v2           #inviteeVisibility:I
    .end local v3           #past:Z
    :cond_44
    move v3, v5

    .line 124
    goto :goto_f

    .restart local v3       #past:Z
    :cond_46
    move v4, v5

    .line 126
    goto :goto_27

    :cond_48
    move v2, v5

    .line 129
    goto :goto_2f
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->clear()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 142
    return-void
.end method

.method protected measureChildren(II)V
    .registers 10
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v6, 0x4000

    const/4 v3, 0x0

    .line 93
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 94
    .local v1, width:I
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mHideInvitees:Z

    if-nez v2, :cond_38

    .line 98
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->size()I

    move-result v2

    if-lez v2, :cond_39

    sget v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sPadding:I

    add-int/lit8 v2, v2, 0x0

    invoke-static {v4, v1, v6, v3, v3}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->measure(Landroid/view/View;IIII)V

    invoke-static {v4, v3, v3}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->setCorner(Landroid/view/View;II)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setVisibility(I)V

    :goto_25
    add-int/lit8 v0, v2, 0x0

    .line 100
    .local v0, currentY:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesHeight:I

    invoke-static {v2, v1, v6, v4, v6}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->measure(Landroid/view/View;IIII)V

    .line 102
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->setCorner(Landroid/view/View;II)V

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    .line 105
    .end local v0           #currentY:I
    :cond_38
    return-void

    .line 98
    :cond_39
    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setVisibility(I)V

    move v2, v3

    goto :goto_25
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    if-ne p1, v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_d

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onViewAllInviteesClicked()V

    .line 149
    :cond_d
    return-void
.end method
