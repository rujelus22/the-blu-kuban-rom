.class final Lcom/google/android/apps/plus/fragments/PostFragment$7;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;->post()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

.field final synthetic val$audience:Lcom/google/android/apps/plus/content/AudienceData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1479
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$7;->val$audience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 1482
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$7;->val$audience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1483
    .local v0, circles:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/CircleData;>;"
    new-instance v1, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v1, v3, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 1484
    .local v1, circlesAudience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    const-string v4, "com.google.android.apps.plus"

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    :try_start_26
    new-instance v3, Landroid/content/ContentValues;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v5, "package_name"

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "audience_data"

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbAudienceData;->serialize(Lcom/google/android/apps/plus/content/AudienceData;)[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v4, "platform_audience"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_40} :catch_41

    .line 1486
    :goto_40
    return-void

    .line 1484
    :catch_41
    move-exception v2

    const-string v3, "EsPlatformData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to save audience: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_40
.end method
