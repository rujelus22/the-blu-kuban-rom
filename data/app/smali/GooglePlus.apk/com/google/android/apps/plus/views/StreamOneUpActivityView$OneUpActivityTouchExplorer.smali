.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;
.super Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;
.source "StreamOneUpActivityView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OneUpActivityTouchExplorer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/TouchExplorationHelper",
        "<",
        "Lcom/google/android/apps/plus/views/ClickableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsItemCacheStale:Z

.field private mItemCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter "context"

    .prologue
    .line 1847
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    .line 1848
    invoke-direct {p0, p2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;-><init>(Landroid/content/Context;)V

    .line 1849
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    .line 1850
    new-instance v0, Ljava/util/ArrayList;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;
    invoke-static {p1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$800(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    .line 1851
    return-void
.end method

.method private refreshItemCache()V
    .registers 3

    .prologue
    .line 1858
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    if-eqz v0, :cond_1e

    .line 1859
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1860
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$800(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1861
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1862
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    .line 1864
    :cond_1e
    return-void
.end method


# virtual methods
.method protected final bridge synthetic getIdForItem(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    .prologue
    .line 1842
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->refreshItemCache()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected final bridge synthetic getItemAt(FF)Ljava/lang/Object;
    .registers 9
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1842
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->refreshItemCache()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_b
    if-ge v1, v2, :cond_26

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    float-to-int v4, p1

    float-to-int v5, p2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_22

    :goto_21
    return-object v0

    :cond_22
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    :cond_26
    const/4 v0, 0x0

    goto :goto_21
.end method

.method protected final bridge synthetic getItemForId(I)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 1842
    if-ltz p1, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->refreshDrawableState()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableItem;

    :goto_17
    return-object v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method protected final getVisibleItems(Ljava/util/List;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1895
    .local p1, items:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/views/ClickableItem;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->refreshItemCache()V

    .line 1896
    const/4 v0, 0x0

    .local v0, i:I
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, size:I
    :goto_a
    if-ge v0, v2, :cond_1a

    .line 1897
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 1898
    .local v1, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1896
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 1900
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_1a
    return-void
.end method

.method public final invalidateItemCache()V
    .registers 2

    .prologue
    .line 1854
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    .line 1855
    return-void
.end method

.method protected final bridge synthetic performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1842
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    const/16 v2, 0x10

    if-ne p2, v2, :cond_2f

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    invoke-interface {p1, v2, v3, v1}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    invoke-interface {p1, v1, v2, v0}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    :goto_2e
    return v0

    :cond_2f
    move v0, v1

    goto :goto_2e
.end method

.method protected final bridge synthetic populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1842
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final bridge synthetic populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1842
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
