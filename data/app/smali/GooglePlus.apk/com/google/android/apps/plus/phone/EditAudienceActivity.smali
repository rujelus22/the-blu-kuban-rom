.class public Lcom/google/android/apps/plus/phone/EditAudienceActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EditAudienceActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;


# instance fields
.field private mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

.field private mPositiveButton:Landroid/view/View;

.field private mShakeDetectorWasRunning:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 270
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 9
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 204
    if-nez p1, :cond_33

    const/4 v4, -0x1

    if-ne p2, v4, :cond_33

    .line 205
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v4, :cond_33

    .line 206
    const-string v4, "person_id"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 207
    .local v3, personId:Ljava/lang/String;
    if-eqz v3, :cond_1e

    .line 208
    const-string v4, "person_data"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/PersonData;

    .line 210
    .local v2, person:Lcom/google/android/apps/plus/content/PersonData;
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addSelectedPerson(Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    .line 212
    .end local v2           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_1e
    const-string v4, "circle_id"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 213
    .local v1, circleId:Ljava/lang/String;
    if-eqz v1, :cond_33

    .line 214
    const-string v4, "circle_data"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    .line 216
    .local v0, circle:Lcom/google/android/apps/plus/content/CircleData;
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v4, v1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addSelectedCircle(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    .line 220
    .end local v0           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v1           #circleId:Ljava/lang/String;
    .end local v3           #personId:Ljava/lang/String;
    :cond_33
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 7
    .parameter "fragment"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 79
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v0, :cond_50

    .line 80
    check-cast p1, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setOnSelectionChangeListener(Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setCircleSelectionEnabled(Z)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "circle_usage_type"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setCircleUsageType(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "search_plus_pages_enabled"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setIncludePlusPages(Z)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "filter_null_gaia_ids"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setFilterNullGaiaIds(Z)V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audience_is_read_only"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setIncomingAudienceIsReadOnly(Z)V

    .line 92
    :cond_50
    return-void
.end method

.method public final onAudienceChanged(Ljava/lang/String;)V
    .registers 4
    .parameter "countText"

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v0, :cond_13

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isSelectionValid()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 250
    :cond_13
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    .prologue
    .line 227
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_20

    .line 238
    :goto_7
    return-void

    .line 229
    :pswitch_8
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 230
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "audience"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 231
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setResult(ILandroid/content/Intent;)V

    .line 232
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_1c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->finish()V

    goto :goto_7

    .line 227
    :pswitch_data_20
    .packed-switch 0x7f09005c
        :pswitch_8
        :pswitch_1c
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v2, 0x7f030020

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setContentView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, title:Ljava/lang/String;
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->showTitlebar(ZZ)V

    .line 56
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 57
    const v2, 0x7f100004

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->createTitlebarButtons(I)V

    .line 60
    const v2, 0x7f09005c

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    .line 61
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v2, :cond_39

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isSelectionValid()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 64
    :cond_39
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    const v2, 0x7f09005d

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 69
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_58

    .line 70
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mShakeDetectorWasRunning:Z

    .line 72
    :cond_58
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 144
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 118
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    .line 121
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_14

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 123
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_14

    .line 124
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 127
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_14
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 161
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_16

    .line 171
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 163
    :sswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_9

    .line 167
    :sswitch_12
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->onSearchRequested()Z

    goto :goto_9

    .line 161
    :sswitch_data_16
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f09029a -> :sswitch_12
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 152
    const v0, 0x7f09029a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 153
    invoke-interface {p1}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .registers 4

    .prologue
    .line 99
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->isIntentAccountActive()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 101
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->hasAudience()Z

    move-result v1

    if-nez v1, :cond_24

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audience"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    .line 104
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    if-eqz v0, :cond_24

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 111
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    :cond_24
    :goto_24
    return-void

    .line 109
    :cond_25
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->finish()V

    goto :goto_24
.end method

.method public onSearchRequested()Z
    .registers 11

    .prologue
    const/4 v2, 0x1

    const/4 v9, 0x0

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "search_phones_enabled"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 183
    .local v5, includePhoneOnlyContacts:Z
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "search_plus_pages_enabled"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 185
    .local v6, includePlusPages:Z
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "search_pub_profiles_enabled"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 187
    .local v4, includePublicProfiles:Z
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "circle_usage_type"

    const/4 v7, -0x1

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 190
    .local v3, searchUsageType:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v7, "filter_null_gaia_ids"

    invoke-virtual {v0, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    move-object v0, p0

    move v7, v2

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getPeopleSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZIZZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v9}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 196
    return v2
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 135
    return-void
.end method
