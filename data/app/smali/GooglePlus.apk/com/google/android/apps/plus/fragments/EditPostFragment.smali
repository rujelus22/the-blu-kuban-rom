.class public Lcom/google/android/apps/plus/fragments/EditPostFragment;
.super Landroid/support/v4/app/Fragment;
.source "EditPostFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private mChanged:Z

.field private mEditRequestId:Ljava/lang/Integer;

.field private mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 47
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditPostFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditPostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EditPostFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditPostFragment;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->handleEditPost(Lcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/fragments/EditPostFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/EditPostFragment;)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EditPostActivity;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->invalidateMenu()V

    :cond_b
    return-void
.end method

.method private handleEditPost(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 10
    .parameter "result"

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 202
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 204
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_51

    .line 205
    const v3, 0x48ba7

    invoke-virtual {v0, v3}, Landroid/app/Activity;->dismissDialog(I)V

    .line 206
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_5d

    .line 207
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    .line 208
    .local v2, exception:Ljava/lang/Exception;
    instance-of v3, v2, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_52

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    .end local v2           #exception:Ljava/lang/Exception;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_52

    .line 211
    const v3, 0x7f080158

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f080157

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    .line 215
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {v1, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 224
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :cond_51
    :goto_51
    return-void

    .line 219
    :cond_52
    const v3, 0x7f080156

    invoke-static {v0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_51

    .line 221
    :cond_5d
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_51
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    if-eqz p1, :cond_22

    .line 61
    const-string v0, "edit_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 62
    const-string v0, "edit_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    .line 65
    :cond_19
    const-string v0, "changed"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    .line 67
    :cond_22
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 72
    const v5, 0x7f030023

    const/4 v6, 0x0

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 74
    .local v4, view:Landroid/view/View;
    const v5, 0x7f09004f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 77
    .local v3, intent:Landroid/content/Intent;
    const-string v5, "account"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 78
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    const-string v5, "activity_id"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, activityId:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const/4 v6, 0x0

    invoke-virtual {v5, p0, v0, v1, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    .line 81
    const-string v5, "content"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, editableText:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-nez v2, :cond_3b

    const-string v2, ""

    .end local v2           #editableText:Ljava/lang/String;
    :cond_3b
    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHtml(Ljava/lang/String;)V

    .line 84
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v6, Lcom/google/android/apps/plus/fragments/EditPostFragment$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EditPostFragment;)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 101
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 103
    return-object v4
.end method

.method public final onDiscard()V
    .registers 3

    .prologue
    .line 152
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EditPostActivity;

    .line 155
    .local v0, editPostActivity:Lcom/google/android/apps/plus/phone/EditPostActivity;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    if-eqz v1, :cond_16

    .line 156
    const v1, 0xdc073

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->showDialog(I)V

    .line 161
    :goto_15
    return-void

    .line 158
    :cond_16
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->setResult(I)V

    .line 159
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->finish()V

    goto :goto_15
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-ne p1, v0, :cond_7

    .line 139
    packed-switch p2, :pswitch_data_e

    .line 145
    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 141
    :pswitch_9
    invoke-static {p1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 142
    const/4 v0, 0x1

    goto :goto_8

    .line 139
    :pswitch_data_e
    .packed-switch 0x6
        :pswitch_9
    .end packed-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 120
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 123
    return-void
.end method

.method public final onPost()V
    .registers 7

    .prologue
    .line 167
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/EditPostActivity;

    .line 170
    .local v3, editPostActivity:Lcom/google/android/apps/plus/phone/EditPostActivity;
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    if-eqz v5, :cond_1b

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 171
    :cond_1b
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EditPostActivity;->setResult(I)V

    .line 172
    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EditPostActivity;->finish()V

    .line 184
    :goto_22
    return-void

    .line 176
    :cond_23
    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 177
    .local v4, intent:Landroid/content/Intent;
    const-string v5, "account"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 178
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    const-string v5, "activity_id"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, activityId:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v2

    .line 182
    .local v2, content:Ljava/lang/String;
    const v5, 0x48ba7

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EditPostActivity;->showDialog(I)V

    .line 183
    invoke-static {v3, v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->editActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    goto :goto_22
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 108
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_25

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_25

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 114
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->handleEditPost(Lcom/google/android/apps/plus/service/ServiceResult;)V

    .line 116
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_25
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 127
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 130
    const-string v0, "edit_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    :cond_12
    const-string v0, "changed"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 134
    return-void
.end method
