.class public final Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "SimpleParticipantsLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field mParticipants:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field mProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Collection;[Ljava/lang/String;)V
    .registers 4
    .parameter "context"
    .parameter
    .parameter "projection"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p2, participants:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mParticipants:Ljava/util/Collection;

    .line 30
    iput-object p3, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mProjection:[Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .registers 10

    .prologue
    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mProjection:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mParticipants:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_10
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_93

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mProjection:[Ljava/lang/String;

    array-length v6, v5

    move v1, v2

    :goto_26
    if-ge v1, v6, :cond_10

    aget-object v7, v5, v1

    const-string v8, "_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    :goto_39
    add-int/lit8 v1, v1, 0x1

    goto :goto_26

    :cond_3c
    const-string v8, "participant_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4c

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_39

    :cond_4c
    const-string v8, "full_name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5c

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_39

    :cond_5c
    const-string v8, "first_name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6c

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFirstName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_39

    :cond_6c
    const-string v8, "conversation_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7d

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_39

    :cond_7d
    const-string v8, "active"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8e

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_39

    :cond_8e
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_39

    :cond_93
    iget-object v0, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    return-object v0
.end method

.method protected final onStartLoading()V
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    if-eqz v0, :cond_a

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->deliverResult(Ljava/lang/Object;)V

    .line 77
    :goto_9
    return-void

    .line 76
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;->forceLoad()V

    goto :goto_9
.end method
