.class final Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;
.super Ljava/lang/Object;
.source "PlusOneFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PlusOneFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PreviewLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 7
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    .line 318
    .local v0, loader:Lcom/google/android/apps/plus/phone/EsCursorLoader;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->access$200(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsApiProvider;->makePreviewUri(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->setUri(Landroid/net/Uri;)V

    .line 319
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->access$300(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->setSelectionArgs([Ljava/lang/String;)V

    .line 320
    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 9
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 311
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    if-eqz p2, :cond_83

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_83

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "com.google.android.apps.content.EXTRA_ACTIVITY"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    const/4 v0, 0x0

    if-eqz v3, :cond_87

    array-length v4, v3

    if-lez v4, :cond_87

    aget-object v0, v3, v1

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-object v3, v0

    :goto_21
    if-eqz v3, :cond_24

    move v1, v2

    :cond_24
    if-nez v1, :cond_49

    const-string v0, "PlusOneActivity"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_49

    const-string v0, "PlusOneActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to url retrieve preview for: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mUrl:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->access$300(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_49
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mLoggedPreview:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->access$700(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Z

    move-result v0

    if-nez v0, :cond_7e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->access$702(Lcom/google/android/apps/plus/fragments/PlusOneFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->access$200(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v2

    if-eqz v1, :cond_84

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_6f
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static {v1, v4, v2, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    :cond_7e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->access$800(Lcom/google/android/apps/plus/fragments/PlusOneFragment;Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    :cond_83
    return-void

    :cond_84
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_6f

    :cond_87
    move-object v3, v0

    goto :goto_21
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 360
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method
