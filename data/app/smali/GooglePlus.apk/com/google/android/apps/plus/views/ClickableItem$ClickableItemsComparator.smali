.class public final Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;
.super Ljava/lang/Object;
.source "ClickableItem.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ClickableItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClickableItemsComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/plus/views/ClickableItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I
    .registers 10
    .parameter "lhs"
    .parameter "rhs"

    .prologue
    .line 65
    invoke-interface {p0}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 66
    .local v1, firstBounds:Landroid/graphics/Rect;
    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    .line 69
    .local v4, secondBounds:Landroid/graphics/Rect;
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    iget v7, v4, Landroid/graphics/Rect;->top:I

    if-gt v6, v7, :cond_10

    .line 70
    const/4 v2, -0x1

    .line 104
    :cond_f
    :goto_f
    return v2

    .line 74
    :cond_10
    iget v6, v1, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    if-lt v6, v7, :cond_18

    .line 75
    const/4 v2, 0x1

    goto :goto_f

    .line 79
    :cond_18
    iget v6, v1, Landroid/graphics/Rect;->left:I

    iget v7, v4, Landroid/graphics/Rect;->left:I

    sub-int v2, v6, v7

    .line 80
    .local v2, leftDifference:I
    if-nez v2, :cond_f

    .line 85
    iget v6, v1, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->top:I

    sub-int v5, v6, v7

    .line 86
    .local v5, topDifference:I
    if-eqz v5, :cond_2a

    move v2, v5

    .line 87
    goto :goto_f

    .line 91
    :cond_2a
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v6, v7

    .line 92
    .local v0, bottomDifference:I
    if-eqz v0, :cond_34

    move v2, v0

    .line 93
    goto :goto_f

    .line 97
    :cond_34
    iget v6, v1, Landroid/graphics/Rect;->right:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    sub-int v3, v6, v7

    .line 98
    .local v3, rightDifference:I
    if-eqz v3, :cond_3e

    move v2, v3

    .line 99
    goto :goto_f

    .line 104
    :cond_3e
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v7

    sub-int v2, v6, v7

    goto :goto_f
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p2
    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method
