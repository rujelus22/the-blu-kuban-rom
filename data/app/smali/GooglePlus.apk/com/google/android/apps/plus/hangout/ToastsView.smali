.class public Lcom/google/android/apps/plus/hangout/ToastsView;
.super Landroid/widget/FrameLayout;
.source "ToastsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/ToastsView$4;,
        Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;,
        Lcom/google/android/apps/plus/hangout/ToastsView$RemoteMuteToast;,
        Lcom/google/android/apps/plus/hangout/ToastsView$MeetingMemberToast;,
        Lcom/google/android/apps/plus/hangout/ToastsView$StringToastInfo;,
        Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;,
        Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;
    }
.end annotation


# instance fields
.field private final animIn:Landroid/view/animation/Animation;

.field private final animOut:Landroid/view/animation/Animation;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;

.field private hideToastRunnable:Ljava/lang/Runnable;

.field private imageView:Landroid/widget/ImageView;

.field private final mHandler:Landroid/os/Handler;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 220
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->mHandler:Landroid/os/Handler;

    .line 225
    new-instance v2, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;B)V

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->eventHandler:Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;

    .line 229
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 230
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v2, 0x7f030046

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 232
    .local v1, rootView:Landroid/view/View;
    const v2, 0x7f0900ed

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->imageView:Landroid/widget/ImageView;

    .line 233
    const v2, 0x7f0900ee

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->mTextView:Landroid/widget/TextView;

    .line 235
    new-instance v2, Lcom/google/android/apps/plus/hangout/ToastsView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/hangout/ToastsView$1;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->hideToastRunnable:Ljava/lang/Runnable;

    .line 242
    const v2, 0x7f040002

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->animIn:Landroid/view/animation/Animation;

    .line 243
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->animIn:Landroid/view/animation/Animation;

    new-instance v3, Lcom/google/android/apps/plus/hangout/ToastsView$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/hangout/ToastsView$2;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 257
    const v2, 0x7f040003

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->animOut:Landroid/view/animation/Animation;

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->animOut:Landroid/view/animation/Animation;

    new-instance v3, Lcom/google/android/apps/plus/hangout/ToastsView$3;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/hangout/ToastsView$3;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 272
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/ToastsView;)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->animOut:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method


# virtual methods
.method public final addToast(I)V
    .registers 4
    .parameter "messageId"

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, string:Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/plus/hangout/ToastsView$StringToastInfo;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/hangout/ToastsView$StringToastInfo;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;)V

    .line 285
    return-void
.end method

.method public final addToast(Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;)V
    .registers 6
    .parameter "toast"

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;->populateView(Landroid/widget/ImageView;Landroid/widget/TextView;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->hideToastRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->hideToastRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->animIn:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 295
    :cond_2e
    return-void
.end method

.method public final onPause()V
    .registers 5

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->eventHandler:Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 280
    return-void
.end method

.method public final onResume()V
    .registers 5

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView;->eventHandler:Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 276
    return-void
.end method
