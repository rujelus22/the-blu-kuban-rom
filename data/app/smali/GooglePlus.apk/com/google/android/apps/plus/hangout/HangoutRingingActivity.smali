.class public Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "HangoutRingingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;,
        Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    }
.end annotation


# static fields
.field private static final INVITER_PROJECTION:[Ljava/lang/String;

.field private static isCurrentlyRinging:Z

.field private static mRingtone:Landroid/media/Ringtone;

.field private static sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

.field private final mAnswerWidgetPingRunnable:Ljava/lang/Runnable;

.field private mCallTimeoutRunnable:Ljava/lang/Runnable;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field volatile mContinueVibrating:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private final mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

.field private mHasActed:Z

.field private mInviteId:Ljava/lang/String;

.field private mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

.field private mInviterCircleNamesTextView:Landroid/widget/TextView;

.field private mInviterId:Ljava/lang/String;

.field private mInviterName:Ljava/lang/String;

.field private mIsHangoutLite:Z

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPackedCircleIds:Ljava/lang/String;

.field private mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

.field private final mPersonLoaderCallbacks:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

.field private mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

.field private mSelfVideoVerticalGravity:F

.field private mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

.field private mSelfVideoViewContainer:Landroid/widget/FrameLayout;

.field mVibrator:Landroid/os/Vibrator;

.field mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

.field private toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

.field private toggleVideoMuteMenuButton:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 99
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    .line 100
    sput-boolean v2, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    .line 141
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "packed_circle_ids"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->INVITER_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 129
    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    .line 130
    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    .line 132
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    .line 134
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

    .line 136
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    .line 158
    const v0, -0x414ccccd

    iput v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoVerticalGravity:F

    .line 479
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidgetPingRunnable:Ljava/lang/Runnable;

    .line 491
    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    .line 921
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPackedCircleIds:Ljava/lang/String;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPackedCircleIds:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_23
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .registers 5
    .parameter "x0"

    .prologue
    const/4 v3, 0x1

    .line 67
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    if-nez v0, :cond_32

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    const-string v0, "Accepted invitation"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_33

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    :goto_26
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v3, v2}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->startActivity(Landroid/content/Intent;)V

    :cond_32
    return-void

    :cond_33
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    const-string v0, "Not yet signed in. Will send finish once signed in."

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->stopRingTone()V

    goto :goto_26
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    if-nez v0, :cond_26

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    const-string v0, "Rejected invitation"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_27

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    :cond_26
    :goto_26
    return-void

    :cond_27
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    const-string v0, "Not yet signed in. Will send finish once signed in."

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->stopRingTone()V

    goto :goto_26
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->createMissedHangoutNotification()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800()[Ljava/lang/String;
    .registers 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->INVITER_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPackedCircleIds:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "account"

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":notifications:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createMissedHangoutNotification()V
    .registers 19

    .prologue
    .line 827
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08033f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 829
    .local v11, notifcationTitle:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/content/PersonData;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v14, v2, v3, v4}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    .local v14, person:Lcom/google/android/apps/plus/content/PersonData;
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    new-instance v16, Lcom/google/android/apps/plus/content/AudienceData;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    new-instance v17, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/hangout/HangoutActivity;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unique"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    new-instance v2, Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getNick()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->MissedCall:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    const-string v3, "hangout_info"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "hangout_skip_green_room"

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "audience"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v2, 0x800

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v15, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 839
    .local v10, contentIntent:Landroid/app/PendingIntent;
    new-instance v12, Landroid/app/Notification;

    const v2, 0x7f020140

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v12, v2, v11, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 841
    .local v12, notification:Landroid/app/Notification;
    iget v2, v12, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v12, Landroid/app/Notification;->flags:I

    .line 843
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080340

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v12, v0, v11, v2, v10}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 847
    const-string v2, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/app/NotificationManager;

    .line 850
    .local v13, notificationManager:Landroid/app/NotificationManager;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v13, v2, v3, v12}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 853
    return-void
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 6
    .parameter "context"
    .parameter "account"

    .prologue
    .line 413
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    .line 414
    .local v0, existingRingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
    if-eqz v0, :cond_9

    .line 415
    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    .line 417
    :cond_9
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v2

    .line 418
    .local v2, notificationTag:Ljava/lang/String;
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 420
    .local v1, notificationManager:Landroid/app/NotificationManager;
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 421
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 423
    return-void
.end method

.method private exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .registers 6
    .parameter "status"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 495
    sput-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    .line 496
    sput-boolean v2, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_13

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 500
    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    .line 502
    :cond_13
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->stopRingTone()V

    .line 504
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    if-eqz v0, :cond_30

    .line 508
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getApp()Lcom/google/android/apps/plus/phone/EsApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 511
    :cond_30
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->ACCEPTED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    if-eq p1, v0, :cond_3b

    .line 512
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    .line 515
    :cond_3b
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->removeStatusBarNotification()V

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->finish()V

    .line 518
    return-void
.end method

.method public static onC2DMReceive(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .registers 32
    .parameter "context"
    .parameter "account"
    .parameter "intent"

    .prologue
    .line 165
    const-string v3, "Hangout Invitation Receiver got invitation tickle"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 167
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v3, v4, :cond_18

    .line 169
    const-string v3, "Ignoring hangout invitation since this device doesn\'t support hangouts"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 318
    :cond_17
    :goto_17
    return-void

    .line 173
    :cond_18
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080012

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0c0006

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3a

    .line 174
    const-string v3, "Ignoring hangout invitation because of setting"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_17

    .line 178
    :cond_3a
    const-string v3, "id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 179
    .local v21, hangoutInviteId:Ljava/lang/String;
    const-string v3, "notification"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 181
    .local v23, hangoutInviteProtoBase64:Ljava/lang/String;
    if-eqz v21, :cond_4e

    if-nez v23, :cond_6c

    .line 182
    :cond_4e
    const-string v3, "Incorrect tickle: inviteId = %s, hangoutInviteProtoBase64 = %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v21, v4, v5

    const/4 v5, 0x1

    aput-object v23, v4, v5

    const-string v5, "GoogleMeeting"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_17

    const-string v5, "GoogleMeeting"

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_17

    .line 187
    :cond_6c
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v24

    .line 188
    .local v24, hangoutInviteProtoBytes:[B
    const/16 v22, 0x0

    .line 190
    .local v22, hangoutInviteNotification:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    :try_start_75
    invoke-static/range {v24 .. v24}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->parseFrom([B)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    :try_end_78
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_75 .. :try_end_78} :catch_87

    move-result-object v22

    .line 196
    :goto_79
    if-nez v22, :cond_8e

    .line 197
    const-string v3, "Could not decode invite: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v23, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_17

    .line 193
    :catch_87
    move-exception v3

    const-string v3, "Invalid BatchCommand message received"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    goto :goto_79

    .line 201
    :cond_8e
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasCommand()Z

    move-result v3

    if-nez v3, :cond_9b

    .line 202
    const-string v3, "Ignoring hangoutInviteNotification without any command"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 206
    :cond_9b
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getContext()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v25

    .line 208
    .local v25, hangoutStartContext:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->DISMISSED:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    if-ne v3, v4, :cond_102

    .line 209
    const-string v3, "Got hangoutInviteNotification:\nCommand: %s\nHangoutId: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    if-eqz v3, :cond_fb

    .line 214
    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-boolean v4, v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z

    if-eqz v4, :cond_ce

    const-string v3, "Ignoring hangout ring cancellation since user already acted on it"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    :cond_ce
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getContext()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasHangoutId()Z

    move-result v5

    if-eqz v5, :cond_f4

    invoke-virtual {v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f4

    const-string v4, "Cancelling hangout ringing."

    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    goto/16 :goto_17

    :cond_f4
    const-string v3, "Ignoring hangout ring cancellation since hangout ids don\'t match"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 218
    :cond_fb
    const-string v3, "Ignoring dismiss command since ring activity is not running."

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 222
    :cond_102
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    if-eq v3, v4, :cond_123

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->LITE:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    if-eq v3, v4, :cond_123

    .line 224
    const-string v3, "Ignoring Hangout ring for unsupported hangout type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 229
    :cond_123
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasInvitation()Z

    move-result v3

    if-nez v3, :cond_130

    .line 230
    const-string v3, "Ignoring hangoutStartContext without invitation"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 234
    :cond_130
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getInvitation()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v26

    .line 236
    .local v26, invitation:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->hasInviterGaiaId()Z

    move-result v3

    if-eqz v3, :cond_140

    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->hasTimestamp()Z

    move-result v3

    if-nez v3, :cond_147

    .line 237
    :cond_140
    const-string v3, "Ignoring hangoutStartContext without invitation data"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 241
    :cond_147
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getTimestamp()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x493e0

    cmp-long v3, v3, v5

    if-lez v3, :cond_17a

    .line 242
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring expired hangout invitation tickle. Tickle age = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getTimestamp()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 247
    :cond_17a
    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInvitationType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-result-object v27

    .line 249
    .local v27, inviteType:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;
    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v27

    if-eq v0, v3, :cond_1a8

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT_SYNC:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v27

    if-eq v0, v3, :cond_1a8

    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v27

    if-eq v0, v3, :cond_1a8

    .line 252
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring unsupported invitation type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInvitationType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 256
    :cond_1a8
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 257
    .local v11, inviterId:Ljava/lang/String;
    invoke-virtual/range {v26 .. v26}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterProfileName()Ljava/lang/String;

    move-result-object v28

    .line 258
    .local v28, inviterName:Ljava/lang/String;
    const-string v3, "Got hangoutInviteNotification:\nCommand: %s\nInviterGaiaId: %s\nInviterName: %s\nHangoutId: %s\nHangoutType: %s\nNotificationType: %s"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v11, v4, v5

    const/4 v5, 0x2

    aput-object v28, v4, v5

    const/4 v5, 0x3

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getNotificationType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    .line 268
    .local v8, launchSource:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;
    sget-object v3, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    move-object/from16 v0, v27

    if-ne v0, v3, :cond_1f3

    .line 269
    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Transfer:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    .line 272
    :cond_1f3
    new-instance v2, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getNick()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    .line 277
    .local v2, hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    const-string v3, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v3

    if-eqz v3, :cond_24e

    const/16 v18, 0x1

    .line 282
    .local v18, callInProgress:Z
    :goto_217
    :try_start_217
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :try_end_222
    .catch Ljava/lang/LinkageError; {:try_start_217 .. :try_end_222} :catch_251

    move-result-object v17

    .line 289
    .local v17, appState:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->ENTERING_MEETING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-object/from16 v0, v17

    if-eq v0, v3, :cond_235

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITHOUT_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-object/from16 v0, v17

    if-eq v0, v3, :cond_235

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-object/from16 v0, v17

    if-ne v0, v3, :cond_25c

    :cond_235
    const/16 v20, 0x1

    .line 293
    .local v20, hangoutInProgress:Z
    :goto_237
    if-eqz v20, :cond_25f

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v3

    if-eqz v3, :cond_25f

    .line 295
    const-string v3, "Ignoring the ring/ding since user is already in same hangout"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 277
    .end local v17           #appState:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    .end local v18           #callInProgress:Z
    .end local v20           #hangoutInProgress:Z
    :cond_24e
    const/16 v18, 0x0

    goto :goto_217

    .line 283
    .restart local v18       #callInProgress:Z
    :catch_251
    move-exception v19

    .line 285
    .local v19, err:Ljava/lang/LinkageError;
    const-string v3, "Hangout native lib is missing or misconfigured"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 286
    invoke-virtual/range {v19 .. v19}, Ljava/lang/LinkageError;->printStackTrace()V

    goto/16 :goto_17

    .line 289
    .end local v19           #err:Ljava/lang/LinkageError;
    .restart local v17       #appState:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :cond_25c
    const/16 v20, 0x0

    goto :goto_237

    .line 299
    .restart local v20       #hangoutInProgress:Z
    :cond_25f
    invoke-virtual/range {v22 .. v22}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getNotificationType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->NOTIFICATION_DING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    if-eq v3, v4, :cond_26f

    if-nez v18, :cond_26f

    if-nez v20, :cond_26f

    sget-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    if-eqz v3, :cond_365

    .line 302
    :cond_26f
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Creating ding notification. AppState: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". callInProgress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". hangoutInProgress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". isCurrentlyRinging: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 306
    new-instance v2, Lcom/google/android/apps/plus/service/Hangout$Info;

    .end local v2           #hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    .end local v11           #inviterId:Ljava/lang/String;
    move-result-object v13

    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getNick()Ljava/lang/String;

    move-result-object v14

    sget-object v15, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ding:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/16 v16, 0x0

    move-object v9, v2

    invoke-direct/range {v9 .. v16}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    .line 310
    .restart local v2       #hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldNotify(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_17

    new-instance v4, Landroid/app/Notification;

    invoke-direct {v4}, Landroid/app/Notification;-><init>()V

    const v3, 0x7f020140

    iput v3, v4, Landroid/app/Notification;->icon:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, v4, Landroid/app/Notification;->when:J

    iget v3, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v4, Landroid/app/Notification;->flags:I

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_35e

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v4, Landroid/app/Notification;->sound:Landroid/net/Uri;

    :goto_2e5
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldVibrate(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2f1

    iget v3, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v4, Landroid/app/Notification;->defaults:I

    :cond_2f1
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2, v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x800

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08033c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v28, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08033d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v28, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f08033e

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v28, v9, v10

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v5, v4, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v6, v7, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const-string v3, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v3, v5, v6, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_17

    :cond_35e
    iget v3, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v4, Landroid/app/Notification;->defaults:I

    goto :goto_2e5

    .line 314
    .restart local v11       #inviterId:Ljava/lang/String;
    :cond_365
    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->isCurrentlyRinging:Z

    .line 316
    invoke-virtual/range {v25 .. v25}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v3

    sget-object v4, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->LITE:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    if-ne v3, v4, :cond_3a8

    const/4 v14, 0x1

    :goto_371
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNING_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-eq v4, v5, :cond_385

    sget-object v5, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v4, v5, :cond_388

    :cond_385
    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    :cond_388
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v12, v28

    move-object v13, v2

    invoke-static/range {v9 .. v14}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutRingingActivityIntent$55105fd9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$Info;Z)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x1000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_17

    :cond_3a8
    const/4 v14, 0x0

    goto :goto_371
.end method

.method private removeStatusBarNotification()V
    .registers 4

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 821
    return-void
.end method

.method private sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    .registers 6
    .parameter "status"

    .prologue
    .line 521
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sending hangout finish request. Status: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 522
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviteId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->sendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    return-void
.end method

.method public static stopRingActivity()V
    .registers 2

    .prologue
    .line 324
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    .line 325
    .local v0, existingRingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
    if-eqz v0, :cond_c

    .line 326
    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;->IGNORED:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    .line 327
    invoke-direct {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->createMissedHangoutNotification()V

    .line 329
    :cond_c
    return-void
.end method

.method private stopRingTone()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 897
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    if-eqz v0, :cond_c

    .line 898
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    .line 899
    sput-object v1, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    .line 901
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    if-eqz v0, :cond_15

    .line 902
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mContinueVibrating:Z

    .line 903
    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    .line 906
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 907
    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 860
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 790
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 12
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 544
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0xf

    const/4 v7, 0x4

    if-ne v3, v7, :cond_22f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_224

    move v7, v4

    :goto_22
    if-eqz v7, :cond_227

    const v3, -0x41bd70a4

    :goto_27
    iput v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoVerticalGravity:F

    if-eqz v7, :cond_22c

    move v3, v4

    :goto_2c
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->setRequestedOrientation(I)V

    .line 546
    :goto_2f
    const v3, 0x7f030040

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->setContentView(I)V

    .line 549
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v7, 0x680080

    invoke-virtual {v3, v7}, Landroid/view/Window;->addFlags(I)V

    .line 554
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 555
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "account"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 556
    const-string v3, "hangout_info"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/Hangout$Info;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 557
    const-string v3, "hangout_invite_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviteId:Ljava/lang/String;

    .line 558
    const-string v3, "hangout_inviter_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    .line 559
    const-string v3, "hangout_inviter_name"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    .line 560
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_84

    .line 561
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f080317

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    .line 563
    :cond_84
    const-string v3, "hangout_is_lite"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    .line 565
    const v3, 0x7f0900c2

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 566
    .local v2, inviterNameTextView:Landroid/widget/TextView;
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 567
    const v3, 0x7f0900c3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterCircleNamesTextView:Landroid/widget/TextView;

    .line 568
    const v3, 0x7f0900c1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    .line 569
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    if-eqz v3, :cond_234

    .line 570
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    .line 575
    :goto_bf
    const v3, 0x7f0900be

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    .line 576
    new-instance v3, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v3, p0, v7, v8}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 577
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 578
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    new-instance v7, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$2;

    invoke-direct {v7, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$2;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 585
    const v3, 0x7f0900c6

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    .line 586
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    new-instance v7, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$3;

    invoke-direct {v7, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$3;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setOnTriggerListener(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;)V

    .line 619
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->clearAnimation()V

    .line 620
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    const v7, 0x7f07000c

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setTargetResources(I)V

    .line 621
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    const v7, 0x7f07000d

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setTargetDescriptionsResourceId(I)V

    .line 623
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    const v7, 0x7f07000e

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setDirectionDescriptionsResourceId(I)V

    .line 625
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidget:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->reset(Z)V

    .line 627
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAnswerWidgetPingRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x3e8

    invoke-virtual {v3, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 629
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    if-nez v3, :cond_133

    .line 630
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PersonLoaderCallbacks;

    invoke-virtual {v3, v5, v6, v7}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 633
    :cond_133
    const-string v3, "vibrator"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Vibrator;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibrator:Landroid/os/Vibrator;

    .line 634
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mNotificationManager:Landroid/app/NotificationManager;

    .line 636
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    if-nez v3, :cond_1f9

    .line 637
    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$4;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    .line 644
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mCallTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x7530

    invoke-virtual {v3, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 645
    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    if-nez v3, :cond_25b

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f080010

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f080014

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7, v6, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v6

    sput-object v6, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    :goto_183
    sget-object v6, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    if-nez v6, :cond_246

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Cannot get a ringtone for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :goto_199
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f080013

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0c0007

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7, v6, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1c7

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    if-nez v3, :cond_1c7

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mContinueVibrating:Z

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    invoke-direct {v3, p0, v5}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mVibratorThread:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$VibratorThread;->start()V

    .line 647
    :cond_1c7
    new-instance v1, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.c2dm.intent.RECEIVE"

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 648
    .local v1, invitationFilter:Landroid/content/IntentFilter;
    const-string v3, "com.google.android.apps.hangout.NOTIFICATION"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 650
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutRingingEventHandler:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;

    invoke-virtual {v3, p0, v4, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 653
    sput-object p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    .line 655
    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    invoke-direct {v3, p0, v5}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    .line 656
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getApp()Lcom/google/android/apps/plus/phone/EsApplication;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPhoneStateChangeListener:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$PhoneStateChangeListener;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.PHONE_STATE"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 661
    .end local v1           #invitationFilter:Landroid/content/IntentFilter;
    :cond_1f9
    const v3, 0x7f0900c4

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    .line 663
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$5;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$5;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 675
    const v3, 0x7f0900c5

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    .line 677
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$6;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$6;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 684
    return-void

    .end local v0           #intent:Landroid/content/Intent;
    .end local v2           #inviterNameTextView:Landroid/widget/TextView;
    :cond_224
    move v7, v5

    .line 545
    goto/16 :goto_22

    :cond_227
    const v3, -0x42333333

    goto/16 :goto_27

    :cond_22c
    move v3, v5

    goto/16 :goto_2c

    :cond_22f
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->setRequestedOrientation(I)V

    goto/16 :goto_2f

    .line 572
    .restart local v0       #intent:Landroid/content/Intent;
    .restart local v2       #inviterNameTextView:Landroid/widget/TextView;
    :cond_234
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    .line 573
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    goto/16 :goto_bf

    .line 645
    :cond_246
    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v3}, Landroid/media/Ringtone;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_1c7

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Landroid/media/Ringtone;->setStreamType(I)V

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v3}, Landroid/media/Ringtone;->play()V

    goto/16 :goto_199

    :cond_25b
    move-object v3, v6

    goto/16 :goto_183
.end method

.method protected onPause()V
    .registers 12

    .prologue
    const v4, 0x7f080339

    const/4 v10, 0x0

    .line 777
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    .line 779
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sRingingActivity:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    if-eqz v0, :cond_4d

    .line 780
    new-instance v6, Landroid/app/Notification;

    const v0, 0x7f020140

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v6, v0, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviteId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mInviterName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mIsHangoutLite:Z

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutRingingActivityIntent$55105fd9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$Info;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const/16 v1, 0x10

    iput v1, v6, Landroid/app/Notification;->flags:I

    invoke-virtual {v6, v7, v8, v9, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 782
    :cond_4d
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->onPause()V

    .line 783
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 767
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 768
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->removeStatusBarNotification()V

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->onResume()V

    .line 770
    return-void
.end method

.method protected onStart()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    .line 750
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    .line 751
    new-instance v1, Lcom/google/android/apps/plus/hangout/SelfVideoView;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    .line 752
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 754
    .local v0, layoutParams:Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 755
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->turnOffFlashLightSupport()V

    .line 756
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    sget-object v2, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;)V

    .line 758
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoVerticalGravity:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setVerticalGravity(F)V

    .line 759
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 760
    return-void
.end method
