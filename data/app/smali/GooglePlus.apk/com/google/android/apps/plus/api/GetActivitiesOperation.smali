.class public final Lcom/google/android/apps/plus/api/GetActivitiesOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetActivitiesOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivitiesRequest;",
        "Lcom/google/api/services/plusi/model/GetActivitiesResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCircleId:Ljava/lang/String;

.field private final mContinuationToken:Ljava/lang/String;

.field private final mFromWidget:Z

.field private final mGaiaId:Ljava/lang/String;

.field private final mMaxCount:I

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private final mView:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 21
    .parameter "context"
    .parameter "account"
    .parameter "view"
    .parameter "circleId"
    .parameter "gaiaId"
    .parameter "fromWidget"
    .parameter "continuationToken"
    .parameter "maxCount"
    .parameter "syncState"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 49
    const-string v4, "getactivities"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v8, p11

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 51
    iput p3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    .line 52
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_28

    const-string v1, "f."

    invoke-virtual {p4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 53
    const/4 v1, 0x2

    invoke-virtual {p4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    .line 55
    :cond_28
    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    .line 57
    iput-boolean p6, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mFromWidget:Z

    .line 58
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    .line 59
    if-lez p8, :cond_3d

    .end local p8
    :goto_34
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mMaxCount:I

    .line 60
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 61
    return-void

    .line 59
    .restart local p8
    :cond_3d
    const/16 p8, 0xa

    goto :goto_34
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 10
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mFromWidget:Z

    iget v4, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    const-string v4, "MOBILE"

    iget-object v5, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Stream;->continuationToken:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsPostsData;->updateStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"

    .prologue
    const/4 v1, 0x0

    .line 26
    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;

    .end local p1
    new-instance v0, Lcom/google/api/services/plusi/model/StreamParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/StreamParams;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    packed-switch v0, :pswitch_data_a8

    const-string v0, "ALL"

    :goto_13
    iput-object v0, v2, Lcom/google/api/services/plusi/model/StreamParams;->viewType:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1e

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    if-nez v0, :cond_26

    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    if-nez v0, :cond_26

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_9f

    :cond_26
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v2, "LATEST"

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->sort:Ljava/lang/String;

    :goto_2c
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->focusGroupId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->productionStreamOid:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->continuesToken:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget v2, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mMaxCount:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxNumUpdates:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v2, "MOBILE"

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->collapserType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxComments:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxNumImages:Ljava/lang/Integer;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    if-nez v0, :cond_69

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    if-nez v0, :cond_69

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_a6

    :cond_69
    const/4 v0, 0x1

    :goto_6a
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->skipPopularMixin:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v1, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/FieldRequestOptions;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeLegacyMediaData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v1, Lcom/google/api/services/plusi/model/UpdateFilter;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/UpdateFilter;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mFromWidget:Z

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamNamespaces(Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/UpdateFilter;->includeNamespace:Ljava/util/List;

    return-void

    :pswitch_97
    const-string v0, "CIRCLES"

    goto/16 :goto_13

    :pswitch_9b
    const-string v0, "WHATS_HOT"

    goto/16 :goto_13

    :cond_9f
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v2, "BEST"

    iput-object v2, v0, Lcom/google/api/services/plusi/model/StreamParams;->sort:Ljava/lang/String;

    goto :goto_2c

    :cond_a6
    move v0, v1

    goto :goto_6a

    :pswitch_data_a8
    .packed-switch 0x0
        :pswitch_97
        :pswitch_9b
    .end packed-switch
.end method
