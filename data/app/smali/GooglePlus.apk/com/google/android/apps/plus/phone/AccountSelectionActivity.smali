.class public Lcom/google/android/apps/plus/phone/AccountSelectionActivity;
.super Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.source "AccountSelectionActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V
    .registers 8
    .parameter "oobResponse"
    .parameter "account"
    .parameter "pages"

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "intent"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 43
    .local v1, startIntent:Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "run_oob"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz p1, :cond_2f

    const-string v2, "network_oob"

    new-instance v3, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    invoke-direct {v3, p1}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_2f
    if-eqz p3, :cond_36

    const-string v2, "plus_pages"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_36
    if-eqz v1, :cond_3d

    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 45
    .local v0, nextIntent:Landroid/content/Intent;
    :cond_3d
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->startActivity(Landroid/content/Intent;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->finish()V

    .line 47
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 2
    .parameter "savedInstanceState"

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->showAccountSelectionOrUpgradeAccount(Landroid/os/Bundle;)V

    .line 30
    return-void
.end method
