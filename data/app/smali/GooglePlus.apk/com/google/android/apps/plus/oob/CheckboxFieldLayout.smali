.class public Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "CheckboxFieldLayout.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private mInput:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .registers 8
    .parameter "field"
    .parameter "id"
    .parameter "actionCallback"

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v2

    iget-object v0, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    .line 63
    .local v0, inputField:Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getServerBooleanValue()Ljava/lang/Boolean;

    move-result-object v1

    .line 66
    .local v1, serverValue:Ljava/lang/Boolean;
    if-eqz v1, :cond_27

    .line 67
    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 70
    :cond_27
    if-eqz p3, :cond_2e

    .line 71
    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 73
    :cond_2e
    return-void
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .registers 4

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v1

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-static {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    .line 89
    .local v0, inputField:Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    new-instance v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    .line 90
    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    .line 91
    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .registers 4
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-interface {v0}, Lcom/google/android/apps/plus/oob/ActionCallback;->onInputChanged$7c32a9fe()V

    .line 81
    return-void
.end method
