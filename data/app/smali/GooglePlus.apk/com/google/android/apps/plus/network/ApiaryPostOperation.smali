.class public final Lcom/google/android/apps/plus/network/ApiaryPostOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "ApiaryPostOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostActivityRequest;",
        "Lcom/google/api/services/plusi/model/PostActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

.field private final mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

.field private final mAttachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mContent:Ljava/lang/String;

.field private final mDeepLinkId:Ljava/lang/String;

.field private final mExternalId:Ljava/lang/String;

.field private final mLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private mPostedAttachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/ApiaryActivity;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/lang/String;)V
    .registers 24
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activity"
    .parameter "content"
    .parameter
    .parameter "externalId"
    .parameter "location"
    .parameter "audience"
    .parameter "info"
    .parameter "deepLinkId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/network/ApiaryActivity;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            "Lcom/google/android/apps/plus/network/ApiaryApiInfo;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 107
    .local p7, attachments:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/api/MediaRef;>;"
    const-string v8, "postactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityRequestJson;

    move-result-object v9

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityResponseJson;

    move-result-object v7

    new-instance v1, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;

    const-string v4, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite"

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p11

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v8

    move-object v6, v9

    move-object v8, p3

    move-object v9, p4

    move-object v10, v1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    .line 116
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

    .line 117
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContent:Ljava/lang/String;

    .line 118
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAttachments:Ljava/util/List;

    .line 119
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mExternalId:Ljava/lang/String;

    .line 120
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 121
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 122
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 125
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mDeepLinkId:Ljava/lang/String;

    .line 126
    return-void
.end method

.method private buildMediaReference$13d52a54(Landroid/content/Context;Lcom/google/android/apps/plus/api/MediaRef;ZLjava/util/List;Ljava/util/Set;Ljava/util/Set;)Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;
    .registers 31
    .parameter "context"
    .parameter "ref"
    .parameter "attachThumbnail"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;"
        }
    .end annotation

    .prologue
    .line 289
    .local p4, attachmentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/api/MediaRef;>;"
    .local p5, photoSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    .local p6, caidSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v15, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;

    invoke-direct {v15}, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;-><init>()V

    .line 295
    .local v15, mediaReference:Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v6, v7, :cond_3b

    const-string v16, "2"

    .line 297
    .local v16, mediaType:Ljava/lang/String;
    :goto_f
    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->mediaType:Ljava/lang/String;

    .line 299
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v6

    if-eqz v6, :cond_176

    .line 300
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v14

    .line 301
    .local v14, localUri:Landroid/net/Uri;
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->getFingerprint(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v17

    .line 303
    .local v17, newFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    if-nez v17, :cond_3e

    .line 305
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Could not determine fingerprint for media: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const/4 v15, 0x0

    .line 437
    .end local v14           #localUri:Landroid/net/Uri;
    .end local v15           #mediaReference:Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;
    .end local v17           #newFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    :goto_3a
    return-object v15

    .line 295
    .end local v16           #mediaType:Ljava/lang/String;
    .restart local v15       #mediaReference:Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;
    :cond_3b
    const-string v16, "1"

    goto :goto_f

    .line 309
    .restart local v14       #localUri:Landroid/net/Uri;
    .restart local v16       #mediaType:Ljava/lang/String;
    .restart local v17       #newFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    :cond_3e
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v19

    .line 310
    .local v19, photoId:J
    const-wide/16 v6, 0x0

    cmp-long v6, v19, v6

    if-eqz v6, :cond_f9

    .line 311
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v19

    invoke-static {v6, v0, v1}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->getFingerprint(Landroid/content/Context;J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v18

    .line 312
    .local v18, oldFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    invoke-virtual/range {v17 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_f6

    .line 316
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    invoke-virtual/range {v17 .. v17}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->getPhotoIdFromStream(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    .line 319
    .local v4, newPhotoId:J
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Fingerprint mismatch; old: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", new: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v8, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v8, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v23, Lcom/google/android/apps/plus/network/ApiaryPostOperation$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/apps/plus/network/ApiaryPostOperation$1;-><init>(Lcom/google/android/apps/plus/network/ApiaryPostOperation;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 325
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 368
    .end local v4           #newPhotoId:J
    .end local v14           #localUri:Landroid/net/Uri;
    .end local v18           #oldFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    .end local v19           #photoId:J
    .local v2, attachment:Lcom/google/android/apps/plus/api/MediaRef;
    :goto_c0
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v6

    if-eqz v6, :cond_19b

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19b

    .line 369
    const-string v6, "HttpTransaction"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_f3

    .line 370
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Duplicate server reference found; "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :cond_f3
    const/4 v15, 0x0

    goto/16 :goto_3a

    .line 329
    .end local v2           #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    .restart local v14       #localUri:Landroid/net/Uri;
    .restart local v18       #oldFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    .restart local v19       #photoId:J
    :cond_f6
    move-object/from16 v2, p2

    .line 331
    .restart local v2       #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    goto :goto_c0

    .line 333
    .end local v2           #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v18           #oldFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    :cond_f9
    const-string v6, "HttpTransaction"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_11a

    .line 334
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Looking for remote photo w/ CAID: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_11a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    invoke-virtual/range {v17 .. v17}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->getPhotoIdFromStream(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    .line 340
    .restart local v4       #newPhotoId:J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_172

    .line 341
    const-string v6, "HttpTransaction"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_157

    .line 342
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Found remote photo; ID: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " matches CAID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_157
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v9

    .line 347
    .local v9, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    .line 350
    .local v3, ownerGaiaId:Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 352
    .restart local v2       #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    goto/16 :goto_c0

    .line 354
    .end local v2           #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v3           #ownerGaiaId:Ljava/lang/String;
    .end local v9           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_172
    move-object/from16 v2, p2

    .restart local v2       #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    goto/16 :goto_c0

    .line 358
    .end local v2           #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v4           #newPhotoId:J
    .end local v14           #localUri:Landroid/net/Uri;
    .end local v17           #newFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    .end local v19           #photoId:J
    :cond_176
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v6

    if-eqz v6, :cond_182

    .line 360
    move-object/from16 v2, p2

    .line 361
    .restart local v2       #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    const/16 v17, 0x0

    .restart local v17       #newFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    goto/16 :goto_c0

    .line 363
    .end local v2           #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v17           #newFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    :cond_182
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "No photo ID or local Uri for attachment: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/4 v15, 0x0

    goto/16 :goto_3a

    .line 375
    .restart local v2       #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    .restart local v17       #newFingerprint:Lcom/android/gallery3d/common/Fingerprint;
    :cond_19b
    if-eqz v17, :cond_1cb

    invoke-virtual/range {v17 .. v17}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v12

    .line 376
    .local v12, caid:Ljava/lang/String;
    :goto_1a1
    if-eqz v12, :cond_1cd

    .line 380
    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1cd

    .line 381
    const-string v6, "HttpTransaction"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1c8

    .line 382
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Duplicate CAID found; "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_1c8
    const/4 v15, 0x0

    goto/16 :goto_3a

    .line 375
    .end local v12           #caid:Ljava/lang/String;
    :cond_1cb
    const/4 v12, 0x0

    goto :goto_1a1

    .line 388
    .restart local v12       #caid:Ljava/lang/String;
    :cond_1cd
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v6

    if-nez v6, :cond_23b

    .line 389
    if-eqz v12, :cond_1dc

    .line 390
    iput-object v12, v15, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->clientAssignedUniqueId:Ljava/lang/String;

    .line 391
    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 394
    :cond_1dc
    if-eqz p3, :cond_231

    .line 395
    const/4 v10, 0x0

    .line 397
    .local v10, bitmap:Landroid/graphics/Bitmap;
    :try_start_1df
    new-instance v21, Lcom/google/android/apps/plus/content/LocalImageRequest;

    const/16 v6, 0x140

    const/16 v7, 0x140

    move-object/from16 v0, v21

    invoke-direct {v0, v2, v6, v7}, Lcom/google/android/apps/plus/content/LocalImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .line 399
    .local v21, request:Lcom/google/android/apps/plus/content/LocalImageRequest;
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsPhotosData;->loadLocalBitmap(Landroid/content/Context;Lcom/google/android/apps/plus/content/LocalImageRequest;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 401
    if-nez v10, :cond_218

    .line 402
    const-string v6, "HttpTransaction"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Bitmap decoding failed for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_20c
    .catchall {:try_start_1df .. :try_end_20c} :catchall_22a

    .line 413
    :goto_20c
    if-eqz v10, :cond_211

    .line 415
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 435
    .end local v10           #bitmap:Landroid/graphics/Bitmap;
    .end local v21           #request:Lcom/google/android/apps/plus/content/LocalImageRequest;
    :cond_211
    :goto_211
    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3a

    .line 404
    .restart local v10       #bitmap:Landroid/graphics/Bitmap;
    .restart local v21       #request:Lcom/google/android/apps/plus/content/LocalImageRequest;
    :cond_218
    const/16 v6, 0x55

    :try_start_21a
    invoke-static {v10, v6}, Lcom/google/android/apps/plus/util/ImageUtils;->compressBitmap(Landroid/graphics/Bitmap;I)[B

    move-result-object v11

    .line 406
    .local v11, bitmapBytes:[B
    const/4 v6, 0x0

    invoke-static {v11, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v13

    .line 408
    .local v13, encodedBitmap:Ljava/lang/String;
    iput-object v13, v15, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageData:Ljava/lang/String;

    .line 409
    const-string v6, "1"

    iput-object v6, v15, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageStatus:Ljava/lang/String;
    :try_end_229
    .catchall {:try_start_21a .. :try_end_229} :catchall_22a

    goto :goto_20c

    .line 413
    .end local v11           #bitmapBytes:[B
    .end local v13           #encodedBitmap:Ljava/lang/String;
    .end local v21           #request:Lcom/google/android/apps/plus/content/LocalImageRequest;
    :catchall_22a
    move-exception v6

    if-eqz v10, :cond_230

    .line 415
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    :cond_230
    throw v6

    .line 418
    .end local v10           #bitmap:Landroid/graphics/Bitmap;
    :cond_231
    if-nez v17, :cond_236

    .line 420
    const/4 v15, 0x0

    goto/16 :goto_3a

    .line 422
    :cond_236
    const-string v6, "2"

    iput-object v6, v15, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageStatus:Ljava/lang/String;

    goto :goto_211

    .line 425
    :cond_23b
    new-instance v22, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;

    invoke-direct/range {v22 .. v22}, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;-><init>()V

    .line 427
    .local v22, sourcePhoto:Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v22

    iput-object v6, v0, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;->obfuscatedOwnerId:Ljava/lang/String;

    .line 428
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v22

    iput-object v6, v0, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;->photoId:Ljava/lang/String;

    .line 429
    move-object/from16 v0, v22

    iput-object v0, v15, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->sourcePhoto:Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;

    .line 430
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_211
.end method

.method private fillPostParams(Lcom/google/api/services/plusi/model/PostActivityRequest;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Lcom/google/api/services/plusi/model/PostActivityRequest;
    .registers 11
    .parameter "postParams"
    .parameter "info"

    .prologue
    .line 225
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContent:Ljava/lang/String;

    iput-object v5, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateText:Ljava/lang/String;

    .line 226
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mExternalId:Ljava/lang/String;

    iput-object v5, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->externalId:Ljava/lang/String;

    .line 228
    if-eqz p2, :cond_4a

    invoke-virtual {p2}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v5

    if-eqz v5, :cond_4a

    .line 229
    invoke-virtual {p2}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 230
    .local v4, pkg:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4a

    .line 231
    const-string v2, "Mobile"

    .line 232
    .local v2, name:Ljava/lang/String;
    const-string v5, "com.google.android.apps.social"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_41

    .line 234
    :try_start_28
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v6, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 236
    .local v0, appName:Ljava/lang/CharSequence;
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_40
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_28 .. :try_end_40} :catch_90

    move-result-object v2

    .line 241
    .end local v0           #appName:Ljava/lang/CharSequence;
    :cond_41
    :goto_41
    new-instance v1, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;-><init>()V

    .line 243
    .local v1, attribution:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;
    iput-object v2, v1, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;->androidAppName:Ljava/lang/String;

    .line 245
    iput-object v1, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->attribution:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;

    .line 249
    .end local v1           #attribution:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;
    .end local v2           #name:Ljava/lang/String;
    .end local v4           #pkg:Ljava/lang/String;
    :cond_4a
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

    if-eqz v5, :cond_60

    .line 250
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/network/ApiaryActivity;->getMediaJson()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->mediaJson:Ljava/lang/String;

    .line 251
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

    iget-object v6, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mDeepLinkId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/network/ApiaryActivity;->getEmbed(Ljava/lang/String;)Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-result-object v5

    iput-object v5, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    .line 254
    :cond_60
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAttachments:Ljava/util/List;

    if-eqz v5, :cond_7b

    .line 255
    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAttachments:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPostedAttachments:Ljava/util/List;

    .line 256
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAttachments:Ljava/util/List;

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->getPhotosShareData(Ljava/util/List;)Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    move-result-object v3

    .line 258
    .local v3, photosShareData:Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;
    if-eqz v3, :cond_7b

    .line 259
    iput-object v3, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->photosShareData:Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    .line 263
    .end local v3           #photosShareData:Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;
    :cond_7b
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v5, :cond_87

    .line 264
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbLocation;->toProtocolObject()Lcom/google/api/services/plusi/model/Location;

    move-result-object v5

    iput-object v5, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->location:Lcom/google/api/services/plusi/model/Location;

    .line 267
    :cond_87
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;

    move-result-object v5

    iput-object v5, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    .line 268
    return-object p1

    .restart local v2       #name:Ljava/lang/String;
    .restart local v4       #pkg:Ljava/lang/String;
    :catch_90
    move-exception v5

    goto :goto_41
.end method

.method private static getFingerprint(Landroid/content/Context;J)Lcom/android/gallery3d/common/Fingerprint;
    .registers 11
    .parameter "context"
    .parameter "photoId"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 473
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    .line 474
    .local v6, account:Lcom/google/android/apps/plus/content/EsAccount;
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 477
    .local v1, queryUri:Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "fingerprint"

    aput-object v0, v2, v4

    .line 479
    .local v2, projection:[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 481
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_3d

    .line 483
    :try_start_23
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3e

    .line 484
    new-instance v3, Lcom/android/gallery3d/common/Fingerprint;

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V
    :try_end_3a
    .catchall {:try_start_23 .. :try_end_3a} :catchall_42

    .line 487
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 490
    :cond_3d
    :goto_3d
    return-object v3

    .line 487
    :cond_3e
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_3d

    :catchall_42
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getFingerprint(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/gallery3d/common/Fingerprint;
    .registers 10
    .parameter "context"
    .parameter "localUri"

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 448
    invoke-static {p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v0

    invoke-virtual {v0, v5, v4}, Lcom/google/android/picasastore/PicasaStoreFacade;->getFingerprintUri(ZZ)Landroid/net/Uri;

    move-result-object v1

    .line 449
    .local v1, queryUri:Landroid/net/Uri;
    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 450
    .local v2, projection:[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 452
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_39

    .line 454
    :try_start_1f
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3a

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 455
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 456
    .local v6, bytes:[B
    new-instance v3, Lcom/android/gallery3d/common/Fingerprint;

    invoke-direct {v3, v6}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V
    :try_end_36
    .catchall {:try_start_1f .. :try_end_36} :catchall_3e

    .line 459
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 462
    .end local v6           #bytes:[B
    :cond_39
    :goto_39
    return-object v3

    .line 459
    :cond_3a
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_39

    :catchall_3e
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getPhotoIdFromStream(Landroid/content/Context;Ljava/lang/String;)J
    .registers 12
    .parameter "context"
    .parameter "streamId"

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 502
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    .line 503
    .local v6, account:Lcom/google/android/apps/plus/content/EsAccount;
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    .line 504
    .local v7, builder:Landroid/net/Uri$Builder;
    invoke-virtual {v7, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 506
    .local v9, uri:Landroid/net/Uri;
    invoke-static {v9, v6}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 507
    .local v1, queryUri:Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "photo_id"

    aput-object v0, v2, v5

    .line 509
    .local v2, projection:[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 511
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_4c

    .line 513
    :try_start_33
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 514
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_49

    .line 515
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_44
    .catchall {:try_start_33 .. :try_end_44} :catchall_4f

    move-result-wide v3

    .line 519
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 522
    :goto_48
    return-wide v3

    .line 519
    :cond_49
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 522
    :cond_4c
    const-wide/16 v3, 0x0

    goto :goto_48

    .line 519
    :catchall_4f
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private getPhotosShareData(Ljava/util/List;)Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)",
            "Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;"
        }
    .end annotation

    .prologue
    .local p1, attachments:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/api/MediaRef;>;"
    const/4 v10, 0x0

    const/4 v12, 0x4

    .line 130
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 162
    :cond_8
    :goto_8
    return-object v10

    .line 134
    :cond_9
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .local v9, mediaRefList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;>;"
    new-instance v5, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAttachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 138
    .local v5, photoSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Long;>;"
    new-instance v6, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAttachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 139
    .local v6, clientSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 140
    const/4 v11, 0x0

    .line 141
    .local v11, thumbnailCount:I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_2e
    :goto_2e
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    .line 142
    .local v2, attachment:Lcom/google/android/apps/plus/api/MediaRef;
    if-ge v11, v12, :cond_5a

    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPostedAttachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v12, :cond_5a

    const/4 v3, 0x1

    .line 144
    .local v3, attachBytes:Z
    :goto_45
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPostedAttachments:Ljava/util/List;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->buildMediaReference$13d52a54(Landroid/content/Context;Lcom/google/android/apps/plus/api/MediaRef;ZLjava/util/List;Ljava/util/Set;Ljava/util/Set;)Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;

    move-result-object v8

    .line 146
    .local v8, mediaRef:Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;
    if-eqz v8, :cond_2e

    .line 147
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    iget-object v0, v8, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageData:Ljava/lang/String;

    if-eqz v0, :cond_2e

    .line 150
    add-int/lit8 v11, v11, 0x1

    goto :goto_2e

    .line 142
    .end local v3           #attachBytes:Z
    .end local v8           #mediaRef:Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;
    :cond_5a
    const/4 v3, 0x0

    goto :goto_45

    .line 155
    .end local v2           #attachment:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_5c
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 160
    new-instance v10, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    invoke-direct {v10}, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;-><init>()V

    .line 161
    .local v10, photosShareData:Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;
    iput-object v9, v10, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;->mediaRef:Ljava/util/List;

    goto :goto_8
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 14
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 65
    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->uploadsUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "account"

    invoke-virtual {v6, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/InstantUpload;->isEnabled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_51

    const-string v3, "0"

    iget-object v7, v0, Lcom/google/api/services/plusi/model/Update;->albumId:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_51

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->albumId:Ljava/lang/String;

    const-string v3, "album_id"

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_51
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPostedAttachments:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mPostedAttachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5b
    :goto_5b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v7

    if-nez v7, :cond_5b

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v7

    if-eqz v7, :cond_5b

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    const-string v7, "HttpTransaction"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_98

    const-string v7, "HttpTransaction"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  -- on-demand upload; img: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_98
    const-string v7, "content_uri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_5b

    :cond_a5
    invoke-static {v10, v10, v10, v11, v11}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "DEFAULT"

    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    if-eqz v0, :cond_bf

    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->savePostingPreferences(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ShareboxSettings;)V

    :cond_bf
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 65
    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->fillPostParams(Lcom/google/api/services/plusi/model/PostActivityRequest;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Lcom/google/api/services/plusi/model/PostActivityRequest;

    return-void
.end method
