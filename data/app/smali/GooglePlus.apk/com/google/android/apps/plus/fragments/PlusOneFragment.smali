.class public Lcom/google/android/apps/plus/fragments/PlusOneFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "PlusOneFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

.field private mInsert:Z

.field private mLoggedPreview:Z

.field private final mPreviewLoaderCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private mProgressView:Landroid/widget/ProgressBar;

.field private mRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mToken:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    .line 71
    new-instance v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 81
    new-instance v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment$PreviewLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mPreviewLoaderCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 311
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PlusOneFragment;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_1e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2, v0, p1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    :cond_1e
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mInsert:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mLoggedPreview:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/fragments/PlusOneFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mLoggedPreview:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/PlusOneFragment;Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090198

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    if-eqz p1, :cond_1f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/views/ActivityPreviewViewFactory;->createViewFromActivity(Landroid/content/Context;Lcom/google/android/apps/plus/network/ApiaryActivity;)Lcom/google/android/apps/plus/views/ActivityPreviewView;

    move-result-object v1

    if-eqz v1, :cond_1f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1f
    return-void
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PlusOneFragment#mAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 301
    const/4 v0, 0x0

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .registers 25
    .parameter "savedInstanceState"

    .prologue
    .line 175
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 178
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    .line 179
    .local v12, args:Landroid/os/Bundle;
    const-string v2, "PlusOneFragment#mApiaryApiInfo"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 180
    const-string v2, "PlusOneFragment#mToken"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mToken:Ljava/lang/String;

    .line 181
    const-string v2, "PlusOneFragment#mUrl"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mUrl:Ljava/lang/String;

    .line 182
    const-string v2, "PlusOneFragment#mInsert"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mInsert:Z

    .line 183
    const-string v2, "PlusOneFragment#mAccount"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 185
    if-nez p1, :cond_195

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mInsert:Z

    if-eqz v2, :cond_195

    .line 187
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v3}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    .line 189
    .local v4, analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mInsert:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mToken:Ljava/lang/String;

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/plus/service/EsService;->applyPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/lang/String;ZLjava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    .line 191
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mLoggedPreview:Z

    .line 198
    .end local v4           #analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    :cond_7f
    :goto_7f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f090193

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/views/AvatarView;

    .line 200
    .local v13, avatarView:Lcom/google/android/apps/plus/views/AvatarView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 202
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f090194

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 203
    .local v22, userText:Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802d2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v18

    .line 206
    .local v18, pkgName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 208
    .local v17, pkgManager:Landroid/content/pm/PackageManager;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f090196

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 209
    .local v9, appIcon:Landroid/widget/ImageView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f090197

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 211
    .local v11, appText:Landroid/widget/TextView;
    :try_start_f4
    invoke-virtual/range {v17 .. v18}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 213
    const/4 v2, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v10

    .line 215
    .local v10, appName:Ljava/lang/CharSequence;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802d3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v10, v5, v6

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_122
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_f4 .. :try_end_122} :catch_1c1

    .line 224
    .end local v10           #appName:Ljava/lang/CharSequence;
    :goto_122
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f090199

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/Button;

    .line 225
    .local v19, shareButton:Landroid/widget/Button;
    new-instance v2, Lcom/google/android/apps/plus/fragments/PlusOneFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f09019a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    .line 246
    .local v14, continueButton:Landroid/widget/Button;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f09019b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/Button;

    .line 249
    .local v20, undoButton:Landroid/widget/Button;
    new-instance v21, Lcom/google/android/apps/plus/fragments/PlusOneFragment$3;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)V

    .line 271
    .local v21, undoOnClickListener:Landroid/view/View$OnClickListener;
    new-instance v16, Lcom/google/android/apps/plus/fragments/PlusOneFragment$4;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/PlusOneFragment;)V

    .line 285
    .local v16, okOnClickListener:Landroid/view/View$OnClickListener;
    const v2, 0x7f080336

    invoke-virtual {v14, v2}, Landroid/widget/Button;->setText(I)V

    .line 286
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    const v2, 0x7f080335

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 289
    invoke-virtual/range {v20 .. v21}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v5, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mPreviewLoaderCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v2, v3, v5, v6}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    .line 294
    return-void

    .line 192
    .end local v9           #appIcon:Landroid/widget/ImageView;
    .end local v11           #appText:Landroid/widget/TextView;
    .end local v13           #avatarView:Lcom/google/android/apps/plus/views/AvatarView;
    .end local v14           #continueButton:Landroid/widget/Button;
    .end local v16           #okOnClickListener:Landroid/view/View$OnClickListener;
    .end local v17           #pkgManager:Landroid/content/pm/PackageManager;
    .end local v18           #pkgName:Ljava/lang/String;
    .end local v19           #shareButton:Landroid/widget/Button;
    .end local v20           #undoButton:Landroid/widget/Button;
    .end local v21           #undoOnClickListener:Landroid/view/View$OnClickListener;
    .end local v22           #userText:Landroid/widget/TextView;
    :cond_195
    if-eqz p1, :cond_7f

    .line 193
    const-string v2, "PlusOneFragment#mRequestId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1bf

    const-string v2, "PlusOneFragment#mRequestId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_1ad
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    .line 195
    const-string v2, "PlusOneFragment#mLoggedPreview"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mLoggedPreview:Z

    goto/16 :goto_7f

    .line 193
    :cond_1bf
    const/4 v2, 0x0

    goto :goto_1ad

    .line 217
    .restart local v9       #appIcon:Landroid/widget/ImageView;
    .restart local v11       #appText:Landroid/widget/TextView;
    .restart local v13       #avatarView:Lcom/google/android/apps/plus/views/AvatarView;
    .restart local v17       #pkgManager:Landroid/content/pm/PackageManager;
    .restart local v18       #pkgName:Ljava/lang/String;
    .restart local v22       #userText:Landroid/widget/TextView;
    :catch_1c1
    move-exception v15

    .line 220
    .local v15, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x4

    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    const/4 v2, 0x4

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_122
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 383
    const/4 v0, 0x1

    if-ne p1, v0, :cond_11

    .line 384
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 387
    :cond_11
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 99
    const v0, 0x7f030091

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final onFinishedWrite(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "result"

    .prologue
    .line 107
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 110
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 111
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 113
    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    .line 114
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 135
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 137
    return-void
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 144
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_27

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_27

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 149
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    if-eqz v0, :cond_2d

    .line 150
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->onFinishedWrite(Lcom/google/android/apps/plus/service/ServiceResult;)V

    .line 157
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_27
    :goto_27
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    .line 158
    return-void

    .line 153
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_2d
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    goto :goto_27
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1a

    .line 123
    const-string v0, "PlusOneFragment#mRequestId"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 127
    :goto_12
    const-string v0, "PlusOneFragment#mLoggedPreview"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mLoggedPreview:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 128
    return-void

    .line 125
    :cond_1a
    const-string v0, "PlusOneFragment#mRequestId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_12
.end method

.method public final setProgressBar(Landroid/widget/ProgressBar;)V
    .registers 3
    .parameter "progressView"

    .prologue
    .line 390
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    .line 392
    return-void
.end method

.method protected final updateSpinner(Landroid/widget/ProgressBar;)V
    .registers 4
    .parameter "progressView"

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_f

    .line 403
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mProgressView:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->mRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_10

    const/16 v0, 0x8

    :goto_c
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 406
    :cond_f
    return-void

    .line 403
    :cond_10
    const/4 v0, 0x0

    goto :goto_c
.end method
