.class public final Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PhotosCreateCommentOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;",
        "Lcom/google/api/services/plusi/model/PhotosCreateCommentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mComment:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "ownerId"
    .parameter "comment"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 31
    const-string v3, "photoscreatecomment"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosCreateCommentRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosCreateCommentResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosCreateCommentResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 34
    iput-wide p3, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mPhotoId:J

    .line 35
    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mOwnerId:Ljava/lang/String;

    .line 36
    iput-object p6, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mComment:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentResponse;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->onStartResultProcessing()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mPhotoId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentResponse;->comment:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoCommentList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 20
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;->obfuscatedOwnerId:Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mPhotoId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;->photoId:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mComment:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;->comment:Ljava/lang/String;

    return-void
.end method
