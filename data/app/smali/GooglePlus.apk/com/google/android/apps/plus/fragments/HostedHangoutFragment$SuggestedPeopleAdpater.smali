.class final Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "HostedHangoutFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuggestedPeopleAdpater"
.end annotation


# instance fields
.field final mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 5
    .parameter
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 185
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 187
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 10
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    const/4 v2, 0x1

    .line 231
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;

    .line 232
    .local v0, item:Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 233
    .local v1, participantId:Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->setPersonId(Ljava/lang/String;)V

    .line 234
    const/4 v3, 0x2

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, " .*"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->setParticipantName(Ljava/lang/String;)V

    .line 236
    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_26

    :goto_22
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->setChecked(Z)V

    .line 237
    return-void

    .line 236
    :cond_26
    const/4 v2, 0x0

    goto :goto_22
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "container"

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0300cc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onItemClick(I)V
    .registers 10
    .parameter "position"

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 191
    .local v0, cursor:Landroid/database/Cursor;
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 192
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "g:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_37

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1e
    new-instance v1, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v1, v3, v4, v2}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .local v1, person:Lcom/google/android/apps/plus/content/PersonData;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantIdFromPerson(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v3

    #calls: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isInAudience(Ljava/lang/String;)Z
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_53

    .line 194
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->removePerson(Lcom/google/android/apps/plus/content/PersonData;)V

    .line 198
    :goto_36
    return-void

    .line 192
    .end local v1           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_37
    const-string v5, "e:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_47

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    move-object v3, v2

    move-object v2, v7

    goto :goto_1e

    :cond_47
    const-string v5, "p:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5b

    move-object v7, v3

    move-object v3, v2

    move-object v2, v7

    goto :goto_1e

    .line 196
    .restart local v1       #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_53
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_36

    .end local v1           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_5b
    move-object v3, v2

    goto :goto_1e
.end method
