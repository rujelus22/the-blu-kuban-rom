.class public final Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetPlusOnePeopleOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;",
        "Lcom/google/api/services/plusi/model/GetPlusonePeopleResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mNumPeopleToReturn:I

.field private mPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPerson;",
            ">;"
        }
    .end annotation
.end field

.field private final mPlusOneId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;I)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "plusOneId"
    .parameter "numPeopleToReturn"

    .prologue
    const/4 v6, 0x0

    .line 40
    const-string v3, "getplusonepeople"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetPlusonePeopleRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPlusonePeopleResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetPlusonePeopleResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 47
    iput-object p5, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPlusOneId:Ljava/lang/String;

    .line 48
    const/16 v0, 0x32

    iput v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mNumPeopleToReturn:I

    .line 49
    return-void
.end method


# virtual methods
.method public final getPeople()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPerson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPeople:Ljava/util/List;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleResponse;->person:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPeople:Ljava/util/List;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPlusOneId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;->plusoneId:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mNumPeopleToReturn:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;->numPeopleToReturn:Ljava/lang/Integer;

    return-void
.end method
