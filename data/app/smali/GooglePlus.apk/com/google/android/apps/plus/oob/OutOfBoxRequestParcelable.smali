.class public Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;
.super Ljava/lang/Object;
.source "OutOfBoxRequestParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 61
    .local v1, length:I
    if-lez v1, :cond_1a

    .line 62
    new-array v0, v1, [B

    .line 63
    .local v0, jsonData:[B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 64
    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iput-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    .line 66
    .end local v0           #jsonData:[B
    :cond_1a
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V
    .registers 2
    .parameter "request"

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public final getRequest()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "out"
    .parameter "flags"

    .prologue
    .line 36
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    if-eqz v1, :cond_16

    .line 37
    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v0

    .line 38
    .local v0, jsonData:[B
    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 43
    .end local v0           #jsonData:[B
    :goto_15
    return-void

    .line 41
    :cond_16
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_15
.end method
