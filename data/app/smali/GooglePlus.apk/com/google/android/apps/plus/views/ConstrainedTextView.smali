.class public Lcom/google/android/apps/plus/views/ConstrainedTextView;
.super Landroid/view/View;
.source "ConstrainedTextView.java"


# instance fields
.field private mContentLayout:Landroid/text/StaticLayout;

.field private mMaxLines:I

.field private mText:Ljava/lang/CharSequence;

.field private mTextPaint:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 11
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    sget-object v4, Lcom/google/android/apps/plus/R$styleable;->ConstrainedTextView:[I

    invoke-virtual {p1, p2, v4, v5, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 52
    .local v1, color:I
    invoke-virtual {v0, v6, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 53
    .local v3, textStyle:I
    const/4 v4, 0x0

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    .line 54
    .local v2, textSize:F
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    .line 56
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4}, Landroid/text/TextPaint;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    .line 57
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 58
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 59
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 60
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 61
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_9

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 155
    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .registers 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v7, 0x4000

    .line 119
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 120
    .local v4, width:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 121
    .local v1, height:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 124
    .local v2, heightMode:I
    const/high16 v5, -0x8000

    if-eq v2, v5, :cond_14

    if-ne v2, v7, :cond_47

    .line 126
    :cond_14
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v5}, Landroid/text/TextPaint;->descent()F

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    float-to-int v5, v5

    div-int v3, v1, v5

    .line 127
    .local v3, maxLines:I
    iget v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    if-lez v5, :cond_2e

    .line 128
    iget v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 135
    :cond_2e
    :goto_2e
    if-lez v3, :cond_4a

    .line 136
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    invoke-static {v5, v6, v4, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Landroid/text/StaticLayout;

    .line 138
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    .line 144
    .local v0, desiredHeight:I
    :goto_40
    if-eq v2, v7, :cond_43

    .line 145
    move v1, v0

    .line 147
    :cond_43
    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setMeasuredDimension(II)V

    .line 148
    return-void

    .line 131
    .end local v0           #desiredHeight:I
    .end local v3           #maxLines:I
    :cond_47
    iget v3, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    .restart local v3       #maxLines:I
    goto :goto_2e

    .line 140
    :cond_4a
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Landroid/text/StaticLayout;

    .line 141
    const/4 v0, 0x0

    .restart local v0       #desiredHeight:I
    goto :goto_40
.end method

.method public setMaxLines(I)V
    .registers 2
    .parameter "maxLines"

    .prologue
    .line 82
    iput p1, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    .line 84
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "text"

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    .line 69
    return-void
.end method

.method public setTextColor(I)V
    .registers 3
    .parameter "color"

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    .line 99
    return-void
.end method

.method public setTextSize(F)V
    .registers 3
    .parameter "textSize"

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    .line 107
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .parameter "typeface"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    .line 115
    return-void
.end method
