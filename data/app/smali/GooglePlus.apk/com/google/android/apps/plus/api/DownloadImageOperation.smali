.class public final Lcom/google/android/apps/plus/api/DownloadImageOperation;
.super Lcom/google/android/apps/plus/network/HttpOperation;
.source "DownloadImageOperation.java"


# instance fields
.field private mImageBytes:[B

.field private final mRequest:Lcom/google/android/apps/plus/content/CachedImageRequest;

.field private final mSaveToCache:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "request"
    .parameter "intent"
    .parameter "listener"

    .prologue
    const/4 v5, 0x0

    .line 52
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/DownloadImageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "request"
    .parameter "saveToCache"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 68
    const-string v2, "GET"

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/CachedImageRequest;->getDownloadUrl()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x3a98

    invoke-direct {v5, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 70
    iput-object p3, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mRequest:Lcom/google/android/apps/plus/content/CachedImageRequest;

    .line 71
    iput-boolean p4, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mSaveToCache:Z

    .line 72
    return-void
.end method


# virtual methods
.method public final getImageBytes()[B
    .registers 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mImageBytes:[B

    return-object v0
.end method

.method protected final onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V
    .registers 8
    .parameter "inputStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->onStartResultProcessing()V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    check-cast v0, Ljava/io/ByteArrayOutputStream;

    .line 84
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    :try_start_9
    iget-object v2, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mRequest:Lcom/google/android/apps/plus/content/CachedImageRequest;

    instance-of v2, v2, Lcom/google/android/apps/plus/content/MediaImageRequest;

    if-eqz v2, :cond_9f

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mRequest:Lcom/google/android/apps/plus/content/CachedImageRequest;

    check-cast v2, Lcom/google/android/apps/plus/content/MediaImageRequest;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getMediaType()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_20

    const/4 v4, 0x2

    if-eq v3, v4, :cond_20

    const/4 v4, 0x1

    if-ne v3, v4, :cond_30

    :cond_20
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/util/GifDrawable;->isGif([B)Z

    move-result v3

    if-eqz v3, :cond_42

    iput-object v5, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mImageBytes:[B

    .line 90
    :cond_30
    :goto_30
    iget-boolean v2, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mSaveToCache:Z

    if-eqz v2, :cond_41

    iget-object v2, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mImageBytes:[B

    if-eqz v2, :cond_41

    .line 91
    iget-object v2, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mRequest:Lcom/google/android/apps/plus/content/CachedImageRequest;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mImageBytes:[B

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/content/EsMediaCache;->insertMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;[B)V

    .line 96
    :cond_41
    return-void

    .line 85
    :cond_42
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getHeight()I

    move-result v2

    if-gtz v3, :cond_5c

    if-gtz v2, :cond_5c

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8b

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/FIFEUtil;->getImageUrlSize(Ljava/lang/String;)Landroid/graphics/Point;

    move-result-object v2

    :goto_58
    iget v3, v2, Landroid/graphics/Point;->x:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    :cond_5c
    if-lez v3, :cond_95

    if-lez v2, :cond_95

    invoke-static {v5, v3, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeBitmap([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_90

    const/4 v2, 0x0

    :goto_67
    iput-object v2, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mImageBytes:[B
    :try_end_69
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_69} :catch_6a

    goto :goto_30

    .line 93
    :catch_6a
    move-exception v1

    .line 94
    .local v1, e:Ljava/lang/OutOfMemoryError;
    const-string v2, "HttpTransaction"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DownloadImageOperation OutOfMemoryError on image bytes: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 95
    new-instance v2, Lcom/google/android/apps/plus/api/ProtocolException;

    const-string v3, "Cannot handle downloaded image"

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/api/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 85
    .end local v1           #e:Ljava/lang/OutOfMemoryError;
    :cond_8b
    :try_start_8b
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->getImageUrlSize(Ljava/lang/String;)Landroid/graphics/Point;

    move-result-object v2

    goto :goto_58

    :cond_90
    invoke-static {v2}, Lcom/google/android/apps/plus/util/ImageUtils;->compressBitmap(Landroid/graphics/Bitmap;)[B

    move-result-object v2

    goto :goto_67

    :cond_95
    const-string v2, "HttpTransaction"

    const-string v3, "DownloadImageOperation could not resize image; width or height were not specified"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v5, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mImageBytes:[B

    goto :goto_30

    .line 87
    :cond_9f
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/api/DownloadImageOperation;->mImageBytes:[B
    :try_end_a5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8b .. :try_end_a5} :catch_6a

    goto :goto_30
.end method
