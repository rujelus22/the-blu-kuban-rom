.class public abstract Lcom/google/android/apps/plus/content/EsLocalPageData;
.super Ljava/lang/Object;
.source "EsLocalPageData.java"


# direct methods
.method private static buildOpeningHoursStringForADay(Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;)Ljava/lang/String;
    .registers 8
    .parameter "day"

    .prologue
    const/4 v4, 0x0

    .line 292
    iget-object v5, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->interval:Ljava/util/List;

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->interval:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 312
    :cond_d
    :goto_d
    return-object v4

    .line 295
    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 296
    .local v3, intervals:Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    .line 297
    .local v0, firstInterval:Z
    iget-object v5, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->interval:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_1a
    :goto_1a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;

    .line 298
    .local v2, interval:Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;
    iget-object v5, v2, Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;->value:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 299
    if-nez v0, :cond_35

    .line 300
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_35
    const/4 v0, 0x0

    .line 303
    iget-object v5, v2, Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;->value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1a

    .line 306
    .end local v2           #interval:Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;
    :cond_3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_d

    .line 309
    iget-object v4, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->dayName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_63

    .line 310
    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->dayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :cond_63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_d
.end method

.method private static getCircleActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;
    .registers 2
    .parameter "profile"

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->circleActivity:Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;

    return-object v0
.end method

.method public static getCircleReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;
    .registers 6
    .parameter "profile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/SimpleProfile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/GoogleReviewProto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v0, circleReviews:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/GoogleReviewProto;>;"
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getCircleActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;

    move-result-object v3

    .line 233
    .local v3, story:Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasCircleActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 234
    iget-object v4, v3, Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;->activity:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_15
    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;

    .line 235
    .local v1, entry:Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;
    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    if-eqz v4, :cond_15

    .line 236
    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_15

    .line 240
    .end local v1           #entry:Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_2b
    return-object v0
.end method

.method public static getFullAddress(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .registers 6
    .parameter "profile"

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 96
    iget-object v3, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/FrontendPaperProto;->address:Lcom/google/api/services/plusi/model/PlacePageAddressProto;

    if-nez v3, :cond_d

    .line 108
    :cond_c
    :goto_c
    return-object v2

    .line 99
    :cond_d
    iget-object v3, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/FrontendPaperProto;->address:Lcom/google/api/services/plusi/model/PlacePageAddressProto;

    iget-object v1, v3, Lcom/google/api/services/plusi/model/PlacePageAddressProto;->addressLine:Ljava/util/List;

    .line 100
    .local v1, addressLines:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_c

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_c

    .line 103
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 104
    .local v0, address:Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 105
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v4, :cond_43

    .line 106
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 108
    :cond_43
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_c
.end method

.method public static getOpeningHoursFull(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .registers 9
    .parameter "profile"

    .prologue
    const/4 v6, 0x0

    .line 316
    iget-object v7, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->openingHours:Lcom/google/api/services/plusi/model/OpeningHoursProto;

    .line 317
    .local v5, openingHours:Lcom/google/api/services/plusi/model/OpeningHoursProto;
    if-eqz v5, :cond_17

    iget-object v7, v5, Lcom/google/api/services/plusi/model/OpeningHoursProto;->day:Ljava/util/List;

    if-eqz v7, :cond_17

    iget-object v7, v5, Lcom/google/api/services/plusi/model/OpeningHoursProto;->day:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_18

    .line 335
    :cond_17
    :goto_17
    return-object v6

    .line 320
    :cond_18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 321
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v3, 0x1

    .line 322
    .local v3, firstDay:Z
    iget-object v7, v5, Lcom/google/api/services/plusi/model/OpeningHoursProto;->day:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :cond_24
    :goto_24
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_42

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;

    .line 323
    .local v1, day:Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;
    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsLocalPageData;->buildOpeningHoursStringForADay(Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;)Ljava/lang/String;

    move-result-object v2

    .line 324
    .local v2, dayOpeningHours:Ljava/lang/String;
    if-eqz v2, :cond_24

    .line 325
    if-nez v3, :cond_3d

    .line 326
    const-string v7, "\n"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    :cond_3d
    const/4 v3, 0x0

    .line 329
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_24

    .line 332
    .end local v1           #day:Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;
    .end local v2           #dayOpeningHours:Ljava/lang/String;
    :cond_42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-eqz v7, :cond_17

    .line 335
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_17
.end method

.method public static getOpeningHoursSummary(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .registers 3
    .parameter "profile"

    .prologue
    .line 284
    iget-object v1, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v1, Lcom/google/api/services/plusi/model/FrontendPaperProto;->openingHours:Lcom/google/api/services/plusi/model/OpeningHoursProto;

    .line 285
    .local v0, openingHours:Lcom/google/api/services/plusi/model/OpeningHoursProto;
    if-eqz v0, :cond_e

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OpeningHoursProto;->today:Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;

    if-nez v1, :cond_10

    .line 286
    :cond_e
    const/4 v1, 0x0

    .line 288
    :goto_f
    return-object v1

    :cond_10
    iget-object v1, v0, Lcom/google/api/services/plusi/model/OpeningHoursProto;->today:Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsLocalPageData;->buildOpeningHoursStringForADay(Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;)Ljava/lang/String;

    move-result-object v1

    goto :goto_f
.end method

.method public static getPriceLabel(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .registers 3
    .parameter "profile"

    .prologue
    .line 153
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/AttributeProto;

    move-result-object v0

    .line 154
    .local v0, priceAttribute:Lcom/google/api/services/plusi/model/AttributeProto;
    if-eqz v0, :cond_9

    iget-object v1, v0, Lcom/google/api/services/plusi/model/AttributeProto;->labelDisplay:Ljava/lang/String;

    :goto_8
    return-object v1

    :cond_9
    const/4 v1, 0x0

    goto :goto_8
.end method

.method private static getPriceStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/AttributeProto;
    .registers 2
    .parameter "profile"

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->priceContinuous:Lcom/google/api/services/plusi/model/AttributeProto;

    if-eqz v0, :cond_13

    .line 146
    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->priceContinuous:Lcom/google/api/services/plusi/model/AttributeProto;

    .line 148
    :goto_12
    return-object v0

    :cond_13
    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->price:Lcom/google/api/services/plusi/model/AttributeProto;

    goto :goto_12
.end method

.method public static getPriceValue(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .registers 3
    .parameter "profile"

    .prologue
    .line 158
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/AttributeProto;

    move-result-object v0

    .line 159
    .local v0, priceAttribute:Lcom/google/api/services/plusi/model/AttributeProto;
    if-eqz v0, :cond_b

    iget-object v1, v0, Lcom/google/api/services/plusi/model/AttributeProto;->value:Lcom/google/api/services/plusi/model/AttributeProtoCanonicalValue;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AttributeProtoCanonicalValue;->priceLevel:Ljava/lang/String;

    :goto_a
    return-object v1

    :cond_b
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public static getReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;
    .registers 7
    .parameter "profile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/SimpleProfile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/GoogleReviewProto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v4, reviews:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/GoogleReviewProto;>;"
    iget-object v5, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->googleReviews:Lcom/google/api/services/plusi/model/GoogleReviewsProto;

    .line 249
    .local v0, googleReviewsStory:Lcom/google/api/services/plusi/model/GoogleReviewsProto;
    if-eqz v0, :cond_34

    iget-object v5, v0, Lcom/google/api/services/plusi/model/GoogleReviewsProto;->review:Ljava/util/List;

    if-eqz v5, :cond_34

    iget-object v5, v0, Lcom/google/api/services/plusi/model/GoogleReviewsProto;->review:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_34

    const/4 v1, 0x1

    .line 252
    .local v1, hasReviews:Z
    :goto_1c
    if-eqz v1, :cond_36

    .line 253
    iget-object v5, v0, Lcom/google/api/services/plusi/model/GoogleReviewsProto;->review:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_36

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    .line 254
    .local v3, review:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_24

    .line 249
    .end local v1           #hasReviews:Z
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #review:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    :cond_34
    const/4 v1, 0x0

    goto :goto_1c

    .line 257
    .restart local v1       #hasReviews:Z
    :cond_36
    return-object v4
.end method

.method public static getUserActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;
    .registers 2
    .parameter "profile"

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->userActivity:Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;

    return-object v0
.end method

.method public static getYourReview(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/GoogleReviewProto;
    .registers 5
    .parameter "profile"

    .prologue
    .line 210
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getUserActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;

    move-result-object v2

    .line 211
    .local v2, story:Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasYourActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 212
    iget-object v3, v2, Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;->activity:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;

    .line 213
    .local v0, entry:Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;
    iget-object v3, v0, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    if-eqz v3, :cond_10

    .line 214
    iget-object v3, v0, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    .line 218
    .end local v0           #entry:Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;
    .end local v1           #i$:Ljava/util/Iterator;
    :goto_22
    return-object v3

    :cond_23
    const/4 v3, 0x0

    goto :goto_22
.end method

.method public static getZagatAspects(Lcom/google/api/services/plusi/model/GoogleReviewProto;)Ljava/util/List;
    .registers 2
    .parameter "review"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/GoogleReviewProto;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    if-eqz p0, :cond_6

    iget-object v0, p0, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    if-nez v0, :cond_8

    .line 348
    :cond_6
    const/4 v0, 0x0

    .line 350
    :goto_7
    return-object v0

    :cond_8
    iget-object v0, p0, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    goto :goto_7
.end method

.method private static hasActivity(Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;)Z
    .registers 3
    .parameter "story"

    .prologue
    .line 199
    if-eqz p0, :cond_14

    iget-object v1, p0, Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;->totalReviews:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;->totalReviews:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_14

    iget-object v1, p0, Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;->activity:Ljava/util/List;

    if-eqz v1, :cond_14

    const/4 v0, 0x1

    .line 203
    .local v0, hasActivity:Z
    :goto_13
    return v0

    .line 199
    .end local v0           #hasActivity:Z
    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public static hasCircleActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z
    .registers 3
    .parameter "profile"

    .prologue
    .line 194
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getCircleActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;

    move-result-object v0

    .line 195
    .local v0, story:Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasActivity(Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;)Z

    move-result v1

    return v1
.end method

.method public static hasYourActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z
    .registers 3
    .parameter "profile"

    .prologue
    .line 185
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getUserActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;

    move-result-object v0

    .line 186
    .local v0, story:Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasActivity(Lcom/google/api/services/plusi/model/CirclePlaceActivityProto;)Z

    move-result v1

    return v1
.end method
