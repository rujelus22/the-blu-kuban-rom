.class public final Lcom/google/android/apps/plus/content/EsEventData;
.super Ljava/lang/Object;
.source "EsEventData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;,
        Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;,
        Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;,
        Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;,
        Lcom/google/android/apps/plus/content/EsEventData$InviteeList;,
        Lcom/google/android/apps/plus/content/EsEventData$EventPerson;,
        Lcom/google/android/apps/plus/content/EsEventData$EventComment;,
        Lcom/google/android/apps/plus/content/EsEventData$EventActivity;
    }
.end annotation


# static fields
.field public static final ASPECT_ICON:Ljava/lang/String;

.field public static final ASPECT_LARGE:Ljava/lang/String;

.field public static final ASPECT_MEDIUM:Ljava/lang/String;

.field public static final ASPECT_SMALL:Ljava/lang/String;

.field public static final EVENT_ACTIVITY_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVENT_COALESCED_FRAME_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventComment;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVENT_PERSON_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;"
        }
    .end annotation
.end field

.field private static final EVENT_QUERY_PROJECTION:[Ljava/lang/String;

.field public static final FORMAT_GIF:Ljava/lang/String;

.field public static final FORMAT_JPG:Ljava/lang/String;

.field public static final FORMAT_MOV:Ljava/lang/String;

.field public static final INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$InviteeList;",
            ">;"
        }
    .end annotation
.end field

.field public static final RSVP_ATTENDING:Ljava/lang/String;

.field public static final RSVP_CHECKIN:Ljava/lang/String;

.field public static final RSVP_INVITED:Ljava/lang/String;

.field public static final RSVP_MAYBE:Ljava/lang/String;

.field public static final RSVP_NOT_ATTENDING:Ljava/lang/String;

.field public static final RSVP_NOT_ATTENDING_AND_REMOVE:Ljava/lang/String;

.field public static final RSVP_NOT_RESPONDED:Ljava/lang/String;

.field private static final SYNC_QUERY_PROJECTION:[Ljava/lang/String;

.field private static final mSyncLock:Ljava/lang/Object;

.field private static final sEventOperationSyncObject:Ljava/lang/Object;

.field private static sEventThemesLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->ATTENDING:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    .line 136
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->CHECKIN:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    .line 137
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->MAYBE:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    .line 138
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->INVITED:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_INVITED:Ljava/lang/String;

    .line 139
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->NOT_ATTENDING:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    .line 140
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->NOT_ATTENDING_AND_REMOVE:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING_AND_REMOVE:Ljava/lang/String;

    .line 142
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->NOT_RESPONDED:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$RsvpType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    .line 149
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;->GIF:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->FORMAT_GIF:Ljava/lang/String;

    .line 150
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;->JPG:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->FORMAT_JPG:Ljava/lang/String;

    .line 151
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;->MOV:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageFormat;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->FORMAT_MOV:Ljava/lang/String;

    .line 154
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->SMALL:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->ASPECT_SMALL:Ljava/lang/String;

    .line 155
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->MEDIUM:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->ASPECT_MEDIUM:Ljava/lang/String;

    .line 156
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->LARGE:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->ASPECT_LARGE:Ljava/lang/String;

    .line 157
    sget-object v0, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->ICON:Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;

    invoke-virtual {v0}, Lcom/google/apps/people/events/proto/EventsCommonData$CommonEnums$ImageAspectRatio;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->ASPECT_ICON:Ljava/lang/String;

    .line 188
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->mSyncLock:Ljava/lang/Object;

    .line 190
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v3

    const-string v1, "polling_token"

    aput-object v1, v0, v4

    const-string v1, "resume_token"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->SYNC_QUERY_PROJECTION:[Ljava/lang/String;

    .line 196
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "event_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    .line 215
    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_ACTIVITY_JSON:Lcom/google/android/apps/plus/json/EsJson;

    .line 230
    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventComment;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;

    .line 241
    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_PERSON_JSON:Lcom/google/android/apps/plus/json/EsJson;

    .line 251
    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;

    new-array v1, v5, [Ljava/lang/Object;

    const-class v2, Lcom/google/api/services/plusi/model/InviteeJson;

    aput-object v2, v1, v3

    const-string v2, "invitees"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    .line 261
    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;

    new-array v1, v5, [Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_PERSON_JSON:Lcom/google/android/apps/plus/json/EsJson;

    aput-object v2, v1, v3

    const-string v2, "people"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COALESCED_FRAME_JSON:Lcom/google/android/apps/plus/json/EsJson;

    .line 2525
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemesLock:Ljava/lang/Object;

    return-void
.end method

.method public static canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z
    .registers 4
    .parameter "event"
    .parameter "userGaiaId"

    .prologue
    .line 1331
    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1c

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1e

    :cond_1c
    const/4 v0, 0x1

    .line 1337
    .local v0, allowsPhotos:Z
    :goto_1d
    return v0

    .line 1331
    .end local v0           #allowsPhotos:Z
    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public static canRsvp(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .registers 2
    .parameter "event"

    .prologue
    .line 2704
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isBroadcastView:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isBroadcastView:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public static canViewerAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .registers 2
    .parameter "event"

    .prologue
    .line 2902
    if-eqz p0, :cond_18

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->canUploadPhotos:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->canUploadPhotos:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public static copyRsvpFromSummary(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 6
    .parameter "event"
    .parameter "account"

    .prologue
    .line 1861
    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v2, :cond_3d

    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3d

    .line 1862
    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/InviteeSummary;

    .line 1863
    .local v1, summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v2, :cond_12

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_12

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    if-eqz v2, :cond_12

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_INVITED:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 1866
    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    invoke-static {p0, p1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->setViewerInfoRsvp(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 1871
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    :cond_3d
    return-void
.end method

.method public static deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "eventId"

    .prologue
    .line 2301
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2303
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "events"

    const-string v4, "event_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2305
    .local v0, count:I
    const/4 v2, 0x0

    .line 2308
    .local v2, disableIs:Z
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 2309
    const/4 v2, 0x1

    .line 2310
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    .line 2313
    :cond_25
    if-lez v0, :cond_31

    .line 2314
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2317
    :cond_31
    const-string v3, "EsEventData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_56

    .line 2318
    const-string v4, "EsEventData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "[DELETE_EVENT], id: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v2, :cond_57

    const-string v3, "; disable IS"

    :goto_4b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2320
    :cond_56
    return-void

    .line 2318
    :cond_57
    const-string v3, ""

    goto :goto_4b
.end method

.method public static disableInstantShare(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    .prologue
    const/4 v1, 0x0

    .line 1432
    const-string v0, "EsEventData"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1433
    const-string v0, "EsEventData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#disableInstantShare; now: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1436
    :cond_22
    const-wide/16 v5, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShareInternal(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1437
    return-void
.end method

.method public static enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V
    .registers 14
    .parameter "context"
    .parameter "enabled"
    .parameter "event"

    .prologue
    const/4 v3, 0x4

    .line 1444
    const-string v0, "EsEventData"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1445
    const-string v0, "EsEventData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#enableInstantShare; event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1448
    :cond_1f
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/AlarmManager;

    .line 1450
    .local v7, alarmManager:Landroid/app/AlarmManager;
    iget-object v0, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getEventFinishedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    .line 1452
    .local v8, eventEndIntent:Landroid/app/PendingIntent;
    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v5

    .line 1453
    .local v5, eventEndAlarmTime:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 1455
    .local v9, now:J
    invoke-virtual {v7, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 1456
    if-eqz p1, :cond_86

    const-wide/16 v0, 0x1388

    add-long/2addr v0, v9

    cmp-long v0, v0, v5

    if-gez v0, :cond_86

    .line 1457
    const-string v0, "EsEventData"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 1458
    const-string v0, "EsEventData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#enableInstantShare; start IS; now: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", end: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wake in: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sub-long v2, v5, v9

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    :cond_73
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShareInternal(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1465
    const/4 v0, 0x0

    invoke-virtual {v7, v0, v5, v6, v8}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1474
    :goto_85
    return-void

    .line 1467
    :cond_86
    const-string v0, "EsEventData"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_ac

    .line 1468
    const-string v0, "EsEventData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#enableInstantShare; event over; now: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", end: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1472
    :cond_ac
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    goto :goto_85
.end method

.method private static enableInstantShareInternal(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "eventOwnerId"
    .parameter "eventName"
    .parameter "eventEndTime"

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 1382
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1383
    .local v3, resolver:Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1384
    .local v4, values:Landroid/content/ContentValues;
    if-eqz p2, :cond_5e

    move v1, v5

    .line 1385
    .local v1, enable:Z
    :goto_e
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1387
    .local v0, activeISEventId:Ljava/lang/String;
    if-eqz v1, :cond_24

    .line 1388
    const-string v6, "auto_upload_account_name"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1389
    const-string v6, "auto_upload_account_type"

    const-string v7, "com.google"

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    :cond_24
    const-string v6, "instant_share_eventid"

    invoke-virtual {v4, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    const-string v6, "instant_share_endtime"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1394
    sget-object v6, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->settingsUri:Landroid/net/Uri;

    invoke-virtual {v3, v6, v4, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1396
    if-eqz v1, :cond_60

    .line 1397
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/Intents;->getViewEventActivityNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1399
    .local v2, intent:Landroid/app/PendingIntent;
    invoke-static {p0, p4, v2, v5}, Lcom/google/android/apps/plus/util/NotificationUtils;->notifyInstantShareEnabled(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V

    .line 1401
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/InstantUpload;->ensureSyncEnabled$1f9c1b47(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 1405
    .end local v2           #intent:Landroid/app/PendingIntent;
    :goto_43
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->startMonitoring(Landroid/content/Context;)V

    .line 1407
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5d

    .line 1410
    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v6, Lcom/google/android/apps/plus/content/EsEventData$2;

    invoke-direct {v6, p0, v0, p1, p2}, Lcom/google/android/apps/plus/content/EsEventData$2;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1426
    :cond_5d
    return-void

    .line 1384
    .end local v0           #activeISEventId:Ljava/lang/String;
    .end local v1           #enable:Z
    :cond_5e
    const/4 v1, 0x0

    goto :goto_e

    .line 1403
    .restart local v0       #activeISEventId:Ljava/lang/String;
    .restart local v1       #enable:Z
    :cond_60
    invoke-static {p0}, Lcom/google/android/apps/plus/util/NotificationUtils;->cancelInstantShareEnabled(Landroid/content/Context;)V

    goto :goto_43
.end method

.method private static ensureFreshEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 12
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2531
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2537
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v7, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemesLock:Ljava/lang/Object;

    monitor-enter v7

    .line 2540
    :try_start_b
    const-string v6, "SELECT event_themes_sync_time  FROM account_status"

    const/4 v8, 0x0

    invoke-static {v2, v6, v8}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_3a
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_b .. :try_end_11} :catch_36

    move-result-wide v3

    .line 2547
    .local v3, lastSyncTimestamp:J
    :goto_12
    :try_start_12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v0, v8, v3

    .line 2548
    .local v0, age:J
    const-wide/32 v8, 0x5265c00

    cmp-long v6, v0, v8

    if-lez v6, :cond_34

    .line 2549
    new-instance v5, Lcom/google/android/apps/plus/api/GetEventThemesOperation;

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-direct {v5, p0, p1, v6, v8}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 2551
    .local v5, op:Lcom/google/android/apps/plus/api/GetEventThemesOperation;
    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->start()V

    .line 2552
    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->hasError()Z

    move-result v6

    if-eqz v6, :cond_34

    .line 2555
    const-string v6, "EsEventData"

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->logError(Ljava/lang/String;)V

    .line 2558
    .end local v5           #op:Lcom/google/android/apps/plus/api/GetEventThemesOperation;
    :cond_34
    monitor-exit v7
    :try_end_35
    .catchall {:try_start_12 .. :try_end_35} :catchall_3a

    return-void

    .line 2544
    .end local v0           #age:J
    .end local v3           #lastSyncTimestamp:J
    :catch_36
    move-exception v6

    const-wide/16 v3, -0x1

    .restart local v3       #lastSyncTimestamp:J
    goto :goto_12

    .line 2558
    .end local v3           #lastSyncTimestamp:J
    :catchall_3a
    move-exception v6

    monitor-exit v7

    throw v6
.end method

.method public static getDisplayTime(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)J
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "eventId"

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 660
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 662
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-array v4, v2, [Ljava/lang/String;

    aput-object p2, v4, v6

    .line 665
    .local v4, eventArgs:[Ljava/lang/String;
    const-wide/16 v9, 0x0

    .line 666
    .local v9, displayTime:J
    const-string v1, "events"

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "display_time"

    aput-object v3, v2, v6

    const-string v3, "event_id=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 670
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_21
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 671
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2b
    .catchall {:try_start_21 .. :try_end_2b} :catchall_30

    move-result-wide v9

    .line 674
    :cond_2c
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 676
    return-wide v9

    .line 674
    :catchall_30
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "projection"

    .prologue
    const/4 v5, 0x0

    .line 392
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 394
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "events"

    const-string v3, "event_id=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v4, v2

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public static getEventActivities$29a139(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "projection"

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 589
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 598
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v8, 0x0

    .line 600
    .local v8, displayTime:J
    :try_start_d
    const-string v1, "SELECT display_time FROM events WHERE event_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_18
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_d .. :try_end_18} :catch_31

    move-result-wide v8

    .line 606
    :goto_19
    const-string v1, "event_activities"

    const-string v3, "event_id = ? AND timestamp >= ?"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    aput-object p2, v4, v6

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    const-string v7, "timestamp DESC"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :catch_31
    move-exception v1

    goto :goto_19
.end method

.method public static getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J
    .registers 5
    .parameter "event"

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-nez v0, :cond_17

    :cond_a
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    :goto_16
    return-wide v0

    :cond_17
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_16
.end method

.method public static getEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "eventId"

    .prologue
    .line 298
    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    monitor-enter v6

    .line 299
    :try_start_3
    new-instance v0, Lcom/google/android/apps/plus/api/GetEventOperation;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/GetEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 301
    .local v0, op:Lcom/google/android/apps/plus/api/GetEventOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetEventOperation;->start()V

    .line 303
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetEventOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 304
    const-string v1, "EsEventData"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/GetEventOperation;->logError(Ljava/lang/String;)V

    .line 307
    :cond_1b
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetEventOperation;->hasError()Z

    move-result v1

    if-nez v1, :cond_24

    const/4 v1, 0x1

    :goto_22
    monitor-exit v6
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_26

    return v1

    :cond_24
    const/4 v1, 0x0

    goto :goto_22

    .line 308
    .end local v0           #op:Lcom/google/android/apps/plus/api/GetEventOperation;
    :catchall_26
    move-exception v1

    monitor-exit v6

    throw v1
.end method

.method public static getEventName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "eventId"

    .prologue
    .line 1296
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1299
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT name FROM events WHERE event_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_13} :catch_15

    move-result-object v1

    .line 1301
    :goto_14
    return-object v1

    :catch_15
    move-exception v1

    const/4 v1, 0x0

    goto :goto_14
.end method

.method public static getEventTheme(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "themeId"
    .parameter "projection"

    .prologue
    const/4 v5, 0x0

    .line 2498
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2502
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, -0x1

    if-ne p2, v1, :cond_20

    .line 2503
    const-string v3, "is_default!=0"

    .line 2504
    .local v3, selection:Ljava/lang/String;
    const/4 v4, 0x0

    .line 2510
    .local v4, selectionArgs:[Ljava/lang/String;
    :goto_f
    const-string v1, "event_themes"

    const-string v7, "theme_id"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2513
    .local v8, cursor:Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_2d

    .line 2520
    .end local v8           #cursor:Landroid/database/Cursor;
    :goto_1f
    return-object v8

    .line 2506
    .end local v3           #selection:Ljava/lang/String;
    .end local v4           #selectionArgs:[Ljava/lang/String;
    :cond_20
    const-string v3, "theme_id=?"

    .line 2507
    .restart local v3       #selection:Ljava/lang/String;
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .restart local v4       #selectionArgs:[Ljava/lang/String;
    goto :goto_f

    .line 2517
    .restart local v8       #cursor:Landroid/database/Cursor;
    :cond_2d
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2519
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->ensureFreshEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2520
    const-string v1, "event_themes"

    const-string v7, "theme_id"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_1f
.end method

.method public static getEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "filter"
    .parameter "projection"

    .prologue
    const/4 v4, 0x0

    .line 2479
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->ensureFreshEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2481
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2483
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "event_themes"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, "is_featured="

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p2, :cond_2a

    const/4 v2, 0x1

    :goto_18
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "theme_id"

    move-object v2, p3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :cond_2a
    const/4 v2, 0x0

    goto :goto_18
.end method

.method public static getImageUrl(Lcom/google/api/services/plusi/model/Theme;)Ljava/lang/String;
    .registers 3
    .parameter "theme"

    .prologue
    .line 2463
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;

    move-result-object v0

    .line 2464
    .local v0, image:Lcom/google/api/services/plusi/model/ThemeImage;
    if-eqz v0, :cond_9

    .line 2465
    iget-object v1, v0, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    .line 2467
    :goto_8
    return-object v1

    :cond_9
    const/4 v1, 0x0

    goto :goto_8
.end method

.method public static getInviteeSummary(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Lcom/google/api/services/plusi/model/InviteeSummary;
    .registers 7
    .parameter "event"
    .parameter "rsvpType"

    .prologue
    .line 2781
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-nez v3, :cond_6

    .line 2782
    const/4 v1, 0x0

    .line 2802
    :cond_5
    :goto_5
    return-object v1

    .line 2785
    :cond_6
    const/4 v1, 0x0

    .line 2787
    .local v1, returnSummary:Lcom/google/api/services/plusi/model/InviteeSummary;
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/InviteeSummary;

    .line 2788
    .local v2, summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    if-eqz v3, :cond_d

    .line 2791
    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_32

    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v3, :cond_32

    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_d

    .line 2792
    :cond_32
    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2796
    move-object v1, v2

    .line 2797
    goto :goto_5
.end method

.method public static getMyCurrentEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "now"
    .parameter "projection"

    .prologue
    const/4 v5, 0x0

    .line 641
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 643
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "events"

    const-string v3, "mine = 1 AND ? < end_time AND source = 1"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    const-string v7, "end_time ASC"

    move-object v2, p4

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 648
    .local v8, cursor:Landroid/database/Cursor;
    return-object v8
.end method

.method private static getMyEventIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;
    .registers 11
    .parameter "db"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1840
    const-string v1, "events"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "event_id"

    aput-object v0, v2, v3

    const-string v3, "mine = 1"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1843
    .local v8, cursor:Landroid/database/Cursor;
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1845
    .local v9, eventList:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :goto_1a
    :try_start_1a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 1846
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_28
    .catchall {:try_start_1a .. :try_end_28} :catchall_29

    goto :goto_1a

    .line 1849
    :catchall_29
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1851
    return-object v9
.end method

.method public static getMyPastEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "now"
    .parameter "projection"

    .prologue
    const/4 v5, 0x0

    .line 621
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 623
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "events"

    const-string v3, "mine = 1 AND ? > end_time AND source = 1"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    const-string v7, "end_time DESC"

    move-object v2, p4

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 628
    .local v8, cursor:Landroid/database/Cursor;
    return-object v8
.end method

.method public static getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "eventId"

    .prologue
    const/4 v1, 0x0

    .line 407
    if-eqz p1, :cond_9

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 421
    :cond_9
    :goto_9
    return-object v1

    .line 411
    :cond_a
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    invoke-static {p0, p1, p2, v2}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 413
    .local v0, c:Landroid/database/Cursor;
    if-eqz v0, :cond_2d

    :try_start_12
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 414
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlusEvent;
    :try_end_27
    .catchall {:try_start_12 .. :try_end_27} :catchall_33

    .line 417
    if-eqz v0, :cond_9

    .line 418
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_9

    .line 417
    :cond_2d
    if-eqz v0, :cond_9

    .line 418
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_9

    .line 417
    :catchall_33
    move-exception v1

    if-eqz v0, :cond_39

    .line 418
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_39
    throw v1
.end method

.method public static getRsvpStatus(Lcom/google/api/services/plusi/model/PlusEvent;)I
    .registers 3
    .parameter "event"

    .prologue
    .line 2713
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v0

    .line 2715
    .local v0, rsvpType:Ljava/lang/String;
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 2717
    :cond_14
    const/4 v1, 0x1

    .line 2724
    :goto_15
    return v1

    .line 2718
    :cond_16
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 2719
    const/4 v1, 0x2

    goto :goto_15

    .line 2720
    :cond_20
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_30

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING_AND_REMOVE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 2722
    :cond_30
    const/4 v1, 0x3

    goto :goto_15

    .line 2724
    :cond_32
    const/4 v1, 0x0

    goto :goto_15
.end method

.method public static getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;
    .registers 5
    .parameter "event"

    .prologue
    .line 2660
    if-eqz p0, :cond_59

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v0, :cond_59

    .line 2661
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_32

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_32

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_32

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_32

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING_AND_REMOVE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 2664
    :cond_32
    :goto_32
    return-object v0

    .line 2661
    :cond_33
    const-string v1, "EsEventData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_56

    const-string v1, "EsEventData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[FILTER_RSVP_TYPE]; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not recognized"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_56
    sget-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    goto :goto_32

    .line 2664
    :cond_59
    sget-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    goto :goto_32
.end method

.method public static getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;
    .registers 6
    .parameter "theme"

    .prologue
    .line 2426
    const/4 v1, 0x0

    .line 2428
    .local v1, image:Lcom/google/api/services/plusi/model/ThemeImage;
    if-eqz p0, :cond_3f

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Theme;->image:Ljava/util/List;

    if-eqz v3, :cond_3f

    .line 2429
    iget-object v3, p0, Lcom/google/api/services/plusi/model/Theme;->image:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3f

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ThemeImage;

    .line 2430
    .local v2, themeImage:Lcom/google/api/services/plusi/model/ThemeImage;
    if-eqz v2, :cond_d

    .line 2431
    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->ASPECT_LARGE:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/ThemeImage;->aspectRatio:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 2443
    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->FORMAT_JPG:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/ThemeImage;->format:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2444
    move-object v1, v2

    goto :goto_d

    .line 2446
    :cond_31
    if-nez v1, :cond_d

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->FORMAT_MOV:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/ThemeImage;->format:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 2449
    move-object v1, v2

    goto :goto_d

    .line 2453
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #themeImage:Lcom/google/api/services/plusi/model/ThemeImage;
    :cond_3f
    return-object v1
.end method

.method public static insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;)V
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "plusEvent"
    .parameter "update"

    .prologue
    .line 712
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 714
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 715
    .local v7, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v3, p3, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    .line 717
    .local v3, eventId:Ljava/lang/String;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 719
    :try_start_12
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;)Z

    .line 721
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_23
    .catchall {:try_start_12 .. :try_end_23} :catchall_59

    .line 723
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 727
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_40

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 730
    const/4 v0, 0x1

    invoke-static {p0, v0, p3}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V

    .line 733
    :cond_40
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, i$:Ljava/util/Iterator;
    :goto_44
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5e

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/Uri;

    .line 734
    .local v11, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v11, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_44

    .line 723
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v11           #notifyUri:Landroid/net/Uri;
    :catchall_59
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 736
    .restart local v10       #i$:Ljava/util/Iterator;
    :cond_5e
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 737
    return-void
.end method

.method public static insertEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "pollingToken"
    .parameter
    .parameter "updateOnly"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p4, activities:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/EsEventData$EventActivity;>;"
    const/4 v8, 0x0

    .line 751
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 753
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 755
    .local v5, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 757
    const/4 v0, 0x0

    :try_start_12
    invoke-static {v1, p2, v0}, Lcom/google/android/apps/plus/content/EsEventData;->insertResumeTokenInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    if-eqz p4, :cond_1e

    .line 763
    const/4 v4, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventActivitiesInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;)V

    .line 766
    :cond_1e
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_21
    .catchall {:try_start_12 .. :try_end_21} :catchall_3c

    .line 768
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 771
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 772
    .local v7, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_28

    .line 768
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #notifyUri:Landroid/net/Uri;
    :catchall_3c
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 774
    .restart local v6       #i$:Ljava/util/Iterator;
    :cond_41
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 775
    return-void
.end method

.method private static insertEventActivitiesInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;)V
    .registers 34
    .parameter "context"
    .parameter "db"
    .parameter "eventId"
    .parameter
    .parameter "updateOnly"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;Z",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1590
    .local p3, activities:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/EsEventData$EventActivity;>;"
    .local p5, notificationList:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    const/4 v5, 0x1

    new-array v9, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v9, v5

    .line 1593
    .local v9, eventArgs:[Ljava/lang/String;
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    .line 1595
    .local v24, statusMap:Ljava/util/Map;,"Ljava/util/Map<Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;>;"
    const/16 v25, 0x0

    .line 1597
    .local v25, updatedPhotos:Z
    const-string v6, "event_activities"

    const/4 v5, 0x5

    new-array v7, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "_id"

    aput-object v8, v7, v5

    const/4 v5, 0x1

    const-string v8, "type"

    aput-object v8, v7, v5

    const/4 v5, 0x2

    const-string v8, "owner_gaia_id"

    aput-object v8, v7, v5

    const/4 v5, 0x3

    const-string v8, "timestamp"

    aput-object v8, v7, v5

    const/4 v5, 0x4

    const-string v8, "fingerprint"

    aput-object v8, v7, v5

    const-string v8, "event_id=?"

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 1607
    .local v15, cursor:Landroid/database/Cursor;
    :goto_36
    :try_start_36
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_88

    .line 1608
    new-instance v19, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;-><init>(B)V

    .line 1609
    .local v19, key:Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;
    new-instance v23, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;-><init>(B)V

    .line 1611
    .local v23, status:Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;
    const/4 v5, 0x0

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    iput-object v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->id:Ljava/lang/String;

    .line 1612
    const/4 v5, 0x1

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->type:I

    .line 1613
    const/4 v5, 0x2

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    .line 1614
    const/4 v5, 0x3

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->timestamp:J

    .line 1615
    const/4 v5, 0x4

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, v23

    iput v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->fingerprint:I

    .line 1617
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_82
    .catchall {:try_start_36 .. :try_end_82} :catchall_83

    goto :goto_36

    .line 1620
    .end local v19           #key:Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;
    .end local v23           #status:Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;
    :catchall_83
    move-exception v5

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_88
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1624
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 1625
    .local v27, values:Landroid/content/ContentValues;
    new-instance v19, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;-><init>(B)V

    .line 1626
    .restart local v19       #key:Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 1628
    .local v22, seenAlbum:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, i$:Ljava/util/Iterator;
    :cond_a1
    :goto_a1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1db

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    .line 1629
    .local v13, activity:Lcom/google/android/apps/plus/content/EsEventData$EventActivity;
    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->type:I

    .line 1630
    iget-object v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    .line 1631
    iget-wide v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->timestamp:J

    .line 1633
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;

    .line 1634
    .restart local v23       #status:Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;
    iget-object v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    if-nez v5, :cond_16f

    const/16 v17, 0x0

    .line 1635
    .local v17, fingerprint:I
    :goto_cf
    const/16 v20, 0x0

    .line 1636
    .local v20, photo:Lcom/google/api/services/plusi/model/DataPhoto;
    const/16 v26, 0x0

    .line 1637
    .local v26, url:Ljava/lang/String;
    const/4 v14, 0x0

    .line 1638
    .local v14, comment:Ljava/lang/String;
    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_177

    .line 1639
    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhotoJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhotoJson;

    move-result-object v5

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/api/services/plusi/model/DataPhotoJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    .end local v20           #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    check-cast v20, Lcom/google/api/services/plusi/model/DataPhoto;

    .line 1640
    .restart local v20       #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    if-eqz v20, :cond_f0

    .line 1641
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    move-object/from16 v26, v0

    .line 1650
    :cond_f0
    :goto_f0
    if-nez v23, :cond_18e

    .line 1651
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    .line 1652
    const-string v5, "event_id"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1653
    const-string v5, "type"

    iget v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1654
    const-string v5, "timestamp"

    iget-wide v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1655
    const-string v5, "owner_gaia_id"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656
    const-string v5, "owner_name"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    const-string v5, "data"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1658
    const-string v5, "url"

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    const-string v5, "comment"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660
    const-string v5, "fingerprint"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1661
    const-string v5, "event_activities"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1676
    :goto_158
    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_a1

    .line 1677
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    move-object/from16 v3, v22

    move-object/from16 v4, p5

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertEventPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V

    .line 1679
    const/16 v25, 0x1

    goto/16 :goto_a1

    .line 1634
    .end local v14           #comment:Ljava/lang/String;
    .end local v17           #fingerprint:I
    .end local v20           #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    .end local v26           #url:Ljava/lang/String;
    :cond_16f
    iget-object v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v17

    goto/16 :goto_cf

    .line 1643
    .restart local v14       #comment:Ljava/lang/String;
    .restart local v17       #fingerprint:I
    .restart local v20       #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    .restart local v26       #url:Ljava/lang/String;
    :cond_177
    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_f0

    .line 1644
    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/json/EsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/plus/content/EsEventData$EventComment;

    .line 1645
    .local v16, eventComment:Lcom/google/android/apps/plus/content/EsEventData$EventComment;
    if-eqz v16, :cond_f0

    .line 1646
    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    goto/16 :goto_f0

    .line 1663
    .end local v16           #eventComment:Lcom/google/android/apps/plus/content/EsEventData$EventComment;
    :cond_18e
    move-object/from16 v0, v23

    iget v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->fingerprint:I

    move/from16 v0, v17

    if-eq v0, v5, :cond_1d2

    .line 1664
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    .line 1665
    const-string v5, "data"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1666
    const-string v5, "url"

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667
    const-string v5, "comment"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1668
    const-string v5, "fingerprint"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1669
    const-string v5, "event_activities"

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, v23

    iget-object v10, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->id:Ljava/lang/String;

    aput-object v10, v7, v8

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1673
    :cond_1d2
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_158

    .line 1684
    .end local v13           #activity:Lcom/google/android/apps/plus/content/EsEventData$EventActivity;
    .end local v14           #comment:Ljava/lang/String;
    .end local v17           #fingerprint:I
    .end local v20           #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    .end local v23           #status:Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;
    .end local v26           #url:Ljava/lang/String;
    :cond_1db
    if-nez p4, :cond_205

    .line 1685
    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1e5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_205

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;

    .line 1686
    .restart local v23       #status:Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;
    const-string v5, "event_activities"

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, v23

    iget-object v10, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->id:Ljava/lang/String;

    aput-object v10, v7, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1e5

    .line 1692
    .end local v23           #status:Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;
    :cond_205
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    .line 1693
    const-string v5, "activity_refresh_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1694
    const-string v5, "events"

    const-string v6, "event_id=?"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v5, v1, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1696
    if-eqz v25, :cond_235

    .line 1697
    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v5, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 1699
    .local v21, photoNotifyUri:Landroid/net/Uri;
    if-eqz p5, :cond_236

    .line 1700
    move-object/from16 v0, p5

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1705
    .end local v21           #photoNotifyUri:Landroid/net/Uri;
    :cond_235
    :goto_235
    return-void

    .line 1702
    .restart local v21       #photoNotifyUri:Landroid/net/Uri;
    :cond_236
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_235
.end method

.method public static insertEventHomeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .registers 25
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1768
    .local p2, upcoming:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/PlusEvent;>;"
    .local p3, upcomingDeclined:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/PlusEvent;>;"
    .local p4, past:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/PlusEvent;>;"
    .local p5, resolvedPeople:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1770
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1772
    .local v8, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    .line 1773
    .local v4, userGaiaId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsEventData;->getMyEventIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;

    move-result-object v6

    .line 1774
    .local v6, myEvents:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x5

    new-array v7, v2, [I

    .line 1776
    .local v7, eventStatus:[I
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    move-object/from16 v2, p0

    move-object/from16 v5, p2

    move-object/from16 v9, p5

    .line 1779
    :try_start_21
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;)V

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    move-object/from16 v9, p5

    .line 1782
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;)V

    move-object/from16 v2, p0

    move-object/from16 v5, p4

    move-object/from16 v9, p5

    .line 1785
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;)V

    .line 1788
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 1789
    .local v15, sb:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v6, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    .line 1790
    .local v10, eventIds:[Ljava/lang/String;
    const-string v2, "event_id IN ("

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1791
    const/4 v11, 0x0

    .local v11, i:I
    :goto_4a
    array-length v2, v10

    if-ge v11, v2, :cond_5c

    .line 1792
    if-eqz v11, :cond_54

    .line 1793
    const/16 v2, 0x2c

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1795
    :cond_54
    const/16 v2, 0x3f

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1791
    add-int/lit8 v11, v11, 0x1

    goto :goto_4a

    .line 1797
    :cond_5c
    const/16 v2, 0x29

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1798
    const-string v2, "events"

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v5, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1800
    const/4 v2, 0x3

    array-length v5, v10

    aput v5, v7, v2

    .line 1803
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 1804
    .local v16, values:Landroid/content/ContentValues;
    const-string v2, "event_list_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1806
    const-string v2, "account_status"

    const/4 v5, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0, v5, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1807
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_8e
    .catchall {:try_start_21 .. :try_end_8e} :catchall_120

    .line 1809
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1812
    const-string v2, "EsEventData"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_eb

    .line 1813
    const-string v2, "EsEventData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "[INSERT_EVENT_LIST]; "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " inserted, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x1

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " changed, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x2

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " not changed, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x3

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " removed, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x4

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " ignored"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1821
    :cond_eb
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v14

    .line 1823
    .local v14, plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v2

    if-eqz v2, :cond_107

    .line 1824
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v2, v14}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V

    .line 1827
    :cond_107
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, i$:Ljava/util/Iterator;
    :goto_10b
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_125

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    .line 1828
    .local v13, notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v13, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_10b

    .line 1809
    .end local v10           #eventIds:[Ljava/lang/String;
    .end local v11           #i:I
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v13           #notifyUri:Landroid/net/Uri;
    .end local v14           #plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    .end local v15           #sb:Ljava/lang/StringBuilder;
    .end local v16           #values:Landroid/content/ContentValues;
    :catchall_120
    move-exception v2

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 1830
    .restart local v10       #eventIds:[Ljava/lang/String;
    .restart local v11       #i:I
    .restart local v12       #i$:Ljava/util/Iterator;
    .restart local v14       #plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    .restart local v15       #sb:Ljava/lang/StringBuilder;
    .restart local v16       #values:Landroid/content/ContentValues;
    :cond_125
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v2, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1831
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v2, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1832
    return-void
.end method

.method private static insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;)Z
    .registers 20
    .parameter "context"
    .parameter "userGaiaId"
    .parameter "db"
    .parameter "eventId"
    .parameter "activityId"
    .parameter "plusEvent"
    .parameter "update"
    .parameter
    .parameter "displayTime"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Lcom/google/api/services/plusi/model/Update;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 976
    .local p7, pendingNotification:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    .local p9, resolvedPeople:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction$6b5f16b7(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/Long;Ljava/util/List;I)Z

    move-result v0

    return v0
.end method

.method static insertEventInTransaction$6b5f16b7(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/Long;Ljava/util/List;I)Z
    .registers 36
    .parameter "context"
    .parameter "userGaiaId"
    .parameter "db"
    .parameter "eventId"
    .parameter "activityId"
    .parameter "plusEvent"
    .parameter "update"
    .parameter "displayTime"
    .parameter
    .parameter "source"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Lcom/google/api/services/plusi/model/Update;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 997
    .local p8, resolvedPeople:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 999
    .local v20, now:J
    const/4 v13, 0x0

    .line 1000
    .local v13, dbModified:Z
    const/16 v18, 0x1

    .line 1001
    .local v18, newEvent:Z
    const/16 v22, 0x0

    .line 1002
    .local v22, oldFingerprint:I
    const-string v5, "events"

    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "fingerprint"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "source"

    aput-object v7, v6, v4

    const-string v7, "event_id=?"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1006
    .local v12, cursor:Landroid/database/Cursor;
    :try_start_29
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_47

    .line 1007
    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 1008
    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_38
    .catchall {:try_start_29 .. :try_end_38} :catchall_273

    move-result v17

    .line 1009
    .local v17, existingEventSource:I
    const/16 v18, 0x0

    .line 1013
    const/4 v4, 0x1

    move/from16 v0, v17

    if-ne v0, v4, :cond_47

    if-nez p9, :cond_47

    .line 1015
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    const/4 v4, 0x0

    .line 1091
    .end local v17           #existingEventSource:I
    :goto_46
    return v4

    .line 1015
    :cond_47
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1022
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v16

    .line 1023
    .local v16, eventBytes:[B
    invoke-static/range {v16 .. v16}, Ljava/util/Arrays;->hashCode([B)I

    move-result v19

    .line 1025
    .local v19, newFingerprint:I
    new-instance v25, Landroid/content/ContentValues;

    invoke-direct/range {v25 .. v25}, Landroid/content/ContentValues;-><init>()V

    .line 1026
    .local v25, values:Landroid/content/ContentValues;
    const-string v4, "source"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1028
    if-nez v18, :cond_70

    move/from16 v0, v22

    move/from16 v1, v19

    if-eq v0, v1, :cond_298

    .line 1029
    :cond_70
    const-string v4, "refresh_timestamp"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1030
    const-string v4, "name"

    move-object/from16 v0, p5

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    const-string v4, "event_data"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1032
    const-string v4, "mine"

    move-object/from16 v0, p5

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1033
    const-string v5, "can_invite_people"

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_ca

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v4, :cond_278

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    if-eqz v4, :cond_278

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_278

    :cond_ca
    const/4 v4, 0x1

    :goto_cb
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1038
    const-string v5, "can_post_photos"

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_fc

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v4, :cond_27b

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    if-eqz v4, :cond_27b

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_27b

    :cond_fc
    const/4 v4, 0x1

    :goto_fd
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1043
    const-string v5, "can_comment"

    if-eqz p6, :cond_27e

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->canViewerComment:Ljava/lang/Boolean;

    if-eqz v4, :cond_27e

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->canViewerComment:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_27e

    const/4 v4, 0x1

    :goto_11b
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1048
    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_281

    .line 1049
    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v23

    .line 1054
    .local v23, startTime:J
    :goto_134
    const-string v4, "start_time"

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1056
    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v14

    .line 1057
    .local v14, endTime:J
    const-string v4, "end_time"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1059
    const-string v4, "fingerprint"

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1060
    if-nez v18, :cond_15d

    if-eqz p4, :cond_166

    .line 1061
    :cond_15d
    const-string v4, "activity_id"

    move-object/from16 v0, v25

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    :cond_166
    if-eqz p7, :cond_171

    .line 1065
    const-string v4, "display_time"

    move-object/from16 v0, v25

    move-object/from16 v1, p7

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1068
    :cond_171
    if-eqz v18, :cond_285

    .line 1069
    const-string v4, "event_id"

    move-object/from16 v0, v25

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    const-string v4, "owner_gaia_id"

    move-object/from16 v0, p5

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    const-string v4, "events"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1077
    :goto_191
    const/4 v13, 0x1

    .line 1084
    .end local v14           #endTime:J
    .end local v23           #startTime:J
    :cond_192
    :goto_192
    const-string v4, "EsEventData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_265

    .line 1085
    const-string v4, "EsEventData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[INSERT_EVENT], duration: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v20

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1086
    const/4 v5, 0x3

    const-string v6, "EsEventData"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x0

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "EVENT [id: "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    iget-object v9, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, ", owner: "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    if-nez v4, :cond_2b6

    const-string v4, "N/A"

    :goto_1eb
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "MMM dd, yyyy h:mmaa"

    new-instance v9, Ljava/util/Date;

    move-object/from16 v0, p5

    iget-object v10, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-static {v4, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v9, ", start: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_238

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-eqz v4, :cond_238

    const-string v4, "MMM dd, yyyy h:mmaa"

    new-instance v9, Ljava/util/Date;

    move-object/from16 v0, p5

    iget-object v10, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-static {v4, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v9, ", end: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_238
    const-string v4, ", \n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "      title: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    iget-object v8, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 1089
    :cond_265
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p8

    move-object/from16 v3, p2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->insertReferencedPeopleInTransaction(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    move v4, v13

    .line 1091
    goto/16 :goto_46

    .line 1019
    .end local v16           #eventBytes:[B
    .end local v19           #newFingerprint:I
    .end local v25           #values:Landroid/content/ContentValues;
    :catchall_273
    move-exception v4

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v4

    .line 1033
    .restart local v16       #eventBytes:[B
    .restart local v19       #newFingerprint:I
    .restart local v25       #values:Landroid/content/ContentValues;
    :cond_278
    const/4 v4, 0x0

    goto/16 :goto_cb

    .line 1038
    :cond_27b
    const/4 v4, 0x0

    goto/16 :goto_fd

    .line 1043
    :cond_27e
    const/4 v4, 0x0

    goto/16 :goto_11b

    .line 1051
    :cond_281
    move-wide/from16 v23, v20

    .restart local v23       #startTime:J
    goto/16 :goto_134

    .line 1074
    .restart local v14       #endTime:J
    :cond_285
    const-string v4, "events"

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_191

    .line 1078
    .end local v14           #endTime:J
    .end local v23           #startTime:J
    :cond_298
    if-eqz p4, :cond_192

    .line 1079
    const-string v4, "activity_id"

    move-object/from16 v0, v25

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    const-string v4, "events"

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_192

    .line 1086
    :cond_2b6
    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    goto/16 :goto_1eb
.end method

.method public static insertEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, inviteeList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Invitee;>;"
    const/4 v9, 0x0

    .line 1221
    const-string v6, "EsEventData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_41

    .line 1222
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_41

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Invitee;

    .line 1223
    .local v3, invitee:Lcom/google/api/services/plusi/model/Invitee;
    const-string v6, "EsEventData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[INSERT_EVENT_INVITEE]; "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/google/api/services/plusi/model/InviteeJson;->getInstance()Lcom/google/api/services/plusi/model/InviteeJson;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/google/api/services/plusi/model/InviteeJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_e

    .line 1228
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #invitee:Lcom/google/api/services/plusi/model/Invitee;
    :cond_41
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1231
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x1

    new-array v1, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v1, v6

    .line 1234
    .local v1, eventArgs:[Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1235
    .local v5, values:Landroid/content/ContentValues;
    new-instance v4, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;

    invoke-direct {v4}, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;-><init>()V

    .line 1236
    .local v4, list:Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    iput-object p3, v4, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    .line 1238
    const-string v6, "invitee_roster_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1239
    const-string v6, "invitee_roster"

    sget-object v7, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v7, v4}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1241
    invoke-static {p0, p2, v9, p3, v0}, Lcom/google/android/apps/plus/content/EsEventData;->insertPeopleInInviteeSummaries(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1243
    const-string v6, "events"

    const-string v7, "event_id=?"

    invoke-virtual {v0, v6, v5, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1245
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1246
    return-void
.end method

.method private static insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;)V
    .registers 23
    .parameter "context"
    .parameter "db"
    .parameter "userGaiaId"
    .parameter
    .parameter
    .parameter "eventStatus"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;[I",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1735
    .local p3, eventList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/PlusEvent;>;"
    .local p4, myEvents:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .local p6, notificationList:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    .local p7, resolvedPeople:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    if-nez p3, :cond_3

    .line 1754
    :cond_2
    return-void

    .line 1739
    :cond_3
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, i$:Ljava/util/Iterator;
    :goto_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/PlusEvent;

    .line 1740
    .local v6, event:Lcom/google/api/services/plusi/model/PlusEvent;
    move-object/from16 v0, p2

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/content/EsEventData;->isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 1741
    const/4 v1, 0x4

    aget v2, p5, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, p5, v1

    goto :goto_7

    .line 1745
    :cond_23
    iget-object v1, v6, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v12

    .line 1746
    .local v12, hasEvent:Z
    iget-object v4, v6, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move-object/from16 v8, p6

    move-object/from16 v10, p7

    invoke-static/range {v1 .. v10}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;)Z

    move-result v11

    .line 1749
    .local v11, dbChanged:Z
    if-eqz v11, :cond_4b

    if-eqz v12, :cond_49

    const/4 v14, 0x1

    .line 1752
    .local v14, statusIndex:I
    :goto_42
    aget v1, p5, v14

    add-int/lit8 v1, v1, 0x1

    aput v1, p5, v14

    goto :goto_7

    .line 1749
    .end local v14           #statusIndex:I
    :cond_49
    const/4 v14, 0x0

    goto :goto_42

    :cond_4b
    const/4 v14, 0x2

    goto :goto_42
.end method

.method public static insertEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 26
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Theme;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2337
    .local p2, themes:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Theme;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2339
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2341
    :try_start_b
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    .line 2342
    .local v19, knownThemes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;>;"
    const-string v3, "event_themes"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "theme_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "is_default"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "is_featured"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "image_url"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_31
    .catchall {:try_start_b .. :try_end_31} :catchall_77

    move-result-object v11

    .line 2350
    .local v11, cursor:Landroid/database/Cursor;
    :goto_32
    :try_start_32
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_80

    .line 2351
    new-instance v20, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;-><init>(B)V

    .line 2352
    .local v20, theme:Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 2353
    .local v21, themeId:I
    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_7c

    const/4 v3, 0x1

    :goto_4d
    move-object/from16 v0, v20

    iput-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isDefault:Z

    .line 2354
    const/4 v3, 0x2

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_7e

    const/4 v3, 0x1

    :goto_59
    move-object/from16 v0, v20

    iput-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isFeatured:Z

    .line 2355
    const/4 v3, 0x3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->imageUrl:Ljava/lang/String;

    .line 2356
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_71
    .catchall {:try_start_32 .. :try_end_71} :catchall_72

    goto :goto_32

    .line 2359
    .end local v20           #theme:Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;
    .end local v21           #themeId:I
    :catchall_72
    move-exception v3

    :try_start_73
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_77
    .catchall {:try_start_73 .. :try_end_77} :catchall_77

    .line 2414
    .end local v11           #cursor:Landroid/database/Cursor;
    .end local v19           #knownThemes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;>;"
    :catchall_77
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 2353
    .restart local v11       #cursor:Landroid/database/Cursor;
    .restart local v19       #knownThemes:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;>;"
    .restart local v20       #theme:Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;
    .restart local v21       #themeId:I
    :cond_7c
    const/4 v3, 0x0

    goto :goto_4d

    .line 2354
    :cond_7e
    const/4 v3, 0x0

    goto :goto_59

    .line 2359
    .end local v20           #theme:Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;
    .end local v21           #themeId:I
    :cond_80
    :try_start_80
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2362
    new-instance v12, Ljava/util/HashSet;

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v12, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2363
    .local v12, deletedThemes:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/Integer;>;"
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 2365
    .local v22, values:Landroid/content/ContentValues;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_95
    :goto_95
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_150

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/api/services/plusi/model/Theme;

    .line 2366
    .local v20, theme:Lcom/google/api/services/plusi/model/Theme;
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    if-eqz v3, :cond_95

    .line 2367
    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/content/EsEventData;->getImageUrl(Lcom/google/api/services/plusi/model/Theme;)Ljava/lang/String;

    move-result-object v15

    .line 2371
    .local v15, imageUrl:Ljava/lang/String;
    if-eqz v15, :cond_95

    .line 2372
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    invoke-virtual {v12, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2377
    const/16 v16, 0x0

    .line 2378
    .local v16, isDefault:Z
    const/16 v17, 0x0

    .line 2379
    .local v17, isFeatured:Z
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->category:Ljava/util/List;

    if-eqz v3, :cond_ec

    .line 2380
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->category:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, i$:Ljava/util/Iterator;
    :cond_c6
    :goto_c6
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_ec

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/api/services/plusi/model/EventCategory;

    .line 2381
    .local v10, category:Lcom/google/api/services/plusi/model/EventCategory;
    const-string v3, "FEATURED"

    iget-object v4, v10, Lcom/google/api/services/plusi/model/EventCategory;->category:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_df

    .line 2382
    const/16 v17, 0x1

    goto :goto_c6

    .line 2383
    :cond_df
    const-string v3, "DEFAULT"

    iget-object v4, v10, Lcom/google/api/services/plusi/model/EventCategory;->category:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c6

    .line 2384
    const/16 v16, 0x1

    goto :goto_c6

    .line 2389
    .end local v10           #category:Lcom/google/api/services/plusi/model/EventCategory;
    .end local v14           #i$:Ljava/util/Iterator;
    :cond_ec
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;

    .line 2390
    .local v18, knownTheme:Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;
    if-eqz v18, :cond_114

    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isDefault:Z

    move/from16 v0, v16

    if-ne v3, v0, :cond_114

    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isFeatured:Z

    move/from16 v0, v17

    if-ne v3, v0, :cond_114

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->imageUrl:Ljava/lang/String;

    invoke-static {v3, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_95

    .line 2394
    :cond_114
    const-string v3, "theme_id"

    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2395
    const-string v4, "is_featured"

    if-eqz v17, :cond_14c

    const/4 v3, 0x1

    :goto_124
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2396
    const-string v4, "is_default"

    if-eqz v16, :cond_14e

    const/4 v3, 0x1

    :goto_132
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2397
    const-string v3, "image_url"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2398
    const-string v3, "event_themes"

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_95

    .line 2395
    :cond_14c
    const/4 v3, 0x0

    goto :goto_124

    .line 2396
    :cond_14e
    const/4 v3, 0x0

    goto :goto_132

    .line 2402
    .end local v15           #imageUrl:Ljava/lang/String;
    .end local v16           #isDefault:Z
    .end local v17           #isFeatured:Z
    .end local v18           #knownTheme:Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;
    .end local v20           #theme:Lcom/google/api/services/plusi/model/Theme;
    :cond_150
    invoke-virtual {v12}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, i$:Ljava/util/Iterator;
    :goto_154
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_172

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    .line 2403
    .local v21, themeId:Ljava/lang/Integer;
    const-string v3, "event_themes"

    const-string v4, "theme_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_154

    .line 2407
    .end local v21           #themeId:Ljava/lang/Integer;
    :cond_172
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    .line 2408
    const-string v3, "event_themes_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2410
    const-string v3, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2412
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_190
    .catchall {:try_start_80 .. :try_end_190} :catchall_77

    .line 2414
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2415
    return-void
.end method

.method private static insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 11
    .parameter "context"
    .parameter "eventId"
    .parameter "person"
    .parameter
    .parameter "db"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 894
    .local p3, resolvedPeople:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    const/4 v1, 0x0

    .line 896
    .local v1, inserted:Z
    if-eqz p2, :cond_2f

    if-eqz p3, :cond_2f

    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v3, :cond_2f

    .line 897
    const/4 v0, 0x0

    .local v0, i:I
    :goto_a
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2f

    if-nez v1, :cond_2f

    .line 898
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/EmbedsPerson;

    .line 900
    .local v2, targetPerson:Lcom/google/api/services/plusi/model/EmbedsPerson;
    iget-object v3, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 901
    iget-object v3, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    invoke-static {p1, v3, v4, v5, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 903
    const/4 v1, 0x1

    .line 897
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 909
    .end local v0           #i:I
    .end local v2           #targetPerson:Lcom/google/api/services/plusi/model/EmbedsPerson;
    :cond_2f
    if-nez v1, :cond_44

    if-eqz p2, :cond_44

    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v3, :cond_44

    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    if-eqz v3, :cond_44

    .line 911
    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    iget-object v5, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    invoke-static {p1, v3, v4, v5, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 913
    :cond_44
    return-void
.end method

.method private static insertPeopleInInviteeSummaries(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 8
    .parameter "context"
    .parameter "eventId"
    .parameter
    .parameter
    .parameter "db"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 872
    .local p2, people:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    .local p3, summaries:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Invitee;>;"
    if-eqz p3, :cond_1d

    .line 873
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/Invitee;

    .line 874
    .local v1, invitee:Lcom/google/api/services/plusi/model/Invitee;
    iget-object v2, v1, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {p0, p1, v2, p2, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 875
    iget-object v2, v1, Lcom/google/api/services/plusi/model/Invitee;->inviter:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {p0, p1, v2, p2, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_6

    .line 878
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #invitee:Lcom/google/api/services/plusi/model/Invitee;
    :cond_1d
    return-void
.end method

.method private static insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 10
    .parameter "eventId"
    .parameter "userGaiaId"
    .parameter "userName"
    .parameter "userAvatarPhotoUrl"
    .parameter "db"

    .prologue
    .line 931
    if-eqz p1, :cond_4

    if-nez p3, :cond_5

    .line 958
    :cond_4
    :goto_4
    return-void

    .line 935
    :cond_5
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v0, v3

    const/4 v3, 0x1

    aput-object p1, v0, v3

    .line 937
    .local v0, eventPersonArgs:[Ljava/lang/String;
    const/4 v1, 0x1

    .line 940
    .local v1, newEventPerson:Z
    :try_start_f
    const-string v3, "SELECT event_id FROM event_people WHERE event_id=? AND gaia_id=?"

    invoke-static {p4, v3, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_14
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_f .. :try_end_14} :catch_30

    .line 944
    const/4 v1, 0x0

    .line 949
    :goto_15
    if-eqz v1, :cond_2c

    .line 950
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 951
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "event_id"

    invoke-virtual {v2, v3, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    const-string v3, "gaia_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    const-string v3, "event_people"

    const/4 v4, 0x0

    invoke-virtual {p4, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 957
    .end local v2           #values:Landroid/content/ContentValues;
    :cond_2c
    invoke-static {p4, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_4

    :catch_30
    move-exception v3

    goto :goto_15
.end method

.method private static insertReferencedPeopleInTransaction(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 8
    .parameter "context"
    .parameter "event"
    .parameter
    .parameter "db"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 850
    .local p2, people:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    .line 852
    .local v0, eventId:Ljava/lang/String;
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->creator:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {p0, v0, v3, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 853
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v3, :cond_23

    .line 854
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/InviteeSummary;

    .line 855
    .local v2, summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->invitee:Ljava/util/List;

    invoke-static {p0, v0, p2, v3, p3}, Lcom/google/android/apps/plus/content/EsEventData;->insertPeopleInInviteeSummaries(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_11

    .line 858
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    :cond_23
    return-void
.end method

.method private static insertResumeTokenInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "db"
    .parameter "eventId"
    .parameter "resumeToken"

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 1143
    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v3

    .line 1146
    .local v4, eventArgs:[Ljava/lang/String;
    const/4 v9, 0x1

    .line 1147
    .local v9, newEvent:Z
    const/4 v10, 0x0

    .line 1148
    .local v10, oldResumeToken:Ljava/lang/String;
    const-string v1, "events"

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "resume_token"

    aput-object v0, v2, v3

    const-string v3, "event_id=?"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1152
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1153
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_24
    .catchall {:try_start_1a .. :try_end_24} :catchall_41

    move-result-object v10

    .line 1154
    const/4 v9, 0x0

    .line 1157
    :cond_26
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1160
    invoke-static {v10, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_40

    .line 1161
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 1162
    .local v11, values:Landroid/content/ContentValues;
    const-string v0, "resume_token"

    invoke-virtual {v11, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    if-eqz v9, :cond_46

    .line 1165
    const-string v0, "events"

    invoke-virtual {p0, v0, v5, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1170
    .end local v11           #values:Landroid/content/ContentValues;
    :cond_40
    :goto_40
    return-void

    .line 1157
    :catchall_41
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1167
    .restart local v11       #values:Landroid/content/ContentValues;
    :cond_46
    const-string v0, "events"

    const-string v1, "event_id=?"

    invoke-virtual {p0, v0, v11, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_40
.end method

.method public static isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z
    .registers 6
    .parameter "event"
    .parameter "now"

    .prologue
    .line 1352
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v0

    .line 1353
    .local v0, eventEnd:J
    cmp-long v2, p1, v0

    if-lez v2, :cond_a

    const/4 v2, 0x1

    :goto_9
    return v2

    :cond_a
    const/4 v2, 0x0

    goto :goto_9
.end method

.method public static isEventPostable(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .registers 2
    .parameter "event"

    .prologue
    .line 2892
    if-eqz p0, :cond_26

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    if-nez v0, :cond_24

    :cond_c
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isBroadcastView:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isBroadcastView:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_24

    :cond_18
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_26

    :cond_24
    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method

.method public static isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z
    .registers 16
    .parameter "event"
    .parameter "userGaiaId"
    .parameter "now"

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1318
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v0

    .line 1319
    .local v0, allowsPhotos:Z
    iget-object v8, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v8, :cond_33

    iget-object v8, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    if-eqz v8, :cond_33

    move v5, v6

    .line 1320
    .local v5, hasRsvpType:Z
    :goto_11
    iget-object v8, p0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/32 v10, 0xa4cb80

    sub-long v3, v8, v10

    .line 1321
    .local v3, eventStart:J
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v8

    const-wide/16 v10, 0x1388

    sub-long v1, v8, v10

    .line 1323
    .local v1, eventEnd:J
    if-eqz v0, :cond_35

    if-eqz v5, :cond_35

    cmp-long v8, p2, v3

    if-lez v8, :cond_35

    cmp-long v8, p2, v1

    if-gez v8, :cond_35

    :goto_32
    return v6

    .end local v1           #eventEnd:J
    .end local v3           #eventStart:J
    .end local v5           #hasRsvpType:Z
    :cond_33
    move v5, v7

    .line 1319
    goto :goto_11

    .restart local v1       #eventEnd:J
    .restart local v3       #eventStart:J
    .restart local v5       #hasRsvpType:Z
    :cond_35
    move v6, v7

    .line 1323
    goto :goto_32
.end method

.method private static isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z
    .registers 6
    .parameter "event"
    .parameter "userGaiaId"

    .prologue
    const/4 v2, 0x1

    .line 2566
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2585
    :cond_9
    :goto_9
    return v2

    .line 2576
    :cond_a
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 2578
    :cond_14
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v3, :cond_37

    .line 2579
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_37

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/InviteeSummary;

    .line 2580
    .local v1, summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    iget-object v3, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v3, :cond_1e

    iget-object v3, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1e

    goto :goto_9

    .line 2585
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    :cond_37
    const/4 v2, 0x0

    goto :goto_9
.end method

.method public static isViewerCheckedIn(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .registers 3
    .parameter "event"

    .prologue
    .line 2879
    sget-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static readEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)Z
    .registers 23
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "pollingToken"
    .parameter "resumeToken"
    .parameter "invitationToken"
    .parameter "fetchNewer"
    .parameter "resolveTokens"
    .parameter "syncState"
    .parameter "metrics"

    .prologue
    .line 329
    sget-object v12, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    monitor-enter v12

    .line 332
    if-eqz p7, :cond_30

    .line 333
    :try_start_5
    new-instance v2, Lcom/google/android/apps/plus/api/EventReadOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move/from16 v6, p6

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/EventReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 341
    .local v2, op:Lcom/google/android/apps/plus/api/EventReadOperation;
    :goto_11
    if-eqz p8, :cond_43

    if-eqz p9, :cond_43

    .line 342
    move-object/from16 v0, p8

    move-object/from16 v1, p9

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/api/EventReadOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    .line 347
    :goto_1c
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/EventReadOperation;->hasError()Z

    move-result v3

    if-eqz v3, :cond_27

    .line 348
    const-string v3, "EsEventData"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/api/EventReadOperation;->logError(Ljava/lang/String;)V

    .line 351
    :cond_27
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/EventReadOperation;->hasError()Z

    move-result v3

    if-nez v3, :cond_4a

    const/4 v3, 0x1

    :goto_2e
    monitor-exit v12

    return v3

    .line 336
    .end local v2           #op:Lcom/google/android/apps/plus/api/EventReadOperation;
    :cond_30
    new-instance v2, Lcom/google/android/apps/plus/api/EventReadOperation;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/plus/api/EventReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .restart local v2       #op:Lcom/google/android/apps/plus/api/EventReadOperation;
    goto :goto_11

    .line 344
    :cond_43
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/EventReadOperation;->start()V
    :try_end_46
    .catchall {:try_start_5 .. :try_end_46} :catchall_47

    goto :goto_1c

    .line 352
    .end local v2           #op:Lcom/google/android/apps/plus/api/EventReadOperation;
    :catchall_47
    move-exception v3

    monitor-exit v12

    throw v3

    .line 351
    .restart local v2       #op:Lcom/google/android/apps/plus/api/EventReadOperation;
    :cond_4a
    const/4 v3, 0x0

    goto :goto_2e
.end method

.method public static refreshEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"

    .prologue
    .line 434
    new-instance v0, Lcom/google/android/apps/plus/content/EsEventData$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData$1;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 441
    return-void
.end method

.method public static retrieveEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "projection"

    .prologue
    .line 275
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 276
    .local v0, cursor:Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_b

    .line 285
    .end local v0           #cursor:Landroid/database/Cursor;
    :goto_a
    return-object v0

    .line 280
    .restart local v0       #cursor:Landroid/database/Cursor;
    :cond_b
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 282
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 283
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_a

    .line 285
    :cond_19
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public static rsvpForEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"
    .parameter "rsvpType"

    .prologue
    const/4 v9, 0x0

    .line 367
    sget-object v10, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    monitor-enter v10

    .line 369
    :try_start_4
    invoke-static {p0, p1, p2, p4}, Lcom/google/android/apps/plus/content/EsEventData;->setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 372
    .local v6, rollbackRsvpType:Ljava/lang/String;
    if-nez v6, :cond_d

    .line 373
    monitor-exit v10

    move v1, v9

    .line 378
    :goto_c
    return v1

    .line 375
    :cond_d
    new-instance v0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 377
    .local v0, op:Lcom/google/android/apps/plus/api/SendEventRsvpOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->start()V

    .line 378
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->hasError()Z

    move-result v1

    if-nez v1, :cond_28

    const/4 v1, 0x1

    :goto_23
    monitor-exit v10
    :try_end_24
    .catchall {:try_start_4 .. :try_end_24} :catchall_25

    goto :goto_c

    .line 380
    .end local v0           #op:Lcom/google/android/apps/plus/api/SendEventRsvpOperation;
    .end local v6           #rollbackRsvpType:Ljava/lang/String;
    :catchall_25
    move-exception v1

    monitor-exit v10

    throw v1

    .restart local v0       #op:Lcom/google/android/apps/plus/api/SendEventRsvpOperation;
    .restart local v6       #rollbackRsvpType:Ljava/lang/String;
    :cond_28
    move v1, v9

    .line 378
    goto :goto_23
.end method

.method public static setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 24
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "rsvpType"

    .prologue
    .line 455
    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 457
    .local v10, checkin:Z
    if-eqz v10, :cond_5a

    const-string v14, "UNDO_CHECKIN"

    .line 459
    .local v14, previousRsvpType:Ljava/lang/String;
    :goto_c
    const/4 v12, 0x0

    .line 461
    .local v12, event:Lcom/google/api/services/plusi/model/PlusEvent;
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 463
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "events"

    sget-object v4, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 466
    .local v11, cursor:Landroid/database/Cursor;
    :try_start_28
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 467
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v12

    .end local v12           #event:Lcom/google/api/services/plusi/model/PlusEvent;
    check-cast v12, Lcom/google/api/services/plusi/model/PlusEvent;
    :try_end_3d
    .catchall {:try_start_28 .. :try_end_3d} :catchall_5d

    .line 470
    .restart local v12       #event:Lcom/google/api/services/plusi/model/PlusEvent;
    :cond_3d
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 473
    if-eqz v12, :cond_17e

    .line 475
    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    if-eqz v3, :cond_4e

    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_62

    :cond_4e
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3}, Lcom/google/android/apps/plus/content/EsEventData;->isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_62

    .line 477
    const/4 v3, 0x0

    .line 553
    :goto_59
    return-object v3

    .line 457
    .end local v2           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v11           #cursor:Landroid/database/Cursor;
    .end local v12           #event:Lcom/google/api/services/plusi/model/PlusEvent;
    .end local v14           #previousRsvpType:Ljava/lang/String;
    :cond_5a
    sget-object v14, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    goto :goto_c

    .line 470
    .restart local v2       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11       #cursor:Landroid/database/Cursor;
    .restart local v14       #previousRsvpType:Ljava/lang/String;
    :catchall_5d
    move-exception v3

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v3

    .line 480
    .restart local v12       #event:Lcom/google/api/services/plusi/model/PlusEvent;
    :cond_62
    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v3, :cond_145

    .line 483
    const/16 v18, 0x0

    .line 484
    .local v18, updatedExistingSummary:Z
    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 485
    .local v13, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/google/api/services/plusi/model/InviteeSummary;>;"
    :cond_6e
    :goto_6e
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11e

    .line 486
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/InviteeSummary;

    .line 489
    .local v17, summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    if-eqz v3, :cond_6e

    .line 490
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v3, :cond_c3

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_c3

    const/16 v16, 0x1

    .line 496
    .local v16, setByViewer:Z
    :goto_92
    if-eqz v16, :cond_9a

    if-nez v10, :cond_9a

    .line 497
    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    .line 500
    :cond_9a
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d1

    .line 501
    const/16 v18, 0x1

    .line 502
    if-nez v16, :cond_6e

    .line 503
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    .line 504
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    if-nez v3, :cond_c6

    const/4 v3, 0x1

    :goto_ba
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    goto :goto_6e

    .line 490
    .end local v16           #setByViewer:Z
    :cond_c3
    const/16 v16, 0x0

    goto :goto_92

    .line 504
    .restart local v16       #setByViewer:Z
    :cond_c6
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_ba

    .line 507
    :cond_d1
    if-eqz v16, :cond_6e

    .line 511
    const/4 v15, 0x1

    .line 512
    .local v15, remove:Z
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e8

    .line 513
    const-string v3, "UNDO_CHECKIN"

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    .line 515
    :cond_e8
    if-eqz v15, :cond_6e

    .line 516
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    .line 517
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    if-eqz v3, :cond_6e

    .line 518
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    .line 521
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_6e

    .line 522
    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_6e

    .line 532
    .end local v15           #remove:Z
    .end local v16           #setByViewer:Z
    .end local v17           #summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    :cond_11e
    if-nez v18, :cond_145

    .line 533
    new-instance v17, Lcom/google/api/services/plusi/model/InviteeSummary;

    invoke-direct/range {v17 .. v17}, Lcom/google/api/services/plusi/model/InviteeSummary;-><init>()V

    .line 534
    .restart local v17       #summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    move-object/from16 v0, p3

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    .line 535
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    .line 536
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    .line 537
    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v3, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 542
    .end local v13           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/google/api/services/plusi/model/InviteeSummary;>;"
    .end local v17           #summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    .end local v18           #updatedExistingSummary:Z
    :cond_145
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v12, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->setViewerInfoRsvp(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 544
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 545
    .local v19, values:Landroid/content/ContentValues;
    const-string v3, "event_data"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    invoke-virtual {v4, v12}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 546
    const-string v3, "refresh_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 547
    const-string v3, "events"

    const-string v4, "event_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 551
    .end local v19           #values:Landroid/content/ContentValues;
    :cond_17e
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move-object v3, v14

    .line 553
    goto/16 :goto_59
.end method

.method private static setViewerInfoRsvp(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 5
    .parameter "event"
    .parameter "account"
    .parameter "rsvpType"

    .prologue
    .line 566
    if-eqz p0, :cond_24

    .line 568
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-nez v0, :cond_20

    .line 569
    new-instance v0, Lcom/google/api/services/plusi/model/Invitee;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Invitee;-><init>()V

    iput-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    .line 570
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    new-instance v1, Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/EmbedsPerson;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    .line 571
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    .line 574
    :cond_20
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iput-object p2, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    .line 576
    :cond_24
    return-void
.end method

.method public static syncCurrentEvents$1ef5a3b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 25
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    .line 2826
    sget-object v21, Lcom/google/android/apps/plus/content/EsEventData;->mSyncLock:Ljava/lang/Object;

    monitor-enter v21

    .line 2827
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2828
    monitor-exit v21

    .line 2872
    :goto_a
    return-void

    .line 2831
    :cond_b
    const-string v2, "Current events"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 2833
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 2834
    .local v18, now:J
    const-wide/32 v2, 0x112a880

    sub-long v15, v18, v2

    .line 2835
    .local v15, currentEventsStart:J
    const-wide/32 v2, 0xa4cb80

    add-long v13, v18, v2

    .line 2836
    .local v13, currentEventsEnd:J
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2839
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "events"

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->SYNC_QUERY_PROJECTION:[Ljava/lang/String;

    const-string v4, "end_time > ? AND start_time < ?"

    const/4 v7, 0x2

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    invoke-static {v13, v14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_45
    .catchall {:try_start_3 .. :try_end_45} :catchall_89

    move-result-object v17

    .line 2848
    .local v17, cursor:Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 2850
    .local v12, countSynced:I
    :cond_47
    :goto_47
    :try_start_47
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7f

    .line 2851
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_7f

    .line 2852
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2856
    .local v4, eventId:Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2857
    .local v5, pollingToken:Ljava/lang/String;
    const/4 v2, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2859
    .local v6, resumeToken:Ljava/lang/String;
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    new-instance v11, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v11}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v10, p2

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/plus/content/EsEventData;->readEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)Z
    :try_end_79
    .catchall {:try_start_47 .. :try_end_79} :catchall_8c

    move-result v20

    .line 2863
    .local v20, success:Z
    if-eqz v20, :cond_47

    .line 2864
    add-int/lit8 v12, v12, 0x1

    goto :goto_47

    .line 2868
    .end local v4           #eventId:Ljava/lang/String;
    .end local v5           #pollingToken:Ljava/lang/String;
    .end local v6           #resumeToken:Ljava/lang/String;
    .end local v20           #success:Z
    :cond_7f
    :try_start_7f
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 2871
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    .line 2872
    monitor-exit v21
    :try_end_88
    .catchall {:try_start_7f .. :try_end_88} :catchall_89

    goto :goto_a

    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v12           #countSynced:I
    .end local v13           #currentEventsEnd:J
    .end local v15           #currentEventsStart:J
    .end local v17           #cursor:Landroid/database/Cursor;
    .end local v18           #now:J
    :catchall_89
    move-exception v2

    monitor-exit v21

    throw v2

    .line 2868
    .restart local v1       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v12       #countSynced:I
    .restart local v13       #currentEventsEnd:J
    .restart local v15       #currentEventsStart:J
    .restart local v17       #cursor:Landroid/database/Cursor;
    .restart local v18       #now:J
    :catchall_8c
    move-exception v2

    :try_start_8d
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_91
    .catchall {:try_start_8d .. :try_end_91} :catchall_89
.end method

.method public static timeUntilInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)J
    .registers 10
    .parameter "event"
    .parameter "userGaiaId"
    .parameter "now"

    .prologue
    .line 1362
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1363
    const-wide/16 v2, 0x0

    .line 1370
    :goto_8
    return-wide v2

    .line 1365
    :cond_9
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v0

    .line 1367
    .local v0, eventEnd:J
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    cmp-long v2, p2, v0

    if-lez v2, :cond_1a

    .line 1368
    :cond_17
    const-wide/16 v2, -0x1

    goto :goto_8

    .line 1370
    :cond_1a
    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb80

    sub-long/2addr v2, v4

    sub-long/2addr v2, p2

    goto :goto_8
.end method

.method public static updateEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V
    .registers 34
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "plusEvent"
    .parameter "update"
    .parameter "pollingToken"
    .parameter "resumeToken"
    .parameter
    .parameter "updatesOnly"
    .parameter "displayTime"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Lcom/google/api/services/plusi/model/Update;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;ZJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 795
    .local p7, activities:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/EsEventData$EventActivity;>;"
    .local p11, referencedPeople:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/EmbedsPerson;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 797
    .local v5, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 798
    .local v10, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    .line 800
    .local v6, eventId:Ljava/lang/String;
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 802
    :try_start_14
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    const/4 v12, 0x0

    move-object/from16 v3, p0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    invoke-static/range {v3 .. v12}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;)Z

    .line 806
    if-eqz p11, :cond_4f

    .line 807
    invoke-interface/range {p11 .. p11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, i$:Ljava/util/Iterator;
    :goto_2e
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4f

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/api/services/plusi/model/EmbedsPerson;

    .line 808
    .local v21, person:Lcom/google/api/services/plusi/model/EmbedsPerson;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    invoke-static {v6, v3, v4, v7, v5}, Lcom/google/android/apps/plus/content/EsEventData;->insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_49
    .catchall {:try_start_14 .. :try_end_49} :catchall_4a

    goto :goto_2e

    .line 823
    .end local v19           #i$:Ljava/util/Iterator;
    .end local v21           #person:Lcom/google/api/services/plusi/model/EmbedsPerson;
    :catchall_4a
    move-exception v3

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 813
    :cond_4f
    const/4 v3, 0x1

    :try_start_50
    new-array v15, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v6, v15, v3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v12, "events"

    const/4 v7, 0x1

    new-array v13, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "polling_token"

    aput-object v8, v13, v7

    const-string v14, "event_id=?"

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v11, v5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_6d
    .catchall {:try_start_50 .. :try_end_6d} :catchall_4a

    move-result-object v7

    :try_start_6e
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_7a

    const/4 v3, 0x0

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_78
    .catchall {:try_start_6e .. :try_end_78} :catchall_138

    move-result-object v3

    const/4 v4, 0x0

    :cond_7a
    :try_start_7a
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p5

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_99

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "polling_token"

    move-object/from16 v0, p5

    invoke-virtual {v3, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_13d

    const-string v4, "events"

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 814
    :cond_99
    :goto_99
    move-object/from16 v0, p6

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/plus/content/EsEventData;->insertResumeTokenInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    const/4 v3, 0x1

    new-array v15, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v6, v15, v3

    const/4 v7, 0x1

    const-wide/16 v3, 0x0

    const-string v12, "events"

    const/4 v8, 0x1

    new-array v13, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "display_time"

    aput-object v9, v13, v8

    const-string v14, "event_id=?"

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v11, v5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_bd
    .catchall {:try_start_7a .. :try_end_bd} :catchall_4a

    move-result-object v8

    :try_start_be
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_ca

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_c8
    .catchall {:try_start_be .. :try_end_c8} :catchall_146

    move-result-wide v3

    const/4 v7, 0x0

    :cond_ca
    :try_start_ca
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    cmp-long v3, p9, v3

    if-eqz v3, :cond_e7

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "display_time"

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz v7, :cond_14b

    const-string v4, "events"

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 816
    :cond_e7
    :goto_e7
    if-eqz p7, :cond_f3

    move-object/from16 v4, p0

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object v9, v10

    .line 817
    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventActivitiesInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;)V

    .line 821
    :cond_f3
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_f6
    .catchall {:try_start_ca .. :try_end_f6} :catchall_4a

    .line 823
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 827
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_11d

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v3

    if-eqz v3, :cond_11d

    .line 830
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v3, v1}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V

    .line 833
    :cond_11d
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .restart local v19       #i$:Ljava/util/Iterator;
    :goto_121
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_153

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/net/Uri;

    .line 834
    .local v20, notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_121

    .line 813
    .end local v19           #i$:Ljava/util/Iterator;
    .end local v20           #notifyUri:Landroid/net/Uri;
    :catchall_138
    move-exception v3

    :try_start_139
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_13d
    const-string v4, "events"

    const-string v7, "event_id=?"

    invoke-virtual {v5, v4, v3, v7, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_99

    .line 815
    :catchall_146
    move-exception v3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_14b
    const-string v4, "events"

    const-string v7, "event_id=?"

    invoke-virtual {v5, v4, v3, v7, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_152
    .catchall {:try_start_139 .. :try_end_152} :catchall_4a

    goto :goto_e7

    .line 836
    .restart local v19       #i$:Ljava/util/Iterator;
    :cond_153
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 837
    return-void
.end method

.method public static updateEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .registers 24
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "blacklist"
    .parameter "gaiaId"
    .parameter "email"

    .prologue
    .line 1254
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1257
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    const/16 v17, 0x0

    .line 1258
    .local v17, inviteeList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Invitee;>;"
    const-string v5, "events"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "invitee_roster"

    aput-object v8, v6, v7

    const-string v7, "event_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1262
    .local v14, cursor:Landroid/database/Cursor;
    :try_start_23
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 1263
    const/4 v5, 0x0

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    .line 1264
    .local v13, bytes:[B
    if-eqz v13, :cond_3c

    .line 1265
    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/json/EsJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;

    iget-object v0, v5, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    move-object/from16 v17, v0
    :try_end_3c
    .catchall {:try_start_23 .. :try_end_3c} :catchall_42

    .line 1269
    .end local v13           #bytes:[B
    :cond_3c
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1272
    if-nez v17, :cond_47

    .line 1292
    :cond_41
    :goto_41
    return-void

    .line 1269
    :catchall_42
    move-exception v5

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v5

    .line 1276
    :cond_47
    const/16 v16, 0x0

    .line 1277
    .local v16, invitee:Lcom/google/api/services/plusi/model/Invitee;
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, i$:Ljava/util/Iterator;
    :cond_4d
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_77

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/Invitee;

    .line 1278
    .local v12, anInvitee:Lcom/google/api/services/plusi/model/Invitee;
    iget-object v5, v12, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v5, :cond_4d

    iget-object v5, v12, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4d

    iget-object v5, v12, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EmbedsPerson;->email:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4d

    .line 1281
    move-object/from16 v16, v12

    .line 1286
    .end local v12           #anInvitee:Lcom/google/api/services/plusi/model/Invitee;
    :cond_77
    if-eqz v16, :cond_41

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    if-eqz v5, :cond_8b

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move/from16 v0, p3

    if-eq v5, v0, :cond_41

    .line 1289
    :cond_8b
    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    .line 1290
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_41
.end method

.method public static validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .registers 4
    .parameter "context"
    .parameter "account"

    .prologue
    .line 1482
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/content/EsEventData;->getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v0

    .line 1484
    .local v0, event:Lcom/google/api/services/plusi/model/PlusEvent;
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v1

    return v1
.end method

.method private static validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .registers 21
    .parameter "context"
    .parameter "account"
    .parameter "event"

    .prologue
    .line 1497
    const/4 v3, 0x0

    .line 1499
    .local v3, enableInstantShare:Z
    const-string v14, "EsEventData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_22

    .line 1500
    const-string v14, "EsEventData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "#validateInstantShare; now: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1504
    :cond_22
    if-eqz p1, :cond_112

    if-eqz p2, :cond_112

    .line 1505
    :try_start_26
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v13

    .line 1506
    .local v13, userGaiaId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v10

    .line 1507
    .local v10, isPlusPage:Z
    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    .line 1509
    .local v7, eventId:Ljava/lang/String;
    const-string v14, "alarm"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 1511
    .local v2, alarmManager:Landroid/app/AlarmManager;
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/google/android/apps/plus/phone/Intents;->getEventFinishedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 1513
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 1515
    .local v11, now:J
    const-string v14, "EsEventData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_6d

    .line 1516
    const-string v14, "EsEventData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "#validateInstantShare; cur event: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    :cond_6d
    if-nez v10, :cond_112

    move-object/from16 v0, p2

    invoke-static {v0, v13, v11, v12}, Lcom/google/android/apps/plus/content/EsEventData;->isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z

    move-result v14

    if-eqz v14, :cond_112

    .line 1520
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v7, v14}, Lcom/google/android/apps/plus/phone/Intents;->getViewEventActivityNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v9

    .line 1522
    .local v9, intent:Landroid/app/PendingIntent;
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14, v9, v15}, Lcom/google/android/apps/plus/util/NotificationUtils;->notifyInstantShareEnabled(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V

    .line 1524
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v4

    .line 1525
    .local v4, eventEndAlarmTime:J
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/phone/Intents;->getEventFinishedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    .line 1527
    .local v6, eventEndIntent:Landroid/app/PendingIntent;
    const/4 v14, 0x0

    invoke-virtual {v2, v14, v4, v5, v6}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1528
    const/4 v3, 0x1

    .line 1530
    const-string v14, "EsEventData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_cf

    .line 1531
    const-string v14, "EsEventData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "#validateInstantShare; keep IS; now: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", end: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", wake in: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v4, v11

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1535
    :cond_cf
    const-string v14, "EsEventData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_112

    .line 1536
    const-string v8, "MMM dd, yyyy h:mmaa"

    .line 1537
    .local v8, inFormat:Ljava/lang/CharSequence;
    const-string v14, "EsEventData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Enable Instant Share; now: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v16, Ljava/util/Date;

    move-object/from16 v0, v16

    invoke-direct {v0, v11, v12}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v16

    invoke-static {v8, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", alarm: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    new-instance v16, Ljava/util/Date;

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v16

    invoke-static {v8, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_112
    .catchall {:try_start_26 .. :try_end_112} :catchall_12b

    .line 1545
    .end local v2           #alarmManager:Landroid/app/AlarmManager;
    .end local v4           #eventEndAlarmTime:J
    .end local v6           #eventEndIntent:Landroid/app/PendingIntent;
    .end local v7           #eventId:Ljava/lang/String;
    .end local v8           #inFormat:Ljava/lang/CharSequence;
    .end local v9           #intent:Landroid/app/PendingIntent;
    .end local v10           #isPlusPage:Z
    .end local v11           #now:J
    .end local v13           #userGaiaId:Ljava/lang/String;
    :cond_112
    if-nez v3, :cond_12a

    .line 1547
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    .line 1548
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/util/NotificationUtils;->cancelInstantShareEnabled(Landroid/content/Context;)V

    .line 1550
    const-string v14, "EsEventData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_12a

    .line 1551
    const-string v14, "EsEventData"

    const-string v15, "Disable Instant Share"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1555
    :cond_12a
    return v3

    .line 1545
    :catchall_12b
    move-exception v14

    if-nez v3, :cond_145

    .line 1547
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    .line 1548
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/util/NotificationUtils;->cancelInstantShareEnabled(Landroid/content/Context;)V

    .line 1550
    const-string v15, "EsEventData"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_145

    .line 1551
    const-string v15, "EsEventData"

    const-string v16, "Disable Instant Share"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_145
    throw v14
.end method
