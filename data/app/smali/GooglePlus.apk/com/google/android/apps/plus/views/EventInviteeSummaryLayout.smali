.class public Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventInviteeSummaryLayout.java"


# static fields
.field private static sFontColor:I

.field private static sFontSize:F

.field private static sGuestsFormat:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sRsvpInvitedFormat:Ljava/lang/String;

.field private static sRsvpInvitedPastFormat:Ljava/lang/String;

.field private static sRsvpMaybeFormat:Ljava/lang/String;

.field private static sRsvpYesFormat:Ljava/lang/String;

.field private static sRsvpYesPastFormat:Ljava/lang/String;


# instance fields
.field private mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

.field private mSize:I

.field private mStatus:Landroid/widget/TextView;

.field private mVisibleSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method private static getGaiaIds(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I
    .registers 9
    .parameter "event"
    .parameter "rsvpType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 107
    .local p2, gaiaIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 108
    .local v0, count:I
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->getInviteeSummary(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Lcom/google/api/services/plusi/model/InviteeSummary;

    move-result-object v3

    .line 110
    .local v3, summary:Lcom/google/api/services/plusi/model/InviteeSummary;
    if-eqz v3, :cond_31

    .line 111
    iget-object v5, v3, Lcom/google/api/services/plusi/model/InviteeSummary;->invitee:Ljava/util/List;

    if-eqz v5, :cond_2b

    .line 112
    iget-object v5, v3, Lcom/google/api/services/plusi/model/InviteeSummary;->invitee:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_11
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Invitee;

    .line 113
    .local v2, invitee:Lcom/google/api/services/plusi/model/Invitee;
    iget-object v5, v2, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v5, :cond_11

    .line 114
    iget-object v5, v2, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v4, v5, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    .line 116
    .local v4, targetId:Ljava/lang/String;
    if-eqz v4, :cond_11

    .line 117
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 123
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #invitee:Lcom/google/api/services/plusi/model/Invitee;
    .end local v4           #targetId:Ljava/lang/String;
    :cond_2b
    iget-object v5, v3, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 126
    :cond_31
    return v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56
    sget-boolean v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sInitialized:Z

    if-nez v0, :cond_54

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 59
    .local v7, resources:Landroid/content/res/Resources;
    const v0, 0x7f080103

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpYesFormat:Ljava/lang/String;

    .line 60
    const v0, 0x7f080104

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpYesPastFormat:Ljava/lang/String;

    .line 61
    const v0, 0x7f080101

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sGuestsFormat:Ljava/lang/String;

    .line 62
    const v0, 0x7f080102

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpMaybeFormat:Ljava/lang/String;

    .line 63
    const v0, 0x7f080105

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpInvitedFormat:Ljava/lang/String;

    .line 64
    const v0, 0x7f080106

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpInvitedPastFormat:Ljava/lang/String;

    .line 66
    const v0, 0x7f0d00ac

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontSize:F

    .line 67
    const v0, 0x7f0a0069

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontColor:I

    .line 68
    sput-boolean v6, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sInitialized:Z

    .line 71
    .end local v7           #resources:Landroid/content/res/Resources;
    :cond_54
    sget v3, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontSize:F

    sget v4, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontColor:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->addView(Landroid/view/View;)V

    .line 75
    new-instance v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->addView(Landroid/view/View;)V

    .line 78
    iput v5, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    .line 79
    iput v5, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    .line 80
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/EventActionListener;Z)V
    .registers 11
    .parameter "event"
    .parameter "listener"
    .parameter "past"

    .prologue
    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v0, gaiaIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getGaiaIds(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    add-int/lit8 v1, v2, 0x0

    .line 136
    .local v1, size:I
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getGaiaIds(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    add-int/2addr v1, v2

    .line 137
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getGaiaIds(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    add-int/2addr v1, v2

    .line 139
    iput v1, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    iget v3, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    invoke-virtual {v2, v0, p2, v3}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->bindIds(Ljava/util/ArrayList;Lcom/google/android/apps/plus/views/EventActionListener;I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    sget-object v2, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sGuestsFormat:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->requestLayout()V

    .line 140
    return-void
.end method

.method public final clear()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 194
    iput v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    .line 195
    iput v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    .line 196
    return-void
.end method

.method protected measureChildren(II)V
    .registers 8
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v4, -0x8000

    const/4 v3, 0x0

    .line 84
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 85
    .local v1, width:I
    const/4 v0, 0x0

    .line 87
    .local v0, currentHeight:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-static {v2, v1, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->measure(Landroid/view/View;IIII)V

    .line 88
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-static {v2, v3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setCorner(Landroid/view/View;II)V

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    .line 91
    iget v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    if-lez v2, :cond_2d

    .line 92
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-static {v2, v1, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->measure(Landroid/view/View;IIII)V

    .line 93
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setCorner(Landroid/view/View;II)V

    .line 94
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->getMeasuredHeight()I

    .line 96
    :cond_2d
    return-void
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 190
    iget v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    return v0
.end method
