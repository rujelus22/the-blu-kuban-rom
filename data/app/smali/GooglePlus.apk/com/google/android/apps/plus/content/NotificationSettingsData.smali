.class public Lcom/google/android/apps/plus/content/NotificationSettingsData;
.super Ljava/lang/Object;
.source "NotificationSettingsData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/NotificationSettingsData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

.field private final mEmailAddress:Ljava/lang/String;

.field private final mMobileNotificationType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/apps/plus/content/NotificationSettingsData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/NotificationSettingsData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mEmailAddress:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mMobileNotificationType:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/NotificationSettingsData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .registers 5
    .parameter "emailAddress"
    .parameter "mobileNotificationType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/NotificationSettingsCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p3, categoryList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/NotificationSettingsCategory;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mEmailAddress:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mMobileNotificationType:Ljava/lang/String;

    .line 33
    if-eqz p3, :cond_17

    .line 34
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    invoke-interface {p3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 39
    :goto_16
    return-void

    .line 37
    :cond_17
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    goto :goto_16
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final getCategoriesCount()I
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    array-length v0, v0

    return v0
.end method

.method public final getCategory(I)Lcom/google/android/apps/plus/content/NotificationSettingsCategory;
    .registers 3
    .parameter "index"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getEmailAddress()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final getMobileNotificationType()Ljava/lang/String;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mMobileNotificationType:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mMobileNotificationType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsData;->mCategories:[Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 52
    return-void
.end method
