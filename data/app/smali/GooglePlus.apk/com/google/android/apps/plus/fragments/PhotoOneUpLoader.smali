.class public final Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "PhotoOneUpLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$LeftoverQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentLoadingQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentCountQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:J

.field private final mPhotoUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "ownerId"
    .parameter "photoId"
    .parameter "photoUrl"

    .prologue
    .line 166
    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    .line 160
    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    .line 168
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 169
    iput-wide p4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    .line 170
    iput-object p6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoUrl:Ljava/lang/String;

    .line 171
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mOwnerId:Ljava/lang/String;

    .line 172
    return-void
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .registers 24

    .prologue
    .line 216
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 217
    .local v1, resolver:Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-eqz v3, :cond_dd

    .line 220
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 223
    .local v2, photoUri:Landroid/net/Uri;
    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 262
    .end local v2           #photoUri:Landroid/net/Uri;
    .local v18, photoCursor:Landroid/database/Cursor;
    :goto_2d
    if-eqz v18, :cond_1b9

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1b9

    .line 263
    const/16 v3, 0xb

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 264
    .local v10, commentCount:I
    const/4 v3, -0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 270
    :goto_43
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    .line 272
    .local v4, commentQueryUri:Landroid/net/Uri;
    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentCountQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 275
    .local v9, c:Landroid/database/Cursor;
    if-eqz v9, :cond_1bc

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1bc

    const/4 v3, 0x2

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_1bc

    .line 276
    move-object v11, v9

    .line 284
    .local v11, commentCountCursor:Landroid/database/Cursor;
    :goto_6f
    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "create_time"

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 286
    .local v12, commentCursor:Landroid/database/Cursor;
    if-eqz v12, :cond_1c4

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 289
    .local v13, commentCursorCount:I
    :goto_80
    if-eq v10, v13, :cond_1c7

    .line 290
    new-instance v14, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentLoadingQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v14, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .local v14, commentLoadCursor:Landroid/database/Cursor;
    move-object v3, v14

    .line 292
    check-cast v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const v5, 0x7ffffffd

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    .line 300
    :goto_a3
    new-instance v16, Landroid/database/MatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$LeftoverQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 301
    .local v16, leftoverCursor:Landroid/database/MatrixCursor;
    invoke-virtual/range {v16 .. v16}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const v5, 0x7ffffffc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 303
    new-instance v19, Landroid/database/MergeCursor;

    const/4 v3, 0x5

    new-array v3, v3, [Landroid/database/Cursor;

    const/4 v5, 0x0

    aput-object v18, v3, v5

    const/4 v5, 0x1

    aput-object v11, v3, v5

    const/4 v5, 0x2

    aput-object v12, v3, v5

    const/4 v5, 0x3

    aput-object v14, v3, v5

    const/4 v5, 0x4

    aput-object v16, v3, v5

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 310
    .local v19, returnCursor:Landroid/database/MergeCursor;
    return-object v19

    .line 225
    .end local v4           #commentQueryUri:Landroid/net/Uri;
    .end local v9           #c:Landroid/database/Cursor;
    .end local v10           #commentCount:I
    .end local v11           #commentCountCursor:Landroid/database/Cursor;
    .end local v12           #commentCursor:Landroid/database/Cursor;
    .end local v13           #commentCursorCount:I
    .end local v14           #commentLoadCursor:Landroid/database/Cursor;
    .end local v16           #leftoverCursor:Landroid/database/MatrixCursor;
    .end local v18           #photoCursor:Landroid/database/Cursor;
    .end local v19           #returnCursor:Landroid/database/MergeCursor;
    :cond_dd
    new-instance v18, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 226
    .restart local v18       #photoCursor:Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoData(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/api/services/plusi/model/DataVideo;

    move-result-object v21

    .line 229
    .local v21, videoData:Lcom/google/api/services/plusi/model/DataVideo;
    if-eqz v21, :cond_1ae

    .line 230
    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v22

    .line 234
    .local v22, videoDataBytes:[B
    :goto_102
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mOwnerId:Ljava/lang/String;

    if-eqz v3, :cond_1b2

    const/4 v15, 0x0

    .line 235
    .local v15, downloadable:Ljava/lang/Integer;
    :goto_109
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mOwnerId:Ljava/lang/String;

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->getUserName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 237
    .local v17, ownerName:Ljava/lang/String;
    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    array-length v3, v3

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v20, v0

    .line 238
    .local v20, row:[Ljava/lang/Object;
    const/4 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v20, v3

    .line 239
    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 240
    const/4 v3, 0x2

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v20, v3

    .line 241
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mOwnerId:Ljava/lang/String;

    aput-object v5, v20, v3

    .line 242
    const/4 v3, 0x4

    aput-object v17, v20, v3

    .line 243
    const/4 v3, 0x5

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v20, v3

    .line 244
    const/4 v3, 0x6

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 245
    const/4 v3, 0x7

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 246
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoUrl:Ljava/lang/String;

    aput-object v5, v20, v3

    .line 247
    const/16 v3, 0x9

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 248
    const/16 v3, 0xa

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 249
    const/16 v3, 0xb

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 250
    const/16 v3, 0xc

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 251
    const/16 v3, 0xd

    aput-object v22, v20, v3

    .line 252
    const/16 v3, 0xe

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 253
    const/16 v3, 0xf

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 254
    const/16 v3, 0x10

    const-string v5, "ORIGINAL"

    aput-object v5, v20, v3

    .line 255
    const/16 v3, 0x11

    aput-object v15, v20, v3

    .line 256
    const/16 v3, 0x12

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 257
    const/16 v3, 0x13

    const/4 v5, 0x0

    aput-object v5, v20, v3

    move-object/from16 v3, v18

    .line 258
    check-cast v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2d

    .line 232
    .end local v15           #downloadable:Ljava/lang/Integer;
    .end local v17           #ownerName:Ljava/lang/String;
    .end local v20           #row:[Ljava/lang/Object;
    .end local v22           #videoDataBytes:[B
    :cond_1ae
    const/16 v22, 0x0

    .restart local v22       #videoDataBytes:[B
    goto/16 :goto_102

    .line 234
    :cond_1b2
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    goto/16 :goto_109

    .line 266
    .end local v21           #videoData:Lcom/google/api/services/plusi/model/DataVideo;
    .end local v22           #videoDataBytes:[B
    :cond_1b9
    const/4 v10, 0x0

    .restart local v10       #commentCount:I
    goto/16 :goto_43

    .line 278
    .restart local v4       #commentQueryUri:Landroid/net/Uri;
    .restart local v9       #c:Landroid/database/Cursor;
    :cond_1bc
    if-eqz v9, :cond_1c1

    .line 279
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 281
    :cond_1c1
    const/4 v11, 0x0

    .restart local v11       #commentCountCursor:Landroid/database/Cursor;
    goto/16 :goto_6f

    .line 286
    .restart local v12       #commentCursor:Landroid/database/Cursor;
    :cond_1c4
    const/4 v13, 0x0

    goto/16 :goto_80

    .line 296
    .restart local v13       #commentCursorCount:I
    :cond_1c7
    const/4 v14, 0x0

    .restart local v14       #commentLoadCursor:Landroid/database/Cursor;
    goto/16 :goto_a3
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final onAbandon()V
    .registers 3

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_14

    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    .line 205
    :cond_14
    return-void
.end method

.method protected final onReset()V
    .registers 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->cancelLoad()Z

    .line 210
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onReset()V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->onAbandon()V

    .line 212
    return-void
.end method

.method protected final onStartLoading()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 176
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onStartLoading()V

    .line 177
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    if-nez v3, :cond_2d

    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 179
    .local v2, resolver:Landroid/content/ContentResolver;
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 181
    .local v0, commentUri:Landroid/net/Uri;
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 183
    .local v1, photoUri:Landroid/net/Uri;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v2, v1, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 184
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v2, v0, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 185
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    .line 187
    .end local v0           #commentUri:Landroid/net/Uri;
    .end local v1           #photoUri:Landroid/net/Uri;
    .end local v2           #resolver:Landroid/content/ContentResolver;
    :cond_2d
    return-void
.end method

.method protected final onStopLoading()V
    .registers 1

    .prologue
    .line 197
    return-void
.end method
