.class final Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "EsSyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsSyncAdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 330
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 331
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    .line 332
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    return-object v0
.end method

.method private isSubscribed(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 12
    .parameter "account"
    .parameter "feed"
    .parameter "service"

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 504
    const/4 v1, 0x5

    new-array v4, v1, [Ljava/lang/String;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    aput-object v1, v4, v7

    aput-object p2, v4, v3

    const/4 v1, 0x2

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v1, 0x3

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v1, 0x4

    aput-object p3, v4, v1

    .line 511
    .local v4, selectionArgs:[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 512
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v1, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "authority = ? AND feed = ? AND _sync_account = ? AND _sync_account_type = ? AND service = ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 514
    .local v6, cursor:Landroid/database/Cursor;
    if-nez v6, :cond_33

    move v1, v7

    .line 521
    :goto_32
    return v1

    .line 519
    :cond_33
    :try_start_33
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_3b

    move-result v1

    .line 521
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_32

    :catchall_3b
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private subscribe(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "account"
    .parameter "feed"
    .parameter "service"

    .prologue
    .line 531
    const-string v0, "EsSyncAdapterService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 532
    const-string v0, "EsSyncAdapterService"

    const-string v1, "  --> Subscribe all feeds"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "feed"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_sync_account"

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_sync_account_type"

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "authority"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "service"

    invoke-virtual {v2, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 537
    return-void
.end method

.method private updateSubscribedFeeds(Landroid/accounts/Account;)V
    .registers 13
    .parameter "account"

    .prologue
    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x3

    .line 470
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v4

    if-eqz v4, :cond_69

    const-string v4, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {p1, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_69

    move v0, v2

    .line 473
    .local v0, isSyncEnabled:Z
    :goto_13
    if-eqz v0, :cond_6b

    .line 474
    const/4 v1, 0x1

    .line 475
    .local v1, needCleanup:Z
    const-string v4, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    const-string v5, "webupdates"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->isSubscribed(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_28

    .line 476
    const-string v4, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    const-string v5, "webupdates"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->subscribe(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const/4 v1, 0x0

    .line 480
    :cond_28
    const-string v4, "com.google.plus.events"

    const-string v5, "events"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->isSubscribed(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3a

    .line 481
    const-string v4, "com.google.plus.events"

    const-string v5, "events"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->subscribe(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const/4 v1, 0x0

    .line 485
    :cond_3a
    if-eqz v1, :cond_68

    .line 486
    const-string v4, "EsSyncAdapterService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4b

    .line 487
    const-string v4, "EsSyncAdapterService"

    const-string v5, "  --> Already subscribed"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_4b
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "com.google.plus.notifications"

    aput-object v5, v4, v3

    const-string v3, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    aput-object v3, v4, v2

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v2, v4, v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "authority = ? AND feed = ? AND _sync_account_type = ?"

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 494
    .end local v1           #needCleanup:Z
    :cond_68
    :goto_68
    return-void

    .end local v0           #isSyncEnabled:Z
    :cond_69
    move v0, v3

    .line 470
    goto :goto_13

    .line 492
    .restart local v0       #isSyncEnabled:Z
    :cond_6b
    const-string v4, "EsSyncAdapterService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_7a

    const-string v4, "EsSyncAdapterService"

    const-string v5, "  --> Unsubscribe all feeds"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "com.google.android.apps.plus.content.EsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_sync_account=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " AND _sync_account_type=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " AND authority=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v8, v8, [Ljava/lang/String;

    iget-object v9, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v9, v8, v3

    iget-object v3, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v3, v8, v2

    aput-object v5, v8, v10

    invoke-virtual {v4, v7, v6, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_68
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .registers 23
    .parameter "account"
    .parameter "extras"
    .parameter "authority"
    .parameter "provider"
    .parameter "syncResult"

    .prologue
    .line 365
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    .line 366
    .local v3, activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v3, :cond_3d

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3d

    const/4 v7, 0x1

    .line 369
    .local v7, isActiveAccount:Z
    :goto_19
    if-eqz p2, :cond_3f

    const-string v10, "initialize"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_3f

    const/4 v8, 0x1

    .line 371
    .local v8, isInitializationRequest:Z
    :goto_27
    if-eqz v8, :cond_43

    .line 372
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v11

    const-string v12, "com.google.android.apps.plus.content.EsProvider"

    if-eqz v7, :cond_41

    const/4 v10, 0x1

    :goto_36
    invoke-static {v11, v12, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 374
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->updateSubscribedFeeds(Landroid/accounts/Account;)V

    .line 451
    :cond_3c
    :goto_3c
    return-void

    .line 366
    .end local v7           #isActiveAccount:Z
    .end local v8           #isInitializationRequest:Z
    :cond_3d
    const/4 v7, 0x0

    goto :goto_19

    .line 369
    .restart local v7       #isActiveAccount:Z
    :cond_3f
    const/4 v8, 0x0

    goto :goto_27

    .line 372
    .restart local v8       #isInitializationRequest:Z
    :cond_41
    const/4 v10, 0x0

    goto :goto_36

    .line 380
    :cond_43
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    const-string v11, "sync"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    const-string v11, "adapters_reset"

    const/4 v13, 0x0

    invoke-interface {v12, v11, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    if-nez v11, :cond_92

    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    const-string v11, "com.google"

    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v13

    if-eqz v13, :cond_84

    array-length v14, v13

    const/4 v10, 0x0

    move v11, v10

    :goto_66
    if-ge v11, v14, :cond_84

    aget-object v15, v13, v11

    const-string v10, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v15, v10}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v10

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v10, v0, :cond_82

    const/4 v10, 0x1

    :goto_77
    if-nez v10, :cond_7e

    iget-object v10, v15, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->deactivateAccount(Ljava/lang/String;)V

    :cond_7e
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_66

    :cond_82
    const/4 v10, 0x0

    goto :goto_77

    :cond_84
    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    const-string v11, "adapters_reset"

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 382
    :cond_92
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->updateSubscribedFeeds(Landroid/accounts/Account;)V

    .line 384
    if-nez v7, :cond_cd

    .line 385
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccountUnsafe(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    .line 386
    .local v2, accountToUpgrade:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v2, :cond_3c

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->isAccountUpgradeRequired(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v10

    if-eqz v10, :cond_3c

    .line 389
    :try_start_ab
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    :try_end_b2
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_b2} :catch_167

    .line 394
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    .line 395
    if-eqz v3, :cond_171

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_171

    const/4 v7, 0x1

    .line 397
    :goto_cb
    if-eqz v7, :cond_3c

    .line 405
    .end local v2           #accountToUpgrade:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_cd
    if-eqz p2, :cond_127

    const-string v10, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_127

    .line 406
    const-string v10, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 407
    .local v5, feed:Ljava/lang/String;
    const-string v10, "EsSyncAdapterService"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_fe

    .line 408
    const-string v10, "EsSyncAdapterService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "  --> Sync specific feed: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :cond_fe
    const-string v10, "sync_from_tickle"

    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 413
    const-string v10, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_174

    .line 415
    const-string v10, "sync_what"

    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 416
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    new-instance v11, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-direct {v11, v12}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v13, 0x0

    invoke-static {v10, v3, v11, v12, v13}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 431
    .end local v5           #feed:Ljava/lang/String;
    :cond_127
    :goto_127
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->getAccountSyncState(Ljava/lang/String;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 433
    :try_start_132
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v11

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {v10, v3, v0, v11, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$100(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/os/Bundle;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/SyncResult;)V

    .line 434
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v10

    if-nez v10, :cond_161

    .line 435
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v10

    invoke-direct {v6, v10}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 436
    .local v6, handler:Landroid/os/Handler;
    move-object v9, v3

    .line 437
    .local v9, theAccount:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v10, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v10, v0, v9, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl$1;-><init>(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;Lcom/google/android/apps/plus/content/EsAccount;Landroid/os/Bundle;)V

    invoke-virtual {v6, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_161
    .catchall {:try_start_132 .. :try_end_161} :catchall_1ac

    .line 450
    .end local v6           #handler:Landroid/os/Handler;
    .end local v9           #theAccount:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_161
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    goto/16 :goto_3c

    .line 390
    .restart local v2       #accountToUpgrade:Lcom/google/android/apps/plus/content/EsAccount;
    :catch_167
    move-exception v4

    .line 391
    .local v4, e:Ljava/lang/Exception;
    const-string v10, "EsSyncAdapterService"

    const-string v11, "Failed to upgrade account"

    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3c

    .line 395
    .end local v4           #e:Ljava/lang/Exception;
    :cond_171
    const/4 v7, 0x0

    goto/16 :goto_cb

    .line 419
    .end local v2           #accountToUpgrade:Lcom/google/android/apps/plus/content/EsAccount;
    .restart local v5       #feed:Ljava/lang/String;
    :cond_174
    const-string v10, "com.google.plus.events"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_196

    .line 421
    const-string v10, "sync_what"

    const/4 v11, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 422
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    new-instance v11, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-direct {v11, v12}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v13, 0x0

    invoke-static {v10, v3, v11, v12, v13}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    goto :goto_127

    .line 426
    :cond_196
    const-string v10, "EsSyncAdapterService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Unexpected feed: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3c

    .line 450
    .end local v5           #feed:Ljava/lang/String;
    :catchall_1ac
    move-exception v10

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    throw v10
.end method

.method public final onSyncCanceled()V
    .registers 2

    .prologue
    .line 459
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 460
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    .line 462
    :cond_d
    return-void
.end method
