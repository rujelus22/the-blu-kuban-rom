.class public final Lcom/google/android/apps/plus/api/DeleteCommentOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "DeleteCommentOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/DeleteCommentRequest;",
        "Lcom/google/api/services/plusi/model/DeleteCommentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCommentId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "commentId"

    .prologue
    .line 37
    const-string v3, "deletecomment"

    invoke-static {}, Lcom/google/api/services/plusi/model/DeleteCommentRequestJson;->getInstance()Lcom/google/api/services/plusi/model/DeleteCommentRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/DeleteCommentResponseJson;->getInstance()Lcom/google/api/services/plusi/model/DeleteCommentResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 44
    iput-object p5, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mCommentId:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mCommentId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mCommentId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/DeleteCommentRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->mCommentId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/DeleteCommentRequest;->commentId:Ljava/lang/String;

    return-void
.end method
