.class public Lcom/google/android/apps/plus/views/SkyjamCardView;
.super Lcom/google/android/apps/plus/views/StreamCardView;
.source "SkyjamCardView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;


# static fields
.field private static sAlbumBorderDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field protected static sEmptyThumbnailBitmap:Landroid/graphics/Bitmap;

.field private static sGoogleMusicBitmap:Landroid/graphics/Bitmap;

.field protected static sListenBuyTextPaint:Landroid/text/TextPaint;

.field protected static sNonTitleTextPaint:Landroid/text/TextPaint;

.field private static sPlayOverlayBitmap:Landroid/graphics/Bitmap;

.field private static sSkyjamCardViewInitialized:Z

.field protected static sSkyjamMediaBorderDimension:I

.field protected static sSkyjamMediaDimension:I

.field protected static sSkyjamPlayButtonDescription:Ljava/lang/CharSequence;

.field protected static sTitleTextPaint:Landroid/text/TextPaint;


# instance fields
.field protected mAlbum:Ljava/lang/String;

.field protected mAlbumLayout:Landroid/text/StaticLayout;

.field protected mArtist:Ljava/lang/String;

.field protected mArtistLayout:Landroid/text/StaticLayout;

.field protected mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

.field protected mListenBuyLayout:Landroid/text/StaticLayout;

.field protected mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

.field protected mThumbnailUrl:Ljava/lang/String;

.field protected mTitle:Ljava/lang/String;

.field protected mTitleLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/SkyjamCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 10
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v6, 0x7f0d0158

    const v5, 0x7f0d0157

    const v4, 0x7f0d0156

    const/4 v3, 0x1

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/StreamCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    sget-boolean v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamCardViewInitialized:Z

    if-nez v1, :cond_c6

    .line 71
    sput-boolean v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamCardViewInitialized:Z

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    .local v0, res:Landroid/content/res/Resources;
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 76
    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 77
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTitleTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00d3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 78
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 80
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTitleTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 83
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 84
    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 85
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00d4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 86
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 88
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 91
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 92
    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sListenBuyTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 93
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sListenBuyTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 94
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sListenBuyTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 96
    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sListenBuyTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 99
    const v1, 0x7f02006b

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sEmptyThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 101
    const v1, 0x7f02006e

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sGoogleMusicBitmap:Landroid/graphics/Bitmap;

    .line 102
    const v1, 0x7f02012e

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sPlayOverlayBitmap:Landroid/graphics/Bitmap;

    .line 103
    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sAlbumBorderDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 106
    const v1, 0x7f0d0159

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaDimension:I

    .line 107
    const v1, 0x7f0d015a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    .line 110
    const v1, 0x7f0803f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamPlayButtonDescription:Ljava/lang/CharSequence;

    .line 113
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_c6
    return-void
.end method


# virtual methods
.method protected final draw(Landroid/graphics/Canvas;IIII)I
    .registers 16
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v9, 0x0

    .line 237
    sget v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sYDoublePadding:I

    add-int/2addr v3, p5

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->getMediaHeightPercentage()F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 238
    .local v2, contentHeight:I
    invoke-static {p1, p4, v2}, Lcom/google/android/apps/plus/views/SkyjamCardView;->drawMediaTopAreaStageWithTiledBackground(Landroid/graphics/Canvas;II)V

    .line 240
    sget v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaDimension:I

    sub-int/2addr v3, v4

    div-int/lit8 v1, v3, 0x2

    .line 243
    .local v1, border:I
    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sAlbumBorderDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    add-int/2addr v4, p2

    sget v5, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    add-int/2addr v5, p3

    invoke-virtual {v3, p2, p3, v4, v5}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 245
    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sAlbumBorderDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 247
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 248
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 249
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_34

    .line 250
    sget-object v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sEmptyThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 253
    :cond_34
    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sDrawRect:Landroid/graphics/Rect;

    add-int v4, p2, v1

    add-int v5, p3, v1

    add-int v6, p2, v1

    sget v7, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaDimension:I

    add-int/2addr v6, v7

    add-int v7, p3, v1

    sget v8, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaDimension:I

    add-int/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 255
    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sDrawRect:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 257
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    if-eqz v3, :cond_57

    .line 258
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/plus/views/ClickableImageButton;->draw(Landroid/graphics/Canvas;)V

    .line 261
    :cond_57
    invoke-virtual {p0, p1, p4, p5}, Lcom/google/android/apps/plus/views/SkyjamCardView;->drawMediaTopAreaShadow(Landroid/graphics/Canvas;II)V

    .line 263
    sget v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v3, v4

    add-int/2addr p2, v3

    .line 264
    sget v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v3, v4

    sub-int/2addr p4, v3

    .line 266
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v3, :cond_82

    .line 267
    int-to-float v3, p2

    int-to-float v4, p3

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 268
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 269
    neg-int v3, p2

    int-to-float v3, v3

    neg-int v4, p3

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 270
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr p3, v3

    .line 273
    :cond_82
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtistLayout:Landroid/text/StaticLayout;

    if-eqz v3, :cond_9e

    .line 274
    int-to-float v3, p2

    int-to-float v4, p3

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 275
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtistLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 276
    neg-int v3, p2

    int-to-float v3, v3

    neg-int v4, p3

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 277
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtistLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr p3, v3

    .line 280
    :cond_9e
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbumLayout:Landroid/text/StaticLayout;

    if-eqz v3, :cond_ba

    .line 281
    int-to-float v3, p2

    int-to-float v4, p3

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 282
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbumLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 283
    neg-int v3, p2

    int-to-float v3, v3

    neg-int v4, p3

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 284
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbumLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr p3, v3

    .line 287
    :cond_ba
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mListenBuyLayout:Landroid/text/StaticLayout;

    if-eqz v3, :cond_d6

    .line 288
    int-to-float v3, p2

    int-to-float v4, p3

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 289
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mListenBuyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 290
    neg-int v3, p2

    int-to-float v3, v3

    neg-int v4, p3

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 291
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mListenBuyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr p3, v3

    .line 294
    :cond_d6
    sub-int v3, v2, p3

    sget-object v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sGoogleMusicBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-lt v3, v4, :cond_ec

    .line 295
    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sGoogleMusicBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, p2

    int-to-float v5, p3

    invoke-virtual {p1, v3, v4, v5, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 296
    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sGoogleMusicBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 299
    :cond_ec
    sget v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v3, v4

    sub-int/2addr p2, v3

    .line 300
    sget v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v3, v4

    add-int/2addr p4, v3

    .line 302
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/SkyjamCardView;->drawPlusOneBar(Landroid/graphics/Canvas;)V

    .line 303
    invoke-virtual {p0, p1, p2, p4, p5}, Lcom/google/android/apps/plus/views/SkyjamCardView;->drawMediaBottomArea$1be95c43(Landroid/graphics/Canvas;III)I

    .line 304
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/SkyjamCardView;->drawWhatsHot(Landroid/graphics/Canvas;)V

    .line 305
    return p5
.end method

.method public final init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V
    .registers 15
    .parameter "cursor"
    .parameter "displaySizeType"
    .parameter "size"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewedListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"

    .prologue
    .line 120
    invoke-super/range {p0 .. p8}, Lcom/google/android/apps/plus/views/StreamCardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 123
    const/4 v5, 0x6

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 124
    .local v3, mediaBytes:[B
    if-eqz v3, :cond_4c

    .line 125
    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v0

    .line 126
    .local v0, dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    const/4 v1, 0x0

    .local v1, i:I
    array-length v2, v0

    .local v2, length:I
    :goto_10
    if-ge v1, v2, :cond_4c

    .line 127
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v4

    .line 128
    .local v4, type:I
    const/16 v5, 0xb

    if-eq v4, v5, :cond_20

    const/16 v5, 0xd

    if-ne v4, v5, :cond_56

    .line 130
    :cond_20
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbMedia;->getSongTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4d

    .line 131
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbMedia;->getSongTitle()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitle:Ljava/lang/String;

    .line 132
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbum()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbum:Ljava/lang/String;

    .line 136
    :goto_3c
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbumArtist()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtist:Ljava/lang/String;

    .line 137
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 142
    .end local v0           #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v4           #type:I
    :cond_4c
    return-void

    .line 134
    .restart local v0       #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .restart local v1       #i:I
    .restart local v2       #length:I
    .restart local v4       #type:I
    :cond_4d
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbum()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitle:Ljava/lang/String;

    goto :goto_3c

    .line 126
    :cond_56
    add-int/lit8 v1, v1, 0x1

    goto :goto_10
.end method

.method protected final layoutElements(IIII)I
    .registers 15
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 163
    sget v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sYDoublePadding:I

    add-int/2addr v0, p4

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->getMediaHeightPercentage()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .line 164
    .local v7, contentHeight:I
    move v9, p2

    .line 166
    .local v9, startY:I
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mThumbnailUrl:Ljava/lang/String;

    if-eqz v0, :cond_25

    .line 167
    new-instance v0, Lcom/google/android/apps/plus/views/MediaImage;

    new-instance v1, Lcom/google/android/apps/plus/content/MediaImageRequest;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mThumbnailUrl:Ljava/lang/String;

    const/4 v3, 0x3

    sget v4, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaDimension:I

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;II)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 172
    :cond_25
    sget v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaDimension:I

    sub-int/2addr v0, v1

    div-int/lit8 v6, v0, 0x2

    .line 174
    .local v6, border:I
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbum:Ljava/lang/String;

    if-eqz v0, :cond_5e

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 176
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardView;->sPlayOverlayBitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    sget-object v5, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamPlayButtonDescription:Ljava/lang/CharSequence;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ClickableImageButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    add-int v1, p1, v6

    add-int v2, p2, v6

    sget v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaDimension:I

    add-int/2addr v2, v3

    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardView;->sPlayOverlayBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ClickableImageButton;->setPosition(II)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 183
    :cond_5e
    sget v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v0, v1

    add-int/2addr p1, v0

    .line 184
    sget v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v0, v1

    sub-int/2addr p3, v0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_97

    .line 187
    sub-int v0, v7, p2

    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int v8, v0, v1

    .line 189
    .local v8, maxLines:I
    if-lez v8, :cond_97

    .line 190
    sget-object v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTitleTextPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitle:Ljava/lang/String;

    invoke-static {v0, v1, p3, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitleLayout:Landroid/text/StaticLayout;

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 196
    .end local v8           #maxLines:I
    :cond_97
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtist:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c4

    .line 197
    sub-int v0, v7, p2

    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int v8, v0, v1

    .line 199
    .restart local v8       #maxLines:I
    if-lez v8, :cond_c4

    .line 200
    sget-object v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtist:Ljava/lang/String;

    invoke-static {v0, v1, p3, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtistLayout:Landroid/text/StaticLayout;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtistLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 206
    .end local v8           #maxLines:I
    :cond_c4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbum:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f1

    .line 207
    sub-int v0, v7, p2

    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int v8, v0, v1

    .line 209
    .restart local v8       #maxLines:I
    if-lez v8, :cond_f1

    .line 210
    sget-object v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sNonTitleTextPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbum:Ljava/lang/String;

    invoke-static {v0, v1, p3, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbumLayout:Landroid/text/StaticLayout;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbumLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 217
    .end local v8           #maxLines:I
    :cond_f1
    sub-int v0, v7, p2

    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sListenBuyTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardView;->sListenBuyTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int v8, v0, v1

    .line 219
    .restart local v8       #maxLines:I
    if-lez v8, :cond_11d

    .line 220
    sget-object v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sListenBuyTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08016f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mListenBuyLayout:Landroid/text/StaticLayout;

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mListenBuyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    .line 226
    :cond_11d
    sget v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v0, v1

    sub-int/2addr p1, v0

    .line 227
    sget v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sSkyjamMediaBorderDimension:I

    sget v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sContentXPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    .line 229
    sget v0, Lcom/google/android/apps/plus/views/SkyjamCardView;->sTopBorderPadding:I

    add-int/2addr v0, v7

    sget v1, Lcom/google/android/apps/plus/views/SkyjamCardView;->sYPadding:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/apps/plus/views/SkyjamCardView;->createPlusOneBar(III)I

    .line 230
    invoke-virtual {p0, p1, v9, p3, p4}, Lcom/google/android/apps/plus/views/SkyjamCardView;->createMediaBottomArea(IIII)I

    .line 232
    return p4
.end method

.method public final onClickableImageButtonClick(Lcom/google/android/apps/plus/views/ClickableImageButton;)V
    .registers 6
    .parameter "button"

    .prologue
    .line 318
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    if-ne p1, v2, :cond_1b

    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 320
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getStreamOneUpActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 322
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "auto_play_music"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 323
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 325
    .end local v0           #context:Landroid/content/Context;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1b
    return-void
.end method

.method public final onMediaImageChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mThumbnailUrl:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SkyjamCardView;->invalidate()V

    .line 314
    :cond_10
    return-void
.end method

.method public onRecycle()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 146
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onRecycle()V

    .line 148
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitle:Ljava/lang/String;

    .line 149
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtist:Ljava/lang/String;

    .line 150
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbum:Ljava/lang/String;

    .line 151
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 153
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mTitleLayout:Landroid/text/StaticLayout;

    .line 154
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mArtistLayout:Landroid/text/StaticLayout;

    .line 155
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAlbumLayout:Landroid/text/StaticLayout;

    .line 156
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mListenBuyLayout:Landroid/text/StaticLayout;

    .line 157
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 158
    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardView;->mAutoPlayButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    .line 159
    return-void
.end method
