.class public final Lcom/google/android/apps/plus/util/PrimitiveUtils;
.super Ljava/lang/Object;
.source "PrimitiveUtils.java"


# direct methods
.method public static safeBoolean(Ljava/lang/Boolean;)Z
    .registers 2
    .parameter "b"

    .prologue
    .line 15
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3
.end method

.method public static safeDouble(Ljava/lang/Double;)D
    .registers 3
    .parameter "d"

    .prologue
    .line 31
    if-nez p0, :cond_5

    const-wide/16 v0, 0x0

    :goto_4
    return-wide v0

    :cond_5
    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_4
.end method

.method public static safeInt(Ljava/lang/Integer;)I
    .registers 2
    .parameter "i"

    .prologue
    .line 19
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3
.end method

.method public static safeLong(Ljava/lang/Long;)J
    .registers 3
    .parameter "l"

    .prologue
    .line 23
    if-nez p0, :cond_5

    const-wide/16 v0, 0x0

    :goto_4
    return-wide v0

    :cond_5
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_4
.end method

.method public static safeLong(Ljava/lang/String;)J
    .registers 3
    .parameter "s"

    .prologue
    .line 27
    if-nez p0, :cond_5

    const-wide/16 v0, 0x0

    :goto_4
    return-wide v0

    :cond_5
    invoke-static {p0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v0

    goto :goto_4
.end method
