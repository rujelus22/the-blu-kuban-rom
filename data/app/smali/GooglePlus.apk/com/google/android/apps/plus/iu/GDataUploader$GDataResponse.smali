.class final Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "GDataUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/GDataUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GDataResponse"
.end annotation


# instance fields
.field errorCode:Ljava/lang/String;

.field fingerprint:Lcom/android/gallery3d/common/Fingerprint;

.field private mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mText:Ljava/lang/StringBuilder;

.field photoId:J

.field photoSize:J

.field photoUrl:Ljava/lang/String;

.field timestamp:J


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 593
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 610
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    .line 614
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2
    .parameter

    .prologue
    .line 593
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public final characters([CII)V
    .registers 5
    .parameter "chars"
    .parameter "start"
    .parameter "length"

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 647
    :cond_9
    return-void
.end method

.method public final endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "uri"
    .parameter "localName"
    .parameter "qName"

    .prologue
    .line 651
    const-string v0, "gphoto:streamId"

    invoke-virtual {v0, p3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1b

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    :cond_1b
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    .line 658
    return-void
.end method

.method public final startDocument()V
    .registers 4

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 619
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "code"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:id"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:size"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:streamId"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:timestamp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 626
    return-void
.end method

.method public final startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .registers 9
    .parameter "uri"
    .parameter "localName"
    .parameter "qName"
    .parameter "attributes"

    .prologue
    const/4 v1, 0x0

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    if-nez v0, :cond_37

    .line 635
    const-string v0, "media:content"

    invoke-virtual {v0, p3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 636
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v2

    move v0, v1

    :goto_1c
    if-ge v0, v2, :cond_34

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_31

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v0

    :goto_2e
    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    .line 642
    :cond_30
    :goto_30
    return-void

    .line 636
    :cond_31
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    :cond_34
    const-string v0, ""

    goto :goto_2e

    .line 640
    :cond_37
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_30
.end method

.method public final validateResult()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;
        }
    .end annotation

    .prologue
    .line 661
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "code"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    .line 663
    :try_start_10
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:id"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoId:J
    :try_end_24
    .catch Ljava/lang/NumberFormatException; {:try_start_10 .. :try_end_24} :catch_5c

    .line 669
    :try_start_24
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:size"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoSize:J
    :try_end_38
    .catch Ljava/lang/NumberFormatException; {:try_start_24 .. :try_end_38} :catch_7a

    .line 675
    :try_start_38
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:timestamp"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->timestamp:J
    :try_end_4c
    .catch Ljava/lang/NumberFormatException; {:try_start_38 .. :try_end_4c} :catch_98

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 683
    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v1, "photo URL missing"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 665
    :catch_5c
    move-exception v0

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error parsing photo ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v3, "gphoto:id"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671
    :catch_7a
    move-exception v0

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error parsing photo size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v3, "gphoto:size"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 678
    :catch_98
    move-exception v0

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error parsing timestamp: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v3, "gphoto:timestamp"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 686
    :cond_b6
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/android/gallery3d/common/Fingerprint;->extractFingerprint(Ljava/util/List;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->fingerprint:Lcom/android/gallery3d/common/Fingerprint;

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->fingerprint:Lcom/android/gallery3d/common/Fingerprint;

    if-nez v0, :cond_d9

    .line 688
    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fingerprint missing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 691
    :cond_d9
    return-void
.end method
