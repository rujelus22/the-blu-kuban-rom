.class final Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "HostedEventListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EventsLoader"
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mMode:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "mode"

    .prologue
    .line 90
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 91
    iput p3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mMode:I

    .line 92
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 93
    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 9

    .prologue
    .line 105
    const/4 v0, 0x1

    .line 107
    .local v0, fallThrough:Z
    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mMode:I

    packed-switch v3, :pswitch_data_44

    .line 132
    const/4 v1, 0x0

    :goto_7
    return-object v1

    .line 109
    :pswitch_8
    const/4 v0, 0x0

    .line 112
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sget-object v7, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$Query;->PROJECTION:[Ljava/lang/String;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/apps/plus/content/EsEventData;->getMyCurrentEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 115
    .local v1, futureCursor:Landroid/database/Cursor;
    if-eqz v0, :cond_23

    if-eqz v1, :cond_27

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_27

    .line 116
    :cond_23
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mMode:I

    goto :goto_7

    .line 121
    .end local v1           #futureCursor:Landroid/database/Cursor;
    :cond_27
    :pswitch_27
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sget-object v7, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$Query;->PROJECTION:[Ljava/lang/String;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/apps/plus/content/EsEventData;->getMyPastEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 124
    .local v2, pastCursor:Landroid/database/Cursor;
    if-eqz v2, :cond_42

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_42

    .line 125
    const/4 v3, 0x1

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mMode:I

    :cond_42
    move-object v1, v2

    .line 128
    goto :goto_7

    .line 107
    :pswitch_data_44
    .packed-switch 0x0
        :pswitch_9
        :pswitch_27
        :pswitch_8
    .end packed-switch
.end method

.method public final getCurrentMode()I
    .registers 2

    .prologue
    .line 100
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;->mMode:I

    return v0
.end method
