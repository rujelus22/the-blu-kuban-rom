.class final Lcom/google/android/apps/plus/phone/HomeActivity$1;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/ShakeDetector$ShakeEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/HomeActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/HomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HomeActivity$1;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onShakeDetected()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 178
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity$1;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 179
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity$1;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 182
    .local v1, context:Landroid/content/Context;
    if-eqz v0, :cond_15

    invoke-static {v1, v0, v4}, Lcom/google/android/apps/plus/service/Hangout;->isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v3

    if-nez v3, :cond_27

    :cond_15
    invoke-static {v1, v0, v4}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-ne v3, v4, :cond_51

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v3

    if-eqz v3, :cond_51

    .line 187
    :cond_27
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "destination"

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 189
    .local v2, hostIntent:Landroid/content/Intent;
    const/high16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    const/high16 v3, 0x1000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 191
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity$1;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 193
    .end local v2           #hostIntent:Landroid/content/Intent;
    :cond_51
    return-void
.end method
