.class public Lcom/google/android/apps/plus/views/ThingActivityPreviewView;
.super Lcom/google/android/apps/plus/views/ActivityPreviewView;
.source "ThingActivityPreviewView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 30
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method


# virtual methods
.method public setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    .registers 14
    .parameter "activity"

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 56
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;->setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ThingActivityPreviewView;->getActivity()Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ThingActivityPreviewView;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ThingActivityPreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300b2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/ThingActivityPreviewView;->addView(Landroid/view/View;)V

    const v0, 0x7f09020e

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EsImageView;

    const v1, 0x7f09020f

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090211

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f090210

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryActivity;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryActivity;->getContent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryActivity;->getFavIconUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryActivity;->getImage()Ljava/lang/String;

    move-result-object v4

    if-eqz v6, :cond_9e

    const-string v9, ""

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9e

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_64
    if-eqz v7, :cond_a2

    const-string v1, ""

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a2

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_74
    if-eqz v8, :cond_a6

    const-string v1, ""

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a6

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    :goto_84
    if-eqz v4, :cond_aa

    const-string v0, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_aa

    invoke-virtual {v3, v10}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    :goto_94
    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ThingActivityPreviewView;->invalidate()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ThingActivityPreviewView;->requestLayout()V

    .line 60
    return-void

    .line 57
    :cond_9e
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_64

    :cond_a2
    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_74

    :cond_a6
    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    goto :goto_84

    :cond_aa
    invoke-virtual {v3, v11}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    goto :goto_94
.end method
