.class public final Lcom/google/android/apps/plus/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_switch_padding:I = 0x7f0d01c4

.field public static final album_bottom_action_bar_divider_height:I = 0x7f0d005a

.field public static final album_bottom_action_bar_divider_width:I = 0x7f0d005b

.field public static final album_bottom_action_bar_height:I = 0x7f0d0059

.field public static final album_comment_count_text_size:I = 0x7f0d004d

.field public static final album_grid_horizontal_spacing:I = 0x7f0d0049

.field public static final album_grid_vertical_spacing:I = 0x7f0d0048

.field public static final album_grid_width:I = 0x7f0d0047

.field public static final album_info_height:I = 0x7f0d004f

.field public static final album_info_inner_padding:I = 0x7f0d0052

.field public static final album_info_left_margin:I = 0x7f0d0051

.field public static final album_info_right_margin:I = 0x7f0d0050

.field public static final album_instructions_height:I = 0x7f0d0057

.field public static final album_instructions_left_padding:I = 0x7f0d0058

.field public static final album_layout_margin:I = 0x7f0d010b

.field public static final album_notification_right_margin:I = 0x7f0d0053

.field public static final album_notification_top_margin:I = 0x7f0d0054

.field public static final album_photo_grid_height:I = 0x7f0d0056

.field public static final album_photo_grid_spacing:I = 0x7f0d004a

.field public static final album_photo_grid_width:I = 0x7f0d0055

.field public static final album_photo_height:I = 0x7f0d004c

.field public static final album_photo_width:I = 0x7f0d004b

.field public static final album_plusone_count_text_size:I = 0x7f0d004e

.field public static final android_stream_item_thumbnail_size:I = 0x7f0d00ca

.field public static final article_icon_margin:I = 0x7f0d0103

.field public static final article_icon_marginRight:I = 0x7f0d0102

.field public static final article_image_marginRight:I = 0x7f0d0105

.field public static final article_image_marginTop:I = 0x7f0d0104

.field public static final article_title_margin:I = 0x7f0d0106

.field public static final audience_autocomplete_dropdown_width:I = 0x7f0d00cb

.field public static final avatar_dimension:I = 0x7f0d0034

.field public static final bar_graph_bar_height:I = 0x7f0d0120

.field public static final bar_graph_bar_spacing:I = 0x7f0d0121

.field public static final bar_graph_label_text_bar_spacing:I = 0x7f0d011f

.field public static final bar_graph_label_text_size:I = 0x7f0d011c

.field public static final bar_graph_total_graph_spacing:I = 0x7f0d011e

.field public static final bar_graph_total_text_size:I = 0x7f0d011b

.field public static final bar_graph_value_text_size:I = 0x7f0d011d

.field public static final below_tooltip_height:I = 0x7f0d01c0

.field public static final below_tooltip_width:I = 0x7f0d01bf

.field public static final card_author_name_text_size:I = 0x7f0d012f

.field public static final card_author_name_y_padding:I = 0x7f0d0130

.field public static final card_auto_text_size:I = 0x7f0d0137

.field public static final card_avatar_size:I = 0x7f0d0138

.field public static final card_border_bottom_padding:I = 0x7f0d012c

.field public static final card_border_left_padding:I = 0x7f0d0129

.field public static final card_border_right_padding:I = 0x7f0d012a

.field public static final card_border_top_padding:I = 0x7f0d012b

.field public static final card_content_x_padding:I = 0x7f0d013a

.field public static final card_content_y_padding:I = 0x7f0d013b

.field public static final card_default_text_size:I = 0x7f0d0131

.field public static final card_hangout_join_button_text_size:I = 0x7f0d015b

.field public static final card_hangout_unsupported_text_size:I = 0x7f0d015c

.field public static final card_links_icon_x_padding:I = 0x7f0d014d

.field public static final card_links_image_dimension:I = 0x7f0d0148

.field public static final card_links_image_max_width_percent:I = 0x7f0d014b

.field public static final card_links_image_stroke_dimension:I = 0x7f0d0149

.field public static final card_links_image_x_padding:I = 0x7f0d014c

.field public static final card_links_title_text_shadow_radius:I = 0x7f0d014f

.field public static final card_links_title_text_shadow_x:I = 0x7f0d0150

.field public static final card_links_title_text_shadow_y:I = 0x7f0d0151

.field public static final card_links_title_text_size:I = 0x7f0d014e

.field public static final card_links_url_text_shadow_radius:I = 0x7f0d0153

.field public static final card_links_url_text_shadow_x:I = 0x7f0d0154

.field public static final card_links_url_text_shadow_y:I = 0x7f0d0155

.field public static final card_links_url_text_size:I = 0x7f0d0152

.field public static final card_links_x_padding:I = 0x7f0d014a

.field public static final card_margin_percentage:I = 0x7f0d019e

.field public static final card_place_review_divider_stroke_width:I = 0x7f0d0160

.field public static final card_place_review_divider_y_padding:I = 0x7f0d015f

.field public static final card_place_review_location_icon_padding:I = 0x7f0d015e

.field public static final card_place_review_post_location_y_padding:I = 0x7f0d015d

.field public static final card_plus_bar_x_padding:I = 0x7f0d0147

.field public static final card_plus_oned_text_shadow_radius:I = 0x7f0d0133

.field public static final card_plus_oned_text_shadow_x:I = 0x7f0d0134

.field public static final card_plus_oned_text_shadow_y:I = 0x7f0d0135

.field public static final card_plus_oned_text_size:I = 0x7f0d0132

.field public static final card_relative_time_text_size:I = 0x7f0d0136

.field public static final card_relative_time_y_offset:I = 0x7f0d0139

.field public static final card_skyjam_listen_buy_text_size:I = 0x7f0d0158

.field public static final card_skyjam_media_border_size:I = 0x7f0d015a

.field public static final card_skyjam_media_size:I = 0x7f0d0159

.field public static final card_skyjam_nontitle_text_size:I = 0x7f0d0157

.field public static final card_skyjam_title_text_size:I = 0x7f0d0156

.field public static final card_tag_background_y_padding:I = 0x7f0d013f

.field public static final card_tag_icon_x_padding:I = 0x7f0d0143

.field public static final card_tag_icon_y_padding_checkin:I = 0x7f0d0144

.field public static final card_tag_icon_y_padding_location:I = 0x7f0d0145

.field public static final card_tag_icon_y_padding_with_photo:I = 0x7f0d0146

.field public static final card_tag_text_shadow_radius:I = 0x7f0d0140

.field public static final card_tag_text_shadow_x:I = 0x7f0d0141

.field public static final card_tag_text_shadow_y:I = 0x7f0d0142

.field public static final card_tag_text_size:I = 0x7f0d013d

.field public static final card_tag_text_x_padding:I = 0x7f0d013e

.field public static final card_tag_y_offset:I = 0x7f0d013c

.field public static final card_x_padding:I = 0x7f0d012d

.field public static final card_y_padding:I = 0x7f0d012e

.field public static final clickable_button_bitmap_text_x_spacing:I = 0x7f0d0021

.field public static final clickable_button_horizontal_spacing:I = 0x7f0d0020

.field public static final comment_author_bitmap_dimension:I = 0x7f0d0011

.field public static final comment_bottom_margin:I = 0x7f0d0026

.field public static final comment_content_font_size:I = 0x7f0d0027

.field public static final comment_font_spacing:I = 0x7f0d002a

.field public static final comment_left_margin:I = 0x7f0d0024

.field public static final comment_right_margin:I = 0x7f0d0025

.field public static final comment_separator_height:I = 0x7f0d0022

.field public static final comment_time_font_size:I = 0x7f0d0028

.field public static final comment_time_y_offset:I = 0x7f0d0029

.field public static final comment_top_margin:I = 0x7f0d0023

.field public static final compose_bar_cumulative_touch_slop:I = 0x7f0d01c3

.field public static final dropdownListPreferredItemHeight:I = 0x7f0d01c1

.field public static final event_activity_comment_count_text_size:I = 0x7f0d00c8

.field public static final event_activity_info_height:I = 0x7f0d00c5

.field public static final event_activity_info_inner_padding:I = 0x7f0d00c7

.field public static final event_activity_info_right_margin:I = 0x7f0d00c6

.field public static final event_activity_plusone_count_text_size:I = 0x7f0d00c9

.field public static final event_card_Details_rsvp_action_button_internal_spacing:I = 0x7f0d00b5

.field public static final event_card_activity_avatar_lineup_margin_bottom:I = 0x7f0d00af

.field public static final event_card_activity_avatar_lineup_margin_left:I = 0x7f0d00ad

.field public static final event_card_activity_avatar_lineup_margin_right:I = 0x7f0d00ae

.field public static final event_card_activity_avatar_magin_top:I = 0x7f0d00bb

.field public static final event_card_activity_avatar_margin_left:I = 0x7f0d00ba

.field public static final event_card_activity_avatar_size:I = 0x7f0d00b9

.field public static final event_card_activity_description_size:I = 0x7f0d00b8

.field public static final event_card_activity_padding_bottom:I = 0x7f0d00c0

.field public static final event_card_activity_padding_left:I = 0x7f0d00be

.field public static final event_card_activity_padding_right:I = 0x7f0d00bd

.field public static final event_card_activity_padding_top:I = 0x7f0d00bf

.field public static final event_card_activity_photo_avatar_margin_bottom:I = 0x7f0d00c4

.field public static final event_card_activity_photo_margin_bottom:I = 0x7f0d00c3

.field public static final event_card_activity_text_margin_left:I = 0x7f0d00bc

.field public static final event_card_activity_text_margin_right:I = 0x7f0d00c1

.field public static final event_card_activity_text_top_avatar_percentage:I = 0x7f0d00c2

.field public static final event_card_activity_time_size:I = 0x7f0d00b7

.field public static final event_card_activity_title_size:I = 0x7f0d00b6

.field public static final event_card_avatar_lineup_item_padding:I = 0x7f0d00b0

.field public static final event_card_avatar_lineup_item_size:I = 0x7f0d00b1

.field public static final event_card_avatar_lineup_overflow_text_size:I = 0x7f0d00b2

.field public static final event_card_details_avatar_percent_overlap:I = 0x7f0d009b

.field public static final event_card_details_avatar_size:I = 0x7f0d009c

.field public static final event_card_details_description_size:I = 0x7f0d00a0

.field public static final event_card_details_going_label_text_size:I = 0x7f0d00b3

.field public static final event_card_details_invited_size:I = 0x7f0d00a4

.field public static final event_card_details_on_air_size:I = 0x7f0d00a5

.field public static final event_card_details_option_description_size:I = 0x7f0d00a9

.field public static final event_card_details_option_min_height:I = 0x7f0d00ab

.field public static final event_card_details_option_min_side_width:I = 0x7f0d00aa

.field public static final event_card_details_option_title_size:I = 0x7f0d00a8

.field public static final event_card_details_padding:I = 0x7f0d009d

.field public static final event_card_details_rsvp_action_button_text_size:I = 0x7f0d00b4

.field public static final event_card_details_rsvp_count_size:I = 0x7f0d00ac

.field public static final event_card_details_rsvp_height:I = 0x7f0d009a

.field public static final event_card_details_secondary_padding:I = 0x7f0d00a2

.field public static final event_card_details_secondary_scroll_padding:I = 0x7f0d00a1

.field public static final event_card_details_see_invitees_height:I = 0x7f0d00a7

.field public static final event_card_details_see_invitees_size:I = 0x7f0d00a6

.field public static final event_card_details_subtitle_size:I = 0x7f0d009f

.field public static final event_card_details_title_size:I = 0x7f0d009e

.field public static final event_card_devails_percent_divider:I = 0x7f0d00a3

.field public static final event_card_divider_stroke_width:I = 0x7f0d0094

.field public static final event_card_info_text_size:I = 0x7f0d0097

.field public static final event_card_name_text_size:I = 0x7f0d0098

.field public static final event_card_padding:I = 0x7f0d0092

.field public static final event_card_ribbon_percent_height_overlap:I = 0x7f0d0095

.field public static final event_card_status_text_size:I = 0x7f0d0096

.field public static final event_card_text_line_spacing:I = 0x7f0d0093

.field public static final event_create_event_text_size:I = 0x7f0d0099

.field public static final expand_button_min_height:I = 0x7f0d0163

.field public static final expand_button_min_width:I = 0x7f0d0162

.field public static final fav_icon_size:I = 0x7f0d0010

.field public static final hangout_avatar_margin:I = 0x7f0d00d5

.field public static final hangout_dropshadow_height:I = 0x7f0d00ef

.field public static final hangout_filmstrip_border_bottom:I = 0x7f0d00e7

.field public static final hangout_filmstrip_border_left:I = 0x7f0d00e8

.field public static final hangout_filmstrip_border_right:I = 0x7f0d00e9

.field public static final hangout_filmstrip_border_top:I = 0x7f0d00e6

.field public static final hangout_filmstrip_gradient_width:I = 0x7f0d00ea

.field public static final hangout_filmstrip_margin:I = 0x7f0d00e5

.field public static final hangout_filmstrip_remote_video_border_bottom:I = 0x7f0d00ed

.field public static final hangout_filmstrip_remote_video_border_left:I = 0x7f0d00ec

.field public static final hangout_filmstrip_remote_video_border_right:I = 0x7f0d00ee

.field public static final hangout_filmstrip_remote_video_border_top:I = 0x7f0d00eb

.field public static final hangout_inset_border_plus_dropshadow_size:I = 0x7f0d00f1

.field public static final hangout_inset_border_size:I = 0x7f0d00f0

.field public static final hangout_invitees_logo_size:I = 0x7f0d00dc

.field public static final hangout_invitees_message_height:I = 0x7f0d00db

.field public static final hangout_invitees_view_height:I = 0x7f0d00da

.field public static final hangout_list_header_height:I = 0x7f0d00e4

.field public static final hangout_message_logo_size:I = 0x7f0d00dd

.field public static final hangout_overlay_menu_height:I = 0x7f0d00d9

.field public static final hangout_ring_toggle_height:I = 0x7f0d00e3

.field public static final hangout_ringing_inviter_avatar_size:I = 0x7f0d00d7

.field public static final hangout_ringing_notification_height:I = 0x7f0d00d8

.field public static final hangout_ringing_title_height:I = 0x7f0d00d6

.field public static final hangout_self_video_container_height:I = 0x7f0d00df

.field public static final hangout_self_video_container_margin:I = 0x7f0d00e0

.field public static final hangout_self_video_container_width:I = 0x7f0d00de

.field public static final hangout_toast_avatar_size:I = 0x7f0d00e2

.field public static final host_action_bar_height:I = 0x7f0d0128

.field public static final host_max_navigation_bar_width:I = 0x7f0d0126

.field public static final host_min_navigation_bar_width:I = 0x7f0d0125

.field public static final host_shadow_width:I = 0x7f0d0124

.field public static final host_spinner_margin_left:I = 0x7f0d0127

.field public static final icon_right_margin:I = 0x7f0d000f

.field public static final label_preference_margin_left:I = 0x7f0d0122

.field public static final large_avatar_dimension:I = 0x7f0d0039

.field public static final max_list_dialog_height:I = 0x7f0d01a3

.field public static final media_card_big_height_percentage:I = 0x7f0d01a2

.field public static final media_card_height_percentage:I = 0x7f0d01a1

.field public static final media_max_portrait_aspect_ratio:I = 0x7f0d01a0

.field public static final media_min_landscape_aspect_ratio:I = 0x7f0d019f

.field public static final medium_avatar_dimension:I = 0x7f0d0035

.field public static final medium_avatar_name_height:I = 0x7f0d0036

.field public static final medium_avatar_selected_dimension:I = 0x7f0d0038

.field public static final medium_avatar_selected_padding:I = 0x7f0d0037

.field public static final micro_kind_max_dimension:I = 0x7f0d0006

.field public static final mini_kind_max_dimension:I = 0x7f0d0007

.field public static final notification_bigpicture_height:I = 0x7f0d0031

.field public static final notification_bigpicture_width:I = 0x7f0d0030

.field public static final notification_large_icon_size:I = 0x7f0d0032

.field public static final oob_items_padding:I = 0x7f0d0003

.field public static final oob_radio_button_margin:I = 0x7f0d0005

.field public static final oob_small_padding:I = 0x7f0d0004

.field public static final participant_tray_avatar_and_padding_dimension:I = 0x7f0d00cd

.field public static final participant_tray_avatar_height:I = 0x7f0d00ce

.field public static final participant_tray_avatar_padding_dimension:I = 0x7f0d00cc

.field public static final person_card_add_button_height:I = 0x7f0d01b3

.field public static final person_card_add_button_long_separator_size:I = 0x7f0d01b9

.field public static final person_card_add_button_min_width:I = 0x7f0d01b4

.field public static final person_card_add_button_size:I = 0x7f0d01b8

.field public static final person_card_avatar_box_height:I = 0x7f0d01ac

.field public static final person_card_avatar_size:I = 0x7f0d01ab

.field public static final person_card_circle_icon_padding_top:I = 0x7f0d01b0

.field public static final person_card_circle_text_size:I = 0x7f0d01af

.field public static final person_card_circles_button_padding:I = 0x7f0d01b7

.field public static final person_card_dismiss_button_margin:I = 0x7f0d01b6

.field public static final person_card_dismiss_button_size:I = 0x7f0d01b5

.field public static final person_card_email_icon_padding_right:I = 0x7f0d01be

.field public static final person_card_email_icon_padding_top:I = 0x7f0d01bd

.field public static final person_card_gap_between_avatar_and_text:I = 0x7f0d01ad

.field public static final person_card_gap_between_icon_and_circles:I = 0x7f0d01b1

.field public static final person_card_gap_between_name_and_circles:I = 0x7f0d01b2

.field public static final person_card_min_height:I = 0x7f0d01a6

.field public static final person_card_min_width:I = 0x7f0d01a5

.field public static final person_card_name_margin_top:I = 0x7f0d01ba

.field public static final person_card_name_text_size:I = 0x7f0d01ae

.field public static final person_card_next_card_peek_width:I = 0x7f0d01bb

.field public static final person_card_padding:I = 0x7f0d01aa

.field public static final person_card_padding_left:I = 0x7f0d01a8

.field public static final person_card_padding_top:I = 0x7f0d01a9

.field public static final person_card_preferred_width:I = 0x7f0d01a4

.field public static final person_card_tall_height:I = 0x7f0d01a7

.field public static final person_card_wide_left_margin:I = 0x7f0d01bc

.field public static final photo_caption_left_padding:I = 0x7f0d006f

.field public static final photo_caption_right_padding:I = 0x7f0d0070

.field public static final photo_caption_text_size:I = 0x7f0d0071

.field public static final photo_crop_stroke_width:I = 0x7f0d005d

.field public static final photo_crop_width:I = 0x7f0d005c

.field public static final photo_home_album_count_left_padding:I = 0x7f0d0043

.field public static final photo_home_album_count_right_padding:I = 0x7f0d0044

.field public static final photo_home_album_count_text_size:I = 0x7f0d003f

.field public static final photo_home_album_name_left_padding:I = 0x7f0d0041

.field public static final photo_home_album_name_right_padding:I = 0x7f0d0042

.field public static final photo_home_album_name_text_size:I = 0x7f0d003e

.field public static final photo_home_info_height:I = 0x7f0d0040

.field public static final photo_home_item_divider_height:I = 0x7f0d003d

.field public static final photo_home_item_height:I = 0x7f0d003b

.field public static final photo_home_item_inner_padding:I = 0x7f0d003c

.field public static final photo_home_notification_right_margin:I = 0x7f0d0046

.field public static final photo_home_notification_top_margin:I = 0x7f0d0045

.field public static final photo_image_margin:I = 0x7f0d0109

.field public static final photo_info_album_right_margin:I = 0x7f0d0061

.field public static final photo_info_album_text_size:I = 0x7f0d006a

.field public static final photo_info_avatar_height:I = 0x7f0d0062

.field public static final photo_info_avatar_right_margin:I = 0x7f0d0063

.field public static final photo_info_comment_count_left_margin:I = 0x7f0d0064

.field public static final photo_info_comment_count_text_width:I = 0x7f0d0065

.field public static final photo_info_comment_text_size:I = 0x7f0d006d

.field public static final photo_info_date_right_margin:I = 0x7f0d0066

.field public static final photo_info_date_text_size:I = 0x7f0d006c

.field public static final photo_info_name_text_size:I = 0x7f0d006b

.field public static final photo_info_plusone_bottom_margin:I = 0x7f0d0067

.field public static final photo_info_plusone_count_left_margin:I = 0x7f0d0068

.field public static final photo_info_plusone_count_text_width:I = 0x7f0d0069

.field public static final photo_info_plusone_text_size:I = 0x7f0d006e

.field public static final photo_info_right_padding:I = 0x7f0d0060

.field public static final photo_one_up_caption_margin_top:I = 0x7f0d019b

.field public static final photo_overlay_bottom_padding:I = 0x7f0d005f

.field public static final photo_overlay_right_padding:I = 0x7f0d005e

.field public static final photo_plusone_drawable_padding:I = 0x7f0d0074

.field public static final photo_plusone_height:I = 0x7f0d0075

.field public static final photo_plusone_left_padding:I = 0x7f0d0073

.field public static final photo_plusone_text_size:I = 0x7f0d0072

.field public static final photo_tag_scroller_avatar_height:I = 0x7f0d0080

.field public static final photo_tag_scroller_avatar_margin_left:I = 0x7f0d0086

.field public static final photo_tag_scroller_avatar_margin_right:I = 0x7f0d0087

.field public static final photo_tag_scroller_avatar_padding_bottom:I = 0x7f0d008b

.field public static final photo_tag_scroller_avatar_padding_left:I = 0x7f0d008a

.field public static final photo_tag_scroller_avatar_padding_right:I = 0x7f0d0089

.field public static final photo_tag_scroller_avatar_padding_top:I = 0x7f0d0088

.field public static final photo_tag_scroller_avatar_width:I = 0x7f0d0081

.field public static final photo_tag_scroller_button_height:I = 0x7f0d0090

.field public static final photo_tag_scroller_button_margin:I = 0x7f0d008e

.field public static final photo_tag_scroller_button_width:I = 0x7f0d008f

.field public static final photo_tag_scroller_height:I = 0x7f0d007f

.field public static final photo_tag_scroller_name_text_size:I = 0x7f0d0077

.field public static final photo_tag_scroller_padding_bottom:I = 0x7f0d0084

.field public static final photo_tag_scroller_padding_left:I = 0x7f0d0085

.field public static final photo_tag_scroller_padding_right:I = 0x7f0d0083

.field public static final photo_tag_scroller_padding_top:I = 0x7f0d0082

.field public static final photo_tag_scroller_secondary_text_size:I = 0x7f0d0078

.field public static final photo_tag_scroller_text_padding_left:I = 0x7f0d008d

.field public static final photo_tag_scroller_text_padding_right:I = 0x7f0d008c

.field public static final photo_tag_shadow_radius:I = 0x7f0d007a

.field public static final photo_tag_stroke_width:I = 0x7f0d007b

.field public static final photo_tag_text_padding:I = 0x7f0d0079

.field public static final photo_tag_text_size:I = 0x7f0d0076

.field public static final photo_tag_title_height:I = 0x7f0d007d

.field public static final photo_tag_title_notification_padding:I = 0x7f0d007e

.field public static final photo_tag_title_text_size:I = 0x7f0d007c

.field public static final photo_title_margin:I = 0x7f0d010a

.field public static final places_map_max_width:I = 0x7f0d003a

.field public static final plus_mention_suggestion_min_space:I = 0x7f0d019d

.field public static final plus_mention_suggestion_popup_offset:I = 0x7f0d019c

.field public static final plus_one_app_name_text_size:I = 0x7f0d0111

.field public static final plus_one_avatar_margin:I = 0x7f0d010c

.field public static final plus_one_confirm_splitter_height:I = 0x7f0d0119

.field public static final plus_one_confirm_splitter_width:I = 0x7f0d0118

.field public static final plus_one_preview_margin:I = 0x7f0d0117

.field public static final plus_one_share_margin:I = 0x7f0d0116

.field public static final plus_one_title_margin:I = 0x7f0d0115

.field public static final plus_one_title_padding_left:I = 0x7f0d011a

.field public static final plus_one_ui_margin:I = 0x7f0d0112

.field public static final plus_one_ui_padding:I = 0x7f0d0114

.field public static final plus_one_user_margin:I = 0x7f0d0113

.field public static final plus_one_user_name_margin:I = 0x7f0d010d

.field public static final plus_one_user_name_marginLeft:I = 0x7f0d010e

.field public static final plus_one_user_name_text_size:I = 0x7f0d0110

.field public static final plusone_icon_marginRight:I = 0x7f0d010f

.field public static final quick_actions_item_height:I = 0x7f0d00d4

.field public static final realtimechat_image_height:I = 0x7f0d00d3

.field public static final rsvp_spinner_icon_spacing:I = 0x7f0d0091

.field public static final sdk_activity_padding:I = 0x7f0d00f3

.field public static final sdk_activity_size:I = 0x7f0d00f2

.field public static final share_app_icon_marginRight:I = 0x7f0d00fa

.field public static final share_app_margin:I = 0x7f0d00ff

.field public static final share_app_name_text_size:I = 0x7f0d00f9

.field public static final share_avatar_dimension:I = 0x7f0d00f5

.field public static final share_avatar_marginRight:I = 0x7f0d00f6

.field public static final share_compose_margin:I = 0x7f0d00fd

.field public static final share_footer_margin:I = 0x7f0d0101

.field public static final share_footer_text_size:I = 0x7f0d0100

.field public static final share_post_margin:I = 0x7f0d00fb

.field public static final share_preview_margin:I = 0x7f0d00fe

.field public static final share_title_padding_left:I = 0x7f0d00f4

.field public static final share_ui_margin:I = 0x7f0d00f8

.field public static final share_user_margin:I = 0x7f0d00fc

.field public static final share_user_name_text_size:I = 0x7f0d00f7

.field public static final spinner_padding_left:I = 0x7f0d01c2

.field public static final stream_content_text_size:I = 0x7f0d001b

.field public static final stream_feedback_text_size:I = 0x7f0d001c

.field public static final stream_footer_plus_one_count_font_size:I = 0x7f0d000e

.field public static final stream_footer_plus_one_icon_y_offset:I = 0x7f0d000c

.field public static final stream_footer_plus_one_selection_padding:I = 0x7f0d000d

.field public static final stream_image_dimension:I = 0x7f0d0012

.field public static final stream_image_right_margin:I = 0x7f0d0014

.field public static final stream_image_small_dimension:I = 0x7f0d0013

.field public static final stream_image_top_margin:I = 0x7f0d0015

.field public static final stream_media_max_fetch_size:I = 0x7f0d000b

.field public static final stream_name_text_size:I = 0x7f0d0019

.field public static final stream_one_up_avatar_margin_left:I = 0x7f0d016b

.field public static final stream_one_up_avatar_margin_right:I = 0x7f0d016c

.field public static final stream_one_up_avatar_margin_top:I = 0x7f0d016a

.field public static final stream_one_up_avatar_size:I = 0x7f0d0179

.field public static final stream_one_up_comment_avatar_margin_right:I = 0x7f0d0181

.field public static final stream_one_up_comment_avatar_margin_top:I = 0x7f0d0180

.field public static final stream_one_up_comment_content_text_size:I = 0x7f0d0194

.field public static final stream_one_up_comment_count_divider_width:I = 0x7f0d017b

.field public static final stream_one_up_comment_count_margin_left:I = 0x7f0d017a

.field public static final stream_one_up_comment_count_text_size:I = 0x7f0d0191

.field public static final stream_one_up_comment_date_text_size:I = 0x7f0d0193

.field public static final stream_one_up_comment_divider_thickness:I = 0x7f0d0183

.field public static final stream_one_up_comment_margin_bottom:I = 0x7f0d017f

.field public static final stream_one_up_comment_margin_left:I = 0x7f0d017d

.field public static final stream_one_up_comment_margin_right:I = 0x7f0d017e

.field public static final stream_one_up_comment_margin_top:I = 0x7f0d017c

.field public static final stream_one_up_comment_name_margin_right:I = 0x7f0d0182

.field public static final stream_one_up_comment_name_text_size:I = 0x7f0d0192

.field public static final stream_one_up_comment_plus_one_text_size:I = 0x7f0d0195

.field public static final stream_one_up_content_text_size:I = 0x7f0d018b

.field public static final stream_one_up_date_text_size:I = 0x7f0d018a

.field public static final stream_one_up_font_spacing:I = 0x7f0d0161

.field public static final stream_one_up_linked_body_text_size:I = 0x7f0d018d

.field public static final stream_one_up_linked_border_radius:I = 0x7f0d0170

.field public static final stream_one_up_linked_border_size:I = 0x7f0d016f

.field public static final stream_one_up_linked_header_text_size:I = 0x7f0d018c

.field public static final stream_one_up_linked_icon_margin_right:I = 0x7f0d0172

.field public static final stream_one_up_linked_inner_margin:I = 0x7f0d0171

.field public static final stream_one_up_list_max_width:I = 0x7f0d0166

.field public static final stream_one_up_list_min_height_land:I = 0x7f0d0165

.field public static final stream_one_up_list_min_height_port:I = 0x7f0d0164

.field public static final stream_one_up_location_icon_margin_right:I = 0x7f0d0177

.field public static final stream_one_up_location_icon_margin_top:I = 0x7f0d0176

.field public static final stream_one_up_location_text_size:I = 0x7f0d0190

.field public static final stream_one_up_margin_bottom:I = 0x7f0d0169

.field public static final stream_one_up_margin_left:I = 0x7f0d0167

.field public static final stream_one_up_margin_right:I = 0x7f0d0168

.field public static final stream_one_up_name_margin_top:I = 0x7f0d016d

.field public static final stream_one_up_name_text_size:I = 0x7f0d0189

.field public static final stream_one_up_place_review_aspects_margin_bottom:I = 0x7f0d019a

.field public static final stream_one_up_place_review_aspects_margin_top:I = 0x7f0d0199

.field public static final stream_one_up_place_review_divider_margin:I = 0x7f0d0197

.field public static final stream_one_up_place_review_divider_stroke_width:I = 0x7f0d0198

.field public static final stream_one_up_plus_one_button_margin_right:I = 0x7f0d0178

.field public static final stream_one_up_reshare_body_text_size:I = 0x7f0d018f

.field public static final stream_one_up_reshare_border_radius:I = 0x7f0d0174

.field public static final stream_one_up_reshare_border_size:I = 0x7f0d0173

.field public static final stream_one_up_reshare_header_text_size:I = 0x7f0d018e

.field public static final stream_one_up_reshare_inner_margin:I = 0x7f0d0175

.field public static final stream_one_up_stage_default_text_size:I = 0x7f0d0196

.field public static final stream_one_up_stage_hangout_avatar_size:I = 0x7f0d0187

.field public static final stream_one_up_stage_hangout_avatar_spacing:I = 0x7f0d0184

.field public static final stream_one_up_stage_hangout_button_margin_bottom:I = 0x7f0d0185

.field public static final stream_one_up_stage_hangout_name_margin:I = 0x7f0d0186

.field public static final stream_one_up_stage_skyjam_play_stop_padding:I = 0x7f0d0188

.field public static final stream_one_up_title_margin_bottom:I = 0x7f0d016e

.field public static final stream_original_author_name_text_size:I = 0x7f0d001a

.field public static final stream_selected_tab_line_height:I = 0x7f0d002e

.field public static final stream_skyjam_album_text_size:I = 0x7f0d001f

.field public static final stream_skyjam_artist_text_size:I = 0x7f0d001e

.field public static final stream_skyjam_media_border_size:I = 0x7f0d0008

.field public static final stream_skyjam_media_size:I = 0x7f0d0009

.field public static final stream_skyjam_right_margin:I = 0x7f0d000a

.field public static final stream_skyjam_title_text_size:I = 0x7f0d001d

.field public static final stream_tab_line_height:I = 0x7f0d002d

.field public static final stream_tab_strip_height:I = 0x7f0d002b

.field public static final stream_tab_strip_spacing:I = 0x7f0d002c

.field public static final stream_tab_text_size:I = 0x7f0d002f

.field public static final stream_video_height:I = 0x7f0d0016

.field public static final switcher_item_font_size:I = 0x7f0d0123

.field public static final tiny_avatar_dimension:I = 0x7f0d0033

.field public static final titlebar_height:I = 0x7f0d0000

.field public static final titlebar_icon_size:I = 0x7f0d0001

.field public static final titlebar_text_size:I = 0x7f0d0002

.field public static final tray_avatar_height:I = 0x7f0d00e1

.field public static final video_image_margin:I = 0x7f0d0108

.field public static final video_title_margin:I = 0x7f0d0107

.field public static final widget_content_text_size:I = 0x7f0d0018

.field public static final widget_margin_bottom:I = 0x7f0d00d2

.field public static final widget_margin_left:I = 0x7f0d00cf

.field public static final widget_margin_right:I = 0x7f0d00d1

.field public static final widget_margin_top:I = 0x7f0d00d0

.field public static final widget_stream_name_text_size:I = 0x7f0d0017


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
