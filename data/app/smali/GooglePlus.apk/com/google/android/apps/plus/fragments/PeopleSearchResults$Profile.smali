.class abstract Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;
.super Ljava/lang/Object;
.source "PeopleSearchResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleSearchResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Profile"
.end annotation


# instance fields
.field avatarUrl:Ljava/lang/String;

.field gaiaId:Ljava/lang/String;

.field name:Ljava/lang/String;

.field personId:Ljava/lang/String;

.field profileType:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 6
    .parameter "personId"
    .parameter "gaiaId"
    .parameter "name"
    .parameter "profileType"
    .parameter "avatarUrl"

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->personId:Ljava/lang/String;

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->gaiaId:Ljava/lang/String;

    .line 85
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->name:Ljava/lang/String;

    .line 86
    iput p4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->profileType:I

    .line 87
    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;->avatarUrl:Ljava/lang/String;

    .line 88
    return-void
.end method
