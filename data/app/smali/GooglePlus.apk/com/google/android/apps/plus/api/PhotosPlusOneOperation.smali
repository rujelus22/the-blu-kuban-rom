.class public final Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PhotosPlusOneOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;",
        "Lcom/google/api/services/plusi/model/PhotosPlusOneResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAlbumId:J

.field private final mIsPlusOne:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;JZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 20
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "ownerId"
    .parameter "albumId"
    .parameter "isPlusOne"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 34
    const-string v4, "photosplusone"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosPlusOneResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosPlusOneResponseJson;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 37
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mPhotoId:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mOwnerId:Ljava/lang/String;

    .line 39
    iput-wide p6, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mAlbumId:J

    .line 40
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mIsPlusOne:Z

    .line 41
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosPlusOneResponse;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->onStartResultProcessing()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mPhotoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mOwnerId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mIsPlusOne:Z

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    :goto_1a
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoPlusOne$55b1eb27(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    :cond_1d
    :goto_1d
    return-void

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1a

    :cond_20
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneResponse;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mPhotoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mOwnerId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneResponse;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoPlusOne$95d6774(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataPlusOne;)V

    goto :goto_1d
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->ownerId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mPhotoId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->photoId:Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mAlbumId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->albumId:Ljava/lang/Long;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->isPlusOne:Ljava/lang/Boolean;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->returnPlusOneResult:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mPhotoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mOwnerId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoPlusOne$55b1eb27(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-void
.end method
