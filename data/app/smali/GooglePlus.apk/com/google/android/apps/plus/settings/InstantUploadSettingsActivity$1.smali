.class final Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "InstantUploadSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 72
    if-nez p2, :cond_3

    .line 111
    :cond_2
    :goto_2
    return-void

    .line 76
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, action:Ljava/lang/String;
    const-string v8, "com.google.android.apps.plus.iu.upload_all_progress"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 78
    const-string v8, "upload_all_progress"

    const/4 v9, -0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 80
    .local v1, progress:I
    const-string v8, "upload_all_count"

    const/4 v9, -0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 82
    .local v7, total:I
    const-string v8, "upload_all_state"

    const/4 v9, -0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 87
    .local v2, state:I
    iget-object v9, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    if-eq v7, v1, :cond_6d

    const/4 v8, 0x1

    :goto_29
    #setter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z
    invoke-static {v9, v8}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$002(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)Z

    .line 89
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    #getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z
    invoke-static {v8}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z

    move-result v8

    if-nez v8, :cond_6f

    .line 90
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v9, 0x7f0800b9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 91
    .local v6, title:Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v9, 0x7f0800bb

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 92
    .local v4, summary:Ljava/lang/String;
    const v8, 0x7f0800c8

    const/4 v9, 0x0

    invoke-static {p1, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 94
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget-object v9, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    #getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mUploadsProgressReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v9}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$100(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 107
    :goto_5c
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$300()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 108
    .local v5, syncNowPreference:Landroid/preference/Preference;
    invoke-virtual {v5, v6}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {v5, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 87
    .end local v4           #summary:Ljava/lang/String;
    .end local v5           #syncNowPreference:Landroid/preference/Preference;
    .end local v6           #title:Ljava/lang/String;
    :cond_6d
    const/4 v8, 0x0

    goto :goto_29

    .line 96
    :cond_6f
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v9, 0x7f0800ba

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 97
    .restart local v6       #title:Ljava/lang/String;
    if-eqz v2, :cond_7d

    const/4 v8, 0x1

    if-ne v2, v8, :cond_98

    .line 99
    :cond_7d
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v9, 0x7f0800bd

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4       #summary:Ljava/lang/String;
    goto :goto_5c

    .line 102
    .end local v4           #summary:Ljava/lang/String;
    :cond_98
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {v8, v2}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$200(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;I)Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, stateString:Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v9, 0x7f0800be

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v3, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4       #summary:Ljava/lang/String;
    goto :goto_5c
.end method
