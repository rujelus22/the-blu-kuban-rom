.class public Lcom/google/android/apps/plus/oob/ErrorFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "ErrorFieldLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .registers 8
    .parameter "field"
    .parameter "id"
    .parameter "actionCallback"

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/ErrorFieldLayout;->getLabelView()Landroid/widget/TextView;

    move-result-object v0

    .line 61
    .local v0, labelView:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/ErrorFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v2

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->error:Lcom/google/api/services/plusi/model/OutOfBoxError;

    iget-object v1, v2, Lcom/google/api/services/plusi/model/OutOfBoxError;->text:Ljava/lang/String;

    .line 62
    .local v1, text:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 63
    return-void
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .registers 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
