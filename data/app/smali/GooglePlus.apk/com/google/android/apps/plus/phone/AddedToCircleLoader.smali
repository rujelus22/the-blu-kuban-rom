.class public final Lcom/google/android/apps/plus/phone/AddedToCircleLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "AddedToCircleLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mActors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mNotificationId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "notificationId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p4, actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mContext:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mNotificationId:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mActors:Ljava/util/List;

    .line 45
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    .line 46
    .local v1, count:I
    new-array v4, v1, [Ljava/lang/String;

    .line 47
    .local v4, selectionArgs:[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .local v5, selectionBuilder:Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v6, sortOrderBuilder:Ljava/lang/StringBuilder;
    const-string v7, "gaia_id IN ("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const-string v7, "(CASE gaia_id"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    const/4 v3, 0x0

    .local v3, i:I
    :goto_26
    if-ge v3, v1, :cond_51

    .line 53
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataActor;

    .line 54
    .local v0, actor:Lcom/google/api/services/plusi/model/DataActor;
    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    .line 55
    .local v2, gaiaId:Ljava/lang/String;
    aput-object v2, v4, v3

    .line 56
    if-lez v3, :cond_39

    .line 57
    const/16 v7, 0x2c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 59
    :cond_39
    const-string v7, " ?"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    const-string v7, " WHEN \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v7, "\' THEN "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    add-int/lit8 v3, v3, 0x1

    goto :goto_26

    .line 66
    .end local v0           #actor:Lcom/google/api/services/plusi/model/DataActor;
    .end local v2           #gaiaId:Ljava/lang/String;
    :cond_51
    const/16 v7, 0x29

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    const-string v7, " END)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    sget-object v7, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v7, p2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->setUri(Landroid/net/Uri;)V

    .line 70
    sget-object v7, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment$AddedToCircleQuery;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->setProjection([Ljava/lang/String;)V

    .line 71
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->setSelection(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->setSelectionArgs([Ljava/lang/String;)V

    .line 73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->setSortOrder(Ljava/lang/String;)V

    .line 74
    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 5

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mNotificationId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->hasCircleActionData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mNotificationId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;->mActors:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertCircleActionData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    .line 85
    :cond_17
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
