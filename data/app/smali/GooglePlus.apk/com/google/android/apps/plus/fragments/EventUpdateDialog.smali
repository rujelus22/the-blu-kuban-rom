.class public Lcom/google/android/apps/plus/fragments/EventUpdateDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "EventUpdateDialog.java"


# instance fields
.field private mEventUpdate:Lcom/google/android/apps/plus/views/EventUpdate;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static newInstance()Lcom/google/android/apps/plus/fragments/EventUpdateDialog;
    .registers 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->getTheme()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->setStyle(II)V

    .line 50
    if-eqz p1, :cond_7e

    .line 51
    new-instance v0, Lcom/google/android/apps/plus/views/EventUpdate;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/EventUpdate;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->mEventUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->mEventUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    const-string v1, "eventupdate"

    if-eqz p1, :cond_7e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".timestampe"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/plus/views/EventUpdate;->timestamp:J

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".ownername"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/plus/views/EventUpdate;->ownerName:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".gaiaid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/plus/views/EventUpdate;->gaiaId:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".comment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/views/EventUpdate;->comment:Ljava/lang/String;

    .line 54
    :cond_7e
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 13
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 69
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_47

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 75
    .local v0, context:Landroid/content/Context;
    :goto_c
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 77
    .local v2, layout:Landroid/widget/LinearLayout;
    new-instance v4, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;

    invoke-direct {v4, v0}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;-><init>(Landroid/content/Context;)V

    .line 80
    .local v4, updateView:Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v5, v7, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    invoke-virtual {v4, v8}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->toggleCardBorderStyle(Z)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 85
    .local v1, fragment:Landroid/support/v4/app/Fragment;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->mEventUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    check-cast v1, Lcom/google/android/apps/plus/views/EventActionListener;

    .end local v1           #fragment:Landroid/support/v4/app/Fragment;
    invoke-virtual {v4, v5, v1, v8}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->bind(Lcom/google/android/apps/plus/views/EventUpdate;Lcom/google/android/apps/plus/views/EventActionListener;Z)V

    .line 87
    new-instance v3, Landroid/widget/ScrollView;

    invoke-direct {v3, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 88
    .local v3, scrollView:Landroid/widget/ScrollView;
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v5}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 92
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 93
    const v5, 0x106000b

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 97
    return-object v2

    .line 72
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #layout:Landroid/widget/LinearLayout;
    .end local v3           #scrollView:Landroid/widget/ScrollView;
    .end local v4           #updateView:Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;
    :cond_47
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v6, 0x103000b

    invoke-direct {v0, v5, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v0       #context:Landroid/content/Context;
    goto :goto_c
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 7
    .parameter "arg0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->mEventUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    const-string v1, "eventupdate"

    if-eqz p1, :cond_66

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".timestampe"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, v0, Lcom/google/android/apps/plus/views/EventUpdate;->timestamp:J

    invoke-virtual {p1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".ownername"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventUpdate;->ownerName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".gaiaid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventUpdate;->gaiaId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".comment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/plus/views/EventUpdate;->comment:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_66
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public final setUpdate(Lcom/google/android/apps/plus/views/EventUpdate;)V
    .registers 2
    .parameter "update"

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->mEventUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    .line 42
    return-void
.end method
