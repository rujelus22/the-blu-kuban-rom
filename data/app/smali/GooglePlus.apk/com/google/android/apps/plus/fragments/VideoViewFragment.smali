.class public Lcom/google/android/apps/plus/fragments/VideoViewFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "VideoViewFragment.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/media/MediaPlayer$OnErrorListener;",
        "Landroid/media/MediaPlayer$OnInfoListener;",
        "Landroid/media/MediaPlayer$OnPreparedListener;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;

.field private static final sPlayableTypes:Landroid/util/SparseBooleanArray;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mError:Z

.field private final mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mIntent:Landroid/content/Intent;

.field private mIsWiFiConnection:Z

.field private mLoading:Z

.field private mOwnerId:Ljava/lang/String;

.field private mPerformedRefetch:Z

.field private mPhotoId:J

.field private mPlayOnResume:Z

.field private mPlayerView:Landroid/widget/VideoView;

.field private mPreviousOrientation:I

.field private mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

.field private mVideoPosition:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    .line 50
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->PROJECTION:[Ljava/lang/String;

    .line 73
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 74
    sput-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 75
    sget-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x16

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 77
    sget-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 78
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    .line 104
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    .line 106
    new-instance v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/VideoViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;-><init>()V

    .line 121
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/VideoViewFragment;)J
    .registers 3
    .parameter "x0"

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    return-wide v0
.end method

.method private startPlayback()V
    .registers 11

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 375
    const-string v1, "READY"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, "FINAL"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a1

    .line 378
    :cond_1b
    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    const-wide/16 v7, 0x0

    cmp-long v1, v1, v7

    if-nez v1, :cond_3d

    .line 380
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataVideo;->stream:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataVideoStream;

    iget-object v0, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->url:Ljava/lang/String;

    .line 387
    .local v0, streamUrl:Ljava/lang/String;
    :goto_2f
    if-eqz v0, :cond_94

    .line 389
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 390
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 400
    .end local v0           #streamUrl:Ljava/lang/String;
    :goto_3c
    return-void

    .line 384
    :cond_3d
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataVideo;->stream:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v3

    :goto_46
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataVideoStream;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->height:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sget-object v4, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->formatId:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v4, v9}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    if-eqz v4, :cond_c4

    iget-object v4, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->url:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c4

    const/16 v4, 0x280

    if-gt v8, v4, :cond_8b

    move v4, v5

    :goto_73
    if-eqz v2, :cond_89

    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    if-eqz v9, :cond_7b

    if-gtz v8, :cond_89

    :cond_7b
    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    if-nez v9, :cond_83

    if-eqz v4, :cond_83

    if-gtz v8, :cond_89

    :cond_83
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    if-nez v4, :cond_c4

    if-gez v8, :cond_c4

    :cond_89
    :goto_89
    move-object v2, v1

    goto :goto_46

    :cond_8b
    move v4, v6

    goto :goto_73

    :cond_8d
    if-nez v2, :cond_91

    move-object v0, v3

    goto :goto_2f

    :cond_91
    iget-object v0, v2, Lcom/google/api/services/plusi/model/DataVideoStream;->url:Ljava/lang/String;

    goto :goto_2f

    .line 392
    .restart local v0       #streamUrl:Ljava/lang/String;
    :cond_94
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    .line 393
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080076

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    goto :goto_3c

    .line 395
    .end local v0           #streamUrl:Ljava/lang/String;
    :cond_a1
    const-string v1, "PENDING"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b8

    .line 396
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080075

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    goto :goto_3c

    .line 398
    :cond_b8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080074

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    goto/16 :goto_3c

    :cond_c4
    move-object v1, v2

    goto :goto_89
.end method

.method private updateView(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    .line 436
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v1, :cond_25

    const-string v1, "READY"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1c

    const-string v1, "FINAL"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_25

    :cond_1c
    const/4 v0, 0x1

    .line 440
    .local v0, hasResults:Z
    :goto_1d
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    if-eqz v1, :cond_27

    .line 441
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 449
    :goto_24
    return-void

    .line 436
    .end local v0           #hasResults:Z
    :cond_25
    const/4 v0, 0x0

    goto :goto_1d

    .line 443
    .restart local v0       #hasResults:Z
    :cond_27
    if-eqz v0, :cond_31

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    if-nez v1, :cond_31

    .line 444
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->showContent(Landroid/view/View;)V

    goto :goto_24

    .line 446
    :cond_31
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_24
.end method


# virtual methods
.method protected final isEmpty()Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 242
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v3, :cond_2b

    const-string v3, "READY"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    const-string v3, "FINAL"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2b

    :cond_1e
    move v1, v2

    .line 245
    .local v1, hasResults:Z
    :goto_1f
    if-eqz v1, :cond_29

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    if-nez v3, :cond_29

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    if-eqz v3, :cond_2a

    :cond_29
    move v0, v2

    .line 246
    .local v0, empty:Z
    :cond_2a
    return v0

    .end local v0           #empty:Z
    .end local v1           #hasResults:Z
    :cond_2b
    move v1, v0

    .line 242
    goto :goto_1f
.end method

.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    .prologue
    .line 251
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 252
    .local v1, obj:Ljava/lang/Object;
    instance-of v3, v1, Landroid/widget/MediaController;

    if-eqz v3, :cond_14

    move-object v2, v1

    .line 253
    check-cast v2, Landroid/widget/MediaController;

    .line 254
    .local v2, playbackController:Landroid/widget/MediaController;
    invoke-virtual {v2}, Landroid/widget/MediaController;->isShowing()Z

    move-result v3

    if-nez v3, :cond_14

    .line 256
    :try_start_11
    invoke-virtual {v2}, Landroid/widget/MediaController;->show()V
    :try_end_14
    .catch Ljava/lang/NullPointerException; {:try_start_11 .. :try_end_14} :catch_15

    .line 269
    .end local v2           #playbackController:Landroid/widget/MediaController;
    :cond_14
    :goto_14
    return-void

    .line 259
    .restart local v2       #playbackController:Landroid/widget/MediaController;
    :catch_15
    move-exception v3

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 260
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 262
    .local v0, fragmentView:Landroid/view/View;
    if-eqz v0, :cond_14

    .line 263
    const v3, 0x7f080076

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 264
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    goto :goto_14
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 128
    if-eqz p1, :cond_2f

    .line 129
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.google.android.apps.plus.VideoViewFragment.INTENT"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    .line 130
    const-string v2, "com.google.android.apps.plus.VideoViewFragment.POSITION"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoPosition:I

    .line 131
    const-string v2, "com.google.android.apps.plus.VideoViewFragment.PLAY_ON_RESUME"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    .line 132
    const-string v2, "com.google.android.apps.plus.VideoViewFragment.PREVIOUS_ORIENTATION"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    .line 142
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v1, v2, Landroid/content/res/Configuration;->orientation:I

    .line 143
    .local v1, currentOrientation:I
    iget v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    if-eq v2, v1, :cond_42

    .line 144
    iput v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    .line 145
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    .line 148
    :cond_42
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 149
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "photo_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    .line 150
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "owner_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mOwnerId:Ljava/lang/String;

    .line 151
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "data"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_87

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "data"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 153
    .local v0, blob:[B
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    .line 154
    if-eqz v0, :cond_87

    .line 155
    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataVideo;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    .line 158
    .end local v0           #blob:[B
    :cond_87
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 10
    .parameter "id"
    .parameter "bundle"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 273
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v5, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    invoke-static {v0, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 276
    .local v2, photoUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, -0x1

    .line 163
    const v3, 0x7f0300d6

    invoke-super {p0, p1, p2, p3, v3}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v2

    .line 166
    .local v2, view:Landroid/view/View;
    new-instance v1, Landroid/widget/MediaController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    .line 167
    .local v1, playbackController:Landroid/widget/MediaController;
    const v3, 0x7f090243

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 168
    .local v0, layout:Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 171
    invoke-virtual {v1, v0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    .line 172
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/widget/MediaController;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    const v3, 0x7f090244

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/VideoView;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    .line 176
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v3, v1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 177
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v3, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 178
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v3, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 180
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-nez v3, :cond_53

    .line 181
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 185
    :cond_53
    const v3, 0x7f080074

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 186
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    .line 188
    return-object v2
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .registers 11
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    const/4 v6, 0x1

    .line 337
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPerformedRefetch:Z

    if-nez v1, :cond_17

    if-ne p2, v6, :cond_17

    .line 339
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPerformedRefetch:Z

    .line 340
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J)I

    .line 352
    :cond_16
    :goto_16
    return v6

    .line 343
    :cond_17
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    .line 344
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 347
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_16

    .line 348
    const v1, 0x7f080076

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 349
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    goto :goto_16
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .registers 7
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 306
    sparse-switch p2, :sswitch_data_1e

    .line 322
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 323
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_e

    .line 324
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    .line 326
    :cond_e
    return v2

    .line 308
    .end local v0           #v:Landroid/view/View;
    :sswitch_f
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 309
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    goto :goto_5

    .line 312
    :sswitch_14
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 313
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    goto :goto_5

    .line 318
    :sswitch_19
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 319
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    goto :goto_5

    .line 306
    :sswitch_data_1e
    .sparse-switch
        0x1 -> :sswitch_19
        0x64 -> :sswitch_19
        0xc8 -> :sswitch_19
        0x2bd -> :sswitch_f
        0x2be -> :sswitch_14
    .end sparse-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v1, 0x0

    .line 47
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    if-eqz p2, :cond_29

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v0, :cond_22

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v0, :cond_29

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->startPlayback()V

    :cond_29
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_32

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    :cond_32
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 302
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 214
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->canPause()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 225
    :cond_1c
    :goto_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 226
    return-void

    .line 220
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    goto :goto_1c
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .registers 5
    .parameter "mp"

    .prologue
    .line 357
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    .line 358
    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 359
    iget v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoPosition:I

    if-nez v1, :cond_19

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    .line 365
    :goto_f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 366
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_18

    .line 367
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    .line 369
    :cond_18
    return-void

    .line 362
    .end local v0           #v:Landroid/view/View;
    :cond_19
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoPosition:I

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->seekTo(I)V

    .line 363
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    goto :goto_f
.end method

.method public final onResume()V
    .registers 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 193
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    .line 195
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 199
    .local v0, connManager:Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 201
    .local v1, networkInfo:Landroid/net/NetworkInfo;
    if-eqz v1, :cond_39

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-ne v4, v2, :cond_39

    :goto_22
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    .line 204
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v2, :cond_2f

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    if-eqz v2, :cond_2f

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->startPlayback()V

    .line 207
    :cond_2f
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    .line 210
    return-void

    :cond_39
    move v2, v3

    .line 201
    goto :goto_22
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 230
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_2b

    .line 233
    const-string v0, "com.google.android.apps.plus.VideoViewFragment.INTENT"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 234
    const-string v0, "com.google.android.apps.plus.VideoViewFragment.POSITION"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 235
    const-string v0, "com.google.android.apps.plus.VideoViewFragment.PLAY_ON_RESUME"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 236
    const-string v0, "com.google.android.apps.plus.VideoViewFragment.PREVIOUS_ORIENTATION"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 238
    :cond_2b
    return-void
.end method
