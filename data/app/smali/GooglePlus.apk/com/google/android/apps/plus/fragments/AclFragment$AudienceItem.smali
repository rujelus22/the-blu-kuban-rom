.class final Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;
.super Ljava/lang/Object;
.source "AclFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/AclFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AudienceItem"
.end annotation


# instance fields
.field public final mContent:Ljava/lang/String;

.field public final mPerson:Lcom/google/android/apps/plus/content/PersonData;

.field public final mType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 4
    .parameter "type"
    .parameter "content"

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mType:I

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mContent:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 3
    .parameter "person"

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mType:I

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    .line 66
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mContent:Ljava/lang/String;

    .line 67
    return-void
.end method
