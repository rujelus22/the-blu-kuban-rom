.class public Lcom/google/android/apps/plus/content/DbMedia;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbMedia.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/DbMedia;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAlbum:Ljava/lang/String;

.field private final mAlbumArtist:Ljava/lang/String;

.field private final mAlbumId:Ljava/lang/String;

.field private final mContentUrl:Ljava/lang/String;

.field private final mDescription:Ljava/lang/String;

.field private final mFaviconUrl:Ljava/lang/String;

.field private final mIsPWA:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:Ljava/lang/String;

.field private final mPlayerHeight:I

.field private final mPlayerUrl:Ljava/lang/String;

.field private final mPlayerWidth:I

.field private final mSkyjamPreviewUrl:Ljava/lang/String;

.field private final mSongTitle:Ljava/lang/String;

.field private final mThumbnailUrl:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private final mTitlePlaintext:Ljava/lang/String;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 190
    new-instance v0, Lcom/google/android/apps/plus/content/DbMedia$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbMedia$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/DbMedia;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    .prologue
    const/4 v0, 0x1

    .line 86
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mContentUrl:Ljava/lang/String;

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mFaviconUrl:Ljava/lang/String;

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitle:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitlePlaintext:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mDescription:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mThumbnailUrl:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerUrl:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mOwnerId:Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumId:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPhotoId:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbum:Ljava/lang/String;

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumArtist:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSongTitle:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSkyjamPreviewUrl:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mType:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerWidth:I

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerHeight:I

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_73

    :goto_70
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mIsPWA:Z

    .line 105
    return-void

    .line 104
    :cond_73
    const/4 v0, 0x0

    goto :goto_70
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbMedia;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .registers 21
    .parameter "contentUrl"
    .parameter "faviconUrl"
    .parameter "title"
    .parameter "titlePlaintext"
    .parameter "description"
    .parameter "thumbnailUrl"
    .parameter "playerUrl"
    .parameter "ownerId"
    .parameter "albumId"
    .parameter "photoId"
    .parameter "album"
    .parameter "albumArtist"
    .parameter "songTitle"
    .parameter "skyjamPreviewUrl"
    .parameter "type"
    .parameter "playerWidth"
    .parameter "playerHeight"
    .parameter "isPWA"

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 61
    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbMedia;->appendProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mContentUrl:Ljava/lang/String;

    .line 62
    invoke-static {p2}, Lcom/google/android/apps/plus/content/DbMedia;->appendProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mFaviconUrl:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitle:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitlePlaintext:Ljava/lang/String;

    .line 65
    iput-object p5, p0, Lcom/google/android/apps/plus/content/DbMedia;->mDescription:Ljava/lang/String;

    .line 66
    invoke-static {p6}, Lcom/google/android/apps/plus/content/DbMedia;->appendProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mThumbnailUrl:Ljava/lang/String;

    .line 67
    invoke-static {p7}, Lcom/google/android/apps/plus/content/DbMedia;->appendProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerUrl:Ljava/lang/String;

    .line 68
    iput-object p8, p0, Lcom/google/android/apps/plus/content/DbMedia;->mOwnerId:Ljava/lang/String;

    .line 69
    iput-object p9, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumId:Ljava/lang/String;

    .line 70
    iput-object p10, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPhotoId:Ljava/lang/String;

    .line 71
    iput-object p11, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbum:Ljava/lang/String;

    .line 72
    iput-object p12, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumArtist:Ljava/lang/String;

    .line 73
    iput-object p13, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSongTitle:Ljava/lang/String;

    .line 74
    invoke-static/range {p14 .. p14}, Lcom/google/android/apps/plus/content/DbMedia;->appendProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSkyjamPreviewUrl:Ljava/lang/String;

    .line 75
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mType:I

    .line 76
    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerWidth:I

    .line 77
    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerHeight:I

    .line 78
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mIsPWA:Z

    .line 79
    return-void
.end method

.method private static appendProtocol(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "url"

    .prologue
    .line 323
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_25

    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_25

    const-string v0, "https:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_25

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 326
    :cond_25
    return-object p0
.end method

.method public static deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;
    .registers 27
    .parameter "array"

    .prologue
    .line 281
    if-nez p0, :cond_5

    .line 282
    const/16 v24, 0x0

    .line 319
    :cond_4
    return-object v24

    .line 285
    :cond_5
    invoke-static/range {p0 .. p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v21

    .line 286
    .local v21, bb:Ljava/nio/ByteBuffer;
    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v23

    .line 287
    .local v23, items:S
    move/from16 v0, v23

    new-array v0, v0, [Lcom/google/android/apps/plus/content/DbMedia;

    move-object/from16 v24, v0

    .line 294
    .local v24, mediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    const/16 v22, 0x0

    .local v22, i:S
    :goto_15
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_4

    .line 295
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    .line 296
    .local v3, contentUrl:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    .line 297
    .local v4, faviconUrl:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getLongString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    .line 298
    .local v5, title:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getLongString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    .line 299
    .local v6, titlePlaintext:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getLongString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    .line 300
    .local v7, description:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    .line 301
    .local v8, thumbnailUrl:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v9

    .line 302
    .local v9, playerUrl:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v10

    .line 303
    .local v10, ownerId:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v11

    .line 304
    .local v11, albumId:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v12

    .line 305
    .local v12, photoId:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v13

    .line 306
    .local v13, album:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v14

    .line 307
    .local v14, albumArtist:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v15

    .line 308
    .local v15, songTitle:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v16

    .line 309
    .local v16, skyjamPreviewUrl:Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v17

    .line 310
    .local v17, type:I
    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v18

    .line 311
    .local v18, playerWidth:I
    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v19

    .line 312
    .local v19, playerHeight:I
    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    const/16 v25, 0x1

    move/from16 v0, v25

    if-ne v2, v0, :cond_78

    const/16 v20, 0x1

    .line 314
    .local v20, isPWA:Z
    :goto_6b
    new-instance v2, Lcom/google/android/apps/plus/content/DbMedia;

    invoke-direct/range {v2 .. v20}, Lcom/google/android/apps/plus/content/DbMedia;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    aput-object v2, v24, v22

    .line 294
    add-int/lit8 v2, v22, 0x1

    int-to-short v0, v2

    move/from16 v22, v0

    goto :goto_15

    .line 312
    .end local v20           #isPWA:Z
    :cond_78
    const/16 v20, 0x0

    goto :goto_6b
.end method

.method public static serialize(Ljava/util/List;)[B
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/DbMedia;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    .local p0, mediaList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/DbMedia;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 239
    .local v2, items:I
    if-nez v2, :cond_8

    .line 240
    const/4 v4, 0x0

    .line 270
    :goto_7
    return-object v4

    .line 243
    :cond_8
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v6, 0x20

    invoke-direct {v5, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 244
    .local v5, stream:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 246
    .local v3, os:Ljava/io/DataOutputStream;
    invoke-virtual {v3, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 247
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_87

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbMedia;

    .line 248
    .local v0, dbMedia:Lcom/google/android/apps/plus/content/DbMedia;
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mContentUrl:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 249
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mFaviconUrl:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 250
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mTitle:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putLongString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 251
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mTitlePlaintext:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putLongString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 252
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mDescription:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putLongString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 253
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mThumbnailUrl:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 254
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerUrl:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 255
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mOwnerId:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 256
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumId:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 257
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mPhotoId:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 258
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbum:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 259
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumArtist:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 260
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mSongTitle:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 261
    iget-object v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mSkyjamPreviewUrl:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/content/DbMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 262
    iget v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mType:I

    invoke-virtual {v3, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 263
    iget v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerWidth:I

    invoke-virtual {v3, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 264
    iget v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerHeight:I

    invoke-virtual {v3, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 265
    iget-boolean v6, v0, Lcom/google/android/apps/plus/content/DbMedia;->mIsPWA:Z

    if-eqz v6, :cond_85

    const/4 v6, 0x1

    :goto_81
    invoke-virtual {v3, v6}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_1b

    :cond_85
    const/4 v6, 0x0

    goto :goto_81

    .line 268
    .end local v0           #dbMedia:Lcom/google/android/apps/plus/content/DbMedia;
    :cond_87
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 269
    .local v4, result:[B
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    goto/16 :goto_7
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 227
    const/4 v0, 0x0

    return v0
.end method

.method public final getAlbum()Ljava/lang/String;
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbum:Ljava/lang/String;

    return-object v0
.end method

.method public final getAlbumArtist()Ljava/lang/String;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumArtist:Ljava/lang/String;

    return-object v0
.end method

.method public final getAlbumId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public final getContentUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mContentUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getOwnerId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mOwnerId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPhotoId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPhotoId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlayerHeight()I
    .registers 2

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerHeight:I

    return v0
.end method

.method public final getPlayerUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlayerWidth()I
    .registers 2

    .prologue
    .line 176
    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerWidth:I

    return v0
.end method

.method public final getSkyjamMarketUrl()Ljava/lang/String;
    .registers 3

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mType:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_c

    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mType:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_f

    .line 113
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mContentUrl:Ljava/lang/String;

    .line 115
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final getSkyjamPreviewUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSkyjamPreviewUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getSongTitle()Ljava/lang/String;
    .registers 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSongTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getThumbnailUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mThumbnailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .registers 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitlePlaintext()Ljava/lang/String;
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitlePlaintext:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .registers 2

    .prologue
    .line 172
    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mType:I

    return v0
.end method

.method public final isPWA()Z
    .registers 2

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mIsPWA:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "p"
    .parameter "flags"

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mContentUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mFaviconUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mTitlePlaintext:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mThumbnailUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mOwnerId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPhotoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbum:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mAlbumArtist:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSongTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mSkyjamPreviewUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 219
    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    iget v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mPlayerHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 222
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbMedia;->mIsPWA:Z

    if-eqz v0, :cond_5f

    const/4 v0, 0x1

    :goto_5a
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 223
    return-void

    .line 222
    :cond_5f
    const/4 v0, 0x0

    goto :goto_5a
.end method
