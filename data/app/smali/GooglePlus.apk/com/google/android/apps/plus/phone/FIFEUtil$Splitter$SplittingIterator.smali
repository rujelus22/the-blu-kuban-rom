.class abstract Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;
.super Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;
.source "FIFEUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "SplittingIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field offset:I

.field final omitEmptyStrings:Z

.field final toSplit:Ljava/lang/CharSequence;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "splitter"
    .parameter "toSplit"

    .prologue
    const/4 v0, 0x0

    .line 558
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;-><init>(B)V

    .line 556
    iput v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->offset:I

    .line 559
    #getter for: Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->omitEmptyStrings:Z
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->access$200(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->omitEmptyStrings:Z

    .line 560
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->toSplit:Ljava/lang/CharSequence;

    .line 561
    return-void
.end method


# virtual methods
.method protected final bridge synthetic computeNext()Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v3, -0x1

    .line 548
    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->offset:I

    if-eq v0, v3, :cond_2f

    iget v1, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->offset:I

    iget v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->offset:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->separatorStart(I)I

    move-result v0

    if-ne v0, v3, :cond_28

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iput v3, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->offset:I

    :goto_17
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->omitEmptyStrings:Z

    if-eqz v2, :cond_1d

    if-eq v1, v0, :cond_1

    :cond_1d
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v2, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_27
    return-object v0

    :cond_28
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->separatorEnd(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;->offset:I

    goto :goto_17

    :cond_2f
    sget-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->DONE:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    const/4 v0, 0x0

    goto :goto_27
.end method

.method abstract separatorEnd(I)I
.end method

.method abstract separatorStart(I)I
.end method
