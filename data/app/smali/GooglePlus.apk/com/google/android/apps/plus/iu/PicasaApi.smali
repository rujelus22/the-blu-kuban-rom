.class public final Lcom/google/android/apps/plus/iu/PicasaApi;
.super Ljava/lang/Object;
.source "PicasaApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;
    }
.end annotation


# instance fields
.field private final mBaseUrl:Ljava/lang/String;

.field private final mClient:Lcom/google/android/apps/plus/iu/GDataClient;

.field private final mOperation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .registers 4
    .parameter "resolver"

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/GDataClient$Operation;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mOperation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;

    .line 43
    new-instance v1, Lcom/google/android/apps/plus/iu/GDataClient;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/GDataClient;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mClient:Lcom/google/android/apps/plus/iu/GDataClient;

    .line 44
    const-string v1, "picasa_gdata_base_url"

    invoke-static {p1, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, baseUrl:Ljava/lang/String;
    if-nez v0, :cond_1b

    const-string v0, "https://picasaweb.google.com/data/feed/api/"

    .end local v0           #baseUrl:Ljava/lang/String;
    :cond_1b
    iput-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mBaseUrl:Ljava/lang/String;

    .line 47
    return-void
.end method

.method private getUploadedPhotos$67a1bd15(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/iu/PhotoCollectorJson;)I
    .registers 16
    .parameter "account"
    .parameter "etagHolder"
    .parameter "url"
    .parameter "parser"

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x3

    .line 98
    const-string v7, "PicasaApi.getUploadedPhotos"

    invoke-static {v7}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v2

    .line 99
    .local v2, statsId:I
    const-string v7, "PicasaAPI"

    invoke-static {v7, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_29

    .line 100
    const-string v7, "PicasaAPI"

    const-string v8, "get uploaded photos for %s etag %s"

    new-array v9, v5, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v3

    aget-object v10, p2, v3

    aput-object v10, v9, v4

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_29
    :try_start_29
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mOperation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;

    .line 106
    .local v1, operation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;
    const/4 v7, 0x0

    aget-object v7, p2, v7

    iput-object v7, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    .line 107
    iget-object v7, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mClient:Lcom/google/android/apps/plus/iu/GDataClient;

    invoke-virtual {v7, p3, v1}, Lcom/google/android/apps/plus/iu/GDataClient;->get(Ljava/lang/String;Lcom/google/android/apps/plus/iu/GDataClient$Operation;)V
    :try_end_35
    .catchall {:try_start_29 .. :try_end_35} :catchall_f6
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_35} :catch_cd

    .line 109
    :try_start_35
    iget v7, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outStatus:I

    sparse-switch v7, :sswitch_data_10a

    .line 123
    const-string v3, "PicasaAPI"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_59

    .line 124
    const-string v3, "PicasaAPI"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getUploadedPhotos failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_59
    .catchall {:try_start_35 .. :try_end_59} :catchall_c6

    .line 126
    :cond_59
    :try_start_59
    iget-object v3, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_5e
    .catchall {:try_start_59 .. :try_end_5e} :catchall_f6
    .catch Ljava/lang/Exception; {:try_start_59 .. :try_end_5e} :catch_cd

    .line 138
    const-string v3, "PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6d

    .line 139
    const-string v3, "PicasaAPI"

    const-string v4, "   done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_6d
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v6

    .end local v1           #operation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;
    :goto_71
    return v3

    .line 112
    .restart local v1       #operation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;
    :sswitch_72
    const/4 v4, 0x0

    :try_start_73
    iget-object v5, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    aput-object v5, p2, v4

    .line 114
    iget-object v4, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-virtual {p4, v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->parse(Ljava/io/InputStream;)V
    :try_end_7c
    .catchall {:try_start_73 .. :try_end_7c} :catchall_c6

    .line 115
    :try_start_7c
    iget-object v4, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_81
    .catchall {:try_start_7c .. :try_end_81} :catchall_f6
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_81} :catch_cd

    .line 138
    const-string v4, "PicasaAPI"

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_90

    .line 139
    const-string v4, "PicasaAPI"

    const-string v5, "   done"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_90
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    goto :goto_71

    .line 118
    :sswitch_94
    :try_start_94
    iget-object v3, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_99
    .catchall {:try_start_94 .. :try_end_99} :catchall_f6
    .catch Ljava/lang/Exception; {:try_start_94 .. :try_end_99} :catch_cd

    .line 138
    const-string v3, "PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a8

    .line 139
    const-string v3, "PicasaAPI"

    const-string v5, "   done"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_a8
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v4

    goto :goto_71

    .line 121
    :sswitch_ad
    :try_start_ad
    iget-object v3, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V
    :try_end_b2
    .catchall {:try_start_ad .. :try_end_b2} :catchall_f6
    .catch Ljava/lang/Exception; {:try_start_ad .. :try_end_b2} :catch_cd

    .line 138
    const-string v3, "PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_c1

    .line 139
    const-string v3, "PicasaAPI"

    const-string v4, "   done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_c1
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v5

    goto :goto_71

    .line 129
    :catchall_c6
    move-exception v3

    :try_start_c7
    iget-object v4, v1, Lcom/google/android/apps/plus/iu/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v3
    :try_end_cd
    .catchall {:try_start_c7 .. :try_end_cd} :catchall_f6
    .catch Ljava/lang/Exception; {:try_start_c7 .. :try_end_cd} :catch_cd

    .line 131
    .end local v1           #operation:Lcom/google/android/apps/plus/iu/GDataClient$Operation;
    :catch_cd
    move-exception v0

    .line 132
    .local v0, e:Ljava/lang/Exception;
    :try_start_ce
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->handleInterrruptedException(Ljava/lang/Throwable;)Z

    .line 133
    const-string v3, "PicasaAPI"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e1

    .line 134
    const-string v3, "PicasaAPI"

    const-string v4, "getUploadedPhotos failed"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e1
    .catchall {:try_start_ce .. :try_end_e1} :catchall_f6

    .line 136
    :cond_e1
    const-string v3, "PicasaAPI"

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_f0

    .line 139
    const-string v3, "PicasaAPI"

    const-string v4, "   done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_f0
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    move v3, v6

    goto/16 :goto_71

    .line 138
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_f6
    move-exception v3

    const-string v4, "PicasaAPI"

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_106

    .line 139
    const-string v4, "PicasaAPI"

    const-string v5, "   done"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_106
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v3

    .line 109
    :sswitch_data_10a
    .sparse-switch
        0xc8 -> :sswitch_72
        0x130 -> :sswitch_94
        0x191 -> :sswitch_ad
        0x193 -> :sswitch_ad
    .end sparse-switch
.end method


# virtual methods
.method public final getUploadedPhotos(Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;)I
    .registers 13
    .parameter "account"
    .parameter "etagHolder"
    .parameter "handler"

    .prologue
    const/4 v6, 0x0

    .line 60
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "user/"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v8, "@gmail."

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_22

    const-string v8, "@googlemail."

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2c

    :cond_22
    const/16 v8, 0x40

    invoke-virtual {v5, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    invoke-virtual {v5, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    :cond_2c
    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "?max-results=1000&imgmax=d&thumbsize=640u&visibility=visible&v=4&alt=json&kind=photo"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "&streamid=camera_sync_created"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, baseUrl:Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;

    invoke-direct {v1, p3}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;-><init>(Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;)V

    .line 71
    .local v1, parser:Lcom/google/android/apps/plus/iu/PhotoCollectorJson;
    const/4 v3, 0x1

    .line 73
    .local v3, startIndex:I
    :goto_4a
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "&start-index="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, url:Ljava/lang/String;
    invoke-direct {p0, p1, p2, v4, v1}, Lcom/google/android/apps/plus/iu/PicasaApi;->getUploadedPhotos$67a1bd15(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/iu/PhotoCollectorJson;)I

    move-result v2

    .line 75
    .local v2, result:I
    if-eqz v2, :cond_68

    .line 87
    .end local v2           #result:I
    :goto_67
    return v2

    .line 77
    .restart local v2       #result:I
    :cond_68
    iget v5, v1, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->entryCount:I

    if-eqz v5, :cond_73

    .line 79
    iget v5, v1, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->entryCount:I

    add-int/2addr v3, v5

    .line 82
    const/4 v5, 0x0

    aput-object v5, p2, v6

    goto :goto_4a

    .line 84
    :cond_73
    const-string v5, "PicasaAPI"

    const/4 v7, 0x3

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_92

    .line 85
    const-string v5, "PicasaAPI"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "total entry count="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_92
    move v2, v6

    .line 87
    goto :goto_67
.end method

.method public final setAuthToken(Ljava/lang/String;)V
    .registers 3
    .parameter "authToken"

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaApi;->mClient:Lcom/google/android/apps/plus/iu/GDataClient;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/iu/GDataClient;->setAuthToken(Ljava/lang/String;)V

    .line 51
    return-void
.end method
