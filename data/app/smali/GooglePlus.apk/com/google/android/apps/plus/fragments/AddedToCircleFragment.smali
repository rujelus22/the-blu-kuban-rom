.class public Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;
.super Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;
.source "AddedToCircleFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/AddedToCircleFragment$AddedToCircleQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

.field private mDataLoaded:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    return-void
.end method


# virtual methods
.method protected final getAdapter()Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    return-object v0
.end method

.method protected final getEmptyText()I
    .registers 2

    .prologue
    .line 174
    const v0, 0x7f0801a4

    return v0
.end method

.method protected final inflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "inflater"
    .parameter "container"

    .prologue
    .line 158
    const v0, 0x7f03007a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_22

    :cond_20
    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method protected final isError()Z
    .registers 2

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method protected final isLoaded()Z
    .registers 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    return v0
.end method

.method public final onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V
    .registers 5
    .parameter "view"
    .parameter "action"

    .prologue
    .line 207
    if-nez p2, :cond_a

    .line 208
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getPersonId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_a
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 10
    .parameter "activity"

    .prologue
    const/4 v2, 0x1

    .line 60
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onAttach(Landroid/app/Activity;)V

    .line 61
    new-instance v0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;-><init>(Landroid/content/Context;IIIIILcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->setShowAddButtonIfNeeded(Z)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    .line 70
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 10
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 97
    packed-switch p1, :pswitch_data_32

    .line 112
    :cond_4
    :goto_4
    return-object v4

    .line 99
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 104
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 105
    .local v2, extras:Landroid/os/Bundle;
    const-string v4, "notif_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 106
    .local v3, notificationId:Ljava/lang/String;
    const-string v4, "circle_action_data"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 107
    .local v1, data:[B
    new-instance v4, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbDataActor;->deserializeDataActorList([B)Ljava/util/List;

    move-result-object v6

    invoke-direct {v4, v0, v5, v3, v6}, Lcom/google/android/apps/plus/phone/AddedToCircleLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_4

    .line 97
    nop

    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 87
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->setAlwaysHideLetterSections(Z)V

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 89
    return-object v0
.end method

.method protected final onInitLoaders(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 121
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 192
    .local v0, cursor:Landroid/database/Cursor;
    if-nez v0, :cond_b

    .line 200
    :goto_a
    return-void

    .line 196
    :cond_b
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 197
    .local v2, personId:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 199
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_a
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 29
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_1a

    :goto_9
    return-void

    :pswitch_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mDataLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/AddedToCircleFragment;->updateView(Landroid/view/View;)V

    goto :goto_9

    :pswitch_data_1a
    .packed-switch 0x0
        :pswitch_a
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method
