.class public Lcom/google/android/apps/plus/views/SearchViewAdapter;
.super Ljava/lang/Object;
.source "SearchViewAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;
    }
.end annotation


# instance fields
.field protected final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mRequestFocus:Z

.field private mSearchView:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mRequestFocus:Z

    .line 67
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    .line 71
    check-cast p1, Landroid/widget/TextView;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    if-eqz v2, :cond_35

    .line 73
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 74
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 75
    .local v1, parent:Landroid/view/View;
    if-eqz v1, :cond_35

    .line 76
    const v2, 0x7f09004d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, button:Landroid/view/View;
    if-eqz v0, :cond_35

    .line 78
    new-instance v2, Lcom/google/android/apps/plus/views/SearchViewAdapter$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter$1;-><init>(Lcom/google/android/apps/plus/views/SearchViewAdapter;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    .end local v0           #button:Landroid/view/View;
    .end local v1           #parent:Landroid/view/View;
    :cond_35
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/SearchViewAdapter;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    return-object v0
.end method

.method public static createInstance(Landroid/view/View;)Lcom/google/android/apps/plus/views/SearchViewAdapter;
    .registers 3
    .parameter "view"

    .prologue
    .line 55
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_c

    .line 56
    new-instance v0, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;-><init>(Landroid/view/View;)V

    .line 60
    :goto_b
    return-object v0

    .line 57
    :cond_c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_18

    .line 58
    new-instance v0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;-><init>(Landroid/view/View;)V

    goto :goto_b

    .line 60
    :cond_18
    new-instance v0, Lcom/google/android/apps/plus/views/SearchViewAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;-><init>(Landroid/view/View;)V

    goto :goto_b
.end method


# virtual methods
.method public addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    .prologue
    .line 136
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 143
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 5
    .parameter "newText"

    .prologue
    .line 110
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;

    .line 111
    .local v1, listener:Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;->onQueryTextChanged(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 113
    .end local v1           #listener:Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;
    :cond_16
    const/4 v2, 0x0

    return v2
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 5
    .parameter "query"

    .prologue
    .line 117
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;

    .line 118
    .local v1, listener:Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;->onQueryTextSubmitted(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 120
    .end local v1           #listener:Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;
    :cond_16
    const/4 v2, 0x0

    return v2
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 6
    .parameter "query"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    .line 128
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->onQueryTextChange(Ljava/lang/String;)Z

    .line 129
    return-void
.end method

.method public requestFocus(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mRequestFocus:Z

    .line 92
    return-void
.end method

.method public setQueryHint(I)V
    .registers 3
    .parameter "hintResId"

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHint(I)V

    .line 96
    return-void
.end method

.method public setQueryText(Ljava/lang/String;)V
    .registers 3
    .parameter "queryText"

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mRequestFocus:Z

    if-eqz v0, :cond_e

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 103
    :cond_e
    return-void
.end method

.method public setVisible(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setVisible(ZLandroid/view/View;)V

    .line 147
    return-void
.end method

.method protected final setVisible(ZLandroid/view/View;)V
    .registers 6
    .parameter "flag"
    .parameter "searchView"

    .prologue
    .line 150
    if-eqz p2, :cond_13

    .line 151
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 152
    .local v1, oldVisibility:I
    if-eqz p1, :cond_14

    const/4 v0, 0x0

    .line 153
    .local v0, newVisibility:I
    :goto_9
    if-eq v1, v0, :cond_13

    .line 154
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 155
    if-eqz p1, :cond_17

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->showSoftInput()V

    .line 167
    .end local v0           #newVisibility:I
    .end local v1           #oldVisibility:I
    :cond_13
    :goto_13
    return-void

    .line 152
    .restart local v1       #oldVisibility:I
    :cond_14
    const/16 v0, 0x8

    goto :goto_9

    .line 158
    .restart local v0       #newVisibility:I
    :cond_17
    invoke-static {p2}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 160
    invoke-virtual {p2}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 162
    invoke-virtual {p2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    goto :goto_13
.end method

.method protected showSoftInput()V
    .registers 5

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/plus/views/SearchViewAdapter$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter$2;-><init>(Lcom/google/android/apps/plus/views/SearchViewAdapter;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 181
    return-void
.end method
