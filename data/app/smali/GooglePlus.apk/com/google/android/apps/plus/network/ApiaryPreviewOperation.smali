.class public final Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "ApiaryPreviewOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/LinkPreviewRequest;",
        "Lcom/google/api/services/plusi/model/LinkPreviewResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

.field private final mSourceUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "sourceUrl"
    .parameter "apiInfo"

    .prologue
    .line 36
    const-string v10, "linkpreview"

    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;

    move-result-object v11

    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v1, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;

    const-string v4, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite"

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v10

    move-object v6, v11

    move-object v10, v1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    .line 40
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->mSourceUrl:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public final getActivity()Lcom/google/android/apps/plus/network/ApiaryActivity;
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p1, Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    .end local p1
    if-eqz p1, :cond_a

    invoke-static {p1}, Lcom/google/android/apps/plus/network/ApiaryActivityFactory;->getApiaryActivity(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->mActivity:Lcom/google/android/apps/plus/network/ApiaryActivity;

    :cond_a
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"

    .prologue
    const/4 v1, 0x1

    .line 20
    check-cast p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->mSourceUrl:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->content:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->useBlackboxPreviewData:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->fallbackToUrl:Ljava/lang/Boolean;

    return-void
.end method
