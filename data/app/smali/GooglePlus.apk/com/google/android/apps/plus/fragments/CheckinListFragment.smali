.class public Lcom/google/android/apps/plus/fragments/CheckinListFragment;
.super Lcom/google/android/apps/plus/fragments/EsListFragment;
.source "CheckinListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;,
        Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;,
        Lcom/google/android/apps/plus/fragments/CheckinListFragment$ServiceListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsListFragment",
        "<",
        "Landroid/widget/ListView;",
        "Lcom/google/android/apps/plus/phone/PlacesAdapter;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;"
    }
.end annotation


# static fields
.field private static final ITEM_NO_LOCATION:Ljava/lang/Object;


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

.field private mLocationListener:Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;

.field private mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

.field private mOnUpdateMenuListener:Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;

.field private mQuery:Ljava/lang/String;

.field private mSearchBar:Landroid/view/View;

.field private mSearchMode:Z

.field private mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->ITEM_NO_LOCATION:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;-><init>()V

    .line 64
    new-instance v0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$ServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/CheckinListFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 101
    new-instance v0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/CheckinListFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationListener:Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;

    .line 151
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)Lcom/google/android/apps/plus/phone/LocationController;
    .registers 2
    .parameter "x0"

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->removeLocationListener()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)Lcom/google/android/apps/plus/api/LocationQuery;
    .registers 2
    .parameter "x0"

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/fragments/CheckinListFragment;Lcom/google/android/apps/plus/api/LocationQuery;)Lcom/google/android/apps/plus/api/LocationQuery;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/CheckinListFragment;Landroid/view/View;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method private doSearch()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 454
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v0, :cond_44

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_45

    .line 457
    new-instance v0, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    .line 462
    :goto_1c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 465
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080096

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    .line 468
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 470
    :cond_44
    return-void

    .line 459
    :cond_45
    new-instance v0, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    goto :goto_1c
.end method

.method private isSearchWithNoEntry()Z
    .registers 2

    .prologue
    .line 551
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchMode:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private removeLocationListener()V
    .registers 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-eqz v0, :cond_c

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->release()V

    .line 515
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    .line 517
    :cond_c
    return-void
.end method

.method private setupAndShowEmptyView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    .prologue
    .line 539
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->isSearchWithNoEntry()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 540
    const v0, 0x7f08016e

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 544
    :goto_c
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showEmptyView(Landroid/view/View;)V

    .line 545
    return-void

    .line 542
    :cond_10
    const v0, 0x7f080166

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setupEmptyView(Landroid/view/View;I)V

    goto :goto_c
.end method

.method private showPlacesOnly()Z
    .registers 4

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "places_only"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private showProgress(Landroid/view/View;Ljava/lang/String;)V
    .registers 4
    .parameter "view"
    .parameter "text"

    .prologue
    .line 526
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->isSearchWithNoEntry()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 527
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setupAndShowEmptyView(Landroid/view/View;)V

    .line 531
    :goto_9
    return-void

    .line 529
    :cond_a
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_9
.end method


# virtual methods
.method public final getLocationData()Lcom/google/android/apps/plus/content/DbLocation;
    .registers 4

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-nez v0, :cond_6

    .line 445
    const/4 v0, 0x0

    .line 447
    :goto_5
    return-object v0

    :cond_6
    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILandroid/location/Location;)V

    goto :goto_5
.end method

.method public final hasLocation()Z
    .registers 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    .line 160
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 164
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 166
    if-eqz p1, :cond_3f

    .line 167
    const-string v2, "location"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/LocationQuery;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    .line 168
    const-string v2, "search_mode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchMode:Z

    .line 169
    const-string v2, "query"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;

    .line 170
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v2, :cond_3e

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 180
    :cond_3e
    :goto_3e
    return-void

    .line 175
    :cond_3f
    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3e

    .line 176
    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/DbLocation;

    .line 177
    .local v1, location:Lcom/google/android/apps/plus/content/DbLocation;
    new-instance v2, Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbLocation;->getAndroidLocation()Landroid/location/Location;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    goto :goto_3e
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 11
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 311
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->isSearchWithNoEntry()Z

    move-result v0

    if-eqz v0, :cond_24

    const-string v7, "no_location_stream_key"

    .line 313
    .local v7, queryKey:Ljava/lang/String;
    :goto_9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/content/EsProvider;->buildLocationQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 314
    .local v2, uri:Landroid/net/Uri;
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showPlacesOnly()Z

    move-result v0

    if-eqz v0, :cond_2b

    const-string v4, "name IS NOT NULL"

    .line 315
    .local v4, where:Ljava/lang/String;
    :goto_17
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/phone/PlacesAdapter$LocationQuery;->PROJECTION:[Ljava/lang/String;

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 311
    .end local v2           #uri:Landroid/net/Uri;
    .end local v4           #where:Ljava/lang/String;
    .end local v7           #queryKey:Ljava/lang/String;
    :cond_24
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->getKey()Ljava/lang/String;

    move-result-object v7

    goto :goto_9

    .restart local v2       #uri:Landroid/net/Uri;
    .restart local v7       #queryKey:Ljava/lang/String;
    :cond_2b
    move-object v4, v5

    .line 314
    goto :goto_17
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 14
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v7, 0x0

    .line 201
    const v6, 0x7f030010

    invoke-super {p0, p1, p2, p3, v6}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v4

    .line 204
    .local v4, view:Landroid/view/View;
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showPlacesOnly()Z

    move-result v6

    if-nez v6, :cond_5a

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchMode:Z

    if-nez v6, :cond_5a

    .line 205
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v8, 0x7f03005a

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v6, v8, v9, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 207
    .local v0, header:Landroid/view/View;
    const v6, 0x1020006

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 208
    .local v2, icon:Landroid/widget/ImageView;
    const v6, 0x1020016

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 209
    .local v3, title:Landroid/widget/TextView;
    const v6, 0x1020005

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 210
    .local v1, hint:Landroid/widget/TextView;
    const v6, 0x7f0200cd

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 211
    const v6, 0x7f080191

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 212
    const v6, 0x7f080192

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 213
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v6, Landroid/widget/ListView;

    sget-object v8, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->ITEM_NO_LOCATION:Ljava/lang/Object;

    const/4 v9, 0x1

    invoke-virtual {v6, v0, v8, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 217
    .end local v0           #header:Landroid/view/View;
    .end local v1           #hint:Landroid/widget/TextView;
    .end local v2           #icon:Landroid/widget/ImageView;
    .end local v3           #title:Landroid/widget/TextView;
    :cond_5a
    new-instance v6, Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-direct {v6, v8}, Lcom/google/android/apps/plus/phone/PlacesAdapter;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    .line 221
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v6, Landroid/widget/ListView;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v6, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 224
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v6, Landroid/widget/ListView;

    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 228
    const v6, 0x7f09005f

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchBar:Landroid/view/View;

    .line 229
    if-eqz p3, :cond_8e

    .line 230
    const-string v6, "search_bar_visible"

    invoke-virtual {p3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 231
    .local v5, visible:Z
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchBar:Landroid/view/View;

    if-eqz v5, :cond_b4

    move v6, v7

    :goto_8b
    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 235
    .end local v5           #visible:Z
    :cond_8e
    const v6, 0x7f09004c

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->createInstance(Landroid/view/View;)Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    .line 237
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    const v7, 0x7f0801ce

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryHint(I)V

    .line 238
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    invoke-virtual {v6, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    .line 240
    const v6, 0x7f080166

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 243
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchMode:Z

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setSearchMode(Z)V

    .line 244
    return-object v4

    .line 231
    .restart local v5       #visible:Z
    :cond_b4
    const/16 v6, 0x8

    goto :goto_8b
.end method

.method public final bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic onDestroyView()V
    .registers 1

    .prologue
    .line 48
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onDestroyView()V

    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 349
    const/4 v0, 0x3

    if-ne p2, v0, :cond_8

    .line 350
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->doSearch()V

    .line 351
    const/4 v0, 0x1

    .line 353
    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 14
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 362
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    .line 363
    .local v3, item:Ljava/lang/Object;
    if-nez v3, :cond_7

    .line 389
    :cond_6
    :goto_6
    return-void

    .line 367
    :cond_7
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v6, Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 368
    .local v1, cursor:Landroid/database/Cursor;
    sget-object v6, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->ITEM_NO_LOCATION:Ljava/lang/Object;

    if-ne v3, v6, :cond_43

    .line 369
    const/4 v4, 0x0

    .line 376
    .local v4, location:Lcom/google/android/apps/plus/content/DbLocation;
    :goto_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 377
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 378
    .local v2, intent:Landroid/content/Intent;
    const-string v6, "android.intent.action.PICK"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_31

    .line 379
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v6, v4}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->startActivity(Landroid/content/Intent;)V

    .line 384
    :cond_31
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 385
    .local v5, result:Landroid/content/Intent;
    const-string v6, "location"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 386
    const/4 v6, -0x1

    invoke-virtual {v0, v6, v5}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 388
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_6

    .line 370
    .end local v0           #activity:Landroid/app/Activity;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v4           #location:Lcom/google/android/apps/plus/content/DbLocation;
    .end local v5           #result:Landroid/content/Intent;
    :cond_43
    if-ne v3, v1, :cond_6

    .line 371
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getLocation(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v4

    .restart local v4       #location:Lcom/google/android/apps/plus/content/DbLocation;
    goto :goto_14
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 48
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    if-eqz p2, :cond_22

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_22

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showContent(Landroid/view/View;)V

    :goto_18
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mOnUpdateMenuListener:Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mOnUpdateMenuListener:Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;->onUpdateMenu()V

    :cond_21
    return-void

    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_35

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080096

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_18

    :cond_35
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setupAndShowEmptyView(Landroid/view/View;)V

    goto :goto_18
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 342
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 283
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onPause()V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 286
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->removeLocationListener()V

    .line 287
    return-void
.end method

.method public final onQueryClose()V
    .registers 3

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryText(Ljava/lang/String;)V

    .line 414
    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "query"

    .prologue
    .line 396
    if-nez p1, :cond_9

    const/4 v0, 0x0

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;

    .line 398
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->doSearch()V

    .line 399
    return-void

    .line 396
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "query"

    .prologue
    .line 406
    return-void
.end method

.method public final onResume()V
    .registers 11

    .prologue
    const v9, 0x7f080096

    .line 252
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onResume()V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 256
    .local v8, intent:Landroid/content/Intent;
    const-string v0, "location"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PlacesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_42

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 276
    :cond_42
    :goto_42
    return-void

    .line 265
    :cond_43
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-nez v0, :cond_63

    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x1

    const-wide/16 v4, 0xbb8

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v6, :cond_85

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v6

    :goto_5c
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationListener:Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/LocationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_63
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v0

    if-nez v0, :cond_87

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x1bfb7a8

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->showDialog(I)V

    .line 267
    :goto_75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v0

    if-nez v0, :cond_8d

    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setupAndShowEmptyView(Landroid/view/View;)V

    goto :goto_42

    .line 265
    :cond_85
    const/4 v6, 0x0

    goto :goto_5c

    :cond_87
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->init()V

    goto :goto_75

    .line 269
    :cond_8d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-nez v0, :cond_a0

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08016c

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_42

    .line 273
    :cond_a0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_42
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 294
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v0, :cond_15

    .line 297
    const-string v0, "location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 298
    const-string v0, "search_mode"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 301
    :cond_15
    const-string v1, "search_bar_visible"

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2b

    const/4 v0, 0x1

    :goto_20
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 303
    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    return-void

    .line 301
    :cond_2b
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public final setOnUpdateMenuListener(Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;)V
    .registers 2
    .parameter "onUpdateMenuListener"

    .prologue
    .line 478
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mOnUpdateMenuListener:Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;

    .line 479
    return-void
.end method

.method public final setSearchMode(Z)V
    .registers 4
    .parameter "searchMode"

    .prologue
    .line 422
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchMode:Z

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchBar:Landroid/view/View;

    if-eqz v0, :cond_e

    .line 425
    if-eqz p1, :cond_f

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 431
    :cond_e
    :goto_e
    return-void

    .line 428
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mSearchBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e
.end method
