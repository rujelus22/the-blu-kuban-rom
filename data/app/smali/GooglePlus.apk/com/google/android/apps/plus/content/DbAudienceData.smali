.class public final Lcom/google/android/apps/plus/content/DbAudienceData;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbAudienceData.java"


# direct methods
.method public static deserialize([B)Lcom/google/android/apps/plus/content/AudienceData;
    .registers 13
    .parameter "bytes"

    .prologue
    .line 53
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 55
    .local v0, bb:Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    .line 56
    .local v4, numPersons:I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    .local v5, personList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    const/4 v2, 0x0

    .local v2, i:I
    :goto_e
    if-ge v2, v4, :cond_2b

    .line 58
    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v11, v7, v8, v9, v10}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    .line 61
    :cond_2b
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    .line 62
    .local v3, numCircles:I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 63
    .local v1, circleList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/CircleData;>;"
    const/4 v2, 0x0

    :goto_35
    if-ge v2, v3, :cond_52

    .line 64
    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v9

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    new-instance v11, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v11, v7, v9, v8, v10}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v2, v2, 0x1

    goto :goto_35

    .line 67
    :cond_52
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 68
    .local v6, totalPersonCount:I
    new-instance v7, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v7, v5, v1, v6}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;I)V

    return-object v7
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/AudienceData;)[B
    .registers 11
    .parameter "audienceData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 28
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 30
    .local v3, dos:Ljava/io/DataOutputStream;
    :try_start_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_17
    if-ge v4, v5, :cond_3a

    aget-object v6, v0, v4

    .line 32
    .local v6, person:Lcom/google/android/apps/plus/content/PersonData;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getCompressedPhotoUrl()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 31
    add-int/lit8 v4, v4, 0x1

    goto :goto_17

    .line 35
    .end local v6           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_3a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v5, v0

    const/4 v4, 0x0

    :goto_47
    if-ge v4, v5, :cond_6a

    aget-object v2, v0, v4

    .line 37
    .local v2, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/content/DbAudienceData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 36
    add-int/lit8 v4, v4, 0x1

    goto :goto_47

    .line 40
    .end local v2           #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_6a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v9

    add-int v7, v8, v9

    .line 42
    .local v7, totalPersonCount:I
    invoke-virtual {v3, v7}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_77
    .catchall {:try_start_a .. :try_end_77} :catchall_7f

    .line 44
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    .line 46
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    return-object v8

    .line 44
    .end local v0           #arr$:[Lcom/google/android/apps/plus/content/CircleData;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v7           #totalPersonCount:I
    :catchall_7f
    move-exception v8

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    throw v8
.end method
