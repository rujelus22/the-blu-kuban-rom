.class public final Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetNearbyActivitiesOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/NearbyStreamRequest;",
        "Lcom/google/api/services/plusi/model/NearbyStreamResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContinuationToken:Ljava/lang/String;

.field private final mLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private final mMaxCount:I

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ILcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "location"
    .parameter "continuationToken"
    .parameter "maxCount"
    .parameter "syncState"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 39
    const-string v3, "nearbystream"

    invoke-static {}, Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;->getInstance()Lcom/google/api/services/plusi/model/NearbyStreamRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/NearbyStreamResponseJson;->getInstance()Lcom/google/api/services/plusi/model/NearbyStreamResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 41
    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 42
    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    .line 43
    if-lez p5, :cond_1e

    .end local p5
    :goto_19
    iput p5, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mMaxCount:I

    .line 44
    iput-object p6, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 45
    return-void

    .line 43
    .restart local p5
    :cond_1e
    const/16 p5, 0xa

    goto :goto_19
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 10
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/NearbyStreamResponse;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v3, v3, v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/NearbyStreamResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    const-string v4, "DEFAULT"

    iget-object v5, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/NearbyStreamResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Stream;->continuationToken:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsPostsData;->updateStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;

    .end local p1
    new-instance v0, Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->latLongE7:Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->latLongE7:Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbLocation;->getLatitudeE7()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;->latitude:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->latLongE7:Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbLocation;->getLongitudeE7()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NearbyStreamRequestLatLongE7;->longitude:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->continuationToken:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->mMaxCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->maxResults:Ljava/lang/Integer;

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityFilters;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityFilters;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    new-instance v1, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/FieldRequestOptions;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeLegacyMediaData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    new-instance v1, Lcom/google/api/services/plusi/model/UpdateFilter;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/UpdateFilter;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/NearbyStreamRequest;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamNamespaces(Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/UpdateFilter;->includeNamespace:Ljava/util/List;

    return-void
.end method
