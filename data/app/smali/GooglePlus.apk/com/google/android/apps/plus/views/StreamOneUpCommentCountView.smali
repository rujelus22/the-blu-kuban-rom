.class public Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;
.super Lcom/google/android/apps/plus/views/OneUpBaseView;
.source "StreamOneUpCommentCountView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sCountMarginLeft:I

.field private static sCountPaint:Landroid/text/TextPaint;

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sDividerWidth:I

.field private static sMarginLeft:I

.field private static sMarginRight:I


# instance fields
.field private mContentDescriptionDirty:Z

.field private mCount:Ljava/lang/String;

.field private mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mDivider:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    .prologue
    const v3, 0x7f0d0191

    const/4 v2, 0x1

    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    .line 42
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    .line 57
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_9c

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    .line 62
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    .line 63
    const v1, 0x7f0d017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    .line 65
    const v1, 0x7f0d017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    .line 68
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 69
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 70
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 71
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 73
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 76
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 77
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 80
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 81
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00ed

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 46
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_9c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v3, 0x7f0d0191

    const/4 v2, 0x1

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    .line 42
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    .line 57
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_9c

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    .line 62
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    .line 63
    const v1, 0x7f0d017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    .line 65
    const v1, 0x7f0d017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    .line 68
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 69
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 70
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 71
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 73
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 76
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 77
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 80
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 81
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00ed

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 50
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_9c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v3, 0x7f0d0191

    const/4 v2, 0x1

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    .line 42
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    .line 57
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_9c

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    .line 62
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    .line 63
    const v1, 0x7f0d017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    .line 65
    const v1, 0x7f0d017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    .line 68
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 69
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 70
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 71
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 73
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 76
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 77
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 80
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 81
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00ed

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 54
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_9c
    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 191
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setCount(I)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->invalidate()V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->requestLayout()V

    .line 194
    return-void
.end method

.method public invalidate()V
    .registers 2

    .prologue
    .line 179
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->invalidate()V

    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    if-eqz v0, :cond_f

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    .line 185
    :cond_f
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter "canvas"

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDraw(Landroid/graphics/Canvas;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getWidth()I

    move-result v7

    .line 111
    .local v7, width:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getHeight()I

    move-result v6

    .line 113
    .local v6, height:I
    int-to-float v3, v7

    int-to-float v4, v6

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_4c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 116
    :cond_4c
    return-void
.end method

.method protected onMeasure(II)V
    .registers 15
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 89
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onMeasure(II)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    add-int v10, v0, v1

    .line 92
    .local v10, xStart:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getPaddingTop()I

    move-result v11

    .line 94
    .local v11, yStart:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getMeasuredWidth()I

    move-result v9

    .line 95
    .local v9, width:I
    sub-int v0, v9, v10

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    sub-int v8, v0, v1

    .line 97
    .local v8, contentWidth:I
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v3, v0

    new-instance v0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f80

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0, v10, v11}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    add-int/2addr v1, v3

    add-int/2addr v1, v10

    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int v0, v2, v0

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v11

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    int-to-float v1, v1

    int-to-float v3, v0

    add-int v4, v10, v8

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    int-to-float v0, v0

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v11, v0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getPaddingBottom()I

    move-result v0

    add-int/2addr v0, v11

    invoke-virtual {p0, v9, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setMeasuredDimension(II)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v0, :cond_74

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    .line 104
    :cond_74
    return-void
.end method

.method public onRecycle()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 171
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 174
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    .line 175
    return-void
.end method

.method public setCount(I)V
    .registers 9
    .parameter "count"

    .prologue
    const/4 v6, 0x1

    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 201
    .local v1, res:Landroid/content/res/Resources;
    const v2, 0x7f0e0023

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, countString:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    .line 205
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 206
    iput-boolean v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    .line 207
    return-void
.end method
