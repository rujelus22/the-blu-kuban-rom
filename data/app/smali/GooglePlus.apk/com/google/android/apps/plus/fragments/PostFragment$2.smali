.class final Lcom/google/android/apps/plus/fragments/PostFragment$2;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 347
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 3
    .parameter "s"

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    .line 382
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 350
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 15
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    .line 354
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    if-nez v7, :cond_9

    .line 376
    :cond_8
    :goto_8
    return-void

    .line 357
    :cond_9
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    .line 358
    .local v0, cursor:I
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1200(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/util/MentionTokenizer;

    move-result-object v7

    invoke-virtual {v7, p1, v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v8}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getThreshold()I

    move-result v8

    add-int/2addr v7, v8

    if-gt v7, v0, :cond_8

    .line 361
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d019d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v3, v7

    .line 363
    .local v3, minHeight:I
    const/4 v7, 0x2

    new-array v2, v7, [I

    .line 364
    .local v2, location:[I
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getLocationOnScreen([I)V

    .line 365
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 366
    .local v6, windowRect:Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 368
    const/4 v7, 0x1

    aget v4, v2, v7

    .line 369
    .local v4, viewScreenY:I
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getCursorYPosition()I

    move-result v7

    add-int v1, v4, v7

    .line 370
    .local v1, cursorScreenY:I
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v5

    .line 372
    .local v5, windowHeight:I
    sub-int v7, v5, v1

    if-ge v7, v3, :cond_8

    .line 373
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1300(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/widget/ScrollView;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v9}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getCursorYTop()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_8
.end method
