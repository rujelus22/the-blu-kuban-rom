.class public final Lcom/google/android/apps/plus/api/PhotosOfUserOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PhotosOfUserOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosOfUserRequest;",
        "Lcom/google/api/services/plusi/model/PhotosOfUserResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCoverOnly:Z

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private final mUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "userId"
    .parameter "coverOnly"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 41
    const-string v3, "photosofuser"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosOfUserResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 44
    iput-object p3, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 45
    iput-object p4, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mUserId:Ljava/lang/String;

    .line 46
    iput-boolean p5, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mCoverOnly:Z

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "userId"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 36
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 8
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->onStartResultProcessing()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mUserId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2f

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->approvedPhoto:Ljava/util/List;

    if-eqz v0, :cond_43

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->approvedPhoto:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_43

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->approvedPhoto:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPhoto;

    :goto_28
    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotosOfYouCover(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPhoto;)V

    :cond_2f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mCoverOnly:Z

    if-nez v0, :cond_42

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->approvedPhoto:Ljava/util/List;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->unapprovedPhoto:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mUserId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertUserPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    :cond_42
    return-void

    :cond_43
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->unapprovedPhoto:Ljava/util/List;

    if-eqz v0, :cond_58

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->unapprovedPhoto:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_58

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserResponse;->unapprovedPhoto:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPhoto;

    goto :goto_28

    :cond_58
    const/4 v0, 0x0

    goto :goto_28
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mUserId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->ownerId:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;-><init>()V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnAlbumInfo:Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnComments:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnDownloadability:Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnOwnerInfo:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPhotos:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPlusOnes:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnShapes:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnVideoUrls:Ljava/lang/Boolean;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;->mCoverOnly:Z

    if-eqz v0, :cond_49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->maxResults:Ljava/lang/Integer;

    :cond_49
    return-void
.end method
