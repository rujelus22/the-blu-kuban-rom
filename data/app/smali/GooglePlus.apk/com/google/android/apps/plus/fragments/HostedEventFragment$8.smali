.class final Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;
.super Landroid/os/AsyncTask;
.source "HostedEventFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field currentEventId:Ljava/lang/String;

.field skippedPhotoIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1130
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->skippedPhotoIds:Ljava/util/List;

    .line 1132
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 1133
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->currentEventId:Ljava/lang/String;

    return-void
.end method

.method private varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .registers 17
    .parameter "params"

    .prologue
    .line 1137
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1138
    .local v6, accountName:Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v10, p1, v2

    .line 1140
    .local v10, localUrls:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1141
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v13, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->uploadsUri:Landroid/net/Uri;

    .line 1142
    .local v13, uploadUri:Landroid/net/Uri;
    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->photosUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v4, v4, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1146
    .local v1, photosUri:Landroid/net/Uri;
    new-instance v12, Ljava/util/StringTokenizer;

    invoke-direct {v12, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 1147
    .local v12, tokenizer:Ljava/util/StringTokenizer;
    new-instance v11, Ljava/util/LinkedHashSet;

    invoke-direct {v11}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1149
    .local v11, photos:Ljava/util/LinkedHashSet;,"Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :goto_33
    invoke-virtual {v12}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_41

    .line 1150
    invoke-virtual {v12}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 1151
    .local v9, localUrl:Ljava/lang/String;
    invoke-virtual {v11, v9}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_33

    .line 1155
    .end local v9           #localUrl:Ljava/lang/String;
    :cond_41
    const/4 v7, 0x0

    .line 1157
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_42
    invoke-virtual {v11}, Ljava/util/LinkedHashSet;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v11, v2}, Ljava/util/LinkedHashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1160
    :goto_55
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_ba

    .line 1161
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 1162
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->skippedPhotoIds:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_71
    .catchall {:try_start_42 .. :try_end_71} :catchall_c0
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_71} :catch_72

    goto :goto_55

    .line 1167
    :catch_72
    move-exception v2

    if-eqz v7, :cond_78

    .line 1168
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1172
    :cond_78
    :goto_78
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 1174
    .local v14, values:Landroid/content/ContentValues;
    invoke-virtual {v11}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, i$:Ljava/util/Iterator;
    :goto_81
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_cf

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1175
    .restart local v9       #localUrl:Ljava/lang/String;
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 1176
    const-string v2, "account"

    invoke-virtual {v14, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    const-string v2, "content_uri"

    invoke-virtual {v14, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    const-string v2, "event_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->currentEventId:Ljava/lang/String;

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->currentEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c7

    .line 1182
    const-string v2, "album_id"

    const-string v3, "camera-sync"

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    :goto_b6
    invoke-virtual {v0, v13, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_81

    .line 1167
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v9           #localUrl:Ljava/lang/String;
    .end local v14           #values:Landroid/content/ContentValues;
    :cond_ba
    if-eqz v7, :cond_78

    .line 1168
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_78

    .line 1167
    :catchall_c0
    move-exception v2

    if-eqz v7, :cond_c6

    .line 1168
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_c6
    throw v2

    .line 1184
    .restart local v8       #i$:Ljava/util/Iterator;
    .restart local v9       #localUrl:Ljava/lang/String;
    .restart local v14       #values:Landroid/content/ContentValues;
    :cond_c7
    const-string v2, "album_id"

    const-string v3, "events"

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b6

    .line 1189
    .end local v9           #localUrl:Ljava/lang/String;
    :cond_cf
    const/4 v2, 0x0

    return-object v2
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 1130
    check-cast p1, [Ljava/lang/String;

    .end local p1
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 6
    .parameter "x0"

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->skippedPhotoIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->currentEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->skippedPhotoIds:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->sharePhotosToEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I

    :cond_13
    return-void
.end method
