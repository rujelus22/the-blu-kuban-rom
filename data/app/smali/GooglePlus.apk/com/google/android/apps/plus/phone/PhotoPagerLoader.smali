.class public final Lcom/google/android/apps/plus/phone/PhotoPagerLoader;
.super Lcom/google/android/apps/plus/phone/PhotoCursorLoader;
.source "PhotoPagerLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;
    }
.end annotation


# instance fields
.field private final mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private final mPhotoUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 23
    .parameter "context"
    .parameter "account"
    .parameter "ownerGaiaId"
    .parameter "mediaRefs"
    .parameter "albumId"
    .parameter "photoOfUserGaiaId"
    .parameter "streamId"
    .parameter "eventId"
    .parameter "photoUrl"
    .parameter "pageHint"

    .prologue
    .line 49
    const/4 v1, -0x1

    move/from16 v0, p10

    if-eq v0, v1, :cond_24

    const/4 v10, 0x1

    :goto_6
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    .line 51
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mOwnerGaiaId:Ljava/lang/String;

    .line 52
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    .line 53
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mPhotoUrl:Ljava/lang/String;

    .line 54
    return-void

    .line 49
    :cond_24
    const/4 v10, 0x0

    goto :goto_6
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 9

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 58
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v3, :cond_6e

    .line 62
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v4, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_f
    array-length v4, v3

    if-ge v2, v4, :cond_94

    aget-object v4, v3, v2

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_40

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    aget-object v5, v3, v2

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    aget-object v5, v3, v2

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    :goto_3d
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    :cond_40
    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    aget-object v5, v3, v2

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    aget-object v5, v3, v2

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    aget-object v5, v3, v2

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_3d

    .line 66
    :cond_6e
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mPhotoUrl:Ljava/lang/String;

    if-eqz v3, :cond_95

    .line 67
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mPhotoUrl:Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v4, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    .line 76
    :cond_94
    :goto_94
    return-object v1

    .line 70
    :cond_95
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->getLoaderUri()Landroid/net/Uri;

    move-result-object v0

    .line 72
    .local v0, loaderUri:Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->setUri(Landroid/net/Uri;)V

    .line 73
    sget-object v2, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->setProjection([Ljava/lang/String;)V

    .line 74
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v1

    .line 76
    .local v1, returnCursor:Landroid/database/Cursor;
    goto :goto_94
.end method
