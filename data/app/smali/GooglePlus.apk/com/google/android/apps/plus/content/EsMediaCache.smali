.class public final Lcom/google/android/apps/plus/content/EsMediaCache;
.super Ljava/lang/Object;
.source "EsMediaCache.java"


# static fields
.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sMediaCacheDir:Ljava/io/File;


# direct methods
.method public static getMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;)[B
    .registers 18
    .parameter "context"
    .parameter "request"

    .prologue
    .line 105
    new-instance v5, Ljava/io/File;

    new-instance v12, Ljava/io/File;

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/CachedImageRequest;->getCacheDir()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/CachedImageRequest;->getCacheFileName()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 107
    .local v5, file:Ljava/io/File;
    const/4 v10, 0x0

    .line 110
    .local v10, stream:Ljava/io/FileInputStream;
    :try_start_17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 111
    .local v6, now:J
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v12

    sub-long v12, v6, v12

    const-wide/16 v14, 0x2710

    cmp-long v12, v12, v14

    if-lez v12, :cond_2a

    .line 112
    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setLastModified(J)Z

    .line 115
    :cond_2a
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2f
    .catchall {:try_start_17 .. :try_end_2f} :catchall_83
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_2f} :catch_98
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_2f} :catch_57

    .line 116
    .end local v10           #stream:Ljava/io/FileInputStream;
    .local v11, stream:Ljava/io/FileInputStream;
    :try_start_2f
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v12

    long-to-int v9, v12

    .line 117
    .local v9, size:I
    new-array v2, v9, [B

    .line 118
    .local v2, bytes:[B
    const/4 v8, 0x0

    .line 119
    .local v8, offset:I
    move v1, v9

    .line 120
    .local v1, byteCount:I
    :goto_38
    if-lez v1, :cond_52

    .line 121
    invoke-virtual {v11, v2, v8, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3

    .line 122
    .local v3, bytesRead:I
    if-gez v3, :cond_4f

    .line 123
    new-instance v12, Ljava/io/IOException;

    invoke-direct {v12}, Ljava/io/IOException;-><init>()V

    throw v12
    :try_end_46
    .catchall {:try_start_2f .. :try_end_46} :catchall_92
    .catch Ljava/io/FileNotFoundException; {:try_start_2f .. :try_end_46} :catch_46
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_46} :catch_95

    .line 129
    .end local v1           #byteCount:I
    .end local v2           #bytes:[B
    .end local v3           #bytesRead:I
    .end local v8           #offset:I
    .end local v9           #size:I
    :catch_46
    move-exception v4

    move-object v10, v11

    .line 131
    .end local v6           #now:J
    .end local v11           #stream:Ljava/io/FileInputStream;
    .local v4, e:Ljava/io/FileNotFoundException;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :goto_48
    if-eqz v10, :cond_4d

    .line 137
    :try_start_4a
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_8c

    .line 140
    :cond_4d
    :goto_4d
    const/4 v2, 0x0

    .line 144
    .end local v4           #e:Ljava/io/FileNotFoundException;
    :goto_4e
    return-object v2

    .line 125
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v1       #byteCount:I
    .restart local v2       #bytes:[B
    .restart local v3       #bytesRead:I
    .restart local v6       #now:J
    .restart local v8       #offset:I
    .restart local v9       #size:I
    .restart local v11       #stream:Ljava/io/FileInputStream;
    :cond_4f
    add-int/2addr v8, v3

    .line 126
    sub-int/2addr v1, v3

    .line 127
    goto :goto_38

    .line 135
    .end local v3           #bytesRead:I
    :cond_52
    :try_start_52
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_55} :catch_8a

    :goto_55
    move-object v10, v11

    .line 140
    .end local v11           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    goto :goto_4e

    .line 132
    .end local v1           #byteCount:I
    .end local v2           #bytes:[B
    .end local v6           #now:J
    .end local v8           #offset:I
    .end local v9           #size:I
    :catch_57
    move-exception v4

    .line 133
    .local v4, e:Ljava/io/IOException;
    :goto_58
    :try_start_58
    const-string v12, "EsMediaCache"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Cannot read media file from cache: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7c
    .catchall {:try_start_58 .. :try_end_7c} :catchall_83

    .line 135
    if-eqz v10, :cond_81

    .line 137
    :try_start_7e
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_81
    .catch Ljava/io/IOException; {:try_start_7e .. :try_end_81} :catch_8e

    .line 144
    :cond_81
    :goto_81
    const/4 v2, 0x0

    goto :goto_4e

    .line 135
    .end local v4           #e:Ljava/io/IOException;
    :catchall_83
    move-exception v12

    :goto_84
    if-eqz v10, :cond_89

    .line 137
    :try_start_86
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_89
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_89} :catch_90

    .line 140
    :cond_89
    :goto_89
    throw v12

    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v1       #byteCount:I
    .restart local v2       #bytes:[B
    .restart local v6       #now:J
    .restart local v8       #offset:I
    .restart local v9       #size:I
    .restart local v11       #stream:Ljava/io/FileInputStream;
    :catch_8a
    move-exception v12

    goto :goto_55

    .end local v1           #byteCount:I
    .end local v2           #bytes:[B
    .end local v6           #now:J
    .end local v8           #offset:I
    .end local v9           #size:I
    .end local v11           #stream:Ljava/io/FileInputStream;
    .local v4, e:Ljava/io/FileNotFoundException;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :catch_8c
    move-exception v12

    goto :goto_4d

    .local v4, e:Ljava/io/IOException;
    :catch_8e
    move-exception v12

    goto :goto_81

    .end local v4           #e:Ljava/io/IOException;
    :catch_90
    move-exception v13

    goto :goto_89

    .line 135
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v6       #now:J
    .restart local v11       #stream:Ljava/io/FileInputStream;
    :catchall_92
    move-exception v12

    move-object v10, v11

    .end local v11           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    goto :goto_84

    .line 132
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v11       #stream:Ljava/io/FileInputStream;
    :catch_95
    move-exception v4

    move-object v10, v11

    .end local v11           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    goto :goto_58

    .line 129
    .end local v6           #now:J
    :catch_98
    move-exception v4

    goto :goto_48
.end method

.method private static getMediaCacheDir(Landroid/content/Context;)Ljava/io/File;
    .registers 5
    .parameter "context"

    .prologue
    .line 209
    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    if-nez v1, :cond_11

    .line 210
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "media"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    .line 213
    :cond_11
    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1e

    .line 215
    :try_start_19
    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_1e} :catch_21

    .line 221
    :cond_1e
    :goto_1e
    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    return-object v1

    .line 216
    :catch_21
    move-exception v0

    .line 217
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "EsMediaCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot create cache media directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1e
.end method

.method public static insertMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;[B)V
    .registers 10
    .parameter "context"
    .parameter "request"
    .parameter "bytes"

    .prologue
    .line 67
    sget-object v4, Lcom/google/android/apps/plus/content/EsMediaCache;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    if-nez v4, :cond_a

    .line 68
    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/content/EsMediaCache;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 71
    :cond_a
    if-eqz p2, :cond_36

    .line 72
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CachedImageRequest;->getCacheDir()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 73
    .local v0, dir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_22

    .line 75
    :try_start_1f
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_22} :catch_3c

    .line 81
    :cond_22
    :goto_22
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CachedImageRequest;->getCacheFileName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 83
    .local v2, file:Ljava/io/File;
    :try_start_2b
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 84
    .local v3, stream:Ljava/io/FileOutputStream;
    invoke-virtual {v3, p2}, Ljava/io/FileOutputStream;->write([B)V

    .line 85
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_36} :catch_52

    .line 93
    .end local v0           #dir:Ljava/io/File;
    .end local v2           #file:Ljava/io/File;
    .end local v3           #stream:Ljava/io/FileOutputStream;
    :cond_36
    :goto_36
    sget-object v4, Lcom/google/android/apps/plus/content/EsMediaCache;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/apps/plus/service/ImageCache;->notifyMediaImageChange(Lcom/google/android/apps/plus/content/CachedImageRequest;[B)V

    .line 94
    return-void

    .line 76
    .restart local v0       #dir:Ljava/io/File;
    :catch_3c
    move-exception v1

    .line 77
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "EsMediaCache"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot create cache media directory: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_22

    .line 86
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v2       #file:Ljava/io/File;
    :catch_52
    move-exception v1

    .line 87
    .local v1, e:Ljava/io/IOException;
    const-string v4, "EsMediaCache"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot write media file to cache: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_36
.end method

.method public static loadMedia(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/Map;
    .registers 8
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CachedImageRequest;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/plus/content/CachedImageRequest;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, requests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/CachedImageRequest;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 188
    .local v3, map:Ljava/util/Map;,"Ljava/util/Map<Lcom/google/android/apps/plus/content/CachedImageRequest;[B>;"
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 189
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_c

    .line 202
    :cond_b
    return-object v3

    .line 193
    :cond_c
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/CachedImageRequest;

    .line 194
    .local v4, request:Lcom/google/android/apps/plus/content/CachedImageRequest;
    invoke-static {p0, v4}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;)[B

    move-result-object v1

    .line 195
    .local v1, bytes:[B
    if-eqz v1, :cond_26

    .line 196
    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_10

    .line 198
    :cond_26
    invoke-static {p0, v0, v4}, Lcom/google/android/apps/plus/service/ImageDownloader;->downloadImage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;)V

    goto :goto_10
.end method

.method public static obtainAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "avatarUrl"
    .parameter "rounded"

    .prologue
    .line 152
    new-instance v0, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    const/4 v3, 0x2

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v4

    invoke-direct {v0, p2, p3, v3, v4}, Lcom/google/android/apps/plus/content/AvatarImageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 154
    .local v0, request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainImage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 155
    .local v2, squareAvatar:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1a

    if-eqz p4, :cond_1a

    .line 156
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 157
    .local v1, roundAvatar:Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 160
    .end local v1           #roundAvatar:Landroid/graphics/Bitmap;
    :goto_19
    return-object v1

    :cond_1a
    move-object v1, v2

    goto :goto_19
.end method

.method public static obtainImage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;)Landroid/graphics/Bitmap;
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "request"

    .prologue
    const/4 v4, 0x0

    .line 168
    invoke-static {p0, p2}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;)[B

    move-result-object v6

    .line 169
    .local v6, image:[B
    if-nez v6, :cond_17

    .line 170
    new-instance v0, Lcom/google/android/apps/plus/api/DownloadImageOperation;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/DownloadImageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 172
    .local v0, op:Lcom/google/android/apps/plus/api/DownloadImageOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->start()V

    .line 173
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->getImageBytes()[B

    move-result-object v6

    .line 176
    .end local v0           #op:Lcom/google/android/apps/plus/api/DownloadImageOperation;
    :cond_17
    if-eqz v6, :cond_1f

    .line 177
    const/4 v1, 0x0

    array-length v2, v6

    invoke-static {v6, v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 179
    :cond_1f
    return-object v4
.end method
