.class final Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedEventFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 317
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateEventComment$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleCreateCommentComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 335
    return-void
.end method

.method public final onDeleteEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 352
    return-void
.end method

.method public final onEventInviteComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$900(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 347
    return-void
.end method

.method public final onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 340
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    .line 341
    .local v0, photoLocation:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleNewPhotoComplete(ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    invoke-static {v1, p1, p2, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method public final onReadEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$302(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Z)Z

    .line 321
    if-eqz p2, :cond_14

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_14

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$402(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 324
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleGetEventUpdatesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 325
    return-void
.end method

.method public final onReportActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleReportEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 358
    return-void
.end method

.method public final onSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 330
    return-void
.end method

.method public final onSharePhotosToEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleSharePhotosToEventCallBack$b5e9bbb(Lcom/google/android/apps/plus/service/ServiceResult;)V

    .line 363
    return-void
.end method
