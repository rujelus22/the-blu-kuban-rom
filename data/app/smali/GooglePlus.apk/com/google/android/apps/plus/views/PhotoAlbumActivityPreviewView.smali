.class public Lcom/google/android/apps/plus/views/PhotoAlbumActivityPreviewView;
.super Lcom/google/android/apps/plus/views/ActivityPreviewView;
.source "PhotoAlbumActivityPreviewView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method


# virtual methods
.method public setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    .registers 12
    .parameter "activity"

    .prologue
    const/16 v9, 0x8

    const/4 v3, 0x0

    .line 57
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;->setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoAlbumActivityPreviewView;->getActivity()Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoAlbumActivityPreviewView;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoAlbumActivityPreviewView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300b4

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/PhotoAlbumActivityPreviewView;->addView(Landroid/view/View;)V

    const v1, 0x7f090218

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x3

    new-array v5, v2, [Lcom/google/android/apps/plus/views/EsImageView;

    const v2, 0x7f090219

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    aput-object v2, v5, v3

    const/4 v6, 0x1

    const v2, 0x7f09021a

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    aput-object v2, v5, v6

    const/4 v6, 0x2

    const v2, 0x7f09021b

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    aput-object v2, v5, v6

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->getImages()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_94

    const-string v6, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_94

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_69
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    array-length v6, v5

    move v1, v3

    :goto_6f
    if-ge v1, v6, :cond_98

    aget-object v7, v5, v1

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_90

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_90

    invoke-virtual {v7, v3}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    :cond_90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6f

    :cond_94
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_69

    :cond_98
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoAlbumActivityPreviewView;->invalidate()V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoAlbumActivityPreviewView;->requestLayout()V

    .line 61
    return-void
.end method
