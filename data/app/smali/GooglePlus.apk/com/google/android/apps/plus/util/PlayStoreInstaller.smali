.class public final Lcom/google/android/apps/plus/util/PlayStoreInstaller;
.super Ljava/lang/Object;
.source "PlayStoreInstaller.java"


# static fields
.field private static final PLAY_STORE_TEST_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 42
    const-string v0, "market://search?q=com.android.youtube"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->PLAY_STORE_TEST_URI:Landroid/net/Uri;

    return-void
.end method

.method private static canResolveIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z
    .registers 5
    .parameter "packageManager"
    .parameter "intent"

    .prologue
    const/4 v1, 0x0

    .line 128
    if-nez p1, :cond_4

    .line 132
    :cond_3
    :goto_3
    return v1

    .line 131
    :cond_4
    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 132
    .local v0, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v1, 0x1

    goto :goto_3
.end method

.method public static getBackgroundInstallIntent(Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter "packageName"

    .prologue
    const/4 v5, 0x1

    .line 86
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://market.android.com/details?id=%1$s&rdid=%1$s&rdot=1"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 88
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string v1, "use_direct_purchase"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 90
    return-object v0
.end method

.method public static getContinueIntent(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter "packageManager"
    .parameter "packageName"
    .parameter "data"

    .prologue
    .line 121
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.VIEW_DEEP_LINK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string v1, "stream"

    const-string v2, "vnd.google.deeplink://link/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "deep_link_id"

    invoke-virtual {v2, v3, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "gplus_source"

    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 124
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->canResolveIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_30

    .end local v0           #intent:Landroid/content/Intent;
    :goto_2f
    return-object v0

    .restart local v0       #intent:Landroid/content/Intent;
    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method public static getInstallIntent(Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter "packageName"

    .prologue
    const/4 v5, 0x1

    .line 97
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "market://details?id=%1$s"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 99
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "use_direct_purchase"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 100
    return-object v0
.end method

.method public static getLaunchIntent(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "packageManager"
    .parameter "packageName"

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 108
    .local v0, intent:Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->canResolveIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_b

    .end local v0           #intent:Landroid/content/Intent;
    :goto_a
    return-object v0

    .restart local v0       #intent:Landroid/content/Intent;
    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public static isPackageInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .registers 6
    .parameter "packageManager"
    .parameter "packageName"

    .prologue
    .line 72
    const/16 v3, 0x80

    invoke-virtual {p0, v3}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v2

    .line 74
    .local v2, packages:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_20

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ApplicationInfo;

    .line 75
    .local v1, packageInfo:Landroid/content/pm/ApplicationInfo;
    iget-object v3, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 76
    const/4 v3, 0x1

    .line 79
    .end local v1           #packageInfo:Landroid/content/pm/ApplicationInfo;
    :goto_1f
    return v3

    :cond_20
    const/4 v3, 0x0

    goto :goto_1f
.end method

.method public static isPlayStoreInstalled(Landroid/content/pm/PackageManager;)Z
    .registers 8
    .parameter "packageManager"

    .prologue
    const/4 v3, 0x0

    .line 52
    :try_start_1
    const-string v4, "com.android.vending"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_7} :catch_b

    move-result-object v1

    .line 53
    .local v1, info:Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_2e

    .line 64
    .end local v1           #info:Landroid/content/pm/ApplicationInfo;
    :cond_a
    :goto_a
    return v3

    .line 56
    :catch_b
    move-exception v0

    .line 57
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "DeepLinking"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 58
    const-string v4, "DeepLinking"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "com.android.vending not found: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 63
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1       #info:Landroid/content/pm/ApplicationInfo;
    :cond_2e
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    sget-object v4, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->PLAY_STORE_TEST_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 64
    .local v2, intent:Landroid/content/Intent;
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->canResolveIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z

    move-result v3

    goto :goto_a
.end method

.method public static notifyCompletedInstall(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 21
    .parameter "context"
    .parameter "authorName"
    .parameter "appName"
    .parameter "packageName"
    .parameter "data"

    .prologue
    .line 146
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 151
    .local v7, packageManager:Landroid/content/pm/PackageManager;
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v7, v0, v1}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getContinueIntent(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 152
    .local v4, intent:Landroid/content/Intent;
    if-nez v4, :cond_3d

    .line 153
    move-object/from16 v0, p3

    invoke-static {v7, v0}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getLaunchIntent(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 154
    const-string v12, "DeepLinking"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_3d

    .line 155
    const-string v12, "DeepLinking"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Could not resolve continue Intent for "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " falling back to launch "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_3d
    const/high16 v12, 0x1400

    invoke-virtual {v4, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    long-to-int v9, v12

    .line 162
    .local v9, requestCode:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 163
    .local v10, when:J
    const v12, 0x7f080403

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    const/4 v14, 0x1

    aput-object p1, v13, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165
    .local v2, contentText:Ljava/lang/String;
    new-instance v5, Landroid/app/Notification;

    const v12, 0x7f020140

    invoke-direct {v5, v12, v2, v10, v11}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 167
    .local v5, notification:Landroid/app/Notification;
    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v9, v4, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 169
    .local v8, pendingIntent:Landroid/app/PendingIntent;
    const v12, 0x7f080029

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v12, v2, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 171
    iget v12, v5, Landroid/app/Notification;->flags:I

    or-int/lit8 v12, v12, 0x10

    iput v12, v5, Landroid/app/Notification;->flags:I

    .line 172
    iget v12, v5, Landroid/app/Notification;->defaults:I

    or-int/lit8 v12, v12, 0x4

    iput v12, v5, Landroid/app/Notification;->defaults:I

    .line 174
    const-string v12, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    .line 176
    .local v6, notificationManager:Landroid/app/NotificationManager;
    const-string v12, "%s:notifications:%s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aput-object p3, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 177
    .local v3, id:Ljava/lang/String;
    const/16 v12, 0x3e8

    invoke-virtual {v6, v3, v12, v5}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 178
    return-void
.end method
