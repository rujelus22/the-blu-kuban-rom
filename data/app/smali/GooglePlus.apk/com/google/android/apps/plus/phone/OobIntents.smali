.class public Lcom/google/android/apps/plus/phone/OobIntents;
.super Ljava/lang/Object;
.source "OobIntents.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/phone/OobIntents;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mInitial:Z

.field private final mStep:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/apps/plus/phone/OobIntents$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/phone/OobIntents$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/phone/OobIntents;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IZ)V
    .registers 3
    .parameter "step"
    .parameter "initial"

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    .line 37
    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_10
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    .line 158
    return-void

    .line 157
    :cond_13
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public static getInitialIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;Ljava/lang/String;)Landroid/content/Intent;
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "oobResponse"
    .parameter "settings"
    .parameter "upgradeOrigin"

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 46
    if-eqz p2, :cond_2f

    .line 47
    new-instance v2, Lcom/google/android/apps/plus/phone/OobIntents;

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/plus/phone/OobIntents;-><init>(IZ)V

    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "oob_intents"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "network_oob"

    new-instance v3, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    invoke-direct {v3, p2}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "oob_origin"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    :goto_2e
    return-object v1

    .line 51
    :cond_2f
    invoke-static {p0, p1, p3, v1}, Lcom/google/android/apps/plus/phone/OobIntents;->nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I

    move-result v0

    .line 52
    .local v0, step:I
    const/4 v1, 0x5

    if-ne v0, v1, :cond_38

    .line 53
    const/4 v1, 0x0

    goto :goto_2e

    .line 56
    :cond_38
    new-instance v1, Lcom/google/android/apps/plus/phone/OobIntents;

    invoke-direct {v1, v0, v3}, Lcom/google/android/apps/plus/phone/OobIntents;-><init>(IZ)V

    invoke-static {p0, p1, p3, v1}, Lcom/google/android/apps/plus/phone/OobIntents;->getStepIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2e
.end method

.method private static getStepIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "settings"
    .parameter "intents"

    .prologue
    .line 131
    iget v0, p3, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    packed-switch v0, :pswitch_data_68

    .line 143
    const/4 v0, 0x0

    :goto_6
    return-object v0

    .line 133
    :pswitch_7
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "plus_pages"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "oob_intents"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_6

    .line 136
    :pswitch_23
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/OobSuggestedPeopleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "oob_intents"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_6

    .line 138
    :pswitch_3a
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/OobContactsSyncActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "oob_intents"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_6

    .line 140
    :pswitch_51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/OobInstantUploadActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "oob_intents"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_6

    .line 131
    :pswitch_data_68
    .packed-switch 0x1
        :pswitch_7
        :pswitch_23
        :pswitch_3a
        :pswitch_51
    .end packed-switch
.end method

.method private static nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "settings"
    .parameter "currentStep"

    .prologue
    const/4 v0, 0x5

    .line 91
    :goto_1
    packed-switch p3, :pswitch_data_3c

    .line 125
    :goto_4
    :pswitch_4
    return v0

    .line 93
    :pswitch_5
    if-eqz p2, :cond_9

    .line 94
    const/4 v0, 0x1

    goto :goto_4

    .line 96
    :cond_9
    const/4 p3, 0x1

    goto :goto_1

    .line 100
    :pswitch_b
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenWarmWelcome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-nez v1, :cond_19

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-nez v1, :cond_19

    .line 101
    const/4 v0, 0x2

    goto :goto_4

    .line 103
    :cond_19
    const/4 p3, 0x2

    goto :goto_1

    .line 107
    :pswitch_1b
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->needContactSyncOob(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_29

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-nez v1, :cond_29

    .line 108
    const/4 v0, 0x3

    goto :goto_4

    .line 110
    :cond_29
    const/4 p3, 0x3

    goto :goto_1

    .line 114
    :pswitch_2b
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->needInstantUploadOob(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_39

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-nez v1, :cond_39

    .line 116
    const/4 v0, 0x4

    goto :goto_4

    .line 118
    :cond_39
    const/4 p3, 0x4

    goto :goto_1

    .line 91
    nop

    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_5
        :pswitch_b
        :pswitch_1b
        :pswitch_2b
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public final getNextIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)Landroid/content/Intent;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "settings"

    .prologue
    .line 63
    iget v2, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    invoke-static {p1, p2, p3, v2}, Lcom/google/android/apps/plus/phone/OobIntents;->nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I

    move-result v1

    .line 64
    .local v1, step:I
    const/4 v2, 0x5

    if-ne v1, v2, :cond_b

    .line 65
    const/4 v2, 0x0

    .line 72
    :goto_a
    return-object v2

    .line 71
    :cond_b
    iget v2, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    if-nez v2, :cond_1a

    const/4 v0, 0x1

    .line 72
    .local v0, initial:Z
    :goto_10
    new-instance v2, Lcom/google/android/apps/plus/phone/OobIntents;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/plus/phone/OobIntents;-><init>(IZ)V

    invoke-static {p1, p2, p3, v2}, Lcom/google/android/apps/plus/phone/OobIntents;->getStepIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/phone/OobIntents;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_a

    .line 71
    .end local v0           #initial:Z
    :cond_1a
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final isInitialIntent()Z
    .registers 2

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    return v0
.end method

.method public final isLastIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)Z
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "settings"

    .prologue
    .line 86
    iget v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    invoke-static {p1, p2, p3, v0}, Lcom/google/android/apps/plus/phone/OobIntents;->nextStep(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    .prologue
    .line 151
    iget v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 152
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobIntents;->mInitial:Z

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    return-void

    .line 152
    :cond_e
    const/4 v0, 0x0

    goto :goto_a
.end method
