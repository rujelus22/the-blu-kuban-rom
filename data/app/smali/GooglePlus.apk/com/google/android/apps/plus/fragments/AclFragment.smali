.class public Lcom/google/android/apps/plus/fragments/AclFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "AclFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;,
        Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAdapter:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 85
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AclFragment;->dismiss()V

    .line 229
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 200
    const v4, 0x7f030057

    invoke-virtual {p1, v4, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 201
    .local v3, view:Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AclFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 204
    .local v0, args:Landroid/os/Bundle;
    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 206
    .local v2, listView:Landroid/widget/ListView;
    const-string v4, "audience"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    .line 207
    .local v1, audienceData:Lcom/google/android/apps/plus/content/AudienceData;
    const-string v4, "account"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/AclFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 208
    new-instance v4, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AclFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/AudienceData;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/AclFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;

    .line 209
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/AclFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 210
    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 213
    const v4, 0x7f09005c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    const v4, 0x7f09005d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AclFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    const-string v5, "acl_display"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 220
    return-object v3
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 15
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/AclFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;

    invoke-virtual {v5, p3}, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;

    .line 237
    .local v2, item:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;
    iget v5, v2, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mType:I

    packed-switch v5, :pswitch_data_46

    .line 253
    :cond_d
    :goto_d
    return-void

    .line 239
    :pswitch_e
    iget-object v3, v2, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    .line 240
    .local v3, person:Lcom/google/android/apps/plus/content/PersonData;
    const/4 v4, 0x0

    .line 241
    .local v4, personId:Ljava/lang/String;
    const/4 v0, 0x0

    .line 242
    .local v0, gaiaId:Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_20

    .line 243
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v0

    .line 245
    :cond_20
    if-eqz v0, :cond_31

    .line 246
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "g:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 248
    :cond_31
    if-eqz v4, :cond_d

    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AclFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/AclFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v6, v4, v7, v8}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 251
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AclFragment;->dismiss()V

    .line 252
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/AclFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_d

    .line 237
    :pswitch_data_46
    .packed-switch 0x0
        :pswitch_e
    .end packed-switch
.end method
