.class public final Lcom/google/android/apps/plus/views/ClickableImageButton;
.super Ljava/lang/Object;
.source "ClickableImageButton.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;
    }
.end annotation


# static fields
.field private static sImageSelectedPaint:Landroid/graphics/Paint;


# instance fields
.field private mClicked:Z

.field private mClickedBitmap:Landroid/graphics/Bitmap;

.field private mContentDescription:Ljava/lang/CharSequence;

.field private mDefaultBitmap:Landroid/graphics/Bitmap;

.field private mListener:Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;

.field private mRect:Landroid/graphics/Rect;

.field private mSelectionRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;Ljava/lang/CharSequence;)V
    .registers 12
    .parameter "context"
    .parameter "defaultBitmap"
    .parameter "clickedBitmap"
    .parameter "listener"
    .parameter "contentDescription"

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mDefaultBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_2e

    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mDefaultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mDefaultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    .line 55
    :cond_2e
    iput-object p3, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClickedBitmap:Landroid/graphics/Bitmap;

    .line 56
    iput-object p4, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mListener:Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;

    .line 57
    iput-object p5, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mContentDescription:Ljava/lang/CharSequence;

    .line 59
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableImageButton;->sImageSelectedPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_5f

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 61
    sput-object v0, Lcom/google/android/apps/plus/views/ClickableImageButton;->sImageSelectedPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x4080

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 62
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableImageButton;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableImageButton;->sImageSelectedPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 66
    :cond_5f
    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p2
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter "canvas"

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClicked:Z

    if-eqz v0, :cond_46

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClickedBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_46

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mListener:Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;

    if-eqz v0, :cond_46

    const/4 v7, 0x1

    .line 125
    .local v7, drawSelectionRect:Z
    :goto_e
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClicked:Z

    if-eqz v0, :cond_48

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClickedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_48

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mListener:Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;

    if-eqz v0, :cond_48

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClickedBitmap:Landroid/graphics/Bitmap;

    .line 128
    .local v6, bitmapToDraw:Landroid/graphics/Bitmap;
    :goto_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v6, v1, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 129
    if-eqz v7, :cond_45

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, 0x2

    int-to-float v1, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v0, v0, 0x2

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v0, v0, -0x2

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, -0x2

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/ClickableImageButton;->sImageSelectedPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 132
    :cond_45
    return-void

    .line 124
    .end local v6           #bitmapToDraw:Landroid/graphics/Bitmap;
    .end local v7           #drawSelectionRect:Z
    :cond_46
    const/4 v7, 0x0

    goto :goto_e

    .line 125
    .restart local v7       #drawSelectionRect:Z
    :cond_48
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mDefaultBitmap:Landroid/graphics/Bitmap;

    goto :goto_1c
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .registers 8
    .parameter "x"
    .parameter "y"
    .parameter "event"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    const/4 v3, 0x3

    if-ne p3, v3, :cond_8

    .line 158
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClicked:Z

    .line 181
    :goto_7
    return v1

    .line 162
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mSelectionRect:Landroid/graphics/Rect;

    if-nez v3, :cond_1a

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    .line 163
    .local v0, checkRect:Landroid/graphics/Rect;
    :goto_e
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_1d

    .line 164
    if-ne p3, v1, :cond_18

    .line 165
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClicked:Z

    :cond_18
    move v1, v2

    .line 167
    goto :goto_7

    .line 162
    .end local v0           #checkRect:Landroid/graphics/Rect;
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mSelectionRect:Landroid/graphics/Rect;

    goto :goto_e

    .line 170
    .restart local v0       #checkRect:Landroid/graphics/Rect;
    :cond_1d
    packed-switch p3, :pswitch_data_34

    goto :goto_7

    .line 172
    :pswitch_21
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClicked:Z

    goto :goto_7

    .line 177
    :pswitch_24
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClicked:Z

    if-eqz v3, :cond_31

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mListener:Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;

    if-eqz v3, :cond_31

    .line 178
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mListener:Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;

    invoke-interface {v3, p0}, Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;->onClickableImageButtonClick(Lcom/google/android/apps/plus/views/ClickableImageButton;)V

    .line 180
    :cond_31
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mClicked:Z

    goto :goto_7

    .line 170
    :pswitch_data_34
    .packed-switch 0x0
        :pswitch_21
        :pswitch_24
    .end packed-switch
.end method

.method public final setPosition(II)V
    .registers 6
    .parameter "left"
    .parameter "top"

    .prologue
    .line 92
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mDefaultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    add-int/2addr v1, p1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mDefaultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    add-int/2addr v2, p2

    invoke-direct {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    .line 94
    return-void
.end method

.method public final setSelectionPadding(I)V
    .registers 7
    .parameter "padding"

    .prologue
    .line 110
    if-gtz p1, :cond_6

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mSelectionRect:Landroid/graphics/Rect;

    .line 116
    :goto_5
    return-void

    .line 113
    :cond_6
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, p1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, p1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, p1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ClickableImageButton;->mSelectionRect:Landroid/graphics/Rect;

    goto :goto_5
.end method
