.class public final Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "SetNotificationLastReadTimeOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeRequest;",
        "Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mReadTimestamp:D


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;D)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "readTimestamp"

    .prologue
    .line 34
    const-string v3, "updatenotificationslastreadtime"

    invoke-static {}, Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeRequestJson;->getInstance()Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeResponseJson;->getInstance()Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 39
    iput-wide p5, p0, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;->mReadTimestamp:D

    .line 40
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;->onStartResultProcessing()V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 19
    check-cast p1, Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeRequest;

    .end local p1
    iget-wide v0, p0, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;->mReadTimestamp:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UpdateNotificationsLastReadTimeRequest;->timeMs:Ljava/lang/Double;

    return-void
.end method
