.class public Lcom/google/android/apps/plus/phone/ShareActivity;
.super Lcom/google/android/apps/plus/phone/PostActivity;
.source "ShareActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;,
        Lcom/google/android/apps/plus/phone/ShareActivity$DialogListener;
    }
.end annotation


# instance fields
.field private mDialogListener:Landroid/content/DialogInterface$OnClickListener;

.field private mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

.field private mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostActivity;-><init>()V

    .line 93
    new-instance v0, Lcom/google/android/apps/plus/phone/ShareActivity$DialogListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ShareActivity$DialogListener;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    .line 94
    new-instance v0, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    .line 447
    return-void
.end method

.method private copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 6
    .parameter "intentExtra"
    .parameter "argsExtra"
    .parameter "args"

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, value:Ljava/lang/String;
    if-eqz v0, :cond_d

    .line 102
    invoke-virtual {p3, p2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_d
    return-void
.end method


# virtual methods
.method protected final getPostFragmentArguments()Landroid/os/Bundle;
    .registers 9

    .prologue
    .line 159
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getPostFragmentArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 160
    .local v0, args:Landroid/os/Bundle;
    if-nez v0, :cond_8

    .line 161
    const/4 v0, 0x0

    .line 191
    .end local v0           #args:Landroid/os/Bundle;
    :goto_7
    return-object v0

    .line 164
    .restart local v0       #args:Landroid/os/Bundle;
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 166
    .local v2, intent:Landroid/content/Intent;
    const-string v4, "com.google.android.apps.plus.CID"

    const-string v5, "cid"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 167
    const-string v4, "com.google.android.apps.plus.LOCATION_NAME"

    const-string v5, "location_name"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 168
    const-string v4, "com.google.android.apps.plus.EXTERNAL_ID"

    const-string v5, "external_id"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 169
    const-string v4, "com.google.android.apps.plus.FOOTER"

    const-string v5, "footer"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 170
    const-string v4, "com.google.android.apps.plus.LATITUDE"

    const-string v5, "latitude"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 171
    const-string v4, "com.google.android.apps.plus.LONGITUDE"

    const-string v5, "longitude"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 172
    const-string v4, "com.google.android.apps.plus.ADDRESS"

    const-string v5, "address"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 173
    const-string v4, "com.google.android.apps.plus.DEEP_LINK_ID"

    const-string v5, "deep_link_id"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 175
    const-string v4, "com.google.android.apps.plus.DEEP_LINK_METADATA"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 176
    .local v1, deepLinkMetadata:Landroid/os/Bundle;
    if-eqz v1, :cond_51

    .line 177
    const-string v4, "deep_link_metadata"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 180
    :cond_51
    const-string v4, "com.google.android.apps.plus.IS_FROM_PLUSONE"

    const-string v5, "is_from_plusone"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 181
    :cond_6b
    const-string v4, "android.intent.extra.TEXT"

    const-string v5, "android.intent.extra.TEXT"

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 184
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    .line 185
    .local v3, urlFromData:Ljava/lang/String;
    if-eqz v3, :cond_89

    const-string v4, "com.google.android.apps.plus.SHARE_GOOGLE"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_89

    .line 186
    const-string v4, "url"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_89
    const-string v4, "api_info"

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_7
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 476
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final getViewId()I
    .registers 2

    .prologue
    .line 196
    const v0, 0x7f0300b1

    return v0
.end method

.method public final invalidateMenu()V
    .registers 3

    .prologue
    .line 319
    const v0, 0x7f10001e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->createTitlebarButtons(I)V

    .line 320
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_f

    .line 321
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->invalidateOptionsMenu()V

    .line 323
    :cond_f
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 16
    .parameter "savedInstanceState"

    .prologue
    const/4 v13, 0x0

    const/4 v7, 0x0

    .line 118
    if-nez p1, :cond_149

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.SHARE_GOOGLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_fe

    const-string v0, "com.google.android.apps.plus.API_KEY"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "com.google.android.apps.plus.CLIENT_ID"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "com.google.android.apps.plus.VERSION"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    const-string v4, "from_signup"

    invoke-virtual {v3, v4, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v6, "calling_package"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_58

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_58

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_58

    if-nez v4, :cond_ae

    :cond_58
    :goto_58
    iput-object v7, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 124
    :goto_5a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    if-nez v0, :cond_72

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v0}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/ShareActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0, v13}, Lcom/google/android/apps/plus/phone/ShareActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->finish()V

    .line 128
    :cond_72
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/PostActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    if-nez p1, :cond_85

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v0}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/ShareActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 137
    :cond_85
    const v0, 0x7f090191

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/phone/ShareActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/ShareActivity$1;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    const v0, 0x7f09019c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/phone/ShareActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/ShareActivity$2;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    const v0, 0x7f080029

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 155
    return-void

    .line 119
    :cond_ae
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_f0

    const-string v0, "com.android.vending"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c8

    const-string v0, "com.google.android.music"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e2

    :cond_c8
    const-string v8, "659910861946.apps.googleusercontent.com"

    :cond_ca
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v10

    move-object v9, v3

    move-object v11, v5

    move-object v12, v0

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    move-object v7, v6

    goto/16 :goto_58

    :cond_e2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_58

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_ca

    goto/16 :goto_58

    :cond_f0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_58

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_ca

    goto/16 :goto_58

    :cond_fe
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v8

    const-string v0, "android.support.v4.app.EXTRA_CALLING_PACKAGE"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11a

    const-string v0, "calling_package"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_11a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_155

    const-string v9, "com.google.android.apps.social"

    :goto_122
    const-string v0, "com.google.android.apps.plus.VERSION"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v6, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v9, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    move-object v1, v7

    move-object v2, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    move-object v7, v0

    goto/16 :goto_58

    .line 121
    :cond_149
    const-string v0, "ShareActivity.mInfo"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    goto/16 :goto_5a

    :cond_155
    move-object v9, v0

    goto :goto_122
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 10
    .parameter "dialogId"
    .parameter "args"

    .prologue
    const/4 v6, 0x1

    const v5, 0x104000a

    const/4 v4, 0x0

    .line 360
    sparse-switch p1, :sswitch_data_ce

    .line 425
    const/4 v1, 0x0

    :goto_9
    return-object v1

    .line 362
    :sswitch_a
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 364
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f08017a

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 367
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_9

    .line 371
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_24
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 372
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f08017d

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 375
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_9

    .line 379
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_3e
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 380
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f08017b

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 383
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_9

    .line 387
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_58
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 389
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f08017c

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 392
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_9

    .line 396
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_72
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 397
    .local v1, progressDialog:Landroid/app/ProgressDialog;
    const v2, 0x7f080164

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ShareActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 398
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 399
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_9

    .line 404
    .end local v1           #progressDialog:Landroid/app/ProgressDialog;
    :sswitch_88
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 405
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f080169

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 406
    const v2, 0x7f0801c6

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 407
    const v2, 0x7f0801c8

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 408
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_9

    .line 412
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_a9
    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v1

    goto/16 :goto_9

    .line 416
    :sswitch_af
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 417
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f08019c

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 418
    const v2, 0x7f08019e

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 419
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 420
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 421
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_9

    .line 360
    :sswitch_data_ce
    .sparse-switch
        0x31db -> :sswitch_24
        0x409e -> :sswitch_72
        0x5339 -> :sswitch_a
        0x58a1 -> :sswitch_58
        0x6e27 -> :sswitch_3e
        0x1bfb7a8 -> :sswitch_88
        0x1d71d84 -> :sswitch_af
        0x7f09003e -> :sswitch_a9
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10001e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 311
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 340
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_16

    .line 353
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 343
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->onDiscard()V

    goto :goto_9

    .line 348
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->post()Z

    goto :goto_9

    .line 340
    :sswitch_data_16
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f09029b -> :sswitch_10
        0x7f09029c -> :sswitch_a
    .end sparse-switch
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 6
    .parameter "menu"

    .prologue
    .line 332
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1d

    .line 333
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 334
    .local v1, item:Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f09029b

    if-ne v2, v3, :cond_1b

    const/4 v2, 0x1

    :goto_15
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 332
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 334
    :cond_1b
    const/4 v2, 0x0

    goto :goto_15

    .line 336
    .end local v1           #item:Landroid/view/MenuItem;
    :cond_1d
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 296
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/PostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_e

    .line 299
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 302
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    if-eqz v0, :cond_19

    .line 303
    const-string v0, "ShareActivity.mInfo"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 305
    :cond_19
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->finish()V

    .line 328
    return-void
.end method

.method protected final showTitlebar(ZZ)V
    .registers 8
    .parameter "showAnimation"
    .parameter "enableUp"

    .prologue
    const/4 v4, 0x0

    .line 481
    invoke-super {p0, p1, v4}, Lcom/google/android/apps/plus/phone/PostActivity;->showTitlebar(ZZ)V

    .line 483
    const v2, 0x7f090236

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 484
    .local v1, titleLayout:Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00f4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 486
    .local v0, paddingLeft:I
    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 487
    return-void
.end method
