.class public Lcom/google/android/apps/plus/service/PackageAddedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PackageAddedReceiver.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "receivedIntent"

    .prologue
    .line 27
    const-string v4, "DeepLinking"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 28
    const-string v4, "DeepLinking"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PackageAddedReceiver.onReceive() "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    :cond_1d
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    .line 31
    .local v3, uri:Ljava/lang/String;
    if-eqz v3, :cond_3a

    .line 32
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 33
    .local v2, packageName:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 34
    .local v1, packageManager:Landroid/content/pm/PackageManager;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->isPackageInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3a

    .line 35
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 36
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {p1, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->notifyDeepLinkingInstall(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    .line 39
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #packageManager:Landroid/content/pm/PackageManager;
    .end local v2           #packageName:Ljava/lang/String;
    :cond_3a
    return-void
.end method
